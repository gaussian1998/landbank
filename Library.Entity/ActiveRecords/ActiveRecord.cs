﻿using Library.Entity;
using Library.Entity.Edmx;


namespace LandBankEntity.ActiveRecords
{
    public class ExceptionRecord : GenericRecord<AS_Exception> { }
    public class UsersRecord : GenericRecord<AS_Users> { }
    public class CodeTableRecord : GenericRecord<AS_Code_Table> { }
    public class UsersRoleRecord : GenericRecord<AS_Users_Role> { }
    public class FlowNameRecord : GenericRecord<AS_Flow_Name> { }
    public class FlowTemplateRecord : GenericRecord<AS_Flow_Template> { }
    public class FlowTemplateFlowRoleRecord : GenericRecord<AS_Flow_Template_Flow_Role> { }
    public class DocNameRecord : GenericRecord<AS_Doc_Name> { }
    public class AgentFlowRecord : GenericRecord<AS_Agent_Flow> { }
    public class FunctionRecord : GenericRecord<AS_Programs> { }
    public class FunctionRolesRecord : GenericRecord<AS_Programs_Roles> { }
    public class TRX_HeadersRecord : GenericRecord<AS_TRX_Headers> { }
    public class FlowOpenRecord : GenericRecord<AS_Flow_Open> { }
    public class GLAccDeprnRecord : GenericRecord<AS_GL_Acc_Deprn> { }
    public class GLAccMonthsRecord : GenericRecord<AS_GL_Acc_Months> { }
    //public class AssetsMpRecord : GenericAccess<AS_LandBankEntities, AS_Assets_MP_Record>{ }
    //public class AssetMpDeprn : GenericAccess<AS_LandBankEntities, AS_ASSETS_MP_Deprn> { }
    public class GenericRecord<Entity> : GenericAccess<AS_LandBankEntities, Entity>
        where Entity : class, new()
    { }
}
