﻿using Library.Common.Models;
using Library.Utility;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Library.Entity
{
    /// <summary>
    ///  此版本自行管理資源釋放
    /// </summary>
    public class GenericAccess<DbContextSubType, Entity>
                        where DbContextSubType : DbContext, new()
                        where Entity : class, new()
    {
        public static List<T> Find<T>(Expression<Func<Entity, bool>> predicate)
            where T : new()
        {
            return QueryContext((context, dbSet) =>
            {

                return (from entity in dbSet.Where(predicate).ToList()
                        select MapProperty.Mapping<T, Entity>(entity)).ToList();

            }).ToList();
        }

        public static List<Entity> Find(Expression<Func<Entity, bool>> predicate)
        {
            return QueryContext((context, dbSet) =>
            {

                return dbSet.Where(predicate).ToList();
            }).ToList();
        }

        public static List<T> Find<T>(Expression<Func<Entity, bool>> predicate, Expression<Func<Entity, T>> select)
            where T : new()
        {
            return QueryContext((context, dbSet) =>
            {
                return dbSet.Where(predicate).Select(select).ToList();
            });
        }

        public static List<T> FindAll<T>()
            where T : new()
        {
            return Find<T>(m => true);
        }

        public static List<Entity> FindAll()
        {
            return Find(m => true);
        }

        public static List<T> FindAll<T>(Expression<Func<Entity, T>> select)
        {
            return QueryContext((context, dbSet) =>
            {
                return dbSet.Where(m => true).Select(select).ToList();
            });
        }

        /// <summary>
        ///  傳回分頁
        /// </summary>
        public static PageList<T> Page<T, Key>( int page, int PageSize, Expression<Func<Entity, Key>> order, Expression<Func<Entity, bool>> predicate)
             where T : new()
        {
            int real_page = (page - 1) >= 0 ? (page - 1) : 0;
            PageList<T> result = new PageList<T>();

            result.Items = QueryContext((context, dbSet) =>
            {
                var query = dbSet.AsQueryable().Where(predicate);
                result.TotalAmount = query.Count();

                return (from entity in query.OrderBy(order).Skip(real_page * PageSize).Take(PageSize).ToList()
                        select MapProperty.Mapping<T, Entity>(entity)).ToList();

            }).ToList();

            return result;
        }

        /// <summary>
        ///  傳回分頁
        /// </summary>
        public static PageList<T> Page<T, Key>(int page, int PageSize, Expression<Func<Entity, Key>> order, Expression<Func<Entity, bool>> predicate, Expression<Func<Entity, T>> select)
             where T : new()
        {
            int real_page = (page - 1) >= 0 ? (page - 1) : 0;
            PageList<T> result = new PageList<T>();

            result.Items = QueryContext((context, dbSet) =>
            {
                var query = dbSet.AsQueryable().Where(predicate);
                result.TotalAmount = query.Count();

                return query.OrderBy(order).Skip(real_page * PageSize).Take(PageSize).Select(select).ToList();

            }).ToList();

            return result;
        }

        public static Entity First(Expression<Func<Entity, bool>> predicate)
        {
            return QueryContext((context, dbSet) =>
            {
                var query = dbSet.Where(predicate);
                if (query.Any())
                    return query.First();

                throw new Exception("First: Can not Find Any Entity");
            });
        }

        public static T First<T>(Expression<Func<Entity, bool>> predicate)
            where T : new()
        {
            return QueryContext((context, dbSet) =>
            {
                var query = dbSet.Where(predicate);
                if (query.Any())
                {
                    var entity = query.First();
                    return MapProperty.Mapping<T, Entity>(entity);
                }

                throw new Exception("First<T>: Can not Find Any Entity");
            });
        }

        public static T First<T>(Expression<Func<Entity, bool>> predicate, Expression<Func<Entity, T>> select)
            where T : new()
        {
            return QueryContext((context, dbSet) =>
            {
                var query = dbSet.Where(predicate).Select(select);
                if (query.Any())
                    return query.First();

                throw new Exception("First<T>: Can not Find Any Entity");
            });
        }

        public static Dictionary<Key,List<T>> Group<Key,T>(Expression<Func<Entity,Key>> keySelector, Expression<Func<Entity, T>> selector)
        {
            return QueryContext((context, dbSet) =>
            {
                Dictionary<Key, List<T>> result = new Dictionary<Key, List<T>>();
                var query = dbSet.AsQueryable().GroupBy( keySelector, selector);
                foreach (var group in query)
                    result[group.Key] = group.ToList();

                return result;
            });
        }

        public static Entity Create(Entity entity)
        {
            return ModifyContext((dbContext, dbSet) =>
            {
                return dbSet.Add(entity);
            });
        }

        public static Entity Create(Action<Entity> modify)
        {
            var entity = new Entity();
            modify(entity);

            return ModifyContext((dbContext, dbSet) =>
            {
                return dbSet.Add(entity);
            });
        }

        public static Entity CreateT(DbContext db, Action<Entity> modify)
        {
            var entity = new Entity();
            modify(entity);

            return Context( db, dbSet =>
            {
                return dbSet.Add(entity);
            });
        }

        public static Entity Create<T>(T vo)
        {
            return ModifyContext((dbContext, dbSet) =>
            {
                var entity = MapProperty.Mapping<Entity, T>(vo);

                return dbSet.Add(entity);
            });
        }

        public static Entity Create<T>(T vo, Action<Entity> beforeAdd)
        {
            return ModifyContext((context, dbSet) =>
            {
                var result = MapProperty.Mapping<Entity, T>(vo);

                beforeAdd(result);
                return dbSet.Add(result);
            });
        }

        public static bool Any(Expression<Func<Entity, bool>> predicate)
        {
            return QueryContext((context, dbSet) =>
            {
                return dbSet.Any(predicate);
            });
        }

        public static int Count(Expression<Func<Entity, bool>> predicate)
        {
            return QueryContext((context, dbSet) =>
            {
                return dbSet.AsQueryable().Where(predicate).Count();
            });
        }

        public static Entity UpdateFirst<T>(Expression<Func<Entity, bool>> predicate, T vo)
        {
            return ModifyContext((context, dbSet) =>
            {
                var entities = dbSet.Where(predicate);
                if (entities.Any())
                    return entities.First().OverrideBy(vo);

                throw new Exception("UpdateFirstBy: No Entity For Update");
            });
        }

        public static int Update(Expression<Func<Entity, bool>> predicate, Action<Entity> modify)
        {
            return ModifyContext( (context, dbSet) =>
            {
                var entities = dbSet.Where(predicate);
                foreach (var entity in entities)
                    modify(entity);

                return entities.Count();
            });
        }

        public static int UpdateT(DbContext db, Expression<Func<Entity, bool>> predicate, Action<Entity> modify)
        {
            return Context( db, dbSet =>
            {
                var entities = dbSet.Where(predicate);
                foreach (var entity in entities)
                    modify(entity);

                return entities.Count();
            });
        }

        public static Entity DeleteFirst(Expression<Func<Entity, bool>> predicate)
        {
            return ModifyContext((dbContext, dbSet) =>
            {
                var query = dbSet.Where(predicate);

                return dbSet.Remove(query.First());
            });
        }

        public static int Delete(Expression<Func<Entity, bool>> predicate)
        {
            return ModifyContext((dbContext, dbSet) =>
            {
                var query = dbSet.Where(predicate);
                dbSet.RemoveRange(query.AsEnumerable());

                return query.Count();
            });
        }

        public static T ModifyContext<T>(Func<DbContext, DbSet<Entity>, T> func)
        {
            using (DbContextSubType db = new DbContextSubType())
            {
                if (db.Set<Entity>() != null)
                {
                    T result = func(db, db.Set<Entity>());
                    db.SaveChanges();
                    return result;
                }
            }

            throw new Exception("Template parmeter T Is Not Entity Type,You Must Pass Entity Type");
        }

        public static T Context<T>(DbContext db, Func<DbSet<Entity>, T> func)
        {
            if (db.Set<Entity>() != null)
            {
                T result = func(db.Set<Entity>());
                return result;
            }

            throw new Exception("Template parmeter T Is Not Entity Type,You Must Pass Entity Type");
        }

        private static T QueryContext<T>(Func<DbContext, DbSet<Entity>, T> func)
        {
            using (DbContextSubType db = new DbContextSubType())
            {
                if (db.Set<Entity>() != null)
                    return func(db, db.Set<Entity>());
            }

            throw new Exception("Template parmeter T Is Not Entity Type,You Must Pass Entity Type");
        }
    }
}
