using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Mail_UserRepository : EFRepository<AS_Mail_User>, IAS_Mail_UserRepository
	{

	}

	public  interface IAS_Mail_UserRepository : IRepository<AS_Mail_User>
	{

	}

   public  class AS_Mail_User_Records : GenericAccessUnitOfWork<AS_Mail_User>
	{

	}

}