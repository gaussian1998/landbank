//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Library.Entity.Edmx
{
    using System;
    using System.Collections.Generic;
    
    public partial class AS_GL_Trade_Item
    {
        public int ID { get; set; }
        public string Open_Type { get; set; }
        public string Account_Type { get; set; }
        public string Account { get; set; }
        public string Branch_Ind { get; set; }
        public string Trans_Type { get; set; }
        public string Account_Key { get; set; }
        public string DB_CR { get; set; }
        public string Real_Memo { get; set; }
        public Nullable<decimal> Master_No { get; set; }
        public string Item { get; set; }
        public decimal Amount { get; set; }
        public string Amount_Field_Name { get; set; }
    }
}
