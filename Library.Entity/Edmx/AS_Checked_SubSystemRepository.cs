using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Checked_SubSystemRepository : EFRepository<AS_Checked_SubSystem>, IAS_Checked_SubSystemRepository
	{

	}

	public  interface IAS_Checked_SubSystemRepository : IRepository<AS_Checked_SubSystem>
	{

	}

   public  class AS_Checked_SubSystem_Records : GenericAccessUnitOfWork<AS_Checked_SubSystem>
	{

	}

}