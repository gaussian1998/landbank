using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Impairment_GroupRepository : EFRepository<AS_Impairment_Group>, IAS_Impairment_GroupRepository
	{

	}

	public  interface IAS_Impairment_GroupRepository : IRepository<AS_Impairment_Group>
	{

	}

   public  class AS_Impairment_Group_Records : GenericAccessUnitOfWork<AS_Impairment_Group>
	{

	}

}