using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_RoleRepository : EFRepository<AS_Role>, IAS_RoleRepository
	{

	}

	public  interface IAS_RoleRepository : IRepository<AS_Role>
	{

	}

   public  class AS_Role_Records : GenericAccessUnitOfWork<AS_Role>
	{

	}

}