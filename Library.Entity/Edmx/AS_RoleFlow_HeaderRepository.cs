using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_RoleFlow_HeaderRepository : EFRepository<AS_RoleFlow_Header>, IAS_RoleFlow_HeaderRepository
	{

	}

	public  interface IAS_RoleFlow_HeaderRepository : IRepository<AS_RoleFlow_Header>
	{

	}

   public  class AS_RoleFlow_Header_Records : GenericAccessUnitOfWork<AS_RoleFlow_Header>
	{

	}

}