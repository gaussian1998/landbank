using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Deprn_HisRepository : EFRepository<AS_Deprn_His>, IAS_Deprn_HisRepository
	{

	}

	public  interface IAS_Deprn_HisRepository : IRepository<AS_Deprn_His>
	{

	}

   public  class AS_Deprn_His_Records : GenericAccessUnitOfWork<AS_Deprn_His>
	{

	}

}