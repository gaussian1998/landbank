namespace Library.Entity.Edmx
{
	public static class RepositoryHelper
	{
		public static IUnitOfWork GetUnitOfWork()
		{
			return new EFUnitOfWork();
		}		
		
		public static Acc_EntriesByAccountingRepository GetAcc_EntriesByAccountingRepository()
		{
			var repository = new Acc_EntriesByAccountingRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static Acc_EntriesByAccountingRepository GetAcc_EntriesByAccountingRepository(IUnitOfWork unitOfWork)
		{
			var repository = new Acc_EntriesByAccountingRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static Acc_EntriesByHeaderRepository GetAcc_EntriesByHeaderRepository()
		{
			var repository = new Acc_EntriesByHeaderRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static Acc_EntriesByHeaderRepository GetAcc_EntriesByHeaderRepository(IUnitOfWork unitOfWork)
		{
			var repository = new Acc_EntriesByHeaderRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_AD_UsersRepository GetAS_AD_UsersRepository()
		{
			var repository = new AS_AD_UsersRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_AD_UsersRepository GetAS_AD_UsersRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_AD_UsersRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Agent_FlowRepository GetAS_Agent_FlowRepository()
		{
			var repository = new AS_Agent_FlowRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Agent_FlowRepository GetAS_Agent_FlowRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Agent_FlowRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Asset_CategoryRepository GetAS_Asset_CategoryRepository()
		{
			var repository = new AS_Asset_CategoryRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Asset_CategoryRepository GetAS_Asset_CategoryRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Asset_CategoryRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Asset_Category_Detail_KindsRepository GetAS_Asset_Category_Detail_KindsRepository()
		{
			var repository = new AS_Asset_Category_Detail_KindsRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Asset_Category_Detail_KindsRepository GetAS_Asset_Category_Detail_KindsRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Asset_Category_Detail_KindsRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Asset_Category_Main_KindsRepository GetAS_Asset_Category_Main_KindsRepository()
		{
			var repository = new AS_Asset_Category_Main_KindsRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Asset_Category_Main_KindsRepository GetAS_Asset_Category_Main_KindsRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Asset_Category_Main_KindsRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Asset_Category_SerialRepository GetAS_Asset_Category_SerialRepository()
		{
			var repository = new AS_Asset_Category_SerialRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Asset_Category_SerialRepository GetAS_Asset_Category_SerialRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Asset_Category_SerialRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Asset_Category_Use_TypesRepository GetAS_Asset_Category_Use_TypesRepository()
		{
			var repository = new AS_Asset_Category_Use_TypesRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Asset_Category_Use_TypesRepository GetAS_Asset_Category_Use_TypesRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Asset_Category_Use_TypesRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Asset_CostRepository GetAS_Asset_CostRepository()
		{
			var repository = new AS_Asset_CostRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Asset_CostRepository GetAS_Asset_CostRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Asset_CostRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Asset_FamilyRepository GetAS_Asset_FamilyRepository()
		{
			var repository = new AS_Asset_FamilyRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Asset_FamilyRepository GetAS_Asset_FamilyRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Asset_FamilyRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_ActivationRepository GetAS_Assets_ActivationRepository()
		{
			var repository = new AS_Assets_ActivationRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_ActivationRepository GetAS_Assets_ActivationRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_ActivationRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Activation_LBDetailRepository GetAS_Assets_Activation_LBDetailRepository()
		{
			var repository = new AS_Assets_Activation_LBDetailRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Activation_LBDetailRepository GetAS_Assets_Activation_LBDetailRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Activation_LBDetailRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Activation_PB2UserRepository GetAS_Assets_Activation_PB2UserRepository()
		{
			var repository = new AS_Assets_Activation_PB2UserRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Activation_PB2UserRepository GetAS_Assets_Activation_PB2UserRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Activation_PB2UserRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_ActivationTargetRepository GetAS_Assets_ActivationTargetRepository()
		{
			var repository = new AS_Assets_ActivationTargetRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_ActivationTargetRepository GetAS_Assets_ActivationTargetRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_ActivationTargetRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_AssetMasterRepository GetAS_Assets_AssetMasterRepository()
		{
			var repository = new AS_Assets_AssetMasterRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_AssetMasterRepository GetAS_Assets_AssetMasterRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_AssetMasterRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_BuildRepository GetAS_Assets_BuildRepository()
		{
			var repository = new AS_Assets_BuildRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_BuildRepository GetAS_Assets_BuildRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_BuildRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_CategoryRepository GetAS_Assets_Build_CategoryRepository()
		{
			var repository = new AS_Assets_Build_CategoryRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_CategoryRepository GetAS_Assets_Build_CategoryRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_CategoryRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_ChangeRepository GetAS_Assets_Build_ChangeRepository()
		{
			var repository = new AS_Assets_Build_ChangeRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_ChangeRepository GetAS_Assets_Build_ChangeRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_ChangeRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_Change_RecordRepository GetAS_Assets_Build_Change_RecordRepository()
		{
			var repository = new AS_Assets_Build_Change_RecordRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_Change_RecordRepository GetAS_Assets_Build_Change_RecordRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_Change_RecordRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_DetailRepository GetAS_Assets_Build_DetailRepository()
		{
			var repository = new AS_Assets_Build_DetailRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_DetailRepository GetAS_Assets_Build_DetailRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_DetailRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_Detail_RecordRepository GetAS_Assets_Build_Detail_RecordRepository()
		{
			var repository = new AS_Assets_Build_Detail_RecordRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_Detail_RecordRepository GetAS_Assets_Build_Detail_RecordRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_Detail_RecordRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_HeaderRepository GetAS_Assets_Build_HeaderRepository()
		{
			var repository = new AS_Assets_Build_HeaderRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_HeaderRepository GetAS_Assets_Build_HeaderRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_HeaderRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_LandRepository GetAS_Assets_Build_LandRepository()
		{
			var repository = new AS_Assets_Build_LandRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_LandRepository GetAS_Assets_Build_LandRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_LandRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_Land_RecordRepository GetAS_Assets_Build_Land_RecordRepository()
		{
			var repository = new AS_Assets_Build_Land_RecordRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_Land_RecordRepository GetAS_Assets_Build_Land_RecordRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_Land_RecordRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_LogRepository GetAS_Assets_Build_LogRepository()
		{
			var repository = new AS_Assets_Build_LogRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_LogRepository GetAS_Assets_Build_LogRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_LogRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_MPRepository GetAS_Assets_Build_MPRepository()
		{
			var repository = new AS_Assets_Build_MPRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_MPRepository GetAS_Assets_Build_MPRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_MPRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_MP_CategoryRepository GetAS_Assets_Build_MP_CategoryRepository()
		{
			var repository = new AS_Assets_Build_MP_CategoryRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_MP_CategoryRepository GetAS_Assets_Build_MP_CategoryRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_MP_CategoryRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_MP_ChangeRepository GetAS_Assets_Build_MP_ChangeRepository()
		{
			var repository = new AS_Assets_Build_MP_ChangeRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_MP_ChangeRepository GetAS_Assets_Build_MP_ChangeRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_MP_ChangeRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_MP_Change_RecordRepository GetAS_Assets_Build_MP_Change_RecordRepository()
		{
			var repository = new AS_Assets_Build_MP_Change_RecordRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_MP_Change_RecordRepository GetAS_Assets_Build_MP_Change_RecordRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_MP_Change_RecordRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_MP_RecordRepository GetAS_Assets_Build_MP_RecordRepository()
		{
			var repository = new AS_Assets_Build_MP_RecordRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_MP_RecordRepository GetAS_Assets_Build_MP_RecordRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_MP_RecordRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_MP_RetireRepository GetAS_Assets_Build_MP_RetireRepository()
		{
			var repository = new AS_Assets_Build_MP_RetireRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_MP_RetireRepository GetAS_Assets_Build_MP_RetireRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_MP_RetireRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_MP_YearsRepository GetAS_Assets_Build_MP_YearsRepository()
		{
			var repository = new AS_Assets_Build_MP_YearsRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_MP_YearsRepository GetAS_Assets_Build_MP_YearsRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_MP_YearsRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_RecordRepository GetAS_Assets_Build_RecordRepository()
		{
			var repository = new AS_Assets_Build_RecordRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_RecordRepository GetAS_Assets_Build_RecordRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_RecordRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_RetireRepository GetAS_Assets_Build_RetireRepository()
		{
			var repository = new AS_Assets_Build_RetireRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_RetireRepository GetAS_Assets_Build_RetireRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_RetireRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Build_YearsRepository GetAS_Assets_Build_YearsRepository()
		{
			var repository = new AS_Assets_Build_YearsRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Build_YearsRepository GetAS_Assets_Build_YearsRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Build_YearsRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Compare_ApplyRepository GetAS_Assets_Compare_ApplyRepository()
		{
			var repository = new AS_Assets_Compare_ApplyRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Compare_ApplyRepository GetAS_Assets_Compare_ApplyRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Compare_ApplyRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Compare_AssetsRepository GetAS_Assets_Compare_AssetsRepository()
		{
			var repository = new AS_Assets_Compare_AssetsRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Compare_AssetsRepository GetAS_Assets_Compare_AssetsRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Compare_AssetsRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_DocumentTypeRepository GetAS_Assets_DocumentTypeRepository()
		{
			var repository = new AS_Assets_DocumentTypeRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_DocumentTypeRepository GetAS_Assets_DocumentTypeRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_DocumentTypeRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_EvaluationRepository GetAS_Assets_EvaluationRepository()
		{
			var repository = new AS_Assets_EvaluationRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_EvaluationRepository GetAS_Assets_EvaluationRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_EvaluationRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Evaluation_DetailRepository GetAS_Assets_Evaluation_DetailRepository()
		{
			var repository = new AS_Assets_Evaluation_DetailRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Evaluation_DetailRepository GetAS_Assets_Evaluation_DetailRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Evaluation_DetailRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Evaluation_FileRepository GetAS_Assets_Evaluation_FileRepository()
		{
			var repository = new AS_Assets_Evaluation_FileRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Evaluation_FileRepository GetAS_Assets_Evaluation_FileRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Evaluation_FileRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_InspectRepository GetAS_Assets_InspectRepository()
		{
			var repository = new AS_Assets_InspectRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_InspectRepository GetAS_Assets_InspectRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_InspectRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Inspect_DetailRepository GetAS_Assets_Inspect_DetailRepository()
		{
			var repository = new AS_Assets_Inspect_DetailRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Inspect_DetailRepository GetAS_Assets_Inspect_DetailRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Inspect_DetailRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Inspect_PicRepository GetAS_Assets_Inspect_PicRepository()
		{
			var repository = new AS_Assets_Inspect_PicRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Inspect_PicRepository GetAS_Assets_Inspect_PicRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Inspect_PicRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_LandRepository GetAS_Assets_LandRepository()
		{
			var repository = new AS_Assets_LandRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_LandRepository GetAS_Assets_LandRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_LandRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Land_ChangeRepository GetAS_Assets_Land_ChangeRepository()
		{
			var repository = new AS_Assets_Land_ChangeRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Land_ChangeRepository GetAS_Assets_Land_ChangeRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Land_ChangeRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Land_HeaderRepository GetAS_Assets_Land_HeaderRepository()
		{
			var repository = new AS_Assets_Land_HeaderRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Land_HeaderRepository GetAS_Assets_Land_HeaderRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Land_HeaderRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Land_LogRepository GetAS_Assets_Land_LogRepository()
		{
			var repository = new AS_Assets_Land_LogRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Land_LogRepository GetAS_Assets_Land_LogRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Land_LogRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Land_MLRepository GetAS_Assets_Land_MLRepository()
		{
			var repository = new AS_Assets_Land_MLRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Land_MLRepository GetAS_Assets_Land_MLRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Land_MLRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Land_MPRepository GetAS_Assets_Land_MPRepository()
		{
			var repository = new AS_Assets_Land_MPRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Land_MPRepository GetAS_Assets_Land_MPRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Land_MPRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Land_MP_Change_RecordRepository GetAS_Assets_Land_MP_Change_RecordRepository()
		{
			var repository = new AS_Assets_Land_MP_Change_RecordRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Land_MP_Change_RecordRepository GetAS_Assets_Land_MP_Change_RecordRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Land_MP_Change_RecordRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Land_MP_RecordRepository GetAS_Assets_Land_MP_RecordRepository()
		{
			var repository = new AS_Assets_Land_MP_RecordRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Land_MP_RecordRepository GetAS_Assets_Land_MP_RecordRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Land_MP_RecordRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Land_MP_RetireRepository GetAS_Assets_Land_MP_RetireRepository()
		{
			var repository = new AS_Assets_Land_MP_RetireRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Land_MP_RetireRepository GetAS_Assets_Land_MP_RetireRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Land_MP_RetireRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Land_RecordRepository GetAS_Assets_Land_RecordRepository()
		{
			var repository = new AS_Assets_Land_RecordRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Land_RecordRepository GetAS_Assets_Land_RecordRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Land_RecordRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Land_RetireRepository GetAS_Assets_Land_RetireRepository()
		{
			var repository = new AS_Assets_Land_RetireRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Land_RetireRepository GetAS_Assets_Land_RetireRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Land_RetireRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Land_SplitRepository GetAS_Assets_Land_SplitRepository()
		{
			var repository = new AS_Assets_Land_SplitRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Land_SplitRepository GetAS_Assets_Land_SplitRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Land_SplitRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_LessorRepository GetAS_Assets_LessorRepository()
		{
			var repository = new AS_Assets_LessorRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_LessorRepository GetAS_Assets_LessorRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_LessorRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_MPRepository GetAS_Assets_MPRepository()
		{
			var repository = new AS_Assets_MPRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_MPRepository GetAS_Assets_MPRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_MPRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_MP_DeprnRepository GetAS_Assets_MP_DeprnRepository()
		{
			var repository = new AS_Assets_MP_DeprnRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_MP_DeprnRepository GetAS_Assets_MP_DeprnRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_MP_DeprnRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_MP_HeaderRepository GetAS_Assets_MP_HeaderRepository()
		{
			var repository = new AS_Assets_MP_HeaderRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_MP_HeaderRepository GetAS_Assets_MP_HeaderRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_MP_HeaderRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_MP_HISRepository GetAS_Assets_MP_HISRepository()
		{
			var repository = new AS_Assets_MP_HISRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_MP_HISRepository GetAS_Assets_MP_HISRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_MP_HISRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_MP_ImportRepository GetAS_Assets_MP_ImportRepository()
		{
			var repository = new AS_Assets_MP_ImportRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_MP_ImportRepository GetAS_Assets_MP_ImportRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_MP_ImportRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_MP_ImportHISRepository GetAS_Assets_MP_ImportHISRepository()
		{
			var repository = new AS_Assets_MP_ImportHISRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_MP_ImportHISRepository GetAS_Assets_MP_ImportHISRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_MP_ImportHISRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_MP_ImportReviewRepository GetAS_Assets_MP_ImportReviewRepository()
		{
			var repository = new AS_Assets_MP_ImportReviewRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_MP_ImportReviewRepository GetAS_Assets_MP_ImportReviewRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_MP_ImportReviewRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_MP_RecordRepository GetAS_Assets_MP_RecordRepository()
		{
			var repository = new AS_Assets_MP_RecordRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_MP_RecordRepository GetAS_Assets_MP_RecordRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_MP_RecordRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_PlanningBranchRepository GetAS_Assets_PlanningBranchRepository()
		{
			var repository = new AS_Assets_PlanningBranchRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_PlanningBranchRepository GetAS_Assets_PlanningBranchRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_PlanningBranchRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_ReNew_HeaderRepository GetAS_Assets_ReNew_HeaderRepository()
		{
			var repository = new AS_Assets_ReNew_HeaderRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_ReNew_HeaderRepository GetAS_Assets_ReNew_HeaderRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_ReNew_HeaderRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_ReNew_RecordRepository GetAS_Assets_ReNew_RecordRepository()
		{
			var repository = new AS_Assets_ReNew_RecordRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_ReNew_RecordRepository GetAS_Assets_ReNew_RecordRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_ReNew_RecordRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_RightRepository GetAS_Assets_RightRepository()
		{
			var repository = new AS_Assets_RightRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_RightRepository GetAS_Assets_RightRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_RightRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Right_File_SystemRepository GetAS_Assets_Right_File_SystemRepository()
		{
			var repository = new AS_Assets_Right_File_SystemRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Right_File_SystemRepository GetAS_Assets_Right_File_SystemRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Right_File_SystemRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Right_HeadersRepository GetAS_Assets_Right_HeadersRepository()
		{
			var repository = new AS_Assets_Right_HeadersRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Right_HeadersRepository GetAS_Assets_Right_HeadersRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Right_HeadersRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Assets_Right_RecordRepository GetAS_Assets_Right_RecordRepository()
		{
			var repository = new AS_Assets_Right_RecordRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Assets_Right_RecordRepository GetAS_Assets_Right_RecordRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Assets_Right_RecordRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Branch_CurrencyRepository GetAS_Branch_CurrencyRepository()
		{
			var repository = new AS_Branch_CurrencyRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Branch_CurrencyRepository GetAS_Branch_CurrencyRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Branch_CurrencyRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Checked_SubSystemRepository GetAS_Checked_SubSystemRepository()
		{
			var repository = new AS_Checked_SubSystemRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Checked_SubSystemRepository GetAS_Checked_SubSystemRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Checked_SubSystemRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Code_TableRepository GetAS_Code_TableRepository()
		{
			var repository = new AS_Code_TableRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Code_TableRepository GetAS_Code_TableRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Code_TableRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_ConnectionRepository GetAS_ConnectionRepository()
		{
			var repository = new AS_ConnectionRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_ConnectionRepository GetAS_ConnectionRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_ConnectionRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_CurrencyRepository GetAS_CurrencyRepository()
		{
			var repository = new AS_CurrencyRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_CurrencyRepository GetAS_CurrencyRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_CurrencyRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_DepartmentRepository GetAS_DepartmentRepository()
		{
			var repository = new AS_DepartmentRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_DepartmentRepository GetAS_DepartmentRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_DepartmentRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_DeprnRepository GetAS_DeprnRepository()
		{
			var repository = new AS_DeprnRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_DeprnRepository GetAS_DeprnRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_DeprnRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Deprn_AccountRepository GetAS_Deprn_AccountRepository()
		{
			var repository = new AS_Deprn_AccountRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Deprn_AccountRepository GetAS_Deprn_AccountRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Deprn_AccountRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Deprn_ChargesRepository GetAS_Deprn_ChargesRepository()
		{
			var repository = new AS_Deprn_ChargesRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Deprn_ChargesRepository GetAS_Deprn_ChargesRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Deprn_ChargesRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Deprn_HisRepository GetAS_Deprn_HisRepository()
		{
			var repository = new AS_Deprn_HisRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Deprn_HisRepository GetAS_Deprn_HisRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Deprn_HisRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Doc_NameRepository GetAS_Doc_NameRepository()
		{
			var repository = new AS_Doc_NameRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Doc_NameRepository GetAS_Doc_NameRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Doc_NameRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_EntropyRepository GetAS_EntropyRepository()
		{
			var repository = new AS_EntropyRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_EntropyRepository GetAS_EntropyRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_EntropyRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_ExceptionRepository GetAS_ExceptionRepository()
		{
			var repository = new AS_ExceptionRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_ExceptionRepository GetAS_ExceptionRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_ExceptionRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_File_SystemRepository GetAS_File_SystemRepository()
		{
			var repository = new AS_File_SystemRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_File_SystemRepository GetAS_File_SystemRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_File_SystemRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Flow_ClosedRepository GetAS_Flow_ClosedRepository()
		{
			var repository = new AS_Flow_ClosedRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Flow_ClosedRepository GetAS_Flow_ClosedRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Flow_ClosedRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Flow_DeputyRepository GetAS_Flow_DeputyRepository()
		{
			var repository = new AS_Flow_DeputyRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Flow_DeputyRepository GetAS_Flow_DeputyRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Flow_DeputyRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Flow_FormInfoRepository GetAS_Flow_FormInfoRepository()
		{
			var repository = new AS_Flow_FormInfoRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Flow_FormInfoRepository GetAS_Flow_FormInfoRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Flow_FormInfoRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Flow_NameRepository GetAS_Flow_NameRepository()
		{
			var repository = new AS_Flow_NameRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Flow_NameRepository GetAS_Flow_NameRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Flow_NameRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Flow_OpenRepository GetAS_Flow_OpenRepository()
		{
			var repository = new AS_Flow_OpenRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Flow_OpenRepository GetAS_Flow_OpenRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Flow_OpenRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Flow_Open_LogRepository GetAS_Flow_Open_LogRepository()
		{
			var repository = new AS_Flow_Open_LogRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Flow_Open_LogRepository GetAS_Flow_Open_LogRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Flow_Open_LogRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Flow_RecordRepository GetAS_Flow_RecordRepository()
		{
			var repository = new AS_Flow_RecordRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Flow_RecordRepository GetAS_Flow_RecordRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Flow_RecordRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Flow_SerialRepository GetAS_Flow_SerialRepository()
		{
			var repository = new AS_Flow_SerialRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Flow_SerialRepository GetAS_Flow_SerialRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Flow_SerialRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Flow_TemplateRepository GetAS_Flow_TemplateRepository()
		{
			var repository = new AS_Flow_TemplateRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Flow_TemplateRepository GetAS_Flow_TemplateRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Flow_TemplateRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Flow_Template_Flow_RoleRepository GetAS_Flow_Template_Flow_RoleRepository()
		{
			var repository = new AS_Flow_Template_Flow_RoleRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Flow_Template_Flow_RoleRepository GetAS_Flow_Template_Flow_RoleRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Flow_Template_Flow_RoleRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Flow_UploadRepository GetAS_Flow_UploadRepository()
		{
			var repository = new AS_Flow_UploadRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Flow_UploadRepository GetAS_Flow_UploadRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Flow_UploadRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_GL_Acc_DeletedRepository GetAS_GL_Acc_DeletedRepository()
		{
			var repository = new AS_GL_Acc_DeletedRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_GL_Acc_DeletedRepository GetAS_GL_Acc_DeletedRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_GL_Acc_DeletedRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_GL_Acc_DeprnRepository GetAS_GL_Acc_DeprnRepository()
		{
			var repository = new AS_GL_Acc_DeprnRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_GL_Acc_DeprnRepository GetAS_GL_Acc_DeprnRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_GL_Acc_DeprnRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_GL_Acc_Deprn_HisRepository GetAS_GL_Acc_Deprn_HisRepository()
		{
			var repository = new AS_GL_Acc_Deprn_HisRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_GL_Acc_Deprn_HisRepository GetAS_GL_Acc_Deprn_HisRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_GL_Acc_Deprn_HisRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_GL_Acc_DetailRepository GetAS_GL_Acc_DetailRepository()
		{
			var repository = new AS_GL_Acc_DetailRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_GL_Acc_DetailRepository GetAS_GL_Acc_DetailRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_GL_Acc_DetailRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_GL_Acc_MonthsRepository GetAS_GL_Acc_MonthsRepository()
		{
			var repository = new AS_GL_Acc_MonthsRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_GL_Acc_MonthsRepository GetAS_GL_Acc_MonthsRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_GL_Acc_MonthsRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_GL_AccountRepository GetAS_GL_AccountRepository()
		{
			var repository = new AS_GL_AccountRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_GL_AccountRepository GetAS_GL_AccountRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_GL_AccountRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_GL_CalendarRepository GetAS_GL_CalendarRepository()
		{
			var repository = new AS_GL_CalendarRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_GL_CalendarRepository GetAS_GL_CalendarRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_GL_CalendarRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_GL_LedgerRepository GetAS_GL_LedgerRepository()
		{
			var repository = new AS_GL_LedgerRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_GL_LedgerRepository GetAS_GL_LedgerRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_GL_LedgerRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_GL_Trade_Acc_SubRepository GetAS_GL_Trade_Acc_SubRepository()
		{
			var repository = new AS_GL_Trade_Acc_SubRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_GL_Trade_Acc_SubRepository GetAS_GL_Trade_Acc_SubRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_GL_Trade_Acc_SubRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_GL_Trade_AccountRepository GetAS_GL_Trade_AccountRepository()
		{
			var repository = new AS_GL_Trade_AccountRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_GL_Trade_AccountRepository GetAS_GL_Trade_AccountRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_GL_Trade_AccountRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_GL_Trade_DefineRepository GetAS_GL_Trade_DefineRepository()
		{
			var repository = new AS_GL_Trade_DefineRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_GL_Trade_DefineRepository GetAS_GL_Trade_DefineRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_GL_Trade_DefineRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_GL_Trade_ItemRepository GetAS_GL_Trade_ItemRepository()
		{
			var repository = new AS_GL_Trade_ItemRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_GL_Trade_ItemRepository GetAS_GL_Trade_ItemRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_GL_Trade_ItemRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_History_ParameterRepository GetAS_History_ParameterRepository()
		{
			var repository = new AS_History_ParameterRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_History_ParameterRepository GetAS_History_ParameterRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_History_ParameterRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Impairment_GroupRepository GetAS_Impairment_GroupRepository()
		{
			var repository = new AS_Impairment_GroupRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Impairment_GroupRepository GetAS_Impairment_GroupRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Impairment_GroupRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Keep_PositionRepository GetAS_Keep_PositionRepository()
		{
			var repository = new AS_Keep_PositionRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Keep_PositionRepository GetAS_Keep_PositionRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Keep_PositionRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Land_CodeRepository GetAS_Land_CodeRepository()
		{
			var repository = new AS_Land_CodeRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Land_CodeRepository GetAS_Land_CodeRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Land_CodeRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Lease_InsuranceRepository GetAS_Lease_InsuranceRepository()
		{
			var repository = new AS_Lease_InsuranceRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Lease_InsuranceRepository GetAS_Lease_InsuranceRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Lease_InsuranceRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Lease_ManageExpenseRepository GetAS_Lease_ManageExpenseRepository()
		{
			var repository = new AS_Lease_ManageExpenseRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Lease_ManageExpenseRepository GetAS_Lease_ManageExpenseRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Lease_ManageExpenseRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Lease_MarginRepository GetAS_Lease_MarginRepository()
		{
			var repository = new AS_Lease_MarginRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Lease_MarginRepository GetAS_Lease_MarginRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Lease_MarginRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Lease_NoteRepository GetAS_Lease_NoteRepository()
		{
			var repository = new AS_Lease_NoteRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Lease_NoteRepository GetAS_Lease_NoteRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Lease_NoteRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Lease_Payment_AccountRepository GetAS_Lease_Payment_AccountRepository()
		{
			var repository = new AS_Lease_Payment_AccountRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Lease_Payment_AccountRepository GetAS_Lease_Payment_AccountRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Lease_Payment_AccountRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Lease_RentRepository GetAS_Lease_RentRepository()
		{
			var repository = new AS_Lease_RentRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Lease_RentRepository GetAS_Lease_RentRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Lease_RentRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_LeaseRightsRepository GetAS_LeaseRightsRepository()
		{
			var repository = new AS_LeaseRightsRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_LeaseRightsRepository GetAS_LeaseRightsRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_LeaseRightsRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_LeaseRights_DeprnRepository GetAS_LeaseRights_DeprnRepository()
		{
			var repository = new AS_LeaseRights_DeprnRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_LeaseRights_DeprnRepository GetAS_LeaseRights_DeprnRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_LeaseRights_DeprnRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_LeaseRights_HeaderRepository GetAS_LeaseRights_HeaderRepository()
		{
			var repository = new AS_LeaseRights_HeaderRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_LeaseRights_HeaderRepository GetAS_LeaseRights_HeaderRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_LeaseRights_HeaderRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_LeaseRights_RecordRepository GetAS_LeaseRights_RecordRepository()
		{
			var repository = new AS_LeaseRights_RecordRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_LeaseRights_RecordRepository GetAS_LeaseRights_RecordRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_LeaseRights_RecordRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Leases_ArableRepository GetAS_Leases_ArableRepository()
		{
			var repository = new AS_Leases_ArableRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Leases_ArableRepository GetAS_Leases_ArableRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Leases_ArableRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Leases_BuildRepository GetAS_Leases_BuildRepository()
		{
			var repository = new AS_Leases_BuildRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Leases_BuildRepository GetAS_Leases_BuildRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Leases_BuildRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Leases_Build_ParkingRepository GetAS_Leases_Build_ParkingRepository()
		{
			var repository = new AS_Leases_Build_ParkingRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Leases_Build_ParkingRepository GetAS_Leases_Build_ParkingRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Leases_Build_ParkingRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Leases_HeadersRepository GetAS_Leases_HeadersRepository()
		{
			var repository = new AS_Leases_HeadersRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Leases_HeadersRepository GetAS_Leases_HeadersRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Leases_HeadersRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Leases_Integration_HeaderRepository GetAS_Leases_Integration_HeaderRepository()
		{
			var repository = new AS_Leases_Integration_HeaderRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Leases_Integration_HeaderRepository GetAS_Leases_Integration_HeaderRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Leases_Integration_HeaderRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Leases_LandRepository GetAS_Leases_LandRepository()
		{
			var repository = new AS_Leases_LandRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Leases_LandRepository GetAS_Leases_LandRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Leases_LandRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Leases_OtherRepository GetAS_Leases_OtherRepository()
		{
			var repository = new AS_Leases_OtherRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Leases_OtherRepository GetAS_Leases_OtherRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Leases_OtherRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Leases_PictureRepository GetAS_Leases_PictureRepository()
		{
			var repository = new AS_Leases_PictureRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Leases_PictureRepository GetAS_Leases_PictureRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Leases_PictureRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Leases_Rent_To_BuildRepository GetAS_Leases_Rent_To_BuildRepository()
		{
			var repository = new AS_Leases_Rent_To_BuildRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Leases_Rent_To_BuildRepository GetAS_Leases_Rent_To_BuildRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Leases_Rent_To_BuildRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Leases_RentalRepository GetAS_Leases_RentalRepository()
		{
			var repository = new AS_Leases_RentalRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Leases_RentalRepository GetAS_Leases_RentalRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Leases_RentalRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Life_tableRepository GetAS_Life_tableRepository()
		{
			var repository = new AS_Life_tableRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Life_tableRepository GetAS_Life_tableRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Life_tableRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Log_DefineRepository GetAS_Log_DefineRepository()
		{
			var repository = new AS_Log_DefineRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Log_DefineRepository GetAS_Log_DefineRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Log_DefineRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Log_RecordRepository GetAS_Log_RecordRepository()
		{
			var repository = new AS_Log_RecordRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Log_RecordRepository GetAS_Log_RecordRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Log_RecordRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Mail_UserRepository GetAS_Mail_UserRepository()
		{
			var repository = new AS_Mail_UserRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Mail_UserRepository GetAS_Mail_UserRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Mail_UserRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_MenuRepository GetAS_MenuRepository()
		{
			var repository = new AS_MenuRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_MenuRepository GetAS_MenuRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_MenuRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Opration_HisRepository GetAS_Opration_HisRepository()
		{
			var repository = new AS_Opration_HisRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Opration_HisRepository GetAS_Opration_HisRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Opration_HisRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Post_HeaderRepository GetAS_Post_HeaderRepository()
		{
			var repository = new AS_Post_HeaderRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Post_HeaderRepository GetAS_Post_HeaderRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Post_HeaderRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Post_HisRepository GetAS_Post_HisRepository()
		{
			var repository = new AS_Post_HisRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Post_HisRepository GetAS_Post_HisRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Post_HisRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Prog_TemplateRepository GetAS_Prog_TemplateRepository()
		{
			var repository = new AS_Prog_TemplateRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Prog_TemplateRepository GetAS_Prog_TemplateRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Prog_TemplateRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_ProgramsRepository GetAS_ProgramsRepository()
		{
			var repository = new AS_ProgramsRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_ProgramsRepository GetAS_ProgramsRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_ProgramsRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Programs_RolesRepository GetAS_Programs_RolesRepository()
		{
			var repository = new AS_Programs_RolesRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Programs_RolesRepository GetAS_Programs_RolesRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Programs_RolesRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_RateRepository GetAS_RateRepository()
		{
			var repository = new AS_RateRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_RateRepository GetAS_RateRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_RateRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_RoleRepository GetAS_RoleRepository()
		{
			var repository = new AS_RoleRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_RoleRepository GetAS_RoleRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_RoleRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Role_FlowRepository GetAS_Role_FlowRepository()
		{
			var repository = new AS_Role_FlowRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Role_FlowRepository GetAS_Role_FlowRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Role_FlowRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_RoleFlow_HeaderRepository GetAS_RoleFlow_HeaderRepository()
		{
			var repository = new AS_RoleFlow_HeaderRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_RoleFlow_HeaderRepository GetAS_RoleFlow_HeaderRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_RoleFlow_HeaderRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_ToDoListRepository GetAS_ToDoListRepository()
		{
			var repository = new AS_ToDoListRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_ToDoListRepository GetAS_ToDoListRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_ToDoListRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_TRX_ContentsRepository GetAS_TRX_ContentsRepository()
		{
			var repository = new AS_TRX_ContentsRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_TRX_ContentsRepository GetAS_TRX_ContentsRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_TRX_ContentsRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_TRX_Contents_DetailRepository GetAS_TRX_Contents_DetailRepository()
		{
			var repository = new AS_TRX_Contents_DetailRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_TRX_Contents_DetailRepository GetAS_TRX_Contents_DetailRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_TRX_Contents_DetailRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_TRX_CostRepository GetAS_TRX_CostRepository()
		{
			var repository = new AS_TRX_CostRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_TRX_CostRepository GetAS_TRX_CostRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_TRX_CostRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_TRX_HeadersRepository GetAS_TRX_HeadersRepository()
		{
			var repository = new AS_TRX_HeadersRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_TRX_HeadersRepository GetAS_TRX_HeadersRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_TRX_HeadersRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Upload_DetailRepository GetAS_Upload_DetailRepository()
		{
			var repository = new AS_Upload_DetailRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Upload_DetailRepository GetAS_Upload_DetailRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Upload_DetailRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_UsersRepository GetAS_UsersRepository()
		{
			var repository = new AS_UsersRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_UsersRepository GetAS_UsersRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_UsersRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Users_DeputyRepository GetAS_Users_DeputyRepository()
		{
			var repository = new AS_Users_DeputyRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Users_DeputyRepository GetAS_Users_DeputyRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Users_DeputyRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Users_FlowRoleRepository GetAS_Users_FlowRoleRepository()
		{
			var repository = new AS_Users_FlowRoleRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Users_FlowRoleRepository GetAS_Users_FlowRoleRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Users_FlowRoleRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_Users_RoleRepository GetAS_Users_RoleRepository()
		{
			var repository = new AS_Users_RoleRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_Users_RoleRepository GetAS_Users_RoleRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_Users_RoleRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_VW_Acc_SettleRepository GetAS_VW_Acc_SettleRepository()
		{
			var repository = new AS_VW_Acc_SettleRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_VW_Acc_SettleRepository GetAS_VW_Acc_SettleRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_VW_Acc_SettleRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_VW_Asset_FamilyRepository GetAS_VW_Asset_FamilyRepository()
		{
			var repository = new AS_VW_Asset_FamilyRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_VW_Asset_FamilyRepository GetAS_VW_Asset_FamilyRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_VW_Asset_FamilyRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_VW_Code_CityRepository GetAS_VW_Code_CityRepository()
		{
			var repository = new AS_VW_Code_CityRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_VW_Code_CityRepository GetAS_VW_Code_CityRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_VW_Code_CityRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_VW_Code_DistrictRepository GetAS_VW_Code_DistrictRepository()
		{
			var repository = new AS_VW_Code_DistrictRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_VW_Code_DistrictRepository GetAS_VW_Code_DistrictRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_VW_Code_DistrictRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_VW_Code_TableRepository GetAS_VW_Code_TableRepository()
		{
			var repository = new AS_VW_Code_TableRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_VW_Code_TableRepository GetAS_VW_Code_TableRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_VW_Code_TableRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_VW_Department_ReportRepository GetAS_VW_Department_ReportRepository()
		{
			var repository = new AS_VW_Department_ReportRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_VW_Department_ReportRepository GetAS_VW_Department_ReportRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_VW_Department_ReportRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_VW_MPRepository GetAS_VW_MPRepository()
		{
			var repository = new AS_VW_MPRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_VW_MPRepository GetAS_VW_MPRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_VW_MPRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_VW_MP_ImportHisRepository GetAS_VW_MP_ImportHisRepository()
		{
			var repository = new AS_VW_MP_ImportHisRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_VW_MP_ImportHisRepository GetAS_VW_MP_ImportHisRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_VW_MP_ImportHisRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_VW_Post_ReportRepository GetAS_VW_Post_ReportRepository()
		{
			var repository = new AS_VW_Post_ReportRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_VW_Post_ReportRepository GetAS_VW_Post_ReportRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_VW_Post_ReportRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_VW_TodoList_ReportRepository GetAS_VW_TodoList_ReportRepository()
		{
			var repository = new AS_VW_TodoList_ReportRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_VW_TodoList_ReportRepository GetAS_VW_TodoList_ReportRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_VW_TodoList_ReportRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static AS_VW_UserAuthority_ReportRepository GetAS_VW_UserAuthority_ReportRepository()
		{
			var repository = new AS_VW_UserAuthority_ReportRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static AS_VW_UserAuthority_ReportRepository GetAS_VW_UserAuthority_ReportRepository(IUnitOfWork unitOfWork)
		{
			var repository = new AS_VW_UserAuthority_ReportRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static ContentFileRepository GetContentFileRepository()
		{
			var repository = new ContentFileRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static ContentFileRepository GetContentFileRepository(IUnitOfWork unitOfWork)
		{
			var repository = new ContentFileRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static FR_Accounting_ReportRepository GetFR_Accounting_ReportRepository()
		{
			var repository = new FR_Accounting_ReportRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static FR_Accounting_ReportRepository GetFR_Accounting_ReportRepository(IUnitOfWork unitOfWork)
		{
			var repository = new FR_Accounting_ReportRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static MPRepository GetMPRepository()
		{
			var repository = new MPRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static MPRepository GetMPRepository(IUnitOfWork unitOfWork)
		{
			var repository = new MPRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		
	}
}