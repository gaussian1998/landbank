using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Land_CodeRepository : EFRepository<AS_Land_Code>, IAS_Land_CodeRepository
	{

	}

	public  interface IAS_Land_CodeRepository : IRepository<AS_Land_Code>
	{

	}

   public  class AS_Land_Code_Records : GenericAccessUnitOfWork<AS_Land_Code>
	{

	}

}