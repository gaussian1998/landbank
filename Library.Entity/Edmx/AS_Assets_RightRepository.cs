using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_RightRepository : EFRepository<AS_Assets_Right>, IAS_Assets_RightRepository
	{

	}

	public  interface IAS_Assets_RightRepository : IRepository<AS_Assets_Right>
	{

	}

   public  class AS_Assets_Right_Records : GenericAccessUnitOfWork<AS_Assets_Right>
	{

	}

}