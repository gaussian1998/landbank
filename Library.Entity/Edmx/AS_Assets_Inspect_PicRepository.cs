using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Inspect_PicRepository : EFRepository<AS_Assets_Inspect_Pic>, IAS_Assets_Inspect_PicRepository
	{

	}

	public  interface IAS_Assets_Inspect_PicRepository : IRepository<AS_Assets_Inspect_Pic>
	{

	}

   public  class AS_Assets_Inspect_Pic_Records : GenericAccessUnitOfWork<AS_Assets_Inspect_Pic>
	{

	}

}