using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Deprn_AccountRepository : EFRepository<AS_Deprn_Account>, IAS_Deprn_AccountRepository
	{

	}

	public  interface IAS_Deprn_AccountRepository : IRepository<AS_Deprn_Account>
	{

	}

   public  class AS_Deprn_Account_Records : GenericAccessUnitOfWork<AS_Deprn_Account>
	{

	}

}