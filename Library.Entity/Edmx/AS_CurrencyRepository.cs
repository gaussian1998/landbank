using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_CurrencyRepository : EFRepository<AS_Currency>, IAS_CurrencyRepository
	{

	}

	public  interface IAS_CurrencyRepository : IRepository<AS_Currency>
	{

	}

   public  class AS_Currency_Records : GenericAccessUnitOfWork<AS_Currency>
	{

	}

}