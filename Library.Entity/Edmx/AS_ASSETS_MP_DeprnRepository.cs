using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_MP_DeprnRepository : EFRepository<AS_Assets_MP_Deprn>, IAS_Assets_MP_DeprnRepository
	{

	}

	public  interface IAS_Assets_MP_DeprnRepository : IRepository<AS_Assets_MP_Deprn>
	{

	}

   public  class AS_Assets_MP_Deprn_Records : GenericAccessUnitOfWork<AS_Assets_MP_Deprn>
	{

	}

}