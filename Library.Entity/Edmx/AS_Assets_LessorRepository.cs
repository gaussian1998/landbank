using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_LessorRepository : EFRepository<AS_Assets_Lessor>, IAS_Assets_LessorRepository
	{

	}

	public  interface IAS_Assets_LessorRepository : IRepository<AS_Assets_Lessor>
	{

	}

   public  class AS_Assets_Lessor_Records : GenericAccessUnitOfWork<AS_Assets_Lessor>
	{

	}

}