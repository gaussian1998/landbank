using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_LeaseRightsRepository : EFRepository<AS_LeaseRights>, IAS_LeaseRightsRepository
	{

	}

	public  interface IAS_LeaseRightsRepository : IRepository<AS_LeaseRights>
	{

	}

   public  class AS_LeaseRights_Records : GenericAccessUnitOfWork<AS_LeaseRights>
	{

	}

}