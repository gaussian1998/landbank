using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Activation_PB2UserRepository : EFRepository<AS_Assets_Activation_PB2User>, IAS_Assets_Activation_PB2UserRepository
	{

	}

	public  interface IAS_Assets_Activation_PB2UserRepository : IRepository<AS_Assets_Activation_PB2User>
	{

	}

   public  class AS_Assets_Activation_PB2User_Records : GenericAccessUnitOfWork<AS_Assets_Activation_PB2User>
	{

	}

}