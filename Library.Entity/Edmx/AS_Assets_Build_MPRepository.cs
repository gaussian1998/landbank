using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Build_MPRepository : EFRepository<AS_Assets_Build_MP>, IAS_Assets_Build_MPRepository
	{

	}

	public  interface IAS_Assets_Build_MPRepository : IRepository<AS_Assets_Build_MP>
	{

	}

   public  class AS_Assets_Build_MP_Records : GenericAccessUnitOfWork<AS_Assets_Build_MP>
	{

	}

}