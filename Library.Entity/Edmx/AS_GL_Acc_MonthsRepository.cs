using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_GL_Acc_MonthsRepository : EFRepository<AS_GL_Acc_Months>, IAS_GL_Acc_MonthsRepository
	{

	}

	public  interface IAS_GL_Acc_MonthsRepository : IRepository<AS_GL_Acc_Months>
	{

	}

   public  class AS_GL_Acc_Months_Records : GenericAccessUnitOfWork<AS_GL_Acc_Months>
	{

	}

}