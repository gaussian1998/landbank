using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_MP_HISRepository : EFRepository<AS_Assets_MP_HIS>, IAS_Assets_MP_HISRepository
	{

	}

	public  interface IAS_Assets_MP_HISRepository : IRepository<AS_Assets_MP_HIS>
	{

	}

   public  class AS_Assets_MP_HIS_Records : GenericAccessUnitOfWork<AS_Assets_MP_HIS>
	{

	}

}