using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_VW_Asset_FamilyRepository : EFRepository<AS_VW_Asset_Family>, IAS_VW_Asset_FamilyRepository
	{

	}

	public  interface IAS_VW_Asset_FamilyRepository : IRepository<AS_VW_Asset_Family>
	{

	}

   public  class AS_VW_Asset_Family_Records : GenericAccessUnitOfWork<AS_VW_Asset_Family>
	{

	}

}