using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Lease_RentRepository : EFRepository<AS_Lease_Rent>, IAS_Lease_RentRepository
	{

	}

	public  interface IAS_Lease_RentRepository : IRepository<AS_Lease_Rent>
	{

	}

   public  class AS_Lease_Rent_Records : GenericAccessUnitOfWork<AS_Lease_Rent>
	{

	}

}