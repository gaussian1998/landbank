using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Build_HeaderRepository : EFRepository<AS_Assets_Build_Header>, IAS_Assets_Build_HeaderRepository
	{

	}

	public  interface IAS_Assets_Build_HeaderRepository : IRepository<AS_Assets_Build_Header>
	{

	}

   public  class AS_Assets_Build_Header_Records : GenericAccessUnitOfWork<AS_Assets_Build_Header>
	{

	}

}