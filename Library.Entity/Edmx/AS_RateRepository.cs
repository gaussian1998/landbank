using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_RateRepository : EFRepository<AS_Rate>, IAS_RateRepository
	{

	}

	public  interface IAS_RateRepository : IRepository<AS_Rate>
	{

	}

   public  class AS_Rate_Records : GenericAccessUnitOfWork<AS_Rate>
	{

	}

}