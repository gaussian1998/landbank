using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Activation_LBDetailRepository : EFRepository<AS_Assets_Activation_LBDetail>, IAS_Assets_Activation_LBDetailRepository
	{

	}

	public  interface IAS_Assets_Activation_LBDetailRepository : IRepository<AS_Assets_Activation_LBDetail>
	{

	}

   public  class AS_Assets_Activation_LBDetail_Records : GenericAccessUnitOfWork<AS_Assets_Activation_LBDetail>
	{

	}

}