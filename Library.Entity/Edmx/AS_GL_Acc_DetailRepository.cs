using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_GL_Acc_DetailRepository : EFRepository<AS_GL_Acc_Detail>, IAS_GL_Acc_DetailRepository
	{

	}

	public  interface IAS_GL_Acc_DetailRepository : IRepository<AS_GL_Acc_Detail>
	{

	}

   public  class AS_GL_Acc_Detail_Records : GenericAccessUnitOfWork<AS_GL_Acc_Detail>
	{

	}

}