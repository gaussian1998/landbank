using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_GL_Trade_DefineRepository : EFRepository<AS_GL_Trade_Define>, IAS_GL_Trade_DefineRepository
	{

	}

	public  interface IAS_GL_Trade_DefineRepository : IRepository<AS_GL_Trade_Define>
	{

	}

   public  class AS_GL_Trade_Define_Records : GenericAccessUnitOfWork<AS_GL_Trade_Define>
	{

	}

}