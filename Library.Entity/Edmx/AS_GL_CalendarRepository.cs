using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_GL_CalendarRepository : EFRepository<AS_GL_Calendar>, IAS_GL_CalendarRepository
	{

	}

	public  interface IAS_GL_CalendarRepository : IRepository<AS_GL_Calendar>
	{

	}

   public  class AS_GL_Calendar_Records : GenericAccessUnitOfWork<AS_GL_Calendar>
	{

	}

}