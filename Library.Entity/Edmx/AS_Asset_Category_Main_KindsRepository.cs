using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Asset_Category_Main_KindsRepository : EFRepository<AS_Asset_Category_Main_Kinds>, IAS_Asset_Category_Main_KindsRepository
	{

	}

	public  interface IAS_Asset_Category_Main_KindsRepository : IRepository<AS_Asset_Category_Main_Kinds>
	{

	}

   public  class AS_Asset_Category_Main_Kinds_Records : GenericAccessUnitOfWork<AS_Asset_Category_Main_Kinds>
	{

	}

}