using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_DeprnRepository : EFRepository<AS_Deprn>, IAS_DeprnRepository
	{

	}

	public  interface IAS_DeprnRepository : IRepository<AS_Deprn>
	{

	}

   public  class AS_Deprn_Records : GenericAccessUnitOfWork<AS_Deprn>
	{

	}

}