using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Asset_FamilyRepository : EFRepository<AS_Asset_Family>, IAS_Asset_FamilyRepository
	{

	}

	public  interface IAS_Asset_FamilyRepository : IRepository<AS_Asset_Family>
	{

	}

   public  class AS_Asset_Family_Records : GenericAccessUnitOfWork<AS_Asset_Family>
	{

	}

}