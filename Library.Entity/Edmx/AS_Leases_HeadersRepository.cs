using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Leases_HeadersRepository : EFRepository<AS_Leases_Headers>, IAS_Leases_HeadersRepository
	{

	}

	public  interface IAS_Leases_HeadersRepository : IRepository<AS_Leases_Headers>
	{

	}

   public  class AS_Leases_Headers_Records : GenericAccessUnitOfWork<AS_Leases_Headers>
	{

	}

}