using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Land_SplitRepository : EFRepository<AS_Assets_Land_Split>, IAS_Assets_Land_SplitRepository
	{

	}

	public  interface IAS_Assets_Land_SplitRepository : IRepository<AS_Assets_Land_Split>
	{

	}

   public  class AS_Assets_Land_Split_Records : GenericAccessUnitOfWork<AS_Assets_Land_Split>
	{

	}

}