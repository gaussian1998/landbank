using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_DepartmentRepository : EFRepository<AS_Department>, IAS_DepartmentRepository
	{

	}

	public  interface IAS_DepartmentRepository : IRepository<AS_Department>
	{

	}

   public  class AS_Department_Records : GenericAccessUnitOfWork<AS_Department>
	{

	}

}