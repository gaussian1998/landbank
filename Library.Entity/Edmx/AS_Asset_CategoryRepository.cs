using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Asset_CategoryRepository : EFRepository<AS_Asset_Category>, IAS_Asset_CategoryRepository
	{

	}

	public  interface IAS_Asset_CategoryRepository : IRepository<AS_Asset_Category>
	{

	}

   public  class AS_Asset_Category_Records : GenericAccessUnitOfWork<AS_Asset_Category>
	{

	}

}