using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Flow_FormInfoRepository : EFRepository<AS_Flow_FormInfo>, IAS_Flow_FormInfoRepository
	{

	}

	public  interface IAS_Flow_FormInfoRepository : IRepository<AS_Flow_FormInfo>
	{

	}

   public  class AS_Flow_FormInfo_Records : GenericAccessUnitOfWork<AS_Flow_FormInfo>
	{

	}

}