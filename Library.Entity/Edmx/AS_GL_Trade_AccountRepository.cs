using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_GL_Trade_AccountRepository : EFRepository<AS_GL_Trade_Account>, IAS_GL_Trade_AccountRepository
	{

	}

	public  interface IAS_GL_Trade_AccountRepository : IRepository<AS_GL_Trade_Account>
	{

	}

   public  class AS_GL_Trade_Account_Records : GenericAccessUnitOfWork<AS_GL_Trade_Account>
	{

	}

}