using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_VW_Code_DistrictRepository : EFRepository<AS_VW_Code_District>, IAS_VW_Code_DistrictRepository
	{

	}

	public  interface IAS_VW_Code_DistrictRepository : IRepository<AS_VW_Code_District>
	{

	}

   public  class AS_VW_Code_District_Records : GenericAccessUnitOfWork<AS_VW_Code_District>
	{

	}

}