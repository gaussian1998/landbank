using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_VW_MPRepository : EFRepository<AS_VW_MP>, IAS_VW_MPRepository
	{

	}

	public  interface IAS_VW_MPRepository : IRepository<AS_VW_MP>
	{

	}

   public  class AS_VW_MP_Records : GenericAccessUnitOfWork<AS_VW_MP>
	{

	}

}