using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_VW_Post_ReportRepository : EFRepository<AS_VW_Post_Report>, IAS_VW_Post_ReportRepository
	{

	}

	public  interface IAS_VW_Post_ReportRepository : IRepository<AS_VW_Post_Report>
	{

	}

   public  class AS_VW_Post_Report_Records : GenericAccessUnitOfWork<AS_VW_Post_Report>
	{

	}

}