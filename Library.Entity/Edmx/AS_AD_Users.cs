//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Library.Entity.Edmx
{
    using System;
    using System.Collections.Generic;
    
    public partial class AS_AD_Users
    {
        public int ID { get; set; }
        public decimal USER_ID { get; set; }
        public string USER_NAME { get; set; }
        public Nullable<System.DateTime> LAST_UPDATE_DATE { get; set; }
        public Nullable<decimal> LAST_UPDATED_BY { get; set; }
        public Nullable<System.DateTime> CREATION_DATE { get; set; }
        public Nullable<decimal> CREATED_BY { get; set; }
        public Nullable<decimal> LAST_UPDATE_LOGIN { get; set; }
        public string ENCRYPTED_FOUNDATION_PASSWORD { get; set; }
        public string ENCRYPTED_USER_PASSWORD { get; set; }
        public Nullable<decimal> SESSION_NUMBER { get; set; }
        public Nullable<System.DateTime> START_DATE { get; set; }
        public Nullable<System.DateTime> END_DATE { get; set; }
        public string DESCRIPTION { get; set; }
        public Nullable<System.DateTime> LAST_LOGON_DATE { get; set; }
        public Nullable<System.DateTime> PASSWORD_DATE { get; set; }
        public Nullable<decimal> PASSWORD_ACCESSES_LEFT { get; set; }
        public Nullable<decimal> PASSWORD_LIFESPAN_ACCESSES { get; set; }
        public Nullable<decimal> PASSWORD_LIFESPAN_DAYS { get; set; }
        public Nullable<decimal> EMPLOYEE_ID { get; set; }
        public string EMAIL_ADDRESS { get; set; }
        public string FAX { get; set; }
        public Nullable<decimal> CUSTOMER_ID { get; set; }
        public Nullable<decimal> SUPPLIER_ID { get; set; }
        public string WEB_PASSWORD { get; set; }
        public string USER_GUID { get; set; }
        public Nullable<decimal> GCN_CODE_COMBINATION_ID { get; set; }
        public string STATUS { get; set; }
        public string UPDATE_FLAG { get; set; }
        public Nullable<decimal> PERSON_PARTY_ID { get; set; }
        public string EXPLOYEE_NAME { get; set; }
    }
}
