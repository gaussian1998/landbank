using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Evaluation_DetailRepository : EFRepository<AS_Assets_Evaluation_Detail>, IAS_Assets_Evaluation_DetailRepository
	{

	}

	public  interface IAS_Assets_Evaluation_DetailRepository : IRepository<AS_Assets_Evaluation_Detail>
	{

	}

   public  class AS_Assets_Evaluation_Detail_Records : GenericAccessUnitOfWork<AS_Assets_Evaluation_Detail>
	{

	}

}