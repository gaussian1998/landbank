using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_VW_Code_TableRepository : EFRepository<AS_VW_Code_Table>, IAS_VW_Code_TableRepository
	{

	}

	public  interface IAS_VW_Code_TableRepository : IRepository<AS_VW_Code_Table>
	{

	}

   public  class AS_VW_Code_Table_Records : GenericAccessUnitOfWork<AS_VW_Code_Table>
	{

	}

}