using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Role_FlowRepository : EFRepository<AS_Role_Flow>, IAS_Role_FlowRepository
	{

	}

	public  interface IAS_Role_FlowRepository : IRepository<AS_Role_Flow>
	{

	}

   public  class AS_Role_Flow_Records : GenericAccessUnitOfWork<AS_Role_Flow>
	{

	}

}