using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_MenuRepository : EFRepository<AS_Menu>, IAS_MenuRepository
	{

	}

	public  interface IAS_MenuRepository : IRepository<AS_Menu>
	{

	}

   public  class AS_Menu_Records : GenericAccessUnitOfWork<AS_Menu>
	{

	}

}