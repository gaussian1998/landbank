using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_ActivationTargetRepository : EFRepository<AS_Assets_ActivationTarget>, IAS_Assets_ActivationTargetRepository
	{

	}

	public  interface IAS_Assets_ActivationTargetRepository : IRepository<AS_Assets_ActivationTarget>
	{

	}

   public  class AS_Assets_ActivationTarget_Records : GenericAccessUnitOfWork<AS_Assets_ActivationTarget>
	{

	}

}