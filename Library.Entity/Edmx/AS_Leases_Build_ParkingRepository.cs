using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Leases_Build_ParkingRepository : EFRepository<AS_Leases_Build_Parking>, IAS_Leases_Build_ParkingRepository
	{

	}

	public  interface IAS_Leases_Build_ParkingRepository : IRepository<AS_Leases_Build_Parking>
	{

	}

   public  class AS_Leases_Build_Parking_Records : GenericAccessUnitOfWork<AS_Leases_Build_Parking>
	{

	}

}