using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_EntropyRepository : EFRepository<AS_Entropy>, IAS_EntropyRepository
	{

	}

	public  interface IAS_EntropyRepository : IRepository<AS_Entropy>
	{

	}

   public  class AS_Entropy_Records : GenericAccessUnitOfWork<AS_Entropy>
	{

	}

}