using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Flow_SerialRepository : EFRepository<AS_Flow_Serial>, IAS_Flow_SerialRepository
	{

	}

	public  interface IAS_Flow_SerialRepository : IRepository<AS_Flow_Serial>
	{

	}

   public  class AS_Flow_Serial_Records : GenericAccessUnitOfWork<AS_Flow_Serial>
	{

	}

}