using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_GL_Trade_ItemRepository : EFRepository<AS_GL_Trade_Item>, IAS_GL_Trade_ItemRepository
	{

	}

	public  interface IAS_GL_Trade_ItemRepository : IRepository<AS_GL_Trade_Item>
	{

	}

   public  class AS_GL_Trade_Item_Records : GenericAccessUnitOfWork<AS_GL_Trade_Item>
	{

	}

}