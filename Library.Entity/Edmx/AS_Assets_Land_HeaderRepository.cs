using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Land_HeaderRepository : EFRepository<AS_Assets_Land_Header>, IAS_Assets_Land_HeaderRepository
	{

	}

	public  interface IAS_Assets_Land_HeaderRepository : IRepository<AS_Assets_Land_Header>
	{

	}

   public  class AS_Assets_Land_Header_Records : GenericAccessUnitOfWork<AS_Assets_Land_Header>
	{

	}

}