using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_MP_HeaderRepository : EFRepository<AS_Assets_MP_Header>, IAS_Assets_MP_HeaderRepository
	{

	}

	public  interface IAS_Assets_MP_HeaderRepository : IRepository<AS_Assets_MP_Header>
	{

	}

   public  class AS_Assets_MP_Header_Records : GenericAccessUnitOfWork<AS_Assets_MP_Header>
	{

	}

}