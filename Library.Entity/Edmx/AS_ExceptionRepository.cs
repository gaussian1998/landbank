using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_ExceptionRepository : EFRepository<AS_Exception>, IAS_ExceptionRepository
	{

	}

	public  interface IAS_ExceptionRepository : IRepository<AS_Exception>
	{

	}

   public  class AS_Exception_Records : GenericAccessUnitOfWork<AS_Exception>
	{

	}

}