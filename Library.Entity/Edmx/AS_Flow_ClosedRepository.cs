using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Flow_ClosedRepository : EFRepository<AS_Flow_Closed>, IAS_Flow_ClosedRepository
	{

	}

	public  interface IAS_Flow_ClosedRepository : IRepository<AS_Flow_Closed>
	{

	}

   public  class AS_Flow_Closed_Records : GenericAccessUnitOfWork<AS_Flow_Closed>
	{

	}

}