using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Build_RecordRepository : EFRepository<AS_Assets_Build_Record>, IAS_Assets_Build_RecordRepository
	{

	}

	public  interface IAS_Assets_Build_RecordRepository : IRepository<AS_Assets_Build_Record>
	{

	}

   public  class AS_Assets_Build_Record_Records : GenericAccessUnitOfWork<AS_Assets_Build_Record>
	{

	}

}