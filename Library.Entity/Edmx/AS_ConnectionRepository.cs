using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_ConnectionRepository : EFRepository<AS_Connection>, IAS_ConnectionRepository
	{

	}

	public  interface IAS_ConnectionRepository : IRepository<AS_Connection>
	{

	}

   public  class AS_Connection_Records : GenericAccessUnitOfWork<AS_Connection>
	{

	}

}