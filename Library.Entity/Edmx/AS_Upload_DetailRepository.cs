using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Upload_DetailRepository : EFRepository<AS_Upload_Detail>, IAS_Upload_DetailRepository
	{

	}

	public  interface IAS_Upload_DetailRepository : IRepository<AS_Upload_Detail>
	{

	}

   public  class AS_Upload_Detail_Records : GenericAccessUnitOfWork<AS_Upload_Detail>
	{

	}

}