using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_GL_LedgerRepository : EFRepository<AS_GL_Ledger>, IAS_GL_LedgerRepository
	{

	}

	public  interface IAS_GL_LedgerRepository : IRepository<AS_GL_Ledger>
	{

	}

   public  class AS_GL_Ledger_Records : GenericAccessUnitOfWork<AS_GL_Ledger>
	{

	}

}