using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Land_ChangeRepository : EFRepository<AS_Assets_Land_Change>, IAS_Assets_Land_ChangeRepository
	{

	}

	public  interface IAS_Assets_Land_ChangeRepository : IRepository<AS_Assets_Land_Change>
	{

	}

   public  class AS_Assets_Land_Change_Records : GenericAccessUnitOfWork<AS_Assets_Land_Change>
	{

	}

}