using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class Acc_EntriesByHeaderRepository : EFRepository<Acc_EntriesByHeader>, IAcc_EntriesByHeaderRepository
	{

	}

	public  interface IAcc_EntriesByHeaderRepository : IRepository<Acc_EntriesByHeader>
	{

	}

   public  class Acc_EntriesByHeader_Records : GenericAccessUnitOfWork<Acc_EntriesByHeader>
	{

	}

}