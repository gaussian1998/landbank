using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Leases_BuildRepository : EFRepository<AS_Leases_Build>, IAS_Leases_BuildRepository
	{

	}

	public  interface IAS_Leases_BuildRepository : IRepository<AS_Leases_Build>
	{

	}

   public  class AS_Leases_Build_Records : GenericAccessUnitOfWork<AS_Leases_Build>
	{

	}

}