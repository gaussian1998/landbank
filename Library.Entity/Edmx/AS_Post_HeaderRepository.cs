using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Post_HeaderRepository : EFRepository<AS_Post_Header>, IAS_Post_HeaderRepository
	{

	}

	public  interface IAS_Post_HeaderRepository : IRepository<AS_Post_Header>
	{

	}

   public  class AS_Post_Header_Records : GenericAccessUnitOfWork<AS_Post_Header>
	{

	}

}