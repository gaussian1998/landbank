using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_VW_MP_ImportHisRepository : EFRepository<AS_VW_MP_ImportHis>, IAS_VW_MP_ImportHisRepository
	{

	}

	public  interface IAS_VW_MP_ImportHisRepository : IRepository<AS_VW_MP_ImportHis>
	{

	}

   public  class AS_VW_MP_ImportHis_Records : GenericAccessUnitOfWork<AS_VW_MP_ImportHis>
	{

	}

}