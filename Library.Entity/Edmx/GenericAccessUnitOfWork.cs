﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Utility;
using Library.Common.Models;

namespace Library.Entity.Edmx
{
    /// <summary>
    ///  EFUnitOfWork 會被自動釋放,所以它是可以任意傳到其他模組的資源
    ///  此回答來自微軟EF工作小組
    /// </summary>
    public  class GenericAccessUnitOfWork<Entity>
        where Entity : class, new()
    {
        public static bool Any(Expression<Func<Entity, bool>> predicate)
        {
            var repo = GetRepository();

            return repo.All().Any(predicate);
        }

        public static int Count(Expression<Func<Entity, bool>> predicate)
        {
            var repo = GetRepository();

            return repo.Where(predicate).Count();
        }

        /// <summary>
        ///  T不能為Entity,否則會有極大效能問題
        /// </summary>
        public static List<T> Find<T>(Expression<Func<Entity, bool>> predicate)
            where T : new()
        {
            return Find(predicate).Select( entity => MapProperty.Mapping<T, Entity>(entity) ).ToList();
        }

        public static List<Entity> Find(Expression<Func<Entity, bool>> predicate)
        {
            var repo = GetRepository();
            return repo.Where(predicate).ToList();
        }

        public static List<T> Find<T>(Expression<Func<Entity, bool>> predicate, Expression<Func<Entity, T>> select)
            where T : new()
        {
            var repo = GetRepository();
            return repo.Where(predicate).Select(select).ToList();
        }

        public static List<T> FindModify<T>(Expression<Func<Entity, bool>> predicate, Func<T, T> modify)
            where T : new()
        {
            var repo = GetRepository();

            return (from entity in repo.Where(predicate).ToList()
                    select modify( MapProperty.Mapping<T, Entity>(entity)) ).ToList();
        }

        public static List<T> FindConvert<T>(Expression<Func<Entity, bool>> predicate, Func<Entity, T> convert)
            where T : new()
        {
            var repo = GetRepository();

            return (from entity in repo.Where(predicate).ToList()
                    select convert(entity)).ToList();
        }

        public static List<T> FindAll<T>()
            where T : new()
        {
            return Find<T>(m => true);
        }

        public static List<Entity> FindAll()
        {
            return Find(m => true);
        }

        public static List<T> FindAll<T>(Expression<Func<Entity, T>> select)
        {
            var repo = GetRepository();
            return repo.Where(m => true).Select(select).ToList();
        }

        /// <summary>
        ///  傳回分頁,若 T為Entity會有嚴重效能問題,須改用下面函式
        /// </summary>
        public static PageList<T> Page<T, Key>(int Page, int PageSize, Expression<Func<Entity, Key>> order, Expression<Func<Entity, bool>> predicate)
             where T : new()
        {
            var repo = GetRepository();

            int real_page = (Page - 1) >= 0 ? (Page - 1) : 0;
            var query = repo.Where(predicate);
            return new PageList<T>
            {
                TotalAmount = query.Count(),
                Items = (from entity in query.OrderBy(order).Skip(real_page * PageSize).Take(PageSize).ToList()
                         select MapProperty.Mapping<T, Entity>(entity)).ToList()
            };
        }

        public static PageList<Entity> Page<Key>(int Page, int PageSize, Expression<Func<Entity, Key>> order, Expression<Func<Entity, bool>> predicate)
        {
            var repo = GetRepository();

            int real_page = (Page - 1) >= 0 ? (Page - 1) : 0;
            var query = repo.Where(predicate);
            return new PageList<Entity>
            {
                TotalAmount = query.Count(),
                Items = query.OrderBy(order).Skip(real_page * PageSize).Take(PageSize).ToList()
            };
        }

        /// <summary>
        ///  傳回分頁
        /// </summary>
        public static PageList<T> Page<T, Key>(int page, int PageSize, Expression<Func<Entity, Key>> order, Expression<Func<Entity, bool>> predicate, Expression<Func<Entity, T>> select)
             where T : new()
        {
            var repo = GetRepository();

            int real_page = (page - 1) >= 0 ? (page - 1) : 0;
            var query = repo.Where(predicate);
            return new PageList<T>
            {
                TotalAmount = query.Count(),
                Items = query.OrderBy(order).Skip(real_page * PageSize).Take(PageSize).Select(select).ToList()
            };
        }

        public static Entity First(Expression<Func<Entity, bool>> predicate)
        {
            var repo = GetRepository();
            var query = repo.Where(predicate);
            if (query.Any())
                return query.First();

            throw new Exception("First: Can not Find Any Entity");
        }

        public static T First<T>(Expression<Func<Entity, bool>> predicate)
         where T : new()
        {
            var repo = GetRepository();
            var query = repo.Where(predicate);
            if (query.Any())
            {
                var entity = query.First();
                return MapProperty.Mapping<T, Entity>(entity);
            }

            throw new Exception("First<T>: Can not Find Any Entity");
        }

        public static T FirstModify<T>(Expression<Func<Entity, bool>> predicate, Func<T, T> modify)
            where T : new()
        {
            var repo = GetRepository();
            var query = repo.Where(predicate);
            if (query.Any())
            {
                var entity = query.First();
                return modify( MapProperty.Mapping<T, Entity>(entity) );
            }

            throw new Exception("FirstModify<T>: Can not Find Any Entity");
        }

        public static T First<T>(Expression<Func<Entity, bool>> predicate, Expression<Func<Entity, T>> select)
        {
            var repo = GetRepository();
            var query = repo.Where(predicate).Select(select);
            if (query.Any())
                return query.First();

            throw new Exception("First<T>: Can not Find Any Entity");
        }

        public static TResult Max<TResult>(Expression<Func<Entity, TResult>> selector)
        {
            var repo = GetRepository();

            if (repo.All().Any())
                return repo.All().Max(selector);
            else
                return default(TResult);
        }

        public static TResult Max<TResult>(Expression<Func<Entity, bool>> predicate, Expression<Func<Entity, TResult>> selector)
        {
            var repo = GetRepository();
            var query = repo.Where(predicate);

            if (query.Any())
                return query.Max(selector);
            else
                return default(TResult);
        }

        public static Dictionary<Key, List<Entity>> Group<Key>(Expression<Func<Entity, Key>> keySelector)
        {
            var repo = GetRepository();

            Dictionary<Key, List<Entity>> result = new Dictionary<Key, List<Entity>>();
            var query = repo.All().GroupBy(keySelector);
            foreach (var group in query)
                result[group.Key] = group.ToList();

            return result;
        }

        public static Dictionary<Key, List<T>> Group<Key, T>(Expression<Func<Entity, Key>> keySelector, Expression<Func<Entity, T>> selector)
        {
            var repo = GetRepository();

            Dictionary<Key, List<T>> result = new Dictionary<Key, List<T>>();
            var query = repo.All().GroupBy(keySelector, selector);
            foreach (var group in query)
                result[group.Key] = group.ToList();

            return result;
        }

        public static Dictionary<Key, List<T>> Group<Key, T>(Expression<Func<Entity, bool>> predicate , Expression<Func<Entity, Key>> keySelector, Expression<Func<Entity, T>> selector)
        {
            var repo = GetRepository();

            Dictionary<Key, List<T>> result = new Dictionary<Key, List<T>>();
            var query = repo.All().Where(predicate).GroupBy(keySelector, selector);
            foreach (var group in query)
                result[group.Key] = group.ToList();

            return result;
        }

        public static List<T> GroupList<Key,T>(Expression<Func<Entity, bool>> predicate, Expression<Func<Entity, Key>> keySelector, Expression<Func<IGrouping<Key,Entity>, T>> selector)
        {
            var repo = GetRepository();

            var query = repo.All().Where(predicate).GroupBy(keySelector);
            return query.Select(selector).ToList();
        }

        public static PageList<T> GroupList<Key, T>(int Page, int PageSize, Expression<Func<Entity, bool>> predicate, Expression<Func<Entity, Key>> keySelector, Expression<Func<IGrouping<Key, Entity>, T>> selector)
        {
            var repo = GetRepository();

            int real_page = (Page - 1) >= 0 ? (Page - 1) : 0;
            var query = repo.All().Where(predicate).GroupBy(keySelector);
            return new PageList<T>
            {
                TotalAmount = query.Count(),
                Items = query.OrderBy( g => g.Count() ).Skip(real_page * PageSize).Take(PageSize).Select(selector).ToList()
            };
        }


        public static Entity Create(Entity entity)
        {
            using (ModifyContext ef = new ModifyContext())
            {
                var repo = GetRepository(ef);
                return repo.Add(entity);
            }
        }

        public static Entity Create<T>(T vo)
        {
            return Create(vo, entity => { });
        }

        public static Entity Create<T>(T vo, Action<Entity> beforeAdd)
        {
            var entity = MapProperty.Mapping<Entity, T>(vo);
            beforeAdd(entity);

            using (ModifyContext ef = new ModifyContext())
            {
                var repo = GetRepository(ef);
                return repo.Add(entity);
            }
        }

        public static Entity Create(Action<Entity> modify)
        {
            var entity = new Entity();
            modify(entity);

            using (ModifyContext ef = new ModifyContext())
            {
                var repo = GetRepository(ef);
                return repo.Add(entity);
            }
        }

        public static Entity CreateNotExist(Expression<Func<Entity, bool>> predicate, Action<Entity> modify)
        {
            var entity = new Entity();
            modify(entity);

            using (ModifyContext ef = new ModifyContext())
            {
                var repo = GetRepository(ef);
                if ( !repo.Where(predicate).Any() )
                    return repo.Add(entity);
                else
                    return null;
            }
        }

        public static Entity UpdateFirst<T>(Expression<Func<Entity, bool>> predicate, T vo)
        {
            using (ModifyContext ef = new ModifyContext())
            {
                var repo = GetRepository(ef);
                var entities = repo.Where(predicate);
                if (entities.Any())
                    return entities.First().OverrideBy(vo);

                throw new Exception("UpdateFirstBy: No Entity For Update");
            }
        }

        public static int Update(Expression<Func<Entity, bool>> predicate, Action<Entity> modify)
        {
            using (ModifyContext ef = new ModifyContext())
            {
                var repo = GetRepository(ef);
                var entities = repo.Where(predicate);
                foreach (var entity in entities)
                    modify(entity);

                return entities.Count();
            }
        }

        public static Entity DeleteFirst(Expression<Func<Entity, bool>> predicate)
        {
            using (ModifyContext ef = new ModifyContext())
            {
                var repo = GetRepository(ef);
                var query = repo.Where(predicate);

                return repo.Delete(query.First());
            }
        }

        public static int Delete(Expression<Func<Entity, bool>> predicate)
        {
            using (ModifyContext ef = new ModifyContext())
            {
                var repo = GetRepository(ef);
                var query = repo.Where(predicate);

                ef.Remove( query );
                return query.Count();
            }
        }
        
        private static IRepository<Entity> GetRepository()
        {
            return GetRepository(new EFUnitOfWork());
        }

        private static IRepository<Entity> GetRepository(EFUnitOfWork ef)
        {
            var repository = new Repository();
            repository.UnitOfWork = ef;
            return repository;
        }

        private class Repository : EFRepository<Entity>, IRepository<Entity>
        { }

        private class ModifyContext : IDisposable
        {
            public ModifyContext()
            {
                ef = new EFUnitOfWork();
            }

            public void Dispose()
            {
                ef.Commit();
            }

            public void Remove(IEnumerable<Entity> entities)
            {
                ef.Context.Set<Entity>().RemoveRange(entities);
            }

            public static implicit operator EFUnitOfWork(ModifyContext commit)
            {
                return commit.ef;
            }

            private readonly EFUnitOfWork ef;
        }
    }


}
