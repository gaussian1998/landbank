//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Library.Entity.Edmx
{
    using System;
    using System.Collections.Generic;
    
    public partial class AS_Log_Record
    {
        public int ID { get; set; }
        public decimal UserID { get; set; }
        public Nullable<decimal> BranchCode { get; set; }
        public string ModeID { get; set; }
        public string IP { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> OperationTime { get; set; }
        public string Log_define_ID { get; set; }
        public string ModifyDescription { get; set; }
    }
}
