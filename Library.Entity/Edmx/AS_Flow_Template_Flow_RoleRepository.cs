using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Flow_Template_Flow_RoleRepository : EFRepository<AS_Flow_Template_Flow_Role>, IAS_Flow_Template_Flow_RoleRepository
	{

	}

	public  interface IAS_Flow_Template_Flow_RoleRepository : IRepository<AS_Flow_Template_Flow_Role>
	{

	}

   public  class AS_Flow_Template_Flow_Role_Records : GenericAccessUnitOfWork<AS_Flow_Template_Flow_Role>
	{

	}

}