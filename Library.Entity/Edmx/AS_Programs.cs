//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Library.Entity.Edmx
{
    using System;
    using System.Collections.Generic;
    
    public partial class AS_Programs
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AS_Programs()
        {
            this.AS_Programs_Roles = new HashSet<AS_Programs_Roles>();
        }
    
        public int ID { get; set; }
        public string Doc_Type { get; set; }
        public string Programs_Code { get; set; }
        public string Programs_Number { get; set; }
        public string Programs_Name { get; set; }
        public string Brief_Name { get; set; }
        public string Text { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<bool> Cancel_Code { get; set; }
        public string Remark { get; set; }
        public Nullable<System.DateTime> Create_Time { get; set; }
        public Nullable<decimal> Create_By { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public Nullable<decimal> Last_Updated_By { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_Programs_Roles> AS_Programs_Roles { get; set; }
    }
}
