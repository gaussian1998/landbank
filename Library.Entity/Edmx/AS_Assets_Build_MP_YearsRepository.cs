using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Build_MP_YearsRepository : EFRepository<AS_Assets_Build_MP_Years>, IAS_Assets_Build_MP_YearsRepository
	{

	}

	public  interface IAS_Assets_Build_MP_YearsRepository : IRepository<AS_Assets_Build_MP_Years>
	{

	}

   public  class AS_Assets_Build_MP_Years_Records : GenericAccessUnitOfWork<AS_Assets_Build_MP_Years>
	{

	}

}