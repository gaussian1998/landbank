using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Flow_NameRepository : EFRepository<AS_Flow_Name>, IAS_Flow_NameRepository
	{

	}

	public  interface IAS_Flow_NameRepository : IRepository<AS_Flow_Name>
	{

	}

   public  class AS_Flow_Name_Records : GenericAccessUnitOfWork<AS_Flow_Name>
	{

	}

}