using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_ProgramsRepository : EFRepository<AS_Programs>, IAS_ProgramsRepository
	{

	}

	public  interface IAS_ProgramsRepository : IRepository<AS_Programs>
	{

	}

   public  class AS_Programs_Records : GenericAccessUnitOfWork<AS_Programs>
	{

	}

}