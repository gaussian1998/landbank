using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Leases_RentalRepository : EFRepository<AS_Leases_Rental>, IAS_Leases_RentalRepository
	{

	}

	public  interface IAS_Leases_RentalRepository : IRepository<AS_Leases_Rental>
	{

	}

   public  class AS_Leases_Rental_Records : GenericAccessUnitOfWork<AS_Leases_Rental>
	{

	}

}