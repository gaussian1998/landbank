using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Right_RecordRepository : EFRepository<AS_Assets_Right_Record>, IAS_Assets_Right_RecordRepository
	{

	}

	public  interface IAS_Assets_Right_RecordRepository : IRepository<AS_Assets_Right_Record>
	{

	}

   public  class AS_Assets_Right_Record_Records : GenericAccessUnitOfWork<AS_Assets_Right_Record>
	{

	}

}