using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_PlanningBranchRepository : EFRepository<AS_Assets_PlanningBranch>, IAS_Assets_PlanningBranchRepository
	{

	}

	public  interface IAS_Assets_PlanningBranchRepository : IRepository<AS_Assets_PlanningBranch>
	{

	}

   public  class AS_Assets_PlanningBranch_Records : GenericAccessUnitOfWork<AS_Assets_PlanningBranch>
	{

	}

}