using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Land_RecordRepository : EFRepository<AS_Assets_Land_Record>, IAS_Assets_Land_RecordRepository
	{

	}

	public  interface IAS_Assets_Land_RecordRepository : IRepository<AS_Assets_Land_Record>
	{

	}

   public  class AS_Assets_Land_Record_Records : GenericAccessUnitOfWork<AS_Assets_Land_Record>
	{

	}

}