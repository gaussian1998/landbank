using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_GL_Acc_DeprnRepository : EFRepository<AS_GL_Acc_Deprn>, IAS_GL_Acc_DeprnRepository
	{

	}

	public  interface IAS_GL_Acc_DeprnRepository : IRepository<AS_GL_Acc_Deprn>
	{

	}

   public  class AS_GL_Acc_Deprn_Records : GenericAccessUnitOfWork<AS_GL_Acc_Deprn>
	{

	}

}