using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_UsersRepository : EFRepository<AS_Users>, IAS_UsersRepository
	{

	}

	public  interface IAS_UsersRepository : IRepository<AS_Users>
	{

	}

   public  class AS_Users_Records : GenericAccessUnitOfWork<AS_Users>
	{

	}

}