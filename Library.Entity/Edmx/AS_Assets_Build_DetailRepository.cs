using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Build_DetailRepository : EFRepository<AS_Assets_Build_Detail>, IAS_Assets_Build_DetailRepository
	{

	}

	public  interface IAS_Assets_Build_DetailRepository : IRepository<AS_Assets_Build_Detail>
	{

	}

   public  class AS_Assets_Build_Detail_Records : GenericAccessUnitOfWork<AS_Assets_Build_Detail>
	{

	}

}