using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Post_HisRepository : EFRepository<AS_Post_His>, IAS_Post_HisRepository
	{

	}

	public  interface IAS_Post_HisRepository : IRepository<AS_Post_His>
	{

	}

   public  class AS_Post_His_Records : GenericAccessUnitOfWork<AS_Post_His>
	{

	}

}