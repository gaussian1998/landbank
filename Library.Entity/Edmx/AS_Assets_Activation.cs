//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Library.Entity.Edmx
{
    using System;
    using System.Collections.Generic;
    
    public partial class AS_Assets_Activation
    {
        public long ID { get; set; }
        public Nullable<long> ParantID { get; set; }
        public Nullable<long> Document_Type { get; set; }
        public Nullable<int> Source_Type { get; set; }
        public string Activation_No { get; set; }
        public string BOOK_TYPE_CODE { get; set; }
        public Nullable<System.DateTime> BeginDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Jurisdiction1 { get; set; }
        public string Jurisdiction2 { get; set; }
        public string Office_Branch { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<long> Activation_Target { get; set; }
        public string TEL { get; set; }
        public string Email { get; set; }
        public string Remark { get; set; }
        public Nullable<System.DateTime> Create_Time { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public Nullable<int> Last_Updated_By { get; set; }
    }
}
