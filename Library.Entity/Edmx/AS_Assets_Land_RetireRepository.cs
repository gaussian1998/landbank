using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Land_RetireRepository : EFRepository<AS_Assets_Land_Retire>, IAS_Assets_Land_RetireRepository
	{

	}

	public  interface IAS_Assets_Land_RetireRepository : IRepository<AS_Assets_Land_Retire>
	{

	}

   public  class AS_Assets_Land_Retire_Records : GenericAccessUnitOfWork<AS_Assets_Land_Retire>
	{

	}

}