using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Compare_AssetsRepository : EFRepository<AS_Assets_Compare_Assets>, IAS_Assets_Compare_AssetsRepository
	{

	}

	public  interface IAS_Assets_Compare_AssetsRepository : IRepository<AS_Assets_Compare_Assets>
	{

	}

   public  class AS_Assets_Compare_Assets_Records : GenericAccessUnitOfWork<AS_Assets_Compare_Assets>
	{

	}

}