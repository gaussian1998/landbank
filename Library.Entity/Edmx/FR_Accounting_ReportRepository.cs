using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class FR_Accounting_ReportRepository : EFRepository<FR_Accounting_Report>, IFR_Accounting_ReportRepository
	{

	}

	public  interface IFR_Accounting_ReportRepository : IRepository<FR_Accounting_Report>
	{

	}

   public  class FR_Accounting_Report_Records : GenericAccessUnitOfWork<FR_Accounting_Report>
	{

	}

}