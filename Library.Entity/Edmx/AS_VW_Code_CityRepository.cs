using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_VW_Code_CityRepository : EFRepository<AS_VW_Code_City>, IAS_VW_Code_CityRepository
	{

	}

	public  interface IAS_VW_Code_CityRepository : IRepository<AS_VW_Code_City>
	{

	}

   public  class AS_VW_Code_City_Records : GenericAccessUnitOfWork<AS_VW_Code_City>
	{

	}

}