using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Evaluation_FileRepository : EFRepository<AS_Assets_Evaluation_File>, IAS_Assets_Evaluation_FileRepository
	{

	}

	public  interface IAS_Assets_Evaluation_FileRepository : IRepository<AS_Assets_Evaluation_File>
	{

	}

   public  class AS_Assets_Evaluation_File_Records : GenericAccessUnitOfWork<AS_Assets_Evaluation_File>
	{

	}

}