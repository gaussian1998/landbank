using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Leases_ArableRepository : EFRepository<AS_Leases_Arable>, IAS_Leases_ArableRepository
	{

	}

	public  interface IAS_Leases_ArableRepository : IRepository<AS_Leases_Arable>
	{

	}

   public  class AS_Leases_Arable_Records : GenericAccessUnitOfWork<AS_Leases_Arable>
	{

	}

}