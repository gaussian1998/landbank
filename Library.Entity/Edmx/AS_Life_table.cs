//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Library.Entity.Edmx
{
    using System;
    using System.Collections.Generic;
    
    public partial class AS_Life_table
    {
        public int ID { get; set; }
        public string Life_Code_No { get; set; }
        public string Life_Code_1 { get; set; }
        public string Life_Code_2 { get; set; }
        public string Life_Code_3 { get; set; }
        public string Life_Code_4 { get; set; }
        public string Life_Code_5 { get; set; }
        public string Asset_Text { get; set; }
        public decimal Life_Years { get; set; }
        public decimal Life_Months { get; set; }
        public bool Not_Deprn_Flag { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public Nullable<decimal> Last_Updated_By { get; set; }
    }
}
