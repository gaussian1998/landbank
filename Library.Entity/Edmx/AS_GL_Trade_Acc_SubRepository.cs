using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_GL_Trade_Acc_SubRepository : EFRepository<AS_GL_Trade_Acc_Sub>, IAS_GL_Trade_Acc_SubRepository
	{

	}

	public  interface IAS_GL_Trade_Acc_SubRepository : IRepository<AS_GL_Trade_Acc_Sub>
	{

	}

   public  class AS_GL_Trade_Acc_Sub_Records : GenericAccessUnitOfWork<AS_GL_Trade_Acc_Sub>
	{

	}

}