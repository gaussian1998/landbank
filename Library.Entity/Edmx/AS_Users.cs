//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Library.Entity.Edmx
{
    using System;
    using System.Collections.Generic;
    
    public partial class AS_Users
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AS_Users()
        {
            this.AS_Flow_Open = new HashSet<AS_Flow_Open>();
            this.AS_Flow_Open1 = new HashSet<AS_Flow_Open>();
            this.AS_Flow_Open2 = new HashSet<AS_Flow_Open>();
            this.AS_Flow_Open_Log = new HashSet<AS_Flow_Open_Log>();
            this.AS_Flow_Open_Log1 = new HashSet<AS_Flow_Open_Log>();
            this.AS_History_Parameter = new HashSet<AS_History_Parameter>();
            this.AS_History_Parameter1 = new HashSet<AS_History_Parameter>();
            this.AS_Opration_His = new HashSet<AS_Opration_His>();
            this.AS_Post_Header = new HashSet<AS_Post_Header>();
            this.AS_Post_Header1 = new HashSet<AS_Post_Header>();
            this.AS_Role_Flow = new HashSet<AS_Role_Flow>();
            this.AS_Users_Deputy = new HashSet<AS_Users_Deputy>();
            this.AS_Users_Deputy1 = new HashSet<AS_Users_Deputy>();
            this.AS_Users_FlowRole = new HashSet<AS_Users_FlowRole>();
            this.AS_Users_Role = new HashSet<AS_Users_Role>();
        }
    
        public int ID { get; set; }
        public string User_Code { get; set; }
        public string User_Name { get; set; }
        public string Branch_Code { get; set; }
        public string Branch_Name { get; set; }
        public string Department_Code { get; set; }
        public string Department_Name { get; set; }
        public Nullable<int> Department_ID { get; set; }
        public string Office_Branch { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<System.DateTime> Quit_Date { get; set; }
        public string Email { get; set; }
        public string Notes { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public Nullable<int> Last_Updated_By { get; set; }
        public Nullable<bool> Delete_Flag { get; set; }
        public string Title { get; set; }
        public string Sub_Dept_Code { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_Flow_Open> AS_Flow_Open { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_Flow_Open> AS_Flow_Open1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_Flow_Open> AS_Flow_Open2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_Flow_Open_Log> AS_Flow_Open_Log { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_Flow_Open_Log> AS_Flow_Open_Log1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_History_Parameter> AS_History_Parameter { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_History_Parameter> AS_History_Parameter1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_Opration_His> AS_Opration_His { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_Post_Header> AS_Post_Header { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_Post_Header> AS_Post_Header1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_Role_Flow> AS_Role_Flow { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_Users_Deputy> AS_Users_Deputy { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_Users_Deputy> AS_Users_Deputy1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_Users_FlowRole> AS_Users_FlowRole { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_Users_Role> AS_Users_Role { get; set; }
    }
}
