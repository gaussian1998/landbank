using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_LeaseRights_HeaderRepository : EFRepository<AS_LeaseRights_Header>, IAS_LeaseRights_HeaderRepository
	{

	}

	public  interface IAS_LeaseRights_HeaderRepository : IRepository<AS_LeaseRights_Header>
	{

	}

   public  class AS_LeaseRights_Header_Records : GenericAccessUnitOfWork<AS_LeaseRights_Header>
	{

	}

}