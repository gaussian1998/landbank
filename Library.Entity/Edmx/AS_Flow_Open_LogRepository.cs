using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Flow_Open_LogRepository : EFRepository<AS_Flow_Open_Log>, IAS_Flow_Open_LogRepository
	{

	}

	public  interface IAS_Flow_Open_LogRepository : IRepository<AS_Flow_Open_Log>
	{

	}

   public  class AS_Flow_Open_Log_Records : GenericAccessUnitOfWork<AS_Flow_Open_Log>
	{

	}

}