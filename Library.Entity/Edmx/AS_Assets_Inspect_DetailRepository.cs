using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Inspect_DetailRepository : EFRepository<AS_Assets_Inspect_Detail>, IAS_Assets_Inspect_DetailRepository
	{

	}

	public  interface IAS_Assets_Inspect_DetailRepository : IRepository<AS_Assets_Inspect_Detail>
	{

	}

   public  class AS_Assets_Inspect_Detail_Records : GenericAccessUnitOfWork<AS_Assets_Inspect_Detail>
	{

	}

}