using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_ReNew_HeaderRepository : EFRepository<AS_Assets_ReNew_Header>, IAS_Assets_ReNew_HeaderRepository
	{

	}

	public  interface IAS_Assets_ReNew_HeaderRepository : IRepository<AS_Assets_ReNew_Header>
	{

	}

   public  class AS_Assets_ReNew_Header_Records : GenericAccessUnitOfWork<AS_Assets_ReNew_Header>
	{

	}

}