using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_VW_UserAuthority_ReportRepository : EFRepository<AS_VW_UserAuthority_Report>, IAS_VW_UserAuthority_ReportRepository
	{

	}

	public  interface IAS_VW_UserAuthority_ReportRepository : IRepository<AS_VW_UserAuthority_Report>
	{

	}

   public  class AS_VW_UserAuthority_Report_Records : GenericAccessUnitOfWork<AS_VW_UserAuthority_Report>
	{

	}

}