using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Lease_MarginRepository : EFRepository<AS_Lease_Margin>, IAS_Lease_MarginRepository
	{

	}

	public  interface IAS_Lease_MarginRepository : IRepository<AS_Lease_Margin>
	{

	}

   public  class AS_Lease_Margin_Records : GenericAccessUnitOfWork<AS_Lease_Margin>
	{

	}

}