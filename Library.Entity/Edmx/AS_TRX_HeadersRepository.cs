using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_TRX_HeadersRepository : EFRepository<AS_TRX_Headers>, IAS_TRX_HeadersRepository
	{

	}

	public  interface IAS_TRX_HeadersRepository : IRepository<AS_TRX_Headers>
	{

	}

   public  class AS_TRX_Headers_Records : GenericAccessUnitOfWork<AS_TRX_Headers>
	{

	}

}