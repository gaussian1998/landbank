using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Lease_ManageExpenseRepository : EFRepository<AS_Lease_ManageExpense>, IAS_Lease_ManageExpenseRepository
	{

	}

	public  interface IAS_Lease_ManageExpenseRepository : IRepository<AS_Lease_ManageExpense>
	{

	}

   public  class AS_Lease_ManageExpense_Records : GenericAccessUnitOfWork<AS_Lease_ManageExpense>
	{

	}

}