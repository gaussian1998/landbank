using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Flow_DeputyRepository : EFRepository<AS_Flow_Deputy>, IAS_Flow_DeputyRepository
	{

	}

	public  interface IAS_Flow_DeputyRepository : IRepository<AS_Flow_Deputy>
	{

	}

   public  class AS_Flow_Deputy_Records : GenericAccessUnitOfWork<AS_Flow_Deputy>
	{

	}

}