using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Lease_InsuranceRepository : EFRepository<AS_Lease_Insurance>, IAS_Lease_InsuranceRepository
	{

	}

	public  interface IAS_Lease_InsuranceRepository : IRepository<AS_Lease_Insurance>
	{

	}

   public  class AS_Lease_Insurance_Records : GenericAccessUnitOfWork<AS_Lease_Insurance>
	{

	}

}