using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_ActivationRepository : EFRepository<AS_Assets_Activation>, IAS_Assets_ActivationRepository
	{

	}

	public  interface IAS_Assets_ActivationRepository : IRepository<AS_Assets_Activation>
	{

	}

   public  class AS_Assets_Activation_Records : GenericAccessUnitOfWork<AS_Assets_Activation>
	{

	}

}