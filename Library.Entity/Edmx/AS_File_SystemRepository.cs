using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_File_SystemRepository : EFRepository<AS_File_System>, IAS_File_SystemRepository
	{

	}

	public  interface IAS_File_SystemRepository : IRepository<AS_File_System>
	{

	}

   public  class AS_File_System_Records : GenericAccessUnitOfWork<AS_File_System>
	{

	}

}