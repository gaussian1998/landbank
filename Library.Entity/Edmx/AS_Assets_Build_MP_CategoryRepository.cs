using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Build_MP_CategoryRepository : EFRepository<AS_Assets_Build_MP_Category>, IAS_Assets_Build_MP_CategoryRepository
	{

	}

	public  interface IAS_Assets_Build_MP_CategoryRepository : IRepository<AS_Assets_Build_MP_Category>
	{

	}

   public  class AS_Assets_Build_MP_Category_Records : GenericAccessUnitOfWork<AS_Assets_Build_MP_Category>
	{

	}

}