using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_TRX_ContentsRepository : EFRepository<AS_TRX_Contents>, IAS_TRX_ContentsRepository
	{

	}

	public  interface IAS_TRX_ContentsRepository : IRepository<AS_TRX_Contents>
	{

	}

   public  class AS_TRX_Contents_Records : GenericAccessUnitOfWork<AS_TRX_Contents>
	{

	}

}