//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Library.Entity.Edmx
{
    using System;
    using System.Collections.Generic;
    
    public partial class AS_Rate
    {
        public int ID { get; set; }
        public Nullable<decimal> Branch_Code { get; set; }
        public string Department { get; set; }
        public string Book_Type { get; set; }
        public string Currency { get; set; }
        public string Rate_ID { get; set; }
        public Nullable<decimal> Rate { get; set; }
        public Nullable<System.DateTime> Input_Date { get; set; }
        public decimal Basis { get; set; }
        public decimal Days_Of_Year { get; set; }
    }
}
