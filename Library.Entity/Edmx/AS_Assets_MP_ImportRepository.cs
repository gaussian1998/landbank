using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_MP_ImportRepository : EFRepository<AS_Assets_MP_Import>, IAS_Assets_MP_ImportRepository
	{

	}

	public  interface IAS_Assets_MP_ImportRepository : IRepository<AS_Assets_MP_Import>
	{

	}

   public  class AS_Assets_MP_Import_Records : GenericAccessUnitOfWork<AS_Assets_MP_Import>
	{

	}

}