//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Library.Entity.Edmx
{
    using System;
    using System.Collections.Generic;
    
    public partial class AS_Assets_Land
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AS_Assets_Land()
        {
            this.AS_TRX_Cost = new HashSet<AS_TRX_Cost>();
        }
    
        public int ID { get; set; }
        public string Office_Branch { get; set; }
        public string Book_Type_Code { get; set; }
        public string Asset_Number { get; set; }
        public string Asset_Category_code { get; set; }
        public string Location_Disp { get; set; }
        public string Old_Asset_Number { get; set; }
        public string Old_Asset_Category_code { get; set; }
        public string Description { get; set; }
        public string City_Code { get; set; }
        public string City_Name { get; set; }
        public string District_Code { get; set; }
        public string District_Name { get; set; }
        public string Section_Code { get; set; }
        public string Section_Name { get; set; }
        public string Sub_Section_Name { get; set; }
        public string Parent_Land_Number { get; set; }
        public string Filial_Land_Number { get; set; }
        public string Authorization_Number { get; set; }
        public string Authorized_name { get; set; }
        public decimal Area_Size { get; set; }
        public decimal Authorized_Scope_Molecule { get; set; }
        public decimal Authorized_Scope_Denomminx { get; set; }
        public decimal Authorized_Area { get; set; }
        public string Used_Type { get; set; }
        public string Used_Status { get; set; }
        public string Obtained_Method { get; set; }
        public string Used_Partition { get; set; }
        public string Is_Marginal_Land { get; set; }
        public decimal Own_Area { get; set; }
        public decimal NONOwn_Area { get; set; }
        public decimal Original_Cost { get; set; }
        public decimal Deprn_Amount { get; set; }
        public decimal Reval_Land_VAT { get; set; }
        public decimal Reval_Adjustment_Amount { get; set; }
        public decimal Business_Area_Size { get; set; }
        public decimal NONBusiness_Area_Size { get; set; }
        public decimal Current_Cost { get; set; }
        public decimal Reval_Reserve { get; set; }
        public decimal Business_Book_Amount { get; set; }
        public decimal NONBusiness_Book_Amount { get; set; }
        public decimal Business_Reval_Reserve { get; set; }
        public decimal NONBusiness_Reval_Reserve { get; set; }
        public System.DateTime Datetime_Placed_In_Service { get; set; }
        public decimal Year_Number { get; set; }
        public decimal Announce_Amount { get; set; }
        public decimal Announce_Price { get; set; }
        public string Tax_Type { get; set; }
        public string Reduction_reason { get; set; }
        public decimal Reduction_Area { get; set; }
        public decimal Declared_Price { get; set; }
        public decimal Dutiable_Amount { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public decimal Current_units { get; set; }
        public decimal Retire_Cost { get; set; }
        public decimal Sell_Amount { get; set; }
        public decimal Sell_Cost { get; set; }
        public decimal Transfer_Price { get; set; }
        public string Delete_Reason { get; set; }
        public string Urban_Renewal { get; set; }
        public string Land_Item { get; set; }
        public string CROP { get; set; }
        public System.DateTime Create_Time { get; set; }
        public string Created_By { get; set; }
        public string Created_By_Name { get; set; }
        public Nullable<System.DateTime> Last_UpDatetimed_Time { get; set; }
        public Nullable<decimal> Last_UpDatetimed_By { get; set; }
        public string Last_UpDatetimed_By_Name { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AS_TRX_Cost> AS_TRX_Cost { get; set; }
    }
}
