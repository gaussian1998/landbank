using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_VW_TodoList_ReportRepository : EFRepository<AS_VW_TodoList_Report>, IAS_VW_TodoList_ReportRepository
	{

	}

	public  interface IAS_VW_TodoList_ReportRepository : IRepository<AS_VW_TodoList_Report>
	{

	}

   public  class AS_VW_TodoList_Report_Records : GenericAccessUnitOfWork<AS_VW_TodoList_Report>
	{

	}

}