using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_InspectRepository : EFRepository<AS_Assets_Inspect>, IAS_Assets_InspectRepository
	{

	}

	public  interface IAS_Assets_InspectRepository : IRepository<AS_Assets_Inspect>
	{

	}

   public  class AS_Assets_Inspect_Records : GenericAccessUnitOfWork<AS_Assets_Inspect>
	{

	}

}