//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Library.Entity.Edmx
{
    using System;
    using System.Collections.Generic;
    
    public partial class AS_Flow_Template_Flow_Role
    {
        public int ID { get; set; }
        public int Flow_Template_ID { get; set; }
        public int Flow_Role_ID { get; set; }
        public int Step_No { get; set; }
        public Nullable<bool> End_Point { get; set; }
    
        public virtual AS_Code_Table AS_Code_Table { get; set; }
        public virtual AS_Flow_Template AS_Flow_Template { get; set; }
    }
}
