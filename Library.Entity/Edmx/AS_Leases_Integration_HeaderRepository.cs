using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Leases_Integration_HeaderRepository : EFRepository<AS_Leases_Integration_Header>, IAS_Leases_Integration_HeaderRepository
	{

	}

	public  interface IAS_Leases_Integration_HeaderRepository : IRepository<AS_Leases_Integration_Header>
	{

	}

   public  class AS_Leases_Integration_Header_Records : GenericAccessUnitOfWork<AS_Leases_Integration_Header>
	{

	}

}