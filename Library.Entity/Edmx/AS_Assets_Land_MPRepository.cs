using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Land_MPRepository : EFRepository<AS_Assets_Land_MP>, IAS_Assets_Land_MPRepository
	{

	}

	public  interface IAS_Assets_Land_MPRepository : IRepository<AS_Assets_Land_MP>
	{

	}

   public  class AS_Assets_Land_MP_Records : GenericAccessUnitOfWork<AS_Assets_Land_MP>
	{

	}

}