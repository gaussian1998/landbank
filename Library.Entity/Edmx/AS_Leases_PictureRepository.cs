using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Leases_PictureRepository : EFRepository<AS_Leases_Picture>, IAS_Leases_PictureRepository
	{

	}

	public  interface IAS_Leases_PictureRepository : IRepository<AS_Leases_Picture>
	{

	}

   public  class AS_Leases_Picture_Records : GenericAccessUnitOfWork<AS_Leases_Picture>
	{

	}

}