using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_LandRepository : EFRepository<AS_Assets_Land>, IAS_Assets_LandRepository
	{

	}

	public  interface IAS_Assets_LandRepository : IRepository<AS_Assets_Land>
	{

	}

   public  class AS_Assets_Land_Records : GenericAccessUnitOfWork<AS_Assets_Land>
	{

	}

}