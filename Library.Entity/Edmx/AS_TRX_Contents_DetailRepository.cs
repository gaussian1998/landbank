using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_TRX_Contents_DetailRepository : EFRepository<AS_TRX_Contents_Detail>, IAS_TRX_Contents_DetailRepository
	{

	}

	public  interface IAS_TRX_Contents_DetailRepository : IRepository<AS_TRX_Contents_Detail>
	{

	}

   public  class AS_TRX_Contents_Detail_Records : GenericAccessUnitOfWork<AS_TRX_Contents_Detail>
	{

	}

}