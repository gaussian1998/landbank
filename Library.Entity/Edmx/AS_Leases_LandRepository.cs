using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Leases_LandRepository : EFRepository<AS_Leases_Land>, IAS_Leases_LandRepository
	{

	}

	public  interface IAS_Leases_LandRepository : IRepository<AS_Leases_Land>
	{

	}

   public  class AS_Leases_Land_Records : GenericAccessUnitOfWork<AS_Leases_Land>
	{

	}

}