using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Log_DefineRepository : EFRepository<AS_Log_Define>, IAS_Log_DefineRepository
	{

	}

	public  interface IAS_Log_DefineRepository : IRepository<AS_Log_Define>
	{

	}

   public  class AS_Log_Define_Records : GenericAccessUnitOfWork<AS_Log_Define>
	{

	}

}