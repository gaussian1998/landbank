using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Land_MLRepository : EFRepository<AS_Assets_Land_ML>, IAS_Assets_Land_MLRepository
	{

	}

	public  interface IAS_Assets_Land_MLRepository : IRepository<AS_Assets_Land_ML>
	{

	}

   public  class AS_Assets_Land_ML_Records : GenericAccessUnitOfWork<AS_Assets_Land_ML>
	{

	}

}