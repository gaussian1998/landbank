using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Asset_Category_Use_TypesRepository : EFRepository<AS_Asset_Category_Use_Types>, IAS_Asset_Category_Use_TypesRepository
	{

	}

	public  interface IAS_Asset_Category_Use_TypesRepository : IRepository<AS_Asset_Category_Use_Types>
	{

	}

   public  class AS_Asset_Category_Use_Types_Records : GenericAccessUnitOfWork<AS_Asset_Category_Use_Types>
	{

	}

}