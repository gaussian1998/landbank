using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Flow_UploadRepository : EFRepository<AS_Flow_Upload>, IAS_Flow_UploadRepository
	{

	}

	public  interface IAS_Flow_UploadRepository : IRepository<AS_Flow_Upload>
	{

	}

   public  class AS_Flow_Upload_Records : GenericAccessUnitOfWork<AS_Flow_Upload>
	{

	}

}