using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_LeaseRights_DeprnRepository : EFRepository<AS_LeaseRights_Deprn>, IAS_LeaseRights_DeprnRepository
	{

	}

	public  interface IAS_LeaseRights_DeprnRepository : IRepository<AS_LeaseRights_Deprn>
	{

	}

   public  class AS_LeaseRights_Deprn_Records : GenericAccessUnitOfWork<AS_LeaseRights_Deprn>
	{

	}

}