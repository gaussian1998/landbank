using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_ReNew_RecordRepository : EFRepository<AS_Assets_ReNew_Record>, IAS_Assets_ReNew_RecordRepository
	{

	}

	public  interface IAS_Assets_ReNew_RecordRepository : IRepository<AS_Assets_ReNew_Record>
	{

	}

   public  class AS_Assets_ReNew_Record_Records : GenericAccessUnitOfWork<AS_Assets_ReNew_Record>
	{

	}

}