using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Land_LogRepository : EFRepository<AS_Assets_Land_Log>, IAS_Assets_Land_LogRepository
	{

	}

	public  interface IAS_Assets_Land_LogRepository : IRepository<AS_Assets_Land_Log>
	{

	}

   public  class AS_Assets_Land_Log_Records : GenericAccessUnitOfWork<AS_Assets_Land_Log>
	{

	}

}