using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Deprn_ChargesRepository : EFRepository<AS_Deprn_Charges>, IAS_Deprn_ChargesRepository
	{

	}

	public  interface IAS_Deprn_ChargesRepository : IRepository<AS_Deprn_Charges>
	{

	}

   public  class AS_Deprn_Charges_Records : GenericAccessUnitOfWork<AS_Deprn_Charges>
	{

	}

}