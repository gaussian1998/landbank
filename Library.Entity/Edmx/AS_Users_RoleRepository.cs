using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Users_RoleRepository : EFRepository<AS_Users_Role>, IAS_Users_RoleRepository
	{

	}

	public  interface IAS_Users_RoleRepository : IRepository<AS_Users_Role>
	{

	}

   public  class AS_Users_Role_Records : GenericAccessUnitOfWork<AS_Users_Role>
	{

	}

}