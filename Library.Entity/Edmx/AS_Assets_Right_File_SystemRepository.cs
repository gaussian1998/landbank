using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Right_File_SystemRepository : EFRepository<AS_Assets_Right_File_System>, IAS_Assets_Right_File_SystemRepository
	{

	}

	public  interface IAS_Assets_Right_File_SystemRepository : IRepository<AS_Assets_Right_File_System>
	{

	}

   public  class AS_Assets_Right_File_System_Records : GenericAccessUnitOfWork<AS_Assets_Right_File_System>
	{

	}

}