using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class ContentFileRepository : EFRepository<ContentFile>, IContentFileRepository
	{

	}

	public  interface IContentFileRepository : IRepository<ContentFile>
	{

	}

   public  class ContentFile_Records : GenericAccessUnitOfWork<ContentFile>
	{

	}

}