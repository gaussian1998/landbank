using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Flow_RecordRepository : EFRepository<AS_Flow_Record>, IAS_Flow_RecordRepository
	{

	}

	public  interface IAS_Flow_RecordRepository : IRepository<AS_Flow_Record>
	{

	}

   public  class AS_Flow_Record_Records : GenericAccessUnitOfWork<AS_Flow_Record>
	{

	}

}