using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Leases_OtherRepository : EFRepository<AS_Leases_Other>, IAS_Leases_OtherRepository
	{

	}

	public  interface IAS_Leases_OtherRepository : IRepository<AS_Leases_Other>
	{

	}

   public  class AS_Leases_Other_Records : GenericAccessUnitOfWork<AS_Leases_Other>
	{

	}

}