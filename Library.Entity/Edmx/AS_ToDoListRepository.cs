using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_ToDoListRepository : EFRepository<AS_ToDoList>, IAS_ToDoListRepository
	{

	}

	public  interface IAS_ToDoListRepository : IRepository<AS_ToDoList>
	{

	}

   public  class AS_ToDoList_Records : GenericAccessUnitOfWork<AS_ToDoList>
	{

	}

}