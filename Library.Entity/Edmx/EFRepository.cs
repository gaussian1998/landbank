using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Library.Entity.Edmx
{
	public class EFRepository<T> : IRepository<T> where T : class
	{
		public IUnitOfWork UnitOfWork { get; set; }
		
		private IDbSet<T> _objectset;
		private IDbSet<T> ObjectSet
		{
			get
			{
				if (_objectset == null)
				{
					_objectset = UnitOfWork.Context.Set<T>();
				}
				return _objectset;
			}
		}

		public virtual IQueryable<T> All()
		{
			return ObjectSet.AsQueryable();
		}

		public IQueryable<T> Where(Expression<Func<T, bool>> expression)
		{
			return ObjectSet.Where(expression);
		}

		public T Add(T entity)
		{
			return ObjectSet.Add(entity);
		}

		public T Delete(T entity)
		{
			return ObjectSet.Remove(entity);
		}

	}
}