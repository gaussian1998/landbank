using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_DocumentTypeRepository : EFRepository<AS_Assets_DocumentType>, IAS_Assets_DocumentTypeRepository
	{

	}

	public  interface IAS_Assets_DocumentTypeRepository : IRepository<AS_Assets_DocumentType>
	{

	}

   public  class AS_Assets_DocumentType_Records : GenericAccessUnitOfWork<AS_Assets_DocumentType>
	{

	}

}