//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Library.Entity.Edmx
{
    using System;
    using System.Collections.Generic;
    
    public partial class AS_GL_Calendar
    {
        public int ID { get; set; }
        public string GL_Term { get; set; }
        public string Book_Type { get; set; }
        public string Calendar_Value { get; set; }
        public Nullable<decimal> Years { get; set; }
        public Nullable<decimal> Months { get; set; }
        public Nullable<decimal> Quarter { get; set; }
        public Nullable<System.DateTime> Date_Of_Begin { get; set; }
        public Nullable<System.DateTime> Date_Of_End { get; set; }
        public decimal Term_Basis { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public Nullable<decimal> Last_Updated_By { get; set; }
    }
}
