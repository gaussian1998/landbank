using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_GL_Acc_DeletedRepository : EFRepository<AS_GL_Acc_Deleted>, IAS_GL_Acc_DeletedRepository
	{

	}

	public  interface IAS_GL_Acc_DeletedRepository : IRepository<AS_GL_Acc_Deleted>
	{

	}

   public  class AS_GL_Acc_Deleted_Records : GenericAccessUnitOfWork<AS_GL_Acc_Deleted>
	{

	}

}