using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Prog_TemplateRepository : EFRepository<AS_Prog_Template>, IAS_Prog_TemplateRepository
	{

	}

	public  interface IAS_Prog_TemplateRepository : IRepository<AS_Prog_Template>
	{

	}

   public  class AS_Prog_Template_Records : GenericAccessUnitOfWork<AS_Prog_Template>
	{

	}

}