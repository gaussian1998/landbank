using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Flow_OpenRepository : EFRepository<AS_Flow_Open>, IAS_Flow_OpenRepository
	{

	}

	public  interface IAS_Flow_OpenRepository : IRepository<AS_Flow_Open>
	{

	}

   public  class AS_Flow_Open_Records : GenericAccessUnitOfWork<AS_Flow_Open>
	{

	}

}