using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_EvaluationRepository : EFRepository<AS_Assets_Evaluation>, IAS_Assets_EvaluationRepository
	{

	}

	public  interface IAS_Assets_EvaluationRepository : IRepository<AS_Assets_Evaluation>
	{

	}

   public  class AS_Assets_Evaluation_Records : GenericAccessUnitOfWork<AS_Assets_Evaluation>
	{

	}

}