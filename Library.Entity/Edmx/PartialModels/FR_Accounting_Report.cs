﻿using Library.Common.Extension;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Library.Entity.Edmx
{
    public partial class FR_Accounting_Report
    {
        public decimal Debit_Amount { get; set; }
        public decimal Credit_Amount { get; set; }

        public static explicit operator FR_Accounting_Report(AS_GL_Acc_Months entity)
        {
            return new FR_Accounting_Report
            {
                FR_Account = entity.Account,
                FR_Account_Name = entity.Account_Name,
                FR_Debit_Amount = entity.DB_CR == "D" ? entity.Amount.Money() : "",
                FR_Credit_Amount = entity.DB_CR == "C" ? entity.Amount.Money() : "",
                FR_Trans_Seq = entity.Trans_Seq,
                FR_Asset_Number = entity.Asset_Number,
                FR_Lease_Number = entity.Lease_Number,
                FR_Batch_Seq = entity.Batch_Seq,
                FR_Post_Seq = entity.Post_Seq.ToString(),
                FR_Post_Date = entity.Post_Date.ShortDescription(),
                FR_Settle_Date = entity.Settle_Date.ShortDescription(),
                FR_Payment_User = entity.Payment_User,
                FR_Virtual_Account = entity.Virtual_Account,
                Debit_Amount = entity.DB_CR == "D" ? entity.Amount : 0,
                Credit_Amount = entity.DB_CR == "C" ? entity.Amount : 0
            };
        }

        public FR_Accounting_Report SetBranch(string branch)
        {
            FR_Branch = branch;
            return this;
        }
    }

    public static class FR_Accounting_Report_Extension
    {
        public static List<FR_Accounting_Report> GroupMoney<Key>(this List<FR_Accounting_Report> source, Func<FR_Accounting_Report, Key> keySelector)
        {
            var query = source.GroupBy(keySelector).Select( g => new FR_Accounting_Report
            {
                FR_Branch = g.FirstOrDefault().FR_Branch,
                FR_Account = g.FirstOrDefault().FR_Account,
                FR_Account_Name = g.FirstOrDefault().FR_Account_Name,
                FR_Trans_Seq = g.FirstOrDefault().FR_Trans_Seq,
                FR_Asset_Number = g.FirstOrDefault().FR_Asset_Number,
                FR_Lease_Number = g.FirstOrDefault().FR_Lease_Number,
                FR_Batch_Seq = g.FirstOrDefault().FR_Batch_Seq,
                FR_Post_Seq = g.FirstOrDefault().FR_Post_Seq,
                FR_Post_Date = g.FirstOrDefault().FR_Post_Date,
                FR_Settle_Date = g.FirstOrDefault().FR_Settle_Date,
                FR_Payment_User = g.FirstOrDefault().FR_Payment_User,
                FR_Virtual_Account = g.FirstOrDefault().FR_Virtual_Account,
                FR_Debit_Amount = g.Sum(m => m.Debit_Amount).Money(),
                FR_Credit_Amount = g.Sum(m => m.Credit_Amount).Money(),
            });

            return query.ToList();
        }
    }
}
