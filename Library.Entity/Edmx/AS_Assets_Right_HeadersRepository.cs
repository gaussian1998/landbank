using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Right_HeadersRepository : EFRepository<AS_Assets_Right_Headers>, IAS_Assets_Right_HeadersRepository
	{

	}

	public  interface IAS_Assets_Right_HeadersRepository : IRepository<AS_Assets_Right_Headers>
	{

	}

   public  class AS_Assets_Right_Headers_Records : GenericAccessUnitOfWork<AS_Assets_Right_Headers>
	{

	}

}