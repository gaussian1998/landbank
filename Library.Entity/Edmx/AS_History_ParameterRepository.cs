using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_History_ParameterRepository : EFRepository<AS_History_Parameter>, IAS_History_ParameterRepository
	{

	}

	public  interface IAS_History_ParameterRepository : IRepository<AS_History_Parameter>
	{

	}

   public  class AS_History_Parameter_Records : GenericAccessUnitOfWork<AS_History_Parameter>
	{

	}

}