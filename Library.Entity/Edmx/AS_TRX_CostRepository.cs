using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_TRX_CostRepository : EFRepository<AS_TRX_Cost>, IAS_TRX_CostRepository
	{

	}

	public  interface IAS_TRX_CostRepository : IRepository<AS_TRX_Cost>
	{

	}

   public  class AS_TRX_Cost_Records : GenericAccessUnitOfWork<AS_TRX_Cost>
	{

	}

}