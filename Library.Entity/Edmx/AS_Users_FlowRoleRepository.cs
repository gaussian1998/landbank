using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Users_FlowRoleRepository : EFRepository<AS_Users_FlowRole>, IAS_Users_FlowRoleRepository
	{

	}

	public  interface IAS_Users_FlowRoleRepository : IRepository<AS_Users_FlowRole>
	{

	}

   public  class AS_Users_FlowRole_Records : GenericAccessUnitOfWork<AS_Users_FlowRole>
	{

	}

}