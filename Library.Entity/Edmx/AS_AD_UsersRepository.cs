using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_AD_UsersRepository : EFRepository<AS_AD_Users>, IAS_AD_UsersRepository
	{

	}

	public  interface IAS_AD_UsersRepository : IRepository<AS_AD_Users>
	{

	}

   public  class AS_AD_Users_Records : GenericAccessUnitOfWork<AS_AD_Users>
	{

	}

}