using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Build_RetireRepository : EFRepository<AS_Assets_Build_Retire>, IAS_Assets_Build_RetireRepository
	{

	}

	public  interface IAS_Assets_Build_RetireRepository : IRepository<AS_Assets_Build_Retire>
	{

	}

   public  class AS_Assets_Build_Retire_Records : GenericAccessUnitOfWork<AS_Assets_Build_Retire>
	{

	}

}