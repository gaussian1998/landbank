using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Leases_Rent_To_BuildRepository : EFRepository<AS_Leases_Rent_To_Build>, IAS_Leases_Rent_To_BuildRepository
	{

	}

	public  interface IAS_Leases_Rent_To_BuildRepository : IRepository<AS_Leases_Rent_To_Build>
	{

	}

   public  class AS_Leases_Rent_To_Build_Records : GenericAccessUnitOfWork<AS_Leases_Rent_To_Build>
	{

	}

}