using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_VW_Department_ReportRepository : EFRepository<AS_VW_Department_Report>, IAS_VW_Department_ReportRepository
	{

	}

	public  interface IAS_VW_Department_ReportRepository : IRepository<AS_VW_Department_Report>
	{

	}

   public  class AS_VW_Department_Report_Records : GenericAccessUnitOfWork<AS_VW_Department_Report>
	{

	}

}