using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Agent_FlowRepository : EFRepository<AS_Agent_Flow>, IAS_Agent_FlowRepository
	{

	}

	public  interface IAS_Agent_FlowRepository : IRepository<AS_Agent_Flow>
	{

	}

   public  class AS_Agent_Flow_Records : GenericAccessUnitOfWork<AS_Agent_Flow>
	{

	}

}