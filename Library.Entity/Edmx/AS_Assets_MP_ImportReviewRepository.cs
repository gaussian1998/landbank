using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_MP_ImportReviewRepository : EFRepository<AS_Assets_MP_ImportReview>, IAS_Assets_MP_ImportReviewRepository
	{

	}

	public  interface IAS_Assets_MP_ImportReviewRepository : IRepository<AS_Assets_MP_ImportReview>
	{

	}

   public  class AS_Assets_MP_ImportReview_Records : GenericAccessUnitOfWork<AS_Assets_MP_ImportReview>
	{

	}

}