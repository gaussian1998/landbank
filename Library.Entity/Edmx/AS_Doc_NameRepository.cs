using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Doc_NameRepository : EFRepository<AS_Doc_Name>, IAS_Doc_NameRepository
	{

	}

	public  interface IAS_Doc_NameRepository : IRepository<AS_Doc_Name>
	{

	}

   public  class AS_Doc_Name_Records : GenericAccessUnitOfWork<AS_Doc_Name>
	{

	}

}