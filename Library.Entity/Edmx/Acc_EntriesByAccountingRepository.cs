using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class Acc_EntriesByAccountingRepository : EFRepository<Acc_EntriesByAccounting>, IAcc_EntriesByAccountingRepository
	{

	}

	public  interface IAcc_EntriesByAccountingRepository : IRepository<Acc_EntriesByAccounting>
	{

	}

   public  class Acc_EntriesByAccounting_Records : GenericAccessUnitOfWork<Acc_EntriesByAccounting>
	{

	}

}