using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_LeaseRights_RecordRepository : EFRepository<AS_LeaseRights_Record>, IAS_LeaseRights_RecordRepository
	{

	}

	public  interface IAS_LeaseRights_RecordRepository : IRepository<AS_LeaseRights_Record>
	{

	}

   public  class AS_LeaseRights_Record_Records : GenericAccessUnitOfWork<AS_LeaseRights_Record>
	{

	}

}