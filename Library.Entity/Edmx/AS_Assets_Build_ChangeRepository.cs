using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Build_ChangeRepository : EFRepository<AS_Assets_Build_Change>, IAS_Assets_Build_ChangeRepository
	{

	}

	public  interface IAS_Assets_Build_ChangeRepository : IRepository<AS_Assets_Build_Change>
	{

	}

   public  class AS_Assets_Build_Change_Records : GenericAccessUnitOfWork<AS_Assets_Build_Change>
	{

	}

}