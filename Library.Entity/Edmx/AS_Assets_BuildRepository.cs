using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_BuildRepository : EFRepository<AS_Assets_Build>, IAS_Assets_BuildRepository
	{

	}

	public  interface IAS_Assets_BuildRepository : IRepository<AS_Assets_Build>
	{

	}

   public  class AS_Assets_Build_Records : GenericAccessUnitOfWork<AS_Assets_Build>
	{

	}

}