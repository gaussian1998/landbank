using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Lease_Payment_AccountRepository : EFRepository<AS_Lease_Payment_Account>, IAS_Lease_Payment_AccountRepository
	{

	}

	public  interface IAS_Lease_Payment_AccountRepository : IRepository<AS_Lease_Payment_Account>
	{

	}

   public  class AS_Lease_Payment_Account_Records : GenericAccessUnitOfWork<AS_Lease_Payment_Account>
	{

	}

}