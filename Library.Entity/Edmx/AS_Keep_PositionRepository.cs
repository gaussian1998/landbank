using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Keep_PositionRepository : EFRepository<AS_Keep_Position>, IAS_Keep_PositionRepository
	{

	}

	public  interface IAS_Keep_PositionRepository : IRepository<AS_Keep_Position>
	{

	}

   public  class AS_Keep_Position_Records : GenericAccessUnitOfWork<AS_Keep_Position>
	{

	}

}