using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Flow_TemplateRepository : EFRepository<AS_Flow_Template>, IAS_Flow_TemplateRepository
	{

	}

	public  interface IAS_Flow_TemplateRepository : IRepository<AS_Flow_Template>
	{

	}

   public  class AS_Flow_Template_Records : GenericAccessUnitOfWork<AS_Flow_Template>
	{

	}

}