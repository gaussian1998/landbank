using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_GL_Acc_Deprn_HisRepository : EFRepository<AS_GL_Acc_Deprn_His>, IAS_GL_Acc_Deprn_HisRepository
	{

	}

	public  interface IAS_GL_Acc_Deprn_HisRepository : IRepository<AS_GL_Acc_Deprn_His>
	{

	}

   public  class AS_GL_Acc_Deprn_His_Records : GenericAccessUnitOfWork<AS_GL_Acc_Deprn_His>
	{

	}

}