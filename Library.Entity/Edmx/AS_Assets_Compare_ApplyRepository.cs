using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Compare_ApplyRepository : EFRepository<AS_Assets_Compare_Apply>, IAS_Assets_Compare_ApplyRepository
	{

	}

	public  interface IAS_Assets_Compare_ApplyRepository : IRepository<AS_Assets_Compare_Apply>
	{

	}

   public  class AS_Assets_Compare_Apply_Records : GenericAccessUnitOfWork<AS_Assets_Compare_Apply>
	{

	}

}