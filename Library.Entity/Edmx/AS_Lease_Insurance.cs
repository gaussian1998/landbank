//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Library.Entity.Edmx
{
    using System;
    using System.Collections.Generic;
    
    public partial class AS_Lease_Insurance
    {
        public int ID { get; set; }
        public Nullable<decimal> LEASE_HEADER_ID { get; set; }
        public string LEASE_NUMBER { get; set; }
        public string INSURANCE_TYPE { get; set; }
        public string COMPANY { get; set; }
        public string INSURANCE_NO { get; set; }
        public string COMPANY_CONTACT { get; set; }
        public string COMPANY_PHONE { get; set; }
        public string APPLICANT { get; set; }
        public string INSURANT { get; set; }
        public Nullable<System.DateTime> START_DATE { get; set; }
        public Nullable<System.DateTime> END_DATE { get; set; }
        public Nullable<int> PRICE { get; set; }
        public string DESCRIPTION { get; set; }
        public Nullable<System.DateTime> CREATION_DATE { get; set; }
        public Nullable<decimal> CREATED_BY { get; set; }
        public Nullable<System.DateTime> LAST_UPDATE_DATE { get; set; }
        public Nullable<decimal> LAST_UPDATED_BY { get; set; }
    }
}
