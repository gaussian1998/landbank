using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Build_LogRepository : EFRepository<AS_Assets_Build_Log>, IAS_Assets_Build_LogRepository
	{

	}

	public  interface IAS_Assets_Build_LogRepository : IRepository<AS_Assets_Build_Log>
	{

	}

   public  class AS_Assets_Build_Log_Records : GenericAccessUnitOfWork<AS_Assets_Build_Log>
	{

	}

}