using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Log_RecordRepository : EFRepository<AS_Log_Record>, IAS_Log_RecordRepository
	{

	}

	public  interface IAS_Log_RecordRepository : IRepository<AS_Log_Record>
	{

	}

   public  class AS_Log_Record_Records : GenericAccessUnitOfWork<AS_Log_Record>
	{

	}

}