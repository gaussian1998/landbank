using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Asset_Category_SerialRepository : EFRepository<AS_Asset_Category_Serial>, IAS_Asset_Category_SerialRepository
	{

	}

	public  interface IAS_Asset_Category_SerialRepository : IRepository<AS_Asset_Category_Serial>
	{

	}

   public  class AS_Asset_Category_Serial_Records : GenericAccessUnitOfWork<AS_Asset_Category_Serial>
	{

	}

}