using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_VW_Acc_SettleRepository : EFRepository<AS_VW_Acc_Settle>, IAS_VW_Acc_SettleRepository
	{

	}

	public  interface IAS_VW_Acc_SettleRepository : IRepository<AS_VW_Acc_Settle>
	{

	}

   public  class AS_VW_Acc_Settle_Records : GenericAccessUnitOfWork<AS_VW_Acc_Settle>
	{

	}

}