using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Programs_RolesRepository : EFRepository<AS_Programs_Roles>, IAS_Programs_RolesRepository
	{

	}

	public  interface IAS_Programs_RolesRepository : IRepository<AS_Programs_Roles>
	{

	}

   public  class AS_Programs_Roles_Records : GenericAccessUnitOfWork<AS_Programs_Roles>
	{

	}

}