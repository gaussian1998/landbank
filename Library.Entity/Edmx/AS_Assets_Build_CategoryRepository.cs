using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Build_CategoryRepository : EFRepository<AS_Assets_Build_Category>, IAS_Assets_Build_CategoryRepository
	{

	}

	public  interface IAS_Assets_Build_CategoryRepository : IRepository<AS_Assets_Build_Category>
	{

	}

   public  class AS_Assets_Build_Category_Records : GenericAccessUnitOfWork<AS_Assets_Build_Category>
	{

	}

}