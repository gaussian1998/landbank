using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Branch_CurrencyRepository : EFRepository<AS_Branch_Currency>, IAS_Branch_CurrencyRepository
	{

	}

	public  interface IAS_Branch_CurrencyRepository : IRepository<AS_Branch_Currency>
	{

	}

   public  class AS_Branch_Currency_Records : GenericAccessUnitOfWork<AS_Branch_Currency>
	{

	}

}