using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Opration_HisRepository : EFRepository<AS_Opration_His>, IAS_Opration_HisRepository
	{

	}

	public  interface IAS_Opration_HisRepository : IRepository<AS_Opration_His>
	{

	}

   public  class AS_Opration_His_Records : GenericAccessUnitOfWork<AS_Opration_His>
	{

	}

}