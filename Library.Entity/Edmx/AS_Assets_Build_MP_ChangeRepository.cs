using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_Build_MP_ChangeRepository : EFRepository<AS_Assets_Build_MP_Change>, IAS_Assets_Build_MP_ChangeRepository
	{

	}

	public  interface IAS_Assets_Build_MP_ChangeRepository : IRepository<AS_Assets_Build_MP_Change>
	{

	}

   public  class AS_Assets_Build_MP_Change_Records : GenericAccessUnitOfWork<AS_Assets_Build_MP_Change>
	{

	}

}