using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Users_DeputyRepository : EFRepository<AS_Users_Deputy>, IAS_Users_DeputyRepository
	{

	}

	public  interface IAS_Users_DeputyRepository : IRepository<AS_Users_Deputy>
	{

	}

   public  class AS_Users_Deputy_Records : GenericAccessUnitOfWork<AS_Users_Deputy>
	{

	}

}