using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_MP_ImportHISRepository : EFRepository<AS_Assets_MP_ImportHIS>, IAS_Assets_MP_ImportHISRepository
	{

	}

	public  interface IAS_Assets_MP_ImportHISRepository : IRepository<AS_Assets_MP_ImportHIS>
	{

	}

   public  class AS_Assets_MP_ImportHIS_Records : GenericAccessUnitOfWork<AS_Assets_MP_ImportHIS>
	{

	}

}