using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_AssetMasterRepository : EFRepository<AS_Assets_AssetMaster>, IAS_Assets_AssetMasterRepository
	{

	}

	public  interface IAS_Assets_AssetMasterRepository : IRepository<AS_Assets_AssetMaster>
	{

	}

   public  class AS_Assets_AssetMaster_Records : GenericAccessUnitOfWork<AS_Assets_AssetMaster>
	{

	}

}