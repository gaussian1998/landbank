﻿//連線字串位置
#define DevelopConfig
//#define TradevanConfig
//#define Control_Version
//#define LandBankConfig
//#define LanceConfig
//#define Kidconfig

namespace Library.Entity.Edmx
{
    public partial class AS_LandBankEntities
    {
#if DevelopConfig
        public AS_LandBankEntities() : base("name=AS_LandBankEntities"){}
#endif

#if TradevanConfig
        public AS_LandBankEntities() : base("name=AS_LandBankEntitiesTradeVan") { }
#endif

#if Control_Version
        public AS_LandBankEntities() : base("name=AS_LandBankEntitiesControl_Version") { }
#endif

#if LandBankConfig
        public AS_LandBankEntities() : base("name=AS_LandBankEntitiesLandBankHost") { }
#endif

#if LanceConfig
        public AS_LandBankEntities() : base("name=AS_LandBankEntitiesLance") { }
#endif

#if Kidconfig
        public AS_LandBankEntities() : base("name=AS_LandBankEntitiesKid") { }
#endif
    }
}
