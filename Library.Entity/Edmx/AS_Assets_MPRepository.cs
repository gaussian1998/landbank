using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Assets_MPRepository : EFRepository<AS_Assets_MP>, IAS_Assets_MPRepository
	{

	}

	public  interface IAS_Assets_MPRepository : IRepository<AS_Assets_MP>
	{

	}

   public  class AS_Assets_MP_Records : GenericAccessUnitOfWork<AS_Assets_MP>
	{

	}

}