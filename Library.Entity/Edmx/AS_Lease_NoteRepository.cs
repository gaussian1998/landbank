using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Lease_NoteRepository : EFRepository<AS_Lease_Note>, IAS_Lease_NoteRepository
	{

	}

	public  interface IAS_Lease_NoteRepository : IRepository<AS_Lease_Note>
	{

	}

   public  class AS_Lease_Note_Records : GenericAccessUnitOfWork<AS_Lease_Note>
	{

	}

}