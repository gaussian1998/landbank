using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Library.Entity.Edmx
{   
	public  class AS_Asset_CostRepository : EFRepository<AS_Asset_Cost>, IAS_Asset_CostRepository
	{

	}

	public  interface IAS_Asset_CostRepository : IRepository<AS_Asset_Cost>
	{

	}

   public  class AS_Asset_Cost_Records : GenericAccessUnitOfWork<AS_Asset_Cost>
	{

	}

}