﻿using System;
using Library.Entity.Edmx;
using Library.Servant.Servant.Email;
using Library.Servant.Servant.Email.Models;
using Library.Servant.Servants.AbstractFactory;


namespace EmailTester
{
    class Program
    {
        static void Main(string[] args)
        {
            int sender = AS_Users_Records.First( user => user.User_Name == "王興測").ID;
            int recver = AS_Users_Records.First(user => user.User_Name == "張家測").ID;

            var result = Servant.Send( new EmailModel {
                UserID = sender,
                RecverID = recver,
                Title = "測試",
                Body = "內容試送"
            } );

            Console.WriteLine( result.ToString() );
        }

        private static readonly IEmailServant Servant = ServantAbstractFactory.Email();
    }
}


