﻿/// <summary>
///  Vistor Pattern
///  適用場合:  需要根據不同型別採用不同動作,且滿足以下條件
///  
///  1. 型別彼此間無繼承關係
///  2. 型別彼此間有繼承關係,但專案已不容許在父類別或繼承介面中增加新函式
///  3. 想把某個分散在各型別的行為集中管理
/// </summary>
namespace Library.Common.Interface
{
    public interface IVistor<T1>
    {
        void visit(T1 accptor);
    }

    public interface IVistor<T1, T2> : IVistor<T2>
    {
        void visit(T1 accptor);
    }

    public interface IVistor<T1, T2, T3> : IVistor<T2, T3>
    {
        void visit(T1 accptor);
    }

    public interface IVistor<T1, T2, T3, T4> : IVistor<T2, T3, T4>
    {
        void visit(T1 accptor);
    }

    public interface IVistor<T1, T2, T3, T4, T5> : IVistor<T2, T3, T4, T5>
    {
        void visit(T1 accptor);
    }

    public interface IVistor<T1, T2, T3, T4, T5, T6> : IVistor<T2, T3, T4, T5, T6>
    {
        void visit(T1 accptor);
    }

    public interface IVistor<T1, T2, T3, T4, T5, T6, T7> : IVistor<T2, T3, T4, T5, T6, T7>
    {
        void visit(T1 accptort);
    }

    public interface IVistor<T1, T2, T3, T4, T5, T6, T7, T8> : IVistor<T2, T3, T4, T5, T6, T7, T8>
    {
        void visit(T1 accptor);
    }

    public interface IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9> : IVistor<T2, T3, T4, T5, T6, T7, T8, T9>
    {
        void visit(T1 accptor);
    }

    public interface IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> : IVistor<T2, T3, T4, T5, T6, T7, T8, T9, T10>
    {
        void visit(T1 accptor);
    }

    public interface IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> : IVistor<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>
    {
        void visit(T1 accptor);
    }

    public interface IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> : IVistor<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>
    {
        void visit(T1 accptor);
    }

    public interface IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> : IVistor<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>
    {
        void visit(T1 accptor);
    }

    public interface IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> : IVistor<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>
    {
        void visit(T1 accptor);
    }

    public interface IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> : IVistor<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>
    {
        void visit(T1 accptor);
    }

    public interface IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> : IVistor<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>
    {
        void visit(T1 accptor);
    }

    public interface IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17> : IVistor<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17>
    {
        void visit(T1 accptor);
    }

    public interface IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18> : IVistor<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18>
    {
        void visit(T1 accptor);
    }
}
