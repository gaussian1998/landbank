﻿using System.IO;

namespace Library.Interface
{
    public interface IStorageService
    {
        void Save(Stream stream, string filename);
        byte[] Load(string filename);
        void Remove(string filename);
    }
}
