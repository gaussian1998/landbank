﻿namespace Library.Common.Interface
{
    public interface IValidator
    {
        bool IsValid();
    }
}
