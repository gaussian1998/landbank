﻿namespace Library.Interface
{
    public interface IEncryption
    {
        string Encrypto(string source);
        string Decrypto(string source);
    }
}
