﻿using Library.Common.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Library.Common.Extension
{
    public static class IEnumerableExtension
    {
        public static PageList<T> Page<T, TKey>(this IEnumerable<T> cadidate, int Page, int PageSize, Func<T, TKey> keySelector)
        {
            int real_page = (Page - 1) >= 0 ? (Page - 1) : 0;
            return new PageList<T>
            {
                TotalAmount = cadidate.Count(),
                Items = cadidate.OrderBy(keySelector).Skip(real_page * PageSize).Take(PageSize).ToList()
            };
        }
    }
}
