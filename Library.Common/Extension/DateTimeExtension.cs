﻿using System;

namespace Library.Common.Extension
{
    public static class DateTimeExtension
    {
        public static string Description(this DateTime datetime)
        {
            return datetime.ToString("yyyy/MM/dd HH:mm:ss");
        }
        public static string Description(this DateTime? datetime)
        {
            return datetime.HasValue ? datetime.Value.Description() : "";
        }
        public static string ShortDescription(this DateTime datetime)
        {
            return datetime.ToString("yyyy/MM/dd");
        }
        public static string ShortDescription(this DateTime? datetime)
        {
            return datetime.HasValue ? datetime.Value.ShortDescription() : "";
        }

    }
}
