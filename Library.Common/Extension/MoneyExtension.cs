﻿
using System;

namespace Library.Common.Extension
{
    public static class MoneyExtension
    {
        public static string Money(this decimal value)
        {
            if (value == 0)
                return string.Empty;
            else
                return value.ToString("#,##0");
        }
    }
}
