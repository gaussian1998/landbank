﻿using Library.Common.Interface;
using System;

namespace Library.Common.Extension
{
    public static class ExceptionExtension
    {
        public static bool Accept<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(this Exception ex, IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> vistor)
            where T1 : Exception
            where T2 : Exception
            where T3 : Exception
            where T4 : Exception
            where T5 : Exception
            where T6 : Exception
            where T7 : Exception
            where T8 : Exception
            where T9 : Exception
            where T10 : Exception
            where T11 : Exception
            where T12 : Exception
            where T13 : Exception
            where T14 : Exception
            where T15 : Exception
            where T16 : Exception
        {
            if (ex is T1)
            {
                vistor.visit(ex as T1);
                return true;
            }
            return Accept<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(ex, vistor);
        }

        public static bool Accept<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(this Exception ex, IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> vistor)
            where T1 : Exception
            where T2 : Exception
            where T3 : Exception
            where T4 : Exception
            where T5 : Exception
            where T6 : Exception
            where T7 : Exception
            where T8 : Exception
            where T9 : Exception
            where T10 : Exception
            where T11 : Exception
            where T12 : Exception
            where T13 : Exception
            where T14 : Exception
            where T15 : Exception
        {
            if (ex is T1)
            {
                vistor.visit(ex as T1);
                return true;
            }
            return Accept<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(ex, vistor);
        }

        public static bool Accept<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(this Exception ex, IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> vistor)
            where T1 : Exception
            where T2 : Exception
            where T3 : Exception
            where T4 : Exception
            where T5 : Exception
            where T6 : Exception
            where T7 : Exception
            where T8 : Exception
            where T9 : Exception
            where T10 : Exception
            where T11 : Exception
            where T12 : Exception
            where T13 : Exception
            where T14 : Exception
        {
            if (ex is T1)
            {
                vistor.visit(ex as T1);
                return true;
            }
            return Accept<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(ex, vistor);
        }

        public static bool Accept<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(this Exception ex, IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> vistor)
            where T1 : Exception
            where T2 : Exception
            where T3 : Exception
            where T4 : Exception
            where T5 : Exception
            where T6 : Exception
            where T7 : Exception
            where T8 : Exception
            where T9 : Exception
            where T10 : Exception
            where T11 : Exception
            where T12 : Exception
            where T13 : Exception
        {
            if (ex is T1)
            {
                vistor.visit(ex as T1);
                return true;
            }
            return Accept<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(ex, vistor);
        }

        public static bool Accept<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(this Exception ex, IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> vistor)
            where T1 : Exception
            where T2 : Exception
            where T3 : Exception
            where T4 : Exception
            where T5 : Exception
            where T6 : Exception
            where T7 : Exception
            where T8 : Exception
            where T9 : Exception
            where T10 : Exception
            where T11 : Exception
            where T12 : Exception
        {
            if (ex is T1)
            {
                vistor.visit(ex as T1);
                return true;
            }
            return Accept<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(ex, vistor);
        }

        public static bool Accept<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(this Exception ex, IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> vistor)
            where T1 : Exception
            where T2 : Exception
            where T3 : Exception
            where T4 : Exception
            where T5 : Exception
            where T6 : Exception
            where T7 : Exception
            where T8 : Exception
            where T9 : Exception
            where T10 : Exception
            where T11 : Exception
        {
            if (ex is T1)
            {
                vistor.visit(ex as T1);
                return true;
            }

            return Accept<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(ex, vistor);
        }

        public static bool Accept<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(this Exception ex, IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> vistor)
            where T1 : Exception
            where T2 : Exception
            where T3 : Exception
            where T4 : Exception
            where T5 : Exception
            where T6 : Exception
            where T7 : Exception
            where T8 : Exception
            where T9 : Exception
            where T10 : Exception
        {
            if (ex is T1)
            {
                vistor.visit(ex as T1);
                return true;
            }

            return Accept<T2, T3, T4, T5, T6, T7, T8, T9, T10>(ex, vistor);
        }

        public static bool Accept<T1, T2, T3, T4, T5, T6, T7, T8, T9>(this Exception ex, IVistor<T1, T2, T3, T4, T5, T6, T7, T8, T9> vistor)
            where T1 : Exception
            where T2 : Exception
            where T3 : Exception
            where T4 : Exception
            where T5 : Exception
            where T6 : Exception
            where T7 : Exception
            where T8 : Exception
            where T9 : Exception
        {
            if (ex is T1)
            {
                vistor.visit(ex as T1);
                return true;
            }

            return Accept<T2, T3, T4, T5, T6, T7, T8, T9>(ex, vistor);
        }

        public static bool Accept<T1, T2, T3, T4, T5, T6, T7, T8>(this Exception ex, IVistor<T1, T2, T3, T4, T5, T6, T7, T8> vistor)
            where T1 : Exception
            where T2 : Exception
            where T3 : Exception
            where T4 : Exception
            where T5 : Exception
            where T6 : Exception
            where T7 : Exception
            where T8 : Exception
        {
            if (ex is T1)
            {
                vistor.visit(ex as T1);
                return true;
            }

            return Accept<T2, T3, T4, T5, T6, T7, T8>(ex, vistor);
        }

        public static bool Accept<T1, T2, T3, T4, T5, T6, T7>(this Exception ex, IVistor<T1, T2, T3, T4, T5, T6, T7> vistor)
            where T1 : Exception
            where T2 : Exception
            where T3 : Exception
            where T4 : Exception
            where T5 : Exception
            where T6 : Exception
            where T7 : Exception
        {
            if (ex is T1)
            {
                vistor.visit(ex as T1);
                return true;
            }

            return Accept<T2, T3, T4, T5, T6, T7>(ex, vistor);
        }

        public static bool Accept<T1, T2, T3, T4, T5, T6>(this Exception ex, IVistor<T1, T2, T3, T4, T5, T6> vistor)
            where T1 : Exception
            where T2 : Exception
            where T3 : Exception
            where T4 : Exception
            where T5 : Exception
            where T6 : Exception
        {
            if (ex is T1)
            {
                vistor.visit(ex as T1);
                return true;
            }

            return Accept<T2, T3, T4, T5, T6>(ex, vistor);
        }

        public static bool Accept<T1, T2, T3, T4, T5>(this Exception ex, IVistor<T1, T2, T3, T4, T5> vistor)
            where T1 : Exception
            where T2 : Exception
            where T3 : Exception
            where T4 : Exception
            where T5 : Exception
        {
            if (ex is T1)
            {
                vistor.visit(ex as T1);
                return true;
            }

            return Accept<T2, T3, T4, T5>(ex, vistor);
        }

        public static bool Accept<T1, T2, T3, T4>(this Exception ex, IVistor<T1, T2, T3, T4> vistor)
            where T1 : Exception
            where T2 : Exception
            where T3 : Exception
            where T4 : Exception
        {
            if (ex is T1)
            {
                vistor.visit(ex as T1);
                return true;
            }

            return Accept<T2, T3, T4>(ex, vistor);
        }

        public static bool Accept<T1, T2, T3>(this Exception ex, IVistor<T1, T2, T3> vistor)
            where T1 : Exception
            where T2 : Exception
            where T3 : Exception
        {
            if (ex is T1)
            {
                vistor.visit(ex as T1);
                return true;
            }

            return Accept<T2, T3>(ex, vistor);
        }

        public static bool Accept<T1, T2>(this Exception ex, IVistor<T1, T2> vistor)
            where T1 : Exception
            where T2 : Exception
        {
            if (ex is T1)
            {
                vistor.visit(ex as T1);
                return true;
            }

            return Accept<T2>(ex, vistor);
        }

        public static bool Accept<T1>(this Exception ex, IVistor<T1> vistor)
            where T1 : Exception
        {
            if (ex is T1)
            {
                vistor.visit(ex as T1);
                return true;
            }

            return false;
        }
    }
}
