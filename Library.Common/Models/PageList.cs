﻿using System;
using System.Collections.Generic;

namespace Library.Common.Models
{
    public class PageList<T>
    {
        public int TotalAmount { get; set; }
        public List<T> Items { get; set; }
    }
}
