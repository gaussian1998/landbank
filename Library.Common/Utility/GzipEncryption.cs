﻿using System;
using Library.Interface;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace Library.Common.Utility
{
    public class GzipEncryption : IEncryption
    {
        public string Decrypto(string source)
        {
            byte[] bytIn = Convert.FromBase64String(source);
            using (MemoryStream ms = new MemoryStream(bytIn, 0, bytIn.Length))
            {
                using (GZipStream gzip = new GZipStream(ms, CompressionMode.Decompress))
                {
                    using (StreamReader sr = new StreamReader(gzip,Encoding.UTF8))
                    {
                        return sr.ReadToEnd();
                    }
                }
            }
        }

        public string Encrypto(string source)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                byte[] bytIn = UTF8Encoding.UTF8.GetBytes(source);
                using (GZipStream gzip = new GZipStream(ms, CompressionMode.Compress))
                {
                    gzip.Write(bytIn, 0, bytIn.Length);
                }
                return Convert.ToBase64String(ms.ToArray());
            }
        }
    }
}
