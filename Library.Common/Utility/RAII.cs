﻿using System;

namespace Library.Common.Utility
{
    public static class RAII
    {
        public static void Using<T>(Action action)
            where T : IDisposable, new()
        {
            using (T t = new T())
            {
                action();
            }
        }

        public static void Using<T1, T2>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2>(action);
            }
        }

        public static void Using<T1, T2, T3>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3>(action);
            }
        }

        public static void Using<T1, T2, T3,T4>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3,T4>(action);
            }
        }

        public static void Using<T1, T2, T3, T4,T5>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4,T5>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5,T6>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5,T6>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5, T6, T7>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
            where T7 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5, T6,T7>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5, T6, T7,T8>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
            where T7 : IDisposable, new()
            where T8 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5, T6, T7,T8>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5, T6, T7, T8,T9>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
            where T7 : IDisposable, new()
            where T8 : IDisposable, new()
            where T9 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5, T6, T7, T8, T9>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
            where T7 : IDisposable, new()
            where T8 : IDisposable, new()
            where T9 : IDisposable, new()
            where T10 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5, T6, T7, T8, T9, T10>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
            where T7 : IDisposable, new()
            where T8 : IDisposable, new()
            where T9 : IDisposable, new()
            where T10 : IDisposable, new()
            where T11 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
            where T7 : IDisposable, new()
            where T8 : IDisposable, new()
            where T9 : IDisposable, new()
            where T10 : IDisposable, new()
            where T11 : IDisposable, new()
            where T12 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
            where T7 : IDisposable, new()
            where T8 : IDisposable, new()
            where T9 : IDisposable, new()
            where T10 : IDisposable, new()
            where T11 : IDisposable, new()
            where T12 : IDisposable, new()
            where T13 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
            where T7 : IDisposable, new()
            where T8 : IDisposable, new()
            where T9 : IDisposable, new()
            where T10 : IDisposable, new()
            where T11 : IDisposable, new()
            where T12 : IDisposable, new()
            where T13 : IDisposable, new()
            where T14 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
            where T7 : IDisposable, new()
            where T8 : IDisposable, new()
            where T9 : IDisposable, new()
            where T10 : IDisposable, new()
            where T11 : IDisposable, new()
            where T12 : IDisposable, new()
            where T13 : IDisposable, new()
            where T14 : IDisposable, new()
            where T15 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
            where T7 : IDisposable, new()
            where T8 : IDisposable, new()
            where T9 : IDisposable, new()
            where T10 : IDisposable, new()
            where T11 : IDisposable, new()
            where T12 : IDisposable, new()
            where T13 : IDisposable, new()
            where T14 : IDisposable, new()
            where T15 : IDisposable, new()
            where T16 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
            where T7 : IDisposable, new()
            where T8 : IDisposable, new()
            where T9 : IDisposable, new()
            where T10 : IDisposable, new()
            where T11 : IDisposable, new()
            where T12 : IDisposable, new()
            where T13 : IDisposable, new()
            where T14 : IDisposable, new()
            where T15 : IDisposable, new()
            where T16 : IDisposable, new()
            where T17 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
            where T7 : IDisposable, new()
            where T8 : IDisposable, new()
            where T9 : IDisposable, new()
            where T10 : IDisposable, new()
            where T11 : IDisposable, new()
            where T12 : IDisposable, new()
            where T13 : IDisposable, new()
            where T14 : IDisposable, new()
            where T15 : IDisposable, new()
            where T16 : IDisposable, new()
            where T17 : IDisposable, new()
            where T18 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
            where T7 : IDisposable, new()
            where T8 : IDisposable, new()
            where T9 : IDisposable, new()
            where T10 : IDisposable, new()
            where T11 : IDisposable, new()
            where T12 : IDisposable, new()
            where T13 : IDisposable, new()
            where T14 : IDisposable, new()
            where T15 : IDisposable, new()
            where T16 : IDisposable, new()
            where T17 : IDisposable, new()
            where T18 : IDisposable, new()
            where T19 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
            where T7 : IDisposable, new()
            where T8 : IDisposable, new()
            where T9 : IDisposable, new()
            where T10 : IDisposable, new()
            where T11 : IDisposable, new()
            where T12 : IDisposable, new()
            where T13 : IDisposable, new()
            where T14 : IDisposable, new()
            where T15 : IDisposable, new()
            where T16 : IDisposable, new()
            where T17 : IDisposable, new()
            where T18 : IDisposable, new()
            where T19 : IDisposable, new()
            where T20 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
            where T7 : IDisposable, new()
            where T8 : IDisposable, new()
            where T9 : IDisposable, new()
            where T10 : IDisposable, new()
            where T11 : IDisposable, new()
            where T12 : IDisposable, new()
            where T13 : IDisposable, new()
            where T14 : IDisposable, new()
            where T15 : IDisposable, new()
            where T16 : IDisposable, new()
            where T17 : IDisposable, new()
            where T18 : IDisposable, new()
            where T19 : IDisposable, new()
            where T20 : IDisposable, new()
            where T21 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21>(action);
            }
        }

        public static void Using<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22>(Action action)
            where T1 : IDisposable, new()
            where T2 : IDisposable, new()
            where T3 : IDisposable, new()
            where T4 : IDisposable, new()
            where T5 : IDisposable, new()
            where T6 : IDisposable, new()
            where T7 : IDisposable, new()
            where T8 : IDisposable, new()
            where T9 : IDisposable, new()
            where T10 : IDisposable, new()
            where T11 : IDisposable, new()
            where T12 : IDisposable, new()
            where T13 : IDisposable, new()
            where T14 : IDisposable, new()
            where T15 : IDisposable, new()
            where T16 : IDisposable, new()
            where T17 : IDisposable, new()
            where T18 : IDisposable, new()
            where T19 : IDisposable, new()
            where T20 : IDisposable, new()
            where T21 : IDisposable, new()
            where T22 : IDisposable, new()
        {
            using (T1 t = new T1())
            {
                Using<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22>(action);
            }
        }
    }
}
