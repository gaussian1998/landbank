﻿using System;

namespace Library.Common.Utility
{
    public interface IRunnable
    {
        void run();
    }

    public static class TestUtility
    {
        public static void AssertTrue(bool condition, string errorMessage)
        {
            if (!condition)
                throw new Exception(errorMessage);
        }

        public static void AssertFalse(bool condition, string errorMessage)
        {
            AssertTrue( !condition, errorMessage);
        }
    }

    public static class TestUtility<T>
    {
        public static void AssertTrue(bool condition, int count)
        {
            if (!condition)
                throw new Exception( "Test " + typeof(T).Name + " false " + "#" + count + "#" );
        }

        public static void AssertFalse(bool condition, int count)
        {
            AssertTrue(!condition, count);
        }

        public static void AssertExceptionHappen(Action action , int count)
        {
            try
            {
                action();
            }
            catch
            {
                return;
            }
            throw new Exception("Test " + typeof(T).Name + " must throw exception " + "#" + count + "#");
        }

        public static void AssertNoException(Action action, int count)
        {
            try
            {
                action();
            }
            catch
            {
                throw new Exception("Test " + typeof(T).Name + " can not throw exception " + "#" + count + "#");
            }
        }
    }

    public static class TestSuit
    {
        public static void Run<T1>()
            where T1 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
        }

        public static void Run<T1, T2>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(  typeof(T1).Name + " Test OK"  );
            }
            Run<T2>();
        }

        public static void Run<T1, T2, T3>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3>();
        }
        public static void Run<T1, T2, T3, T4>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4>();
        }

        public static void Run<T1, T2, T3, T4, T5>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5>();
        }

        public static void Run<T1, T2, T3, T4, T5,T6>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6>();
        }

        public static void Run<T1, T2, T3, T4, T5, T6,T7>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7>();
        }

        public static void Run<T1, T2, T3, T4, T5, T6, T7,T8>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8>();
        }

        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9>();
        }

        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9,T10>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10>();
        }

        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
            where T11 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>();
        }

        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11,T12>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
            where T11 : IDisposable, IRunnable, new()
            where T12 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11,T12>();
        }

        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
            where T11 : IDisposable, IRunnable, new()
            where T12 : IDisposable, IRunnable, new()
            where T13 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>();
        }

        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
            where T11 : IDisposable, IRunnable, new()
            where T12 : IDisposable, IRunnable, new()
            where T13 : IDisposable, IRunnable, new()
            where T14 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>();
        }

        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
            where T11 : IDisposable, IRunnable, new()
            where T12 : IDisposable, IRunnable, new()
            where T13 : IDisposable, IRunnable, new()
            where T14 : IDisposable, IRunnable, new()
            where T15 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>();
        }

        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
            where T11 : IDisposable, IRunnable, new()
            where T12 : IDisposable, IRunnable, new()
            where T13 : IDisposable, IRunnable, new()
            where T14 : IDisposable, IRunnable, new()
            where T15 : IDisposable, IRunnable, new()
            where T16 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>();
        }

        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
            where T11 : IDisposable, IRunnable, new()
            where T12 : IDisposable, IRunnable, new()
            where T13 : IDisposable, IRunnable, new()
            where T14 : IDisposable, IRunnable, new()
            where T15 : IDisposable, IRunnable, new()
            where T16 : IDisposable, IRunnable, new()
            where T17 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17>();
        }

        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
            where T11 : IDisposable, IRunnable, new()
            where T12 : IDisposable, IRunnable, new()
            where T13 : IDisposable, IRunnable, new()
            where T14 : IDisposable, IRunnable, new()
            where T15 : IDisposable, IRunnable, new()
            where T16 : IDisposable, IRunnable, new()
            where T17 : IDisposable, IRunnable, new()
            where T18 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18>();
        }


        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
            where T11 : IDisposable, IRunnable, new()
            where T12 : IDisposable, IRunnable, new()
            where T13 : IDisposable, IRunnable, new()
            where T14 : IDisposable, IRunnable, new()
            where T15 : IDisposable, IRunnable, new()
            where T16 : IDisposable, IRunnable, new()
            where T17 : IDisposable, IRunnable, new()
            where T18 : IDisposable, IRunnable, new()
            where T19 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19>();
        }


        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
            where T11 : IDisposable, IRunnable, new()
            where T12 : IDisposable, IRunnable, new()
            where T13 : IDisposable, IRunnable, new()
            where T14 : IDisposable, IRunnable, new()
            where T15 : IDisposable, IRunnable, new()
            where T16 : IDisposable, IRunnable, new()
            where T17 : IDisposable, IRunnable, new()
            where T18 : IDisposable, IRunnable, new()
            where T19 : IDisposable, IRunnable, new()
            where T20 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20>();
        }


        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
            where T11 : IDisposable, IRunnable, new()
            where T12 : IDisposable, IRunnable, new()
            where T13 : IDisposable, IRunnable, new()
            where T14 : IDisposable, IRunnable, new()
            where T15 : IDisposable, IRunnable, new()
            where T16 : IDisposable, IRunnable, new()
            where T17 : IDisposable, IRunnable, new()
            where T18 : IDisposable, IRunnable, new()
            where T19 : IDisposable, IRunnable, new()
            where T20 : IDisposable, IRunnable, new()
            where T21 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21>();
        }

        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
            where T11 : IDisposable, IRunnable, new()
            where T12 : IDisposable, IRunnable, new()
            where T13 : IDisposable, IRunnable, new()
            where T14 : IDisposable, IRunnable, new()
            where T15 : IDisposable, IRunnable, new()
            where T16 : IDisposable, IRunnable, new()
            where T17 : IDisposable, IRunnable, new()
            where T18 : IDisposable, IRunnable, new()
            where T19 : IDisposable, IRunnable, new()
            where T20 : IDisposable, IRunnable, new()
            where T21 : IDisposable, IRunnable, new()
            where T22 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22>();
        }

        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
            where T11 : IDisposable, IRunnable, new()
            where T12 : IDisposable, IRunnable, new()
            where T13 : IDisposable, IRunnable, new()
            where T14 : IDisposable, IRunnable, new()
            where T15 : IDisposable, IRunnable, new()
            where T16 : IDisposable, IRunnable, new()
            where T17 : IDisposable, IRunnable, new()
            where T18 : IDisposable, IRunnable, new()
            where T19 : IDisposable, IRunnable, new()
            where T20 : IDisposable, IRunnable, new()
            where T21 : IDisposable, IRunnable, new()
            where T22 : IDisposable, IRunnable, new()
            where T23 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23>();
        }


        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
            where T11 : IDisposable, IRunnable, new()
            where T12 : IDisposable, IRunnable, new()
            where T13 : IDisposable, IRunnable, new()
            where T14 : IDisposable, IRunnable, new()
            where T15 : IDisposable, IRunnable, new()
            where T16 : IDisposable, IRunnable, new()
            where T17 : IDisposable, IRunnable, new()
            where T18 : IDisposable, IRunnable, new()
            where T19 : IDisposable, IRunnable, new()
            where T20 : IDisposable, IRunnable, new()
            where T21 : IDisposable, IRunnable, new()
            where T22 : IDisposable, IRunnable, new()
            where T23 : IDisposable, IRunnable, new()
            where T24 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24>();
        }

        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
            where T11 : IDisposable, IRunnable, new()
            where T12 : IDisposable, IRunnable, new()
            where T13 : IDisposable, IRunnable, new()
            where T14 : IDisposable, IRunnable, new()
            where T15 : IDisposable, IRunnable, new()
            where T16 : IDisposable, IRunnable, new()
            where T17 : IDisposable, IRunnable, new()
            where T18 : IDisposable, IRunnable, new()
            where T19 : IDisposable, IRunnable, new()
            where T20 : IDisposable, IRunnable, new()
            where T21 : IDisposable, IRunnable, new()
            where T22 : IDisposable, IRunnable, new()
            where T23 : IDisposable, IRunnable, new()
            where T24 : IDisposable, IRunnable, new()
            where T25 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25>();
        }

        public static void Run<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25, T26>()
            where T1 : IDisposable, IRunnable, new()
            where T2 : IDisposable, IRunnable, new()
            where T3 : IDisposable, IRunnable, new()
            where T4 : IDisposable, IRunnable, new()
            where T5 : IDisposable, IRunnable, new()
            where T6 : IDisposable, IRunnable, new()
            where T7 : IDisposable, IRunnable, new()
            where T8 : IDisposable, IRunnable, new()
            where T9 : IDisposable, IRunnable, new()
            where T10 : IDisposable, IRunnable, new()
            where T11 : IDisposable, IRunnable, new()
            where T12 : IDisposable, IRunnable, new()
            where T13 : IDisposable, IRunnable, new()
            where T14 : IDisposable, IRunnable, new()
            where T15 : IDisposable, IRunnable, new()
            where T16 : IDisposable, IRunnable, new()
            where T17 : IDisposable, IRunnable, new()
            where T18 : IDisposable, IRunnable, new()
            where T19 : IDisposable, IRunnable, new()
            where T20 : IDisposable, IRunnable, new()
            where T21 : IDisposable, IRunnable, new()
            where T22 : IDisposable, IRunnable, new()
            where T23 : IDisposable, IRunnable, new()
            where T24 : IDisposable, IRunnable, new()
            where T25 : IDisposable, IRunnable, new()
            where T26 : IDisposable, IRunnable, new()
        {
            using (T1 t = new T1())
            {
                Console.WriteLine(typeof(T1).Name + " Test Start");
                t.run();
                Console.WriteLine(typeof(T1).Name + " Test OK");
            }
            Run<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25, T26>();
        }


    }


}
