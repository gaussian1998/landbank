﻿using Library.Interface;
using System;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Text;

namespace Library.Utility
{
    public class SimpleEncryption : IEncryption
    {
        public SimpleEncryption(string Key, string IV)
        {
            this.Key = Key;
            this.IV = IV;
        }

        public string Encrypto(string source)
        {
            using( SymmetricAlgorithm algorithm = Algorithm())
            {
                using (EncryptoMethod encrypto = new EncryptoMethod(algorithm))
                {
                    return encrypto.Excute(source);
                }
            }
        }

        public string Decrypto(string source)
        {
            using (SymmetricAlgorithm algorithm = Algorithm())
            {
                using (DecryptoMethod decrypto = new DecryptoMethod(source, algorithm))
                {
                    return decrypto.Excute();
                }
            }
        }

        private  SymmetricAlgorithm Algorithm()
        {
            SymmetricAlgorithm result = new RijndaelManaged();
            result.GenerateKey();
            result.GenerateIV();
            result.Key = ASCIIEncoding.ASCII.GetBytes(MatchLength(Key, result.Key.Length));
            result.IV = ASCIIEncoding.ASCII.GetBytes(MatchLength(IV, result.IV.Length));

            return result;
        }

        private static string MatchLength(string source, int length)
        {
            if (source.Length > length)
                return source.Substring(0, length);
            else
                return source.PadRight(length);
        }

        private readonly string Key;
        private readonly string IV;
    }

    /// <summary>
    ///  壓縮 -> 加密 -> 加鹽
    /// </summary>
    class EncryptoMethod : IDisposable
    {
        public EncryptoMethod(SymmetricAlgorithm algorithm)
        {
            ms = new MemoryStream();
            gzip = new GZipStream(ms, CompressionMode.Compress);
            cs = new CryptoStream(gzip, algorithm.CreateEncryptor(), CryptoStreamMode.Write);
        }

        public void Dispose()
        {
            cs.Dispose();
            gzip.Dispose();
            ms.Dispose();
        }

        public string Excute(string source)
        {
            byte[] bytIn = UTF8Encoding.UTF8.GetBytes(source.Salt());
            cs.Write(bytIn, 0, bytIn.Length);
            cs.Dispose();

            return Convert.ToBase64String(ms.ToArray());
        }

        private readonly MemoryStream ms;
        private readonly GZipStream gzip;
        private readonly CryptoStream cs;
    }

    /// <summary>
    ///  解鹽 -> 解壓 -> 解密
    /// </summary>
    class DecryptoMethod : IDisposable
    {
        public DecryptoMethod(string source, SymmetricAlgorithm algorithm)
        {
            byte[] bytIn = Convert.FromBase64String(source);
            ms = new MemoryStream(bytIn, 0, bytIn.Length);
            gzip = new GZipStream(ms, CompressionMode.Decompress);
            cs = new CryptoStream(gzip, algorithm.CreateDecryptor(), CryptoStreamMode.Read);
            sr = new StreamReader(cs, Encoding.UTF8);
        }

        public void Dispose()
        {
            sr.Dispose();
            cs.Dispose();
            gzip.Dispose();
            ms.Dispose();
        }

        public string Excute()
        {
            return sr.ReadToEnd().UnSalt();
        }

        private readonly MemoryStream ms;
        private readonly GZipStream gzip;
        private readonly CryptoStream cs;
        private readonly StreamReader sr;
    }



    static class StringExtension
    {
        public static string Reverse(this string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        public static string UnSalt(this string salt)
        {
            return salt.Substring(Guid.NewGuid().ToString().Length).Reverse();
        }

        public static string Salt(this string source)
        {
            return Guid.NewGuid().ToString() + source.Reverse();
        }
    }
}
