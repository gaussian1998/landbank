﻿using System;
using System.Linq.Expressions;

namespace Library.Common.Utility
{
    public static class ExpressionMaker
    {
        public static Expression<Func<T1, T2>> Make<T1,T2>(Expression<Func<T1, T2>> expression)
        {
            return expression;
        }

        public static Func<T1, T2> MakeF<T1, T2>(Func<T1, T2> expression)
        {
            return expression;
        }
    }
}
