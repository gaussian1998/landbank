﻿using System;
using Library.Interface;

namespace Library.Common.Utility
{
    public class NoEncryption : IEncryption
    {
        public string Decrypto(string source)
        {
            return source;
        }

        public string Encrypto(string source)
        {
            return source;
        }
    }
}
