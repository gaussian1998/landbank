﻿namespace Library.Utility
{
    public class Optional<T>
    {
        private bool Valid { get; set; }
        public T Value { get; set; }
        public string ErrorMessage { get; set; }
        public int ErrorCode { get; set; }

        public static Optional<T> Ok(T value)
        {
            return new Optional<T> { Valid = true, Value = value, ErrorMessage = string.Empty, ErrorCode = int.MinValue };
        }

        public static Optional<T> Error(string errorMessage, int ErrorCode = int.MinValue)
        {
            return new Optional<T> { Valid = false, Value = default(T), ErrorMessage = errorMessage, ErrorCode = ErrorCode };
        }

        public static implicit operator bool(Optional<T> other)
        {
            return other.Valid;
        }

        public bool HasErrorCode()
        {
            return ErrorCode >= 0;
        }
    }

    public static class Optional
    {
        public static Optional<T> Ok<T>(T value)
        {
            return Optional<T>.Ok(value);
        }
    }
}
