﻿using Library.Common.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Library.Utility
{
    public static class MapProperty
    {
        public static To Mapping<To, From>(From from, Action<To> _override)
            where To : new()
        {
            To to = new To();

            MappingHelper(to, from);
            _override(to);
            return to;
        }

        public static To Mapping<To, From>(From from)
            where To : new()
        {
            return Mapping<To, From>(from, to => { });
        }

        public static IEnumerable<To> MapAll<To, From>(IEnumerable<From> froms)
            where To : new()
        {
            return froms.Select(from => Mapping<To, From>(from));
        }

        public static IEnumerable<To> MapAll<To, From>(IEnumerable<From> froms, Func<From,To> select)
            where To : new()
        {
            return froms.Select( from => select(from) );
        }

        public static To OverrideBy<To, From>(this To to, From from)
        {
            MappingHelper(to, from);
            return to;
        }

        public static void OverrideBy<To, From>(this To to, From from, Action _override)
        {
            MappingHelper(to, from);
            _override();
        }

        public static PageList<To>  Mapping<To,From>(this PageList<From> from)
            where To : new()
        {
            return new PageList<To> {

                Items = MapAll<To, From>( from.Items ).ToList(),
                TotalAmount = from.TotalAmount
            };
        }

        public static T Clone<T>(T obj)
            where T : new()
        {
            return MapProperty.Mapping<T, T>(obj);
        }

        public static IDictionary<string, object> ToDictionary<T>(this T obj)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            foreach (PropertyInfo property in typeof(T).GetProperties())
                result[property.Name] = property.GetValue(obj);

            return result;
        }
        
        public static DataTable ToDataTable<T>(string tableName, List<T> list)
        {
            DataTable result = new DataTable { TableName = tableName };

            foreach (PropertyInfo property in typeof(T).GetProperties())
                result.Columns.Add(property.Name, property.PropertyType);

            foreach (var data in list)
                result.Rows.Add( Values(data) );

            return result;
        }

        public static DataSet ToDataSet<T>(string tableName, List<T> list)
        {
            DataSet source = new DataSet();
            source.Tables.Add(ToDataTable(tableName, list));

            return source;
        }

        public static DataSet ToDataSet<T>(List<T> list)
        {
            return ToDataSet<T>(typeof(T).Name, list);
        }

        public static IEnumerable<string> PropertyNames<T>(this T obj)
        {
            return from property in typeof(T).GetProperties()
                   select property.Name;
        }

        public static void SetProperty<T, Value>(this T obj, string name, Value value)
        {
            foreach (PropertyInfo property in typeof(T).GetProperties())
            {
                if (property.Name == name && property.PropertyType == typeof(Value))
                    property.SetValue(obj, value);
            }
        }


        private static void MappingHelper<To, From>(To to, From from)
        {
            foreach (PropertyInfo fromProperty in typeof(From).GetProperties())
            {
                foreach (PropertyInfo toProperty in typeof(To).GetProperties())
                {
                    if (IsMappingPermit(toProperty, fromProperty))
                        toProperty.SetValue(to, fromProperty.GetValue(from));
                }
            }
        }

        private static bool IsMappingPermit(PropertyInfo toProperty, PropertyInfo fromProperty)
        {
            return fromProperty.Name == toProperty.Name &&
                fromProperty.PropertyType == toProperty.PropertyType &&
                toProperty.SetMethod != null &&
                fromProperty.GetMethod != null;
        }

        private static object[] Values<T>(T data)
        {
            List<object> result = new List<object>();
            foreach (PropertyInfo property in typeof(T).GetProperties())
                result.Add(property.GetValue(data));

            return result.ToArray();
        }
    }
}
