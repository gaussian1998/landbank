﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    class AS_Flow_Open_Log_Data : IDisposable
    {
        public AS_Flow_Open_Log_Data()
        {
            AS_Flow_Open_Log_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Flow_Open_Log_Data");
        }
    }
}
