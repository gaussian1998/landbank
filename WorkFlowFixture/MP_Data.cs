﻿
using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class MP_Data : IDisposable
    {
        public MP_Data()
        {
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
DELETE FROM MP WHERE 1=1;
");
            }
        }

        public void Dispose()
        {
            Console.WriteLine("MP_Data");
            using (var context = new AS_LandBankEntities())
            {
                //context.Database.ExecuteSqlCommand(BulkCommand());
            }
        }

        private static string BulkCommand()
        {
            return string.Format(@"
BULK INSERT dbo.MP
FROM '{0}\..\..\bcp\MP.bcp'
WITH
(
    BATCHSIZE = 1000,
    DATAFILETYPE = 'native',
    TABLOCK
)", System.Environment.CurrentDirectory);
        }
    }
}