﻿
using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Life_table_Data : IDisposable
    {
        public AS_Life_table_Data()
        {
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
DELETE FROM AS_Life_table WHERE 1=1;
");
            }
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Life_table_Data");
            using (var context = new AS_LandBankEntities())
            {

            }
        }

        private static string BulkCommand()
        {
            return string.Format(@"
BULK INSERT dbo.AS_Life_table
FROM '{0}\..\..\bcp\AS_Life_table.bcp'
WITH
(
    BATCHSIZE = 1000,
    DATAFILETYPE = 'native',
    TABLOCK
)", System.Environment.CurrentDirectory);
        }
    }
}