﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Programs_Roles_Data : IDisposable
    {
        public AS_Programs_Roles_Data()
        {
            AS_Programs_Roles_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("CreateProgramRoles");

            AS_Programs_Records.Update(m => true, entity => {

                entity.AS_Programs_Roles.Add(new AS_Programs_Roles { Role_ID = DataContext.login_role_sa });
            });
        }
    }
}
