﻿using Library.Common.Utility;
using Library.Entity.Edmx;

namespace WorkFlowFixture
{
    /// <summary>
    /// 
    ///   Base:
    ///   ####################
    ///   AS_Rate : nil
    ///   AS_Branch_Currency : nil
    ///   AS_Currency : nil
    ///   AS_GL_Account : nil
    ///   AS_GL_Trade_Define : nil
    ///   AS_GL_Acc_Months : nil
    ///   AS_TRX_Headers : nil
    ///   AS_CodeTable : nil
    ///   AS_Branch : nil
    ///   AS_Flow_Name : nil
    ///   AS_Programs : nil
    ///   AS_Role : nil
    ///   Acc_EntriesByHeader : nil
    ///   Acc_EntriesByAccounting : nil
    ///   AS_Asset_Category_Main_Kinds : nil
    ///   AS_Asset_Category_Use_Types : nil
    ///   
    ///   
    ///   Dependent Level 1:
    ///   ####################
    ///   AS_Users : AS_Code_Table
    ///   AS_FlowTemplate: AS_Flow_Name AS_CodeTable
    ///   AS_Programs_Roles : AS_Role AS_Programs
    ///   AS_Doc_Name: AS_CodeTable
    ///   AS_Asset_Category_Detail_Kinds : AS_Asset_Category_Main_Kinds 
    ///   
    ///   Dependent Level 2:
    ///   ####################
    ///   AS_Post_Header : AS_Users
    ///   AS_Agent_Flow: AS_Doc_Name AS_FlowTemplate
    ///   AS_Flow_Template_Flow_Role : AS_CodeTable AS_FlowTemplate
    ///   AS_Asset_Category : AS_Asset_Category_Detail_Kinds  AS_Asset_Category_Use_Types
    ///   AS_History_Parameter_Data : AS_Users
    /// 
    ///   Dependent Level 3:
    ///   ####################
    ///   AS_UsersFlowRole : AS_Users  AS_CodeTable
    ///   AS_UserRoles : AS_Users AS_Role
    ///   AS_Flow_Open : AS_Agent_Flow
    ///   AS_Operation_His : AS_Users AS_Department AS_Branch
    ///   AS_Role_Flow : AS_Users
    ///   
    ///    Dependent Level 4:
    ///   ####################
    ///   AS_Flow_Open_Log : AS_Flow_Open AS_CodeTable AS_Users
    /// 
    /// </summary>
    /// 

    class DepartmentDAO
    {
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string Department { get; set; }
        public string DepartmentName { get; set; }
    }

    class DataContext
    {
        static void Main(string[] args)
        {
            Dependent_Level_4();
        }

        static void Base()
        {
            RAII.Using<
                AS_Deprn_Data,
                AS_Life_table_Data,
                AS_Rate_Data,
                AS_Role_Data,
                AS_Flow_Name_Data,
                AS_Programs_Data,
                AS_CodeTable_Data,
                AS_Branch_Currency_Data,
                AS_Currency_Data,
                AS_Keep_Position_Data,
                AS_Asset_Category_Main_Kinds_Data,
                AS_Asset_Category_Use_Types_Data
                >(() => { });

            RAII.Using<AS_GL_Acc_Deprn_Data,
                AS_GL_Acc_Detail_Data,
                AS_GL_Acc_Months_Data,
                AS_GL_Account_Data,
                AS_GL_Calendar_Data,
                AS_GL_Trade_Acc_Sub_Data,
                AS_GL_Trade_Account_Data,
                AS_GL_Trade_Define_Data>(() => { });

            RAII.Using<MP_Data,
                AS_Land_Code_Data,
                AS_Assets_Build_Data,
                AS_Assets_Build_Detail_Data,
                AS_Assets_Build_Land_Data,
                AS_Assets_Land_Data>(() => { });
        }

        static void Dependent_Level_1()
        {
            RAII.Using<
                AS_Users_Data,
                AS_Flow_Template_Data,
                AS_Doc_Name_Data,
                AS_Programs_Roles_Data,
                AS_Asset_Category_Detail_Kinds_Data>(() => Base());
        }

        static void Dependent_Level_2()
        {
            RAII.Using<
                AS_Agent_Flow_Data,
                AS_Flow_Template_Flow_Role_Data,
                AS_Asset_Category_Data,
                AS_Post_Header_Data,
                AS_History_Parameter_Data>(() => Dependent_Level_1());
        }

        static void Dependent_Level_3()
        {
            RAII.Using<
                AS_Users_FlowRole_Data,
                AS_Users_Role_Data,
                AS_Flow_Open_Data,
                AS_Operation_His_Data,
                AS_Role_Flow_Data>(() => Dependent_Level_2());
        }

        static void Dependent_Level_4()
        {
            RAII.Using<AS_Flow_Open_Log_Data>(() => Dependent_Level_3());
        }

        public static int login_role_sa;

        public static int flow_role_user_login_1;
        public static int flow_role_user_login_2;
        public static int flow_role_user_login_3;
        public static int flow_role_user_login_4;

        public static int flow_name_user_login_role;

        public static AS_Code_Table DocTypes4;
        public static AS_Code_Table DocTypes5;

        public static int flow_template_user_login_role;

        public static int DocNameUserLoginRole_ADD;
    }
}




