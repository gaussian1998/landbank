﻿using Library.Entity.Edmx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowFixture
{
    public class AS_Programs_Data : IDisposable
    {
        public AS_Programs_Data()
        {
            AS_Programs_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Programs_Records");

            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LoginRole", Programs_Number = "T001", Programs_Name = "角色新建及維護" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "FunctionRole", Programs_Number = "T002", Programs_Name = "程式功能維護" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "UserRole", Programs_Number = "T100", Programs_Name = "權限申請作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "UserConfig", Programs_Number = "T030", Programs_Name = "人員權限設定" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "UserHistory", Programs_Number = "T300", Programs_Name = "人員使用紀錄查詢" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "Report", Programs_Number = "T900", Programs_Name = "報表產製" });

            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "ApprovalRole", Programs_Number = "T003", Programs_Name = "簽核角色維護" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "ApprovalMember", Programs_Number = "T004", Programs_Name = "設定人員簽核角色" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "WorkFlow", Programs_Number = "T005", Programs_Name = "設定流程名稱" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "DocName", Programs_Number = "T006", Programs_Name = "表單名稱維護" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AgentFlow", Programs_Number = "T007", Programs_Name = "設定表單簽核流程" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "ApprovalTracking", Programs_Number = "T008", Programs_Name = "簽核查詢追蹤" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "ApprovalFlow", Programs_Number = "T009", Programs_Name = "簽核查詢" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "ApprovalReassign", Programs_Number = "T010", Programs_Name = "簽核改派" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "ApprovalRevoke", Programs_Number = "T011", Programs_Name = "簽核撤銷結案" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "CaseApproval", Programs_Number = "T012", Programs_Name = "案件簽核" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "Reports", Programs_Number = "T013", Programs_Name = "各類案件統計" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "ApprovalWithdraw", Programs_Number = "T014", Programs_Name = "簽核經辦抽回" });

            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LandAdded", Programs_Number = "B010", Programs_Name = "土地建檔" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LandAssets", Programs_Number = "B020", Programs_Name = "土地資產維護" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LandCost", Programs_Number = "B030", Programs_Name = "土地成本維護" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LandSplit", Programs_Number = "B040", Programs_Name = "土地分割" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LandConsolidation", Programs_Number = "B050", Programs_Name = "土地合併" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LandReTest", Programs_Number = "T019", Programs_Name = "土地重測" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LandReplotting", Programs_Number = "T020", Programs_Name = "土地重劃" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LandDisposal", Programs_Number = "B080", Programs_Name = "土地處分" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LandReclassify", Programs_Number = "B081", Programs_Name = "土地資產分類異動" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LandReport", Programs_Number = "B100", Programs_Name = "土地報表" });

            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "PaymentForm", Programs_Number = "T022", Programs_Name = "出租契約履約管理" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "Estate", Programs_Number = "T023", Programs_Name = "表頭列表頁" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "QueryPosts", Programs_Number = "T024", Programs_Name = "公告事項查詢" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "PostsManagement", Programs_Number = "T025", Programs_Name = "公告事項維護" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "HistoryData", Programs_Number = "T026", Programs_Name = "歷史資料列表" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "HistoryDataStatist", Programs_Number = "T027", Programs_Name = "歷史資料統計" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AccountBook", Programs_Number = "T028", Programs_Name = "帳本管理" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "QueryTodoList", Programs_Number = "T029", Programs_Name = "交辦事項查詢" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "TodoListManagement", Programs_Number = "T031", Programs_Name = "交辦事項維護" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "Currency", Programs_Number = "T032", Programs_Name = "幣別維護" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "ExchangeRate", Programs_Number = "T033", Programs_Name = "匯率維護" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AccountingCalendar", Programs_Number = "T034", Programs_Name = "會計曆維護" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AccountingSubject", Programs_Number = "T035", Programs_Name = "會計科目維護" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "TradeAccountSubject", Programs_Number = "T037", Programs_Name = "'交易分錄會計科目維護" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MPArchive", Programs_Number = "A000", Programs_Name = "動產查詢作業" }); 
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MPAdded", Programs_Number = "A111", Programs_Name = "動產新增作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MPMaintain", Programs_Number = "A201", Programs_Name = "動產維護作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MPDisposal", Programs_Number = "A401", Programs_Name = "動產處分作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MPScrappedNotice", Programs_Number = "A402", Programs_Name = "例行性報廢通知" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MPScrappedReciprocation", Programs_Number = "A403", Programs_Name = "例行性報廢回報" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MPTransfer", Programs_Number = "A301", Programs_Name = "動產移轉作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MPDeprn", Programs_Number = "A501", Programs_Name = "動產折舊作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MPDeprnSetting", Programs_Number = "A502", Programs_Name = "動產折舊設定" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MPInventoryNotice", Programs_Number = "A503", Programs_Name = "盤點通知單(總行)" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MPInventoryReciprocation", Programs_Number = "A504", Programs_Name = "盤點回報單(分行)" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MPInventorySummary", Programs_Number = "A505", Programs_Name = "盤點匯整表(總行)" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MPImport", Programs_Number = "A120", Programs_Name = "動產匯入作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MPImportReview", Programs_Number = "A121", Programs_Name = "動產匯入送審作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MPReport", Programs_Number = "AReport", Programs_Name = "動產報表產製" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AccountingTradeDefine", Programs_Number = "T038", Programs_Name = "會計交易分錄維" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "HistoryDataSetting", Programs_Number = "T039", Programs_Name = "歷史資料設定" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "HistoryDataExport", Programs_Number = "T040", Programs_Name = "歷史資料匯出" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "CodeTable", Programs_Number = "T041", Programs_Name = "系統代碼設定" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "BuildAdded", Programs_Number = "C010", Programs_Name = "房屋新增" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "BuildMaintain", Programs_Number = "C020", Programs_Name = "房屋明細維護" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "BuildDisposal", Programs_Number = "C040", Programs_Name = "房屋處分" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "BuildReclassify", Programs_Number = "C050", Programs_Name = "房屋重分類" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "BuildLifeChange", Programs_Number = "C060", Programs_Name = "房屋使用年限異動" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "BuildMPAdded", Programs_Number = "C080", Programs_Name = "重大組成新增" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "BuildMPMaintain", Programs_Number = "C090", Programs_Name = "重大組成明細維護" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "BuildMPDisposal", Programs_Number = "C110", Programs_Name = "重大組成處分" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "BuildMPReclassify", Programs_Number = "C120", Programs_Name = "重大組成重分類" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "BuildMPLifeChange", Programs_Number = "C130", Programs_Name = "重大組成使用年限異動" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "BuildReports", Programs_Number = "C131", Programs_Name = "房屋報表產製" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "DeprnSetting", Programs_Number = "T042", Programs_Name = "折舊設定維護" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AssetParentRelation", Programs_Number = "T043", Programs_Name = "母子資產查詢" });

            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MUSearch", Programs_Number = "F100", Programs_Name = "主檔更新查詢作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MU", Programs_Number = "F200", Programs_Name = "資產主檔更新" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "MUReport", Programs_Number = "F201", Programs_Name = "資產主檔報表產製" });

            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "Overdue", Programs_Number = "G200", Programs_Name = "資產催繳" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "OverdueList", Programs_Number = "G100", Programs_Name = "逾期案件查詢" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "OverdueSearch", Programs_Number = "G201", Programs_Name = "資產催繳查詢作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "OverdueReport", Programs_Number = "G202", Programs_Name = "資產催繳報表產製" });

            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LeaseRightsSearch", Programs_Number = "D100", Programs_Name = "租賃查詢作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LeaseRightsAdded", Programs_Number = "D200", Programs_Name = "租賃權益新增作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LeaseRightsMaintain", Programs_Number = "D300", Programs_Name = "租賃權益維護作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LeaseRightsDisposal", Programs_Number = "D400", Programs_Name = "租賃權益處分作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LeaseRightsDeprn", Programs_Number = "D500", Programs_Name = "租賃權益折舊作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LeaseRightsDeprnSetting", Programs_Number = "D510", Programs_Name = "租賃權益折舊設定" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LeaseRightsWriteOff", Programs_Number = "D520", Programs_Name = "租賃權益攤銷作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LeaseRightsWriteOffSetting", Programs_Number = "D530", Programs_Name = "租賃權益攤銷設定" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LeaseRightsReport", Programs_Number = "D540", Programs_Name = "租賃權益報表產製" });

            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "IntegrationSearch", Programs_Number = "E110", Programs_Name = "承租契約查詢作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "IntegrationAdded", Programs_Number = "E120", Programs_Name = "承租契約新增作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "IntegrationMaintain", Programs_Number = "E130", Programs_Name = "承租契約維護作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "IntegrationRenewal", Programs_Number = "E140", Programs_Name = "承租續租換約作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "IntegrationPay", Programs_Number = "E150", Programs_Name = "承租租金繳納作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "IntegrationTrack", Programs_Number = "E160", Programs_Name = "承租進度追蹤作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "IntegrationReport", Programs_Number = "E170", Programs_Name = "承租報表產製作業" });

            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "RentInquireSearch", Programs_Number = "E210", Programs_Name = "出租契約查詢作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "RentInquireAdded", Programs_Number = "E220", Programs_Name = "出租契約新增作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "RentInquireMaintain", Programs_Number = "E230", Programs_Name = "出租契約維護作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "RentInquireRenewal", Programs_Number = "E240", Programs_Name = "出租續租換約作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "RentInquirePay", Programs_Number = "E270", Programs_Name = "出租繳納管理作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "RentInquireInvoice", Programs_Number = "E280", Programs_Name = "出租發票匯出作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "RentInquireWriteOff", Programs_Number = "E250", Programs_Name = "出租租金銷帳作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "RentInquireTrack", Programs_Number = "E260", Programs_Name = "出租履約追蹤作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "RentInquireReport", Programs_Number = "E261", Programs_Name = "出租報表產製作業" });

            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "EvaluationSearch", Programs_Number = "H100", Programs_Name = "資產評估查詢作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "EvaluationAssign", Programs_Number = "H200", Programs_Name = "指定評估作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "BuildEvaluation", Programs_Number = "H300", Programs_Name = "房屋評估作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "LandEvaluation", Programs_Number = "H400", Programs_Name = "土地評估作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "EvaluationSubmit", Programs_Number = "H500", Programs_Name = "評估結果提報作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "EvaluationReport", Programs_Number = "H501", Programs_Name = "資產評估報表產製作業" });

            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AssetInspectSearch", Programs_Number = "I100", Programs_Name = "資產巡查查詢作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AssetInspectAssign", Programs_Number = "I200", Programs_Name = "指定巡查作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AssetInspect", Programs_Number = "I300", Programs_Name = "分行巡查作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AssetInspectReport", Programs_Number = "I301", Programs_Name = "資產巡查報表產製作業" });

            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AssetsActivationSearch", Programs_Number = "J100", Programs_Name = "資產活化查詢作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AssetsActivationAssign", Programs_Number = "J200", Programs_Name = "資產活化指派作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AssetsActivation", Programs_Number = "J300", Programs_Name = "資產活化規劃作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AssetsActivationSubmit", Programs_Number = "J400", Programs_Name = "資產活化進度回報作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AssetsActivationReport", Programs_Number = "J401", Programs_Name = "資產活化報表產製作業" });

            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "PolicySearch", Programs_Number = "T600", Programs_Name = "保單查詢作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "PolicyAdded", Programs_Number = "T601", Programs_Name = "保單新增作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "PolicyMaintain", Programs_Number = "T602", Programs_Name = "保單維護作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "InsuranceSearch", Programs_Number = "T603", Programs_Name = "出險查詢作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "InsuranceAdded", Programs_Number = "T604", Programs_Name = "出險新增作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "InsuranceMaintain", Programs_Number = "T605", Programs_Name = "出險維護作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "PolicyReport", Programs_Number = "T606", Programs_Name = "保單報表產製作業" });


            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "DossierSearch", Programs_Number = "T607", Programs_Name = "卷宗查詢作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "DossierAdded", Programs_Number = "T608", Programs_Name = "卷宗新增作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "DossierMaintain", Programs_Number = "T609", Programs_Name = "卷宗維護作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "DossierParSetting", Programs_Number = "T610", Programs_Name = "卷宗參數管理作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "DossierReport", Programs_Number = "T611", Programs_Name = "卷宗報表產製作業" });


            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "DossierRightSearch", Programs_Number = "T612", Programs_Name = "權狀查詢作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "DossierRightAdded", Programs_Number = "T613", Programs_Name = "權狀新增作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "DossierRightMaintain", Programs_Number = "T614", Programs_Name = "權狀維護作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AccessDossierRight", Programs_Number = "T615", Programs_Name = "調閱權狀查詢作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "AccessDossierRightApply", Programs_Number = "T616", Programs_Name = "調閱權狀申請作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "DossierRightReport", Programs_Number = "T617", Programs_Name = "權狀報表產製作業" });

            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "ComplexSearch", Programs_Number = "T618", Programs_Name = "綜合查詢作業" });

            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "IntersectionSearch", Programs_Number = "T619", Programs_Name = "交集查詢自訂報表作業" });
            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "IntersectionNotice", Programs_Number = "T620", Programs_Name = "交集查詢報表通知作業" });

            AS_Programs_Records.Create(new AS_Programs { Programs_Code = "ImpairmGroup", Programs_Number = "T621", Programs_Name = "資產現金單位" });

        }
    }
}
