﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Flow_Name_Data : IDisposable
    {
        public AS_Flow_Name_Data()
        {
            AS_Flow_Name_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Flow_Name_Data");

            Script();
            TestData();
        }

        private void TestData()
        {
            DataContext.flow_name_user_login_role = AS_Flow_Name_Records.Create(new AS_Flow_Name { Flow_Name = "測試簽核流程用樣版", Flow_Code = "Qoo", Flow_Type = "測試簽核流程用樣版", Is_Active = true, Cancel_Code = false, Remark = "測試簽核流程用樣版" }).ID;
        }

        private void Script()
        {
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
SET IDENTITY_INSERT [dbo].[AS_Flow_Name] ON 
SET IDENTITY_INSERT [dbo].[AS_Flow_Name] OFF
");
            }
        }
    }
}
