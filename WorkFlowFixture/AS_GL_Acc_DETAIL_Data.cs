﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_GL_Acc_Detail_Data : IDisposable
    {
        public AS_GL_Acc_Detail_Data()
        {
            AS_GL_Acc_Detail_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_GL_Acc_Detail");
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
SET IDENTITY_INSERT [dbo].[AS_GL_Acc_Detail] ON 
SET IDENTITY_INSERT [dbo].[AS_GL_Acc_Detail] OFF
");
            }
        }
    }


}
