﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Asset_Category_Data : IDisposable
    {
        public AS_Asset_Category_Data()
        {
            AS_Asset_Category_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Asset_Category_Data");
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
SET IDENTITY_INSERT [dbo].[AS_Asset_Category] ON 
SET IDENTITY_INSERT [dbo].[AS_Asset_Category] OFF
");
            }
        }
    }
}