﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Role_Flow_Data : IDisposable
    {
        public AS_Role_Flow_Data()
        {
            AS_Role_Flow_Records.Delete(m => true);
        }

        public void Dispose()
        { }
    }
}
