﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_GL_Acc_Deprn_Data : IDisposable
    {
        public AS_GL_Acc_Deprn_Data()
        {
            AS_GL_Acc_Deprn_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_GL_Acc_Deprn");
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
SET IDENTITY_INSERT [dbo].[AS_GL_Acc_Deprn] ON 
SET IDENTITY_INSERT [dbo].[AS_GL_Acc_Deprn] OFF
");
            }
        }
    }


}
