﻿using System;
using Library.Entity.Edmx;

namespace WorkFlowFixture
{
    public class AS_GL_Calendar_Data : IDisposable
    {
        public AS_GL_Calendar_Data()
        {
            AS_GL_Calendar_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_GL_Calendar_Data");
            using (var context = new AS_LandBankEntities())
            {
                //context.Database.ExecuteSqlCommand(BulkCommand());
            }
        }

        private static string BulkCommand()
        {
            return string.Format(@"
BULK INSERT dbo. AS_GL_Calendar
FROM '{0}\..\..\bcp\AS_GL_Calendar.bcp'
WITH
(
    BATCHSIZE = 1000,
    DATAFILETYPE = 'native',
    TABLOCK
)", System.Environment.CurrentDirectory);
        }
    }
}
