﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Currency_Data : IDisposable
    {
        public AS_Currency_Data()
        {
            AS_Currency_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Currency_Data");
            using (var context = new AS_LandBankEntities())
            {
                //context.Database.ExecuteSqlCommand(BulkCommand());
            }
        }

        private static string BulkCommand()
        {
            return string.Format(@"
BULK INSERT dbo.AS_Currency
FROM '{0}\..\..\bcp\AS_Currency.bcp'
WITH
(
    BATCHSIZE = 1000,
    DATAFILETYPE = 'native',
    TABLOCK
)", System.Environment.CurrentDirectory);
        }
    }
}
