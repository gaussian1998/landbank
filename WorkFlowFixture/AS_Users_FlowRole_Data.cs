﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Users_FlowRole_Data : IDisposable
    {
        public AS_Users_FlowRole_Data()
        {
            AS_Users_FlowRole_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Users_FlowRole_Data");

            AS_Users_Records.Update(m => m.User_Name == "李士測", entity => {

                entity.AS_Users_FlowRole.Add(new AS_Users_FlowRole { FlowRoleID = DataContext.flow_role_user_login_2 });
            });

            AS_Users_Records.Update(m => m.User_Name == "宋測試", entity => {

                entity.AS_Users_FlowRole.Add(new AS_Users_FlowRole { FlowRoleID = DataContext.flow_role_user_login_3 });
            });

            AS_Users_Records.Update(m => m.User_Name == "吳孟測", entity => {

                entity.AS_Users_FlowRole.Add(new AS_Users_FlowRole { FlowRoleID = DataContext.flow_role_user_login_4 });
            });
        }
    }
}
