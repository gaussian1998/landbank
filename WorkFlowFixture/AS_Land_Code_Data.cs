﻿
using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Land_Code_Data : IDisposable
    {
        public AS_Land_Code_Data()
        {
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
DELETE FROM AS_Land_Code WHERE 1=1;
");
            }
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Land_Code_Data");
            using (var context = new AS_LandBankEntities())
            {
                //context.Database.ExecuteSqlCommand(BulkCommand());
            }
        }

        private static string BulkCommand()
        {
            return string.Format(@"
BULK INSERT dbo.AS_Land_Code
FROM '{0}\..\..\bcp\AS_Land_Code.bcp'
WITH
(
    BATCHSIZE = 1000,
    DATAFILETYPE = 'native',
    TABLOCK
)", System.Environment.CurrentDirectory);
        }
    }
}