﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Rate_Data : IDisposable
    {
        public AS_Rate_Data()
        {
            AS_Rate_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_GL_Trade_Define_Data");
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
SET IDENTITY_INSERT [dbo].[AS_Rate] ON 
SET IDENTITY_INSERT [dbo].[AS_Rate] OFF
");
            }
        }
    }
}
