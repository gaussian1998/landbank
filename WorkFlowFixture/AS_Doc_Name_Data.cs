﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Doc_Name_Data : IDisposable
    {
        public AS_Doc_Name_Data()
        {
            AS_Doc_Name_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Doc_Name_Data");

            //Script();
            TestData();
        }

        private void TestData()
        {
            AS_Doc_Name_Records.Create(entity => {

                entity.Doc_No = "T110";
                entity.Doc_Type = "D5";
                entity.Doc_Name = "人員權限職務輪調單";
                entity.Is_Active = true;
                entity.Cancel_Code = false;
                entity.Remark = null;
                entity.Last_Updated_Time = DateTime.Now;
                entity.Last_Updated_By = null;
            });
            DataContext.DocNameUserLoginRole_ADD = AS_Doc_Name_Records.First(m => m.Doc_No == "T110").ID;
        }

        private void Script()
        {
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
SET IDENTITY_INSERT [dbo].[AS_Doc_Name] ON 
SET IDENTITY_INSERT [dbo].[AS_Doc_Name] OFF
");
            }
        }
    }
}
