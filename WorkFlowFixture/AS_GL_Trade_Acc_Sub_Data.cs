﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_GL_Trade_Acc_Sub_Data : IDisposable
    {
        public AS_GL_Trade_Acc_Sub_Data()
        {
            AS_GL_Trade_Acc_Sub_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_GL_Trade_Acc_Sub");
            using (var context = new AS_LandBankEntities())
            {
                //context.Database.ExecuteSqlCommand(BulkCommand());
            }
        }

        private static string BulkCommand()
        {
            return string.Format(@"
BULK INSERT dbo.AS_GL_Trade_Acc_Sub
FROM '{0}\..\..\bcp\AS_GL_Trade_Acc_Sub.bcp'
WITH
(
    BATCHSIZE = 1000,
    DATAFILETYPE = 'native',
    TABLOCK
)", System.Environment.CurrentDirectory);
        }
    }
}
