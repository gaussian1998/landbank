﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    class AS_Users_Role_Data : IDisposable
    {
        public AS_Users_Role_Data()
        {
            AS_Users_Role_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Users_Role_Data");

            AS_Users_Records.Update(m => true, entity => {

                entity.AS_Users_Role.Add(new AS_Users_Role { Role_ID = DataContext.login_role_sa, Is_Enable = true });
            });
        }
    }
}
