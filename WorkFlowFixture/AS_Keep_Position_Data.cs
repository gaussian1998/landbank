﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Keep_Position_Data : IDisposable
    {
        public AS_Keep_Position_Data()
        {
            AS_Keep_Position_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Keep_Position");
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
SET IDENTITY_INSERT [dbo].[AS_Keep_Position] ON 
SET IDENTITY_INSERT [dbo].[AS_Keep_Position] OFF
");
            }
        }
    }


}
