﻿using System;
using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.FormNo;

namespace WorkFlowFixture
{
    public class AS_Flow_Open_Data : IDisposable
    {
        public AS_Flow_Open_Data()
        {
            AS_Flow_Open_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Flow_Open_Data");

            int Apply_1 = AS_Users_Records.First(m => m.User_Name == "王勝測").ID;
            int Apply_2 = AS_Users_Records.First(m => m.User_Name == "丁測發").ID;
            int Apply_3 = AS_Users_Records.First(m => m.User_Name == "王興測").ID;

            string FormNo_1 = LocalApprovalTodoServant.Create(Apply_1, "T110", "K100", "example");
            LocalApprovalTodoServant.Start(Apply_1, FormNo_1, "xyz");

            string FormNo_2 = LocalApprovalTodoServant.Create(Apply_2, "T110", "K100", "example_partial_view");
            LocalApprovalTodoServant.Start(Apply_2, FormNo_2, "qoo");

            string FormNo_3 = LocalApprovalTodoServant.Create(Apply_3, "T110", "K100", "example_fast_report");
            LocalApprovalTodoServant.Start(Apply_3, FormNo_3, "orz");

            string FormNo_4 = LocalApprovalTodoServant.Create(Apply_1, "T110", "K100", "example_http_get");
            LocalApprovalTodoServant.Start(Apply_1, FormNo_4, "ww");
        }
    }
}
