﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Branch_Currency_Data : IDisposable
    {
        public AS_Branch_Currency_Data()
        {
            AS_Branch_Currency_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Branch_Currency_Data");
            using (var context = new AS_LandBankEntities())
            {
                //context.Database.ExecuteSqlCommand(BulkCommand());
            }
        }

        private static string BulkCommand()
        {
            return string.Format(@"
BULK INSERT dbo.AS_Branch_Currency
FROM '{0}\..\..\bcp\AS_Branch_Currency.bcp'
WITH
(
    BATCHSIZE = 1000,
    DATAFILETYPE = 'native',
    TABLOCK
)", System.Environment.CurrentDirectory);
        }
    }
}
