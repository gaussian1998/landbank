﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Agent_Flow_Data : IDisposable
    {
        public AS_Agent_Flow_Data()
        {
            AS_Agent_Flow_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Agent_Flow_Data ");
            Test();
        }

        private void Script()
        {
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
SET IDENTITY_INSERT [dbo].[AS_Agent_Flow] ON 
SET IDENTITY_INSERT [dbo].[AS_Agent_Flow] OFF
");
            }
        }


        private void Test()
        {
            AS_Doc_Name_Records.Update(m => m.ID == DataContext.DocNameUserLoginRole_ADD, entity => {

                entity.AS_Agent_Flow.Add(new AS_Agent_Flow { Flow_Template_ID = DataContext.flow_template_user_login_role, Version = 1 });
            });
        }
    }
}
