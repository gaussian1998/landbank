﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    class AS_Deprn_Data : IDisposable
    {
        public AS_Deprn_Data()
        {
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
DELETE FROM AS_Deprn WHERE 1=1;
");
            }
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Deprn_Data");
            using (var context = new AS_LandBankEntities())
            {}
        }
    }
}
