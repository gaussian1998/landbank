﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class Acc_EntriesByAccounting_Data : IDisposable
    {
        public Acc_EntriesByAccounting_Data()
        {
            Acc_EntriesByAccounting_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("Acc_EntriesByAccounting_Data");
        }
    }
}
