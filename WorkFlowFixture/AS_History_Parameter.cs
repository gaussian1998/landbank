﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_History_Parameter_Data : IDisposable
    {
        public AS_History_Parameter_Data()
        {
            AS_History_Parameter_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_History_Parameter");
            
            Script();
        }

        private void Script()
        {
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
SET IDENTITY_INSERT [dbo].[AS_History_Parameter] ON 
SET IDENTITY_INSERT [dbo].[AS_History_Parameter] OFF
");
            }
        }
    }
}
