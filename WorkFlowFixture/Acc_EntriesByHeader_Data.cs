﻿using Library.Entity.Edmx;
using System;


namespace WorkFlowFixture
{
    public class Acc_EntriesByHeader_Data : IDisposable
    {
        public Acc_EntriesByHeader_Data()
        {
            Acc_EntriesByHeader_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("Acc_EntriesByHeader_Data");
        }
    }
}
