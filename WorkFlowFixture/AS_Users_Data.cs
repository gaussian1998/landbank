﻿using System;
using Library.Entity.Edmx;

namespace WorkFlowFixture
{
    public class AS_Users_Data : IDisposable
    {
        public AS_Users_Data()
        {
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
DELETE FROM AS_Users WHERE 1=1;
");
            }
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Users");
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(BulkCommand());
            }
        }

        private static string BulkCommand()
        {
            return string.Format(@"
BULK INSERT dbo.AS_Users
FROM '{0}\..\..\bcp\AS_Users.bcp'
WITH
(
    BATCHSIZE = 1000,
    DATAFILETYPE = 'native',
    TABLOCK
)", System.Environment.CurrentDirectory);
        }

    }
}