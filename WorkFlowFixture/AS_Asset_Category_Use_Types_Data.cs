﻿using Library.Entity.Edmx;
using System;


namespace WorkFlowFixture
{
    public class AS_Asset_Category_Use_Types_Data : IDisposable
    {
        public AS_Asset_Category_Use_Types_Data()
        {
            AS_Asset_Category_Use_Types_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Asset_Category_Use_Types_Data");
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
SET IDENTITY_INSERT [dbo].[AS_Asset_Category_Use_Types] ON 
SET IDENTITY_INSERT [dbo].[AS_Asset_Category_Use_Types] OFF
");
            }
        }
    }
}
