﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Flow_Template_Flow_Role_Data : IDisposable
    {
        public AS_Flow_Template_Flow_Role_Data()
        {
            AS_Flow_Template_Flow_Role_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Flow_Template_Flow_Role_Records");
            //Script();
            TestData();
        }

        private void TestData()
        {
            AS_Flow_Template_Records.Update(m => m.AS_Flow_Name.ID == DataContext.flow_name_user_login_role, entity => {

                entity.AS_Flow_Template_Flow_Role.Add(new AS_Flow_Template_Flow_Role { Flow_Role_ID = DataContext.flow_role_user_login_2, Step_No = 1 });
                entity.AS_Flow_Template_Flow_Role.Add(new AS_Flow_Template_Flow_Role { Flow_Role_ID = DataContext.flow_role_user_login_3, Step_No = 2 });
                entity.AS_Flow_Template_Flow_Role.Add(new AS_Flow_Template_Flow_Role { Flow_Role_ID = DataContext.flow_role_user_login_4, Step_No = 3 });
            });
        }

        private void Script()
        {
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
SET IDENTITY_INSERT [dbo].[AS_Flow_Template_Flow_Role] ON 
SET IDENTITY_INSERT [dbo].[AS_Flow_Template_Flow_Role] OFF
");
            }
        }


    }
}
