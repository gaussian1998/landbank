﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Flow_Template_Data : IDisposable
    {
        public AS_Flow_Template_Data()
        {
            AS_Flow_Template_Records.Delete(m => true);
        }

       public void Dispose()
        {
            Console.WriteLine("AS_Flow_Template_Data");
            //Script();
            TestData();
        }

        private void TestData()
        {
            AS_Flow_Name_Records.Update(m => m.ID == DataContext.flow_name_user_login_role, entity => {

                entity.AS_Flow_Template.Add(new AS_Flow_Template { Version = 1 });
            });
            DataContext.flow_template_user_login_role = AS_Flow_Template_Records.First(m => m.Flow_Name_ID == DataContext.flow_name_user_login_role).ID;
        }

        private void Script()
        {
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
SET IDENTITY_INSERT [dbo].[AS_Flow_Template] ON 
SET IDENTITY_INSERT [dbo].[AS_Flow_Template] OFF
");
            }
        }
    }
}
