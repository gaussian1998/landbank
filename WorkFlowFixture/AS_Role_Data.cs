﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Role_Data : IDisposable
    {
        public AS_Role_Data()
        {
            AS_Role_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Role_Data");

            DataContext.login_role_sa = AS_Role_Records.Create(new AS_Role { Role_Code = "sa", Role_Name = "系統管理人員" }).ID;
        }
    }
}
