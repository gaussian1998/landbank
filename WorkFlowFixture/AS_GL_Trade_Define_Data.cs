﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_GL_Trade_Define_Data : IDisposable
    {
        public AS_GL_Trade_Define_Data()
        {
            AS_GL_Trade_Define_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_GL_Trade_Define_Data");
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
SET IDENTITY_INSERT [dbo].[AS_GL_Trade_Define] ON 
SET IDENTITY_INSERT [dbo].[AS_GL_Trade_Define] OFF
");
            }
        }
    }
}
