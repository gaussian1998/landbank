﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Post_Header_Data : IDisposable
    {
        public AS_Post_Header_Data()
        {
            AS_Post_Header_Records.Delete(m => true);
        }

        public void Dispose()
        { }
    }
}
