﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_Assets_Build_Land_Data : IDisposable
    {
        public AS_Assets_Build_Land_Data()
        {
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
DELETE FROM AS_Assets_Build_Land WHERE 1=1;
");
            }
        }

        public void Dispose()
        {
            Console.WriteLine("AS_Assets_Build_Land");
            using (var context = new AS_LandBankEntities())
            {
                //context.Database.ExecuteSqlCommand(BulkCommand());
            }
        }

        private static string BulkCommand()
        {
            return string.Format(@"
BULK INSERT dbo.AS_Assets_Build_Land
FROM '{0}\..\..\bcp\AS_Assets_Build_Land.bcp'
WITH
(
    BATCHSIZE = 1000,
    DATAFILETYPE = 'native',
    TABLOCK
)", System.Environment.CurrentDirectory);
        }
    }


}
