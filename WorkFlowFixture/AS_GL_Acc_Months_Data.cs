﻿using Library.Entity.Edmx;
using System;

namespace WorkFlowFixture
{
    public class AS_GL_Acc_Months_Data : IDisposable
    {
        public AS_GL_Acc_Months_Data()
        {
            AS_GL_Acc_Months_Records.Delete(m => true);
        }

        public void Dispose()
        {
            Console.WriteLine("AS_GL_Acc_Months_Data");
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
SET IDENTITY_INSERT [dbo].[AS_GL_Acc_Months] ON 
SET IDENTITY_INSERT [dbo].[AS_GL_Acc_Months] OFF
");
            }
        }
    }


}
