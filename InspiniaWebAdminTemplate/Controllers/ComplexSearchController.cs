﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers
{
    public class ComplexSearchController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "ComplexSearch")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "ComplexSearch")]
        [ExceptionHandle]
        public ActionResult Detail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "ComplexSearch")]
        [ExceptionHandle]
        public ActionResult Q200()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "ComplexSearch")]
        [ExceptionHandle]
        public ActionResult Q201()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "ComplexSearch")]
        [ExceptionHandle]
        public ActionResult Q300()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "ComplexSearch")]
        [ExceptionHandle]
        public ActionResult Q301()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "ComplexSearch")]
        [ExceptionHandle]
        public ActionResult Q400()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "ComplexSearch")]
        [ExceptionHandle]
        public ActionResult Q401()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "ComplexSearch")]
        [ExceptionHandle]
        public ActionResult Q500()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "ComplexSearch")]
        [ExceptionHandle]
        public ActionResult Q501()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "ComplexSearch")]
        [ExceptionHandle]
        public ActionResult Q600()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "ComplexSearch")]
        [ExceptionHandle]
        public ActionResult Q601()
        {
            return View();
        }
    }
}