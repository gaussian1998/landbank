﻿using System.Web.Mvc;
using Library.Servant.Servant.ApprovalMember;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.ApprovalReassignEdit;
using Library.Servant.Servant.ApprovalMember.Models;
using Library.Utility;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers.Flow
{
    [Login]
    [FunctionAuthorize(ID = "ApprovalReassign")]
    [ExceptionHandle]
    public class ApprovalReassignEditController : Controller
    {
        public ActionResult Start(int ID)
        {
            var user = Servant.Edit(this.UserID());
            this.UpdateSearchCondition(new SearchViewModel
            {
                ID = ID,
                BranchCodeID = user.BranchCode,
                BranchName = user.BranchName,
                DepartmentCodeID = user.DepartmentCode,
                DepartmentName = user.DepartmentName
            });

            return RedirectToAction("Index");
        }

        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();

            return View(IndexViewModel(condition));
        }

        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult(Url.Action("Index") + "#page");
        }

        private IndexViewModel IndexViewModel(SearchViewModel condition)
        {
            var result = IndexResult(condition);
            return new IndexViewModel
            {
                FlowOpenID = condition.ID,
                Search = condition,
                Page = result.Page.Mapping<IndexItem, DetailsResult>()
            };
        }

        private IndexResult IndexResult(SearchViewModel condition)
        {
            return Servant.Index(new SearchModel
            {
                Page = condition.Page,
                PageSize = condition.PageSize,
                BranchCodeID = condition.BranchCodeID,
                DepartmentCodeID = condition.DepartmentCodeID
            });
        }

        private IApprovalMemberServant Servant = ServantAbstractFactory.ApprovalMember();
    }
}