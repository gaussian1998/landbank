﻿using System.Linq;
using System.Web.Mvc;
using Library.Utility;
using Library.Servant.Servant.DocName;
using Library.Servant.Servant.DocName.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Models.DocName;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Extension;


namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [FunctionAuthorize(ID = "DocName")]
    [ExceptionHandle]
    public class DocNameController : Controller
    {
        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();

            IndexResult result = DocNameServant.Index((SearchModel)condition);
            return View( new IndexViewModel
            {
                Items = MapProperty.MapAll<DetailsViewModel,DetailsResult>(result.Page.Items).ToList(),
                TotalAmount = result.Page.TotalAmount,
                SearchCondition = condition,
                DocTypeOptions = StaticDataServant.getIDOptions("DocType"),
                DocCodeOptions = StaticDataServant.getDocNames(),
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(SearchViewModel vm)
        {
            this.UpdateSearchCondition<SearchViewModel>(vm);

            return RedirectToAction("Index");
        }

        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult(Url.Action("Index") + "#page");
        }

        public ActionResult Create()
        {
            var result = DocNameServant.New();
            return View((CreateViewModel)result);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(CreateViewModel vm)
        {
            DocNameServant.Create( (CreateModel)vm );
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int ID)
        {
            var result = DocNameServant.Details(ID);
            return View( (EditViewModel)result );
        }

        [HttpPost]
        public JsonResult Update(DetailsViewModel vm)
        {
            UpdateModel updateData = MapProperty.Mapping<UpdateModel, DetailsViewModel>(vm);
            DocNameServant.Update( updateData );

            return Json(new
            {
                message = "表單編號:'" + updateData.DocCode + "' 更新資料成功",
                data = "I'm data",
            });
        }
        
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            DocNameServant.Delete(ID);
            return RedirectToAction("Index");
        }

        public ActionResult Import()
        {
            return View();
        }
        public ActionResult Export()
        {
            return View();
        }

        private IDocNameServant DocNameServant = ServantAbstractFactory.DocName();
    }
}