﻿using System.Linq;
using System.Web.Mvc;
using Library.Utility;
using Library.Servant.Servant.ApprovalRole;
using Library.Servant.Servant.ApprovalRole.Models;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.ApprovalRole;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using InspiniaWebAdminTemplate.Models.Common;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [FunctionAuthorize(ID = "ApprovalRole")]
    [ExceptionHandle]
    public class ApprovalRoleController : Controller
    {
        private IStaticDataServant _staticDataServant = ServantAbstractFactory.StaticData();
        public ActionResult Index()
        {
            ViewBag.DocTypeList = _staticDataServant.GetDocTypes();
            ViewBag.ApproveRoleList = _staticDataServant.GetApproveRole(null);
            //ViewBag. = _staticDataServant.GetDocTypes();
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();

            IndexResult result = ApprovalRoleServant.Index( (SearchModel)condition );
            return View(new IndexViewModel
            {
                Items = MapProperty.MapAll<DetailsViewModel, DetailsResult>(result.Page.Items).ToList(),
                TotalAmount = result.Page.TotalAmount,
                SearchCondition = condition,
                ActiveOptions = result.ActiveOptions,
                CancelOptions = result.CancelOptions
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(SearchViewModel vm)
        {
            this.UpdateSearchCondition<SearchViewModel>(vm);

            return RedirectToAction("Index");
        }

        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult( Url.Action("Index") + "#page");
        }

        public ActionResult Create()
        {
            ViewBag.DocTypeList= _staticDataServant.GetDocTypes();
            ViewBag.GetApproveRoleR04List = _staticDataServant.GetApproveRoleR04();
            ViewBag.GetApproveRoleR05List = _staticDataServant.GetApproveRoleR05();
            ViewBag.GetApproveRoleR07List = _staticDataServant.GetApproveRoleR07();
            ViewBag.GetApproveRoleR06List=_staticDataServant.GetApproveRoleR06();
            ViewBag.GetBranchList = _staticDataServant.GetBranchs();
            return View( (CreateViewModel)ApprovalRoleServant.New() );
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(CreateViewModel vm)
        {
            ApprovalRoleServant.Create( (CreateModel)vm );
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int ID)
        {
            ViewBag.DocTypeList = _staticDataServant.GetDocTypes();
            ViewBag.GetApproveRoleR04List = _staticDataServant.GetApproveRoleR04();
            ViewBag.GetApproveRoleR05List = _staticDataServant.GetApproveRoleR05();
            ViewBag.GetApproveRoleR07List = _staticDataServant.GetApproveRoleR07();
            ViewBag.GetApproveRoleR08List = _staticDataServant.GetApproveRoleR08();
            ViewBag.GetApproveRoleR06List = _staticDataServant.GetApproveRoleR06();
            ViewBag.GetBranchList = _staticDataServant.GetBranchs();
            var result = ApprovalRoleServant.Details(ID);
            return View( (EditViewModel)result );
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Update(UpdateViewModel vm)
        {
            ApprovalRoleServant.Update( (UpdateModel)vm );
            return RedirectToAction("Index");
        }

        public ActionResult GetApproveRole(string operationType)
        {
            List<SelectedModel> _listApproveRole = _staticDataServant.GetApproveRole(operationType);
            List<SelectListItem> _list = new List<SelectListItem>();
            _list.Add(new SelectListItem { Value = "", Text = "全部" });
            //SearchModel _searchModel = (SearchModel)this.GetSearchCondition<SearchViewModel>();

            foreach (SelectedModel item in _listApproveRole)
            {
                _list.Add(new SelectListItem { Value = item.Value, Text = item.Name });
            }
            return Content(JsonConvert.SerializeObject(_list), "application/json");
        }
        public ActionResult GetUserId(string branchCode)
        {
            ListOptionModel<int,string> _listUserId = ApprovalRoleServant.GetUserId(branchCode);
            List<SelectListItem> _list = new List<SelectListItem>();
            foreach (OptionModel<int,string> item in _listUserId.ListOptionModelItems)
            {
                _list.Add(new SelectListItem { Value = item.Value.ToString(), Text = item.Text });
            }
            return Content(JsonConvert.SerializeObject(_list), "application/json");
        }
        [HttpPost]
        public ActionResult CheckCodeId(UpdateModel updateModel)
        {
            //UpdateModel _updateModel = new UpdateModel();
            //if (codeTableId != null) _updateModel.ID = (int)codeTableId;
            //_updateModel.CodeID = codeId;
            //檢查簽核角色編碼是否有重複
            if (ApprovalRoleServant.CheckCodeId(updateModel))
            {
                return Json(new AjaxResultModel() { status = "success", message = "" });
            }
            else
            {
                return Json(new AjaxResultModel() { status = "failed", message = "" });
            }
        }
        private IApprovalRoleServant ApprovalRoleServant = ServantAbstractFactory.ApprovalRole();
       
    }
}