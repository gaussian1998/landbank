﻿using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.ApprovalWithdraw;
using InspiniaWebAdminTemplate.Extension;
using InspiniaWebAdminTemplate.Models.Common;
using Library.Utility;
using Library.Servant.Servant.ApprovalTodo.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.ApprovalTodo;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [FunctionAuthorize(ID = "ApprovalWithdraw")]
    [ExceptionHandle]
    public class ApprovalWithdrawController : Controller
    {
        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();

            var result = Search(condition);
            return View(new IndexViewModel
            {
                Page = result.Page.Mapping<FlowOpenViewModel, FlowOpenIndexItem>(),
                Options = result.Options,
                SearchCondition = condition
            });
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Withdraw(int ID)
        {
            Servant.Withdraw(new WithdrawModel
            {
                ID = ID,
                UserID = this.UserID()
            });

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(FlowSelectedOptionsModel SelectedOptions)
        {
            this.UpdateSearchCondition(new SearchViewModel { SelectedOptions = SelectedOptions });

            return RedirectToAction("Index");
        }

        private IndexResult Search(SearchViewModel condition)
        {
            return Servant.IndexWithdraw(new SearchModel
            {
                UserID = this.UserID(),
                Page = condition.Page,
                PageSize = condition.PageSize,
                SelectedOptions = condition.SelectedOptions
            });
        }

        private IApprovalTodoServant Servant = new LocalApprovalTodoServant();
    }
}