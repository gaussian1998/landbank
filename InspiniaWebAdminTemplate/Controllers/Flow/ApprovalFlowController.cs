﻿using System.Linq;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.ApprovalFlow;
using Library.Utility;
using Library.Servant.Servant.ApprovalFlow;
using Library.Servant.Servant.ApprovalFlow.Models;
using Library.Servant.Servant.Common.Models;
using InspiniaWebAdminTemplate.Models.Common;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [FunctionAuthorize(ID = "ApprovalFlow")]
    [ExceptionHandle]
    public class ApprovalFlowController : Controller
    {
        public ActionResult Index(int? FlowType)
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();
            if (FlowType != null) condition.SelectedOptions.FlowStateCodeID = FlowType;
             var result = Search(condition);
            return View( new IndexViewModel
            {
                Page = result.Page.Mapping<FlowOpenViewModel,FlowOpenIndexItem>(),
                Options = result.Options,
                SearchCondition = condition
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(FlowSelectedOptionsModel SelectedOptions)
        {
            this.UpdateSearchCondition(new SearchViewModel { SelectedOptions = SelectedOptions });

            return RedirectToAction("Index");
        }

        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult(Url.Action("Index") + "#page");
        }

        public ActionResult Logs(int ID)
        {
            var result = Servant.Logs(ID);

            return View(new LogsViewModel {

                FormNo = result.FormNo,
                DocType = result.DocType,
                DocName = result.DocName,
                Logs = MapProperty.MapAll<LogItem, LogItemResult>( result.Logs ).ToList()
            } );
        }

        private IndexResult Search(SearchViewModel condition)
        {
            return Servant.Index(new SearchModel
            {
                Page = condition.Page,
                PageSize = condition.PageSize,
                SelectedOptions = condition.SelectedOptions
            });
        }

        private IApprovalFlowServant Servant = ServantAbstractFactory.ApprovalFlow();
    }
}