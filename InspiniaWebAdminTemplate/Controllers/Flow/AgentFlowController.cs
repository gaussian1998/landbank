﻿using System.Web.Mvc;
using Library.Servant.Servant.AgentFlow.Models;
using Library.Servant.Servant.AgentFlow;
using Library.Servant.Servant.Common;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.AgentFlow;
using Library.Servant.Servant.CodeTable;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [FunctionAuthorize(ID = "AgentFlow")]
    [ExceptionHandle]
    public class AgentFlowController : Controller
    {
        private IStaticDataServant _staticDataServant = ServantAbstractFactory.StaticData();
        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();

            IndexResult result = AgentFlowServant.Index((SearchModel)condition);
            return View( new IndexViewModel
            {
                Page = result.Page,
                SearchCondition = condition,
                DocTypeOptions = LocalCodeTableServant.DocTypeOptions(),
                DocCodeOptions = _staticDataServant.getDocNames(),
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(SearchViewModel vm)
        {
            this.UpdateSearchCondition<SearchViewModel>(vm);

            return RedirectToAction("Index");
        }

        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult(Url.Action("Index", "AgentFlow") + "#page");
        }

        public ActionResult Create()
        {
            var result = AgentFlowServant.New();

            return View((NewViewModel)result);
        }
        
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(CreateViewModel vm)
        {
            if(vm.DocID.HasValue && vm.FlowTemplateID.HasValue)
                AgentFlowServant.Create( new CreateModel { DocID = vm.DocID.Value, FlowTemplateID = vm.FlowTemplateID.Value } );

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int ID)
        {
            var result = AgentFlowServant.Edit(ID);

            return View((EditViewModel)result);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Update(UpdateViewModel vm)
        {
            if( vm.FlowTemplateID.HasValue )
                AgentFlowServant.Update((UpdateModel)vm);

            return RedirectToAction("Index");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            AgentFlowServant.Delete(ID);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult GetFlowTemplate(string oprationType)
        {
            return Json(AgentFlowServant.New(oprationType));
        }
        public ActionResult GetDocNames(string docType)
        {
            List<SelectedModel> _listApproveRole = _staticDataServant.getDocNames(docType);
            List<SelectListItem> _list = new List<SelectListItem>();
            _list.Add(new SelectListItem { Value = "", Text = "全部" });
            //SearchModel _searchModel = (SearchModel)this.GetSearchCondition<SearchViewModel>();

            foreach (SelectedModel item in _listApproveRole)
            {
                _list.Add(new SelectListItem { Value = item.Value, Text = item.Name });
            }
            return Content(JsonConvert.SerializeObject(_list), "application/json");
        }
        private IAgentFlowServant AgentFlowServant = ServantAbstractFactory.AgentFlow();
    }
}