﻿using System.Linq;
using System.Web.Mvc;
using Library.Servant.Servant.ApprovalMember;
using Library.Utility;
using Library.Servant.Servant.ApprovalMember.Models;
using InspiniaWebAdminTemplate.Models.ApprovalMember;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Servants.Search;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.Common.Models;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.Common;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [FunctionAuthorize(ID = "ApprovalMember")]
    [ExceptionHandle]
    public class ApprovalMemberController : Controller
    {
        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();
            ViewBag.BranchList = m_staticDataServant.GetBranchs();
            var result = Servant.Index( (SearchModel)condition );
            return View( new IndexViewModel {

                Page = result.Page.Mapping<DetailsViewModel, DetailsResult>(),
                TotalDepartments = result.Options.DepartmentOptions,
                TotalFlowRoles = result.TotalFlowRoles,
                Search = condition
            } );
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(SearchViewModel vm)
        {
            this.UpdateSearchCondition<SearchViewModel>(vm);

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClearSearchCondition()
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Clear() );

            return RedirectToAction("Index");
        }

        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult(Url.Action("Index", "ApprovalMember") + "#page");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BatchEdit(SearchViewModel vm)
        {
            SearchConditionServant.Update(vm, HttpContext.ApplicationInstance.Context);

            var result = Servant.IndexAll( (SearchModel)vm );
            return View( new BatchEditViewModel {

                Users = result.Page.Items.Select(user => new OptionModel<int, string> { Value = user.UserID, Text = user.UserName }).ToList(),
                TotalFlowRoles = result.TotalFlowRoles,
                SearchRoleName = vm.FlowRoleID.HasValue ? result.TotalFlowRoles.SingleOrDefault(role => role.Value == vm.FlowRoleID).Text : "全部",
                SearchDepartmentName = string.IsNullOrEmpty(vm.DepartmentCodeID) ? "全部" : result.Options.DepartmentOptions.SingleOrDefault( department => department.Value == vm.DepartmentCodeID).Text,
            } );
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BatchUpdate(BatchUpdateViewModel vm)
        {
            if (vm.UpdateUsers != null && vm.UpdateUsers.Count != 0)
                Servant.BatchUpdate(  (BatchUpdateModel)vm );

            return RedirectToAction("Index");
        }


        public ActionResult Edit(int UserID)
        {
            var user  = Servant.Edit(UserID);
            EditViewModel _editViewModel = new EditViewModel
            {

                UserID = UserID,
                BranchName = user.BranchName,
                DepartmentName = user.DepartmentName,
                UserName = user.UserName,
                Cadidate = user.CadidateRoles,
                Selected = user.SelectedRoles
            };
            //取得作業別分類的左右選單
            List<OptionModel<int, string>> _listLeftRole1 = new List<OptionModel<int, string>>();
            List<OptionModel<int, string>> _listLeftRole2 = new List<OptionModel<int, string>>();
            List<OptionModel<int, string>> _listLeftRole3 = new List<OptionModel<int, string>>();
            List<OptionModel<int, string>> _listLeftRole4 = new List<OptionModel<int, string>>();
            List<OptionModel<int, string>> _listRightRole1 = new List<OptionModel<int, string>>();
            List<OptionModel<int, string>> _listRightRole2 = new List<OptionModel<int, string>>();
            List<OptionModel<int, string>> _listRightRole3 = new List<OptionModel<int, string>>();
            List<OptionModel<int, string>> _listRightRole4 = new List<OptionModel<int, string>>();
            //第一類
            List<string> _firstDocType = new List<string> { "D1", "D7", "D8","D19" };
            List<string> _secondDocType = new List<string> { "D3", "D4"};
            foreach (OptionModel<int, string> _option in _editViewModel.Cadidate) {   
                string[] _textSplit = _option.Text.Split('-');
                //第一類
                if (_firstDocType.Contains(_textSplit[1]))
                {
                    _listLeftRole1.Add(new OptionModel<int, string>
                    {
                        Value = _option.Value,
                        Text = _textSplit[0]
                    });
                }
                else if (_secondDocType.Contains(_textSplit[1]))
                {
                    //第二類
                    _listLeftRole2.Add(new OptionModel<int, string>
                    {
                        Value = _option.Value,
                        Text = _textSplit[0]
                    });
                }
                else {
                    //只要類別是空的代表是固定角色開頭是A
                    if (_textSplit[1] == null || _textSplit[1].Trim() == "")
                    {
                        _listLeftRole4.Add(new OptionModel<int, string>
                        {
                            Value = _option.Value,
                            Text = _textSplit[0]
                        });
                    }
                    else {
                        _listLeftRole3.Add(new OptionModel<int, string>
                        {
                            Value = _option.Value,
                            Text = _textSplit[0]
                        });
                    }
                }
            }
            foreach (OptionModel<int, string> _option in _editViewModel.Selected)
            {
                string[] _textSplit = _option.Text.Split('-');
                //第一類
                if (_firstDocType.Contains(_textSplit[1]))
                {
                    _listRightRole1.Add(new OptionModel<int, string>
                    {
                        Value = _option.Value,
                        Text = _textSplit[0]
                    });
                }
                else if (_secondDocType.Contains(_textSplit[1]))
                {
                    _listRightRole2.Add(new OptionModel<int, string>
                    {
                        Value = _option.Value,
                        Text = _textSplit[0]
                    });

                }
                else {
                    //只要類別是空的代表是固定角色開頭是A
                    if (_textSplit[1] == null || _textSplit[1].Trim() == "")
                    {
                        _listRightRole4.Add(new OptionModel<int, string>
                        {
                            Value = _option.Value,
                            Text = _textSplit[0]
                        });
                    }
                    else {
                        _listRightRole3.Add(new OptionModel<int, string>
                        {
                            Value = _option.Value,
                            Text = _textSplit[0]
                        });
                    }
                }
            }
            ViewBag.LeftRole1 = _listLeftRole1;
            ViewBag.LeftRole2 = _listLeftRole2;
            ViewBag.LeftRole3 = _listLeftRole3;
            ViewBag.LeftRole4 = _listLeftRole4;
            ViewBag.RightRole1 = _listRightRole1;
            ViewBag.RightRole2 = _listRightRole2;
            ViewBag.RightRole3 = _listRightRole3;
            ViewBag.RightRole4 = _listRightRole4;
            return View(_editViewModel);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Update(UpdateViewModel vm)
        {
            UpdateRoleModel _model = new UpdateRoleModel
            {
                UserID = vm.UserID
            };
            _model.ApprovalRoles = new List<int>();
            if (vm.ApprovalRoles1 != null && vm.ApprovalRoles1.Count > 0) {
                foreach (int _number in vm.ApprovalRoles1) {
                    _model.ApprovalRoles.Add(_number);
                }
            }
            if (vm.ApprovalRoles2 != null && vm.ApprovalRoles2.Count > 0)
            {
                foreach (int _number in vm.ApprovalRoles2)
                {
                    _model.ApprovalRoles.Add(_number);
                }
            }
            if (vm.ApprovalRoles3 != null && vm.ApprovalRoles3.Count > 0)
            {
                foreach (int _number in vm.ApprovalRoles3)
                {
                    _model.ApprovalRoles.Add(_number);
                }
            }
            if (vm.ApprovalRoles4 != null && vm.ApprovalRoles4.Count > 0)
            {
                foreach (int _number in vm.ApprovalRoles4)
                {
                    _model.ApprovalRoles.Add(_number);
                }
            }
            Servant.Update(_model);

            return RedirectToAction("Index");
        }

        private IApprovalMemberServant Servant = ServantAbstractFactory.ApprovalMember();
        private IStaticDataServant m_staticDataServant = ServantAbstractFactory.StaticData();
    }
}