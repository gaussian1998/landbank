﻿using FastReport.Web;
using Library.Entity.Edmx;
using Library.Utility;
using System.Data;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Extension;

namespace InspiniaWebAdminTemplate.Controllers.Flow
{
    [Login]
    [ExceptionHandle]
    public class ApprovalReportController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Details()
        {
            var webReport = new WebReport();

            webReport.Report.Load(Server.FrxPath("ApprovalReport.frx"));
            webReport.RegisterData(DataSet());
            webReport.Width = 1000;
            return PartialView(webReport);
        }


        private DataSet DataSet()
        {
            return MapProperty.ToDataSet("AS_Code_Table", AS_Code_Table_Records.FindAll(entity => new { ID = entity.ID }));
        }
    }
}