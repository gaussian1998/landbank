﻿using System.Linq;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Models.WorkFlow;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.WorkFlow;
using Library.Servant.Servant.WorkFlow.Models;
using Library.Utility;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using InspiniaWebAdminTemplate.Models.Common;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [FunctionAuthorize(ID = "WorkFlow")]
    [ExceptionHandle]
    public class FlowNameController : Controller
    {
        private IStaticDataServant _staticDataServant = ServantAbstractFactory.StaticData();
        public ActionResult Index()
        {
            ViewBag.DocTypeList = _staticDataServant.GetDocTypes();
            ViewBag.FlowCodeList = _staticDataServant.GetFlowCode(null);
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();
            IndexResult result = WorkFlowServant.Index( (SearchModel)condition );

            return View( new IndexViewModel
            {
                Items = MapProperty.MapAll<DetailsViewModel, DetailsResult>(result.Page.Items).ToList(),
                TotalAmount = result.Page.TotalAmount,
                SearchCondition = condition
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(SearchViewModel vm)
        {
            this.UpdateSearchCondition<SearchViewModel>(vm);

            return RedirectToAction("Index");
        }

        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult(Url.Action("Index", "FlowName") + "#page");
        }

        public ActionResult Create()
        {
            ViewBag.DocTypeList = _staticDataServant.GetDocTypes();
            ViewBag.GetFlowCodeR04List = _staticDataServant.GetApproveRoleR04();
            ViewBag.GetFlowCodeR06List = _staticDataServant.GetApproveRoleR06();
            ViewBag.GetFlowCodeR07List = _staticDataServant.GetApproveRoleR07();
            var result = WorkFlowServant.New();
            return View( (CreateViewModel)result );
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateViewModel vm)
        {
            WorkFlowServant.Create( (CreateModel)vm );
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int ID)
        {
            ViewBag.DocTypeList = _staticDataServant.GetDocTypes();
            ViewBag.GetFlowCodeR04List = _staticDataServant.GetApproveRoleR04();
            ViewBag.GetFlowCodeR06List = _staticDataServant.GetApproveRoleR06();
            ViewBag.GetFlowCodeR07List = _staticDataServant.GetApproveRoleR07();
            EditResult result = WorkFlowServant.Details(ID);

            return View( (EditViewModel)result );
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(UpdateViewModel vm)
        {
            WorkFlowServant.Update( (UpdateModel)vm );

            return RedirectToAction("Index");
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int ID)
        {
            WorkFlowServant.Delete(ID);
            return RedirectToAction("Index");
        }
        public ActionResult GetFlowCode(string operationType)
        {
            List<SelectedModel> _listApproveRole = _staticDataServant.GetFlowCode(operationType);
            List<SelectListItem> _list = new List<SelectListItem>();
            _list.Add(new SelectListItem { Value = "", Text = "全部" });
            //SearchModel _searchModel = (SearchModel)this.GetSearchCondition<SearchViewModel>();

            foreach (SelectedModel item in _listApproveRole)
            {
                _list.Add(new SelectListItem { Value = item.Value, Text = item.Name });
            }
            return Content(JsonConvert.SerializeObject(_list), "application/json");
        }
        [HttpPost]
        public ActionResult CheckFlowCode(UpdateModel updateModel)
        {
            //檢查簽核流程編碼是否有重複
            if (WorkFlowServant.CheckFlowCode(updateModel))
            {
                return Json(new AjaxResultModel() { status = "success", message = "" });
            }
            else
            {
                return Json(new AjaxResultModel() { status = "failed", message = "" });
            }
        }
        private IWorkFlowServant WorkFlowServant = ServantAbstractFactory.WorkFlow();
    }
}