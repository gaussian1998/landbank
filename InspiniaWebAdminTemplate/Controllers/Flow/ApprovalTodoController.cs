﻿using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.ApprovalTodo;
using InspiniaWebAdminTemplate.Models.Common;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.ApprovalTodo.Models;
using Library.Utility;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [ExceptionHandle]
    public class ApprovalTodoController : Controller, FormResultVistor
    {
        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();

            IndexResult result = Search(condition);
            return View(new IndexViewModel
            {
                Page = result.Page.Mapping<FlowOpenViewModel, FlowOpenIndexItem>(),
                Options = result.Options,
                SearchCondition = condition
            });
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(FlowSelectedOptionsModel SelectedOptions)
        {
            this.UpdateSearchCondition( new SearchViewModel { SelectedOptions = SelectedOptions } );

            return RedirectToAction("Index");
        }

        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult(Url.Action("Index") + "#page");
        }

        public ActionResult Edit(int ID)
        {
            return View( Servant.Edit(ID) );
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Update(UpdateViewModel vm)
        {
            switch (vm.Action)
            {
                case 1:
                    Servant.Approval(new ApprovalModel
                    {
                        ID = vm.ID,
                        UserID = this.UserID(),
                        Remark = vm.Remark
                    });
                    break;

                case 2:
                    Servant.Reject(new RejectModel
                    {
                        ID = vm.ID,
                        UserID = this.UserID(),
                        Remark = vm.Remark
                    });
                    break;

                case 3:
                    Servant.RejectTo( new RejectToModel {

                        ID = vm.ID,
                        UserID = this.UserID(),
                        RejectToStep = vm.RejectToStep,
                        Remark = vm.Remark
                    } );
                    break;

                default:
                    break;
            }

            return RedirectToAction("Index");
        }

        private IndexResult Search(SearchViewModel condition)
        {
            return Servant.IndexApproval(new SearchModel
            {
                UserID = this.UserID(),
                Page = condition.Page,
                PageSize = condition.PageSize,
                IncludeApplyUser = true,
                SelectedOptions = condition.SelectedOptions
            });
        }

        [ChildActionOnly]
        public ActionResult Form(int ID)
        {
            var result = Servant.Form(ID);

            return result.Accept(this);
        }

        [ChildActionOnly]
        public ActionResult Visit(PartialViewModel model)
        {
            return PartialView("PartialView", model);
        }

        [ChildActionOnly]
        public ActionResult Visit(HttpGetViewModel model)
        {
            return PartialView("HttpGetView", model);
        }

        [ChildActionOnly]
        public ActionResult Visit(FastReportViewModel model)
        {
            return PartialView("FastReportView", model);
        }

        public ActionResult Visit(NullViewModel model)
        {
            return Content("簽核的 EventListener 沒有實作任何Provider");
        }

        private IApprovalTodoServant Servant = ServantAbstractFactory.ApprovalTodo();
    }
}