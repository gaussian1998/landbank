﻿using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.ApprovalReassign;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.ApprovalTodo.Models;
using Library.Utility;
using Library.Servant.Servant.Common.Models;
using InspiniaWebAdminTemplate.Models.Common;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [FunctionAuthorize(ID = "ApprovalReassign")]
    [ExceptionHandle]
    public class ApprovalReassignController : Controller
    {
        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();

            var result = Search(condition);
            return View(new IndexViewModel
            {
                Page = result.Page.Mapping<FlowOpenViewModel, FlowOpenIndexItem>(),
                Options = result.Options,
                SearchCondition = condition
            } );
        }

        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult(Url.Action("Index") + "#page");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(FlowSelectedOptionsModel SelectedOptions)
        {
            this.UpdateSearchCondition(new SearchViewModel { SelectedOptions = SelectedOptions });

            return RedirectToAction("Index");
        }

        public ActionResult Update(int ID, int? ReassignID)
        {
            if (ReassignID.HasValue)
                Servant.Reassign(new ReassignModel
                {

                    UserID = this.UserID(),
                    ReassignUserID = ReassignID.Value,
                    FlowID = ID
                });

            return RedirectToAction("Index");
        }

        private IndexResult Search(SearchViewModel condition)
        {
            return Servant.IndexReassign( new SearchModel
            {
                UserID = this.UserID(),
                Page = condition.Page,
                PageSize = condition.PageSize,
                SelectedOptions = condition.SelectedOptions
            });
        }

        private IApprovalTodoServant Servant = ServantAbstractFactory.ApprovalTodo();
    }
}