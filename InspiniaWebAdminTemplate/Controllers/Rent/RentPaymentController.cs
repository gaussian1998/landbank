﻿using InspiniaWebAdminTemplate.Attributes;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [FunctionAuthorize(ID = "RentPayment")]
    [ExceptionHandle]
    public class RentPaymentController : Controller
    {
        // GET: RentPayment
        public ActionResult Index()
        {
            return View();
        }
    }
}