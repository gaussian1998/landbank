﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    public class RentController : Controller
    {
        // GET: Rent
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Detail()
        {
            return View();
        }

        public ActionResult Update()
        {
            return View();
        }

        public ActionResult RentalCreateExample()
        {
            return View();
        }

        public ActionResult RentalCreateExampleIBOX()
        {
            return View();
        }

        public ActionResult PaymentForm()
        {
            return View();
        }

        public ActionResult PaymentFormCreate()
        {
            return View();
        }

        public ActionResult PaymentFormEdit()
        {
            return View();
        }
        public ActionResult ContractMaintain()
        {
            return View();
        }
    }
}