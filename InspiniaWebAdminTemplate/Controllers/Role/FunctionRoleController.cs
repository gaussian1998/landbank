﻿using System.Linq;
using System.Web.Mvc;
using Library.Utility;
using Library.Servant.Servant.FunctionRole;
using Library.Servant.Servant.FunctionRole.Models;
using InspiniaWebAdminTemplate.Models.FunctionRole;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Models.Common;
using InspiniaWebAdminTemplate.Extension;
using System.Collections.Generic;
using Library.Servant.Servant.Common;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [FunctionAuthorize(ID = "FunctionRole")]
    [ExceptionHandle]
    public class FunctionRoleController : Controller
    {
        private IEnumerable<SelectListItem> _docTypes
        {
            get {
                return StaticDataServant.GetDocTypes().Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.Value
                });
            }
        }

        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();
            IndexResult result = Servant.Index((SearchModel)condition);

            ViewBag.DocTypes = new SelectList(_docTypes, "Value", "Text", condition.DocType);

            return View( new IndexViewModel {

                Items = MapProperty.MapAll( result.Items, item => (DetailsViewModel)item ).ToList(),
                TotalAmount = result.TotalAmount,
                TotalFunctions = result.TotalFunctions,
                condition = condition
            } );
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(SearchViewModel vm)
        {
            this.UpdateSearchCondition<SearchViewModel>(vm);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult(Url.Action("Index") + "#page");
        }

        [HttpGet]
        public ActionResult Edit(int ID)
        {
            EditResult result = Servant.Edit(ID);

            return View((EditViewModel)result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(UpdateViewModel vm)
        {
            Servant.Update( (UpdateModel)vm );

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Create()
        {
            NewResult result = Servant.New();

            return View(new NewViewModel
            {
                CadidateRoles = result.CadidateRoles.Select(m => new OptionViewModel { Value = m.Value, Text = m.Text }).ToList()
            });
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateViewModel vm)
        {
            Servant.Create((CreateModel)vm);

            return RedirectToAction("Index");
        }

        private IFucntionRoleServant Servant = ServantAbstractFactory.FucntionRole();
    }
}