﻿using System.Linq;
using System.Web.Mvc;
using Library.Entity.Edmx;
using Library.Servant.Enums;
using System.Collections.Generic;
using Library.Servant.Servant.UserRole;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.UserRole.Models;
using InspiniaWebAdminTemplate.Models.UserRole;
using InspiniaWebAdminTemplate.Servants.Search;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.UserInformation.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.Common;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [FunctionAuthorize(ID = "ApplyAuth")]
    [ExceptionHandle]
    public class ApplyAuthController : Controller
    {
        private IEnumerable<SelectListItem> _statusList // 之後會由Servant取得
        {
            get {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text="新增", Value="1" },
                    new SelectListItem { Text="簽核中", Value="2" }, // 在新增中不允許作業
                    new SelectListItem { Text="已核准", Value="3" }, // 在新增中不允許作業
                    new SelectListItem { Text="退回修改", Value="4" },
                    new SelectListItem { Text="抽回修改", Value="5" },
                    new SelectListItem { Text="作廢", Value="6" } // 在新增中不允許作業
                };
            }
        }

        private IEnumerable<SelectListItem> _typeList
        {
            get {
                return StaticDataServant.GetUserTransactionTypes().Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.Value
                });
            }
        }
        
        public ActionResult Index()
        {
            var userCode = StaticDataServant.GetUserCode(this.UserID());
            var isNull = TempData["IsNull"] as bool?;

            var vm = (DetailViewModel)Servant.Detail(userCode, "");

            ViewBag.CanEdit = true;
            ViewBag.CanQuery = false;
            ViewBag.HasNoData = isNull == true || (!string.IsNullOrEmpty(userCode) && string.IsNullOrEmpty(vm.UserCode));

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Submit(DetailViewModel vm)
        {
            vm.HeaderInfo.AppliedUserId = this.UserID();
            FormType type = FormType.Undefined;
            string formName = "";

            switch (vm.HeaderInfo.Type)
            {
                case FormType.RoleCreate:
                    type = vm.HeaderInfo.IsModified ? FormType.RoleCreateWithModified : FormType.RoleCreate;
                    formName = FormType.RoleCreate.GetFormName();
                    break;
                case FormType.RoleRotation:
                    type = vm.HeaderInfo.IsModified ? FormType.RoleRotationWithModified : FormType.RoleRotation;
                    formName = FormType.RoleRotation.GetFormName();
                    break;
                case FormType.RoleOther:
                    type = vm.HeaderInfo.IsModified ? FormType.RoleOtherWithModified : FormType.RoleOther;
                    formName = FormType.RoleOther.GetFormName();
                    break;
                default:
                    return RedirectToAction("Index");
            }

            vm.HeaderInfo.FlowCode = type.GetNewFormNO();
            vm.HeaderInfo.Type = type;
            Servant.Submit((DetailModel)vm);

            var docID = AS_Doc_Name_Records.First(m => m.Doc_Name == formName).ID;
            LocalApprovalTodoServant.Create(this.UserID(), docID, vm.HeaderInfo.FlowCode, "UserRole");
            LocalApprovalTodoServant.Start(this.UserID(), vm.HeaderInfo.FlowCode);

            return RedirectToAction("Index", "QueryUserRole");
        }

        private IUserRoleServant Servant = ServantAbstractFactory.UserRole();
    }
}
