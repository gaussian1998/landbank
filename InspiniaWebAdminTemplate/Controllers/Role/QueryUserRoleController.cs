﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Extension;
using InspiniaWebAdminTemplate.Models.QueryUserRole;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.QueryUserRole;
using Library.Servant.Servant.QueryUserRole.Models;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers.Role
{
    [Login]
    [FunctionAuthorize(ID = "QueryUserRole")]
    [ExceptionHandle]
    public class QueryUserRoleController : Controller
    {
        private IEnumerable<SelectListItem> _departments
        {
            get {
                return StaticDataServant.GetDepts().Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.Value
                });
            }
        }

        private IEnumerable<SelectListItem> _branchs
        {
            get {
                return StaticDataServant.GetBranchs().Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.Value
                });
            }
        }

        private IEnumerable<SelectListItem> _statusList // 之後會由Servant取得
        {
            get {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text="新增", Value="1" },
                    new SelectListItem { Text="簽核中", Value="2" }, // 在新增中不允許作業
                    new SelectListItem { Text="已核准", Value="3" }, // 在新增中不允許作業
                    new SelectListItem { Text="退回修改", Value="4" },
                    new SelectListItem { Text="抽回修改", Value="5" },
                    new SelectListItem { Text="作廢", Value="6" } // 在新增中不允許作業
                };
            }
        }

        private IEnumerable<SelectListItem> _typeList
        {
            get {
                return StaticDataServant.GetUserTransactionTypes().Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.Value
                });
            }
        }
        
        [HttpGet]
        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult(Url.Action("Index") + "#page");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(SearchViewModel vm)
        {
            this.UpdateSearchCondition<SearchViewModel>(vm);
            return RedirectToAction("Index");
        }

        // GET: QueryUserRole
        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();

            IndexResult result = Servant.Index((SearchModel)condition);

            ViewBag.Branchs = new SelectList(_branchs, "Value", "Text", condition.Branch);
            ViewBag.Departments = new SelectList(_departments, "Value", "Text", condition.Department);
            ViewBag.StatusList = new SelectList(_statusList, "Value", "Text", condition.Status);
            ViewBag.TypeList = new SelectList(_typeList, "Value", "Text", condition.Type);

            IndexViewModel vm = new IndexViewModel
            {
                Items = result.Items.AsEnumerable().Select(m => (ItemViewModel)m).ToList(),
                TotalAmount = result.TotalAmount,
                Condition = condition
            };

            return View(vm);
        }

        public ActionResult Show(string id, string flowCode)
        {
            var vm = (DetailViewModel)Servant.Detail(id, flowCode);
            vm.FlowCode = flowCode;

            ViewBag.CanEdit = false;
            ViewBag.CanQuery = false;
            ViewBag.HasNoData = false;
            ViewBag.FlowCode = flowCode;

            if (string.IsNullOrEmpty(vm.UserCode)) {
                return RedirectToAction("Index", new { flowCode = flowCode });
            }

            return View("Detail", vm);
        }

        private IQueryUserRole Servant = ServantAbstractFactory.QueryUserRole();
    }
}