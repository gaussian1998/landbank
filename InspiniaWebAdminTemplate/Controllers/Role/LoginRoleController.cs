﻿using System.Linq;
using System.Web.Mvc;
using Library.Servant.Servant.LoginRole.Models;
using Library.Servant.Servant.LoginRole;
using Library.Utility;
using InspiniaWebAdminTemplate.Models.LoginRole;
using InspiniaWebAdminTemplate.Models.Common;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Servants.Search;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Extension;
using System.Collections.Generic;
using Library.Servant.Servant.Common;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [FunctionAuthorize(ID = "LoginRole")]
    [ExceptionHandle]
    public class LoginRoleController : Controller
    {
        private IEnumerable<SelectListItem> _docTypes
        {
            get {
                return StaticDataServant.GetDocTypes().Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.Value
                });
            }
        }

        private IEnumerable<SelectListItem> _operatingTypes
        {
            get {
                return Servant.GetOperatingTypes().Select(m => new SelectListItem
                {
                    Text = m.Text,
                    Value = m.Value
                });
            }
        }

        private IEnumerable<SelectListItem> _branchTypes
        {
            get {
                return Servant.GetBranchTypes().Select(m => new SelectListItem
                {
                    Text = m.Text,
                    Value = m.Value
                });
            }
        }
        
        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();
            IndexResult result = Servant.Index( (SearchModel)condition );

            IndexViewModel vm = new IndexViewModel
            {
                Items = MapProperty.MapAll( result.Items, item => (DetailsViewModel)item ).ToList(),
                TotalAmount = result.TotalAmount,
                TotalRoles = result.TotalRoles,
                condition = condition
            };

            ViewBag.DocTypes = new SelectList(_docTypes, "Value", "Text", vm.condition.DocType);

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(SearchViewModel vm)
        {
            this.UpdateSearchCondition<SearchViewModel>(vm);
            return RedirectToAction("Index");
        }

        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult(Url.Action("Index") + "#page");
        }

        [HttpGet]
        public ActionResult Create()
        {
            NewResult result = Servant.New();

            ViewBag.IsAdmin = false;
            ViewBag.IsNew = true;
            ViewBag.DocTypes = new SelectList(_docTypes, "Value", "Text", "");
            ViewBag.OperatingTypes = new SelectList(_operatingTypes, "Value", "Text", "");
            ViewBag.BranchTypes = new SelectList(_branchTypes, "Value", "Text", "");
            return View("Detail", (DetailViewModel)result );
        }

        [HttpGet]
        public ActionResult Edit(int ID)
        {
            EditResult result = Servant.Edit(ID);

            ViewBag.IsAdmin = result.Code == "Psuper" ? true : false;
            ViewBag.IsNew = false;
            ViewBag.DocTypes = new SelectList(_docTypes, "Value", "Text", result.DocType);
            ViewBag.OperatingTypes = new SelectList(_operatingTypes, "Value", "Text", result.OperatingType);
            ViewBag.BranchTypes = new SelectList(_branchTypes, "Value", "Text", result.BranchType);
            return View("Detail", (DetailViewModel)result );
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Update(UpdateViewModel vm)
        {
            Servant.Update( (UpdateModel)vm );

            return RedirectToAction("Index");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int ID)
        {
            Servant.Delete(ID);

            return RedirectToAction("Index");
        }

        private ILoginRoleServant Servant = ServantAbstractFactory.LoginRole();
    }
}