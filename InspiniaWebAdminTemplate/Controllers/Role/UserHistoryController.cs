﻿using System.Linq;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.UserHistory;
using Library.Servant.Servant.UserHistory.Models;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Servants.Search;
using Library.Servant.Servant.UserHistory;
using InspiniaWebAdminTemplate.Extension;

namespace InspiniaWebAdminTemplate.Controllers.Role
{
    [Login]
    [FunctionAuthorize(ID = "UserHistory")]
    [ExceptionHandle]
    public class UserHistoryController : Controller
    {
        // GET: UserHistory
        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();
            IndexResult result = UserHistoryServant.Index((SearchModel)condition);

            IndexViewModel vm = new IndexViewModel
            {
                Items = result.Items.Select(m => (DetailsViewModel)m).ToList(),
                TotalAmount = result.TotalAmount,
                condition = condition
            };

            return View(vm);
        }

        [HttpGet]
        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult(Url.Action("Index") + "#page");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(SearchViewModel vm)
        {
            this.UpdateSearchCondition<SearchViewModel>(vm);
            return RedirectToAction("Index");
        }

        private IUserHistoryServant UserHistoryServant = ServantAbstractFactory.UserHistory();
    }
}