﻿using System.Linq;
using System.Web.Mvc;
using Library.Entity.Edmx;
using Library.Servant.Enums;
using System.Collections.Generic;
using Library.Servant.Servant.UserRole;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.UserRole.Models;
using InspiniaWebAdminTemplate.Models.UserRole;
using InspiniaWebAdminTemplate.Servants.Search;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.UserInformation.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.Common;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [FunctionAuthorize(ID = "UserRole")]
    [ExceptionHandle]
    public class UserRoleController : Controller
    {
        private IEnumerable<SelectListItem> _statusList
        {
            get {
                return StaticDataServant.GetApprovalTypes().Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.Value
                });
            }
        }

        private IEnumerable<SelectListItem> _typeList
        {
            get {
                return StaticDataServant.GetUserTransactionTypes().Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.Value
                });
            }
        }
        
        private IEnumerable<SelectListItem> _departments
        {
            get {
                return StaticDataServant.GetDepts().Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.Value
                });
            }
        }

        private IEnumerable<SelectListItem> _branchs
        {
            get {
                return StaticDataServant.GetBranchs().Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.Value
                });
            }
        }

        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();
            
            IndexResult result = Servant.Index( (SearchModel)condition);

            ViewBag.Branchs = new SelectList(_branchs, "Value", "Text", condition.Branch);
            ViewBag.Departments = new SelectList(_departments, "Value", "Text", condition.Department);

            IndexViewModel vm = new IndexViewModel
            {
                Items = result.Items.AsEnumerable().Select(m => (ItemViewModel)m).ToList(),
                TotalAmount = result.TotalAmount,
                Condition = condition
            };

            return View( vm );
        }

        [HttpGet]
        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult(Url.Action("Index") + "#page");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(SearchViewModel vm)
        {
            this.UpdateSearchCondition<SearchViewModel>(vm);
            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public ActionResult Create(string userCode, string flowCode)
        {
            var vm = (DetailViewModel)Servant.Detail(userCode, flowCode);

            ViewBag.CanEdit = true;
            ViewBag.CanQuery = true;
            ViewBag.HasNoData = (!string.IsNullOrEmpty(userCode) && string.IsNullOrEmpty(vm.UserCode));

            return View("Detail", vm);
        }
        
        [HttpGet]
        public ActionResult Query(string userCode, string flowCode)
        {
            var vm = (DetailViewModel)Servant.Detail(userCode, flowCode);
            vm.FlowCode = flowCode;

            ViewBag.CanEdit = true;
            ViewBag.CanQuery = false;
            ViewBag.HasNoData = false;
            ViewBag.FlowCode = flowCode;

            if (string.IsNullOrEmpty(vm.UserCode)) {                
                return RedirectToAction("Index", new { flowCode = flowCode });
            }

            return View("Detail", vm);
        }

        [HttpGet]
        public ActionResult Show(string userCode, string flowCode)
        {
            var vm = (DetailViewModel)Servant.Detail(userCode, flowCode);
            vm.FlowCode = flowCode;

            ViewBag.CanEdit = false;
            ViewBag.CanQuery = false;
            ViewBag.HasNoData = false;
            ViewBag.FlowCode = flowCode;

            if (string.IsNullOrEmpty(vm.UserCode)) {
                return RedirectToAction("Index", new { flowCode = flowCode });
            }

            return View("Detail", vm);
        }

        [HttpGet]
        public ActionResult Edit(string userCode, bool isSubmited, string flowCode)
        {
            var vm = (DetailViewModel)Servant.Detail(userCode, flowCode);
            vm.FlowCode = flowCode;
            
            ViewBag.CanEdit = !isSubmited;
            ViewBag.CanQuery = false;
            ViewBag.HasNoData = false;
            ViewBag.FlowCode = flowCode;

            return View("Detail", vm);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Submit(DetailViewModel vm)
        {
            vm.HeaderInfo.AppliedUserId = this.UserID();
            FormType type = FormType.Undefined;
            string formName = "";

            switch (vm.HeaderInfo.Type) {
                case FormType.RoleCreate:
                    type = vm.HeaderInfo.IsModified ? FormType.RoleCreateWithModified : FormType.RoleCreate;
                    formName = FormType.RoleCreate.GetFormName();
                    break;
                case FormType.RoleRotation:
                    type = vm.HeaderInfo.IsModified ? FormType.RoleRotationWithModified : FormType.RoleRotation;
                    formName = FormType.RoleRotation.GetFormName();
                    break;
                case FormType.RoleOther:
                    type = vm.HeaderInfo.IsModified ? FormType.RoleOtherWithModified : FormType.RoleOther;
                    formName = FormType.RoleOther.GetFormName();
                    break;
                default:
                    return RedirectToAction("Index");
            }

            vm.HeaderInfo.FlowCode = type.GetNewFormNO();
            vm.HeaderInfo.Type = type;
            Servant.Submit((DetailModel)vm);

            var docID = AS_Doc_Name_Records.First(m => m.Doc_Name == formName).ID;
            LocalApprovalTodoServant.Create(this.UserID(), docID, vm.HeaderInfo.FlowCode, "UserRole");
            LocalApprovalTodoServant.Start(this.UserID(), vm.HeaderInfo.FlowCode);

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string userCode, string flowCode)
        {
            Servant.Delete(userCode, flowCode);

            return RedirectToAction("Index");
        }
        
        private IUserRoleServant Servant = ServantAbstractFactory.UserRole();
    }
}
