﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FastReport.Web;
using InspiniaWebAdminTemplate.Extension;
using Library.Entity.Edmx;

namespace InspiniaWebAdminTemplate.Controllers.Role
{
    public class DepartmentController : Controller
    {
        // GET: Department
        public ActionResult Index()
        {
            WebReport webReport = new WebReport();
            webReport.Width = 1000;
            webReport.Bind(Server.FrxPath("DepartmentStatisticReport.frx"), new List<AS_VW_Department_Report>());
            webReport.Report.SetParameterValue("Creator", this.User.Identity.Name);
            webReport.Report.SetParameterValue("CreateDate", DateTime.Now);

            ViewBag.WebReport = webReport;
            return View();
        }
    }
}