﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Extension;
using InspiniaWebAdminTemplate.Models.UserRole;
using Library.Servant.Servant.UserRole.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.UserRole;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers.Role
{
    public class QueryAuthAppliedStatusController : Controller
    {
        private IEnumerable<SelectListItem> _statusList
        {
            get {
                return StaticDataServant.GetApprovalTypes().Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.Value
                });
            }
        }

        private IEnumerable<SelectListItem> _typeList
        {
            get {
                return StaticDataServant.GetUserTransactionTypes().Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.Value
                });
            }
        }


        [HttpGet]
        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult(Url.Action("Index") + "#page");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(SearchViewModel vm)
        {
            this.UpdateSearchCondition<SearchViewModel>(vm);
            return RedirectToAction("Index");
        }

        [Login]
        [FunctionAuthorize(ID = "UserRole")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();

            IndexResult result = Servant.Index((SearchModel)condition);

            ViewBag.StatusList = new SelectList(_statusList, "Value", "Text", condition.Status);
            ViewBag.TypeList = new SelectList(_typeList, "Value", "Text", condition.Type);

            IndexViewModel vm = new IndexViewModel
            {
                Items = result.Items.AsEnumerable().Select(m => (ItemViewModel)m).ToList(),
                TotalAmount = result.TotalAmount,
                Condition = condition
            };

            return View(vm);
        }

        private IUserRoleServant Servant = ServantAbstractFactory.UserRole();
    }
}