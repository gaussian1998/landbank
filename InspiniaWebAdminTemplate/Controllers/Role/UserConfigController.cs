﻿using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.UserConfig;
using Library.Servant.Servant.UserConfig;
using Library.Servant.Servant.UserConfig.Models;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Extension;

namespace InspiniaWebAdminTemplate.Controllers.Role
{
    [Login]
    [FunctionAuthorize(ID = "UserConfig")]
    [ExceptionHandle]
    public class UserConfigController : Controller
    {
        public ActionResult Index()
        {
            return View((IndexViewModel)UserConfigServant.Index(this.UserID()));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(IndexViewModel updates)
        {
            UserConfigServant.Update(new UserInfoModel {
                UserCode = updates.UserCode,
                UserName = updates.UserName,
                BranchName = updates.BranchName,
                Department = updates.Department,
                IsEnable = updates.IsEnable,
                IsManager = updates.IsManager,
                Title = updates.Title,
                LeaveDate = updates.LeaveDate,
                Email = updates.Email,
                Remark = updates.Remark,
                DeputyRecordID = updates.DeputyRecordID,
                DeputyID = updates.DeputyID,
                DeputyName = updates.DeputyName,
                StartDeputy = updates.StartDeputy,
                EndDeputy = updates.EndDeputy
            });
            return RedirectToAction("Index");
        }

        private IUserConfigServant UserConfigServant = ServantAbstractFactory.UserConfig();
    }
}