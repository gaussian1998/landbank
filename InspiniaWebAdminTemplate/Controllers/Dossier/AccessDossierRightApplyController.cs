﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Dossier
{
    public class AccessDossierRightApplyController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "AccessDossierRightApply")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }
    }
}