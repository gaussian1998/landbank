﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Dossier
{
    public class DossierRightMaintainController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        public ActionResult Detail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        public ActionResult ContentDetail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        public ActionResult Edit()
        {
            return View();
        }
    }
}