﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Dossier
{
    public class DossierAddedController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "DossierAdded")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }
    }
}