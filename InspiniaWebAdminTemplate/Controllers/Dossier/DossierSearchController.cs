﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Dossier
{
    public class DossierSearchController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "DossierSearch")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "DossierSearch")]
        [ExceptionHandle]
        public ActionResult Detail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "DossierSearch")]
        [ExceptionHandle]
        public ActionResult ContentDetail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "DossierSearch")]
        [ExceptionHandle]
        public ActionResult AssetDetail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "DossierSearch")]
        [ExceptionHandle]
        public ActionResult Edit()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "DossierSearch")]
        [ExceptionHandle]
        public ActionResult ContentEdit()
        {
            return View();
        }

    }
}