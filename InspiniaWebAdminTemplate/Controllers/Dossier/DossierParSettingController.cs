﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Dossier
{
    public class DossierParSettingController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "DossierParSetting")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }
    }
}