﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Dossier
{
    public class DossierReportController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "DossierReport")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }
    }
}