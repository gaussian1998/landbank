﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Dossier
{
    public class DossierMaintainController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "DossierMaintain")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "DossierMaintain")]
        [ExceptionHandle]
        public ActionResult Detail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "DossierMaintain")]
        [ExceptionHandle]
        public ActionResult ContentDetail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "DossierMaintain")]
        [ExceptionHandle]
        public ActionResult Edit()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "DossierMaintain")]
        [ExceptionHandle]
        public ActionResult ContentEdit()
        {
            return View();
        }
    }
}