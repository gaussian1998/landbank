﻿using System.Web.Mvc;
using System.Collections.Generic;
using InspiniaWebAdminTemplate.Models.Example;
using Library.Common.Web.Utility;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.UserInformation.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.ApprovalTodo;
using Library.Entity.Edmx;
using FastReport.Web;
using System.Data;
using Library.Utility;
using InspiniaWebAdminTemplate.Extension;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    public class ExampleController : Controller
    {
        private UserInfor user;
        public ExampleController()
        {
            user = UserInfoServant.Details(System.Web.HttpContext.Current);
        }

        public ActionResult TestUser()
        {
            return Content("id =" + user.ID);
        }

        public ActionResult FormAdvanced()
        {
            return View();
        }

        public ActionResult Table()
        {
            return View();
        }

        public ActionResult MultiSelect()
        {
            return View();
        }

        public ActionResult MultiSelect2()
        {
            return View();
        }

        public ActionResult CascadeSelect()
        {
            return View();
        }

        

        public ActionResult TreeView()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SubmitMultiSelect(List<int> Right)
        {
            return Content("SubmitMultiSelect :" + ( Right==null ? "null" : Right.Count.ToString() ) );
        }

        public ActionResult Icon()
        {
            return View();
        }
        public ActionResult Page()
        {
            return View();
        }



        public ActionResult UpdatePage(int page)
        {
            return Content("UpdatePage#" + page);
        }



        public ActionResult Example()
        {
            return View( new ExampleViewModel {

                RentBase = new RentBase {
                    unit = "土銀",
                    number = 500,
                    date = "2016/03/25",
                },

                RentDateTime = new RentDateTime()
            } );
        }

        public ActionResult Login()
        {
            var cookie = new EncryptionCookie(HttpContext.ApplicationInstance.Context);

            cookie.Update("x","1");
            return Content("Login");
        }

        public ActionResult Use()
        {
            var cookie = new EncryptionCookie(HttpContext.ApplicationInstance.Context);

            return Content("Use " + cookie.Get("x"));
        }

        public ActionResult Logout()
        {
            var cookie = new EncryptionCookie(HttpContext.ApplicationInstance.Context);
            cookie.Delete("x");

            return Content("Logout");
        }

        public ActionResult CreateExampleFlow()
        {
            string docNo = AS_Doc_Name_Records.First(m => m.Doc_Name == "人員權限新增單").Doc_No;
            string FormNo = LocalApprovalTodoServant.Create( this.UserID(), docNo, "Test", "example");
 
            LocalApprovalTodoServant.Start( this.UserID(), FormNo);
            return Content("CreateExampleFlow");
        }


        public ActionResult FormPartialView(string Data)
        {
            ViewBag.Title = Data;
            return PartialView();
        }

        public ActionResult Form(int a, int b)
        {
            ViewBag.Title = "a" + " + b = " + (a+b) ;
            return View("FormPartialView");
        }

        public ActionResult FastReport()
        {
            DataSet source = new DataSet();
            source.Tables.Add( MapProperty.ToDataTable("AS_Users", new List<User>
            {
                new User { User_Name = "Qoo" },
                new User { User_Name = "Orz" },
            }) );

            var webReport = new WebReport();
            webReport.Report.Load(System.IO.Path.Combine(Server.MapPath("~/App_Data"), "example.frx"));
            webReport.RegisterData(source);
            webReport.Width = 1000;
            ViewBag.WebReport = webReport;
            return View();
        }
    }

    class User
    {
        public string User_Name { get; set; }
    }
}