﻿using System.Collections.Generic;
using System.Web;
using System.Linq;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Service;
using Library.Interface;
using Library.Servant.Servant.Common;
using InspiniaWebAdminTemplate.Attributes;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    public class ExampleUploadController : Controller
    {
        public ActionResult Submit(List<long> ThumbList)
        {
            return RedirectToAction("Index");
        }
        
        public ActionResult Index()
        {
            return View( FakeFileServant.FindAll().Select( file => new ExampleFileViewModel { Id = file.Id, Src = Url.Action("Details", new { filename = file.Filename }) } ) );
        }

        [HttpPost]
        public ActionResult Create(HttpPostedFileBase file)
        {
            if (file == null)
            {
                return Json(new { OK = false });
            }
            else
            {
                long Id = FakeFileServant.Create( id => Save(file, id) );
                return Json(new { OK = true, Id = Id, Src = Url.Action("Details", new { filename = Encode( Id, file.FileName) }) });
            }
        }

        //<img src="~/ExampleUpload/Details?filename=123.png">
        public ActionResult Details(string filename)
        {
            IStorageService stroage = StorageServant.Create(Server);

            return new FileContentResult(stroage.Load(filename), "application/octet-stream");
        }

        [HttpPost]
        public ActionResult Delete(long Id)
        {
            IStorageService stroage = StorageServant.Create(Server);

            stroage.Delete(Id);
            return Json(new { Id = Id });
        }

        private string Save(HttpPostedFileBase file, long id)
        {
            IStorageService stroage = StorageServant.Create(Server);
            stroage.Save( file.InputStream, Encode( id, file.FileName));

            return Encode(id, file.FileName);
        }
        
        private static string Encode(long id, string uploadFileName)
        {
            return string.Format("{0}_{1}", id, uploadFileName);
        }
    }

    public class ExampleFileViewModel
    {
        public long Id { get; set; }
        public string Src { get; set; }
    }
}