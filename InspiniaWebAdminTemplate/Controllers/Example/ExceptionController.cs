﻿using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.Exceptions;
using System;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers
{
    // 客製化例外處理
    [ExceptionHandle]
    public class ExceptionController : Controller
    {
        // 測試網址
        // http://{domain_name}/Exception
        public ActionResult Index()
        {
            throw new Exception("直接丟出例外!!!!!!!!!!!!!!!!!!!");

            return Content("**不可能執行到這行**"); ;
        }

        public ActionResult NotSupported()
        {
            ExceptionServant.NotSupported();
            return View();
        }

        public ActionResult InvalidOperation()
        {
            ExceptionServant.InvalidOperation();
            return View();
        }

        public ActionResult NOT_NULL()
        {
            ExceptionServant.NOT_NULL();
            return View();
        }

        public ActionResult UNIQUE()
        {
            ExceptionServant.UNIQUE();
            return View();
        }


        private RemoteExceptionServant ExceptionServant = new RemoteExceptionServant();
        //private LocalExceptionServant ExceptionServant = new LocalExceptionServant();
    }
}