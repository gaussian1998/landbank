﻿using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Repository.Inspect;
using Library.Servant.Servant.Inspect;
using Library.Servant.Servant.Inspect.InspectModels;
using Library.Servant.Servants.AbstractFactory;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.AssetInspect
{
    public class AssetInspectSearchController : Controller
    {
        private IInspectServant InspectServant = ServantAbstractFactory.Inspect();

       
        int PageSize = 10;
        [Login]
        [FunctionAuthorize(ID = "AssetInspectSearch")]
        [ExceptionHandle]
        public ActionResult Index()
        {
          
            EInt type = new EInt();
            type.type = 6;
            EJObject so = this.InspectServant.DocumentType(type);
            type.type = 5;
            EJObject td = this.InspectServant.DocumentType(type);
            EString code = new EString();
            code.type = "V01";
            EJObject bc = this.InspectServant.GetCode(code);
            code.type = "C01";
            EJObject ju1 = this.InspectServant.GetCode(code);
            code.type = "C02";
            EJObject ju2 = this.InspectServant.GetCode(code);
            code.type = "T02";
            EJObject ty = this.InspectServant.GetCode(code);
           
            ViewBag.DocumentType = td.Obj;
            ViewBag.Source_Type = so.Obj;
            ViewBag.DateNow = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.MGDateNow = InspectGeneral.CvtD2S(DateTime.Today).Replace("-", "/");
            ViewBag.bc = bc.Obj;
            ViewBag.ju1 = ju1.Obj;
            ViewBag.ju2 = ju2.Obj;
            ViewBag.ty = ty.Obj;
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "AssetInspectSearch")]
        [ExceptionHandle]
        public ActionResult IndexList(string json = "", int page = 1)
        {

            EJObject con = new EJObject();
            var condata = JObject.Parse(json);
            condata["Page"] = page;
            condata["PageSize"] = PageSize;
            con.Obj = condata;
            if (condata["Document_Type"] == null)
                return null;
            EJObject tiq = new EJObject();
            if (condata["Document_Type"].ToString() == "5001")
            {
                tiq = this.InspectServant.Index(con);
                ViewBag.Action = "300";
            }
            else
            {
                tiq = this.InspectServant.IndexA(con);
                ViewBag.Action = "200";
            }
            if (json != "")
                ViewBag.test = condata;
            else
                ViewBag.test = "NULL";

            EInt tpb = new EInt();
            tpb.type = 1;
            EJObject PB = this.InspectServant.GetDPM(tpb);

            for (int j = 0; j < tiq.Obj["Data"].Count(); j++)
            {
                tiq.Obj["Data"][j]["J1"] = "總行";

            }
            ViewBag.data = tiq.Obj;
            TempData["amount"] = tiq.Obj["amount"]; //筆數
            TempData["pagestart"] = tiq.Obj["pagestart"]; //變數+1 就是起始序號
            TempData["pagecount"] = tiq.Obj["pagecount"]; //一頁幾筆
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "AssetInspectSearch")]
        [ExceptionHandle]
        public ActionResult Detail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "AssetInspectSearch")]
        [ExceptionHandle]
        public ActionResult ContentDetail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "AssetInspectSearch")]
        [ExceptionHandle]
        public ActionResult ContentDetail_2()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "AssetInspectSearch")]
        [ExceptionHandle]
        public ActionResult CheckDetail()
        {
            return View();
        }
    }
}