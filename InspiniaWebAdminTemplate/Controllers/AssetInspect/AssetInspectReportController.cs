﻿using FastReport.Web;
using InspiniaWebAdminTemplate.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Servant.Inspect;
using Library.Servant.Servant.Inspect.InspectModels;
using Newtonsoft.Json.Linq;
using Library.Servant.Servants.AbstractFactory;
using System.Web.UI.WebControls;

namespace InspiniaWebAdminTemplate.Controllers.AssetInspect
{
    public class AssetInspectReportController : Controller
    {
        private IInspectServant InspectServant = ServantAbstractFactory.Inspect();
        [Login]
        [FunctionAuthorize(ID = "AssetInspectReport")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            
            EJObject condition = new EJObject();
            condition.Obj = new JObject();
                  EJObject tiq = this.InspectServant.GetReport(condition);

           
            string ReportName = "BibiDemo";
            string DataJson =tiq.Obj["Data"].ToString();

            DataTable dt = (DataTable)JsonConvert.DeserializeObject(DataJson, (typeof(DataTable)));
            WebReport WR = new WebReport();
           //DataSet myDataSet = JsonConvert.DeserializeObject<DataSet>(DataJson);
            WR.Report.RegisterData(dt, "Data");
            WR.Report.Load(Server.MapPath("~/App_Data/Reports/" + ReportName + ".frx"));
           WR.Width = Unit.Percentage(100);
            WR.Height = Unit.Percentage(100);
            ViewBag.test =  tiq.Obj["Data"].ToString() + dt.Rows.Count.ToString();
            ViewBag.Report = WR.GetHtml();
            return View();
        }
    
    }
}