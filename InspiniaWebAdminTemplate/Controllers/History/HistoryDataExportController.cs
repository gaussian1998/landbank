﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Extension;
using InspiniaWebAdminTemplate.Models.HistoryDataExport;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.HistoryData;
using Library.Servant.Servant.HistoryData.Models;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers.History
{
    [Login]
    [FunctionAuthorize(ID = "HistoryDataExport")]
    [ExceptionHandle]
    public class HistoryDataExportController : Controller
    {
        private List<SelectListItem> _transactionList
        {
            get {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text="土地", Value="1" },
                    new SelectListItem { Text="房屋", Value="2" },
                    new SelectListItem { Text="動產", Value="3" }
                };
            }
        }
        
        // GET: HistoryDataExport
        public ActionResult Index()
        {
            var condition = this.GetSearchCondition<IndexSearchViewModel>();
            var result = servant.GetHistory((HistoryCondition)condition);
            
            return View(new IndexViewModel {
                Items = result.Items.Select(m => (IndexItemViewModel)m).ToList(),
                TotoalAmount = result.TotalAmount,
                TransactionOptions = new SelectList(_transactionList, "Value", "Text", condition?.TransactionCode),
                TableOptions = new SelectList(servant.GetHistoryList().Content, "Value", "Text", condition?.TableName),
                Condition = condition
            });
        }

        public ActionResult UpdatePage(int page)
        {
            var condition = this.GetSearchCondition<IndexSearchViewModel>();
            condition.Page = page;
            this.UpdateSearchCondition<IndexSearchViewModel>(condition);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(IndexSearchViewModel vm)
        {
            this.UpdateSearchCondition<IndexSearchViewModel>(vm);

            return RedirectToAction("Index");
        }

        public ActionResult Export(string table)
        {
            return File(servant.GetCsvContent(table).Value,
                "application/csv",
                string.Format("{0}.csv", table));
        }

        private IHistoryData servant = ServantAbstractFactory.HistoryData();
    }
}