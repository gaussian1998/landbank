﻿using InspiniaWebAdminTemplate.Attributes;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [FunctionAuthorize(ID = "HistoryDataStatist")]
    [ExceptionHandle]
    public class HistoryDataStatisticsController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult New(string type)
        {
            if (type == "Q110") {
                return View("MovablePropertyHistoryCreate");
            }
            else if (type == "Q120") {
                return View("LandHistoryCreate");
            }
            else if (type == "Q130") {
                return View("HouseHistoryCreate");
            }
            else {
                return Redirect("Index");
            }
        }
    }
}