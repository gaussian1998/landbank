﻿using InspiniaWebAdminTemplate.Attributes;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [FunctionAuthorize(ID = "HistoryData")]
    [ExceptionHandle]
    public class HistoryDataController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}