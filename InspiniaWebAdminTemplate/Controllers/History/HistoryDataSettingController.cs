﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.HistoryDataSetting;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.HistoryData;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.HistoryData.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Controllers.History
{
    [Login]
    [FunctionAuthorize(ID = "HistoryDataSetting")]
    [ExceptionHandle]
    public class HistoryDataSettingController : Controller
    {
        private List<SelectListItem> _list
        {
            get {
                return StaticDataServant.GetDocTypes().Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.Value
                }).ToList();
            }
        }

        // GET: HistoryDataSetting
        public ActionResult Index()
        {
            IndexSearchViewModel condition = this.GetSearchCondition<IndexSearchViewModel>();

            var vm = (IndexViewModel)servant.GetSettings((SettingCondition)condition);
            vm.Condition = condition;
            vm.TrasactionList = new SelectList(_list, "Value", "Text", vm.Condition?.TrasactionCode);

            return View(vm);
        }

        public ActionResult UpdatePage(int page)
        {
            var condition = this.GetSearchCondition<IndexSearchViewModel>();
            condition.Page = page;
            this.UpdateSearchCondition<IndexSearchViewModel>(condition);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(IndexSearchViewModel vm)
        {
            this.UpdateSearchCondition<IndexSearchViewModel>(vm);
            return RedirectToAction("Index");
        }

        public ActionResult Save(List<IndexItemViewModel> Items)
        {
            servant.SaveSettings(new ListRemoteModel<SettingItemsInfo> { Content = Items.Select(m => (SettingItemsInfo)m).ToList() });
            return RedirectToAction("Index");
        }

        private IHistoryData servant = ServantAbstractFactory.HistoryData();
    }
}