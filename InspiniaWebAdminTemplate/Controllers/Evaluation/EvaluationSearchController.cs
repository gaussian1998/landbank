﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Evaluation
{
    public class EvaluationSearchController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "EvaluationSearch")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "EvaluationSearch")]
        [ExceptionHandle]
        public ActionResult Detail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "EvaluationSearch")]
        [ExceptionHandle]
        public ActionResult Notice()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "EvaluationSearch")]
        [ExceptionHandle]
        public ActionResult ContentDetail()
        {
            return View();
        }


        [Login]
        [FunctionAuthorize(ID = "EvaluationSearch")]
        [ExceptionHandle]
        public ActionResult ContentDetail_1()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "EvaluationSearch")]
        [ExceptionHandle]
        public ActionResult ContentDetail_2()
        {
            return View();
        }
    }
}