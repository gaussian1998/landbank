﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Evaluation
{
    public class EvaluationReportController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "EvaluationReport")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }
    }
}