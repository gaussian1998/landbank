﻿using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using InspiniaWebAdminTemplate.Service;
using Library.Interface;
using Library.Servant.Repository.Evaluation;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.Evaluation;
using Library.Servant.Servant.Evaluation.EvaluationModels;
using Library.Servant.Servant.UserInformation.Models;
using Library.Servant.Servants.AbstractFactory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Evaluation
{
    public class BuildEvaluationController : Controller
    {
        private IEvaluationServant EvaluationServant = ServantAbstractFactory.Evaluation();

        private string Type = "H300";
        int PageSize = 10;

        [Login]
        [FunctionAuthorize(ID = "BuildEvaluation")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            EInt type = new EInt();
            type.type = 8;
            EJObject td = this.EvaluationServant.DocumentType(type);
            EString code = new EString();
            code.type = "V01";
            EJObject bc = this.EvaluationServant.GetCode(code);
            type.type = 1;
            EJObject ju1 = this.EvaluationServant.GetDPM(type);
            type.type = 2;
            EJObject ju2 = this.EvaluationServant.GetDPM(type);
            code.type = "T02";
            EJObject ty = this.EvaluationServant.GetCode(code);

            ViewBag.DocumentType = td.Obj;
            ViewBag.DateNow = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.MGDateNow = EvaluationGeneral.CvtD2S(DateTime.Today).Replace("-", "/");
            ViewBag.bc = bc.Obj;
            ViewBag.ju1 = ju1.Obj;
            ViewBag.ju2 = ju2.Obj;
            ViewBag.ty = ty.Obj;
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "BuildEvaluation")]
        [ExceptionHandle]
        public ActionResult IndexList(string json = "", int page = 1)
        {

            EJObject con = new EJObject();
            var condata = JObject.Parse(json);
            condata["Page"] = page;
            condata["PageSize"] = PageSize;
            con.Obj = condata;
            EJObject tiq = this.EvaluationServant.Index(con);
            if (json != "")
                ViewBag.test = condata;
            else
                ViewBag.test = "NULL";

            EInt tpb = new EInt();
            tpb.type = 1;
            EJObject PB = this.EvaluationServant.GetDPM(tpb);

            for (int j = 0; j < tiq.Obj["Data"].Count(); j++)
            {
                tiq.Obj["Data"][j]["J1"] = "總行";
                tiq.Obj["Data"][j]["sEndDate"] = tiq.Obj["Data"][j]["EndDate"].ToString() == "" ? "" :
                    (((DateTime)tiq.Obj["Data"][j]["EndDate"]).Year - 1911).ToString()+((DateTime)tiq.Obj["Data"][j]["EndDate"]).ToString("-MM-dd");
            }
            ViewBag.data = tiq.Obj;
            TempData["amount"] = tiq.Obj["amount"]; //筆數
            TempData["pagestart"] = tiq.Obj["pagestart"]; //變數+1 就是起始序號
            TempData["pagecount"] = tiq.Obj["pagecount"]; //一頁幾筆
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "BuildEvaluation")]
        [ExceptionHandle]
        public ActionResult Detail()
        {
            string ID = "";
            if (Url.RequestContext.RouteData.Values["id"] != null)
                ID = Url.RequestContext.RouteData.Values["id"].ToString();
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            EJObject us = new EJObject();
            us.Obj = new JObject();
            us.Obj["UserID"] = user.ID;

            EJObject U = this.EvaluationServant.GetUser(us);
            ViewBag.User = U.Obj["Data"][0];

            EInt type = new EInt();

            EString code = new EString();
            code.type = "V01";
            EJObject bc = this.EvaluationServant.GetCode(code);
            type.type = 1;
            EJObject ju1 = this.EvaluationServant.GetDPM(type);
            type.type = 2;
            EJObject ju2 = this.EvaluationServant.GetDPM(type);

            type.type = 8;
            EJObject td = this.EvaluationServant.DocumentType(type);

            ViewBag.DocumentType = td.Obj;
            ViewBag.DateNow = DateTime.Today.ToString("yyyy/MM/dd");
            ViewBag.MGDateNow = EvaluationGeneral.CvtD2S(DateTime.Today).Replace("-", "/");
            ViewBag.bc = bc.Obj;
            ViewBag.ju1 = ju1.Obj;
            ViewBag.ju2 = ju2.Obj;
            ViewBag.ID = ID;


            EString edd = new EString();
            edd.type = ID;
            EJObject EdOrd = this.EvaluationServant.GetOrder(edd);
            //ViewBag.No = EdOrd.Obj["Data"][1]["mh"]["Activation_No"];
            for (int i = 0; i < ju2.Obj["Data"].Count(); i++)
            {
                if (EdOrd.Obj["Data"][0]["mh"]["Office_Branch"].ToString() == ju2.Obj["Data"][i]["mh"]["Department"].ToString())
                {
                    EdOrd.Obj["Data"][0]["mh"]["Office_Branch"] = ju2.Obj["Data"][i]["mh"]["Department_Name"].ToString();
                }
            }
            for (int i = 0; i < ju1.Obj["Data"].Count(); i++)
            {
                if (EdOrd.Obj["Data"][0]["mh"]["Office_Branch"].ToString() == ju1.Obj["Data"][i]["mh"]["Department"].ToString())
                {
                    EdOrd.Obj["Data"][0]["mh"]["Office_Branch"] = ju1.Obj["Data"][i]["mh"]["Department_Name"].ToString();
                }
            }
            for (int i = 0; i < td.Obj["Data"].Count(); i++)
            {
                if (EdOrd.Obj["Data"][0]["mh"]["Source_Type"].ToString() == td.Obj["Data"][i]["mh"]["ID"].ToString())
                {
                    EdOrd.Obj["Data"][0]["mh"]["Source_Type"] = td.Obj["Data"][i]["mh"]["Name"].ToString();
                }
            }
            ViewBag.isnew = false;
            ViewBag.EdOrd = EdOrd.Obj["Data"][0];
            ViewBag.No = EdOrd.Obj["Data"][0]["mh"]["TRX_Header_ID"];
            ViewBag.State = EdOrd.Obj["Data"][0]["TY"];
            TempData["RealID"] = ID;
            TempData["RealNo"] = EdOrd.Obj["Data"][0]["mh"]["TRX_Header_ID"];

            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "BuildEvaluation")]
        [ExceptionHandle]
        public ActionResult DetailH(string json = "{}", int page = 1, int customsize = 10, string AID = "0", int V = 0)
        {
            if (json == "") json = "{}";
            EJObject con = new EJObject();
            var condata = JObject.Parse(json);


            condata["Page"] = page;
            condata["PageSize"] = customsize;

            condata["AID"] = AID;
            con.Obj = condata;
            JArray btmp = new JArray();


            EJObject tiq = new EJObject();


            //AB.Obj["Data"].AddAfterSelf



            if (condata["ID"] == null)
            {
                if (btmp.Count() == 0)
                    btmp.Add(0);
                condata["ID"] = btmp;
                EJObject ABC = this.EvaluationServant.GetDetail(con);
                /*for (int i = 0; i < ABC.Obj["Data"].Count(); i++)
                {
                    btmp.Add(Int32.Parse(ABC.Obj["Data"][i]["Build_ID"].ToString()));
                }*/
                condata["ID"] = ABC.Obj["tmpq2"];
            }
            else
            {
                var tb = (JArray)condata["ID"];
                for (int i = 0; i < tb.Count(); i++)
                {
                    btmp.Add(Int32.Parse(tb[i].ToString()));
                }
                condata["ID"] = btmp;
            }
            if (btmp.Count() == 0)
            {
                btmp.Add(0);
                condata["ID"] = btmp;
            }
            con.Obj = condata;
            EJObject AB = this.EvaluationServant.GetDetail(con);




            ViewBag.data = AB.Obj;
            ViewBag.OrgID = condata["ID"].ToString().Replace(System.Environment.NewLine, "");
            ViewBag.V = V;
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "BuildEvaluation")]
        [ExceptionHandle]
        public ActionResult SaveF(string s)
        {
            JObject AS = new JObject();
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);

            AS["Data"] = JObject.Parse(s);
            //return RedirectToAction("ContentDetail", new { id = ss["Build"]["ID"] });
          

            AS["Last_Updated_By"] = user.ID.ToString();
            EJObject D = new EJObject();
            D.Obj = AS;
            EJObject A = this.EvaluationServant.UpdateDetail(D);
            // return RedirectToAction("ContentDetail", new { id = AS["Build"]["ID"] });
            return Content("{\"ID\":\"" + A.Obj["TRX_Header_ID"].ToString() + "\"}");
        }
        [Login]
        [FunctionAuthorize(ID = "BuildEvaluation")]
        [ExceptionHandle]
        public ActionResult ContentDetail()
        {
            string ID = "";
            if (Url.RequestContext.RouteData.Values["id"] != null)
                ID = Url.RequestContext.RouteData.Values["id"].ToString();
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            EJObject us = new EJObject();
            us.Obj = new JObject();
            us.Obj["UserID"] = user.ID;

            EJObject U = this.EvaluationServant.GetUser(us);
            ViewBag.User = U.Obj["Data"][0];

            EJObject con = new EJObject();
            con.Obj = new JObject(new JProperty("id", ID));
            EJObject CD = this.EvaluationServant.ContentDetail(con);
            EJObject con2 = new EJObject();
            con2.Obj = new JObject(new JProperty("BuildID", CD.Obj["Data"][0]["m"]["Build_ID"]));
            EJObject HL = this.EvaluationServant.GetHisIns(con2);
            ViewBag.ID = ID;
            ViewBag.HL = HL.Obj["Data"];

            EInt type = new EInt();
            type.type = 2;
            EJObject ju1 = this.EvaluationServant.GetDPM(type);
            type.type = 8;
            EJObject td = this.EvaluationServant.DocumentType(type);
            for (int i = 0; i < ju1.Obj["Data"].Count(); i++)
            {
                if (CD.Obj["Data"][0]["ma"]["Office_Branch"].ToString() == ju1.Obj["Data"][i]["mh"]["Department"].ToString())
                {
                    CD.Obj["Data"][0]["ma"]["Office_Branch"] = ju1.Obj["Data"][i]["mh"]["Department_Name"].ToString();
                }
            }
            for (int i = 0; i < td.Obj["Data"].Count(); i++)
            {
                if (CD.Obj["Data"][0]["ma"]["Source_Type"].ToString() == td.Obj["Data"][i]["mh"]["ID"].ToString())
                {
                    CD.Obj["Data"][0]["ma"]["Source_Type"] = td.Obj["Data"][i]["mh"]["Name"].ToString();
                }
            }
            if (CD.Obj["Data"][0]["ma"]["DeadLine"].ToString()!="")
            CD.Obj["Data"][0]["ma"]["sDeadLine"] = (((DateTime)CD.Obj["Data"][0]["ma"]["DeadLine"]).Year-1911).ToString()+
                                                    ((DateTime)CD.Obj["Data"][0]["ma"]["DeadLine"]).ToString(".MM.dd");

            if (CD.Obj["Data"][0]["ma"]["EndDate"].ToString() != "")
                CD.Obj["Data"][0]["ma"]["sEndDate"] = (((DateTime)CD.Obj["Data"][0]["ma"]["EndDate"]).Year - 1911).ToString() +
                                                   ((DateTime)CD.Obj["Data"][0]["ma"]["EndDate"]).ToString(".MM.dd");
            ViewBag.Data = CD.Obj["Data"][0];

            ViewBag.Data2 = CD.Obj["Data2"];
            ViewBag.Data3 = CD.Obj["Data3"];
            ViewBag.DataM = CD.Obj["Data"][0]["m"];
            ViewBag.DataM["BeginDate1"] = CD.Obj["Data"][0]["m"]["BeginDate"].ToString() != "" ?
                ((DateTime)CD.Obj["Data"][0]["m"]["BeginDate"]).ToString("yyyy/MM/dd") : "";
            ViewBag.DataM["EndDate1"] = CD.Obj["Data"][0]["m"]["EndDate"].ToString() != "" ?
                ((DateTime)CD.Obj["Data"][0]["m"]["EndDate"]).ToString("yyyy/MM/dd") : "";

            ViewBag.DataL = CD.Obj["Data"][0]["l"];
            ViewBag.DataL["Datetime_Placed_In_Service1"] = CD.Obj["Data"][0]["l"]["Date_Placed_In_Service"].ToString() != "" ?
               ((DateTime)CD.Obj["Data"][0]["l"]["Date_Placed_In_Service"]).ToString("yyyy/MM/dd") : "";

            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "BuildEvaluation")]
        [ExceptionHandle]
        public ActionResult SavePic(HttpPostedFileBase file, string UID)
        {
            if (file == null)
            {
                return Content("沒有選擇檔案");
            }
            else
            {
                EJObject PP = new EJObject();
                PP.Obj = new JObject();
                PP.Obj["ID"] = UID;
                PP.Obj["FileName"] = file.FileName;
                EJObject RP = this.EvaluationServant.SavePic(PP);
                long Id = Int64.Parse(RP.Obj["ID"].ToString());
                Save(file, Id);
                string pl = Encode(Id, file.FileName);
                return Content(pl);
            }
        }
        [Login]
        [FunctionAuthorize(ID = "BuildEvaluation")]
        [ExceptionHandle]
        public ActionResult LoadPic(string filename)
        {
            IStorageService stroage = StorageServant.Create(Server);

            return new FileContentResult(stroage.Load(filename), "application/octet-stream");
        }
        [Login]
        [FunctionAuthorize(ID = "BuildEvaluation")]
        [ExceptionHandle]
        public ActionResult GetPicList(string ID)
        {
            EJObject PP = new EJObject();
            PP.Obj = new JObject();
            PP.Obj["ID"] = ID;
            EJObject RP = this.EvaluationServant.GetPicList(PP);
            return Content(RP.Obj.ToString());
        }
        private string Save(HttpPostedFileBase file, long id)
        {
            IStorageService stroage = StorageServant.Create(Server);
            stroage.Save(file.InputStream, Encode(id, file.FileName));

            return Encode(id, file.FileName);
        }
        private static string Encode(long id, string uploadFileName)
        {
            return string.Format("{0}_{1}", id, uploadFileName);
        }
        [Login]
        [FunctionAuthorize(ID = "BuildEvaluation")]
        [ExceptionHandle]

        public ActionResult Edit(string id, string signmode)
        {
            string ID = "";
            if (Url.RequestContext.RouteData.Values["id"] != null)
                ID = Url.RequestContext.RouteData.Values["id"].ToString();
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            EJObject us = new EJObject();
            us.Obj = new JObject();
            us.Obj["UserID"] = user.ID;

            EJObject U = this.EvaluationServant.GetUser(us);
            ViewBag.User = U.Obj["Data"][0];

            EInt type = new EInt();

            EString code = new EString();
            code.type = "V01";
            EJObject bc = this.EvaluationServant.GetCode(code);
            type.type = 1;
            EJObject ju1 = this.EvaluationServant.GetDPM(type);
            type.type = 2;
            EJObject ju2 = this.EvaluationServant.GetDPM(type);

            type.type = 8;
            EJObject td = this.EvaluationServant.DocumentType(type);
            ////////////這邊是彈框搜尋要的東西///////////////
            type.type = 1;
            EJObject ck = this.EvaluationServant.GetCategoryKind(type);
            EJObject ct = this.EvaluationServant.GetCategoryType(type);
            ViewBag.ck = ck.Obj["Data"];
            ViewBag.ct = ct.Obj["Data"];
            /////////////////////////////////////////////////
            ViewBag.DocumentType = td.Obj;
            ViewBag.DateNow = DateTime.Today.ToString("yyyy/MM/dd");
            ViewBag.MGDateNow = EvaluationGeneral.CvtD2S(DateTime.Today).Replace("-", "/");
            ViewBag.bc = bc.Obj;
            ViewBag.ju1 = ju1.Obj;
            ViewBag.ju2 = ju2.Obj;
            ViewBag.ID = ID;
            EJObject tmp = new EJObject();
            tmp.Obj = JObject.Parse("{}");
            EJObject MaxNo = this.EvaluationServant.GetMaxNo(tmp);
            if (ID == "")
            {
                ViewBag.isnew = true;
                string tmp1 = "000001";
                if (MaxNo.Obj["Data"].Count() > 0)
                    tmp1 = MaxNo.Obj["Data"][0]["maxno"].ToString().PadLeft(6, '0');
                ViewBag.No = EvaluationGeneral.CvtD2S(DateTime.Today).Replace("-", "") + "H300" + tmp1; ViewBag.State = "新增";
                TempData["RealNo"] = EvaluationGeneral.CvtD2S(DateTime.Today).Replace("-", "") + "H300";
                TempData["RealID"] = null;
                ViewBag.ID = "0";
            }
            else
            {
                EString edd = new EString();
                edd.type = ID;
                EJObject EdOrd = this.EvaluationServant.GetOrder(edd);
                //ViewBag.No = EdOrd.Obj["Data"][1]["mh"]["Activation_No"];
                if ((EdOrd.Obj["Data"].Count() <= 0) ||
                    ((EdOrd.Obj["Data"][0]["mh"]["Type"].ToString() != "0") &&
                    (EdOrd.Obj["Data"][0]["mh"]["Type"].ToString() != "3") &&
                    (EdOrd.Obj["Data"][0]["mh"]["Type"].ToString() != "4")))
                {
                    return RedirectToAction("Index");
                }
                if (EdOrd.Obj["Data"][0]["mh"]["EndDate"].ToString() != "")
                    EdOrd.Obj["Data"][0]["mh"]["sEndDate"] = ((DateTime)EdOrd.Obj["Data"][0]["mh"]["EndDate"]).ToString("yyyy/MM/dd");
                if (EdOrd.Obj["Data"][0]["mh"]["DeadLine"].ToString() != "")
                    EdOrd.Obj["Data"][0]["mh"]["sDeadLine"] = ((DateTime)EdOrd.Obj["Data"][0]["mh"]["DeadLine"]).ToString("yyyy/MM/dd");
                ViewBag.isnew = false;
                ViewBag.EdOrd = EdOrd.Obj["Data"][0];
                ViewBag.No = EdOrd.Obj["Data"][0]["mh"]["TRX_Header_ID"];
                ViewBag.State = EdOrd.Obj["Data"][0]["TY"];
                TempData["RealID"] = ID;
                TempData["RealNo"] = EdOrd.Obj["Data"][0]["mh"]["TRX_Header_ID"];
            }
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "BuildEvaluation")]
        [ExceptionHandle]
        public ActionResult EditH(string json = "{}", int page = 1, int customsize = 10, string AID = "0", int V = 0)
        {
            if (json == "") json = "{}";
            EJObject con = new EJObject();
            var condata = JObject.Parse(json);


            condata["Page"] = page;
            condata["PageSize"] = customsize;

            condata["AID"] = AID;
            con.Obj = condata;
            JArray btmp = new JArray();


            EJObject tiq = new EJObject();


            //AB.Obj["Data"].AddAfterSelf



            if (condata["ID"] == null)
            {
                if (btmp.Count() == 0)
                    btmp.Add(0);
                condata["ID"] = btmp;
                EJObject ABC = this.EvaluationServant.GetDetail(con);
                /*for (int i = 0; i < ABC.Obj["Data"].Count(); i++)
                {
                    btmp.Add(Int32.Parse(ABC.Obj["Data"][i]["Build_ID"].ToString()));
                }*/
                condata["ID"] = ABC.Obj["tmpq2"];
            }
            else
            {
                var tb = (JArray)condata["ID"];
                for (int i = 0; i < tb.Count(); i++)
                {
                    btmp.Add(Int32.Parse(tb[i].ToString()));
                }
                condata["ID"] = btmp;
            }
            if (btmp.Count() == 0)
            {
                btmp.Add(0);
                condata["ID"] = btmp;
            }
            con.Obj = condata;
            EJObject AB = this.EvaluationServant.GetDetail(con);




            ViewBag.data = AB.Obj;
            ViewBag.OrgID = condata["ID"].ToString().Replace(System.Environment.NewLine, "");
            ViewBag.V = V;
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "BuildEvaluation")]
        [ExceptionHandle]
        public ActionResult GetBuildList(string json = "{}", int page = 1, int customsize = 10)
        {
            EJObject con = new EJObject();
            var condata = JObject.Parse(json);
            condata["Page"] = page;
            condata["PageSize"] = customsize;
            con.Obj = condata;
            EJObject tiq = this.EvaluationServant.GetBuild(con);
            ViewBag.Data = tiq.Obj;
            return View();
        }
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildEvaluation")]
        [ExceptionHandle]
        public ActionResult UpdateRemark(String DESCRIPTION, String ID)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);

            EJObject s = new EJObject();
            s.Obj = new JObject();
            s.Obj["ID"] = ID;
            s.Obj["S"] = DESCRIPTION;
            s.Obj["User"] = user.ID;
            EJObject EdOrd = this.EvaluationServant.UpdateRemark(s);
            return RedirectToAction("Detail", new { id = ID });
        }
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildEvaluation")]
        [ExceptionHandle]
        public ActionResult UpdateOrderT(string s)
        {
            JObject ss = JObject.Parse(s);
            //return RedirectToAction("ContentDetail", new { id = ss["Build"]["ID"] });
            return Content(ss["Build"]["ID"].ToString());
        }
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildEvaluation")]
        [ExceptionHandle]
        public ActionResult UpdateOrder(FormCollection asset3)
        {
            //JObject asset2 = asset3;
            JObject asset = new JObject();
            asset["BeginDate"] = asset3["BeginDate"].ToString();
            if (asset3["SaveMode"].ToString() == "1")
                asset["Type"] = "1";
            else
                asset["Type"] = "0";
            if (TempData["RealID"] != null)
                asset["ID"] = TempData["RealID"].ToString();
            asset["TRX_Header_ID"] = TempData["RealNo"].ToString();
            foreach (string key in asset3.Keys)
            {
                asset[key] = asset3[key].ToString();
            }

            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            asset["Created_By"] = user.ID.ToString();
            asset["Last_Updated_By"] = user.ID.ToString();

            EJObject T = new EJObject();
            T.Obj = new JObject(new JProperty("Data", asset));

            var Result = this.EvaluationServant.UpdateOrder(T);

            if (asset["Type"].ToString() == "0")
                return RedirectToAction("Edit", new { id = Result.Obj["TRX_Header_ID"] });
            else
            {
                string ID = Result.Obj["TRX_Header_ID"].ToString();
                UserInfor user2 = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
                int DocID;
                string message = "提交成功!";
                EJObject T2 = new EJObject();
                T2.Obj = new JObject();
                T2.Obj["DocCode"] = "H30B";
                if (EvaluationServant.GetDocID(T2).Obj["DocID"].ToString() != "0")
                {
                    EString edd = new EString();
                    edd.type = ID;
                    EJObject EdOrd = this.EvaluationServant.GetOrder(edd);
                    string FNo = EdOrd.Obj["Data"][0]["mh"]["TRX_Header_ID"].ToString();
                    int s = (int)EvaluationServant.GetDocID(T2).Obj["DocID"];
                    LocalApprovalTodoServant.Create(user2.ID, s, FNo, "BuildEvaluation");
                    LocalApprovalTodoServant.Start(user2.ID, FNo, ID);
                }
                else
                {
                    message = "查無此表單!!";
                }
                TempData["message"] = message;
                return RedirectToAction("Index");
            }
        }
    }
}