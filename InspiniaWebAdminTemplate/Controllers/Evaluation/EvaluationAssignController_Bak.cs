﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Evaluation
{
    public class EvaluationAssignController1 : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "EvaluationAssign")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "EvaluationAssign")]
        [ExceptionHandle]
        public ActionResult Detail()
        {
            return RedirectToAction("Detail", "EvaluationSearch");
        }

        [Login]
        [FunctionAuthorize(ID = "EvaluationAssign")]
        [ExceptionHandle]
        public ActionResult Edit()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "EvaluationAssign")]
        [ExceptionHandle]
        public ActionResult ContentDetail()
        {
            return RedirectToAction("ContentDetail", "EvaluationSearch");
        }
        
    }
}