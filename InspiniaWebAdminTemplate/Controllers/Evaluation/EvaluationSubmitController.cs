﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Evaluation
{
    public class EvaluationSubmitController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "EvaluationSubmit")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "EvaluationSubmit")]
        [ExceptionHandle]
        public ActionResult Detail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "EvaluationSubmit")]
        [ExceptionHandle]
        public ActionResult ContentDetail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "EvaluationSubmit")]
        [ExceptionHandle]
        public ActionResult Edit()
        {
            return View();
        }
    }
}