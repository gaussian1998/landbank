﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Extension;
using InspiniaWebAdminTemplate.Models.PostsManagement;
using Library.Entity.Edmx;
using Library.Servant.Enums;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.PostsManagement;
using Library.Servant.Servant.PostsManagement.Models;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Extension;
using System.Web;
using System.IO;

namespace InspiniaWebAdminTemplate.Controllers.Post
{
    [Login]
    [FunctionAuthorize(ID = "PostsManagement")]
    [ExceptionHandle]
    public class PostsManagementController : Controller
    {
        private IEnumerable<SelectListItem> _statusList // 之後會由Servant取得
        {
            get {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text="新增", Value="1" },
                    new SelectListItem { Text="簽核中", Value="2" }, // 在新增中不允許作業
                    new SelectListItem { Text="已核准", Value="3" }, // 在新增中不允許作業
                    new SelectListItem { Text="退回修改", Value="4" },
                    new SelectListItem { Text="抽回修改", Value="5" },
                    new SelectListItem { Text="作廢", Value="6" } // 在新增中不允許作業
                };
            }
        }

        private IEnumerable<KeyValuePair<string, FormType>> _documentList
        {
            get {
                return new List<KeyValuePair<string, FormType>>
                {
                    new KeyValuePair<string, FormType>(FormType.PostCreate.GetFormName(), FormType.PostCreate),
                    new KeyValuePair<string, FormType>(FormType.PostModified.GetFormName(), FormType.PostModified)
                };
            }
        }

        private IEnumerable<SelectListItem> _announceTypeList
        {
            get {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text="一般公告", Value="0" },
                    new SelectListItem { Text="最新資訊", Value="1" },
                    new SelectListItem { Text="法規新訊", Value="2" }
                };
            }
        }

        private IEnumerable<SelectListItem> _announceDepartmentList
        {
            get {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text="全部", Value="0" },
                    new SelectListItem { Text="產權管理部", Value="1" },
                    new SelectListItem { Text="各分行", Value="2" }
                };
            }
        }
        
        public ActionResult FormIndex(string flowCode)
        {
            var info = Servant.FormIndex(flowCode);

            foreach (var item in info.Items) { //todo_julius 亂寫的，到時候要從code table來
                item.Type = _announceTypeList.First(m => m.Value == item.Type).Text;
                item.AnnounceDepartment = _announceDepartmentList.First(m => m.Value == item.AnnounceDepartment).Text;
            }

            ViewBag.DocumentList = new SelectList(_documentList, "Value", "Key", info.HeaderInfo.FlowType);
            ViewBag.Branch = LocalPostsManagementServant.GetUserBranch(this.UserID());

            return View((FormIndexViewModel)info);
        }

        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();
            condition.currentUser = this.UserID();
            IndexResult result = Servant.Index((SearchModel)condition);

            ViewBag.DocumentList = new SelectList(_documentList, "Value", "Key", condition.DocumentType);
            ViewBag.StatusList = new SelectList(_statusList, "Value", "Text", condition.ApprovalStatus);


            IndexViewModel vm = new IndexViewModel
            {
                Items = result.Items.Select(m => (ItemViewModel)m).ToList(),
                TotalAmount = result.TotalAmount,
                Condition = condition
            };

            foreach (var item in vm.Items) { //todo_julius 亂寫的，到時候要從code table來
                item.Type = _announceTypeList.First(m => m.Value == item.Type).Text;
                item.AnnounceDepartment = _announceDepartmentList.First(m => m.Value == item.AnnounceDepartment).Text;
            }

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(SearchViewModel vm)
        {
            this.UpdateSearchCondition<SearchViewModel>(vm);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveForm(HeaderViewModel headerInfo)
        {
            if (headerInfo.FlowType == FormType.PostCreate || headerInfo.FlowType == FormType.PostModified) {

                headerInfo.FlowCode = headerInfo.FlowType.GetNewFormNO();
                var prefixCode = headerInfo.FlowType.GetPrefixCode();
                var docID = AS_Doc_Name_Records.First(m => m.Doc_No == prefixCode).ID;
                LocalApprovalTodoServant.Create( this.UserID(), docID, headerInfo.FlowCode, "Posts");
                headerInfo.CreateBy = this.UserID();
                Servant.SaveForm((HeaderModel)headerInfo);

                return RedirectToAction(headerInfo.FlowType == FormType.PostCreate ? "Create" : "Search", new { flowCode = headerInfo.FlowCode });
            }
            else {
                return RedirectToAction("Index");
            }            
        }

        [HttpGet]
        public ActionResult Create(string flowCode)
        {
            TempData["CanEdit"] = true;

            return RedirectToAction("Detail", new { flowCode = flowCode, postCode = "", newFlowCode = flowCode });
        }

        public ActionResult Edit(string flowCode, string postCode)
        {
            TempData["CanEdit"] = true;

            return RedirectToAction("Detail", new { flowCode = flowCode, postCode = postCode, newFlowCode = flowCode });
        }

        public ActionResult SearchEdit(string curFlowCode, string flowCode, string postCode)
        {
            TempData["CanEdit"] = true;

            return RedirectToAction("Detail", new { flowCode = flowCode, postCode = postCode, newFlowCode = curFlowCode });
        }

        public ActionResult Show(string flowCode, string postCode)
        {
            TempData["CanEdit"] = false;

            return RedirectToAction("Detail", new { flowCode = flowCode, postCode = postCode, newFlowCode = flowCode });
        }

        public ActionResult Search(string flowCode)
        {
            var vm = (FormSearchViewModel)Servant.Search(flowCode);
            vm.FlowCode = flowCode;

            foreach (var item in vm.Items) { //todo_julius 亂寫的，到時候要從code table來
                item.Type = _announceTypeList.First(m => m.Value == item.Type).Text;
                item.AnnounceDepartment = _announceDepartmentList.First(m => m.Value == item.AnnounceDepartment).Text;
            }

            ViewBag.AnnounceTypeList = new SelectList(_announceTypeList, "Value", "Text", "");

            return View(vm);
        }

        public ActionResult SavePost(PostViewModel vm, HttpPostedFileBase file)
        {
            vm.Principal = this.UserID().ToString();
            if (file != null) {
                vm.FileName = Path.GetFileName(file.FileName);
            }
            Servant.SavePost((PostInfo)vm);

            return RedirectToAction("Index", new { flowCode = vm.NewFlowCode });
        }

        public ActionResult Detail(string flowCode, string postCode, string newFlowCode)
        {
            var vm = new PostViewModel {
                FlowCode = flowCode,
                NewFlowCode = newFlowCode,
                PostCode = postCode,
                Principal = this.UserID().ToString()
            };

            var posInfo = Servant.Detail((PostInfo)vm);

            ViewBag.AnnounceTypeList = new SelectList(_announceTypeList, "Value", "Text", vm.DocumentType);
            ViewBag.AnnounceDepartmentList = new SelectList(_announceDepartmentList, "Value", "Text", vm.AnnounceDepartment);
            ViewBag.CanEdit = TempData["CanEdit"] as bool? ?? false;

            return View((PostViewModel)posInfo);
        }

        public ActionResult Submit(string flowCode)
        {
            Servant.Submit(flowCode);
            LocalApprovalTodoServant.Start( this.UserID(), flowCode);

            return RedirectToAction("Index", new { flowCode = flowCode });
        }
                
        public ActionResult Discard(string flowCode)
        {
            Servant.Discard(flowCode);

            return RedirectToAction("Index", new { flowCode = "" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string flowCode, string postCode)
        {
            Servant.Delete(postCode);

            return RedirectToAction("Index", new { flowCode = flowCode });
        }

        private IPostsManagementServant Servant = ServantAbstractFactory.PostsManagement();
    }
}