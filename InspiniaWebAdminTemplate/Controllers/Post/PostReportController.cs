﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FastReport.Web;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Extension;
using InspiniaWebAdminTemplate.Models.PostReport;
using Library.Entity.Edmx;
using Library.Servant.Servant.PostsManagement;
using Library.Servant.Servant.PostsManagement.Models;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers.Post
{
    public class PostReportController : Controller
    {
        [Login]
        // GET: PostReport
        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();

            WebReport webReport = new WebReport();
            webReport.Width = 1000;
            webReport.Bind(Server.FrxPath("PostReport.frx"), Servant.GetReportContent((ReportSearchModel)condition));
            webReport.Report.SetParameterValue("Creator", this.User.Identity.Name);
            webReport.Report.SetParameterValue("CreateDate", DateTime.Now);

            ViewBag.WebReport = webReport;
            return View(condition);
        }

        private IPostsManagementServant Servant = ServantAbstractFactory.PostsManagement();
    }
}