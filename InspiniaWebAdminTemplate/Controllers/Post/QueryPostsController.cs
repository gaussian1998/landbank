﻿using System.Collections.Generic;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.QueryPosts;
using Library.Servant.Servant.PostsManagement.Models;
using Library.Servant.Servants.AbstractFactory;
using System.Linq;
using Library.Servant.Enums;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.PostsManagement;

namespace InspiniaWebAdminTemplate.Controllers.Post
{
    [Login]
    [FunctionAuthorize(ID = "QueryPosts")]
    [ExceptionHandle]
    public class QueryPostsController : Controller
    {
        private IEnumerable<KeyValuePair<string, FormType>> _documentList
        {
            get {
                return new List<KeyValuePair<string, FormType>>
                {
                    new KeyValuePair<string, FormType>(FormType.PostCreate.GetFormName(), FormType.PostCreate),
                    new KeyValuePair<string, FormType>(FormType.PostModified.GetFormName(), FormType.PostModified)
                };
            }
        }

        private IEnumerable<SelectListItem> _statusList// 之後會由Servant取得
        {
            get {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text="新增", Value="1" },
                    new SelectListItem { Text="簽核中", Value="2" },
                    new SelectListItem { Text="已核准", Value="3" },
                    new SelectListItem { Text="退回修改", Value="4" },
                    new SelectListItem { Text="抽回修改", Value="5" },
                    new SelectListItem { Text="作廢", Value="6" }
                };
            }
        }

        private IEnumerable<SelectListItem> _announceTypeList
        {
            get {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text="一般公告", Value="0" },
                    new SelectListItem { Text="最新資訊", Value="1" },
                    new SelectListItem { Text="法規新訊", Value="2" }
                };
            }
        }

        private IEnumerable<SelectListItem> _announceDepartmentList
        {
            get {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text="全部", Value="0" },
                    new SelectListItem { Text="產權管理部", Value="1" },
                    new SelectListItem { Text="各分行", Value="2" }
                };
            }
        }

        // GET: QueryPosts
        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();
            condition.currentUser = this.UserID();
            IndexResult result = Servant.Index((SearchModel)condition);

            ViewBag.DocumentList = new SelectList(_documentList, "Value", "Key", condition.DocumentType);
            ViewBag.StatusList = new SelectList(_statusList, "Value", "Text", condition.ApprovalStatus);


            IndexViewModel vm = new IndexViewModel
            {
                Items = result.Items.Select(m => (ItemViewModel)m).ToList(),
                TotalAmount = result.TotalAmount,
                Condtiton = condition
            };

            foreach (var item in vm.Items) { //todo_julius 亂寫的，到時候要從code table來
                item.Type = _announceTypeList.First(m => m.Value == item.Type).Text;
                item.AnnounceDepartment = _announceDepartmentList.First(m => m.Value == item.AnnounceDepartment).Text;
            }

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(SearchViewModel vm)
        {
            this.UpdateSearchCondition<SearchViewModel>(vm);
            return RedirectToAction("Index");
        }

        private IPostsManagementServant Servant = ServantAbstractFactory.PostsManagement();
    }
}