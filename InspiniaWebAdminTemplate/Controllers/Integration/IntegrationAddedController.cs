﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Models.Integration;
using Library.Servant.Repository.Models;
using Library.Servant.Servant.Integration.IntegrationModels;
using Library.Servant.Servant.Integration;
using Library.Servant.Servants.AbstractFactory;
using Newtonsoft.Json;
using Library.Servant.Servant.UserInformation.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.ApprovalTodo;

namespace InspiniaWebAdminTemplate.Controllers.Integration
{
    public class IntegrationAddedController : Controller
    {
        private IIGServant IGServant = ServantAbstractFactory.Integration();
        private string Type = IntegrationHelper.GetFormCode("IntegrationAdded");
        int PageSize = 10;        

        [Login]
        [FunctionAuthorize(ID = "IntegrationAdded")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            SearchModel condition = new SearchModel()
            {
                Transaction_Type = Type,
                Page = 1,
                PageSize = PageSize
            };
            IndexModel IndexQuery = this.IGServant.Index(condition);

            IndexViewModel vm = new IndexViewModel()
            {
                condition = IntegrationHelper.ToSearchViewModel(IndexQuery.condition),
                Items = IndexQuery.Items.Select(o => IntegrationHelper.ToIntegrationHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(IntegrationHelper.GetIntegrationView("Index"), vm);
        }

        [Login]
        [FunctionAuthorize(ID = "IntegrationAdded")]
        [ExceptionHandle]
        /// <summary>
        /// 單據條件查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexQuery(SearchViewModel condition)
        {
            condition.Transaction_Type = Type;
            condition.Page = 1;
            condition.PageSize = PageSize;
            IndexModel IndexQuery = this.IGServant.Index(IntegrationHelper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => IntegrationHelper.ToIntegrationHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(IntegrationHelper.GetIntegrationView("Index"), vm);
        }

        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "IntegrationAdded")]
        [ExceptionHandle]
        /// <summary>
        /// 單據刪除
        /// </summary>
        /// <returns></returns>
        public bool DeleteIntegration(string ID)
        {
            return this.IGServant.DeleteIntegration(int.Parse(ID));
        }


        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "IntegrationAdded")]
        [ExceptionHandle]
        /// <summary>
        /// 租約刪除
        /// </summary>
        /// <returns></returns>
        public bool DeleteContract(string[] IDs)
        {
            for (int idx = 0; idx < IDs.Length; idx++)
            {
                if (!string.IsNullOrEmpty(IDs[idx].Trim()))
                {
                    this.IGServant.DeleteContract(int.Parse(IDs[idx]));
                }
            }
            return true;
        }

        /// <summary>
        /// 分頁Action
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "IntegrationSearch")]
        [ExceptionHandle]
        public ActionResult UpdatePage(int page)
        {
            SearchViewModel condition = new SearchViewModel() { Page = page, PageSize = PageSize };
            IndexModel IndexQuery = this.IGServant.Index(IntegrationHelper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => IntegrationHelper.ToIntegrationHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(IntegrationHelper.GetIntegrationView("Index"), vm);
        }

        /// <summary>
        /// 跑簽核流程
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "IntegrationAdded")]
        [ExceptionHandle]
        public ActionResult CreateFlow(HeaderViewModel header)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);            
            string message = "提交成功!";
            if (this.IGServant.GetIntegrationDetail(header.TRX_Header_ID, Type).Header == null) {                
                header.Transaction_Type = Type;
                header.Remark = (header.Remark == null) ? "" : header.Remark;
                header.Created_By = user.ID.ToString();
                header.Create_Time = DateTime.Now;
                header.Last_Updated_By = user.ID.ToString();
                header.Last_Updated_Time = DateTime.Now;
                header.OriOffice_Branch = "";
                header.DisposeReason = "";
                header.Flow_Status = "0";
                header.Last_Updated_By = user.ID.ToString();
                this.IGServant.CreateIntegration(IntegrationHelper.ToIntegrationHeaderModel(header));
            }
            this.IGServant.UpdateApprovalStatus(header.TRX_Header_ID, "1");
            LocalApprovalTodoServant.Create(user.ID, header.Transaction_Type, header.TRX_Header_ID, "IntegrationAdded");
            LocalApprovalTodoServant.Start(user.ID, header.TRX_Header_ID, header.TRX_Header_ID);
            TempData["message"] = message;
            return RedirectToAction("Edit", new { id = header.TRX_Header_ID });
        }

        /// <summary>
        /// 新增單據
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "IntegrationAdded")]
        [ExceptionHandle]
        [HttpGet]
        public ActionResult New()
        {
            ViewBag.TRXID = this.IGServant.CreateTRXID(Type);
            return View(IntegrationHelper.GetIntegrationView("IntegrationDetail"));
        }


        /// <summary>
        /// 編輯單據畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "IntegrationAdded")]
        [ExceptionHandle]
        public ActionResult Edit(string id, string signmode, bool CanEdit = true)
        {
            var Detail = this.IGServant.GetIntegrationDetail(id, Type);

            var vm = new IntegrationViewModel();
            vm.CanEdit = CanEdit;
            if (Detail == null || Detail.Header == null) {
                vm.CanEdit = CanEdit;
                vm.Header = (HeaderViewModel)TempData["IntegrationHeader"];
                if (!string.IsNullOrEmpty(id))
                {
                    ViewBag.TRXID = id;
                }
                else
                {
                    ViewBag.TRXID = vm.Header.TRX_Header_ID;
                }
            }
            else {                
                vm.Header = IntegrationHelper.ToIntegrationHeaderViewModel(Detail.Header);
                ViewBag.TRXID = id;
            }
           
            if (Detail != null && Detail.Contracts != null)
            {
                vm.Contracts = Detail.Contracts.Select(m => IntegrationHelper.ToContactsViewModel(m));
            }

            if (!string.IsNullOrEmpty(signmode))
            {
                ViewBag.SignMode = true;
            }

            string json = JsonConvert.SerializeObject(vm);

            return View(IntegrationHelper.GetIntegrationView("IntegrationDetail"), vm);
        }

        /// <summary>
        /// 處理單據頭
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "IntegrationAdded")]
        [ExceptionHandle]
        public ActionResult HandleHeader(HeaderViewModel header)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            header.Transaction_Type = Type;
            header.Remark = (header.Remark == null) ? "" : header.Remark;
            header.Created_By = user.ID.ToString();
            header.Create_Time = DateTime.Now;
            header.Last_Updated_By = user.ID.ToString();
            header.Last_Updated_Time = DateTime.Now;
            header.OriOffice_Branch = "";
            header.DisposeReason = "";
            header.Flow_Status = "0";
            header.Last_Updated_By = user.ID.ToString();
            TempData["message"] = "單據儲存成功!";
            this.IGServant.CreateIntegration(IntegrationHelper.ToIntegrationHeaderModel(header));
            return RedirectToAction("Edit", new { id = header.TRX_Header_ID});
        }

        /// <summary>
        /// 新增租約
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "IntegrationAdded")]
        [ExceptionHandle]
        public ActionResult NewContract(HeaderViewModel header)
        {
            TempData["IntegrationHeader"] = header;
            ContactModel Detail = new ContactModel();
            bool isNew = true;
            var vm = new ContactDetailViewModel()
            {
                isNew = isNew,
                canEdit = true,
                TRX_Header_ID = header.TRX_Header_ID,
                Transaction_Type = header.Transaction_Type,
                OldNumber = "",
                Contract = IntegrationHelper.ToContactsViewModel(Detail)
            };
            ViewBag.SignMode = false;
           
            string json = JsonConvert.SerializeObject(vm);
            return View(IntegrationHelper.GetIntegrationView("ContractDetail"), vm);
        }

        /// <summary>
        /// 取得租約編號
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "IntegrationAdded")]
        [ExceptionHandle]
        [HttpPost]
        public string GetLeaseNumber(string type)
        {
            return this.IGServant.CreateContractTRXID(type);
        }

        

        /// <summary>
        /// 編輯租約內容
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "IntegrationAdded")]
        [ExceptionHandle]
        [HttpPost]
        public bool editContractDetail(string parking, string land, string build, string rent, string manage,
                                        string payment, string margin, string note, string fire, string public_insurance)
        {
            ContactViewModel vm = new ContactViewModel();
            List<ParkingViewModel> parkingList = JsonConvert.DeserializeObject<List<ParkingViewModel>>(parking);
            List<LandViewModel> landList = JsonConvert.DeserializeObject<List<LandViewModel>>(land);            
            List<RentViewModel> rentList = JsonConvert.DeserializeObject<List<RentViewModel>>(rent);
            List<ManageExpenseViewModel> manageList = JsonConvert.DeserializeObject<List<ManageExpenseViewModel>>(manage);
            List<PaymentAccountViewModel> paymentList = JsonConvert.DeserializeObject<List<PaymentAccountViewModel>>(payment);
            List<MarginViewModel> marginList = JsonConvert.DeserializeObject<List<MarginViewModel>>(margin);
            List<NoteViewModel> noteList = JsonConvert.DeserializeObject<List<NoteViewModel>>(note);
            List<InsuranceViewModel> fireList = JsonConvert.DeserializeObject<List<InsuranceViewModel>>(fire);
            List<InsuranceViewModel> publicList = JsonConvert.DeserializeObject<List<InsuranceViewModel>>(public_insurance);
            List<BuildViewModel> buildList = JsonConvert.DeserializeObject<List<BuildViewModel>>(build);

            vm.ParkingList = parkingList;
            vm.LandList = landList;
            vm.BuildList = buildList;
            vm.RentList = rentList;
            vm.ManageList = manageList;
            vm.PaymentList = paymentList;
            vm.MarginList = marginList;
            vm.NoteList = noteList;
            vm.FireList = fireList;
            vm.PublicList = publicList;
            TempData["Contract"] = vm;

            return true;
        }

        /// <summary>
        /// 編輯租約
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "IntegrationAdded")]
        [ExceptionHandle]
        [HttpGet]
        public ActionResult EditContact(string id, string TRX_Header_ID, string Transaction_Type, string signmode, bool CanEdit)
        {
            TempData["IntegrationHeader"] = new HeaderViewModel() { TRX_Header_ID = TRX_Header_ID, Transaction_Type = Transaction_Type};
            ContactModel Detail = this.IGServant.GetContractDetail(int.Parse(id));
            var vm = new ContactDetailViewModel()
            {
                canEdit = CanEdit,
                TRX_Header_ID = TRX_Header_ID,
                Transaction_Type = Transaction_Type,
                Contract = IntegrationHelper.ToContactsViewModel(Detail)
            }; 

            if (!string.IsNullOrEmpty(signmode))
            {
                ViewBag.SignMode = true;
            }

            string json = JsonConvert.SerializeObject(vm);           
            return View(IntegrationHelper.GetIntegrationView("ContractDetail"), vm);
        }

        /// <summary>
        /// 處理租約Header
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "IntegrationAdded")]
        [ExceptionHandle]
        public ActionResult HandleContractHeader(ContactViewModel contract)
        {
            string TRX_Header_ID = "";
            if (TempData["IntegrationHeader"] != null) {
                HeaderViewModel header = (HeaderViewModel) TempData["IntegrationHeader"];
                TRX_Header_ID = header.TRX_Header_ID;
                contract.TRANSACTION_TYPE = Type;                
                contract.TRX_Header_ID = header.TRX_Header_ID;
            }            
            if (TempData["Contract"] != null)
            {
                ContactViewModel temp = (ContactViewModel)TempData["Contract"];
                contract.ParkingList = temp.ParkingList;
                contract.LandList = temp.LandList;
                contract.BuildList = temp.BuildList;
                contract.RentList = temp.RentList;
                contract.ManageList = temp.ManageList;
                contract.PaymentList = temp.PaymentList;
                contract.MarginList = temp.MarginList;
                contract.NoteList = temp.NoteList;
                contract.FireList = temp.FireList;
                contract.PublicList = temp.PublicList;
            }
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);            
            contract.CREATED_BY = user.ID.ToString();
            contract.CREATED_TIME = DateTime.Now;
            contract.Last_Updated_By = user.ID.ToString();
            contract.Last_Updated_Time = DateTime.Now;
            TempData["message"] = "租約儲存成功!";
            string LeaseNo = contract.LEASE_NUMBER;
            this.IGServant.CreateContract(IntegrationHelper.ToContactModel(contract));
            return RedirectToAction("Edit", new { id = TRX_Header_ID });
        }

        #region == 抓縣市代碼 ==
        [Login]
        [FunctionAuthorize(ID = "IntegrationAdded")]
        [ExceptionHandle]
        public JsonResult GetCityList()
        {
            var vm = this.IGServant.GetCityList();
            return Json(vm);
        }

        [Login]
        [FunctionAuthorize(ID = "IntegrationAdded")]
        [ExceptionHandle]
        public JsonResult GetDistrict(string CITY_NAME)
        {
            var vm = this.IGServant.GetDistrict(CITY_NAME);
            return Json(vm);
        }        
        #endregion
    }
}