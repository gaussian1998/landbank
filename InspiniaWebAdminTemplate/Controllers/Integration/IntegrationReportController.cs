﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Models.Integration;
using Library.Servant.Servant.Integration.IntegrationModels;
using Library.Servant.Servant.Integration;
using Library.Servant.Servants.AbstractFactory;


namespace InspiniaWebAdminTemplate.Controllers.Integration
{
    public class IntegrationReportController : Controller
    {
        private IIGServant IGServant = ServantAbstractFactory.Integration();
        private string Type = IntegrationHelper.GetFormCode("IntegrationReport");
        int PageSize = 10;

        [Login]
        [FunctionAuthorize(ID = "IntegrationReport")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }

    }
}