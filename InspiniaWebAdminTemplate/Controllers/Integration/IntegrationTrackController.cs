﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Models.Integration;
using Library.Servant.Servant.Integration.IntegrationModels;
using Library.Servant.Servant.Integration;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers.Integration
{
    public class IntegrationTrackController : Controller
    {
        private IIGServant IGServant = ServantAbstractFactory.Integration();
        private string Type = IntegrationHelper.GetFormCode("IntegrationTrack");
        int PageSize = 10;

        [Login]
        [FunctionAuthorize(ID = "IntegrationTrack")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            SearchModel condition = new SearchModel()
            {
                Transaction_Type = Type,
                Page = 1,
                PageSize = PageSize
            };
            IndexModel IndexQuery = this.IGServant.Index(condition);

            IndexViewModel vm = new IndexViewModel()
            {
                condition = IntegrationHelper.ToSearchViewModel(IndexQuery.condition),
                Items = IndexQuery.Items.Select(o => IntegrationHelper.ToIntegrationHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(IntegrationHelper.GetIntegrationView("Index"), vm);
        }

        /// <summary>
        /// 分頁Action
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "IntegrationSearch")]
        [ExceptionHandle]
        public ActionResult UpdatePage(int page)
        {
            SearchViewModel condition = new SearchViewModel() { Page = page, PageSize = PageSize };
            IndexModel IndexQuery = this.IGServant.Index(IntegrationHelper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => IntegrationHelper.ToIntegrationHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(IntegrationHelper.GetIntegrationView("Index"), vm);
        }


        [Login]
        [FunctionAuthorize(ID = "IntegrationTrack")]
        [ExceptionHandle]
        public ActionResult Detail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "IntegrationTrack")]
        [ExceptionHandle]
        public ActionResult Edit()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "IntegrationTrack")]
        [ExceptionHandle]
        public ActionResult ContentDetail()
        {
            return View();
        }
    }
}