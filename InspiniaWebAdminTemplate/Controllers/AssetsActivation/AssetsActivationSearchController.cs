﻿using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.Activation;
using Library.Servant.Servant.Activation.ActivationModels;
using InspiniaWebAdminTemplate.Models.Activation;
using Library.Servant.Servants.AbstractFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Servant.UserInformation.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Newtonsoft.Json.Linq;
using Library.Servant.Repository.Activation;

namespace InspiniaWebAdminTemplate.Controllers.AssetsActivation
{
    public class AssetsActivationSearchController : Controller
    {
        private IActivationServant ActivationServant = ServantAbstractFactory.Activation();
        int PageSize = 10;
        [Login]
        [FunctionAuthorize(ID = "AssetsActivationSearch")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            SearchModel condition = new SearchModel();
            EJObject tg = this.ActivationServant.GetTarget(condition);
            EInt type = new EInt();
            type.type = 2;
            EJObject so = this.ActivationServant.DocumentType(type);
            type.type = 1;
            EJObject td = this.ActivationServant.DocumentType(type);
            EString code = new EString();
            code.type = "V01";
            EJObject bc = this.ActivationServant.GetCode(code);
            code.type = "C01";
            EJObject ju1 = this.ActivationServant.GetCode(code);
            code.type = "C02";
            EJObject ju2 = this.ActivationServant.GetCode(code);
            code.type = "T02";
            EJObject ty = this.ActivationServant.GetCode(code);
            ViewBag.Target = tg.Obj;
            ViewBag.DocumentType = td.Obj;
            ViewBag.Source_Type = so.Obj;
            ViewBag.DateNow = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.MGDateNow = ActivationGeneral.CvtD2S(DateTime.Today).Replace("-", "/");
            ViewBag.bc = bc.Obj;
            ViewBag.ju1 = ju1.Obj;
            ViewBag.ju2 = ju2.Obj;
            ViewBag.ty = ty.Obj;

            //EString tts = new EString();
            //tts.type = "abc";
            //EJObject ts = this.ActivationServant.EDOCServiceTest(tts);
            //ViewBag.ts = ts;
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "AssetsActivation")]
        [ExceptionHandle]
        public ActionResult IndexList(string json = "", int page = 1)
        {
            SearchModel condition = new SearchModel()
            {
                Page = 1,
                PageSize = PageSize
            };
            EJObject con = new EJObject();
            var condata = JObject.Parse(json);
            condata["Page"] = page;
            condata["PageSize"] = PageSize;
            con.Obj = condata;
            EJObject tiq = this.ActivationServant.testIndex(con);
            if (json != "")
                ViewBag.test = condata;
            else
                ViewBag.test = "NULL";
            EInt tpb = new EInt();
            tpb.type = 1;
            EJObject PB = this.ActivationServant.GetDPM(tpb);

            for (int j = 0; j < tiq.Obj["Data"].Count(); j++)
            {
                tiq.Obj["Data"][j]["J1"] = "總行";
                for (int i = 0; i < PB.Obj["Data"].Count(); i++)
                {
                    if (tiq.Obj["Data"][j]["J1C"].ToString() == PB.Obj["Data"][i]["mh"]["Branch_Code"].ToString())
                    {
                        tiq.Obj["Data"][j]["J1"] = PB.Obj["Data"][i]["mh"]["Department_Name"].ToString();
                    }
                }
            }
            ViewBag.data = tiq.Obj;
            TempData["amount"] = tiq.Obj["amount"]; //筆數
            TempData["pagestart"] = tiq.Obj["pagestart"]; //變數+1 就是起始序號
            TempData["pagecount"] = tiq.Obj["pagecount"]; //一頁幾筆
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "AssetsActivationSearch")]
        [ExceptionHandle]
        //總行單據
        public ActionResult Detail() 
        {
            string ID = "";
            if (Url.RequestContext.RouteData.Values["id"] != null)
                ID = Url.RequestContext.RouteData.Values["id"].ToString();
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            SearchModel condition = new SearchModel()
            {
                Page = 1,
                PageSize = PageSize
            };
            EJObject tg = this.ActivationServant.GetTarget(condition);

            EInt type = new EInt();
            type.type = 2;
            EJObject td = this.ActivationServant.DocumentType(type);
            EString code = new EString();
            code.type = "V01";
            EJObject bc = this.ActivationServant.GetCode(code);
            code.type = "C01";
            EJObject ju1 = this.ActivationServant.GetCode(code);
            code.type = "C02";
            EJObject ju2 = this.ActivationServant.GetCode(code);

            ViewBag.Target = tg.Obj;
            ViewBag.DocumentType = td.Obj;
            ViewBag.DateNow = DateTime.Today.ToString("yyyy/MM/dd");
            ViewBag.MGDateNow = ActivationGeneral.CvtD2S(DateTime.Today).Replace("-", "/");
            ViewBag.bc = bc.Obj;
            ViewBag.ju1 = ju1.Obj;
            ViewBag.ju2 = ju2.Obj;
            ViewBag.ID = ID;
            if (ID == "")
            {
                TempData["RealID"] = null;
                return RedirectToAction("Index");
            }
            else
            {
                EString edd = new EString();
                edd.type = ID;
                EJObject EdOrd = this.ActivationServant.GetOrder(edd);
                for (int i = 0; i < tg.Obj["Data"].Count(); i++)
                {
                    if (EdOrd.Obj["Data"][0]["mh"]["Activation_Target"].ToString() == tg.Obj["Data"][i]["mh"]["ID"].ToString())
                    {
                        EdOrd.Obj["Data"][0]["mh"]["Activation_Target"] = tg.Obj["Data"][i]["mh"]["Name"].ToString();
                    }
                }
                for (int i = 0; i < ju1.Obj["Data"].Count(); i++)
                {
                    if (EdOrd.Obj["Data"][0]["mh"]["Jurisdiction1"].ToString() == ju1.Obj["Data"][i]["mh"]["Code_ID"].ToString())
                    {
                        EdOrd.Obj["Data"][0]["mh"]["Jurisdiction1"] = ju1.Obj["Data"][i]["mh"]["Text"].ToString();
                    }
                }
                for (int i = 0; i < ju2.Obj["Data"].Count(); i++)
                {
                    if (EdOrd.Obj["Data"][0]["mh"]["Jurisdiction2"].ToString() == ju2.Obj["Data"][i]["mh"]["Code_ID"].ToString())
                    {
                        EdOrd.Obj["Data"][0]["mh"]["Jurisdiction2"] = ju2.Obj["Data"][i]["mh"]["Text"].ToString();
                    }
                }
                for (int i = 0; i < bc.Obj["Data"].Count(); i++)
                {
                    if (EdOrd.Obj["Data"][0]["mh"]["BOOK_TYPE_CODE"].ToString() == bc.Obj["Data"][i]["mh"]["Code_ID"].ToString())
                    {
                        EdOrd.Obj["Data"][0]["mh"]["BOOK_TYPE_CODE"] = bc.Obj["Data"][i]["mh"]["Text"].ToString();
                    }
                }
                for (int i = 0; i < td.Obj["Data"].Count(); i++)
                {
                    if (EdOrd.Obj["Data"][0]["mh"]["Source_Type"].ToString() == td.Obj["Data"][i]["mh"]["ID"].ToString())
                    {
                        EdOrd.Obj["Data"][0]["mh"]["Source_Type"] = td.Obj["Data"][i]["mh"]["Name"].ToString();
                    }
                }
                ViewBag.isnew = false;
                ViewBag.EdOrd = EdOrd.Obj["Data"][0];
                ViewBag.No = EdOrd.Obj["Data"][0]["mh"]["Activation_No"];
                ViewBag.State = EdOrd.Obj["Data"][0]["TY"];
                TempData["RealID"] = ID;
                TempData["RealNo"] = EdOrd.Obj["Data"][0]["mh"]["Activation_No"];
            }
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "AssetsActivationAssign")]
        [ExceptionHandle]
        //總行明細
        public ActionResult DetailList(string json = "{}", int page = 1, int customsize = 10, string AID = "0", int V = 0)
        {
            if (json == "") json = "{}";
            EJObject con = new EJObject();
            var condata = JObject.Parse(json);


            condata["Page"] = page;
            condata["PageSize"] = customsize;

            condata["AID"] = AID;
            con.Obj = condata;


            EJObject AB = this.ActivationServant.GetAPB(con);
            JArray btmp = new JArray();

            if (condata["ID"] == null)
            {
                for (int i = 0; i < AB.Obj["Data"].Count(); i++)
                {
                    btmp.Add(AB.Obj["Data"][i]["BID"].ToString());
                }
            }
            else
            {
                var tb = (JArray)condata["ID"];
                for (int i = 0; i < tb.Count(); i++)
                {
                    btmp.Add(tb[i].ToString());
                }

            }
            if (btmp.Count() == 0)
                btmp.Add(0);
            condata["ID"] = btmp;
            EJObject tiq = this.ActivationServant.GetPB(con);
            ViewBag.data = tiq.Obj;
            ViewBag.OrgID = condata["ID"].ToString().Replace(System.Environment.NewLine, "");
            ViewBag.V = V;
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "AssetsActivationSearch")]
        [ExceptionHandle]
        //分行單據
        public ActionResult Detail_2()
        {
            string ID = "";
            if (Url.RequestContext.RouteData.Values["id"] != null)
                ID = Url.RequestContext.RouteData.Values["id"].ToString();
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            SearchModel condition = new SearchModel()
            {
                Page = 1,
                PageSize = PageSize
            };
            EJObject tg = this.ActivationServant.GetTarget(condition);

            EInt type = new EInt();
            type.type = 2;
            EJObject td = this.ActivationServant.DocumentType(type);
            EString code = new EString();
            code.type = "V01";
            EJObject bc = this.ActivationServant.GetCode(code);
            code.type = "C01";
            EJObject ju1 = this.ActivationServant.GetCode(code);
            code.type = "C02";
            EJObject ju2 = this.ActivationServant.GetCode(code);

            ViewBag.Target = tg.Obj;
            ViewBag.DocumentType = td.Obj;
            ViewBag.DateNow = DateTime.Today.ToString("yyyy/MM/dd");
            ViewBag.MGDateNow = ActivationGeneral.CvtD2S(DateTime.Today).Replace("-", "/");
            ViewBag.bc = bc.Obj;
            ViewBag.ju1 = ju1.Obj;
            ViewBag.ju2 = ju2.Obj;
            ViewBag.ID = ID;
            if (ID == "")
            {
                TempData["RealID"] = null;
                return RedirectToAction("Index");
            }
            else
            {
                EString edd = new EString();
                edd.type = ID;
                EJObject EdOrd = this.ActivationServant.GetOrder(edd);
                for (int i = 0; i < tg.Obj["Data"].Count(); i++)
                {
                    if (EdOrd.Obj["Data"][0]["mh"]["Activation_Target"].ToString() == tg.Obj["Data"][i]["mh"]["ID"].ToString())
                    {
                        EdOrd.Obj["Data"][0]["mh"]["Activation_Target"] = tg.Obj["Data"][i]["mh"]["Name"].ToString();
                    }
                }
                for (int i = 0; i < ju1.Obj["Data"].Count(); i++)
                {
                    if (EdOrd.Obj["Data"][0]["mh"]["Jurisdiction1"].ToString() == ju1.Obj["Data"][i]["mh"]["Code_ID"].ToString())
                    {
                        EdOrd.Obj["Data"][0]["mh"]["Jurisdiction1"] = ju1.Obj["Data"][i]["mh"]["Text"].ToString();
                    }
                }
                for (int i = 0; i < ju2.Obj["Data"].Count(); i++)
                {
                    if (EdOrd.Obj["Data"][0]["mh"]["Jurisdiction2"].ToString() == ju2.Obj["Data"][i]["mh"]["Code_ID"].ToString())
                    {
                        EdOrd.Obj["Data"][0]["mh"]["Jurisdiction2"] = ju2.Obj["Data"][i]["mh"]["Text"].ToString();
                    }
                }
                for (int i = 0; i < bc.Obj["Data"].Count(); i++)
                {
                    if (EdOrd.Obj["Data"][0]["mh"]["BOOK_TYPE_CODE"].ToString() == bc.Obj["Data"][i]["mh"]["Code_ID"].ToString())
                    {
                        EdOrd.Obj["Data"][0]["mh"]["BOOK_TYPE_CODE"] = bc.Obj["Data"][i]["mh"]["Text"].ToString();
                    }
                }
                for (int i = 0; i < td.Obj["Data"].Count(); i++)
                {
                    if (EdOrd.Obj["Data"][0]["mh"]["Source_Type"].ToString() == td.Obj["Data"][i]["mh"]["ID"].ToString())
                    {
                        EdOrd.Obj["Data"][0]["mh"]["Source_Type"] = td.Obj["Data"][i]["mh"]["Name"].ToString();
                    }
                }
                ViewBag.isnew = false;
                ViewBag.EdOrd = EdOrd.Obj["Data"][0];
                ViewBag.No = EdOrd.Obj["Data"][0]["mh"]["Activation_No"];
                ViewBag.State = EdOrd.Obj["Data"][0]["TY"];
                TempData["RealID"] = ID;
                TempData["RealNo"] = EdOrd.Obj["Data"][0]["mh"]["Activation_No"];
            }
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "AssetsActivation")]
        [ExceptionHandle]
        //分行建物明細
        public ActionResult Detail2H(string json = "{}", int page = 1, int customsize = 10, string AID = "0", int V = 0)
        {
            if (json == "") json = "{}";
            EJObject con = new EJObject();
            var condata = JObject.Parse(json);


            condata["Page"] = page;
            condata["PageSize"] = customsize;

            condata["AID"] = AID;
            con.Obj = condata;


            EJObject AB = this.ActivationServant.GetAB(con);
            JArray btmp = new JArray();

            if (condata["ID"] == null)
            {
                for (int i = 0; i < AB.Obj["Data"].Count(); i++)
                {
                    btmp.Add(Int32.Parse(AB.Obj["Data"][i]["BID"].ToString()));
                }
            }
            else
            {
                var tb = (JArray)condata["ID"];
                for (int i = 0; i < tb.Count(); i++)
                {
                    btmp.Add(Int32.Parse(tb[i].ToString()));
                }

            }
            if (btmp.Count() == 0)
                btmp.Add(0);
            condata["ID"] = btmp;
            EJObject tiq = this.ActivationServant.GetBuild(con);
            ViewBag.data = tiq.Obj;
            ViewBag.OrgID = condata["ID"];
            ViewBag.V = V;
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "AssetsActivationSearch")]
        [ExceptionHandle]
        public ActionResult Detail_3()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "AssetsActivationSearch")]
        [ExceptionHandle]
        public ActionResult ContentDetail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "AssetsActivationSearch")]
        [ExceptionHandle]
        public ActionResult ContentDetail_2()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "AssetsActivationSearch")]
        [ExceptionHandle]
        public ActionResult Notice()
        {
            return View();
        }
    }
}