﻿using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Repository.Activation;
using Library.Servant.Servant.Activation;
using Library.Servant.Servant.Activation.ActivationModels;
using Library.Servant.Servant.UserInformation.Models;
using Library.Servant.Servants.AbstractFactory;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.AssetsActivation
{
    public class AssetsActivationAssignController : Controller
    {
        private IActivationServant ActivationServant = ServantAbstractFactory.Activation();
        int PageSize = 10;
        [Login]
        [FunctionAuthorize(ID = "AssetsActivationAssign")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            // EJObject condition = new EJObject();
            //EJObject tg = this.ActivationServant.GetTargetE(condition);
            SearchModel condition = new SearchModel();
            EJObject tg = this.ActivationServant.GetTarget(condition);
            EInt type = new EInt();
            type.type = 2;
            EJObject td = this.ActivationServant.DocumentType(type);
            EString code = new EString();
            code.type = "V01";
            EJObject bc = this.ActivationServant.GetCode(code);
            type.type = 1;
            EJObject ju1 = this.ActivationServant.GetDPM(type);
            type.type = 2;
            EJObject ju2 = this.ActivationServant.GetDPM(type);
            code.type = "T02";
            EJObject ty = this.ActivationServant.GetCode(code);
            ViewBag.Target = tg.Obj;
            ViewBag.DocumentType = td.Obj;
            ViewBag.DateNow = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.MGDateNow = ActivationGeneral.CvtD2S(DateTime.Today).Replace("-", "/");
            ViewBag.bc = bc.Obj;
            ViewBag.ju1 = ju1.Obj;
            ViewBag.ju2 = ju2.Obj;
            ViewBag.ty = ty.Obj;
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "AssetsActivationAssign")]
        [ExceptionHandle]
        public ActionResult IndexList(string json = "", int page = 1)
        {
            SearchModel condition = new SearchModel()
            {
                Page = 1,
                PageSize = PageSize
            };
            EJObject con = new EJObject();
            var condata = JObject.Parse(json);
            condata["Page"] = page;
            condata["PageSize"] = PageSize;
            con.Obj = condata;
            EJObject tiq = this.ActivationServant.J200IndexList(con);
            if (json != "")
                ViewBag.test = condata;
            else
                ViewBag.test = "NULL";
            EInt tpb = new EInt();
            tpb.type = 1;
            EJObject PB = this.ActivationServant.GetDPM(tpb);
           
            for (int j = 0; j < tiq.Obj["Data"].Count(); j++)
            {
                tiq.Obj["Data"][j]["J1"] = "總行";
                for (int i = 0; i < PB.Obj["Data"].Count(); i++)
                {
                    if (tiq.Obj["Data"][j]["J1C"].ToString() == PB.Obj["Data"][i]["mh"]["Branch_Code"].ToString())
                    {
                        tiq.Obj["Data"][j]["J1"] = PB.Obj["Data"][i]["mh"]["Department_Name"].ToString();
                    }
                }
            }
            ViewBag.test = PB.Obj;
            ViewBag.data = tiq.Obj;
            //TempData["amount"] = tiq.Obj["amount"]; //筆數
            //TempData["pagestart"] = tiq.Obj["pagestart"]; //變數+1 就是起始序號
            //TempData["pagecount"] = tiq.Obj["pagecount"]; //一頁幾筆
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "AssetsActivationAssign")]
        [ExceptionHandle]
        public ActionResult Edit()
        {
            string ID = "";
            if (Url.RequestContext.RouteData.Values["id"] != null)
                ID = Url.RequestContext.RouteData.Values["id"].ToString();
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            SearchModel condition = new SearchModel()
            {
                Page = 1,
                PageSize = PageSize
            };
            EJObject tg = this.ActivationServant.GetTarget(condition);

            EInt type = new EInt();
            type.type = 2001;
            EJObject td = this.ActivationServant.DocumentTypeOne(type);
            EString code = new EString();
            code.type = "V01";
            EJObject bc = this.ActivationServant.GetCode(code);
            type.type = 1;
            EJObject ju1 = this.ActivationServant.GetDPM(type);
            type.type = 2;
            EJObject ju2 = this.ActivationServant.GetDPM(type);

            ViewBag.Target = tg.Obj;
            ViewBag.DocumentType = td.Obj;
            ViewBag.DateNow = DateTime.Today.ToString("yyyy/MM/dd");
            ViewBag.MGDateNow = ActivationGeneral.CvtD2S(DateTime.Today).Replace("-", "/");
            ViewBag.bc = bc.Obj;
            ViewBag.ju1 = ju1.Obj;
            ViewBag.ju2 = ju2.Obj;
            ViewBag.ID = ID;
            EJObject tmp = new EJObject();
            tmp.Obj = JObject.Parse("{}");
            EJObject MaxNo = this.ActivationServant.GetMaxNo(tmp);
            if (ID == "")
            {
                ViewBag.isnew = true;
                string tmp1 = "000001";
                if (MaxNo.Obj["Data"].Count() > 0)
                    tmp1 = MaxNo.Obj["Data"][0]["maxno"].ToString().PadLeft(6, '0');
                ViewBag.No = ActivationGeneral.CvtD2S(DateTime.Today).Replace("-", "") + "J200" + tmp1; ViewBag.State = "新增";
                TempData["RealNo"] = ActivationGeneral.CvtD2S(DateTime.Today).Replace("-", "") + "J200";
                TempData["RealID"] = null;
                ViewBag.ID = "0";
            }
            else
            {
                EString edd = new EString();
                edd.type = ID;
                EJObject EdOrd = this.ActivationServant.GetOrder(edd);
                //ViewBag.No = EdOrd.Obj["Data"][1]["mh"]["Activation_No"];
                if ((EdOrd.Obj["Data"].Count() <= 0) ||
                    ((EdOrd.Obj["Data"][0]["mh"]["Type"].ToString() != "0") &&
                    (EdOrd.Obj["Data"][0]["mh"]["Type"].ToString() != "3") &&
                    (EdOrd.Obj["Data"][0]["mh"]["Type"].ToString() != "4")))
                {
                    return RedirectToAction("Index");
                }
                ViewBag.isnew = false;
                ViewBag.EdOrd = EdOrd.Obj["Data"][0];
                ViewBag.No = EdOrd.Obj["Data"][0]["mh"]["Activation_No"];
                ViewBag.State = EdOrd.Obj["Data"][0]["TY"];
                TempData["RealID"] = ID;
                TempData["RealNo"] = EdOrd.Obj["Data"][0]["mh"]["Activation_No"];
            }
            return View();
        }
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "AssetsActivation")]
        [ExceptionHandle]
        public ActionResult UpdateOrder(FormCollection asset3)
        {
            //JObject asset2 = asset3;
            JObject asset = new JObject();
            asset["BeginDate"] = asset3["BeginDate"].ToString();
            if (asset3["SaveMode"].ToString() == "1")
                asset["Type"] = "1";
            else
                asset["Type"] = "0";
            if (TempData["RealID"] != null)
                asset["ID"] = TempData["RealID"].ToString();
            asset["Activation_No"] = TempData["RealNo"].ToString();
            foreach (string key in asset3.Keys)
            {
                asset[key] = asset3[key].ToString();
            }

            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            asset["Created_By"] = user.ID.ToString();
            asset["Last_Updated_By"] = user.ID.ToString();

            EJObject T = new EJObject();
            T.Obj = new JObject(new JProperty("Data", asset));

            var Result = this.ActivationServant.UpdateOrder(T);

            if (asset["Type"].ToString() == "0")
                return RedirectToAction("Edit", new { id = Result.Obj["TRX_Header_ID"] });
            else
                return RedirectToAction("Index");
        }
        [Login]
        [FunctionAuthorize(ID = "AssetsActivation")]
        [ExceptionHandle]
        public ActionResult EditList(string json = "{}", int page = 1, int customsize = 10, string AID = "0", int V = 0)
        {
            if (json == "") json = "{}";
            EJObject con = new EJObject();
            var condata = JObject.Parse(json);


            condata["Page"] = page;
            condata["PageSize"] = customsize;

            condata["AID"] = AID;
            con.Obj = condata;


            EJObject AB = this.ActivationServant.GetAPB(con);
            JArray btmp = new JArray();

            if (condata["ID"] == null)
            {
                for (int i = 0; i < AB.Obj["Data"].Count(); i++)
                {
                    btmp.Add(AB.Obj["Data"][i]["BID"].ToString());
                }
            }
            else
            {
                var tb = (JArray)condata["ID"];
                for (int i = 0; i < tb.Count(); i++)
                {
                    btmp.Add(tb[i].ToString());
                }

            }
            if (btmp.Count() == 0)
                btmp.Add(0);
            condata["ID"] = btmp;
            EJObject tiq = this.ActivationServant.GetPB(con);
            ViewBag.data = tiq.Obj;
            ViewBag.OrgID = condata["ID"].ToString ().Replace(System.Environment.NewLine, "");
            ViewBag.V = V;
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "AssetsActivation")]
        [ExceptionHandle]
        public ActionResult GetPBList(string json = "{}", int page = 1, int customsize = 10)
        {
            EJObject con = new EJObject();
            var condata = JObject.Parse(json);
            condata["Page"] = page;
            condata["PageSize"] = customsize;
            con.Obj = condata;
            //temp
            //EString code = new EString();
            //code.type = "C01";
            //EJObject tiq = this.ActivationServant.GetCode(code);
            //tmpend
            EJObject tiq = this.ActivationServant.GetPB(con);
            ViewBag.Data = tiq.Obj;
            return View();
        }
    }
}