﻿using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.UserInformation.Models;
using Library.Servant.Servant.Activation;
using Library.Servant.Servant.Activation.ActivationModels;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers.AssetsActivation
{
    
    public class AssetsActivationSubmitController : Controller
    {
        private IActivationServant ActivationServant = ServantAbstractFactory.Activation();
        [Login]
        [FunctionAuthorize(ID = "AssetsActivationSubmit")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "AssetsActivationSubmit")]
        [ExceptionHandle]
        public ActionResult Edit()
        {
            return View();
        }
    
        [Login]
        [FunctionAuthorize(ID = "AssetsActivationSubmit")]
        [ExceptionHandle]
        public ActionResult Edit2()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "AssetsActivationSubmit")]
        [ExceptionHandle]
        public ActionResult ContentDetail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "AssetsActivationSubmit")]
        [ExceptionHandle]
        public ActionResult ContentDetail_2()
        {
            return View();
        }


        
    }
}