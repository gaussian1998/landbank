﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.AssetsActivation
{
    public class AssetsActivationReportController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "AssetsActivationReport")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }
    }
}