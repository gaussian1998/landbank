﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Servant.Land;
using Library.Servant.Servant.Land.LandModels;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Models.Land;
using Newtonsoft.Json;
using Library.Servant.Servant.UserInformation.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.ApprovalFlow;
using Library.Servant.Servant.ApprovalTodo;
using InspiniaWebAdminTemplate.Attributes;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [FunctionAuthorize(ID = "LandCost")]
    [ExceptionHandle]
    public class LandCostController : Controller
    {
        private ILandServant LandServant = ServantAbstractFactory.Land();
        private string Type = "B030";

        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LandCost")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            SearchModel condition = new SearchModel()
            {
                Transaction_Type = Type
            };
            IndexModel IndexQuery = this.LandServant.Index(condition);

            IndexViewModel vm = new IndexViewModel()
            {
                condition = LandHelper.ToSearchViewModel(IndexQuery.condition),
                Items = IndexQuery.Items.Select(o => LandHelper.ToLandHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(LandHelper.GetLandView("Index"), vm);
        }

        /// <summary>
        /// 新增單據
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "LandCost")]
        [ExceptionHandle]
        public ActionResult New()
        {
            ViewBag.TRXID = this.LandServant.CreateTRXID(Type);
            return View(LandHelper.GetLandView("Land"));
        }

        /// <summary>
        /// 編輯單據畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "LandCost")]
        [ExceptionHandle]
        public ActionResult Edit(string id)
        {
            var Detail = this.LandServant.GetDetail(id);

            var vm = new LandViewModel()
            {
                Header = LandHelper.ToLandHeaderViewModel(Detail.Header),
                Assets = Detail.Assets.Select(m => LandHelper.ToAssetLandViewModel(m))
            };

            string json = JsonConvert.SerializeObject(vm);

            return View(LandHelper.GetLandView("Land"), vm);
        }

        /// <summary>
        /// 處理單據頭
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LandCost")]
        [ExceptionHandle]
        public ActionResult HandleHeader(LandHeaderViewModel header)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            header.Transaction_Type = Type;
            header.Description = (header.Description == null) ? "" : header.Description; header.Created_By = decimal.Parse(user.ID.ToString());
            header.Created_By_Name = user.Name;
            header.Create_Time = DateTime.Now;
            header.Last_UpDatetimed_By = decimal.Parse(user.ID.ToString());
            header.Last_UpDatetimed_Time = DateTime.Now;
            header.Last_UpDatetimed_By_Name = user.Name;
            header.Transaction_Status = "";
            TempData["message"] = "單據儲存成功!";
            return RedirectToAction("Edit", new { id = this.LandServant.HandleHeader(LandHelper.ToLandHeaderModel(header)).TRX_Header_ID });
        }

        /// <summary>
        /// 資產搜尋頁面
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "LandCost")]
        [ExceptionHandle]
        public ActionResult Asset_Search(LandHeaderViewModel header)
        {
            TempData["Header"] = header;
            header.Transaction_Type = Type;
            return View(LandHelper.GetLandView("AssetSearch"));
        }

        /// <summary>
        /// 資產搜尋
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "LandCost")]
        [ExceptionHandle]
        public ActionResult Asset_Query(AssetsSearchViewModel condition)
        {
            var data = this.LandServant.SearchAssets(LandHelper.ToAssetsSearchModel(condition));
            AssetsIndexViewModel vm = new AssetsIndexViewModel()
            {
                Items = data.Items.Select(o => LandHelper.ToAssetLandViewModel(o)),
                condition = condition,
                TotalAmount = data.TotalAmount
            };
            
            var Header = (TempData["Header"] != null) ? TempData["Header"] : null;
            TempData["Header"] = Header;
            return View(LandHelper.GetLandView("AssetSearch"), vm);
        }

        /// <summary>
        /// 將資產新增至表單
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LandCost")]
        [ExceptionHandle]
        public ActionResult AssetsToLand(string AssetsList, LandDispose LD)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            LandHeaderViewModel header = null;
            if (TempData["Header"] != null)
            {
                header = TempData["Header"] as LandHeaderViewModel;
                header.Transaction_Type = (header.Transaction_Type == null) ? Type : header.Transaction_Type;
                header.Transaction_Status = "";
                header.Description = (header.Description == null) ? "" : header.Description;

                header.Created_By = decimal.Parse(user.ID.ToString());
                header.Created_By_Name = user.Name;
                header.Create_Time = DateTime.Now;
                header.Last_UpDatetimed_By = decimal.Parse(user.ID.ToString());
                header.Last_UpDatetimed_Time = DateTime.Now;
                header.Last_UpDatetimed_By_Name = user.Name;
            }

            LandModel Land = new LandModel()
            {
                Header = LandHelper.ToLandHeaderModel(header),
                Assets = AssetsList.Split(',').Select(o => LandHelper.ToAssetLandModel(new AssetLandViewModel()
                {
                    Asset_Number = o
                })),
                LD = LD
            };

            var Result = this.LandServant.CreateLand(Land);
            TempData["message"] = "資產導入成功!";
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }


        /// <summary>
        /// 編輯資產畫面
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "LandCost")]
        [ExceptionHandle]
        public ActionResult Asset_Edit(int id)
        {
            var vm = this.LandServant.GetAssetLandDetail(id, "");
            TempData["Header"] = LandHelper.ToLandHeaderViewModel(this.LandServant.GetHeader(vm.Asset_Number, Type));
            ViewBag.Title = "編輯資產";
            ViewBag.FunctionFrom = (TempData["FunctionFrom"] != null) ? Convert.ToString(TempData["FunctionFrom"]) : "";
            return View(LandHelper.GetLandView("Asset"), LandHelper.ToAssetLandViewModel(vm));
        }

        /// <summary>
        /// 編輯資產
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LandCost")]
        [ExceptionHandle]
        public ActionResult Asset_Update(AssetLandViewModel asset)
        {
            LandHeaderViewModel header = TempData["Header"] as LandHeaderViewModel;
            if (header.Description == null)
            {
                header.Description = "";
            }
            asset.Last_Updated_By = "";
            asset.Last_Updated_Time = DateTime.Now;
            var Result = this.LandServant.Update_Asset(new LandModel()
            {
                Header = LandHelper.ToLandHeaderModel(header),
                Assets = new List<AssetLandViewModel>() { asset }.Select(o => LandHelper.ToAssetLandModel(o))
            });
            TempData["message"] = "資產編輯成功!";
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }

        /// <summary>
        /// 跑簽核流程
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LandCost")]
        [ExceptionHandle]
        public ActionResult CreateFlow(LandHeaderViewModel header)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            int DocID;
            string message = "提交成功!";
            if (LandServant.GetDocID(Type, out DocID))
            {
                LocalApprovalTodoServant.Create(user.ID, DocID, header.Form_Number, "LandCost");
                LocalApprovalTodoServant.Start(user.ID, header.Form_Number);
            }
            else
            {
                message = "查無此表單!!";
            }
            TempData["message"] = message;
            return RedirectToAction("Edit", new { id = header.Form_Number });
        }
    }
}