﻿using System.Web.Mvc;
using System;

using System.Linq;
using Library.Servant.Servant.Land;
using Library.Servant.Servant.Land.LandModels;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Models.Land;
using System.Collections.Generic;
using Newtonsoft.Json;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.UserInformation.Models;
using Library.Servant.Servant.ApprovalTodo;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Repository.Land;
namespace InspiniaWebAdminTemplate.Controllers.Land
{
    public class LandImprovedLifeChangeController : Controller
    {
        private ILandServant LandServant = ServantAbstractFactory.Land();
        private string Type = LandGeneral.GetFormCode("LandImprovedLifeChange");
        //private string Type = "B050";
        int PageSize = 10;
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LandImprovedLifeChange")]
        [ExceptionHandle]
        public ActionResult Index()
        {

            SearchModel condition = new SearchModel()
            {
                Transaction_Type = Type,
                Page = 1,
                PageSize = PageSize
            };
            IndexModel IndexQuery = this.LandServant.Index(condition);

            IndexViewModel vm = new IndexViewModel()
            {
                condition = LandHelper.ToSearchViewModel(IndexQuery.condition),
                Items = IndexQuery.Items.Select(o => LandHelper.ToLandHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(LandHelper.GetLandView("Index"), vm);
        }
        [Login]
        [FunctionAuthorize(ID = "LandImprovedLifeChange")]
        [ExceptionHandle]
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexQuery(SearchViewModel condition)
        {
            condition.Transaction_Type = Type;
            condition.Page = 1;
            condition.PageSize = PageSize;
            IndexModel IndexQuery = this.LandServant.Index(LandHelper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => LandHelper.ToLandHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(LandHelper.GetLandView("Index"), vm);
        }

        [Login]
        [FunctionAuthorize(ID = "LandImprovedLifeChange")]
        [ExceptionHandle]
        public ActionResult New()
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            var userBaseInfo = Library.Entity.Edmx.AS_Users_Records.First(u => u.ID == user.ID);
            ViewBag.TRXID = this.LandServant.CreateTRXID(Type);
            return View(LandHelper.GetLandView("LandMP"), new LandMPViewModel()
            {
                Header = new LandHeaderViewModel()
                {
                    Office_Branch = userBaseInfo.Department_Code,
                    Form_Number = this.LandServant.CreateTRXID(Type),
                    Transaction_Type = Type,
                    Transaction_Status = "0",
                    Transaction_Datetime_Entered = DateTime.Now
                },
                MPAssets = new List<AssetLandMPViewModel>()
            });
        }

        /// <summary>
        /// 編輯單據畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "LandImprovedLifeChange")]
        [ExceptionHandle]
        public ActionResult Edit(string id)
        {
            var Detail = this.LandServant.GetMPDetail(id);

            var vm = new LandMPViewModel()
            {
                Header = LandHelper.ToLandHeaderViewModel(Detail.Header),
                MPAssets = Detail.MPAssets.Select(m => LandHelper.ToAssetLandMPViewModel(m))
            };

            if (Detail.ChangeAssets != null)
            {
                vm.ChangeAssets = Detail.ChangeAssets.Select(m => LandHelper.ToAssetLandMPChangeViewModel(m));
            }

            string json = JsonConvert.SerializeObject(vm);

            return View(LandHelper.GetLandView("LandMP"), vm);
        }

        /// <summary>
        /// 處理單據頭
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LandImprovedLifeChange")]
        [ExceptionHandle]
        public ActionResult HandleHeader(LandHeaderViewModel header)
        {
            string[] BranchData = header.Office_Branch.Split('.');
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            header.Transaction_Type = Type;
            header.Form_Number = this.LandServant.CreateTRXID(Type);
            header.Office_Branch = BranchData.Length > 1 ? BranchData[1] : BranchData[0];
            header.Description = (header.Description == null) ? "" : header.Description;
            header.Amotized_Adjustment_Flag = (header.Amotized_Adjustment_Flag == null) ? "" : header.Amotized_Adjustment_Flag;
            header.Created_By = decimal.Parse(user.ID.ToString());
            header.Created_By_Name = user.Name;
            header.Create_Time = DateTime.Now;
            header.Transaction_Status = "0";
            header.Last_UpDatetimed_By = decimal.Parse(user.ID.ToString());
            header.Last_UpDatetimed_By_Name = user.Name;
            header.Last_UpDatetimed_Time = DateTime.Now;
            TempData["message"] = "單據儲存成功!";
            return RedirectToAction("Edit", new { id = this.LandServant.HandleHeader(LandHelper.ToLandHeaderModel(header)).TRX_Header_ID });
        }

        /// <summary>
        /// 處理單據頭
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LandImprovedLifeChange")]
        [ExceptionHandle]
        public ActionResult UpdateHeader(LandHeaderViewModel header)
        {
            string[] BranchData = header.Office_Branch.Split('.');
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            header.Transaction_Type = Type;
            //header.Form_Number = this.LandServant.CreateTRXID(Type);
            header.Office_Branch = BranchData.Length > 1 ? BranchData[1] : BranchData[0];
            header.Description = (header.Description == null) ? "" : header.Description;
            header.Amotized_Adjustment_Flag = (header.Amotized_Adjustment_Flag == null) ? "" : header.Amotized_Adjustment_Flag;
            //header.Created_By = decimal.Parse(user.ID.ToString());
            //header.Created_By_Name = user.Name;
            //header.Create_Time = DateTime.Now;
            //header.Transaction_Status = "0";
            header.Last_UpDatetimed_By = decimal.Parse(user.ID.ToString());
            header.Last_UpDatetimed_By_Name = user.Name;
            header.Last_UpDatetimed_Time = DateTime.Now;
            TempData["message"] = "單據儲存成功!";
            return RedirectToAction("Edit", new { id = this.LandServant.HandleHeader(LandHelper.ToLandHeaderModel(header)).TRX_Header_ID });
        }

        /// <summary>
        /// 資產搜尋頁面
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "LandImprovedLifeChange")]
        [ExceptionHandle]
        public ActionResult Asset_Search(LandHeaderViewModel header)
        {
            TempData["Header"] = header;
            header.Transaction_Type = Type;
            return View(LandHelper.GetLandView("AssetMPSearch"), new AssetsMPIndexViewModel()
            {
                condition = new AssetsMPSearchViewModel()
                {
                    Page = 1,
                    PageSize = PageSize
                },
                Items = new List<AssetLandMPViewModel>()
            });
        }

        /// <summary>
        /// 資產搜尋
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "LandImprovedLifeChange")]
        [ExceptionHandle]
        public ActionResult Asset_Query(AssetsMPSearchViewModel condition)
        {
            condition.PageSize = PageSize;
            condition.Page = 1;
            var data = this.LandServant.SearchAssetsMP(LandHelper.ToAssetsMPSearchModel(condition));
            AssetsMPIndexViewModel vm = new AssetsMPIndexViewModel()
            {
                Items = data.Items.Select(o => LandHelper.ToAssetLandMPViewModel(o)),
                condition = condition,
                TotalAmount = data.TotalAmount
            };

            var Header = (TempData["Header"] != null) ? TempData["Header"] : null;
            TempData["Header"] = Header;
            return View(LandHelper.GetLandView("AssetMPSearch"), vm);
        }

        /// <summary>
        /// 將資產新增至表單
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LandImprovedLifeChange")]
        [ExceptionHandle]
        public ActionResult AssetsToLand(string AssetsList)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            LandHeaderViewModel header = null;
            if (TempData["Header"] != null)
            {
                header = TempData["Header"] as LandHeaderViewModel;
                header.Book_Type_Code = (header.Book_Type_Code == null) ? Type : header.Book_Type_Code;
                header.Transaction_Type = (header.Transaction_Type == null) ? Type : header.Transaction_Type;
                header.Transaction_Status = "0";
                header.Description = (header.Description == null) ? "" : header.Description;
                header.Created_By = decimal.Parse(user.ID.ToString());
                header.Create_Time = DateTime.Now;
            }

            LandMPModel Land = new LandMPModel()
            {
                Header = LandHelper.ToLandHeaderModel(header),
                MPAssets = AssetsList.Split(',').Select(o => LandHelper.SearchDataToAssetLandMPModel(new AssetLandMPViewModel()
                {
                    Asset_Number = o,
                    Created_By = user.ID.ToString(),
                    Create_Time = DateTime.Now,
                    Last_Updated_By = user.ID.ToString(),
                    Last_Updated_Time = DateTime.Now
                }))
            };

            var Result = this.LandServant.Create_MPAssetLefeChange(Land);
            TempData["message"] = "資產導入成功!";
            var Detail = this.LandServant.GetMPDetail(Result.TRX_Header_ID);
            var vm = new LandMPViewModel()
            {
                Header = LandHelper.ToLandHeaderViewModel(Detail.Header),
                MPAssets = Detail.MPAssets.Select(m => LandHelper.ToAssetLandMPViewModel(m))
            };
            return RedirectToAction("Asset_Edit", new { vm.MPAssets.FirstOrDefault().ID });
        }
        /// <summary>
        /// 編輯資產畫面
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "LandImprovedLifeChange")]
        [ExceptionHandle]
        public ActionResult Asset_Edit(int id)
        {
            var vm = this.LandServant.GetAssetLandMPDetail(id, "LandImprovedLifeChange");
            TempData["Header"] = LandHelper.ToLandHeaderViewModel(this.LandServant.GetHeader(vm.TRX_Header_ID, Type));
            ViewBag.Title = "編輯資產";
            if (TempData["SignMode"] != null)
            {
                ViewBag.SignMode = true;
            }
            ViewBag.FunctionFrom = (TempData["FunctionFrom"] != null) ? Convert.ToString(TempData["FunctionFrom"]) : "";
            return View(LandHelper.GetLandView("AssetMPLifeChange"), LandHelper.ToAssetLandMPViewModel(vm));
        } /// <summary>
          /// 編輯資產
          /// </summary>
          /// <param name="vm"></param>
          /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LandImprovedLifeChange")]
        [ExceptionHandle]
        public ActionResult Asset_Update(AssetLandMPViewModel asset)
        {
            LandHeaderViewModel header = TempData["Header"] as LandHeaderViewModel;
            if (header.Description == null)
            {
                header.Description = "";
            }
            asset.Last_Updated_By = "";
            asset.Last_Updated_Time = DateTime.Now;
            var Result = this.LandServant.Update_AssetMPLifeChang(new LandMPModel()
            {
                Header = LandHelper.ToLandHeaderModel(header),
                MPAssets = new List<AssetLandMPViewModel>() { asset }.Select(o => LandHelper.ToAssetLandMPModel(o))
            });
            TempData["message"] = "資產編輯成功!";
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }
        [Login]
        [FunctionAuthorize(ID = "LandImprovedLifeChange")]
        [ExceptionHandle]
        public ActionResult Asset_Delete(int id)
        {
            var vm = this.LandServant.GetAssetLandMPDetail(id, "");
            AssetLandMPViewModel asset = LandHelper.ToAssetLandMPViewModel(vm);
            LandHeaderViewModel header = LandHelper.ToLandHeaderViewModel(this.LandServant.GetHeader(vm.TRX_Header_ID, Type));

            var Result = this.LandServant.Delete_MPAsset(new LandMPModel()
            {
                Header = LandHelper.ToLandHeaderModel(header),
                MPAssets = new List<AssetLandMPViewModel>() { asset }.Select(o => LandHelper.ToAssetLandMPModel(o))
            });
            TempData["message"] = "資產刪除成功!";
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }
        /// <summary>
        /// 分頁Action
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LandImprovedLifeChange")]
        [ExceptionHandle]
        public ActionResult UpdatePage(int page)
        {
            SearchViewModel condition = new SearchViewModel() { Transaction_Type = Type, Page = page, PageSize = PageSize };
            IndexModel IndexQuery = this.LandServant.Index(LandHelper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => LandHelper.ToLandHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(LandHelper.GetLandView("Index"), vm);
        }
        /// <summary>
        /// 跑簽核流程
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LandImprovedLifeChange")]
        [ExceptionHandle]
        public ActionResult CreateFlow(LandHeaderViewModel header)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            int DocID;
            string message = "提交成功!";
            //20171027 土地都先走"H"
            if (LandServant.GetDocID(Type + "H", out DocID))
            {
                this.LandServant.UpdateFlowStatus(header.Form_Number, "1");
                LocalApprovalTodoServant.Create(user.ID, DocID, header.Form_Number, "LandAssets");
                LocalApprovalTodoServant.Start(user.ID, header.Form_Number);
            }
            else
            {
                message = "查無此表單!!";
            }
            //this.LandServant.UpdateToLand(header.TRX_Header_ID);
            TempData["message"] = message;
            return RedirectToAction("Edit", new { id = header.Form_Number });
        }
    }
}