﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Servant.Report;
using System.Collections;
using Library.Servant.Servant.LandModels.ReportModel;
using System.Data;
using System.Reflection;
using Library.Servant.Servants.AbstractFactory;
using FastReport;
using FastReport.Web;
using FastReport.Utils;
using System.Web.UI.WebControls;
using InspiniaWebAdminTemplate.Attributes;

namespace InspiniaWebAdminTemplate.Controllers.LandReports
{
    [Login]
    [FunctionAuthorize(ID = "LandReport")]
    [ExceptionHandle]
    public class LandReportsController : Controller
    {
        private IReportServant ReportServant = ServantAbstractFactory.Report();

        [Login]
        [FunctionAuthorize(ID = "LandReport")]
        [ExceptionHandle]
        public ActionResult Reports_1()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "LandReport")]
        [ExceptionHandle]
        public ActionResult Reports_1_A()
        {
            return View();
        }
        


        [Login]
        [FunctionAuthorize(ID = "LandReport")]
        [ExceptionHandle]
        public ActionResult LandAllCityPlanPartitionList()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "LandReport")]
        [ExceptionHandle]
        public ActionResult PrintExcelLandAllCityPlanPartitionList(LandReportSearchModel condition)
        {
            IEnumerable<LandReportModel> query = this.ReportServant.GetLandAllCityPlanPartitionList(new LandReportSearchModel());
            var webReport = new WebReport();
            webReport.Report.Load(Server.MapPath("~/App_Data/Reports/GetLandAllCityPlanPartitionList.frx"));
            webReport.RegisterData(query, "GetLandAllCityPlanPartitionList");
            webReport.Width = 1024;//Unit.Percentage(100);

            ViewBag.WebReport = webReport;
            return View("~/Views/Reports/Index.cshtml");
        }

        [Login]
        [FunctionAuthorize(ID = "LandReport")]
        [ExceptionHandle]
        public ActionResult LandAssetImpairmentList()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "LandReport")]
        [ExceptionHandle]
        public ActionResult PrintExcelLandAssetImpairmentList(LandReportSearchModel condition)
        {
            string ReportType = condition.ReportType;

            FileInfo Template = GetExcelReportTemplate("LandAssetImpairmentList");
            MemoryStream output = new MemoryStream();
            IEnumerable<LandReportModel> query = this.ReportServant.GetLandAssetImpairmentList(condition);
            using (ExcelPackage ep=new ExcelPackage(Template,true))
            {
                var worksheet = ep.Workbook.Worksheets.FirstOrDefault();
                int StartRow =7;
                if(query.Count()>0)
                {

                    int row;
                    
                    for ( row=0; row < query.Count(); row++)
                    {
                        LandReportModel item = query.ToList()[row];
                        worksheet.Cells[(StartRow + row), 1].Value = item.Asset_Number;
                        worksheet.Cells[(StartRow + row), 2].Value = item.City_Name;
                        worksheet.Cells[(StartRow + row), 3].Value = item.District_Name;
                        worksheet.Cells[(StartRow + row), 4].Value = item.Section_Name;
                        worksheet.Cells[(StartRow + row), 5].Value = item.Sub_Section_Name;
                        worksheet.Cells[(StartRow + row), 6].Value = item.Parent_Land_Number+"-"+item.Filial_Land_Number;
                        worksheet.Cells[(StartRow + row), 7].Value = item.Authorized_Area;
                        worksheet.Cells[(StartRow + row), 8].Value = item.Announce_Amount;
                        worksheet.Cells[(StartRow + row), 9].Value = item.All_Announce_Amount;
                        worksheet.Cells[(StartRow + row), 10].Value = "";//BonusNumber;預設為空白
                        worksheet.Cells[(StartRow + row), 11].Value = item.Recoverable_amount;
                        worksheet.Cells[(StartRow + row), 12].Value = item.Current_Cost;
                        worksheet.Cells[(StartRow + row), 13].Value = item.Reval_Reserve;
                        worksheet.Cells[(StartRow + row), 14].Value = item.Deprn_Amount;
                        worksheet.Cells[(StartRow + row), 15].Value = item.BookValueBalanceForAssetImpairment;
                        worksheet.Cells[(StartRow + row), 16].Value = item.RiseInNumber;
                        worksheet.Cells[(StartRow + row), 17].Value = item.CountTheNumberofAccounts;//CountTheNumberofAccounts
                        worksheet.Cells[(StartRow + row), 18].Value = "";//NumberOfPreviousYears;預設為空白
                        worksheet.Cells[(StartRow + row), 19].Value = item.NumberOfRevolutions;//NumberOfRevolutions
                        worksheet.Cells[(StartRow + row), 20].Value = item.RevalAdjustmentAmountBalanceForAssetImpairment; //RevalAdjustmentAmountBalanceForAssetImpairment
                        worksheet.Cells[(StartRow + row), 21].Value = item.ReversalImpairmentLossAssets ;//ReversalImpairmentLossAssets
                        worksheet.Cells[(StartRow + row), 22].Value = item.LossOfAssets;//LossOfAssets

                    }
                   
                    
                }
                ep.SaveAs(output);
                output.Position = 0;
            }
            var fsr = new FileStreamResult(output, ExportContentType);
            fsr.FileDownloadName = "資產減損明細表.xlsx";
            return fsr;
        }

        [Login]
        [FunctionAuthorize(ID = "LandReport")]
        [ExceptionHandle]
        public ActionResult LandAssetRevaluationList()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "LandReport")]
        [ExceptionHandle]
        public ActionResult PrintExcelLandAssetRevaluationList(LandReportSearchModel condition)
        {
            string ReportType = condition.ReportType;

            FileInfo Template = GetExcelReportTemplate("LandAssetRevaluationList");
            MemoryStream output = new MemoryStream();
            IEnumerable<LandReportModel> query = this.ReportServant.GetLandAssetRevaluationList(condition);
            using (ExcelPackage ep = new ExcelPackage(Template, true))
            {
                var worksheet = ep.Workbook.Worksheets.FirstOrDefault();
                int StartRow = 7;
                if (query.Count()>0)
                {

                    int row;

                    for (row = 0; row < query.Count(); row++)
                    {
                        LandReportModel item = query.ToList()[row];
                        worksheet.Cells[(StartRow + row), 1].Value = item.Asset_Number;
                        worksheet.Cells[(StartRow + row), 2].Value = item.City_Name;
                        worksheet.Cells[(StartRow + row), 3].Value = item.District_Name;
                        worksheet.Cells[(StartRow + row), 4].Value = item.Section_Name;
                        worksheet.Cells[(StartRow + row), 5].Value = item.Sub_Section_Name;
                        worksheet.Cells[(StartRow + row), 6].Value = item.Parent_Land_Number + "-" + item.Filial_Land_Number;
                        worksheet.Cells[(StartRow + row), 7].Value = item.Authorized_Area;
                        worksheet.Cells[(StartRow + row), 8].Value = item.Announce_Amount;
                        worksheet.Cells[(StartRow + row), 9].Value = item.All_Announce_Amount;
                        worksheet.Cells[(StartRow + row), 10].Value = item.Current_Cost;
                        worksheet.Cells[(StartRow + row), 11].Value = item.Reval_Reserve;
                        worksheet.Cells[(StartRow + row), 12].Value = item.Deprn_Amount;
                        worksheet.Cells[(StartRow + row), 13].Value = item.TotalForAssetRevaluationList;
                        worksheet.Cells[(StartRow + row), 14].Value = item.RevaluationAfterBookValue;//RevaluationAfterBookValue//重估后帳面價值
                        worksheet.Cells[(StartRow + row), 15].Value = item.ValueAddedMoney;//ValueAddedMoney//增值金額=重估后帳面價值-合計
                        worksheet.Cells[(StartRow + row), 16].Value = item.Reval_Land_VATRevaluationCountTheNumberofAccounts ;//Reval_Land_VATCountTheNumberofAccounts////估計應付土地增值稅帳列數
                        worksheet.Cells[(StartRow + row), 17].Value = "";//Reval_Land_VATRevaluationCountTheNumberofAccounts//估計應付土地增值稅重估后帳列數
                        worksheet.Cells[(StartRow + row), 18].Value = "";//Reval_Land_VATNumberOfRevolutions //估計應付土地增值稅應補提（沖轉)數
                        worksheet.Cells[(StartRow + row), 19].Value = item.Reval_Adjustment_AmountNumberOfRevolutions;//Reval_Adjustment_AmountCountTheNumberofAccounts //未實現重估增值帳列數
                        worksheet.Cells[(StartRow + row), 20].Value = "";//Reval_Adjustment_AmountRevaluationCountTheNumberofAccounts //未實現重估增值重估后帳列數
                        worksheet.Cells[(StartRow + row), 21].Value = "";//Reval_Adjustment_AmountNumberOfRevolutions//未實現重估增值應補提（沖轉)數
                        worksheet.Cells[(StartRow + row), 22].Value = "";//SpecialLoss//特別損失
                    }


                }
                ep.SaveAs(output);
                output.Position = 0;
            }
            var fsr = new FileStreamResult(output, ExportContentType);
            fsr.FileDownloadName = "資產重估明細表.xlsx";
            return fsr;
        }

        [Login]
        [FunctionAuthorize(ID = "LandReport")]
        [ExceptionHandle]
        public ActionResult SearchLandList(LandReportSearchModel condition)
        {
            string viewname=condition.ReportName;
            IEnumerable<LandReportModel> query = null;
            if (viewname == "LandAllCityPlanPartitionList")
            {
                query = this.ReportServant.GetLandAllCityPlanPartitionList(condition);
            }
            else if (viewname == "LandAssetImpairmentList")
            {
                query = this.ReportServant.GetLandAssetImpairmentList(condition);
            }
            else if (viewname == "LandAssetRevaluationList")
            {
                query = this.ReportServant.GetLandAssetRevaluationList(condition);
            }
            return View(viewname,query);
        }


        #region==Excel==
        private FileInfo GetExcelReportTemplate(string source)
        {
            if (!source.EndsWith(".xlsx"))
            {
                source += ".xlsx";
            }
            FileInfo Template = new FileInfo(Server.MapPath("~/App_Data/ReportTemplates/" + source));
            return Template;
        }
        private string ExportContentType
        {
            get
            {
                return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            }
        }
        #endregion
    }
}