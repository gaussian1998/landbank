﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Overdue
{
    public class OverdueListController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "OverdueList")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }
    }
}