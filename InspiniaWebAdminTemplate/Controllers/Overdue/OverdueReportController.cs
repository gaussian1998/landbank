﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Overdue
{
    public class OverdueReportController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "OverdueReport")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }
    }
}