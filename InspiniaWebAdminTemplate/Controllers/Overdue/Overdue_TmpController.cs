﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Overdue
{
    public class Overdue_TmpController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "Overdue")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "Overdue")]
        [ExceptionHandle]
        public ActionResult Edit()
        {
            return View();
        }
    }
}