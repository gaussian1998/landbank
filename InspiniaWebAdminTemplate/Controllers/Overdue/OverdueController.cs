﻿using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.Overdue;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.Overdue.OverdueModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Overdue
{
    public class OverdueController : Controller
    {
        private IOverdueServant OverdueServant = ServantAbstractFactory.Overdue();
        private string Type = "G200";

        [Login]
        [FunctionAuthorize(ID = "Overdue")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            SearchModel condition = new SearchModel()
            {
                Transaction_Type = Type
            };
            //IndexModel IndexQuery = this.OverdueServant.Index(condition);

            //IndexViewModel vm = new IndexViewModel()
            //{
            //    condition = BuildHelper.ToSearchViewModel(IndexQuery.condition),
            //    Items = IndexQuery.Items.Select(o => BuildHelper.ToBuildHeaderViewModel(o)),
            //    TotalAmount = IndexQuery.TotalAmount
            //};
            //return View(BuildHelper.GetBuildView("Index"), vm);
            return View();
        }
    }
}