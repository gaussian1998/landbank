﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Overdue
{
    public class OverdueSearchController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "OverdueSearch")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "OverdueSearch")]
        [ExceptionHandle]
        public ActionResult Detail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "OverdueSearch")]
        [ExceptionHandle]
        public ActionResult ContentDetail()
        {
            return View();
        }
    }
}