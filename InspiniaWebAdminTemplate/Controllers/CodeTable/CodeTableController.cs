﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Extension;
using InspiniaWebAdminTemplate.Models.CodeTable;
using Library.Servant.Servant.CodeTableSetting.Models;
using Library.Servant.Servant.CodeTableSetting;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Models.Common;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Controllers.CodeTable
{
    [Login]
    [FunctionAuthorize(ID = "CodeTable")]
    [ExceptionHandle]
    public class CodeTableController : Controller
    {
        private IEnumerable<SelectListItem> _classList
        {
            get {
                return servant.GetCodeClasses().Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.Value
                });
            }
        }

        private IEnumerable<SelectListItem> _activeList
        {
            get {
                return new List<SelectListItem>
                {
                    new SelectListItem { Value="true", Text="啟用" },
                    new SelectListItem { Value="false", Text="停用" },
                };
            }
        }

        private IEnumerable<SelectListItem> _cancelList
        {
            get {
                return new List<SelectListItem>
                {
                    new SelectListItem { Value="false", Text="使用中" },
                    new SelectListItem { Value="", Text="全部" },
                    new SelectListItem { Value="true", Text="註銷" },
                };
            }
        }

        // GET: CodeTable
        public ActionResult Index()
        {
            IndexSearchViewModel condition = this.GetSearchCondition<IndexSearchViewModel>();

            var result = servant.Index((SearchModel)condition);
            ViewBag.ClassList = new SelectList(_classList, "Value", "Text", condition.CodeClass);
            ViewBag.ActiveList = new SelectList(_activeList, "Value", "Text", condition.IsActive);
            ViewBag.CancelList = new SelectList(_cancelList, "Value", "Text", condition.IsDeleted);
            return View(new IndexViewModel {
                Items = result.Items.Select(m => (IndexItemViewModel)m).ToList(),
                TotoalAmount = result.TotalAmount,
                Condition = condition
            });
        }

        public ActionResult UpdatePage(int page)
        {
            var condition = this.GetSearchCondition<IndexSearchViewModel>();
            condition.Page = page;
            this.UpdateSearchCondition<IndexSearchViewModel>(condition);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(IndexSearchViewModel vm)
        {
            this.UpdateSearchCondition<IndexSearchViewModel>(vm);
            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            ViewBag.Create = true;
            ViewBag.ClassList = new SelectList(_classList, "Value", "Text", "");
            return View("Detail", new DetailViewModel());
        }

        public ActionResult Edit(string code)
        {
            var detailInfo = (DetailViewModel)servant.Detail(code);
            ViewBag.Create = false;
            ViewBag.ClassList = new SelectList(_classList, "Value", "Text", detailInfo.CodeClass);
            return View("Detail", detailInfo);
        }

        public ActionResult SaveAll(List<IndexItemViewModel> Items)
        {
            servant.SaveSettings(new ListRemoteModel<CodeTableInfo> { Content = Items.Select(m => (CodeTableInfo)m).ToList() });
            return RedirectToAction("Index");
        }

        public ActionResult Save(DetailViewModel content)
        {
            servant.SaveSetting((CodeTableInfo)content);
            return RedirectToAction("Index");
        }
        
        private ICodeTableSetting servant = ServantAbstractFactory.CodeTableSetting();
    }
}