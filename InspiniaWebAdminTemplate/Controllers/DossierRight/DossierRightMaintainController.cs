﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Servant.DossierRight;
using Library.Servant.Servant.DossierRight.DossierRightModels;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Models.DossierRight;
using Newtonsoft.Json;
using Library.Servant.Servant.UserInformation.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.ApprovalTodo;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Repository.DossierRight;
namespace InspiniaWebAdminTemplate.Controllers.DossierRight
{
    public class DossierRightMaintainController : Controller
    {
        private IDossierRightServant DossierRightServant = ServantAbstractFactory.DossierRight();
        private string Type = DossierRightGeneral.GetFormCode("DossierRightMaintain");
        int PageSize = 10;
        #region 標頭檔
        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            SearchModel condition = new SearchModel()
            {
                Transaction_Type = Type,
                Page = 1,
                PageSize = PageSize
            };
            IndexModel IndexQuery = this.DossierRightServant.Index(condition);

            IndexViewModel vm = new IndexViewModel()
            {
                condition = Helper.ToSearchViewModel(IndexQuery.condition),
                Items = IndexQuery.Items.Select(o => Helper.ToHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(Helper.GetView("Index"), vm);
        }
        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexQuery(SearchViewModel condition)
        {
            condition.Transaction_Type = Type;
            condition.Page = 1;
            condition.PageSize = PageSize;
            IndexModel IndexQuery = this.DossierRightServant.Index(Helper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => Helper.ToHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(Helper.GetView("Index"), vm);
        }
        /// <summary>
        /// 新增單據
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        public ActionResult New()
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            var userBaseInfo = Library.Entity.Edmx.AS_Users_Records.First(u => u.ID == user.ID);
            ViewBag.TRXID = this.DossierRightServant.CreateTRXID(Type);
            return View(Helper.GetView("DossierRightHeader"), new DossierRightViewModel()
            {
                Header = new HeaderViewModel()
                {
                    Office_Branch_Code = userBaseInfo.Department_Code,
                    Form_Number = this.DossierRightServant.CreateTRXID(Type),
                    Transaction_Type = Type,
                    Transaction_Status = "0",
                    Transaction_Datetime_Entered = DateTime.Now
                },
                Records = new List<DossierRightRecordViewModel>()
            });
        }

        /// <summary>
        /// 編輯單據畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        public ActionResult Edit(string id, string signmode)
        {
            var Detail = this.DossierRightServant.GetDetail(id);

            var vm = new DossierRightViewModel()
            {
                Header = Helper.ToHeaderViewModel(Detail.Header),
                Records = (Detail.Records == null) ? null : Detail.Records.Select(m => Helper.ToRecordViewModel(m))
            };

            if (!string.IsNullOrEmpty(signmode))
            {
                ViewBag.SignMode = true;
            }

            string json = JsonConvert.SerializeObject(vm);

            return View(Helper.GetView("DossierRightHeader"), vm);
        }
        /// <summary>
        /// 處理單據頭
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        public ActionResult CreateHeader(HeaderViewModel header)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            header.Transaction_Type = Type;
            header.Description = (header.Description == null) ? "" : header.Description;
            header.Transaction_Status = "0";
            header.Created_By = decimal.Parse(user.ID.ToString());
            header.Created_By_Name = user.Name;
            header.Create_Time = DateTime.Now;
            header.Last_Updated_By = decimal.Parse(user.ID.ToString());
            header.Last_Updated_By_Name = user.Name;
            header.Last_Updated_Time = DateTime.Now;
            TempData["message"] = "單據儲存成功!";
            return RedirectToAction("Edit", new { id = this.DossierRightServant.CreateHeader(Helper.ToHeaderModel(header)).TRX_Header_ID });
        }
        /// <summary>
        /// 處理單據頭
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        public ActionResult UpdateHeader(HeaderViewModel header)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            header.Transaction_Type = Type;
            header.Description = (header.Description == null) ? "" : header.Description;
            header.Last_Updated_By = decimal.Parse(user.ID.ToString());
            header.Last_Updated_By_Name = user.Name;
            header.Last_Updated_Time = DateTime.Now;
            TempData["message"] = "單據儲存成功!";
            return RedirectToAction("Edit", new { id = this.DossierRightServant.UpdateHeader(Helper.ToHeaderModel(header)).TRX_Header_ID });
        }
        #endregion
        #region 明細檔

        /// <summary>
        /// 權狀搜尋頁面(預設頁)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        public ActionResult Record_Search(HeaderViewModel header)
        {
            TempData["Header"] = header;
            header.Transaction_Type = Type;
            return View(Helper.GetView("RecordSearch"), new RecordIndexViewModel()
            {
                condition = new RecordSearchViewModel()
                {
                    Page = 1,
                    PageSize = PageSize
                },
                Items = new List<DossierRightMainViewModel>()
            });
        }
        /// <summary>
        /// 權狀搜尋搜尋
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        public ActionResult Record_Query(RecordSearchViewModel condition)
        {
            condition.PageSize = PageSize;
            condition.Page = 1;
            var data = this.DossierRightServant.SearchMainRecords(Helper.ToSearchRecordModel(condition));
            RecordIndexViewModel vm = new RecordIndexViewModel()
            {
                Items = data.Items.Select(o => Helper.ToRecordMainViewModel(o)),
                condition = condition,
                TotalAmount = data.TotalAmount
            };

            var Header = (TempData["Header"] != null) ? TempData["Header"] : null;
            TempData["Header"] = Header;
            return View(Helper.GetView("RecordSearch"), vm);
        }
        /// <summary>
        /// 分頁Action
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        public ActionResult RecordUpdatePage(int page)
        {
            RecordSearchViewModel condition = new RecordSearchViewModel()
            {
                Page = page,
                PageSize = PageSize
            };

            var data = this.DossierRightServant.SearchMainRecords(Helper.ToSearchRecordModel(condition));
            RecordIndexViewModel vm = new RecordIndexViewModel()
            {
                Items = data.Items.Select(o => Helper.ToRecordMainViewModel(o)),
                condition = condition,
                TotalAmount = data.TotalAmount
            };

            var Header = (TempData["Header"] != null) ? TempData["Header"] : null;
            TempData["Header"] = Header;
            return View(Helper.GetView("RecordSearch"), vm);
        }
        /// <summary>
        /// 將權狀新增至表單
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        public ActionResult MainToRecord(string AssetsList)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            HeaderViewModel header = null;
            if (TempData["Header"] != null)
            {
                header = TempData["Header"] as HeaderViewModel;
                header.Transaction_Type = (header.Transaction_Type == null) ? Type : header.Transaction_Type;
                header.Transaction_Status = "0";
                header.Description = (header.Description == null) ? "" : header.Description;
                header.Created_By = decimal.Parse(user.ID.ToString());
                header.Create_Time = DateTime.Now;
            }

            DossierRightModel dossierRightModel = new DossierRightModel()
            {
                Header = Helper.ToHeaderModel(header),
                 Records = AssetsList.Split(',').Select(o => Helper.ToRecordModel(new DossierRightRecordViewModel()
                {
                    Asset_Number = o
                }))
            };

            var Result = this.DossierRightServant.ImportFromRecord(dossierRightModel);
            TempData["message"] = "權狀導入成功!";
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }
        /// <summary>
         /// 編輯權狀明細畫面
         /// </summary>
         /// <param name="id"></param>
         /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        public ActionResult Record_Edit(int id)
        {
            var vm = this.DossierRightServant.GetRecordDetail(id);
            TempData["Header"] = Helper.ToHeaderViewModel(this.DossierRightServant.GetHeaderData(vm.AS_Assets_Right_Header_ID));
            ViewBag.Title = "編輯權狀";
            if (TempData["SignMode"] != null)
            {
                ViewBag.SignMode = true;
            }
            ViewBag.FunctionFrom = (TempData["FunctionFrom"] != null) ? Convert.ToString(TempData["FunctionFrom"]) : "";
            return View(Helper.GetView("DossierRightRecord"), Helper.ToRecordViewModel(vm));
        }
        /// <summary>
        /// 編輯權狀
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        public ActionResult Record_Update(DossierRightRecordViewModel record)
        {
            HeaderViewModel header = TempData["Header"] as HeaderViewModel;
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            if (header.Description == null)
            {
                header.Description = "";
            }
            record.Last_Updated_By = user.ID;
            record.Last_Updated_Time = DateTime.Now;
            var Result = this.DossierRightServant.Update_Record(new DossierRightModel()
            {
                Header = Helper.ToHeaderModel(header),
                Records = new List<DossierRightRecordViewModel>() { record }.Select(o => Helper.ToRecordModel(o))
            });
            TempData["message"] = "權狀編輯成功!";
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }
        /// <summary>
        /// 刪除權狀
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "DossierRightMaintain")]
        [ExceptionHandle]
        public ActionResult Record_Delete(int id)
        {
            var vm = this.DossierRightServant.GetRecordDetail(id);
            DossierRightRecordViewModel record = Helper.ToRecordViewModel(vm);
            HeaderViewModel header = Helper.ToHeaderViewModel(this.DossierRightServant.GetHeaderData(vm.AS_Assets_Right_Header_ID));

            var Result = this.DossierRightServant.Delete_Record(new DossierRightModel()
            {
                Header = Helper.ToHeaderModel(header),
                Records = new List<DossierRightRecordViewModel>() { record }.Select(o => Helper.ToRecordModel(o))
            });
            TempData["message"] = "權狀刪除成功!";
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }
        #endregion
    }
}