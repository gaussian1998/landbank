﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Servant.DossierRight;
using Library.Servant.Servant.DossierRight.DossierRightModels;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Models.DossierRight;
using Newtonsoft.Json;
using Library.Servant.Servant.UserInformation.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.ApprovalTodo;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Repository.DossierRight;

namespace InspiniaWebAdminTemplate.Controllers.DossierRight
{
    public class DossierRightSearchController : Controller
    {
        private IDossierRightServant DossierRightServant = ServantAbstractFactory.DossierRight();
        private string Type = DossierRightGeneral.GetFormCode("DossierRightSearch");
        int PageSize = 10;
        [Login]
        [FunctionAuthorize(ID = "DossierRightSearch")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            SearchModel condition = new SearchModel()
            {
                Transaction_Type = Type,
                Page = 1,
                PageSize = PageSize
            };
            IndexModel IndexQuery = this.DossierRightServant.Index(condition);

            IndexViewModel vm = new IndexViewModel()
            {
                condition = Helper.ToSearchViewModel(IndexQuery.condition),
                Items = IndexQuery.Items.Select(o => Helper.ToHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(Helper.GetView("Index"), vm);
        }
        [Login]
        [FunctionAuthorize(ID = "DossierRightSearch")]
        [ExceptionHandle]
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexQuery(SearchViewModel condition)
        {
            condition.Transaction_Type = Type;
            condition.Page = 1;
            condition.PageSize = PageSize;
            IndexModel IndexQuery = this.DossierRightServant.Index(Helper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => Helper.ToHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(Helper.GetView("Index"), vm);
        }

        [Login]
        [FunctionAuthorize(ID = "DossierRightSearch")]
        [ExceptionHandle]
        public ActionResult Detail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "DossierRightSearch")]
        [ExceptionHandle]
        public ActionResult ContentDetail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "DossierRightSearch")]
        [ExceptionHandle]
        public ActionResult Edit()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "DossierRightSearch")]
        [ExceptionHandle]
        public ActionResult ContentEdit()
        {
            return View();
        }
    }
}