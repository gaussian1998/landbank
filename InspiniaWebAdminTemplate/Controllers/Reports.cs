﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FastReport.Web;

namespace InspiniaWebAdminTemplate.Controllers
{
    internal class Reports : Controller
    {
        private readonly string _rptName;
        private readonly string _dataSourceName;
        private readonly string _browserTitle;
        internal Reports(string browserTitle, string rptName, string dataSourceName)
        {
            _rptName = rptName;
            _dataSourceName = dataSourceName;
            _browserTitle = browserTitle;
        }
        // GET: Reports
        internal ActionResult FromDataSet(Func<DataSet> func)
        {
            var webReport = new WebReport();
            webReport.Report.Load(System.IO.Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Reports"), _rptName));
            webReport.RegisterData(func.Invoke());
            webReport.CssClass = "col-sm-12";
            webReport.Width = System.Web.UI.WebControls.Unit.Percentage(100);
            webReport.Height = System.Web.UI.WebControls.Unit.Percentage(100);
            ViewBag.WebReport = webReport;
            ViewBag.Title = _browserTitle;
            return View("~/Views/Reports/Index.cshtml");
        }

  
        internal ActionResult FromIEnumerable<T>(Func<T> func) where T : IEnumerable
        {
            var webReport = new WebReport();
            webReport.Report.Load(System.IO.Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Reports"), _rptName));
            webReport.RegisterData(func.Invoke(), _dataSourceName);
            webReport.CssClass = "col-sm-12";
            webReport.Width = System.Web.UI.WebControls.Unit.Percentage(100);
            webReport.Height = System.Web.UI.WebControls.Unit.Percentage(100);
            ViewBag.WebReport = webReport;
            ViewBag.Title = _browserTitle;
            return View("~/Views/Reports/Index.cshtml");
        }
    }
}