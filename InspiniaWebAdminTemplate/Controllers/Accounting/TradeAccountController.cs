﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.AccountingTradeAccount;
using Library.Servant.Servant.AccountingTradeAccount.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.Common;
using InspiniaWebAdminTemplate.Models.Accounting;
using Library.Servant.Servant.AccountingTradeDefine;
using Library.Servant.Servant.AccountingSubject;
using Newtonsoft.Json;
using Library.Servant.Servant.Accounting;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [FunctionAuthorize(ID = "TradeAccountSubject")]
    [ExceptionHandle]
    public class TradeAccountController : Controller
    {
        IAccountingTradeAccountServant _servant = ServantAbstractFactory.AccountingTradeAccount();
        IAccountingTradeAccountSubServant _servantSub = ServantAbstractFactory.AccountingTradeAccountSub();
        IAccountingTradeDefineServant _servantTrade = ServantAbstractFactory.AccountingTradeDefine();
        Library.Servant.Servant.AccountingSubject.IAccountingSubjectServant _servantAccSubject = ServantAbstractFactory.AccountingSubject();// new LocalAccountingSubjectServant();

        string AccountSubjectNotFund = "無此會計科目";
        string RelationNoExisted = "關連號碼重覆";
        string AccountSujectExisted = "此會計科目己被設定";

        // GET: TradeAccount
        public ActionResult Index()
        {
            AccountingTradeAccountIndexModel result = new AccountingTradeAccountIndexModel
            {
                TransTypes = StaticDataServant.GetTransTypes(),
                TradeTypes = StaticDataServant.GetTradeTypes(),
                Results = _servant.Index(new SearchModel {
                    CurrentPage = 1,
                    PageSize = 10,
                    TransType = "",
                    TradeType = "",
                    TradeNo = 0
                })
            };
            return View(result);
        }

        public ActionResult Query(SearchModel Search)
        {
            Search.TradeName = _servantTrade.Detail(Convert.ToDecimal(Search.TradeNo)).MasterText;
            AccountingTradeAccountIndexModel result = new AccountingTradeAccountIndexModel {
                TransTypes = StaticDataServant.GetTransTypes(),
                TradeTypes = StaticDataServant.GetTradeTypes(),
                Results = _servant.Index(Search)
            };
            return View("Index", result);
        }

        public ActionResult Detail(int ID)
        {
            AccountingTradeAccountDetailModel result = new AccountingTradeAccountDetailModel {
                TransTypes = StaticDataServant.GetTransTypes(),
                TradeTypes = StaticDataServant.GetTradeTypes(),
                Result = _servant.Detail(ID)
            };
            return View(result);
        }
        
        #region Sub Actions
        public ActionResult SubIndex(SubQueryModel SubQuery)
        {
            SubQuery.MasterName = _servantTrade.Detail(Convert.ToDecimal(SubQuery.MasterNo)).MasterText;
            AccountingTradeAccountSubIndexModel result = new AccountingTradeAccountSubIndexModel {
                Conditions = SubQuery,
                Results = _servantSub.Index(SubQuery)
            };
            return View("SubIndex", result);
        }

        public ActionResult SubQuery(SubQueryModel SubQuery)
        {
            AccountingTradeAccountSubIndexModel result = new AccountingTradeAccountSubIndexModel
            {
                Conditions = SubQuery,
                Results = _servantSub.Index(SubQuery)
            };
            return View("SubIndex", result);
        }

        public ActionResult SubDetail(int ID)
        {
            SubDetailModel result = _servantSub.Detail(ID);
            return View(result);
        }

        public string BatchDelete(BatchDeleteModel Batch)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            if (Batch.IDList.Count > 0)
            {
                message = _servant.BatchDelete(Batch);
            }
            return JsonConvert.SerializeObject(messageHandler(message));
        }

        public string BatchDeleteSub(BatchDeleteModel Batch)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            if (Batch.IDList.Count > 0)
            {
                message = _servantSub.BatchDelete(Batch);
            }
            return JsonConvert.SerializeObject(messageHandler(message));
        }
        public string Save(DetailModel Save)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            if (Save.ID > 0)
            {
                message = _servant.Update(Save);
            }
            else
            {
                message = _servant.Create(Save);
            }
            return JsonConvert.SerializeObject(messageHandler(message));
        }

        public string SaveSub(SubDetailModel Save)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            if (Save.ID > 0)
            {
                message = _servantSub.Update(Save);
            }
            else
            {
                message = _servantSub.Create(Save);
            }
            return JsonConvert.SerializeObject(messageHandler(message));
        }

        private MessageModel messageHandler(MessageModel Message)
        {
            if (!Message.IsSuccess)
            {
                MessageCode _code = (MessageCode)Enum.Parse(typeof(MessageCode), Message.MessagCode);
                switch (_code)
                {
                    case MessageCode.AccountSubjectNotFund:
                        Message.Message = AccountSubjectNotFund;
                        break;

                    case MessageCode.RelationNoExisted:
                        Message.Message = RelationNoExisted;
                        break;

                    case MessageCode.AccountSujectExisted:
                        Message.Message = AccountSujectExisted;
                        break;

                    default:
                        break;
                }
            }
            return Message;
        }
        #endregion

        #region Ajax Data Service
        public string GetTradeData(string TransType, string TradeType)
        {
            return JsonConvert.SerializeObject(_servantTrade.QueryTradeDefine(TransType, TradeType));
        }

        public string GetAccountSubject(string Account)
        {
            return JsonConvert.SerializeObject(_servantAccSubject.GetAccountInfo(Account));
        }
        #endregion

    }
}