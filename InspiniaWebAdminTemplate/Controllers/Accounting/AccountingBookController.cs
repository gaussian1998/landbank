﻿using InspiniaWebAdminTemplate.Attributes;
using System.Web.Mvc;
using Library.Servant.Servant.AccountingBook;
using Library.Servant.Servant.AccountingBook.Models;
using InspiniaWebAdminTemplate.Models.Accounting;
using Library.Servant.Servant.Common.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [FunctionAuthorize(ID = "AccountBook")]
    [ExceptionHandle]
    public class AccountingBookController : Controller
    {
        private string codeIsExist = "此帳本編碼已存在，請修正編碼";
        private string bookIsExist = "此帳本已存在，無法儲存";
        private string bookIsNotExits = "此帳本不存在，無法儲存";

        private IAccountingBookServant _servant = ServantAbstractFactory.AccountingBook();

        // GET: AccountingBook
        public ActionResult Index()
        {
            AccountingBookList list = new AccountingBookList
            {
                CurrentPage = 1, //default value
                PageSize = 10,    //default value
                AccountBookCode = string.Empty,
                AccountBookType = "V01",
                IsActive = 2,
                IsCancel = 2
            };
            IndexResult _result = _servant.Index(new SearchModel { PageNumber = 1, PageSize = 10, AccountBookCode = string.Empty, AccountBookType = "V01", IsActive = 2, IsCancel = 2 });
            list.Items = _result.Page;

           return View(list);
        }

        public ActionResult Query(AccountingBookQueryModel Query)
        {
            if (String.IsNullOrEmpty(Query.AccountBookCode))
            {
                Query.AccountBookCode = String.Empty;
            }

            if (String.IsNullOrEmpty(Query.AccountBookType))
            {
                Query.AccountBookType = String.Empty;
            }

            AccountingBookList list = new AccountingBookList
            {
                CurrentPage = Query.CurrentPage, //default value
                PageSize = Query.PageSize,    //default value
                AccountBookCode = Query.AccountBookCode,
                AccountBookType = Query.AccountBookType,
                IsActive = Query.IsActive,
                IsCancel = Query.IsCancel
            };
            IndexResult _result = _servant.Index(
                new SearchModel {
                    PageNumber = Query.CurrentPage,
                    PageSize = Query.PageSize,
                    AccountBookCode = Query.AccountBookCode,
                    AccountBookType = Query.AccountBookType,
                    IsActive = Query.IsActive,
                    IsCancel = Query.IsCancel });

            list.Items = _result.Page;

            return View("Index", list);

        }
        public ActionResult Create()
        {
            return View();
        }
        public string CreateSave(AccountingBookCreateModel create)
        {
            var _userInfo = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            CreateModel _create = new CreateModel
            {
                ClassType = create.ClassType,
                CodeID = create.CodeID,
                Text = create.Text,
                IsActive = create.IsActive,
                CancelCode = create.CancelCode,
                Remark = create.Remark,
                LastUpdatedBy = _userInfo.ID
            };

            MessageModel _message = _servant.Create(_create);
            if (!_message.IsSuccess)
            {
                MessageCode _code = (MessageCode)Enum.Parse(typeof(MessageCode), _message.MessagCode);
                _message.Message = messageHandler(_code);
            }
            return JsonConvert.SerializeObject(_message);

        }
        public ActionResult Detail(int id)
        {
            DetailResult _result = _servant.Detail(id);
            return View(_result);
        }

        [HttpPost]
        public string Update(AccountingBookUpdateModel update)
        {
            var _userInfo = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            UpdateModel _update = new UpdateModel {
                ID = update.ID,
                CodeID = update.CodeID,
                Text = update.Text,
                IsActive = update.IsActive,
                CancelCode = update.CancelCode,
                Remark = update.Remark,
                LastUpdatedBy = _userInfo.ID
            };
            MessageModel _message = _servant.Update(_update);
            if (!_message.IsSuccess)
            {
                MessageCode _code = (MessageCode)Enum.Parse(typeof(MessageCode), _message.MessagCode);
                _message.Message = messageHandler(_code);
            }
            return JsonConvert.SerializeObject(_message);
        }

        public string BatchUpdate(List<AccountingBookBatchUpdateModel> BatchUpdate)
        {
            var _userInfo = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            List<BatchUpdateModel> _list = new List<BatchUpdateModel>();
            foreach (AccountingBookBatchUpdateModel item in BatchUpdate)
            {
                BatchUpdateModel _item = new BatchUpdateModel
                {
                    ID = item.ID,
                    IsActive = item.IsActive,
                    IsCancel = item.IsCancel,
                    LastUpdateBy = _userInfo.ID
                };

                _list.Add(_item);
            }
            MessageModel _message = _servant.BatchUpdate(_list);
            return JsonConvert.SerializeObject(_message);
        }

        private string messageHandler(MessageCode code)
        {
            string _message = "";
            switch (code)
            {
                case MessageCode.AccountBookIsExisted:
                    _message = bookIsExist;
                    break;

                case MessageCode.AccountBookNotExisted:
                    _message = bookIsNotExits;
                    break;

                case MessageCode.CodeIdIsExisted:
                    _message = codeIsExist;
                    break;
                default:
                    break;
            }
            return _message;
        }
        
    }
}