﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;

using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Models.Accounting;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Newtonsoft.Json;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.AssetLife;
using Library.Servant.Servant.AssetLife.Models;
using InspiniaWebAdminTemplate.Extension;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [FunctionAuthorize(ID = "DeprnSetting")]
    [ExceptionHandle]
    public class DepreciationSettingController : Controller
    {

        IAssetLifeServant _servant = ServantAbstractFactory.AssetLifeServant();
        //IAssetLifeServant _servant = new LocalAssetLifeServant();
        // GET: DepreciationSetting
        public ActionResult Index()
        {
            IndexResult result = _servant.Index(new SearchModel {
                CurrentPage = 1,
                PageSize = 10,
                BeginCode = "",
                EndCode = "",
                AssetName = ""
            });

            return View(result);
        }

        public ActionResult Query(SearchModel Search)
        {
            IndexResult result = _servant.Index(Search);

            return View("Index", result);
        }

        public string BatchUpdate(BatchUpdateModel Batch)
        {
            MessageModel message = new MessageModel { IsSuccess = true, Message = "" };
            var _userInfo = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            Batch.LastUpdateBy = _userInfo.ID;

            if (Batch.List.Count > 0)
            {
                message = _servant.BatchUpdate(Batch);
            }
            return JsonConvert.SerializeObject(messageHandler(message));
        }

        public ActionResult Export(AccountingDepreciationExportQueryModel Search)
        {
            SearchModel query = new SearchModel
            {
                AssetName = Search.ExportAssetName,
                BeginCode = Search.ExportBeginCode,
                EndCode = Search.ExportEndCode
            };

            return File(_servant.ExportData(query),
            "application/csv",
            string.Format("V400 折舊明細_{0}.csv", DateTime.Now.ToString("yyyyMMddhhmmssms")));
        }
        public string Upload(HttpPostedFileBase file)
        {
            MessageModel message = new MessageModel { IsSuccess = true };
            List<DetailModel> list = new List<DetailModel>();

            if (file != null && file.ContentLength > 0)
            {

                int i = 0;
                using (System.IO.StreamReader reader = new System.IO.StreamReader(file.InputStream))
                {
                    try
                    {
                        while (!reader.EndOfStream)
                        {
                            string line = reader.ReadLine();
                            if (i > 0)
                            {
                                //skip first line
                                string[] values = line.Split(',');
                                list.Add(new DetailModel
                                {
                                    LifeCodeNo = values[1].Trim(),
                                    LifeCode1 = values[2].Trim(),
                                    LifeCode2 = values[3].Trim(),
                                    LifeCode3 = values[4].Trim(),
                                    LifeCode4 = values[5].Trim(),
                                    LifeCode5 = values[6].Trim(),
                                    AssetText = values[7].Trim(),
                                    LifeYears = Convert.ToInt32(values[8]),
                                    NotDeprnFlag = values[9] == "是" ? true : false
                                });
                            }
                            i++;
                        }
                    }
                    catch (System.Exception ex)
                    {
                        message.IsSuccess = false;
                        message.MessagCode = MessageCode.FormatError.ToString();
                    }

                    message = _servant.ImportData(list, this.UserID());

                }
            }
            else
            {
                message.IsSuccess = false;
                message.Message = "上傳失敗";

            }
            return JsonConvert.SerializeObject(messageHandler(message));
        }
        public ActionResult Create()
        {
            DetailModel result = new DetailModel
            {
                ID = 0
            };
            return View("Detail", result);
        }

        public ActionResult Update(int Id)
        {
            DetailModel result = _servant.Detail(Id);
            return View("Detail", result);

        }
        public string Save(DetailModel Save)
        {
            MessageModel message = new MessageModel { IsSuccess = true };
            var _userInfo = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            Save.LastUpdatedBy = _userInfo.ID;

            if (Save.ID != 0)
            {
                message = _servant.Update(Save);
            }
            else
            {
                message = _servant.Create(Save);
            }
            return JsonConvert.SerializeObject(messageHandler(message));
        }

        private MessageModel messageHandler(MessageModel Message)
        {
            if (!Message.IsSuccess)
            {
                MessageCode _code = (MessageCode)Enum.Parse(typeof(MessageCode), Message.MessagCode);
                switch (_code)
                {
                    case MessageCode.FormatError:
                        Message.Message = "上傳格式有誤";
                        break;

                    default:
                        break;
                }
            }
            return Message;

        }

    }
}