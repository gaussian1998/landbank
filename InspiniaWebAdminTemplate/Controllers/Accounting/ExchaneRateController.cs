﻿using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.ExchangeRate;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Common.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.ExchangeRate;
using Library.Servant.Servant.ExchangeRate.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [FunctionAuthorize(ID = "ExchangeRate")]
    [ExceptionHandle]
    public class ExchaneRateController : Controller
    {
        IExchangeRateServant _servant = ServantAbstractFactory.ExchangeRate(); //new LocalExchangeRateServant();
        IStaticDataServant _remoteStaticDataServant = ServantAbstractFactory.StaticData();
        // GET: ExchaneRate
        public ActionResult Index()
        {
            IndexModel model = new IndexModel
            {
                //Branches = _remoteStaticDataServant.GetBranchs(),
                AccountingBooks = _remoteStaticDataServant.GetAccountingBooks(),
                QueryResult = _servant.Index(new SearchModel {
                    CurrentPage = 1,
                    PageSize = 10,
                    BranchCodeId = -1,
                    AccountingBookCodeId = "-1"
                })
            };
            return View(model);
        }

        public ActionResult Query(SearchModel Search)
        {
            IndexModel model = new IndexModel {
                //Branches = _remoteStaticDataServant.GetBranchs(), 
                AccountingBooks = _remoteStaticDataServant.GetAccountingBooks(),
                QueryResult = _servant.Index(Search)
            };
            return View("Index", model);
        }

        public ActionResult Detail(int ID)
        {
            EditModel model = new EditModel
            {
                //Branches = _remoteStaticDataServant.GetBranchs(),
                Currencys = _remoteStaticDataServant.GetCurrencys(),
                AccountingBooks = _remoteStaticDataServant.GetAccountingBooks(),
                Data = _servant.Detail(ID)
            };

            if (String.IsNullOrEmpty(model.Data.DepartmentName))
            {
                //string branchCode = model.Data.BranchCode.ToString("000");
                //model.Data.DepartmentName = model.Branches.Find(x=>x.Value== branchCode).Name;
            }
            return View(model);
        }

        public ActionResult Create()
        {
            EditModel model = new EditModel
            {
                //Branches = _remoteStaticDataServant.GetBranchs(),
                Currencys = _remoteStaticDataServant.GetCurrencys(),
                AccountingBooks = _remoteStaticDataServant.GetAccountingBooks(),
                Data = new DetailModel
                {
                    //BranchCode = -1,
                    //DepartmentName = "",
                    Currency = "",
                    Rate = 1,
                    RateDate = DateTime.Today.ToString("yyyy-MM-dd"),
                    DayOfYear = DateTime.Today.DayOfYear
                }
            };
            return View("Detail", model);
        }

        public string Save(CreateModel Create)
        {
            var _userInfo = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            Create.LastUpdateBy = _userInfo.ID;

            MessageModel message = _servant.Create(Create);
            return JsonConvert.SerializeObject(message);
        }

        public string BatchDelete(List<int> Delete)
        {
            var _userInfo = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            BatchDeleteModel model = new BatchDeleteModel
            {
                IDList = Delete,
                LastUpdateBy = _userInfo.ID
            };

            MessageModel message = _servant.BatchDelete(model);
            return JsonConvert.SerializeObject(message);
        }
    }
}