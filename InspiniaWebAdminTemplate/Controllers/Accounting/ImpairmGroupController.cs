﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Servant.AccountingImpairmGroup;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.AccountingImpairmGroup.Models;
using Newtonsoft.Json;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Models.Accounting;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.AccountingSubject;
using InspiniaWebAdminTemplate.Attributes;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [FunctionAuthorize(ID = "ImpairmGroup")]
    [ExceptionHandle]
    public class ImpairmGroupController : Controller
    {
        private IAccountingImpairmGroupServant _servant = ServantAbstractFactory.AccountingImpairmGroupServant();
        //private IAccountingImpairmGroupServant _servant = new LocalAccountingImpairmGroupServant();
        private IAccountingSubjectServant _servantAccSubject = ServantAbstractFactory.AccountingSubject();// new LocalAccountingSubjectServant();
        IStaticDataServant _remoteStaticDataServant = ServantAbstractFactory.StaticData();
        // GET: ImpairmGroup
        public ActionResult Index()
        {
            AccountingImpairmGroupIndexModel result = new AccountingImpairmGroupIndexModel {
                Results = _servant.Index(new SearchModel
                {
                    CurrentPage = 1,
                    PageSize = 10,
                    MainKind = "1",
                    DetailKind = "",
                    FromNo = 0,
                    ToNo = 0,
                    FromSubject = "",
                    ToSubject = ""
                }),
                MainKinds = _remoteStaticDataServant.GetAssetMainKinds(),
                DetailKinds = _remoteStaticDataServant.GetAssetDetailKinds(),
                UseTypes = _remoteStaticDataServant.GetAssetUseTypes()
            };

            return View(result);
        }

        public ActionResult Query(SearchModel Search)
        {
            AccountingImpairmGroupIndexModel result = new AccountingImpairmGroupIndexModel
            {
                Results = _servant.Index(Search),
                MainKinds = _remoteStaticDataServant.GetAssetMainKinds(),
                DetailKinds = _remoteStaticDataServant.GetAssetDetailKinds(),
                UseTypes = _remoteStaticDataServant.GetAssetUseTypes()
            };
            return View("Index", result);
        }

        public ActionResult Create()
        {
            AccountingImpairmGroupDetailModel result = new AccountingImpairmGroupDetailModel
            {
                Result = new DetailModel
                {
                    ID = 0,
                    ImpairmGroupNo = 0,
                    AssetCategoryCode = "1",
                    AssetNumber = "",
                    UseType = "",
                    Account = ""
                },
                MainKinds = _remoteStaticDataServant.GetAssetMainKinds(),
                DetailKinds = _remoteStaticDataServant.GetAssetDetailKinds(),
                UseTypes = _remoteStaticDataServant.GetAssetUseTypes()
            };
            return View("Detail", result);
        }

        public ActionResult Detail(int Id)
        {
            AccountingImpairmGroupDetailModel result = new AccountingImpairmGroupDetailModel {
                Result = _servant.Detail(Id),
                MainKinds = _remoteStaticDataServant.GetAssetMainKinds(),
                DetailKinds = _remoteStaticDataServant.GetAssetDetailKinds(),
                UseTypes = _remoteStaticDataServant.GetAssetUseTypes()
            };
            
            return View(result);
        }

        public string Save(DetailModel Save)
        {
            MessageModel message = new MessageModel { IsSuccess = true };
            if (Save.ID > 0)
            {
                message = _servant.Update(Save);
            }
            else
            {
                message = _servant.Create(Save);
            }
            return JsonConvert.SerializeObject(messageHandler(message));
        }

        public string BatchDelete(BatchDeleteModel Batch)
        {
            MessageModel message = new MessageModel { IsSuccess = true };
            message = _servant.BatchDelete(Batch);
            return JsonConvert.SerializeObject(messageHandler(message));
        }

        public string GetAccountSubject(string Account)
        {
            return JsonConvert.SerializeObject(_servantAccSubject.GetAccountInfo(Account));
        }

        private MessageModel messageHandler(MessageModel Message)
        {
            if (!Message.IsSuccess)
            {
                MessageCode _code = (MessageCode)Enum.Parse(typeof(MessageCode), Message.MessagCode);
                switch (_code)
                {
                    default:
                        break;
                }
            }
            return Message;

        }


    }
}