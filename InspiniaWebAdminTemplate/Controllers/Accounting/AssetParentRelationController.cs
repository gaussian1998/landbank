﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.AccountingAssetParentRelation;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.AccountingAssetParentRelation.Models;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [FunctionAuthorize(ID = "AssetParentRelation")]
    [ExceptionHandle]
    public class AssetParentRelationController : Controller
    {
        IAccountingAssetParentRelationServant _servant = ServantAbstractFactory.AccountingAssetParentRelation();
        // GET: AssetParentRelation
        public ActionResult Index()
        {
            IndexResult result = _servant.Index(new SearchModel {
                CurrentPage = 1,
                PageSize = 10,
                RelationType = 0,
                AssetNumber = ""
            });
            return View(result);
        }

        public ActionResult Query(SearchModel Search)
        {
            IndexResult result = _servant.Index(Search);
            return View("Index", result);
        }
    }
}