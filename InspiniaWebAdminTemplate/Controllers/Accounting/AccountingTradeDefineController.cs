﻿using InspiniaWebAdminTemplate.Attributes;
using System.Web.Mvc;

using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common;
using System;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.AccountingTradeDefine.Models;
using Library.Servant.Servant.AccountingTradeDefine;
using InspiniaWebAdminTemplate.Models.Accounting;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{


    [Login]
    [FunctionAuthorize(ID = "AccountingTradeDefine")]
    [ExceptionHandle]
    public class AccountingTradeDefineController : Controller
    {
        private string NoDataDeleted = "沒有資料被選取";

        IAccountingTradeDefineServant _servant = ServantAbstractFactory.AccountingTradeDefine();
        IStaticDataServant _remoteStaticDataServant = ServantAbstractFactory.StaticData();
        // GET: AccountingTradeDefine
        public ActionResult Index()
        {
            AccountingTradeDefineIndexModel result = new AccountingTradeDefineIndexModel {
                TransTypes = _remoteStaticDataServant.GetTransTypes(),
                TradeTypes = _remoteStaticDataServant.GetTradeTypes(),
                Results = _servant.Index(new SearchModel
                {
                    CurrentPage = 1,
                    PageSize = 10,
                    TransType = "",
                    TradeType = "",
                    FromMasterNo = 0,
                    ToMasterNo = 0
                })
            };

            return View(result);
        }

        public ActionResult Query(SearchModel Search)
        {
            AccountingTradeDefineIndexModel result = new AccountingTradeDefineIndexModel
            {
                TransTypes = _remoteStaticDataServant.GetTransTypes(),
                TradeTypes = _remoteStaticDataServant.GetTradeTypes(),
                Results = _servant.Index(Search)
            };
            return View("Index", result);
        }

        public ActionResult Create()
        {
            AccountingTradeDefineDetailModel result = new AccountingTradeDefineDetailModel {
                TransTypes = _remoteStaticDataServant.GetTransTypes(),
                TradeTypes = _remoteStaticDataServant.GetTradeTypes(),
                AccountingBooks = _remoteStaticDataServant.GetAccountingBooks(),
                Branchs = _remoteStaticDataServant.GetBranchs(),
                Depts = _remoteStaticDataServant.GetDepts(),
                Result = new DetailModel {
                    ID = 0,
                    SoftLock = false
                }
            };
            return View("Detail", result);
        }

        public ActionResult Detail(int ID)
        {
            AccountingTradeDefineDetailModel result = new AccountingTradeDefineDetailModel
            {
                TransTypes = _remoteStaticDataServant.GetTransTypes(),
                TradeTypes = _remoteStaticDataServant.GetTradeTypes(),
                AccountingBooks = _remoteStaticDataServant.GetAccountingBooks(),
                Branchs = _remoteStaticDataServant.GetBranchs(),
                Depts = _remoteStaticDataServant.GetDepts(),
                Result = _servant.Detail(ID)
            };
            return View(result);
        }

        public string BatchDelete(BatchDeleteModel Batch)
        {
            var _userInfo = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            Batch.LastUpdateBy = _userInfo.ID;

            MessageModel result = _servant.BatchDelete(Batch);
            return JsonConvert.SerializeObject(messageHandler(result));
        }

        public string Save(DetailModel Save)
        {
            MessageModel message;
            if (Save.ID != 0)
            {
                message = _servant.Update(Save);
            }
            else
            {
                message = _servant.Create(Save);
            }
            return JsonConvert.SerializeObject(messageHandler(message));
        }

        private MessageModel messageHandler(MessageModel Message)
        {
            if (!Message.IsSuccess)
            {
                MessageCode _code = (MessageCode)Enum.Parse(typeof(MessageCode), Message.MessagCode);
                switch (_code)
                {
                    case MessageCode.NoDataDeleted:
                        Message.Message = NoDataDeleted;
                        break;

                    
                    default:
                        break;
                }
            }
            return Message;

        }
        public ActionResult GetTradeType(string id)
        {
            List<SelectedModel> _listTradeTypes = _remoteStaticDataServant.GetTradeTypes();
            if (id != null)
            {
                _listTradeTypes=_remoteStaticDataServant.GetTradeTypes(id);
            }
            List<SelectListItem> _list = new List<SelectListItem>();
            _list.Add(new SelectListItem { Value = "", Text = "全部" });
            foreach (SelectedModel item in _listTradeTypes)
            {
                _list.Add(new SelectListItem { Value = item.Value, Text = item.Name });
            }
            return Content(JsonConvert.SerializeObject(_list), "application/json");
        }
    }
}