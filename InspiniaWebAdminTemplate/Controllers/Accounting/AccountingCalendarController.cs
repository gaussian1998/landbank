﻿using InspiniaWebAdminTemplate.Attributes;
using System.Web.Mvc;
using Library.Servant.Servant.AccountingCalendar;
using Library.Servant.Servant.AccountingCalendar.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common;
using InspiniaWebAdminTemplate.Models.Accounting;
using System;
using Library.Servant.Servants.AbstractFactory;
using Newtonsoft.Json;
using InspiniaWebAdminTemplate.Servants.UserInformation;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [FunctionAuthorize(ID = "AccountingCalendar")]
    [ExceptionHandle]
    public class AccountingCalendarController : Controller
    {
        IAccountingCalendarServant _servant = ServantAbstractFactory.AccountingCalendar();
        //IAccountingCalendarServant _servant = new LocalAccountingCalendarServant();
        //RemoteStaticDataServant _remoteStaticDataServant = new RemoteStaticDataServant();
        IStaticDataServant _remoteStaticDataServant = ServantAbstractFactory.StaticData();// new LocalStaticDataServant();
        string noDataToCopy = "沒有會計曆可供複制";

        // GET: AccountingCalendar
        public ActionResult Index()
        {
            var booklist = _remoteStaticDataServant.GetAccountingBooks();
            AccountingCalendarIndex result = new AccountingCalendarIndex {
                AccountingBooks = booklist,
                AccountingYears = _servant.GetCalendarYears(),
                Results = _servant.Index(new SearchModel {
                    CurrentPage = 1,
                    PageSize = 10,
                    AccountingBook = "NTD_MP_IFRS",
                    AccountingCalendarType = "A",
                    AccountingYear = DateTime.Today.Year.ToString()
                })
            };

            return View(result);
        }

        public ActionResult Query(SearchModel Search)
        {
            AccountingCalendarIndex result = new AccountingCalendarIndex
            {
                AccountingBooks = _remoteStaticDataServant.GetAccountingBooks(),
                AccountingYears = _servant.GetCalendarYears(),
                Results = _servant.Index(Search)
            };

            return View("Index", result);
        }

        public ActionResult Create()
        {
            int year = DateTime.Today.Year;
            AccountingCalendarDetail result = new AccountingCalendarDetail
            {
                Results = new DetailModel {
                    ID = 0,
                    CalendarType = "Y",
                    TermBasis = 25,
                    CalendarValue = "",
                    BeginDate = new DateTime(year, 1, 1),
                    EndDate = new DateTime(year, 12, 31)
                },
                AccountingBooks = _remoteStaticDataServant.GetAccountingBooks()

            };
            return View("Detail", result);
        }
        public ActionResult Detail(int ID)
        {
            AccountingCalendarDetail result = new AccountingCalendarDetail
            {
                Results = _servant.Detail(ID),
                AccountingBooks = _remoteStaticDataServant.GetAccountingBooks()

            };
            return View(result);
        }

        public string BatchDelete(BatchDeleteModel Batch)
        {
            MessageModel message = new MessageModel { IsSuccess = true, Message = "" };
            if (Batch.IDList.Count > 0)
            {
                message = _servant.BatchDelete(Batch);
            }
            return JsonConvert.SerializeObject(messageHandler(message));
        }

        public string Save(DetailModel Save)
        {
            MessageModel message;
            var _userInfo = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            Save.LastUpdateBy = _userInfo.ID;

            if (Save.ID > 0)
            {
                message = _servant.Update(Save);
            }
            else
            {
                message = _servant.Create(Save);
            }
            return JsonConvert.SerializeObject(messageHandler(message));
        }

        public string Copy(CopyModel Copy)
        {
            MessageModel message;
            var _userInfo = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            Copy.LastUpdatedBy = _userInfo.ID;
            message = _servant.Copy(Copy);
            return JsonConvert.SerializeObject(messageHandler(message));
        }
        /// <summary>
        /// define display string of message
        /// </summary>
        /// <param name="Message"></param>
        /// <returns></returns>
        private MessageModel messageHandler(MessageModel Message)
        {
            if (!Message.IsSuccess)
            {
                MessageCode _code = (MessageCode)Enum.Parse(typeof(MessageCode), Message.MessagCode);
                switch (_code)
                {
                    case MessageCode.NoDataToCopy:
                        Message.Message = noDataToCopy;
                        break;
                    default:
                        break;
                }
            }
            return Message;

        }
    }
}