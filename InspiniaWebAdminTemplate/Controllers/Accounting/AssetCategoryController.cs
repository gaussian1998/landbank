﻿using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Entity.Edmx;
using Library.Servant.Servant.AssetCategory;
using Library.Servant.Servant.AssetCategory.Models;
using Library.Servant.Servants.AbstractFactory;
using System.Linq;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [ExceptionHandle]
    public class AssetCategoryController : Controller
    {
        IAssetCategoryServant _servant;

        public AssetCategoryController()
        {
            _servant = ServantAbstractFactory.AssetCategory();
            _servant.UserId = UserInfoServant.Details(System.Web.HttpContext.Current).ID;
        }
        // GET: AssetCategory
        public ActionResult Index(string errorMessage)
        {
            if (!string.IsNullOrEmpty(errorMessage))
            {
                ModelState.AddModelError("Summary", errorMessage);
            }
            return View();
        }

        public ActionResult _CategoryMaps()
        {
            var models = _servant.GetMainKinds().OrderBy(p => p.No);
            return PartialView(models);
        }

        public ActionResult _UseTypes()
        {
            var models = _servant.GetUseTypes();
            return PartialView(models);
        }

        /// <summary>
        /// 取得資產大項分類
        /// </summary>
        /// <returns></returns>
        public JsonResult GetMainKindList()
        {
            var mainKinds = _servant.GetMainKinds().ToList();
            var n = mainKinds.Select(p => new
            {
                ID = p.ID,
                Key = p.Name,
                Value = p.No
            }).OrderBy(p => p.Value);
            return Json(n, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取得資產細項分類
        /// </summary>
        /// <param name="mainKindNo"></param>
        /// <returns></returns>
        public JsonResult GetDetailKindList(string mainKindNo)
        {
            var detailKinds = _servant.GetListDetailKindByMainKindNo(mainKindNo);
            var n = detailKinds.Select(p => new
            {
                ID = p.ID,
                Key = p.Name,
                Value = p.No
            }).OrderBy(p => p.Value);
            return Json(n, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取得自用類別
        /// </summary>
        /// <returns></returns>
        public JsonResult GetUseTypeList()
        {
            var useTypes = _servant.GetUseTypes().ToList();
            var n = useTypes.Select(p => new
            {
                ID = p.ID,
                Key = p.Name,
                Value = p.No
            });

            return Json(n, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ModifyMainKind(AssetCategoryMainKindsModel model)
        {
            if (ModelState.IsValid)
            {
                var mainKind = _servant.GetMainKindByNo(model.No);
                if (mainKind==null || mainKind.No== null)
                {
                    _servant.AddNewMainKind(model);
                }
                else
                {
                    mainKind.Name = model.Name;
                    _servant.UpdateMainKind(mainKind);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult DeleteMainKind(AssetCategoryMainKindsModel model)
        {
            AssetCategoryMainKindsModel _model = _servant.GetMainKindByNo(model.No);
            if (_model != null && _model.No==model.No)
            {
                try
                {
                    _servant.DeleteMainKind(model.No);
                }
                catch (System.Exception)
                {
                    return RedirectToAction("Index", new { errorMessage = "無法刪除" });
                }
            }
            return RedirectToAction("Index");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ModifyDetailKind(AssetCategoryDetailKindsModel model)
        {
            if (ModelState.IsValid)
            {
                var detailKind = _servant.GetDetailKind(model.ID);
                if (detailKind == null || detailKind.ID==0)
                {
                    RedirectToAction("Index", new { errorMessage = "找不到明細類別可更新" });
                }
                else
                {
                    detailKind.Name = model.Name;
                    detailKind.No= model.No;
                    _servant.UpdateDetailKind(detailKind);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }

        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddDetailKind(AssetCategoryDetailKindsModel model)
        {
            if (ModelState.IsValid)
            {
                AssetCategoryDetailKindsModel detailKind = _servant.GetDetailKind(model.Main_Kind_ID, model.No);
                if (detailKind == null || (detailKind.Main_Kind_ID != model.Main_Kind_ID && detailKind.No==null))
                {
                    _servant.AddNewDetailKind(model);
                }
                else
                {
                    RedirectToAction("Index", new { errorMessage = "代碼已存在" });
                }
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }

        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult DeleteDetailKind(AssetCategoryDetailKindsModel model)
        {
            AssetCategoryDetailKindsModel _model = _servant.GetDetailKind(model.Main_Kind_ID, model.No);
            if (_model != null && _model.Main_Kind_ID == model.Main_Kind_ID && _model.No == model.No) 
            {
                try
                {
                    _servant.DeleteDetailKind(model.Main_Kind_ID, model.No);
                }
                catch (System.Exception)
                {
                    return RedirectToAction("Index", new { errorMessage = "無法刪除" });
                }
            }
            return RedirectToAction("Index");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ModifyUseType(AssetCategoryUseTypesModel model)
        {
            if (ModelState.IsValid)
            {
                var useType = _servant.GetUseTypeByNo(model.No);
                if (useType == null || useType.No==null)
                {
                    _servant.AddNewUserType(model);
                }
                else
                {
                    useType.Name = model.Name;
                    _servant.UpdateUseType(useType);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }

        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult DeleteUseType(AssetCategoryUseTypesModel model)
        {
            AssetCategoryUseTypesModel _model = _servant.GetUseTypeByNo(model.No);
            if (_model != null && model.No==_model.No)
            {
                try
                {
                    _servant.DeleteUserType(model.No);
                }
                catch (System.Exception)
                {
                    return RedirectToAction("Index", new { errorMessage = "無法刪除" });
                }
            }
            return RedirectToAction("Index");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddNewCategory(AssetCategoryModel model)
        {
            //整批建檔使用
            //var mainKinds = _servant.GetMainKinds();
            //var useTypes = _servant.GetUseTypes();
            //foreach (var mainKind in mainKinds)
            //{
            //    foreach (var detailKind in mainKind.AS_Asset_Category_Detail_Kinds)
            //    {
            //        foreach (var useType in useTypes)
            //        {
            //            var entity = _servant.GetCategory(mainKind.ID, detailKind.ID, useType.ID);
            //            if (entity == null)
            //            {
            //                _servant.AddNewCategory(new AS_Asset_Category() {
            //                    Main_Kind_ID = mainKind.ID,
            //                    Detail_Kind_ID = detailKind.ID,
            //                     Use_Type_ID = useType.ID
            //                });
            //            }
            //            else
            //            {
            //                return RedirectToAction("Index", new { errorMessage = "資料已存在" });
            //            }
            //        }
            //    }
            //}


            var entity = _servant.GetCategory(model.Main_Kind_ID, model.Detail_Kind_ID, model.Use_Type_ID);
            if (entity == null ||(entity.Main_Kind_ID!= model.Main_Kind_ID &&
                entity.Detail_Kind_ID!= model.Detail_Kind_ID &&entity.Use_Type_ID!=model.Use_Type_ID ))
            {
                _servant.AddNewCategory(model);
            }
            else
            {
                return RedirectToAction("Index", new { errorMessage = "資料已存在" });
            }
            return RedirectToAction("Index");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult DeleteCategory(AssetCategoryModel model)
        {
            AssetCategoryModel _model = _servant.GetCategory(model.Main_Kind_ID, model.Detail_Kind_ID, model.Use_Type_ID);
            if (_model != null && _model.Main_Kind_ID==model.Main_Kind_ID && 
                _model.Detail_Kind_ID== model.Detail_Kind_ID && _model.Use_Type_ID== model.Use_Type_ID)
            {
                try
                {
                    _servant.DeleteCategory(model);
                }
                catch (System.Exception)
                {
                    return RedirectToAction("Index", new { errorMessage = "無法刪除" });
                }
            }
            return RedirectToAction("Index");
        }
    }
}