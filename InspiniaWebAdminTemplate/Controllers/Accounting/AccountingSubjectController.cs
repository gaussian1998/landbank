﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.AccountingSubject;
using Library.Servant.Servant.AccountingSubject.Models;
using InspiniaWebAdminTemplate.Models.Accounting;
using Library.Servant.Servant.Common;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.Common.Models;
using Newtonsoft.Json;
using Library.Servant.Servant.FormNo;
using Library.Servant.Servant.ApprovalTodo;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [FunctionAuthorize(ID = "AccountingSubject")]
    [ExceptionHandle]
    public class AccountingSubjectController : Controller
    {
        private readonly IAccountingSubjectServant _accountingServant;
        IStaticDataServant _remoteStaticDataServant = ServantAbstractFactory.StaticData();
        //IApprovalTodoServant _approvalTodoServant = ServantAbstractFactory.ApprovalTodo();
        public AccountingSubjectController()
        {
            _accountingServant = ServantAbstractFactory.AccountingSubject(); //new LocalAccountingSubjectServant();
        }

        // GET: AccountingEntry
        public ActionResult Index()
        {
            IndexResult result = _accountingServant.Index(new SearchModel {
                CurrentPage = 1,
                PageSize = 10,
                AssetLiablty = -1,
                FromDate = DateTime.MinValue,
                ToDate = DateTime.MaxValue,
                FromAccountSubject = "",
                ToAccountSubject = ""
            });

            return View(result);
        }

        public ActionResult Query(SearchModel Search)
        {
            IndexResult result = _accountingServant.Index(Search);

            return View("Index", result);
        }

        public ActionResult Detail(int ID)
        {
            AccountingSubjectDetailModel result = new AccountingSubjectDetailModel {
                Result = _accountingServant.Detail(ID),
                AccountingBooks = _remoteStaticDataServant.GetAccountingBooks()
            };
            return View(result);
        }

        public ActionResult Create()
        {
            AccountingSubjectDetailModel result = new AccountingSubjectDetailModel
            {
                Result = new DetailModel {
                    ID = 0,
                    AccountType = "R",
                    DBCR = "D",
                    AssetLiablty = "A",
                    OpenDate = DateTime.MinValue,
                    CancelDate = DateTime.MaxValue,
                    LastUpdateDate = DateTime.Today
                },
                AccountingBooks = _remoteStaticDataServant.GetAccountingBooks()
            };

            return View("Detail", result);
        }

        public string Save(DetailModel Save)
        {
            string formNo = LocalApprovalTodoServant.Create( this.UserID(), _accountingServant.GetFlowFormNo(), "GLAC", "AccountingSubjectListener");
            LocalApprovalTodoServant.Start( this.UserID(), formNo, JsonConvert.SerializeObject(Save));

            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            //if (Save.ID > 0)
            //{
            //    message = _accountingServant.Update(Save);
            //}
            //else
            //{
            //    message = _accountingServant.Create(Save);
            //}
            return JsonConvert.SerializeObject(messageHandler(message));
        }

        public ActionResult Export(AccountingSubjectExportQueryModel Search)
        {
            SearchModel query = new SearchModel
            {
                AssetLiablty = Search.ExportAL,
                FromAccountSubject = Search.ExportBeginSubject,
                ToAccountSubject = Search.ExportEndSubject,
                FromDate = Search.ExportFromDate,
                ToDate = Search.ExportToDate
            };

            return File(_accountingServant.ExportData(query),
            "application/csv",
            string.Format("V800 會計科目明細_{0}.csv", DateTime.Now.ToString("yyyyMMddhhmmssms")));
        }

        public string Upload(HttpPostedFileBase file)
        {
            MessageModel message = new MessageModel { IsSuccess = true };
            List<DetailModel> list = new List<DetailModel>();

            if (file != null && file.ContentLength > 0)
            {
                
                int i = 0;
                using (System.IO.StreamReader reader = new System.IO.StreamReader(file.InputStream))
                {
                    try
                    {
                        while (!reader.EndOfStream)
                        {
                            string line = reader.ReadLine();
                            if (i > 0)
                            {
                                //skip first line
                                string[] values = line.Split(',');
                                list.Add(new DetailModel
                                {
                                    AccountingBook = values[0],
                                    Account = values[1],
                                    IFRSAccount = values[2],
                                    AccountName = values[3],
                                    AssetLiablty = values[4] == "資產" ? "A" : "L",
                                    OpenDate = values[5] != "" ? Convert.ToDateTime(values[5]) : DateTime.MinValue,
                                    CancelDate = values[6] != "" ? Convert.ToDateTime(values[6]) : DateTime.MinValue,
                                    DBCR = values[7] == "借" ? "D" : "C",
                                    LedgerType = values[8],
                                    HeadOfficeOnly = values[9] == "是" ? true : false,
                                    IsBankingDept = values[10] == "是" ? true : false,
                                    IsTrustDept = values[11] == "是" ? true : false,
                                    IsBondDept = values[12] == "是" ? true : false,
                                    IsCreditCardDept = values[13] == "是" ? true : false,
                                    IsBudgetLimit = values[14] == "是" ? true : false,
                                    IsAutoLink = values[15] == "是" ? true : false,
                                    IncomeType = values[16],
                                    OppositeAccount = values[17],
                                    SingleDeclareAcc = values[18],
                                    AccountType = values[19].Substring(0, 1),
                                    Notes = values[20]
                                });
                            }
                            i++;
                        }
                    }
                    catch (System.Exception ex)
                    {
                        message.IsSuccess = false;
                        message.MessagCode = MessageCode.FormatError.ToString();
                    }

                    message = _accountingServant.ImportData(list);

                }
            }
            else
            {
                message.IsSuccess = false;
                message.Message = "上傳失敗";

            }
            return JsonConvert.SerializeObject(messageHandler(message));
        }

        public string BatchDelete(BatchDeleteModel Batch)
        {
            var _userInfo = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            Batch.LastUpdateBy = _userInfo.ID;
            MessageModel message = new MessageModel { IsSuccess = true, Message = "" };
            if (Batch.IDList.Count > 0)
            {
                message = _accountingServant.BatchDelete(Batch);
            }
            return JsonConvert.SerializeObject(messageHandler(message));
        }

        private MessageModel messageHandler(MessageModel Message)
        {
            if (!Message.IsSuccess)
            {
                MessageCode _code = (MessageCode)Enum.Parse(typeof(MessageCode), Message.MessagCode);
                switch (_code)
                {
                    case MessageCode.FormatError:
                        Message.Message = "上傳的資料或格式有錯誤";
                        break;

                    default:
                        break;
                }
            }
            return Message;

        }

        public ActionResult VerifyForm(string Data)
        {
            AccountingSubjectVerifyFormModel result = new AccountingSubjectVerifyFormModel { Last = null };
            DetailModel detail = JsonConvert.DeserializeObject<DetailModel>(Data);
            result.Current = detail;
            if (detail.ID != 0)
            {
                DetailModel last = _accountingServant.Detail(detail.ID);
                result.Last = last;
            }
            return View(result);
        }

        [HttpPost]
        public ActionResult ReceiveXls()
        {

            return View();
        }

        [HttpGet]
        public ActionResult GetAccountSubjectInfo(string account)
        {
            return Json(_accountingServant.GetAccountInfo(account) ?? new AsGlAccountModel()
            {
                 Account_Name="查無此科目",
                 IFRS_Account = "查無此科目"
            }, JsonRequestBehavior.AllowGet);
        }
    }

}