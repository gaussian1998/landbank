﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.KeepPosition.Models;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.KeepPosition;
using InspiniaWebAdminTemplate.Models.Accounting;
using Library.Servant.Servant.Common;
using Newtonsoft.Json;
using InspiniaWebAdminTemplate.Servants.UserInformation;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    public class KeepPositionController : Controller
    {
        IKeepPositionServant _servant = ServantAbstractFactory.KeepPosition();
        IStaticDataServant _remoteStaticDataServant = ServantAbstractFactory.StaticData();

        // GET: KeepPosition
        public ActionResult Index()
        {
            AccountingKeepPositionIndexModel result = new AccountingKeepPositionIndexModel {
                Branchs = _remoteStaticDataServant.GetBranchs(),
                Depts = _remoteStaticDataServant.GetDepts(),
                SubDepts = _servant.GetSubDept(),
                Results = _servant.Index(new SearchModel {
                    CurrentPage = 1,
                    PageSize = 10,
                    Layer = -1,
                    BranchCode = "",
                    DeptCode = "",
                    SubDeptCode = ""
                })
            };
            return View(result);
        }

        public ActionResult Query(SearchModel Search)
        {
            AccountingKeepPositionIndexModel result = new AccountingKeepPositionIndexModel
            {
                Branchs = _remoteStaticDataServant.GetBranchs(),
                Depts = _remoteStaticDataServant.GetDepts(),
                SubDepts = _servant.GetSubDept(),
                Results = _servant.Index(Search)
            };
            return View("Index", result);
        }

        public ActionResult Detail(int ID)
        {
            AccountingKeepPositionDetailModel result = new AccountingKeepPositionDetailModel {
                Branchs = _remoteStaticDataServant.GetBranchs(),
                Depts = _remoteStaticDataServant.GetDepts(),
                Result = _servant.Detail(ID)
            };
            return View(result);
        }

        public ActionResult Create()
        {
            AccountingKeepPositionDetailModel result = new AccountingKeepPositionDetailModel
            {
                Branchs = _remoteStaticDataServant.GetBranchs(),
                Depts = _remoteStaticDataServant.GetDepts(),
                Result = new DetailModel
                {
                    ID = 0,
                    DeptCode = "",
                    DeptName = "",
                    SubDeptCode = "",
                    SubDeptName = "",
                    Floor = "",
                    FloorName = "",
                    PositionCode = "",
                    PositionName = ""
                }
            };
            return View("Detail", result);
        }

        public string Save(DetailModel Save)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            var _userInfo = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            Save.LastUpdateBy = _userInfo.ID;

            if (Save.ID != 0)
            {
                message = _servant.Update(Save);
            }
            else
            {
                message = _servant.Create(Save);
            }

            return JsonConvert.SerializeObject(messageHandler(message));
        }
        public string BatchDelete(BatchDeleteModel Batch)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            if(Batch.IDList.Count > 0)
            {
                message = _servant.BatchDelete(Batch);
            }
            return JsonConvert.SerializeObject(messageHandler(message));
        }

        private MessageModel messageHandler(MessageModel Message)
        {
            if (!Message.IsSuccess)
            {
                MessageCode _code = (MessageCode)Enum.Parse(typeof(MessageCode), Message.MessagCode);
                switch (_code)
                {

                    default:
                        break;
                }
            }
            return Message;

        }

    }
}