﻿using InspiniaWebAdminTemplate.Attributes;
using System.Web.Mvc;
using Library.Servant.Servant.Currency.Model;
using Library.Servant.Servant.Currency;
using Library.Servant.Servant.Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Servants.UserInformation;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [FunctionAuthorize(ID = "Currency")]
    [ExceptionHandle]
    public class CurrencyController : Controller
    {
        private ICurrencyServant _servant = ServantAbstractFactory.Currency();

        private string currencyIsExist = "此幣別已存在，請修正編碼";
        private string currencyIsNotExits = "此幣別不存在，無法儲存";

        // GET: Currency
        public ActionResult Index()
        {
            SearchModel _search = new SearchModel { CurrentPage = 1, PageSize = 10 };
            IndexResult _result = _servant.Index(_search);
            return View(_result);
        }

        public ActionResult Page(int page)
        {
            SearchModel _search = new SearchModel { CurrentPage = page, PageSize = 10 };
            IndexResult _result = _servant.Index(_search);
            return View("Index", _result);
        }

        public string Create(CreateModel CreateModel)
        {
            var _userInfo = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            CreateModel.LastUpdateBy = _userInfo.ID;
            MessageModel _message = _servant.Create(CreateModel);
            return JsonConvert.SerializeObject(messageHandler(_message));
        }

        public ActionResult Detail(int ID)
        {
            UpdateModel _update = _servant.Detail(ID);
            return View(_update);
        }

        public string Update(UpdateModel UpdateModel)
        {
            var _userInfo = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            UpdateModel.LastUpdateBy = _userInfo.ID;
            MessageModel _message = _servant.Update(UpdateModel);
            return JsonConvert.SerializeObject(messageHandler(_message));
        }

        public string BatchDelete(IList<int> List)
        {
            BatchDelete _delete = new BatchDelete { IDList = List };
            MessageModel _message = _servant.BatchDelete(_delete);
            return JsonConvert.SerializeObject(_message);
        }

        private MessageModel messageHandler(MessageModel Message)
        {
            if (!Message.IsSuccess)
            {
                MessageCode _code = (MessageCode)Enum.Parse(typeof(MessageCode), Message.MessagCode);
                switch (_code)
                {
                    case MessageCode.CurrencyIsExisted:
                        Message.Message = currencyIsExist;
                        break;

                    case MessageCode.CurrencyNotExisted:
                        Message.Message = currencyIsNotExits;
                        break;

                    default:
                        break;
                }
            }
            return Message;
            
        }

    }
}