﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.LeaseRights
{
    public class LeaseRightsWriteOffSettingController : Controller
    {

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsWriteOffSetting")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }
    }
}