﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Servants.Search;
using InspiniaWebAdminTemplate.Models.MP;
using Library.Servant.Servant.MP;
using Library.Servant.Servant.MP.MPModels;
using Newtonsoft.Json;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.ApprovalFlow;
using Library.Servant.Servant.UserInformation.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.Authentication.Models;
using System.IO;
using System.Text;
using Library.Servant.Repository.MP;
using Library.Servant.Servant.Email;
using Library.Servant.Servant.Email.Models;
using Library.Servant.Servant.LeaseRights;
using Library.Servant.Repository.LeaseRights;

namespace InspiniaWebAdminTemplate.Controllers.LeaseRights
{
    public class LeaseRightsAddedController : Controller
    {
        private ILeaseRightsServant LeaseRightsServant = ServantAbstractFactory.LeaseRights();
        private string Type = LeaseRightsGeneral.GetFormCode("LeaseRightsAdded");
        int PageSize = 10;

        private UserDetail UserInfo
        {
            get
            {
                var u = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
                return (u != null) ? this.LeaseRightsServant.GetUserInfo(u.ID) : new UserDetail();
            }
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            SearchModel condition = new SearchModel()
            {
                Transaction_Type = Type,
                Page = 1,
                PageSize = PageSize
            };
            IndexModel IndexQuery = this.LeaseRightsServant.Index(condition);

            IndexViewModel vm = new IndexViewModel()
            {
                condition = MPHelper.ToSearchViewModel(IndexQuery.condition),
                Items = IndexQuery.Items.Select(o => MPHelper.ToMPHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(MPHelper.GetMPView("Index"), vm);
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexQuery(SearchViewModel condition)
        {
            condition.Transaction_Type = Type;
            condition.Page = 1;
            condition.PageSize = PageSize;
            IndexModel IndexQuery = this.LeaseRightsServant.Index(MPHelper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => MPHelper.ToMPHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(MPHelper.GetMPView("Index"), vm);
        }


        /// <summary>
        /// 分頁Action
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public ActionResult UpdatePage(int page)
        {
            SearchViewModel condition = new SearchViewModel() { Transaction_Type = Type, Page = page, PageSize = PageSize };
            IndexModel IndexQuery = this.LeaseRightsServant.Index(MPHelper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => MPHelper.ToMPHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(MPHelper.GetMPView("Index"), vm);
        }


        /// <summary>
        /// 新增單據
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        [HttpGet]
        public ActionResult New()
        {
            ViewBag.TRXID = this.LeaseRightsServant.CreateTRXID(Type);
            MPHeaderViewModel Header = new MPHeaderViewModel()
            {
                Transaction_TypeName = "租賃權益新增單",
                Flow_Status = "0",
                Flow_StatusName = "新增",
                Office_Branch = this.UserInfo.BranchCode + "." + this.UserInfo.DepartmentCode,
                Source = this.UserInfo.BranchCode == "001" ? "H" : "B",
                Book_Type = MPHelper.GetDefaultBookType(this.UserInfo.BranchCode)
            };
            MPViewModel vm = new MPViewModel()
            {
                Header = Header,
                User = this.UserInfo,
                Assets = new List<AssetViewModel>()
            };
            return View(MPHelper.GetMPView("MP"), vm);
        }

        /// <summary>
        /// 編輯單據畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public ActionResult Edit(string id, string signmode)
        {
            var Detail = this.LeaseRightsServant.GetDetail(id);

            var vm = new MPViewModel()
            {
                Header = MPHelper.ToMPHeaderViewModel(Detail.Header),
                Assets = Detail.Assets.Select(m => MPHelper.ToAssetViewModel(m)),
                User = this.UserInfo
            };

            if (!string.IsNullOrEmpty(signmode))
            {
                ViewBag.SignMode = true;
            }

            string json = JsonConvert.SerializeObject(vm);

            return View(MPHelper.GetMPView("MP"), vm);
        }

        /// <summary>
        /// 處理單據頭
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public ActionResult HandleHeader(MPHeaderViewModel header)
        {
            header.Transaction_Type = Type;
            header.Remark = (header.Remark == null) ? "" : header.Remark;
            header.Created_By = this.UserInfo.ID.ToString();
            header.Create_Time = DateTime.Now;
            header.Last_Updated_By = this.UserInfo.ID.ToString();
            TempData["message"] = "單據儲存成功!";
            return RedirectToAction("Edit", new { id = this.LeaseRightsServant.HandleHeader(MPHelper.ToMPHeaderModel(header)).TRX_Header_ID });
        }

        /// <summary>
        /// 新增資產畫面
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public ActionResult Asset_New(MPHeaderViewModel header)
        {
            ViewBag.FunctionFrom = (TempData["FunctionFrom"] != null) ? Convert.ToString(TempData["FunctionFrom"]) : "";
            TempData["Header"] = header;
            header.Transaction_Type = Type;
            ViewBag.Title = "新增資產";
            AssetViewModel vm = new AssetViewModel()
            {
                IsOversea = (MPHelper.GetDefaultBookType(header.Book_Type) != "NTD_MP_IFRS") ? "Y" : "",
                Asset_Category_Code = "..B",
                Location_Disp = header.Office_Branch,
                Assets_Unit = 1,
                Assigned_Branch = header.Office_Branch,
                Currency = MPHelper.GetDefaultCurrency(header.Book_Type)
            };
            return View(MPHelper.GetMPView("Asset"), vm);
        }

        /// <summary>
        /// 新增資產
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public ActionResult Asset_Create(AssetViewModel asset)
        {
            MPHeaderViewModel Header = null;
            if (TempData["Header"] != null)
            {
                Header = TempData["Header"] as MPHeaderViewModel;
                Header.Created_By = this.UserInfo.ID.ToString();
                Header.Create_Time = DateTime.Now;
                Header.Last_Updated_By = "";
            }
            if (Header != null && Header.Remark == null)
            {
                Header.Remark = "";
            }
            if (string.IsNullOrEmpty(asset.Description))
            {
                asset.Description = "";
            }
            asset.Created_By = this.UserInfo.ID.ToString();
            asset.Create_Time = DateTime.Now;
            asset.Last_Updated_By = "";
            if (asset.Assets_Parent_Number == null)
            {
                asset.Assets_Parent_Number = "";
            }
            if (asset.IsOversea == null)
            {
                asset.IsOversea = "";
            }

            var MP = new MPModel()
            {
                Assets = new List<AssetViewModel>() { asset }.Select(o => MPHelper.ToAssetModel(o))
            };
            if (Header != null)
            {
                MP.Header = MPHelper.ToMPHeaderModel(Header);
            };


            var Result = this.LeaseRightsServant.Create_Asset(MP);

            TempData["message"] = "資產新增成功!";
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }

        /// <summary>
        /// 編輯資產畫面
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public ActionResult Asset_Edit(int id)
        {
            var vm = this.LeaseRightsServant.GetAssetDetail(id);
            TempData["Header"] = MPHelper.ToMPHeaderViewModel(this.LeaseRightsServant.GetHeader(vm.Asset_Number, Type));
            TempData["DeprnRecord"] = this.LeaseRightsServant.GetDeprnRecord(vm.Asset_Number);
            ViewBag.Title = "編輯資產";
            ViewBag.FunctionFrom = (TempData["FunctionFrom"] != null) ? Convert.ToString(TempData["FunctionFrom"]) : "";
            if (TempData["SignMode"] != null)
            {
                ViewBag.SignMode = true;
            }
            return View(MPHelper.GetMPView("Asset"), MPHelper.ToAssetViewModel(vm));
        }

        /// <summary>
        /// 編輯資產
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public ActionResult Asset_Update(AssetViewModel asset)
        {
            MPHeaderViewModel Header = TempData["Header"] as MPHeaderViewModel;
            if (Header.Remark == null)
            {
                Header.Remark = "";
            }
            if (asset.Assets_Parent_Number == null)
            {
                asset.Assets_Parent_Number = "";
            }
            if (asset.IsOversea == null)
            {
                asset.IsOversea = "";
            }
            if (string.IsNullOrEmpty(asset.Description))
            {
                asset.Description = "";
            }
            asset.Last_Updated_By = this.UserInfo.ID.ToString();
            var Result = this.LeaseRightsServant.Update_Asset(new MPModel()
            {
                Header = MPHelper.ToMPHeaderModel(Header),
                Assets = new List<AssetViewModel>() { asset }.Select(o => MPHelper.ToAssetModel(o))
            });
            TempData["message"] = "資產編輯成功!";
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }

        /// <summary>
        /// 跑簽核流程
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public ActionResult CreateFlow(MPHeaderViewModel header)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            int DocID;
            string message = "提交成功!";
            if (LeaseRightsServant.GetDocID(header.Transaction_Type + header.Source, out DocID))
            {
                this.LeaseRightsServant.UpdateFlowStatus(header.TRX_Header_ID, "1");
                LocalApprovalTodoServant.Create(user.ID, DocID, header.TRX_Header_ID, "LeaseRightsAdded");
                LocalApprovalTodoServant.Start(user.ID, header.TRX_Header_ID, header.TRX_Header_ID);
            }
            else
            {
                message = "查無此表單!!";
            }
            //直接測試核准
            //LeaseRightsServant.CreateToMP(header.TRX_Header_ID);

            TempData["message"] = message;
            return RedirectToAction("Edit", new { id = header.TRX_Header_ID });
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public ActionResult MPNotice(string id)
        {
            var Detail = this.LeaseRightsServant.GetDetail(id);

            var vm = new MPViewModel()
            {
                Header = MPHelper.ToMPHeaderViewModel(Detail.Header),
                Assets = Detail.Assets.Select(m => MPHelper.ToAssetViewModel(m))
            };
            return View(MPHelper.GetMPView("MPNotice"), vm);
        }

        private readonly IEmailServant Servant = ServantAbstractFactory.Email();

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public JsonResult MPSendNotice()
        {
            //int sender = AS_Users_Records.First(user => user.User_Name == "王興測").ID;
            //int recver = AS_Users_Records.First(user => user.User_Name == "張家測").ID;

            var result = Servant.Send(new EmailModel
            {
                UserID = 5322,
                RecverID = 2784,
                Title = "台北分行 – 保管人通知信 (動產新增) - 測試信件",
                Body = @"李寧,您好：<br/>
                           本分行採購車輛與設備，如附件。<br/>

                           請您查閱，本次動產資料中有您保管的物品。<br/>

                           如保管人資訊確認無誤，請點擊 [簽收] 。謝謝。<br/>"
            });
            return Json(new { msg = "發送成功!" });
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public ActionResult MPAssetsSignUp()
        {
            return View();
        }

        /// <summary>
        /// 複製資產
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public ActionResult CopyAsset(string TRX_Header_ID, string CopyAssetID, string CopyCount)
        {
            this.LeaseRightsServant.CopyAsset(CopyAssetID, Convert.ToInt32(CopyCount));
            TempData["message"] = "複製成功!";
            return RedirectToAction("Edit", new { id = TRX_Header_ID });
        }


        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public ActionResult Import(MPHeaderViewModel Header, HttpPostedFileBase fileImport)
        {
            //Header
            Header.Transaction_Type = Type;
            Header.Created_By = this.UserInfo.ID.ToString();
            Header.Create_Time = DateTime.Now;
            Header.Last_Updated_By = "";
            if (Header != null && Header.Remark == null)
            {
                Header.Remark = "";
            }
            //Asset
            //CSV檔案處理
            List<AssetViewModel> AssetsListFromCSV = new List<AssetViewModel>();
            if (fileImport != null && fileImport.ContentLength > 0)
            {
                if (fileImport.FileName.EndsWith(".csv"))
                {
                    Stream stream = fileImport.InputStream;
                    using (StreamReader reader = new StreamReader(stream, Encoding.Default))
                    {
                        int index = 0;
                        while (!reader.EndOfStream)
                        {
                            string line = reader.ReadLine();
                            string[] values = line.Split(',');

                            if (index > 0)
                            {
                                AssetViewModel Asset = new AssetViewModel();

                                Asset.type1 = values[0];
                                Asset.type2 = values[1];
                                Asset.type3 = values[2];
                                Asset.Asset_Category_Code = values[0] + "." + values[1] + "." + values[2];
                                Asset.Assets_Category_ID = values[3];
                                Asset.Assigned_ID = values[4];
                                Asset.Assets_Name = values[5];
                                Decimal Cost;
                                if (Decimal.TryParse(values[6], out Cost))
                                {
                                    Asset.Assets_Fixed_Cost = Cost;
                                }
                                int Unit;
                                if (Int32.TryParse(values[8], out Unit))
                                {
                                    Asset.Assets_Unit = Unit;
                                }
                                Asset.Assigned_Branch = values[10];
                                Asset.Model_Number = values[11];
                                Asset.PO_Number = values[12];
                                Asset.Description = values[13];
                                DateTime PostDate;
                                if (DateTime.TryParse(values[14], out PostDate))
                                {
                                    Asset.Post_Date = PostDate;
                                }
                                DateTime TransactionDate;
                                if (DateTime.TryParse(values[17], out TransactionDate))
                                {
                                    Asset.Transaction_Date = TransactionDate;
                                }
                                Asset.Created_By = this.UserInfo.ID.ToString();
                                Asset.Create_Time = DateTime.Now;
                                Asset.Last_Updated_By = "";
                                Asset.Assets_Parent_Number = "";
                                Asset.IsOversea = "";

                                AssetsListFromCSV.Add(Asset);
                            }
                            index++;
                        }
                    }
                }
            }

            var MP = new MPModel()
            {
                Header = MPHelper.ToMPHeaderModel(Header),
                Assets = AssetsListFromCSV.Select(o => MPHelper.ToAssetModel(o))
            };

            var Result = this.LeaseRightsServant.Create_Asset(MP);
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }

        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public string GetAssetNumber(string MainKind, string DetailKind)
        {
            return this.LeaseRightsServant.CreateAssetNumber(MainKind, DetailKind, "");
        }

        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public bool DeleteAsset(string id)
        {
            return this.LeaseRightsServant.DeleteAsset(Convert.ToInt32(id));
        }


        #region== 資產分類代碼 20170830 先放這 ==
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public JsonResult GetAssetMain_Kind(string Type)
        {
            var vm = this.LeaseRightsServant.GetAssetMain_Kind(Type);
            return Json(vm);
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public JsonResult GetAssetDetail_Kind(string MainKind)
        {
            var vm = this.LeaseRightsServant.GetAssetDetail_Kind(MainKind);
            return Json(vm);
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public JsonResult GetAssetUse_Types()
        {
            var vm = this.LeaseRightsServant.GetAssetUse_Types();
            return Json(vm);
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public JsonResult GetKeep_Position(string Target)
        {
            var vm = this.LeaseRightsServant.GetKeep_Position(Target);
            return Json(vm);
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public JsonResult GetUser(string Branch, string Dept)
        {
            var vm = this.LeaseRightsServant.GetUser(Branch, Dept);
            return Json(vm);
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public string GetAccountItem(string AssetDetailKind)
        {
            string value = this.LeaseRightsServant.GetAccountItem(AssetDetailKind);
            return value;
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public JsonResult GetCodeItem(string CodeClass)
        {
            var vm = this.LeaseRightsServant.GetCodeItem(CodeClass);
            return Json(vm);
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public JsonResult SearchAssetsByCategoryCode(string CategoryCode)
        {
            var vm = this.LeaseRightsServant.SearchAssetsByCategoryCode(CategoryCode);
            return Json(vm);
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsAdded")]
        [ExceptionHandle]
        public JsonResult GetAssetByAssetNumber(string AssetNumber)
        {
            var vm = this.LeaseRightsServant.GetAssetDetailByAssetNumber(AssetNumber);
            return Json(MPHelper.ToAssetViewModel(vm));
        }
        #endregion
    }
}