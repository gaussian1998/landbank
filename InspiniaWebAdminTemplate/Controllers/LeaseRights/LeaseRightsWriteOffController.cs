﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.LeaseRights
{
    public class LeaseRightsWriteOffController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsWriteOff")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsWriteOff")]
        [ExceptionHandle]
        public ActionResult Detail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsWriteOff")]
        [ExceptionHandle]
        public ActionResult Edit()
        {
            return View();
        }
    }
}