﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.LeaseRights
{
    public class LeaseRightsReportController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsReport")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }
    }
}