﻿using Library.Servant.Servant.MP;
using Library.Servant.Servant.MP.MPModels;
using InspiniaWebAdminTemplate.Models.MP;
using Library.Servant.Servants.AbstractFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.UserInformation.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.LeaseRights;

namespace InspiniaWebAdminTemplate.Controllers.LeaseRights
{
    public class LeaseRightsSearchController : Controller
    {
        private ILeaseRightsServant LeaseRightsServant = ServantAbstractFactory.LeaseRights();
        int PageSize = 10;

        private UserDetail UserInfo
        {
            get
            {
                var u = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
                return (u != null) ? this.LeaseRightsServant.GetUserInfo(u.ID) : new UserDetail();
            }
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsSearch")]
        [ExceptionHandle]
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            SearchModel condition = new SearchModel()
            {
                Page = 1,
                PageSize = PageSize
            };
            IndexModel IndexQuery = this.LeaseRightsServant.Index(condition);

            IndexViewModel vm = new IndexViewModel()
            {
                condition = MPHelper.ToSearchViewModel(IndexQuery.condition),
                Items = IndexQuery.Items.Select(o => MPHelper.ToMPHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(MPHelper.GetMPView("Index"), vm);
        }


        [Login]
        [FunctionAuthorize(ID = "LeaseRightsSearch")]
        [ExceptionHandle]
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexQuery(SearchViewModel condition)
        {
            condition.Page = 1;
            condition.PageSize = PageSize;
            IndexModel IndexQuery = this.LeaseRightsServant.Index(MPHelper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => MPHelper.ToMPHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(MPHelper.GetMPView("Index"), vm);
        }

        /// <summary>
        /// 分頁Action
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsSearch")]
        [ExceptionHandle]
        public ActionResult UpdatePage(int page)
        {
            SearchViewModel condition = new SearchViewModel() { Page = page, PageSize = PageSize };
            IndexModel IndexQuery = this.LeaseRightsServant.Index(MPHelper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => MPHelper.ToMPHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(MPHelper.GetMPView("Index"), vm);
        }


        /// <summary>
        /// 編輯單據畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsSearch")]
        [ExceptionHandle]
        public ActionResult Edit(string id)
        {
            var Detail = this.LeaseRightsServant.GetDetail(id);

            var vm = new MPViewModel()
            {
                Header = MPHelper.ToMPHeaderViewModel(Detail.Header),
                Assets = Detail.Assets.Select(m => MPHelper.ToAssetViewModel(m)),
                User = this.UserInfo
            };

            string json = JsonConvert.SerializeObject(vm);

            return View(MPHelper.GetMPView("MP"), vm);
        }

        /// <summary>
        /// 處理單據頭
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsSearch")]
        [ExceptionHandle]
        public ActionResult HandleHeader(MPHeaderViewModel header)
        {
            header.Remark = (header.Remark == null) ? "" : header.Remark;
            header.Created_By = this.UserInfo.ID.ToString();
            header.Create_Time = DateTime.Now;
            header.Last_Updated_By = this.UserInfo.ID.ToString();
            TempData["message"] = "單據儲存成功!";
            return RedirectToAction("Edit", new { id = this.LeaseRightsServant.HandleHeader(MPHelper.ToMPHeaderModel(header)).TRX_Header_ID });
        }

        /// <summary>
        /// 新增資產畫面
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsSearch")]
        [ExceptionHandle]
        public ActionResult Asset_New(MPHeaderViewModel header)
        {
            ViewBag.FunctionFrom = (TempData["FunctionFrom"] != null) ? Convert.ToString(TempData["FunctionFrom"]) : "";
            TempData["Header"] = header;
            ViewBag.Title = "新增資產";
            AssetViewModel vm = new AssetViewModel()
            {
                IsOversea = (MPHelper.GetDefaultBookType(header.Book_Type) != "NTD_MP_IFRS") ? "Y" : "",
                Asset_Category_Code = "..B",
                Location_Disp = header.Office_Branch,
                Assets_Unit = 1,
                Assigned_Branch = header.Office_Branch,
                Currency = MPHelper.GetDefaultCurrency(header.Book_Type)
            };
            return View(MPHelper.GetMPView("Asset"), vm);
        }

        /// <summary>
        /// 新增資產
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsSearch")]
        [ExceptionHandle]
        public ActionResult Asset_Create(AssetViewModel asset)
        {
            MPHeaderViewModel Header = null;
            if (TempData["Header"] != null)
            {
                Header = TempData["Header"] as MPHeaderViewModel;
                Header.Created_By = this.UserInfo.ID.ToString();
                Header.Create_Time = DateTime.Now;
                Header.Last_Updated_By = "";
            }
            if (Header != null && Header.Remark == null)
            {
                Header.Remark = "";
            }
            if (string.IsNullOrEmpty(asset.Description))
            {
                asset.Description = "";
            }
            asset.Created_By = this.UserInfo.ID.ToString();
            asset.Create_Time = DateTime.Now;
            asset.Last_Updated_By = "";
            if (asset.Assets_Parent_Number == null)
            {
                asset.Assets_Parent_Number = "";
            }
            if (asset.IsOversea == null)
            {
                asset.IsOversea = "";
            }

            var MP = new MPModel()
            {
                Assets = new List<AssetViewModel>() { asset }.Select(o => MPHelper.ToAssetModel(o))
            };
            if (Header != null)
            {
                MP.Header = MPHelper.ToMPHeaderModel(Header);
            };


            var Result = this.LeaseRightsServant.Create_Asset(MP);

            TempData["message"] = "資產新增成功!";
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }

        /// <summary>
        /// 編輯資產畫面
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsSearch")]
        [ExceptionHandle]
        public ActionResult Asset_Edit(int id, string Transaction_Type)
        {
            var vm = this.LeaseRightsServant.GetAssetDetail(id);
            TempData["Header"] = MPHelper.ToMPHeaderViewModel(this.LeaseRightsServant.GetHeader(vm.Asset_Number, Transaction_Type));
            TempData["DeprnRecord"] = this.LeaseRightsServant.GetDeprnRecord(vm.Asset_Number);
            ViewBag.Title = "編輯資產";
            ViewBag.FunctionFrom = (TempData["FunctionFrom"] != null) ? Convert.ToString(TempData["FunctionFrom"]) : "";
            return View(MPHelper.GetMPView("Asset"), MPHelper.ToAssetViewModel(vm));
        }

        /// <summary>
        /// 編輯資產
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsSearch")]
        [ExceptionHandle]
        public ActionResult Asset_Update(AssetViewModel asset)
        {
            MPHeaderViewModel Header = TempData["Header"] as MPHeaderViewModel;
            if (Header.Remark == null)
            {
                Header.Remark = "";
            }
            if (asset.Assets_Parent_Number == null)
            {
                asset.Assets_Parent_Number = "";
            }
            if (asset.IsOversea == null)
            {
                asset.IsOversea = "";
            }
            if (string.IsNullOrEmpty(asset.Description))
            {
                asset.Description = "";
            }
            asset.Last_Updated_By = this.UserInfo.ID.ToString();
            var Result = this.LeaseRightsServant.Update_Asset(new MPModel()
            {
                Header = MPHelper.ToMPHeaderModel(Header),
                Assets = new List<AssetViewModel>() { asset }.Select(o => MPHelper.ToAssetModel(o))
            });
            TempData["message"] = "資產編輯成功!";
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }
    }
}