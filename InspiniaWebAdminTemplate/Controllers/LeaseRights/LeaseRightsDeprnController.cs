﻿using Library.Servant.Servant.MP;
using Library.Servant.Servants.AbstractFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Servant.MP.MPModels;
using InspiniaWebAdminTemplate.Models.MP;
using Newtonsoft.Json;
using Library.Servant.Servant.UserInformation.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.ApprovalFlow;
using Library.Servant.Servant.ApprovalTodo;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Repository.MP;
using Library.Servant.Servant.Email;
using Library.Servant.Servant.Email.Models;
using Library.Servant.Servant.LeaseRights;
using Library.Servant.Repository.LeaseRights;

namespace InspiniaWebAdminTemplate.Controllers.LeaseRights
{
    public class LeaseRightsDeprnController : Controller
    {
        private ILeaseRightsServant LeaseRightsServant = ServantAbstractFactory.LeaseRights();
        private string Type = LeaseRightsGeneral.GetFormCode("LeaseRightsDeprn");
        int PageSize = 10;

        private UserDetail UserInfo
        {
            get
            {
                var u = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
                return (u != null) ? this.LeaseRightsServant.GetUserInfo(u.ID) : new UserDetail();
            }
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsDeprn")]
        [ExceptionHandle]
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            SearchModel condition = new SearchModel()
            {
                Transaction_Type = Type,
                Page = 1,
                PageSize = PageSize
            };
            IndexModel IndexQuery = this.LeaseRightsServant.Index(condition);

            IndexViewModel vm = new IndexViewModel()
            {
                condition = MPHelper.ToSearchViewModel(IndexQuery.condition),
                Items = IndexQuery.Items.Select(o => MPHelper.ToMPHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(MPHelper.GetMPView("Index"), vm);
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsDeprn")]
        [ExceptionHandle]
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexQuery(SearchViewModel condition)
        {
            condition.Transaction_Type = Type;
            condition.Page = 1;
            condition.PageSize = PageSize;
            IndexModel IndexQuery = this.LeaseRightsServant.Index(MPHelper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => MPHelper.ToMPHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(MPHelper.GetMPView("Index"), vm);
        }

        /// <summary>
        /// 分頁Action
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "MPAdded")]
        [ExceptionHandle]
        public ActionResult UpdatePage(int page)
        {
            SearchViewModel condition = new SearchViewModel() { Transaction_Type = Type, Page = page, PageSize = PageSize };
            IndexModel IndexQuery = this.LeaseRightsServant.Index(MPHelper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => MPHelper.ToMPHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(MPHelper.GetMPView("Index"), vm);
        }

        /// <summary>
        /// 新增單據
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsDeprn")]
        [ExceptionHandle]
        public ActionResult New()
        {
            ViewBag.TRXID = this.LeaseRightsServant.CreateTRXID(Type);
            MPHeaderViewModel Header = new MPHeaderViewModel()
            {
                Transaction_TypeName = "租賃權益攤銷單",
                Flow_Status = "0",
                Flow_StatusName = "新增",
                Office_Branch = this.UserInfo.BranchCode + "." + this.UserInfo.DepartmentCode,
                Source = this.UserInfo.BranchCode == "001" ? "H" : "B",
                Book_Type = MPHelper.GetDefaultBookType(this.UserInfo.BranchCode)
            };
            MPViewModel vm = new MPViewModel()
            {
                Header = Header,
                User = this.UserInfo,
                Assets = new List<AssetViewModel>()
            };
            return View(MPHelper.GetMPView("MP"), vm);
        }

        /// <summary>
        /// 編輯單據畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsDeprn")]
        [ExceptionHandle]
        public ActionResult Edit(string id, string signmode)
        {
            var Detail = this.LeaseRightsServant.GetDetail(id);

            var vm = new MPViewModel()
            {
                Header = MPHelper.ToMPHeaderViewModel(Detail.Header),
                Assets = Detail.Assets.Select(m => MPHelper.ToAssetViewModel(m)),
                User = this.UserInfo
            };

            if (!string.IsNullOrEmpty(signmode))
            {
                ViewBag.SignMode = true;
            }

            string json = JsonConvert.SerializeObject(vm);

            return View(MPHelper.GetMPView("MP"), vm);
        }

        /// <summary>
        /// 處理單據頭
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsDeprn")]
        [ExceptionHandle]
        public ActionResult HandleHeader(MPHeaderViewModel header)
        {
            header.Transaction_Type = Type;
            header.Remark = (header.Remark == null) ? "" : header.Remark;
            header.Created_By = this.UserInfo.ID.ToString();
            header.Create_Time = DateTime.Now;
            header.Last_Updated_By = this.UserInfo.ID.ToString();
            TempData["message"] = "單據儲存成功!";
            return RedirectToAction("Edit", new { id = this.LeaseRightsServant.HandleHeader(MPHelper.ToMPHeaderModel(header)).TRX_Header_ID });
        }

        /// <summary>
        /// 資產搜尋頁面
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsDeprn")]
        [ExceptionHandle]
        public ActionResult Asset_Search(MPHeaderViewModel header)
        {
            TempData["Header"] = header;
            header.Transaction_Type = Type;
            return View(MPHelper.GetMPView("AssetSearch"));
        }

        /// <summary>
        /// 資產搜尋
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsDeprn")]
        [ExceptionHandle]
        public ActionResult Asset_Query(AssetsSearchViewModel condition)
        {
            var data = this.LeaseRightsServant.SearchAssets(MPHelper.ToAssetsSearchModel(condition));
            AssetsIndexViewModel vm = new AssetsIndexViewModel()
            {
                Items = data.Items.Select(o => MPHelper.ToAssetViewModel(o)),
                condition = condition,
                TotalAmount = data.TotalAmount
            };
            var Header = (TempData["Header"] != null) ? TempData["Header"] : null;
            TempData["Header"] = Header;
            return View(MPHelper.GetMPView("AssetSearch"), vm);
        }

        /// <summary>
        /// 將資產新增至表單
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsDeprn")]
        [ExceptionHandle]
        public ActionResult AssetsToMP(string AssetsList)
        {
            MPHeaderViewModel Header = null;
            if (TempData["Header"] != null)
            {
                Header = TempData["Header"] as MPHeaderViewModel;
                Header.Remark = (Header.Remark == null) ? "" : Header.Remark;
                Header.Created_By = this.UserInfo.ID.ToString();
                Header.Create_Time = DateTime.Now;
                Header.Last_Updated_By = "";
            }

            MPModel MP = new MPModel()
            {
                Header = MPHelper.ToMPHeaderModel(Header),
                Assets = AssetsList.Split(',').Select(o => MPHelper.ToAssetModel(new AssetViewModel()
                {
                    Asset_Number = o
                }))
            };

            var Result = this.LeaseRightsServant.CreateMP(MP);
            TempData["message"] = "資產導入成功!";
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }


        /// <summary>
        /// 編輯資產畫面
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsDeprn")]
        [ExceptionHandle]
        [HttpGet]
        public ActionResult Asset_Edit(int id)
        {
            var vm = this.LeaseRightsServant.GetAssetDetail(id);
            TempData["Header"] = MPHelper.ToMPHeaderViewModel(this.LeaseRightsServant.GetHeader(vm.Asset_Number, Type));
            ViewBag.Title = "編輯資產";
            ViewBag.FunctionFrom = (TempData["FunctionFrom"] != null) ? Convert.ToString(TempData["FunctionFrom"]) : "";
            return View(MPHelper.GetMPView("Asset"), MPHelper.ToAssetViewModel(vm));
        }

        /// <summary>
        /// 編輯資產
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsDeprn")]
        [ExceptionHandle]
        public ActionResult Asset_Update(AssetViewModel asset)
        {
            MPHeaderViewModel Header = TempData["Header"] as MPHeaderViewModel;
            if (Header.Remark == null)
            {
                Header.Remark = "";
            }
            if (asset.Assets_Parent_Number == null)
            {
                asset.Assets_Parent_Number = "";
            }
            if (asset.IsOversea == null)
            {
                asset.IsOversea = "";
            }
            if (string.IsNullOrEmpty(asset.Description))
            {
                asset.Description = "";
            }
            asset.Last_Updated_By = this.UserInfo.ID.ToString();
            var Result = this.LeaseRightsServant.Update_Asset(new MPModel()
            {
                Header = MPHelper.ToMPHeaderModel(Header),
                Assets = new List<AssetViewModel>() { asset }.Select(o => MPHelper.ToAssetModel(o))
            });
            TempData["message"] = "資產編輯成功!";
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }


        /// <summary>
        /// 跑簽核流程
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "LeaseRightsDeprn")]
        [ExceptionHandle]
        public ActionResult CreateFlow(MPHeaderViewModel header)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            int DocID;
            string message = "提交成功!";
            if (LeaseRightsServant.GetDocID((header.Transaction_Type + header.Source), out DocID))
            {
                this.LeaseRightsServant.UpdateFlowStatus(header.TRX_Header_ID, "1");
                LocalApprovalTodoServant.Create(user.ID, DocID, header.TRX_Header_ID, "LeaseRightsDeprn");
                LocalApprovalTodoServant.Start(user.ID, header.TRX_Header_ID);
            }
            else
            {
                message = "查無此表單!!";
            }
            TempData["message"] = message;
            return RedirectToAction("Edit", new { id = header.TRX_Header_ID });
        }

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsDeprn")]
        [ExceptionHandle]
        public ActionResult MPNotice(string id)
        {
            var Detail = this.LeaseRightsServant.GetDetail(id);

            var vm = new MPViewModel()
            {
                Header = MPHelper.ToMPHeaderViewModel(Detail.Header),
                Assets = Detail.Assets.Select(m => MPHelper.ToAssetViewModel(m))
            };
            return View(MPHelper.GetMPView("MPNotice"), vm);
        }

        private readonly IEmailServant Servant = ServantAbstractFactory.Email();

        [Login]
        [FunctionAuthorize(ID = "LeaseRightsDeprn")]
        [ExceptionHandle]
        public JsonResult MPSendNotice()
        {
            //int sender = AS_Users_Records.First(user => user.User_Name == "王興測").ID;
            //int recver = AS_Users_Records.First(user => user.User_Name == "張家測").ID;

            var result = Servant.Send(new EmailModel
            {
                UserID = 5322,
                RecverID = 2784,
                Title = "台北分行 – 保管人通知信 (動產新增) - 測試信件",
                Body = @"李寧,您好：<br/>
                           本分行採購車輛與設備，如附件。<br/>

                           請您查閱，本次動產資料中有您保管的物品。<br/>

                           如保管人資訊確認無誤，請點擊 [簽收] 。謝謝。<br/>"
            });
            return Json(new { msg = "發送成功!" });
        }
    }
}