﻿using InspiniaWebAdminTemplate.Attributes;
using System.Web.Mvc;
using Library.Servant.Servant.PostsManagement;
using Library.Servant.Servant.ApprovalTodo;
using System.Collections.Generic;
using System.Linq;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.AccountingBook.Models;
using Library.Servant.Servant.TodoListManagement;

namespace InspiniaWebAdminTemplate.Controllers
{
    [Login]
    [LandBankAuthorize]
    [ExceptionHandle]
    public class HomeController : Controller
    {
        private IEnumerable<SelectListItem> _announceTypeList
        {
            get {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text="一般公告", Value="0" },
                    new SelectListItem { Text="最新資訊", Value="1" },
                    new SelectListItem { Text="法規新訊", Value="2" }
                };
            }
        }

        public ActionResult Index()
        {
            var id = this.UserID();
            var AnnounceVM = LocalPostsManagementServant.GetAnnouncements(id);

            foreach (var item in AnnounceVM.Items) { //todo_julius 亂寫的，到時候要從code table來
                item.Type = _announceTypeList.First(m => m.Value == item.Type).Text;
            }

            var search = new Library.Servant.Servant.ApprovalTodo.Models.SearchModel();
            search.UserID = id;
            var approvalVM = ApprovalTodoServant.IndexApproval(search);

            var todoVM = LocalTodoListManagement.GetTodoList(id);

            ViewBag.Announcement = AnnounceVM;
            ViewBag.Approval = approvalVM;
            ViewBag.Todo = todoVM;

            return View();
        }

        private IApprovalTodoServant ApprovalTodoServant = new LocalApprovalTodoServant();
    }
}