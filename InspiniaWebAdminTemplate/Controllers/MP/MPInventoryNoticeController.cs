﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Servants.Search;
using InspiniaWebAdminTemplate.Models.MP;
using Library.Servant.Servant.MP;
using Library.Servant.Servant.MP.MPModels;
using Newtonsoft.Json;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.ApprovalFlow;
using Library.Servant.Servant.UserInformation.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.Authentication.Models;
using System.IO;
using System.Text;
using Library.Servant.Repository.MP;
using Library.Servant.Servant.Email;
using Library.Servant.Servant.Email.Models;

namespace InspiniaWebAdminTemplate.Controllers.MP
{
    public class MPInventoryNoticeController : Controller
    {
        private IMPServant MPServant = ServantAbstractFactory.MP();
        private string Type = MPGeneral.GetFormCode("MPInventoryNotice");
        int PageSize = 10;

        private UserDetail UserInfo
        {
            get
            {
                var u = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
                return (u != null) ? this.MPServant.GetUserInfo(u.ID) : new UserDetail();
            }
        }

        [Login]
        [FunctionAuthorize(ID = "MPInventoryNotice")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            SearchModel condition = new SearchModel()
            {
                Transaction_Type = Type,
                Page = 1,
                PageSize = PageSize
            };
            IndexModel IndexQuery = this.MPServant.Index(condition);

            IndexViewModel vm = new IndexViewModel()
            {
                condition = MPHelper.ToSearchViewModel(IndexQuery.condition),
                Items = IndexQuery.Items.Select(o => MPHelper.ToMPHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(MPHelper.GetMPView("Index"), vm);
        }

        [Login]
        [FunctionAuthorize(ID = "MPInventoryNotice")]
        [ExceptionHandle]
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexQuery(SearchViewModel condition)
        {
            condition.Transaction_Type = Type;
            condition.Page = 1;
            condition.PageSize = PageSize;
            IndexModel IndexQuery = this.MPServant.Index(MPHelper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => MPHelper.ToMPHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(MPHelper.GetMPView("Index"), vm);
        }

        /// <summary>
        /// 分頁Action
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "MPInventoryNotice")]
        [ExceptionHandle]
        public ActionResult UpdatePage(int page)
        {
            SearchViewModel condition = new SearchViewModel() { Transaction_Type = Type, Page = page, PageSize = PageSize };
            IndexModel IndexQuery = this.MPServant.Index(MPHelper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => MPHelper.ToMPHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(MPHelper.GetMPView("Index"), vm);
        }

        /// <summary>
        /// 新增單據
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "MPInventoryNotice")]
        [ExceptionHandle]
        [HttpGet]
        public ActionResult New()
        {
            ViewBag.TRXID = this.MPServant.CreateTRXID(Type);
            MPHeaderViewModel Header = new MPHeaderViewModel()
            {
                Transaction_TypeName = "動產盤點通知單",
                Flow_Status = "0",
                Flow_StatusName = "新增",
                Office_Branch = this.UserInfo.BranchCode + "." + this.UserInfo.DepartmentCode,
                Source = this.UserInfo.BranchCode == "001" ? "H" : "B",
                Book_Type = MPHelper.GetDefaultBookType(this.UserInfo.BranchCode)
            };
            MPViewModel vm = new MPViewModel()
            {
                Header = Header,
                User = this.UserInfo,
                Assets = new List<AssetViewModel>()
            };
            return View(MPHelper.GetMPView("MP"), vm);
        }

        /// <summary>
        /// 處理單據頭
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "MPInventoryNotice")]
        [ExceptionHandle]
        public ActionResult HandleHeader(MPHeaderViewModel header)
        {
            header.Transaction_Type = Type;
            header.Remark = (header.Remark == null) ? "" : header.Remark;
            header.Created_By = this.UserInfo.ID.ToString();
            header.Create_Time = DateTime.Now;
            header.Last_Updated_By = this.UserInfo.ID.ToString();
            TempData["message"] = "單據儲存成功!";
            return RedirectToAction("Edit", new { id = this.MPServant.HandleHeader(MPHelper.ToMPHeaderModel(header)).TRX_Header_ID });
        }

        /// <summary>
        /// 編輯單據畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "MPInventoryNotice")]
        [ExceptionHandle]
        public ActionResult Edit(string id, string signmode)
        {
            var Detail = this.MPServant.GetDetail(id);

            var vm = new MPViewModel()
            {
                Header = MPHelper.ToMPHeaderViewModel(Detail.Header),
                Assets = Detail.Assets.Select(m => MPHelper.ToAssetViewModel(m)),
                User = this.UserInfo
            };

            if (!string.IsNullOrEmpty(signmode))
            {
                ViewBag.SignMode = true;
            }

            string json = JsonConvert.SerializeObject(vm);

            return View(MPHelper.GetMPView("MP"), vm);
        }


        /// <summary>
        /// 跑簽核流程
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "MPInventoryNotice")]
        [ExceptionHandle]
        public ActionResult CreateFlow(MPHeaderViewModel header)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            int DocID;
            string message = "提交成功!";
            if (MPServant.GetDocID(header.Transaction_Type + header.Source, out DocID))
            {
                this.MPServant.UpdateFlowStatus(header.TRX_Header_ID, "1");
                LocalApprovalTodoServant.Create(user.ID, DocID, header.TRX_Header_ID, "MPInventoryNotice");
                LocalApprovalTodoServant.Start(user.ID, header.TRX_Header_ID, header.TRX_Header_ID);
            }
            else
            {
                message = "查無此表單!!";
            }
            //直接測試核准
            MPServant.MPInventoryNotice(header.TRX_Header_ID);

            TempData["message"] = message;
            return RedirectToAction("Edit", new { id = header.TRX_Header_ID });
        }

        /// <summary>
        /// 刪除單據
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "MPInventoryNotice")]
        [ExceptionHandle]
        public bool DeleteForm(string TRX_Header_ID)
        {
            return this.MPServant.DeleteForm(TRX_Header_ID);
        }


        [Login]
        [FunctionAuthorize(ID = "MPInventoryNotice")]
        [ExceptionHandle]
        public ActionResult MPNotice(string id)
        {
            var Detail = this.MPServant.GetDetail(id);

            var vm = new MPViewModel()
            {
                Header = MPHelper.ToMPHeaderViewModel(Detail.Header),
                Assets = Detail.Assets.Select(m => MPHelper.ToAssetViewModel(m))
            };
            return View(MPHelper.GetMPView("MPNotice"), vm);
        }

        private readonly IEmailServant Servant = ServantAbstractFactory.Email();

        [Login]
        [FunctionAuthorize(ID = "MPInventoryNotice")]
        [ExceptionHandle]
        public JsonResult MPSendNotice()
        {
            //int sender = AS_Users_Records.First(user => user.User_Name == "王興測").ID;
            //int recver = AS_Users_Records.First(user => user.User_Name == "張家測").ID;

            var result = Servant.Send(new EmailModel
            {
                UserID = 5322,
                RecverID = 2784,
                Title = "台北分行 – 保管人通知信 (動產新增) - 測試信件",
                Body = @"李寧,您好：<br/>
                           本分行採購車輛與設備，如附件。<br/>

                           請您查閱，本次動產資料中有您保管的物品。<br/>

                           如保管人資訊確認無誤，請點擊 [簽收] 。謝謝。<br/>"
            });
            return Json(new { msg = "發送成功!" });
        }
    }
}