﻿using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.MP;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Repository.MP;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.MP;
using Library.Servant.Servant.MP.MPModels;
using Library.Servant.Servant.UserInformation.Models;
using Library.Servant.Servants.AbstractFactory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.MP
{
    public class MPImportReviewController : Controller
    {
        private IMPServant MPServant = ServantAbstractFactory.MP();
        private string Type = MPGeneral.GetFormCode("MPImportReview");

        [Login]
        [FunctionAuthorize(ID = "MPImportReview")]
        [ExceptionHandle]
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var index = this.MPServant.ImportReviewIndex();
            var vm = new ImportReviewIndexViewModel()
            {
                Items = index.Items.Select(o => new MPImportReviewViewModel()
                {
                    IRID = o.IRID
                }),
                TotalAmount = index.TotalAmount
            };
            return View(MPHelper.GetMPView("ImportIndex"),vm);
        }

        /// <summary>
        /// 新增單據
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "MPImportReview")]
        [ExceptionHandle]
        public ActionResult New()
        {
            MPImportReviewViewModel vm = new MPImportReviewViewModel();
            vm.Imports = new List<MPImportViewModel>();
            vm.MPs = new List<MPViewModel>();
            return View(MPHelper.GetMPView("MPImport"), vm);
        }

        /// <summary>
        /// 批號搜尋頁面
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "MPImportReview")]
        [ExceptionHandle]
        public ActionResult Import_Search(string IRID,MPHeaderViewModel header)
        {
            TempData["Header"] = header;
            TempData["IRID"] = IRID;
            header.Transaction_Type = Type;
            ImportIndexViewModel vm = new ImportIndexViewModel();
            vm.Items = new List<MPImportViewModel>();
            return View(MPHelper.GetMPView("ImportSearch"), vm);
        }

        /// <summary>
        /// 批號搜尋
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "MPImportReview")]
        [ExceptionHandle]
        public ActionResult Import_Query(ImportSearchViewModel condition)
        {
            condition.LastRecord = true;
            ImportIndexModel importIndex = this.MPServant.ImportIndex(MPHelper.ToImportSearchModel(condition));
            ImportIndexViewModel vm = new ImportIndexViewModel()
            {
               condition = condition,
               Items = importIndex.Items.Select(o=>MPHelper.ToMPImportViewModel(o)),
               TotalAmount = importIndex.TotalAmount
            };
            return View(MPHelper.GetMPView("ImportSearch"), vm);
        }

        /// <summary>
        /// 將資產新增至表單
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "MPImportReview")]
        [ExceptionHandle]
        public ActionResult ImportToMP(string ImportList)
        {
            MPHeaderViewModel Header = null;
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);

            if (TempData["Header"] != null)
            {
                Header = TempData["Header"] as MPHeaderViewModel;
                Header.Remark = (Header.Remark == null) ? "" : Header.Remark;
                Header.Created_By = user.ID.ToString();
                Header.Create_Time = DateTime.Now;
                Header.Flow_Status = "0";
                Header.Last_Updated_By = "";
            }

            string IRID = (TempData["IRID"] != null) ? TempData["IRID"].ToString() : "";

            AssetHandleResult Result = this.MPServant.CreateImportReview(IRID , ImportList.Split(','), MPHelper.ToMPHeaderModel(Header));
            TempData["message"] = "資產導入成功!";
            return RedirectToAction("Edit", new { id = Result.ImportReviewID });
        }

        /// <summary>
        /// 編輯單據畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "MPImportReview")]
        [ExceptionHandle]
        public ActionResult Edit(string id)
        {
            var ImportReviewModel = this.MPServant.GetImportReviewDetail(id);

            MPImportReviewViewModel vm = new MPImportReviewViewModel()
            {
                IRID = id,
                Imports = ImportReviewModel.Imports.Select(o => MPHelper.ToMPImportViewModel(o)),
                MPs = ImportReviewModel.MPs.Select(o=>new MPViewModel()
                {
                     Assets = o.Assets.Select(t=>MPHelper.ToAssetViewModel(t)),
                     Assigned_Branch = o.Assigned_Branch,
                     Email = o.Email,
                     HandledBy = o.HandledBy,
                     Header = MPHelper.ToMPHeaderViewModel(o.Header)
                })
            };
            return View(MPHelper.GetMPView("MPImport"), vm);
        }

        [Login]
        [FunctionAuthorize(ID = "MPImportReview")]
        [ExceptionHandle]
        public ActionResult ViewMPImportMPList(string id)
        {
            var ImportReviewModel = this.MPServant.GetImportReviewDetail(id);

            IndexViewModel vm = new IndexViewModel()
            {
                 Items = ImportReviewModel.MPs.Select(o=>o.Header).Select(o=>MPHelper.ToMPHeaderViewModel(o)),
                 TotalAmount = ImportReviewModel.MPs.Count()
            };

            return View(MPHelper.GetMPView("MPImportMPList"),vm);
        }

        /// <summary>
        /// 編輯單據畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "MPImportReview")]
        [ExceptionHandle]
        public ActionResult EditMP(string id)
        {
            var Detail = this.MPServant.GetDetail(id);

            var vm = new MPViewModel()
            {
                Header = MPHelper.ToMPHeaderViewModel(Detail.Header),
                Assets = Detail.Assets.Select(m => MPHelper.ToAssetViewModel(m))
            };

            string json = JsonConvert.SerializeObject(vm);

            return View(MPHelper.GetMPView("MP"), vm);
        }

        public ActionResult ViewMPImportAssetsList(int ImportID)
        {
            var Index = this.MPServant.GetImportAssets(ImportID);
            AssetsIndexViewModel vm = new AssetsIndexViewModel()
            {
                 TotalAmount = Index.TotalAmount,
                 Items = Index.Items.Select(o=>MPHelper.ToAssetViewModel(o))
            };
            return View(MPHelper.GetMPView("MPImportAssetsList"),vm);
        }

        public ActionResult ViewImportAsset(int id)
        {
            var vm = this.MPServant.GetImportAssetDetail(id);
            ViewBag.Title = "檢視資產";
            return View(MPHelper.GetMPView("Asset"), MPHelper.ToAssetViewModel(vm));
        }

        /// <summary>
        /// 編輯資產畫面
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Asset_Edit(int id)
        {
            var vm = this.MPServant.GetAssetDetail(id);
            TempData["Header"] = MPHelper.ToMPHeaderViewModel(this.MPServant.GetHeader(vm.Asset_Number, Type));
            ViewBag.Title = "編輯資產";
            ViewBag.FunctionFrom = (TempData["FunctionFrom"] != null) ? Convert.ToString(TempData["FunctionFrom"]) : "";
            return View(MPHelper.GetMPView("Asset"), MPHelper.ToAssetViewModel(vm));
        }

        /// <summary>
        /// 編輯資產
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        public ActionResult Asset_Update(AssetViewModel asset)
        {
            MPHeaderViewModel Header = TempData["Header"] as MPHeaderViewModel;
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);

            if (Header.Remark == null)
            {
                Header.Remark = "";
            }
            asset.Date_Placed_In_Service = DateTime.Now;
            asset.Transaction_Date = DateTime.Now;
            if (asset.Assets_Parent_Number == null)
            {
                asset.Assets_Parent_Number = "";
            }
            if (asset.IsOversea == null)
            {
                asset.IsOversea = "";
            }
            asset.Description = "";
            asset.Last_Updated_By = user.ID.ToString();
            var Result = this.MPServant.Update_Asset(new MPModel()
            {
                Header = MPHelper.ToMPHeaderModel(Header),
                Assets = new List<AssetViewModel>() { asset }.Select(o => MPHelper.ToAssetModel(o))
            });
            TempData["message"] = "資產編輯成功!";
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }
    }
}