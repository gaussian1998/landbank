﻿using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.MP;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.MP;
using Library.Servant.Servant.MP.MPModels;
using Library.Servant.Servant.UserInformation.Models;
using Library.Servant.Servants.AbstractFactory;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;


namespace InspiniaWebAdminTemplate.Controllers.MP
{
    public class MPImportController : Controller
    {
        private IMPServant MPServant = ServantAbstractFactory.MP();
        int PageSize = 10;

        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            ImportSearchModel condition = new ImportSearchModel()
            {
                Page = 1,
                PageSize = PageSize
            };
            ImportIndexModel index = this.MPServant.ImportIndex(condition);
            ImportIndexViewModel vm = new ImportIndexViewModel();
            var query = (from i in index.Items
                        join g in
                        (
                           from mr in index.Items
                           group mr by new { mr.Import_Num } into mrg
                           select new
                           {
                               Import_Num = mrg.Key.Import_Num,
                               Create_Time = mrg.Max(o => o.Create_Time)
                           }
                        ) on new { i.Import_Num,i.Create_Time }  equals new { g.Import_Num , g.Create_Time } 
                        select i).OrderByDescending(o=>o.Create_Time).Select(o => MPHelper.ToMPImportViewModel(o));
            //分頁第一頁
            vm.Items = query.OrderByDescending(m => m.Create_Time).Skip((1 - 1) * Convert.ToInt32(PageSize)).Take(Convert.ToInt32(PageSize));
            vm.TotalAmount = query.Count();
            vm.condition = new ImportSearchViewModel()
            {
                 Page = condition.Page,
                 PageSize = condition.PageSize
            };
            
            return View(vm);
        }

        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult QueryIndex(ImportSearchViewModel condition)
        {
            condition.Page = 1;
            condition.PageSize = PageSize;
            ImportIndexModel index = this.MPServant.ImportIndex(MPHelper.ToImportSearchModel(condition));
            ImportIndexViewModel vm = new ImportIndexViewModel();
            var query = (from i in index.Items
                        join g in
                        (
                           from mr in index.Items
                           group mr by new { mr.Import_Num } into mrg
                           select new
                           {
                               Import_Num = mrg.Key.Import_Num,
                               Create_Time = mrg.Max(o => o.Create_Time)
                           }
                        ) on new { i.Import_Num, i.Create_Time } equals new { g.Import_Num, g.Create_Time }
                        select i).OrderByDescending(o => o.Create_Time).Select(o => MPHelper.ToMPImportViewModel(o));
            vm.Items = query;
            //分頁
            if (condition.Page != null)
            {
                //選取該頁之資訊
                vm.Items = query.OrderByDescending(m => m.Create_Time).Skip((condition.Page.Value - 1) * Convert.ToInt32(condition.PageSize)).Take(Convert.ToInt32(condition.PageSize));
            }
            vm.TotalAmount = query.Count();
            

            return View("~/Views/MPImport/Index.cshtml", vm);
        }


        /// <summary>
        /// 分頁Action
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult UpdatePage(int page)
        {
            SearchViewModel condition = new SearchViewModel() { Page = page, PageSize = PageSize };
            IndexModel IndexQuery = this.MPServant.Index(MPHelper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => MPHelper.ToMPHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View("~/Views/MPImport/Index.cshtml", vm);
        }

        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult New()
        {
            MPImportViewModel vm = new MPImportViewModel()
            {
                Import_Num = this.MPServant.CreateImport_Num()
            };
            return View(vm);
        }

        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult Edit(string importNum)
        {
            MPImportViewModel vm = new MPImportViewModel()
            {
                Import_Num = importNum
            };
            return View("New",vm);
        }

        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult Import(MPImportViewModel import,HttpPostedFileBase fileImport)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            //CSV檔案處理
            List<AssetViewModel> AssetsListFromCSV = new List<AssetViewModel>();

            try
            {
                if (fileImport != null && fileImport.ContentLength > 0)
                {
                    if (fileImport.FileName.EndsWith(".csv"))
                    {
                        Stream stream = fileImport.InputStream;
                        using (StreamReader reader = new StreamReader(stream, Encoding.Default))
                        {
                            int index = 0;
                            while (!reader.EndOfStream)
                            {
                                string line = reader.ReadLine();
                                string[] values = line.Split(',');

                                if (index > 0)
                                {
                                    AssetViewModel Asset = new AssetViewModel();

                                    Asset.type1 = values[0];
                                    Asset.type2 = values[1];
                                    Asset.type3 = values[2];
                                    Asset.Asset_Category_Code = values[0] + "." + values[1] + "." + values[2];
                                    Asset.Assets_Category_ID = values[3];
                                    Asset.Assigned_ID = values[4];
                                    Asset.Assets_Name = values[5];
                                    Decimal Cost;
                                    if (Decimal.TryParse(values[6], out Cost))
                                    {
                                        Asset.Assets_Fixed_Cost = Cost;
                                    }
                                    int Unit;
                                    if (Int32.TryParse(values[8], out Unit))
                                    {
                                        Asset.Assets_Unit = Unit;
                                    }
                                    Asset.Assigned_Branch = values[10];
                                    Asset.Model_Number = values[11];
                                    Asset.PO_Number = values[12];
                                    Asset.Description = values[13];
                                    DateTime PostDate;
                                    if (DateTime.TryParse(values[14], out PostDate))
                                    {
                                        Asset.Post_Date = PostDate;
                                    }
                                    DateTime TransactionDate;
                                    if (DateTime.TryParse(values[17], out TransactionDate))
                                    {
                                        Asset.Transaction_Date = TransactionDate;
                                    }
                                    Asset.Created_By = user.ID.ToString();
                                    Asset.Create_Time = DateTime.Now;
                                    AssetsListFromCSV.Add(Asset);
                                }
                                index++;
                            }
                        }
                    }
                }

                import.Status = "1";
            }
            catch(Exception ex)
            {
                import.Status = "2";
            }
            
            import.Create_Time = DateTime.Now;
            import.Created_By = user.ID.ToString();
            var Result = this.MPServant.CreateImport(MPHelper.ToMPImportModel(import),AssetsListFromCSV.Select(o=>MPHelper.ToAssetModel(o)));
            TempData["message"] = (import.Status == "1") ? "匯入成功!" : "匯入失敗!";
            return RedirectToAction("Index", new { id = Result.ImportNum });
        }

        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult Detail(string importNum)
        {
            ImportIndexModel index = this.MPServant.ImportIndex(new ImportSearchModel() { ImportNum = importNum});
            ImportIndexViewModel vm = new ImportIndexViewModel()
            {
                TotalAmount = index.Items.Count(),
                Items = index.Items.Select(o=>MPHelper.ToMPImportViewModel(o))
            };
            return View(vm);
        }

        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult DownLoadCSV(int importID)
        {
            DataTable query = MPHelper.LinqQueryToDataTable(this.MPServant.GetImportData(importID));
            StringBuilder csvContent = new StringBuilder();

            var memoryStream = new MemoryStream();
            var writer = new StreamWriter(memoryStream,System.Text.Encoding.Default);
            for (int r = 0; r < query.Rows.Count; r++)
            {
                for (int c = 1; c < query.Columns.Count; c++)
                {
                    writer.Write(query.Rows[r][c].ToString() + ",");
                    if (c == query.Columns.Count - 1)
                    {
                        writer.WriteLine();
                    }
                }
            }
            writer.Flush();
            memoryStream.Position = 0;
            var fsr = new FileStreamResult(memoryStream, "text/csv");
            fsr.FileDownloadName = "動產匯入資料.csv";
            return fsr;
        }
    }
}