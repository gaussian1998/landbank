﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Servant.Report;
using System.Collections;
using Library.Servant.Servant.MPModels.ReportModel;
using System.Data;
using System.Reflection;
using Library.Servant.Servants.AbstractFactory;
using FastReport.Web;
using InspiniaWebAdminTemplate.Attributes;

namespace InspiniaWebAdminTemplate.Controllers.MP
{
    public class MPReportsController : Controller
    {
        private  IReportServant ReportServant = ServantAbstractFactory.Report();

        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult MPDetails()
        {
            return View();
        }


        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult PrintMPDetails(MPReportSearchModel condition)
        {
            IEnumerable<MPReportModel> query = this.ReportServant.GetMPAssets(condition);
            var webReport = new WebReport();
            webReport.Report.Load(Server.MapPath("~/App_Data/Reports/MPDetails.frx"));
            webReport.RegisterData(query, "MPDetails");
            webReport.Width = 1024;
            ViewBag.WebReport = webReport;
            return View("~/Views/Reports/Index.cshtml");
        }


        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult MPNewList()
        {
            return View();
        }


        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        /// <summary>財產新增報告單-動產</summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public ActionResult PrintMPNewList(MPReportSearchModel condition)
        {
            FileInfo Template = GetReportTemplate("MPNewList");
            MemoryStream output = new MemoryStream();

            IEnumerable<MPReportModel> query = this.ReportServant.GetMPNewList(condition);

            using (ExcelPackage ep = new ExcelPackage(Template, true))
            {
                var worksheet = ep.Workbook.Worksheets.FirstOrDefault();
                int StartRow = 6;

                if (query != null)
                {
                    int row = 0;
                    foreach(var item in query)
                    {
                        worksheet.Cells[(StartRow + row), 1].Value = item.Asset_Number;
                        worksheet.Cells[(StartRow + row), 2].Value = item.Asset_Category_Code;
                        worksheet.Cells[(StartRow + row), 3].Value = item.Assets_Category_ID;
                        worksheet.Cells[(StartRow + row), 4].Value = item.Assets_Name;
                        worksheet.Cells[(StartRow + row), 5].Value = item.Model_Number;
                        worksheet.Cells[(StartRow + row), 6].Value = (item.Transaction_Date != null) ? item.Transaction_Date.Value.ToString("yyyy/MM/dd") : "";
                        worksheet.Cells[(StartRow + row), 7].Value = item.Children;
                        worksheet.Cells[(StartRow + row), 8].Value = item.Life_Years;
                        worksheet.Cells[(StartRow + row), 9].Value = item.Assets_Unit;
                        worksheet.Cells[(StartRow + row), 10].Value = item.Assets_Fixed_Cost;
                        worksheet.Cells[(StartRow + row), 11].Value = item.PO_Number;
                        worksheet.Cells[(StartRow + row), 12].Value = item.Assigned_Branch;
                        worksheet.Cells[(StartRow + row), 13].Value = item.Remark;

                        row++;
                    }
                }
                ep.SaveAs(output);
                output.Position = 0;
            }
            var fsr = new FileStreamResult(output, ExportContentType);
            fsr.FileDownloadName = "財產新增報告單-動產.xlsx";
            return fsr;
        }


        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult MPScrapList()
        {
            return View();
        }


        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult PrintMPScrapList(MPReportSearchModel condition)
        {
            FileInfo Template = GetReportTemplate("MPScrapList");
            MemoryStream output = new MemoryStream();
            IEnumerable<MPReportModel> query = this.ReportServant.GetMPScrapList(condition);
            using (ExcelPackage ep = new ExcelPackage(Template, true))
            {
                var worksheet = ep.Workbook.Worksheets.FirstOrDefault();
                int StartRow =7;
                int row = 0;
                if (query != null)
                {
                    foreach(var item in query)
                    {
                        worksheet.Cells[(StartRow + row), 1].Value = item.Asset_Number;
                        worksheet.Cells[(StartRow + row), 2].Value = item.Assets_Name;
                        worksheet.Cells[(StartRow + row), 3].Value = item.Model_Number;
                        worksheet.Cells[(StartRow + row), 4].Value = (item.Date_Placed_In_Service != null) ? item.Date_Placed_In_Service.Value.ToString("yyyy/MM/dd") : "";
                        worksheet.Cells[(StartRow + row), 5].Value = item.Life_Years;
                        worksheet.Cells[(StartRow + row), 6].Value = item.Life_Month;
                        worksheet.Cells[(StartRow + row), 7].Value = (item.Date_Placed_In_Service != null) ? item.Date_Placed_In_Service.Value.AddYears(item.Life_Years).ToString("yyyy/MM/dd") : "";
                        worksheet.Cells[(StartRow + row), 8].Value = item.Assets_Unit;
                        worksheet.Cells[(StartRow + row), 9].Value = item.Assets_Fixed_Cost;
                        worksheet.Cells[(StartRow + row), 10].Value = item.Deprn_Reserve;
                        worksheet.Cells[(StartRow + row), 11].Value = item.Net_Value;
                        worksheet.Cells[(StartRow + row), 12].Value = item.Assigned_Branch;

                        row++;
                    }
       
                }
                ep.SaveAs(output);
                output.Position = 0;
            }
            var fsr = new FileStreamResult(output, ExportContentType);
            fsr.FileDownloadName = "可報廢財產明細表-動產.xlsx";
            return fsr;
        }


        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult MPScrapApprovedList()
        {
            return View();
        }


        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult PrintMPScrapApprovedList(MPReportSearchModel condition)
        {
            FileInfo Template = GetReportTemplate("MPScrapApprovedList");
            MemoryStream output = new MemoryStream();
            IEnumerable<MPReportModel> query = this.ReportServant.GetMPScrapApprovedList(condition);
            using (ExcelPackage ep = new ExcelPackage(Template, true))
            {
                var worksheet = ep.Workbook.Worksheets.FirstOrDefault();
                int StartRow = 8;
                if (query != null)
                {
                    int row = 0;
                    foreach(var item in query)
                    {
                        worksheet.Cells[(StartRow + row), 1].Value = item.Asset_Number;
                        worksheet.Cells[(StartRow + row), 2].Value = item.Assets_Name;
                        worksheet.Cells[(StartRow + row), 3].Value = item.Model_Number;
                        worksheet.Cells[(StartRow + row), 4].Value = (item.Date_Placed_In_Service != null) ? item.Date_Placed_In_Service.Value.ToString("yyyy/MM/dd") : "";
                        worksheet.Cells[(StartRow + row), 5].Value = item.Life_Years;
                        worksheet.Cells[(StartRow + row), 6].Value = item.Life_Month;
                        worksheet.Cells[(StartRow + row), 7].Value = (item.Date_Placed_In_Service != null) ? item.Date_Placed_In_Service.Value.AddYears(item.Life_Years).ToString("yyyy/MM/dd") : "";
                        worksheet.Cells[(StartRow + row), 8].Value = item.Assets_Unit;
                        worksheet.Cells[(StartRow + row), 9].Value = item.Assets_Fixed_Cost;
                        worksheet.Cells[(StartRow + row), 10].Value = item.Deprn_Cost;
                        worksheet.Cells[(StartRow + row), 11].Value = item.Net_Value;
                        worksheet.Cells[(StartRow + row), 12].Value = item.Assigned_Branch;
                        row++;
                    }
                }
                ep.SaveAs(output);
                output.Position = 0;
            }
            var fsr = new FileStreamResult(output, ExportContentType);
            fsr.FileDownloadName = "財產報廢核准明細表-動產.xlsx";
            return fsr;
        }


        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult MPDisposeList()
        {
            return View();
        }


        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult PrintMPDisposeList(MPReportSearchModel condition)
        {
            FileInfo Template = GetReportTemplate("MPDisposeList");
            MemoryStream output = new MemoryStream();
            IEnumerable<MPReportModel> query = this.ReportServant.GetMPScrapApprovedList(condition);
            using (ExcelPackage ep = new ExcelPackage(Template, true))
            {
                var worksheet = ep.Workbook.Worksheets.FirstOrDefault();
                int StartRow = 8;
                if (query != null)
                {
                    int row = 0;
                    foreach(var item in query)
                    {
                        worksheet.Cells[(StartRow + row), 1].Value = item.Asset_Number;
                        worksheet.Cells[(StartRow + row), 2].Value = item.Assets_Name;
                        worksheet.Cells[(StartRow + row), 3].Value = item.Model_Number;
                        worksheet.Cells[(StartRow + row), 4].Value = (item.Date_Placed_In_Service != null) ? item.Date_Placed_In_Service.Value.ToString("yyyy/MM/dd") : "";
                        worksheet.Cells[(StartRow + row), 5].Value = item.Life_Years;
                        worksheet.Cells[(StartRow + row), 6].Value = item.Life_Month;
                        worksheet.Cells[(StartRow + row), 7].Value = item.Deprn_Date;
                        worksheet.Cells[(StartRow + row), 8].Value = item.Assets_Unit;
                        worksheet.Cells[(StartRow + row), 9].Value = item.Assets_Fixed_Cost;
                        worksheet.Cells[(StartRow + row), 10].Value = item.Deprn_Cost;
                        worksheet.Cells[(StartRow + row), 11].Value = item.Net_Value;
                        worksheet.Cells[(StartRow + row), 12].Value = item.Assigned_Branch;
                    }
                }
                ep.SaveAs(output);
                output.Position = 0;
            }
            var fsr = new FileStreamResult(output, ExportContentType);
            fsr.FileDownloadName = "財產災害損失、變賣明細表.xlsx";
            return fsr;
        }
       
        [HttpPost]

        [Login]
        [FunctionAuthorize(ID = "MPImport")]
        [ExceptionHandle]
        public ActionResult SearchMPList(MPReportSearchModel condition)
        {
            string viewname = condition.ReportName;
            string AssignedBranchSelect = condition.JurisdictionAssignedBranch;
            string OfficeBranchSelect = condition.JurisdictionOfficeBranch;
            string BookTypeCodeSelect = condition.BookTypeCode;
            IEnumerable<MPReportModel> query=null;
            
            if (viewname == "MPDetails")
            {
                query = this.ReportServant.GetMPAssets(condition);

            }
            else if (viewname == "MPDisposeList")
            {
                query = this.ReportServant.GetMPDisposeList(condition);
            }
            else if (viewname == "MPNewList")
            {
                query = this.ReportServant.GetMPNewList(condition);
            }
            else if (viewname == "MPScrapApprovedList")
            {
                query = this.ReportServant.GetMPScrapApprovedList(condition);
            }
            else if (viewname == "MPScrapList")
            {
                query = this.ReportServant.GetMPScrapApprovedList(condition);
            }
            
           
            return View(viewname,query);
        }
        #region===共用===

        /// <summary>取得報表範本</summary>
        private FileInfo GetReportTemplate(string source)
        {
            if (!source.EndsWith(".xlsx"))
            {
                source += ".xlsx";
            }
            FileInfo Template = new FileInfo(Server.MapPath("~/App_Data/ReportTemplates/" + source));
            return Template;
        }

        /// <summary>ContentType</summary>
        private string ExportContentType
        {
            get
            {
                return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            }
        }
        #endregion
    }
}