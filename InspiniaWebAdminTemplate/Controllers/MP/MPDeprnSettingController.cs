﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Servants.Search;
using InspiniaWebAdminTemplate.Models.MP;
using Library.Servant.Servant.MP;
using Library.Servant.Servant.MP.MPModels;
using Newtonsoft.Json;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.ApprovalFlow;
using Library.Servant.Servant.UserInformation.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.Authentication.Models;
using System.IO;
using System.Text;
using Library.Servant.Repository.MP;
using Library.Servant.Servant.Email;
using Library.Servant.Servant.Email.Models;

namespace InspiniaWebAdminTemplate.Controllers.MP
{
    public class MPDeprnSettingController : Controller
    {
        private IMPServant MPServant = ServantAbstractFactory.MP();
        int PageSize = 10;

        [Login]
        [FunctionAuthorize(ID = "MPDeprnSetting")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            var vm = new AssetsIndexViewModel()
            {
                condition = new AssetsSearchViewModel()
                {
                     Page = 1,
                     PageSize = 10
                },
                Items = new List<AssetViewModel>()
            };
            return View(vm);
        }

        /// <summary>
        /// 資產搜尋
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "MPDeprnSetting")]
        [ExceptionHandle]
        public ActionResult Asset_Query(AssetsSearchViewModel condition)
        {
            condition.Page = 1;
            condition.PageSize = 10;
            var data = this.MPServant.SearchAssets(MPHelper.ToAssetsSearchModel(condition));
            AssetsIndexViewModel vm = new AssetsIndexViewModel()
            {
                Items = data.Items.Select(o => MPHelper.ToAssetViewModel(o)),
                condition = condition,
                TotalAmount = data.TotalAmount
            };
            return View("~/Views/MPDeprnSetting/Index.cshtml", vm);
        }

        /// <summary>
        /// 分頁Action
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "MPDeprnSetting")]
        [ExceptionHandle]
        public ActionResult AssetsUpdatePage(int page)
        {
            AssetsSearchViewModel condition = new AssetsSearchViewModel() { Page = page,PageSize = PageSize };
            var data = this.MPServant.SearchAssets(MPHelper.ToAssetsSearchModel(condition));
            AssetsIndexViewModel vm = new AssetsIndexViewModel()
            {
                Items = data.Items.Select(o => MPHelper.ToAssetViewModel(o)),
                condition = condition,
                TotalAmount = data.TotalAmount
            };
            return View("~/Views/MPDeprnSetting/Index.cshtml", vm);
        }


        /// <summary>
        /// 分頁Action
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "MPDeprnSetting")]
        [ExceptionHandle]
        public ActionResult AssetDetail(int id)
        {
            ViewBag.Title = "檢視資產";
            AssetViewModel vm = MPHelper.ToAssetViewModel(this.MPServant.GetAssetMPDetail(id));
            return View(MPHelper.GetMPView("Asset"),vm);
        }

        /// <summary>
        /// 分頁Action
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "MPDeprnSetting")]
        [ExceptionHandle]
        public ActionResult AssetsDeprnUpdate(string Json, AssetsSearchViewModel condition)
        {
            IEnumerable<AssetModel> assets = JsonConvert.DeserializeObject<IEnumerable<AssetModel>>(Json);
            if(this.MPServant.AssetsDeprnUpdate(assets))
            {
                TempData["message"] = "資產折舊設定成功!";
            }
            else
            {
                TempData["message"] = "資產折舊設定失敗!";
            }
            condition.PageSize = 10;
            var data = this.MPServant.SearchAssets(MPHelper.ToAssetsSearchModel(condition));
            AssetsIndexViewModel vm = new AssetsIndexViewModel()
            {
                Items = data.Items.Select(o => MPHelper.ToAssetViewModel(o)),
                condition = condition,
                TotalAmount = data.TotalAmount
            };
            
            return View("~/Views/MPDeprnSetting/Index.cshtml", vm);
        }


    }
}