﻿using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.Authentication;
using Library.Servant.Servant.UserInformation.Models;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Servants.Search;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.Email;
using Library.Servant.Servant.Db;

namespace InspiniaWebAdminTemplate.Controllers
{
    [ExceptionHandle]
    public class AccountController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel vm)
        {
            var result = AdwsServant.Login( vm.Account, vm.Password );
            UserInfoServant.Update( (UserInfor)result, HttpContext.ApplicationInstance.Context);
            AdwsServant.LoginRecord(result.ID, Request.UserHostAddress);
            
            return RedirectToAction("Index","Home");
        }

        public ActionResult Logout()
        {
            AdwsServant.Logout( this.UserID(), Request.UserHostAddress);
            UserInfoServant.Delete(HttpContext.ApplicationInstance.Context);
            SearchConditionServant.Update( new { }, HttpContext.ApplicationInstance.Context);

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult DB()
        {
            if(DbServant.IsExist())
                return Json(new { ok = true });
            else
                return HttpNotFound();
        }

        [HttpPost]
        public ActionResult ADWS()
        {
            if ( AdwsServant.IsExist() )
                return Json(new { ok = true });
            else
                return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Email()
        {
            if (EmailServant.IsExist())
                return Json(new { ok = true });
            else
                return HttpNotFound();
        }
        
        private readonly IDbServant DbServant = ServantAbstractFactory.Db();
        private readonly IAuthenticationServant AdwsServant = ServantAbstractFactory.Authentication();
        private readonly IEmailServant EmailServant = ServantAbstractFactory.Email();
    }
}


