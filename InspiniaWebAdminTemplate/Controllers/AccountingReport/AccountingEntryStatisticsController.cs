﻿using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.AccountingEntryStatistics;
using FastReport.Web;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers.AccountingReport
{
    [Login]
    [ExceptionHandle]
    public class AccountingEntryStatisticsController : Controller
    {
        public ActionResult Index()
        {
            return View(Servant.Options());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Search(BranchSearchModel model)
        {
            model.UserID = this.UserID();
            WebReport webReport = new WebReport { Width = model.ReportWidth };
            webReport.Bind( Server.FrxPath("AccountingEntryStatistics.frx"), Servant.Index(model).GroupsMoney());

            return PartialView(webReport);
        }

        private IAccountingEntryStatisticsServant Servant = ServantAbstractFactory.AccountingEntryStatistics();
    }
}