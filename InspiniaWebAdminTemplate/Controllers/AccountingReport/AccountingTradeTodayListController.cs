﻿using FastReport.Web;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.AccountingTradeTodayList;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers.AccountingReport
{
    [Login]
    [ExceptionHandle]
    public class AccountingTradeTodayListController : Controller
    {
        public ActionResult Index()
        {
            return View(Servant.Options());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Search(BranchSearchModel model)
        {
            WebReport webReport = new WebReport { Width = model.ReportWidth };
            webReport.Bind( Server.FrxPath("AccountingTradeTodayList.frx"), Servant.Index(model).Items);

            return PartialView(webReport);
        }

        private IAccountingTradeTodayListServant Servant = ServantAbstractFactory.AccountingTradeTodayList();
    }
}