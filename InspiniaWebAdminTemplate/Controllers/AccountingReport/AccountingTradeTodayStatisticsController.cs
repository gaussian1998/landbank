﻿using InspiniaWebAdminTemplate.Attributes;
using System.Web.Mvc;
using Library.Servant.Servant.AccountingTradeTodayStatistics;
using FastReport.Web;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers.AccountingReport
{
    [Login]
    [ExceptionHandle]
    public class AccountingTradeTodayStatisticsController : Controller
    {
        public ActionResult Index()
        {
            return View(Servant.Options());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Search(BranchSearchModel model)
        {
            WebReport webReport = new WebReport { Width = model.ReportWidth };
            webReport.Bind(Server.FrxPath("AccountingTradeTodayStatistics.frx"), Servant.Index(model).GroupsMoney());

            return PartialView(webReport);
        }

        private IAccountingTradeTodayStatisticsServant Servant = ServantAbstractFactory.AccountingTradeTodayStatistics();
    }
}