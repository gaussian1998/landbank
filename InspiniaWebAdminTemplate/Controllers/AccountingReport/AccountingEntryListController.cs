﻿using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.AccountingEntryList;
using FastReport.Web;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Controllers.AccountingReport
{
    [Login]
    [ExceptionHandle]
    public class AccountingEntryListController : Controller
    {
        public ActionResult Index()
        {
            return View(Servant.Options());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Search(BranchSearchModel model)
        {
            WebReport webReport = new WebReport { Width = model.ReportWidth };
            webReport.Bind(Server.FrxPath("AccountingEntryList.frx"), Servant.Index(model).Items);

            return PartialView(webReport);
        }

        private IAccountingEntryListServant Servant = ServantAbstractFactory.AccountingEntryList();
    }
}