﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.MU
{
    public class MUReportController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "MUReport")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }
    }
}