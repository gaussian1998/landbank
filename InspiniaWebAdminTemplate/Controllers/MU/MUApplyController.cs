﻿using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.MU;
using Library.Servant.Servant.MU.MUModels;
using InspiniaWebAdminTemplate.Models.MU;
using Library.Servant.Servants.AbstractFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Servant.UserInformation.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.Land;
using Newtonsoft.Json.Linq;
using Library.Servant.Repository.MU;

namespace InspiniaWebAdminTemplate.Controllers.MU
{
    public class MUApplyController : Controller
    {
        private IMUServant MUServant = ServantAbstractFactory.MU();
        private ILandServant LandServant = ServantAbstractFactory.Land();
        private string Type = "F200";
        int PageSize = 10;

        

        [Login]
        [FunctionAuthorize(ID = "MU")]
        [ExceptionHandle]
        public ActionResult Index()
        {
           
            EInt type = new EInt();
            type.type = 2;
            EJObject td = this.MUServant.DocumentType(type);
            EString code = new EString();
            code.type = "V01";
            EJObject bc = this.MUServant.GetCode(code);
            type.type = 1;
            EJObject ju1 = this.MUServant.GetDPM(type);
            type.type = 2;
            EJObject ju2 = this.MUServant.GetDPM(type);
            code.type = "T02";
            EJObject ty = this.MUServant.GetCode(code);
           
            ViewBag.DocumentType = td.Obj;
            ViewBag.DateNow = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.MGDateNow = MUGeneral.CvtD2S(DateTime.Today).Replace("-", "/");
            ViewBag.bc = bc.Obj;
            ViewBag.ju1 = ju1.Obj;
            ViewBag.ju2 = ju2.Obj;
            ViewBag.ty = ty.Obj;
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "MU")]
        [ExceptionHandle]
        public ActionResult IndexList(string json = "", int page = 1)
        {
          
            EJObject con = new EJObject();
            var condata = JObject.Parse(json);
            condata["Page"] = page;
            condata["PageSize"] = PageSize;
            con.Obj = condata;
            EJObject tiq = this.MUServant.Index(con);
            if (json != "")
                ViewBag.test = condata;
            else
                ViewBag.test = "NULL";

            EInt tpb = new EInt();
            tpb.type = 1;
            EJObject PB = this.MUServant.GetDPM(tpb);

            for (int j = 0; j < tiq.Obj["Data"].Count(); j++)
            {
                tiq.Obj["Data"][j]["J1"] = "總行";
               
            }
            ViewBag.data = tiq.Obj;
            TempData["amount"] = tiq.Obj["amount"]; //筆數
            TempData["pagestart"] = tiq.Obj["pagestart"]; //變數+1 就是起始序號
            TempData["pagecount"] = tiq.Obj["pagecount"]; //一頁幾筆
            return View();
        }
        /// <summary>
        /// 編輯單據畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "MU")]
        [ExceptionHandle]
        public ActionResult Edit(string id, string signmode)
        {
          string ID = "";
              if (Url.RequestContext.RouteData.Values["id"] != null)
                ID = Url.RequestContext.RouteData.Values["id"].ToString();
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
          

            EInt type = new EInt();
           
            EString code = new EString();
            code.type = "V01";
            EJObject bc = this.MUServant.GetCode(code);
            type.type = 1;
            EJObject ju1 = this.MUServant.GetDPM(type);
            type.type = 2;
            EJObject ju2 = this.MUServant.GetDPM(type);

           
           
            ViewBag.DateNow = DateTime.Today.ToString("yyyy/MM/dd");
            ViewBag.MGDateNow = MUGeneral.CvtD2S(DateTime.Today).Replace("-", "/");
            ViewBag.bc = bc.Obj;
            ViewBag.ju1 = ju1.Obj;
            ViewBag.ju2 = ju2.Obj;
            ViewBag.ID = ID;
            EJObject tmp = new EJObject();
            tmp.Obj = JObject.Parse("{}");
            EJObject MaxNo = this.MUServant.GetMaxNo(tmp);
            if (ID == "")
            {
                ViewBag.isnew = true;
                string tmp1 = "000001";
                if (MaxNo.Obj["Data"].Count() > 0)
                    tmp1 = MaxNo.Obj["Data"][0]["maxno"].ToString().PadLeft(6, '0');
                ViewBag.No = MUGeneral.CvtD2S(DateTime.Today).Replace("-", "") + "F200" + tmp1; ViewBag.State = "新增";
                TempData["RealNo"] = MUGeneral.CvtD2S(DateTime.Today).Replace("-", "") + "F200";
                TempData["RealID"] = null;
                ViewBag.ID = "0";
            }
            else
            {
                EString edd = new EString();
                edd.type = ID;
                EJObject EdOrd = this.MUServant.GetOrder(edd);
                //ViewBag.No = EdOrd.Obj["Data"][1]["mh"]["Activation_No"];
                if ((EdOrd.Obj["Data"].Count() <= 0) ||
                    ((EdOrd.Obj["Data"][0]["mh"]["Type"].ToString() != "0") &&
                    (EdOrd.Obj["Data"][0]["mh"]["Type"].ToString() != "3") &&
                    (EdOrd.Obj["Data"][0]["mh"]["Type"].ToString() != "4")))
                {
                    return RedirectToAction("Index");
                }
                ViewBag.isnew = false;
                ViewBag.EdOrd = EdOrd.Obj["Data"][0];
                ViewBag.No = EdOrd.Obj["Data"][0]["mh"]["TRX_Header_ID"];
                ViewBag.State = EdOrd.Obj["Data"][0]["TY"];
                TempData["RealID"] = ID;
                TempData["RealNo"] = EdOrd.Obj["Data"][0]["mh"]["TRX_Header_ID"];
            }
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "MU")]
        [ExceptionHandle]
        public ActionResult EditH(string json = "{}", int page = 1, int customsize = 10, string AID = "0", int V = 0)
        {
            if (json == "") json = "{}";
            EJObject con = new EJObject();
            var condata = JObject.Parse(json);


            condata["Page"] = page;
            condata["PageSize"] = customsize;

            condata["AID"] = AID;
            con.Obj = condata;
            JArray btmp = new JArray();
            
           
            EJObject tiq = new EJObject();


            //AB.Obj["Data"].AddAfterSelf


            
            if (condata["ID"] == null)
            {
                if (btmp.Count() == 0)
                    btmp.Add(0);
                condata["ID"] = btmp;
                EJObject ABC = this.MUServant.GetDetail(con);
                /*for (int i = 0; i < ABC.Obj["Data"].Count(); i++)
                {
                    btmp.Add(Int32.Parse(ABC.Obj["Data"][i]["Land_ID"].ToString()));
                }*/
                condata["ID"] = ABC.Obj["tmpq2"];
            }
            else
            {
                var tb = (JArray)condata["ID"];
                for (int i = 0; i < tb.Count(); i++)
                {
                    btmp.Add(Int32.Parse(tb[i].ToString()));
                }
                condata["ID"] = btmp;
            }
            if (btmp.Count() == 0) { 
                btmp.Add(0);
            condata["ID"] = btmp;
            }
            con.Obj = condata;
            EJObject AB = this.MUServant.GetDetail(con);

           

            
            ViewBag.data = AB.Obj;
            ViewBag.OrgID = condata["ID"].ToString().Replace(System.Environment.NewLine, "");
            ViewBag.V = V;
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "MU")]
        [ExceptionHandle]
        public ActionResult GetLandList(string json = "{}", int page = 1, int customsize = 10)
        {
            EJObject con = new EJObject();
            var condata = JObject.Parse(json);
            condata["Page"] = page;
            condata["PageSize"] = customsize;
            con.Obj = condata;
            EJObject tiq = this.MUServant.GetLand(con);
            ViewBag.Data = tiq.Obj;
            return View();
        }
        [Login]
        [FunctionAuthorize(ID = "MU")]
        [ExceptionHandle]
        public ActionResult ApplyEDOC(string Data = "",string AID = "0")
        {
            EJObject con1 = new EJObject();
            var condata = JObject.Parse("{}");
            condata["Page"] = 1;
            condata["PageSize"] = 99999999;
            condata["AID"] = AID;
            condata["ID"] = JToken.FromObject(Data.Split(','));
            con1.Obj = condata;
            EJObject AB = this.MUServant.GetDetail(con1);

           

            EString con = new EString();
            string path = System.IO.File.ReadAllText(Server.MapPath("~/App_Data/HuaAn/ApplyEDOC.xml"));
            string ini =Server.MapPath("~/App_Data/HuaAn/Config.ini");
            string DataP = "";

            path = path.Replace("#1#", MUHelper.GetIni(ini, "ApplyInfo", "ApplyMode"));
            path = path.Replace("#2#", MUHelper.GetIni(ini, "ApplyInfo", "MaxPage"));
            path = path.Replace("#3#", MUHelper.GetIni(ini, "ApplyInfo", "UserID"));
            path = path.Replace("#4#", MUHelper.GetIni(ini, "ApplyInfo", "User"));
            path = path.Replace("#5#", MUHelper.GetIni(ini, "ApplyInfo", "UnitID"));
            path = path.Replace("#6#", MUHelper.GetIni(ini, "ApplyInfo", "Unit"));
            path = path.Replace("#7#", MUHelper.GetIni(ini, "ApplyInfo", "AccountID"));
            path = path.Replace("#8#", MUHelper.GetIni(ini, "ApplyInfo", "PWD"));
            path = path.Replace("#9#", MUHelper.GetIni(ini, "ApplyInfo", "ACCTYPE"));
            path = path.Replace("#10#", MUHelper.GetIni(ini, "ApplyInfo", "AAA_CODE"));
            path = path.Replace("#11#", MUHelper.GetIni(ini, "ApplyInfo", "CERT_ID"));
            path = path.Replace("#12#", MUHelper.GetIni(ini, "ApplyInfo", "CERT_NAME"));

            for (int i=0;i < AB.Obj["Data"].Count(); i++)
            {
                string DataT = System.IO.File.ReadAllText(Server.MapPath("~/App_Data/HuaAn/ApplyData.xml"));
                DataT = DataT.Replace("#NO#", i.ToString());
                DataT = DataT.Replace("#A#", AB.Obj["Data"][i]["City_Name"].ToString());
                DataT = DataT.Replace("#B#", AB.Obj["Data"][i]["District_Name"].ToString());
                DataT = DataT.Replace("#C#", AB.Obj["Data"][i]["Authorization_Number"].ToString());
                DataT = DataT.Replace("#D#", AB.Obj["Data"][i]["Asset_Number"].ToString());
                DataT = DataT.Replace("#E#", "");
                DataT = DataT.Replace("#F#", "");
                DataP += DataT + System.Environment.NewLine;
            }
            path = path.Replace("#DataArea#", DataP);
            con.type = path;
            
            EString tiq = this.MUServant.ApplyEDOC(con);
            ViewBag.Data = tiq.type;
            return Content(tiq.type);
        }
        
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "MU")]
        [ExceptionHandle]
        public ActionResult UpdateOrder(FormCollection asset3)
        {
            //JObject asset2 = asset3;
            JObject asset = new JObject();
            asset["BeginDate"] = asset3["BeginDate"].ToString();
            if (asset3["SaveMode"].ToString() == "1")
                asset["Type"] = "1";
            else
                asset["Type"] = "0";
            if (TempData["RealID"] != null)
                asset["ID"] = TempData["RealID"].ToString();
            asset["TRX_Header_ID"] = TempData["RealNo"].ToString();
            foreach (string key in asset3.Keys)
            {
                asset[key] = asset3[key].ToString();
            }

            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            asset["Created_By"] = user.ID.ToString();
            asset["Last_Updated_By"] = user.ID.ToString();

            EJObject T = new EJObject();
            T.Obj = new JObject(new JProperty("Data", asset));

            var Result = this.MUServant.UpdateF200Order(T);

            if (asset["Type"].ToString() == "0")
                return RedirectToAction("Edit", new { id = Result.Obj["TRX_Header_ID"] });
            else
                return RedirectToAction("Index");
        }


        /// <summary>
        /// 跑簽核流程
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "MU")]
        [ExceptionHandle]
        public ActionResult CreateFlow(MUHeaderViewModel header)
        {
            /*
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            int DocID;
            string message = "提交成功!";
            if (MUServant.GetDocID(Type, out DocID))
            {
                LocalApprovalTodoServant.Create(user.ID, DocID, header.TRX_Header_ID, "MU");
                LocalApprovalTodoServant.Start(user.ID, header.TRX_Header_ID, header.TRX_Header_ID);
            }
            else
            {
                message = "查無此表單!!";
            }
            TempData["message"] = message;
            return RedirectToAction("Edit", new { id = header.TRX_Header_ID });*/
            return RedirectToAction("Edit");
        }


    }
}