﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.MU
{
    public class MUSearchController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "MUSearch")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "MUSearch")]
        [ExceptionHandle]
        public ActionResult Detail()
        {
            return View();
        }
    }
}