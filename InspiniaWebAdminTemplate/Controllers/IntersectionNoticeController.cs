﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers
{
    public class IntersectionNoticeController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "IntersectionNotice")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }
    }
}