﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FastReport.Web;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Extension;
using InspiniaWebAdminTemplate.Models.TodoListReport;
using Library.Entity.Edmx;
using Library.Servant.Servant.TodoListManagement;
using Library.Servant.Servant.TodoListManagement.Models;

namespace InspiniaWebAdminTemplate.Controllers.Todo
{
    [Login]
    public class TodoListReportController : Controller
    {
        private IEnumerable<SelectListItem> _statusList// 之後會由Servant取得
        {
            get {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text="作業中", Value="0" },
                    new SelectListItem { Text="已完成", Value="1" },
                    new SelectListItem { Text="作廢", Value="2" },
                    new SelectListItem { Text="結案", Value="3" }
                };
            }
        }

        // GET: TodoListReport
        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();
            WebReport webReport = new WebReport();
            webReport.Width = 1000;
            webReport.Bind(Server.FrxPath("ToDoListReport.frx"), Servant.GetReportContent((ReportSearchModel)condition));
            webReport.Report.SetParameterValue("Creator", this.User.Identity.Name);
            webReport.Report.SetParameterValue("CreateDate", DateTime.Now);
            webReport.Report.SetParameterValue("Title", "交辦事項明細報表");

            ViewBag.WebReport = webReport;
            ViewBag.StatusList = new SelectList(_statusList, "Value", "Text", condition.StatusCode);
            return View(condition);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(SearchViewModel vm)
        {
            this.UpdateSearchCondition<SearchViewModel>(vm);
            return RedirectToAction("Index");
        }

        private List<AS_VW_TodoList_Report> test
        {
            get {
                return new List<AS_VW_TodoList_Report>
                {
                    new AS_VW_TodoList_Report { Code="123", Status="test", User_Name="you", Subject="test", Create_Time=DateTime.Now, Start=DateTime.Now, End=DateTime.Now },
                    new AS_VW_TodoList_Report { Code="456", Status="test", User_Name="you", Subject="test", Create_Time=DateTime.Now, Start=DateTime.Now, End=DateTime.Now }
                };
            }
        }
        
        private ITodoListManagement Servant = new LocalTodoListManagement();
    }
}