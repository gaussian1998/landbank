﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.TodoListManagement;
using InspiniaWebAdminTemplate.Servants.Search;
using Library.Servant.Servant.TodoListManagement.Models;
using Library.Servant.Servant.TodoListManagement;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.CodeTable;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.ApprovalTodo;
using System.IO;

namespace InspiniaWebAdminTemplate.Controllers.Todo
{
    [Login]
    [FunctionAuthorize(ID = "TodoListManagement")]
    [ExceptionHandle]
    public class TodoListManagementController : Controller
    {
        private IEnumerable<SelectListItem> _statusList// 之後會由Servant取得
        {
            get {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text="作業中", Value="0" },
                    new SelectListItem { Text="已完成", Value="1" },
                    new SelectListItem { Text="作廢", Value="2" },
                    new SelectListItem { Text="結案", Value="3" }
                };
            }
        }

        private IEnumerable<SelectListItem> _categories// 之後會由Servant取得
        {
            get {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text="電腦系統", Value="1" },
                    new SelectListItem { Text="不動產", Value="2" },
                    new SelectListItem { Text="動產", Value="3" }
                };
            }
        }
        
        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();

            ViewBag.StatusList = new SelectList(_statusList, "Value", "Text", condition.Status);

            var result = Servant.Index((SearchModel)condition);

            IndexViewModel vm = new IndexViewModel
            {
                Items = result.Items.Select(m => (ItemViewModel)m).ToList(),
                TotalAmount = result.TotalAmount,
                Conditions = condition
            };

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(SearchViewModel vm)
        {
            this.UpdateSearchCondition<SearchViewModel>(vm);
            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            var vm = (DetailViewModel)Servant.Detail("");
            vm.AssignUserID = this.UserID();
            vm.AssignUser = this.UserInformation().Name;
            vm.UpdateUser = this.UserID();
            vm.UpdateUserName = this.UserInformation().Name;

            ViewBag.CanEdit = true;
            ViewBag.CanAudit = false;
            ViewBag.StatusList = new SelectList(_statusList, "Value", "Text", vm.Status);
            ViewBag.CategoryList = new SelectList(_categories, "Value", "Text", vm.Type);
            ViewBag.BranchList = new SelectList(LocalCodeTableServant.BranchOptions(), "Value", "Text", "");
            ViewBag.DepartmentList = new SelectList(LocalCodeTableServant.DepartmentOptions(), "Value", "Text", "");

            return View("Detail", vm);
        }

        public ActionResult Show(string code)
        {
            var vm = (DetailViewModel)Servant.Detail(code);
            vm.UpdateUser = this.UserID();
            vm.UpdateUserName = this.UserInformation().Name;
            vm.StatusName = _statusList.First(m => m.Value == vm.Status).Text;

            foreach (var item in vm.AuditingFlowList) {
                var info = LocalApprovalTodoServant.Details(item.FlowCode);
                item.CurrentApproalName = info.CurrentApprovalUserName;
                item.FlowStatus = info.ApprovalStatus;
            }

            ViewBag.CanEdit = vm.AssignUserID == this.UserID();
            ViewBag.CanAudit = vm.TodoUsers.Any(m => m.UserID == this.UserID());
            ViewBag.StatusList = new SelectList(_statusList, "Value", "Text", vm.Status);
            ViewBag.CategoryList = new SelectList(_categories, "Value", "Text", vm.Type);
            ViewBag.BranchList = new SelectList(LocalCodeTableServant.BranchOptions(), "Value", "Text", "");
            ViewBag.DepartmentList = new SelectList(LocalCodeTableServant.DepartmentOptions(), "Value", "Text", "");

            return View("Detail", vm);
        }

        public ActionResult Save(DetailViewModel vm)
        {
            if (vm.File != null) {
                vm.FileName = Path.GetFileName(vm.File.FileName);
            }
            Servant.SaveTodoTask((TodoListInfo)vm);

            return RedirectToAction("Index");
        }

        public ActionResult Submit()
        {
            return RedirectToAction("Index");
        }

        public ActionResult GetUsers(string branch, string department)
        {
            var list = StaticDataServant.GetAllUserInfo().Where(m => m.BranchCode == branch && m.DepartmentCode == department).ToList();

            return Json(list.Select(m => new { id = m.UserID, name = m.UserName }));
        }

        public ActionResult GetFlowInfo(string flowCode)
        {
            var info = LocalApprovalTodoServant.Details(flowCode);

            return Json(info);
        }

        private ITodoListManagement Servant = ServantAbstractFactory.TodoList();
    }
}