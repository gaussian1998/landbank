﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.TodoListManagement;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.TodoListManagement;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.TodoListManagement.Models;

namespace InspiniaWebAdminTemplate.Controllers.Todo
{
    [Login]
    [FunctionAuthorize(ID = "QueryTodoList")]
    [ExceptionHandle]
    public class QueryTodoListController : Controller
    {
        private IEnumerable<SelectListItem> _statusList// 之後會由Servant取得
        {
            get {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text="作業中", Value="0" },
                    new SelectListItem { Text="已完成", Value="1" },
                    new SelectListItem { Text="作廢", Value="2" },
                    new SelectListItem { Text="結案", Value="3" }
                };
            }
        }

        // GET: QueryTodoList
        public ActionResult Index()
        {
            SearchViewModel condition = this.GetSearchCondition<SearchViewModel>();

            ViewBag.StatusList = new SelectList(_statusList, "Value", "Text", condition.Status);

            var result = TodoListServant.QueryIndex((SearchModel)condition, this.UserID());

            IndexViewModel vm = new IndexViewModel
            {
                Items = result.Items.Select(m => (ItemViewModel)m).ToList(),
                TotalAmount = result.TotalAmount,
                Conditions = condition
            };

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition(SearchViewModel vm)
        {
            this.UpdateSearchCondition<SearchViewModel>(vm);
            return RedirectToAction("Index");
        }

        private ITodoListManagement TodoListServant = ServantAbstractFactory.TodoList();
    }
}