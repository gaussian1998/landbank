﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.RentInquire
{
    public class RentInquireInvoiceController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "RentInquireInvoice")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }
    }
}