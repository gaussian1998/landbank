﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.RentInquire
{
    public class RentInquireMaintainController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "RentInquireMaintain")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "RentInquireMaintain")]
        [ExceptionHandle]
        public ActionResult Detail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "RentInquireMaintain")]
        [ExceptionHandle]
        public ActionResult Edit()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "RentInquireMaintain")]
        [ExceptionHandle]
        public ActionResult ContentDetail()
        {
            return View();
        }
    }
}