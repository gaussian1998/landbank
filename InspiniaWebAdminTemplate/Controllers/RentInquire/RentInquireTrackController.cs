﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.RentInquire
{
    public class RentInquireTrackController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "RentInquireTrack")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "RentInquireTrack")]
        [ExceptionHandle]
        public ActionResult Detail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "RentInquireTrack")]
        [ExceptionHandle]
        public ActionResult ContentDetail()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "RentInquireTrack")]
        [ExceptionHandle]
        public ActionResult ContentDetail_1()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "RentInquireTrack")]
        [ExceptionHandle]
        public ActionResult Edit()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "RentInquireTrack")]
        [ExceptionHandle]
        public ActionResult ContentEdit()
        {
            return View();
        }
    }
}