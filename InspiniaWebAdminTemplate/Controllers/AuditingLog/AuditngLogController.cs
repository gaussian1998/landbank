﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;

namespace InspiniaWebAdminTemplate.Controllers.AuditingLog
{
    [Login]
    [ExceptionHandle]
    public class AuditngLogController : Controller
    {
        // GET: AuditngLog
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearchCondition()
        {
            //this.UpdateSearchCondition<SearchViewModel>(vm);
            return RedirectToAction("Index");
        }
    }
}