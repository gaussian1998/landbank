﻿using System.Web.Mvc;
using Library.Servant.Servant.AccountingVerify;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Extension;
using InspiniaWebAdminTemplate.Models.AccountingVerify;
using Library.Servant.Servant.AccountingVerify.Models;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [ExceptionHandle]
    public class AccountingVerifyController : Controller
    {
        public ActionResult Index()
        {
            var condition = this.GetSearchCondition<SearchViewModel>();
            var result = Servant.Index( (SearchModel)condition );

            return View( new IndexViewModel {

                TradeOptions = Servant.TradeOptions(),
                Condition = condition,
                Pages = result.Items
            });
        }

        public ActionResult Details(string account)
        {
            var result = Servant.Details(new DetailsModel {  Account = account });

            return View(result.Items);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Verify()
        {
            Servant.Verify(new VerifyModel { UserID = this.UserID() });

            return RedirectToAction("Index");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult UpdateSearch(SearchViewModel model)
        {
            this.UpdateSearchCondition(model);

            return RedirectToAction("Index");
        }

        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult(Url.Action("Index") + "#page");
        }

        private LocalAccountingVerifyServant Servant = new LocalAccountingVerifyServant();
    }
}