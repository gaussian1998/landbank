﻿using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.AccountingDeprnTrial;
using Library.Servant.Servant.AccountingDeprnTrial.Models;
using Library.Servant.Servant.AccountingDeprnTrial;
using InspiniaWebAdminTemplate.Extension;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [ExceptionHandle]
    public class AccountingDeprnTrialController : Controller
    {
        public ActionResult Index()
        {
            var condition = this.GetSearchCondition<SearchViewModel>();
            var result = Servant.Index( new SearchModel
            {
                UserID = this.UserID(),
                TradeKinds = condition.TradeKinds
            } );

            return View(  new IndexViewModel {

                IsApproval = result.IsApproval,
                Options = result.Options,
                Items = result.Items,
                Condition = condition
            } );
        }

        public ActionResult Details(string Account)
        {
            var result = Servant.Details(new DetailsModel
            {
                UserID = this.UserID(),
                Account = Account
            });

            return View(result.Items);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Deprn()
        {
            Servant.Deprn( new DeprnModel {

                UserID = this.UserID()
            } );

            return RedirectToAction("Index");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Clear()
        {
            var condition = this.GetSearchCondition<SearchViewModel>();
            Servant.Clear( new DeprnTrialModel
            {
                UserID = this.UserID(),
                TradeKinds = condition.TradeKinds
            });

            return RedirectToAction("Index");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult DeprnTrial(SearchViewModel vm)
        {
            this.UpdateSearchCondition(vm);
            Servant.DeprnTrial( new DeprnTrialModel {
                
                UserID = this.UserID(),
                TradeKinds = vm.TradeKinds
            } );

            return RedirectToAction("Index");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult UpdateSearch(SearchViewModel vm)
        {
            this.UpdateSearchCondition(vm);

            return RedirectToAction("Index");
        }

        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult( Url.Action("Index") + "#page" );
        }

        private LocalAccountingDeprnTrialServant Servant = new LocalAccountingDeprnTrialServant();
    }
}