﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.AccountingDeprnVerify;
using InspiniaWebAdminTemplate.Extension;
using Library.Servant.Servant.AccountingDeprnVerify.Models;
using Library.Servant.Servant.AccountingDeprnVerify;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [ExceptionHandle]
    public class AccountingDeprnVerifyController : Controller
    {
        public ActionResult Start()
        {
            this.ClearSearchCondition<SearchViewModel>();
            return RedirectToAction("Index");
        }

        public ActionResult Index()
        {
            var condition = this.GetSearchCondition<SearchViewModel>();
            var result = IndexList(condition);

            return View( new IndexViewModel
            {
                TradeOptions = Servant.TradeOptions(),
                Condition = condition,
                Page = result.Page
            } );
        }

        public ActionResult Details(string Account)
        {
            var result = Servant.Details(new DetailsModel { Account = Account });

            return View(result.Items);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Verify()
        {
            var verfiySet = this.GetSearchCondition<SearchViewModel>().VerifyAccount;
            if (verfiySet.Count == 0)
            {
                TempData["ErrorMessage"] = "未選取任何項目";
            }
            else
            {
                this.ClearSearchCondition<SearchViewModel>();
                Servant.Verify(  new VerifyModel { UserID = this.UserID() , Accounts = verfiySet } );
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ReDeprn()
        {
            Servant.ReDeprn();

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSearch(SearchViewModel vm)
        {
            this.ModifySearchCondition<SearchViewModel>( condition => {

                condition.TradeKinds = vm.TradeKinds;
            } );

            return RedirectToAction("Index");
        }

        public ActionResult UpdatePage(int page)
        {
            this.ModifySearchCondition<SearchViewModel>(condition => condition.Page = page);

            return new RedirectResult(Url.Action("Index") + "#page");
        }

        [HttpPost]
        public ActionResult UpdateChecked(string Account, bool IsChecked)
        {
            this.ModifySearchCondition<SearchViewModel>( condition => {

                if(IsChecked)
                    condition.VerifyAccount.Add(Account);
                else
                    condition.VerifyAccount.Remove(Account);
            });

            return CheckedStatus();
        }

        [HttpPost]
        public ActionResult UpdateAllChecked(List<string> Accounts, bool IsChecked)
        {
            this.ModifySearchCondition<SearchViewModel>( condition => {

                Accounts.ForEach( account => {

                    if (IsChecked)
                        condition.VerifyAccount.Add(account);
                    else
                        condition.VerifyAccount.Remove(account);
                } );
            });

            return CheckedStatus();
        }
        

        private ActionResult CheckedStatus()
        {
            var verfiySet = this.GetSearchCondition<SearchViewModel>().VerifyAccount;
            return Json(JsonConvert.SerializeObject(verfiySet));
        }

        private  IndexResult IndexList(SearchViewModel condition)
        {
            return Servant.Index( new SearchModel
            {
                UserID = this.UserID(),
                Page = condition.Page,
                PageSize = condition.PageSize,
                TradeKinds = condition.TradeKinds
            });
        }
        

        private LocalAccountingDeprnVerifyServant Servant = new LocalAccountingDeprnVerifyServant();
    }
}