﻿using System.Data;
using System.Linq;
using System.Web.Mvc;
using InspiniaWebAdminTemplate.Models.Accounting;
using Library.Servant.Servant.Accounting;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Attributes;
using Library.Utility;
using Library.Entity.Edmx;
using System.Collections.Generic;
using System;

/// <summary>
///  writen by ricky
/// </summary>
namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [ExceptionHandle]
    public class AccountingEntriesController : Controller
    {
        private readonly LocalAccountingQueryServant _servant;
        private readonly IAccountingDDLServant _ddlServant;

        public AccountingEntriesController()
        {
            _servant = new LocalAccountingQueryServant();
            _ddlServant = ServantAbstractFactory.AccountingDDLServant();
        }

        public ActionResult Index()
        {
            var ddlTransTypes = _ddlServant.GetTransTypes().ToList();
            var ddlTradeTypes = _ddlServant.GetTradeTypes().ToList();
            ddlTransTypes.Add(new Option() { Key = "全部", Value = "" });
            ddlTradeTypes.Add(new Option() { Key = "全部", Value = "" });
            ViewBag.TransTypes = ddlTransTypes;
            ViewBag.TradeTypes = ddlTradeTypes;
            return View();
        }

        public ActionResult Report(AccountingEntriesQueryModel model)
        {
            //Acc_EntriesByHeader_FR.Reset();
            var report = new Reports("會計分錄檢查表", "AccountingEntries.frx", "AccountingEntries");
            var list = _servant.GetAccountingEntries();


            var ds = new DataSet("AccountingEntries");
            //var dt = MapProperty.ToDataTable("Acc_EntriesByHeader", MapProperty.MapAll<Acc_EntriesByHeader_FR, Acc_EntriesByHeader>( list.Where(e => e!=null) ).ToList()  );
            List<Acc_EntriesByHeader_FR> _listAccEntriesByHeader = new List<Acc_EntriesByHeader_FR>();
            foreach (Acc_EntriesByHeader _accEntriesByHeader in list) {
                Acc_EntriesByHeader_FR _accEntriesByHeaderFR = new Acc_EntriesByHeader_FR();
                _accEntriesByHeaderFR = MapProperty.OverrideBy<Acc_EntriesByHeader_FR, Acc_EntriesByHeader>(_accEntriesByHeaderFR, _accEntriesByHeader);
                if (_accEntriesByHeader.Post_Date == null)
                {
                    _accEntriesByHeaderFR.Post_Date = "";
                }
                else {
                    _accEntriesByHeaderFR.Post_Date =((DateTime)_accEntriesByHeader.Post_Date).ToString("yyyy/MM/dd");
                }
                _listAccEntriesByHeader.Add(_accEntriesByHeaderFR);
            }
            var dt = MapProperty.ToDataTable("Acc_EntriesByHeader", _listAccEntriesByHeader);
            ds.Tables.Add(dt);
            return report.FromDataSet(() => ds);
        }

        public partial class Acc_EntriesByHeader_FR
        {
            public string Post_Date { get; set; }
            public decimal Master_No { get; set; }
            public string Branch_Code { get; set; }
            public string Branch_Name { get; set; }
            public string Department { get; set; }
            public string Department_Name { get; set; }
            public string Account { get; set; }
            public string Account_Name { get; set; }
            public decimal DB { get; set; }
            public decimal CR { get; set; }
            public string Currency { get; set; }
            public string Notes { get; set; }
            public string Master_Text { get; set; }
            public string Asset_Number { get; set; }
            public string Lease_Number { get; set; }
            public string Trans_Type { get; set; }
            public string Batch_Seq { get; set; }
            public string Import_Number { get; set; }
            public System.DateTime Open_Date { get; set; }
            public string EntriesType { get; set; }
            public string Book_Type { get; set; }
            public string Trans_Seq { get; set; }
            public string Trade_Type { get; set; }
        }
    }
}
