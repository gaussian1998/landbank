﻿using InspiniaWebAdminTemplate.Attributes;
using System.Web.Mvc;
using Library.Servant.Servant.AccountingQueryDeprn;
using Library.Servant.Servant.Accounting.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common;
using InspiniaWebAdminTemplate.Models.Accounting;
using System;
using Library.Servant.Servants.AbstractFactory;
using Newtonsoft.Json;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.ComponentModel.DataAnnotations;

/// <summary>
/// Ricky 遺留的程式碼,尚未整理
/// </summary>
namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [ExceptionHandle]
    public class AccountingQueryDeprnController : Controller
    {
        IAccountingQueryDeprnServant _servant = ServantAbstractFactory.AccountingQueryDeprn();
        //private readonly IAccountingQueryServant _servant;
        //private readonly IAccountingDDLServant _ddlServant;
        //private readonly IAccountingSubjectServant _accountServant;
        //private readonly ITradeDefineServant _tradeDefineServer;
        //private readonly IAccountQueryDe

        //public AccountingQueryDeprnController()
        //{
        //    _servant = ServantAbstractFactory.AccountingQuery();
        //    _ddlServant = ServantAbstractFactory.AccountingDDLServant();
        //    _accountServant = new LocalAccountingSubjectServant();
        //    _servant.UserId = UserInfoServant.Details(System.Web.HttpContext.Current).ID;
        //    _tradeDefineServer = ServantAbstractFactory.TradeDefine();
        //}

        /// <summary>
        /// 查看合併後的項目
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult Index(_AccountingQueryModel model)
        {
            //var ddlTransTypes = _ddlServant.GetTransTypes().ToList();
            //var ddlTradeTypes = _ddlServant.GetTradeTypes().ToList();
            //ddlTransTypes.Add(new Option() { Key = "全部", Value = "" });
            //ddlTradeTypes.Add(new Option() { Key = "全部", Value = "" });
            ViewBag.TransTypes = _servant.GetTransTypes();
            ViewBag.TradeTypes = _servant.GetTradeTypes();

            //取得清單
            //GetDetail(model);
            ////群組結果
            //GroupDetail(model);

            //model.Total_Record = model.Details.Count();
            //if (model.Details.Count() > 1000)
            //{
            //    model.Details = model.Details.Take(1000).ToList();
            //    model.Total_Record = 1000;
            //    model.ErrorMessage = "筆數超過1000，請使用匯出功能下載。";
            //}
            //else
            //{
            //    model.ErrorMessage = "";
            //}
            //model.Details = model.Details.Skip((model.Page - 1) * 20).Take(20).ToList();
            return View(_servant.Index(model));
        }

        /// <summary>
        /// 查看子項目
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult Detail(_AccountingQueryModel model)
        {
            //var ddlTransTypes = _ddlServant.GetTransTypes().ToList();
            //var ddlTradeTypes = _ddlServant.GetTradeTypes().ToList();
            //ddlTransTypes.Add(new Option() { Key = "全部", Value = "" });
            //ddlTradeTypes.Add(new Option() { Key = "全部", Value = "" });
            ViewBag.TransTypes = _servant.GetTransTypes();
            ViewBag.TradeTypes = _servant.GetTradeTypes();

            //取得清單
            //GetDetail(model);

            //if (model.Details.Count() > 1000)
            //{
            //    model.Details = model.Details.Take(1000).ToList();
            //    model.Total_Record = 1000;
            //    model.ErrorMessage = "筆數超過1000，請使用匯出功能下載。";
            //}
            //else
            //{
            //    model.ErrorMessage = "";
            //}
            //model.Details = model.Details.Skip((model.Page - 1) * 20).Take(20).ToList();
            return View(_servant.Detail(model));
        }

        /// <summary>
        /// api 取得銀行名稱
        /// </summary>
        /// <param name="branchCode"></param>
        /// <returns></returns>
        [HttpGet]
        public string GetBranchName(string branchCode)
        {
            return _servant.GetBranchName(branchCode);
            //var result = _ddlServant.GetBranch().FirstOrDefault(p => p.Value == branchCode)?.Key;
            //return result ?? "沒有這一個分行";
        }

        /// <summary>
        /// api 取得部門名稱
        /// </summary>
        /// <param name="departmentCode"></param>
        /// <returns></returns>
        [HttpGet]
        public string GetDepartmentName(string departmentCode)
        {
            return _servant.GetDepartmentName(departmentCode);
            //var result = _ddlServant.GetDepartment().FirstOrDefault(p => p.Value == departmentCode)?.Key;
            //return result ?? "沒有這一個部門";
        }

        /// <summary>
        /// 取得子清單
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        //private void GetDetail(_AccountingQueryModel model)
        //{

        //    model.Details = (from p in _servant.GetAccDeprn(
        //                            model.TransType, model.MasterNo, model.AssetNo,
        //                            model.LeaseNo, model.Seq, model.PostSeq, model.BranchCode,
        //                            model.DepartmentCode, model.Account).ToList()
        //                     join tradeDefine in _tradeDefineServer.GetAll().ToList() on p.Master_No equals tradeDefine.Master_No into px
        //                     from tradeDefine in px.DefaultIfEmpty()
        //                     join branch in _ddlServant.GetBranch().ToList() on p.Branch_Code equals branch.Value into py
        //                     from branch in py.DefaultIfEmpty()
        //                     join dep in _ddlServant.GetDepartment().ToList() on p.Department equals dep.Value into pz
        //                     from dep in pz.DefaultIfEmpty()
        //                     select new _AccountQueryDetailModel()
        //                     {
        //                         ID = p.ID,
        //                         Open_Date = p.Open_Date,
        //                         Master_No = p.Master_No,
        //                         BranchDepartment = p.Branch_Code + branch?.Key + "-" + p.Department + dep?.Key,
        //                         GL_Account = p.Account,
        //                         Account_Name = p.Account_Name,
        //                         Amount = p.Amount,
        //                         DB_CR = p.DB_CR,
        //                         Currency = p.Currency,
        //                         Notes = p.Notes,
        //                         Post_Date = p.Post_Date,
        //                         Account_Key = p.Account_Key,
        //                         Trans_Seq = p.Trans_Seq,
        //                         Description = tradeDefine.Master_Text
        //                     }).ToList();
        //    model.Total_Record = model.Details.Count();
        //    model.DBAmount = model.Details.Where(p => p.DB_CR == "D").Sum(p => p.Amount);
        //    model.CRAmount = model.Details.Where(p => p.DB_CR == "C").Sum(p => p.Amount);
        //}

        /// <summary>
        /// 將子項目進行Group
        /// </summary>
        /// <param name="model"></param>
        //private void GroupDetail(_AccountingQueryModel model)
        //{
        //    var result = from p in model.Details
        //                 group p by new
        //                 {
        //                     p.Post_Date,
        //                     p.Master_No,
        //                     p.Description,
        //                     p.BranchDepartment,
        //                     p.GL_Account,
        //                     p.Account_Name,
        //                     p.DB_CR,
        //                     p.Currency,
        //                     p.Notes,
        //                     p.Open_Date

        //                 } into g
        //                 select new _AccountQueryDetailModel()
        //                 {
        //                     ID = 0,
        //                     Post_Date = g.Key.Post_Date,
        //                     Master_No = g.Key.Master_No,
        //                     Description = g.Key.Description,
        //                     BranchDepartment = g.Key.BranchDepartment,
        //                     GL_Account = g.Key.GL_Account,
        //                     Account_Name = g.Key.Account_Name,
        //                     DB_CR = g.Key.DB_CR,
        //                     Currency = g.Key.Currency,
        //                     Notes = g.Key.Notes,
        //                     Open_Date = g.Key.Open_Date,
        //                     Amount = g.Sum(x => x.Amount)
        //                 };
        //    model.Details = result.ToList();
        //}

        /// <summary>
        /// 匯出群組後的excel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ExportGroupExcel(_AccountingQueryModel model)
        {

            //Load Data
            //GetDetail(model);
            //GroupDetail(model);
            string fName;
            byte[] result;
            GenerateCSV(_servant.ExportGroupExcel(model).Details, out fName, out result);

            return File(result, "text/csv", fName);
        }

        /// <summary>
        /// 匯出子項目清單
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExportDetailExcel(IEnumerable<_ExportModel> models)
        {
            //var details = new List<_AccountQueryDetailModel>();
            //foreach (var model in models.Where(p => p.DetailChecked))
            //{
            //    var accountingModel = new _AccountingQueryModel()
            //    {
            //        Account = model.GL_Account,
            //        AssetNo = model.AssetNo,
            //        BranchCode = model.BranchCode,
            //        DepartmentCode = model.DepartmentCode,
            //        LeaseNo = model.LeaseNo,
            //        TransType = model.TransType,
            //        MasterNo = model.Master_No,
            //        Seq = model.Seq,
            //        TransSeq = model.TransSeq
            //    };
            //    GetDetail(accountingModel);
            //    details.AddRange(accountingModel.Details);
            //}
            string fName = "";
            byte[] result;
            GenerateCSV(_servant.ExportDetailExcel(models), out fName, out result);
            return File(result, "text/csv", fName);
        }

        /// <summary>
        /// 批次刪除
        /// </summary>
        /// <param name="id"></param>
        /// <param name="deleteReason"></param>
        /// <returns></returns>
        [HttpPost]
        public string DeleteBatch(int[] id, string deleteReason)
        {
            //if (string.IsNullOrEmpty(deleteReason))
            //{
            //    Response.StatusCode = 400;
            //    return "沒有輸入刪除原因。";
            //}
            //foreach (var item in id)
            //{
            //    var model = _servant.GetAccDeprnById(item);
            //    if (model != null)
            //    {
            //        _servant.DeleteDeprn(item, deleteReason);
            //    }
            //}
            var _userInfo = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            string _message = _servant.DeleteBatch(id, deleteReason, _userInfo.ID);
            if (_message != null && _message != "")
            {
                Response.StatusCode = 400;
            }
            else
            {
                Response.StatusCode = 200;
            }
            return _message;
        }

        /// <summary>
        /// 產生exccel檔案
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fName"></param>
        /// <param name="result"></param>
        private void GenerateCSV(List<_AccountQueryDetailModel> model, out string fName, out byte[] result)
        {
            var sb = new StringBuilder();
            var properties = typeof(_AccountQueryDetailModel).GetProperties();

            foreach (var property in properties)
            {
                if (property.Name != "ID")
                {
                    var customAttributeNamedArguments = property.CustomAttributes.First(p => p.AttributeType == typeof(DisplayAttribute))
                        .NamedArguments;
                    if (customAttributeNamedArguments != null)
                        sb.Append(customAttributeNamedArguments[0].TypedValue.Value + ",");
                }
            }
            sb.Append("\r\n");

            foreach (var item in model)
            {
                foreach (var property in properties)
                {
                    if (property.Name == "ID") continue;
                    if (property.Name.Contains("Date"))
                    {
                        sb.Append(((DateTime)property.GetValue(item)).ToString("yyyy/MM/dd") + ",");
                    }
                    else if (property.Name.Contains("DB_CR"))
                    {
                        var value = property.GetValue(item).ToString();
                        sb.Append(value == "D" ? "借," : "貸,");
                    }
                    else
                    {
                        sb.Append(property.GetValue(item) + ",");
                    }
                }
                sb.Append("\r\n");
            }

            fName = $"折舊明細表-{DateTime.Now:s}.csv";
            var fileContents = Encoding.UTF8.GetBytes(sb.ToString());
            result = Encoding.UTF8.GetPreamble().Concat(fileContents).ToArray();
        }
    }
}