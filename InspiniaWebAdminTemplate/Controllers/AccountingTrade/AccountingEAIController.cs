﻿using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.AccountingEAI;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [ExceptionHandle]
    public class AccountingEAIController : Controller
    {
        public ActionResult Index()
        {
            return View( Servant.Index() );
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add()
        {
            Servant.Add();
            return RedirectToAction("Index");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Remove()
        {
            Servant.Remove();
            return RedirectToAction("Index");
        }

        private LocalAccountingEAIServant Servant = new LocalAccountingEAIServant();
    }
}