﻿using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.Accounting;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Library.Servant.Servant.Accounting.Models;
using InspiniaWebAdminTemplate.Models.AccountingQuery;
using Library.Utility;
using Library.Servant.Servant.AccountingQueryDeprn.Model;
using System.Collections.Generic;

/// <summary>
///  Ricky 遺留程式碼,整理中>< .......By Sean
/// </summary>
namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [ExceptionHandle]
    public class AccountingQueryController : Controller
    {
        public ActionResult Index(_AccountingQueryModel search)
        {
            var result = _servant.Index(search);

            return View( new IndexViewModel {

                TradeTypes = result.TradeTypes,
                TransTypes = result.TransTypes,
                Page = result.Page,
                Search = search,
            } );
        }

        public ActionResult ExportExcel(_AccountingQueryModel search)
        {
            var index_model = _servant.Index(search);
            search.Details = MapProperty.MapAll<_AccountQueryDetailModel, IndexItem>(index_model.Page.Items).ToList();

            return File( search.Details.ToCSV(), "text/csv",  $"帳務明細表-{DateTime.Now:s}.csv");
        }

        /// <summary>
        /// 本月明細資料
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult MonthDetail(int id)
        {
            GetOptions();
            return View( _servant.MonthDetail(id) );
        }

        [HttpPost]
        public void DeleteAccMonth(int id, string message)
        {
            if ( !string.IsNullOrEmpty(message) )
                _servant.DeleteAccMonth(id, message);
        }

        [HttpPost]
        public ActionResult DeleteBatch(int[] id, string deleteReason)
        {
            if (string.IsNullOrEmpty(deleteReason))
                return RedirectToAction("Index");

            foreach (var item in id)
                _servant.DeleteAccMonth(item, deleteReason);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// 其它月月明細資料
        /// </summary>
        /// <param name="id"></param>
        /// <param name="optionType"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OtherDetail(int id)
        {
            GetOptions();
            return View(_servant.GetAccDetailById(id));
        }


        /// <summary>
        /// 帳務調整
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EditAccount(int id)
        {
            GetOptions();
            return View( _servant.MonthDetail(id) );
        }

        /// <summary>
        /// 帳務調整
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult EditAccount(UpdateModel model)
        {
            try
            {
                _servant.Update(model);
            }
            catch(Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            return EditAccount(model._ID);
        }

        /// <summary>
        /// 取得幣別清單
        /// </summary>
        /// <param name="bookType"></param>
        /// <returns></returns>
        [HttpGet]
        public string GetCurrency(string bookType)
        {
            return _servant.GetCurrencyByBookType(bookType);
        }

        [NonAction]
        private void GetOptions()
        {
            ViewBag.branchs = _ddlServant.Get("branchs");
            ViewBag.departments = _ddlServant.Get("departments");
            ViewBag.bookTypes = _ddlServant.Get("bookTypes");
            ViewBag.currencies = _ddlServant.Get("currencies");
            ViewBag.transTypes = _ddlServant.Get("transTypes");
            ViewBag.tradeTypes = _ddlServant.Get("tradeTypes");
        }

        private readonly LocalAccountingQueryServant _servant = new LocalAccountingQueryServant();
        private readonly LocalAccountingDDLServant _ddlServant = new LocalAccountingDDLServant();
    }


    static class CsvExtension
    {
        public static byte[] ToCSV(this List<_AccountQueryDetailModel> objs)
        {
            StringBuilder sb = new StringBuilder();
            sb.CollectHeader();
            sb.CollectBody(objs);
                                    
            return Encoding.UTF8.GetPreamble().Concat( Encoding.UTF8.GetBytes(sb.ToString()) ).ToArray();
        }

        private static void CollectHeader(this StringBuilder sb)
        {
            var properties = typeof(_AccountQueryDetailModel).GetProperties();
            properties.ToList().ForEach(property => {

                if (property.Name != "ID")
                    sb.Append(property.CustomAttributes.First(p => p.AttributeType == typeof(DisplayAttribute)).NamedArguments[0].TypedValue.Value + ",");
            });
            sb.Append("\r\n");
        }

        private static void CollectBody(this StringBuilder sb, List<_AccountQueryDetailModel> objs)
        {
            var properties = typeof(_AccountQueryDetailModel).GetProperties();
            objs.ForEach(item => {

                properties.ToList().ForEach(property =>
                {
                    if (property.Name == "ID")
                    {
                        return;
                    }
                    else if (property.Name.Contains("Date"))
                    {
                        if (property.GetValue(item) != null)
                            sb.Append(((DateTime)property.GetValue(item)).ToString("yyyy/MM/dd") + ",");
                    }
                    else if (property.Name.Contains("DB_CR"))
                    {
                        var value = property.GetValue(item).ToString();
                        sb.Append(value == "D" ? "借," : "貸,");
                    }
                    else
                    {
                        sb.Append(property.GetValue(item) + ",");
                    }
                });
                sb.Append("\r\n");
            });
        }
    }
}
