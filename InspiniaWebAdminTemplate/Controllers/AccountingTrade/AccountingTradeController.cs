﻿using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.AccountingTrade;
using Library.Servant.Servant.AccountingTrade.Models;
using System.Web.Mvc;
using System.Linq;

namespace InspiniaWebAdminTemplate.Controllers.Accounting
{
    [Login]
    [ExceptionHandle]
    public class AccountingTradeController : Controller
    {
        public ActionResult Index()
        {
            return View( Servant.Options() );
        }

        [HttpPost]
        public ActionResult Search(SearchModel model)
        {
            if (model.IsValidate())
            { 
                var result = Servant.Index(model);

                if (result.Items.Any())
                    return PartialView(result.Items.First());
                else
                    return Content("不存在");
            }
            return new EmptyResult();
        }

        private LocalAccountingTradeServant Servant = new LocalAccountingTradeServant();
    }
}