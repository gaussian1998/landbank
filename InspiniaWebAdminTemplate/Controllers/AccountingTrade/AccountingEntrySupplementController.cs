﻿using InspiniaWebAdminTemplate.Attributes;
using Library.Entity.Edmx;
using Library.Servant.Servant.Accounting;
using System;
using System.Linq;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.AccountingTrade
{
    [Login]
    [ExceptionHandle]
    public class AccountingEntrySupplementController : Controller
    {
        public ActionResult Index()
        {
            return View( new AS_GL_Acc_Months()
            {
                Open_Date = DateTime.Today,
                Branch_Code = "001",
                Book_Type = "NTD_RE_IFRS",
                Currency = "NTD",
                Trade_Type = "01",
                Trans_Type = "1"
            });
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(AS_GL_Acc_Months model)
        {
            var errorMessage = "";
            if (ddlServant.GetBranch().All(p => p.Value != model.Branch_Code))
                errorMessage += "找不到銀行代碼 , ";
            if (accountServant.GetAccountInfo(model.Account) == null)
                errorMessage += "找不到科子細目編號 , ";
            if (string.IsNullOrEmpty(model.Notes) || string.IsNullOrEmpty(model.Updated_Notes))
                errorMessage += "請輸入備註及補輸原因 , ";

            ViewBag.ErrorMessage = errorMessage;
            if (ModelState.IsValid && errorMessage == "")
            {
                servant.AddAccountingAccMonthItem(model);
                return RedirectToAction("Index");
            }
            model.Open_Date = DateTime.Today;
            return View(model);
        }

        /// <summary>
        /// 取得code table資料
        /// </summary>
        /// <param name="class"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetCodeTable(string @class)
        {
            return Json( ddlServant.Get(@class), JsonRequestBehavior.AllowGet);
        }

        private readonly LocalAccountingQueryServant servant = new LocalAccountingQueryServant();
        private readonly LocalAccountingDDLServant ddlServant = new LocalAccountingDDLServant();
        private readonly LocalAccountingSubjectServant accountServant = new LocalAccountingSubjectServant();
    }
}