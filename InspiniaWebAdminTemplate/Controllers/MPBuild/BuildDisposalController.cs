﻿using Library.Servant.Servant.Build;
using Library.Servant.Servant.Land;
using Library.Servant.Servants.AbstractFactory;
using InspiniaWebAdminTemplate.Models.Build;
using Library.Servant.Servant.Build.BuildModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using Library.Servant.Servant.UserInformation.Models;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.ApprovalTodo;
using InspiniaWebAdminTemplate.Attributes;
using Library.Servant.Servant.MPBuild.BuildModels;
using Library.Servant.Repository.Build;

namespace InspiniaWebAdminTemplate.Controllers.MPBuild
{
    /// <summary>
    /// 房屋處分
    /// </summary>
    public class BuildDisposalController : Controller
    {
        private IBuildServant BuildServant = ServantAbstractFactory.Build();
        private ILandServant LandServant = ServantAbstractFactory.Land();
        private string _Type = BuildGeneral.GetFormCode("BuildDisposal");
        int PageSize = 10;

        [Login]
        [FunctionAuthorize(ID = "BuildDisposal")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            SearchModel condition = new SearchModel()
            {
                Transaction_Type = _Type
            };
            IndexModel IndexQuery = this.BuildServant.Index(condition);

            IndexViewModel vm = new IndexViewModel()
            {
                condition = BuildHelper.ToSearchViewModel(IndexQuery.condition),
                Items = IndexQuery.Items.Select(o => BuildHelper.ToBuildTRXHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(BuildHelper.GetBuildView("Index"), vm);
        }

        /// <summary>
        /// 新增單據
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildDisposal")]
        [ExceptionHandle]
        public ActionResult New()
        {
            BuildViewModel vm = new BuildViewModel()
            {
                TRXHeader = new BuildTRXHeaderViewModel()
                {
                    Form_Number = this.BuildServant.CreateBuildFormNumber(_Type),
                    Transaction_Status = "0", //事務處理狀態
                    Transaction_Datetime_Entered = DateTime.Now
                },
                Assets = new List<AssetViewModel>()
            };
            return View(BuildHelper.GetBuildView("Build"), vm);
        }

        /// <summary>
        /// 編輯單據畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildDisposal")]
        [ExceptionHandle]
        public ActionResult Edit(string id, string signmode)
        {
            var Detail = this.BuildServant.GetDetail(id);

            var vm = new BuildViewModel()
            {
                TRXHeader = BuildHelper.ToBuildTRXHeaderViewModel(Detail.TRXHeader)
            };

            if (Detail.Assets != null)
                vm.Assets = Detail.Assets.Select(m => BuildHelper.ToAssetViewModel(m));
            else
                vm.Assets = new List<AssetViewModel>();

            if (Detail.Retire != null)
                vm.Retire = Detail.Retire.Select(m => BuildHelper.ToBuildRetireViewModel(m));
            else
                vm.Retire = new List<BuildRetireViewModel>();

            if (!string.IsNullOrEmpty(signmode))
            {
                ViewBag.SignMode = true;
            }
            TempData["UseAsset"] = vm.Assets == null ? new List<string>() : vm.Assets.Select(m => m.Asset_Number).ToList(); //已選擇的資產
            return View(BuildHelper.GetBuildView("Build"), vm);
        }

        /// <summary>
        /// 資產搜尋頁面
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildDisposal")]
        [ExceptionHandle]
        public ActionResult Asset_Search(BuildTRXHeaderViewModel header)
        {
            TempData["Header"] = header;
            if (!String.IsNullOrEmpty(header.Office_Branch))
            {
                header.Office_Branch = header.Office_Branch.Split('.')[1];
            }
            header.Transaction_Type = _Type;
            AssetsIndexViewModel vm = new AssetsIndexViewModel()
            {
                condition = new AssetsSearchViewModel()
                {
                    Page = 1,
                    PageSize = PageSize
                },
                Items = new List<AssetViewModel>(),
            };


            return View(BuildHelper.GetBuildView("AssetSearch"),vm);
        }

        /// <summary>
        /// 資產搜尋
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildDisposal")]
        [ExceptionHandle]
        public ActionResult Asset_Query(AssetsSearchViewModel condition)
        {
            TempData["QueryCondition"] = condition;
            List<string> UseAsset = TempData["UseAsset"] != null ? TempData["UseAsset"] as List<string> : new List<string>();

            condition.PageSize = PageSize;
            condition.Page = 1;
            condition.UseAssetNumber = UseAsset;

            var data = this.BuildServant.SearchAssets(BuildHelper.ToAssetsSearchModel(condition));
            AssetsIndexViewModel vm = new AssetsIndexViewModel()
            {
                Items = data.Items.Select(o => BuildHelper.ToAssetViewModel(o)),
                condition = condition,
                TotalAmount = data.TotalAmount,
            };
            var Header = (TempData["Header"] != null) ? TempData["Header"] : null;
            TempData["Header"] = Header;
            return View(BuildHelper.GetBuildView("AssetSearch"), vm);
        }

        /// <summary>
        /// 將資產新增至表單
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "BuildDisposal")]
        [ExceptionHandle]
        public ActionResult AssetsToBuild(string AssetsList, AssetsIndexViewModel model)
        {
            BuildTRXHeaderViewModel header = null;
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            if (TempData["Header"] != null)
            {
                header = TempData["Header"] as BuildTRXHeaderViewModel;
                header.Office_Branch = header.Office_Branch ?? string.Empty;
                header.Book_Type_Code = "NTD_RE_IFRS";
                header.Transaction_Type = (header.Transaction_Type == null) ? _Type : header.Transaction_Type;
                header.Description = header.Description ?? string.Empty;
                header.Amotized_Adjustment_Flag = header.Amotized_Adjustment_Flag ?? "0";  //會計註記 暫時設定0
                header.Auto_Post = header.Auto_Post ?? 1; //拋轉帳務
                header.Created_By = user.ID;
                header.Created_By_Name = user.Name;
                header.Create_Time = DateTime.Now;
                header.Last_UpDatetimed_Time = null;
                header.Last_UpDatetimed_By = null;
                header.Last_UpDatetimed_By_Name = string.Empty;
            }

            foreach (var item in model.BuildRetireViewModel)
            {
                item.Reason_Code = item.Reason_Code ?? string.Empty;
                item.Created_By = user.ID;
                item.Created_By_Name = user.Name;
                item.Create_Time = DateTime.Now;
                item.Last_UpDatetimed_Time = null;
                item.Last_UpDatetimed_By = null;
                item.Last_UpDatetimed_By_Name = string.Empty;
            }

            BuildModel Build = new BuildModel()
            {
                TRXHeader = BuildHelper.ToBuildTRXHeaderModel(header),
                Header = new BuildHeaderModel(),
                Assets = AssetsList.Split(',').Select(o => BuildHelper.ToAssetModel(new AssetViewModel()
                {
                    Asset_Number = o
                })),
                Retire= model.BuildRetireViewModel.Select(m=> BuildHelper.ToBuildRetireModel(m))
            };

            var Result = this.BuildServant.CreateBuild(Build);
            TempData["message"] = "資產導入成功!";
            return RedirectToAction("Edit", new { id = Result.Form_Number });
        }


        /// <summary>
        /// 土地搜尋頁面
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "BuildDisposal")]
        [ExceptionHandle]
        [HttpPost]
        public ActionResult Asset_LandSearch(AssetViewModel asset)
        {
            var Header = (TempData["Header"] != null) ? TempData["Header"] : null;
            TempData["Header"] = Header;
            if (!string.IsNullOrEmpty(asset.BuildDetailsJsonStr))
            {
                asset.BuildDetails = JsonConvert.DeserializeObject<List<AssetDetailViewModel>>(asset.BuildDetailsJsonStr);
            }
            if (!string.IsNullOrEmpty(asset.BuildLandJsonStr))
            {
                asset.BuildLands = JsonConvert.DeserializeObject<List<BuildLandViewModel>>(asset.BuildLandJsonStr);
            }
            TempData["Asset"] = asset;
            return View(BuildHelper.GetBuildView("BuildLandSearch"));
        }

        /// <summary>
        /// 土地搜尋
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildDisposal")]
        [ExceptionHandle]
        public ActionResult Asset_LandQuery(InspiniaWebAdminTemplate.Models.Land.AssetsSearchViewModel condition)
        {
            var data = this.LandServant.SearchAssets(InspiniaWebAdminTemplate.Models.Land.LandHelper.ToAssetsSearchModel(condition));
            InspiniaWebAdminTemplate.Models.Land.AssetsIndexViewModel vm = new InspiniaWebAdminTemplate.Models.Land.AssetsIndexViewModel()
            {
                Items = data.Items.Select(o => InspiniaWebAdminTemplate.Models.Land.LandHelper.ToAssetLandViewModel(o)),
                condition = condition,
                TotalAmount = data.TotalAmount
            };

            //取的表頭及房屋物件
            var Header = (TempData["Header"] != null) ? TempData["Header"] : null;
            TempData["Header"] = Header;
            var Asset = (TempData["Asset"] != null) ? TempData["Asset"] : null;
            TempData["Asset"] = Asset;

            return View(BuildHelper.GetBuildView("BuildLandSearch"), vm);
        }


        //新增土地關聯至房屋
        [Login]
        [FunctionAuthorize(ID = "BuildDisposal")]
        [ExceptionHandle]
        [HttpPost]
        public ActionResult Asset_AddLandToBuild(string LandList)
        {
            BuildHeaderViewModel header = null;
            if (TempData["Header"] != null)
            {
                header = TempData["Header"] as BuildHeaderViewModel;
                header.Transaction_Type = (header.Transaction_Type == null) ? _Type : header.Transaction_Type;
                header.Transaction_Status = "";
                header.Description = (header.Description == null) ? "" : header.Description;
                header.Transaction_Datetime_Entered = DateTime.Now;
                header.Created_By = "";
                header.Create_Time = DateTime.Now;
                header.Last_Updated_By = "";
            }
            AssetViewModel asset = null;
            if (TempData["Asset"] != null)
            {
                asset = TempData["Asset"] as AssetViewModel;
            }

            BuildModel Build = new BuildModel()
            {
                Header = BuildHelper.ToBuildHeaderModel(header)
            };

            AssetModel Asset = BuildHelper.ToAssetModel(asset);
            if (LandList.Split(',').Length > 0)
            {
                Asset.BuildLands = LandList.Split(',').Select(o => new BuildLandModel()
                {
                    ID = Convert.ToInt32(o)
                });
            }

            Build.Assets = new List<AssetModel>() { Asset };
            AssetHandleResult Result = this.BuildServant.Create_Asset(Build);

            TempData["message"] = "土地導入成功!";
            //回資產編輯頁
            return RedirectToAction("Asset_Edit", new { id = Result.AssetID });
        }

        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildDisposal")]
        [ExceptionHandle]
        public ActionResult Asset_Edit(int id)
        {
            BuildTRXHeaderModel header;
            var vm = this.BuildServant.GetAssetDetail(id, out header);
            TempData["Header"] = (header != null) ? BuildHelper.ToBuildTRXHeaderViewModel(header) : null;
            ViewBag.Title = "編輯資產";
            ViewBag.FunctionFrom = (TempData["FunctionFrom"] != null) ? Convert.ToString(TempData["FunctionFrom"]) : "";
            return View(BuildHelper.GetBuildView("AssetRetrieEdit"), BuildHelper.ToAssetViewModel(vm));
        }


        /// <summary>
        /// 編輯資產
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildDisposal")]
        [ExceptionHandle]
        public ActionResult Asset_Update(AssetViewModel asset)
        {
            BuildHeaderViewModel header = TempData["Header"] as BuildHeaderViewModel;
            if (header.Description == null)
            {
                header.Description = "";
            }
            asset.Last_Updated_By = null;
            asset.Last_Updated_Time = DateTime.Now;
            if (!string.IsNullOrEmpty(asset.BuildDetailsJsonStr))
            {
                asset.BuildDetails = JsonConvert.DeserializeObject<List<AssetDetailViewModel>>(asset.BuildDetailsJsonStr);
            }
            if (!string.IsNullOrEmpty(asset.BuildLandJsonStr))
            {
                asset.BuildLands = JsonConvert.DeserializeObject<List<BuildLandViewModel>>(asset.BuildLandJsonStr);
            }
            var Result = this.BuildServant.Create_Asset(new BuildModel()
            {
                Header = BuildHelper.ToBuildHeaderModel(header),
                Assets = new List<AssetViewModel>() { asset }.Select(o => BuildHelper.ToAssetModel(o))
            });
            TempData["message"] = "房屋編輯成功!";
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }

        /// <summary>
        /// 跑簽核流程
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildDisposal")]
        [ExceptionHandle]
        public ActionResult CreateFlow(BuildTRXHeaderViewModel header)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            int DocID;
            string message = "提交成功!";
            //都先跑總行
            if (BuildServant.GetDocID(_Type.Substring(0, _Type.Length - 1) + "H", out DocID))
            {
                this.BuildServant.UpdateFlowStatus(header.Form_Number, "1");
                LocalApprovalTodoServant.Create(user.ID, DocID, header.Form_Number, "BuildDisposal");
                LocalApprovalTodoServant.Start(user.ID, header.Form_Number, header.Form_Number);
            }
            else
            {
                message = "查無此表單!!";
            }
            TempData["message"] = message;
            return RedirectToAction("Edit", new { id = header.Form_Number });
        }

        /// <summary>
        /// 刪除表頭單據以及資產
        /// </summary>
        /// <param name="id">AS_TRX_Headers.Form_Number</param>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildMaintain")]
        [ExceptionHandle]
        public ActionResult HeaderAndAsset_Del(string id)
        {
            bool result = this.BuildServant.HeaderAndAsset_Del(id, _Type);

            if (result)
                TempData["message"] = "資料刪除成功!";
            else
                TempData["message"] = "資料刪除失敗!";

            return RedirectToAction("Index");
        }

        /// <summary>
        /// 刪除資產
        /// </summary>
        /// <param name="id">AS_Assets_Build_Record.ID</param>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildMaintain")]
        [ExceptionHandle]
        public ActionResult Asset_Del(int id)
        {
            StringResult Result = this.BuildServant.Asset_Del(id, _Type);

            if (Result.IsSuccess)
            {
                TempData["message"] = "資產刪除成功!";
                return RedirectToAction("Edit", new { id = Result.StrResult });
            }
            TempData["message"] = "資產刪除失敗!";
            return RedirectToAction("Index");

        }

        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildMaintain")]
        [ExceptionHandle]
        public ActionResult EditRetireByAsset(AssetViewModel model)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            BuildTRXHeaderViewModel header = TempData["Header"] as BuildTRXHeaderViewModel; //check
            AssetViewModel AssetVM = TempData["AssetVM"] as AssetViewModel;
            if (header.Description == null)
            {
                header.Description = "";
            }
            model.Last_Updated_By = user.ID;
            model.Last_Updated_Time = DateTime.Now;
            
            //原始資料不在前端bind
            BuildRetireViewModel data = new BuildRetireViewModel();
            data = AssetVM.BuildRetire;
            data.Reason_Code = model.BuildRetire.Reason_Code;
            data.Retire_Cost = model.BuildRetire.Retire_Cost;
            data.Sell_Amount = model.BuildRetire.Sell_Amount;
            data.Sell_Cost = model.BuildRetire.Sell_Cost;
            data.Reval_Adjustment_Amount = model.BuildRetire.Reval_Adjustment_Amount;
            data.Reval_Reserve = model.BuildRetire.Reval_Reserve;
            data.Last_UpDatetimed_By = user.ID;
            data.Last_UpDatetimed_By_Name = user.Name;
            data.Last_UpDatetimed_Time = DateTime.Now;

            var Result = this.BuildServant.Create_Asset(new BuildModel()
            {
                Header = new BuildHeaderModel(),
                TRXHeader = BuildHelper.ToBuildTRXHeaderModel(header),
                Assets = new List<AssetViewModel>() { AssetVM }.Select(o => BuildHelper.ToAssetModel(o)),
                Retire = new List<BuildRetireViewModel>() { data }.Select(o => BuildHelper.ToBuildRetireModel(o)),
            });
            TempData["message"] = "房屋編輯成功!";
            return RedirectToAction("Edit", new { id = Result.Form_Number });
        }

        [Login]
        [FunctionAuthorize(ID = "BuildMaintain")]
        [ExceptionHandle]
        public ActionResult AssetsUpdatePage(int page)
        {
            AssetsSearchViewModel condition = TempData["QueryCondition"] as AssetsSearchViewModel;

            condition.PageSize = PageSize;
            condition.Page = page;

            var data = this.BuildServant.SearchAssets(BuildHelper.ToAssetsSearchModel(condition));
            AssetsIndexViewModel vm = new AssetsIndexViewModel()
            {
                Items = data.Items.Select(o => BuildHelper.ToAssetViewModel(o)),
                condition = condition,
                TotalAmount = data.TotalAmount,
            };
            var Header = (TempData["Header"] != null) ? TempData["Header"] : null;
            TempData["Header"] = Header;
            return View(BuildHelper.GetBuildView("AssetSearch"), vm);
        }
    }
}