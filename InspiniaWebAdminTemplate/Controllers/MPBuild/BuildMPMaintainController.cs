﻿using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.Build;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Repository.Build;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.Build;
using Library.Servant.Servant.Build.BuildModels;
using Library.Servant.Servant.Land;
using Library.Servant.Servant.MPBuild.BuildModels;
using Library.Servant.Servant.UserInformation.Models;
using Library.Servant.Servants.AbstractFactory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Build
{
    public class BuildMPMaintainController : Controller
    {
        private IBuildServant BuildServant = ServantAbstractFactory.Build();
        private ILandServant LandServant = ServantAbstractFactory.Land();
        private string _Type = BuildGeneral.GetFormCode("BuildMPMaintain");
        int PageSize = 10;
        //private string Type = "C090";

        [Login]
        [FunctionAuthorize(ID = "BuildMPMaintain")]
        [ExceptionHandle]
        // GET: BuildMPAdded
        public ActionResult Index()
        {
            SearchModel condition = new SearchModel()
            {
                Transaction_Type = _Type
            };
            IndexModel IndexQuery = this.BuildServant.Index(condition);

            IndexViewModel vm = new IndexViewModel()
            {
                condition = BuildHelper.ToSearchViewModel(IndexQuery.condition),
                Items = IndexQuery.Items.Select(o => BuildHelper.ToBuildTRXHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(BuildHelper.GetBuildView("Index"), vm);
        }

        [Login]
        [FunctionAuthorize(ID = "BuildMPMaintain")]
        [ExceptionHandle]
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexQuery(SearchViewModel condition)
        {
            condition.Transaction_Type = _Type;
            condition.Page = 1;
            condition.PageSize = PageSize;
            IndexModel IndexQuery = this.BuildServant.Index(BuildHelper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => BuildHelper.ToBuildTRXHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(BuildHelper.GetBuildView("Index"), vm);
        }

        /// <summary>
        /// 新增單據
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildMPMaintain")]
        [ExceptionHandle]
        public ActionResult New()
        {
            BuildMPViewModel vm = new BuildMPViewModel()
            {
                TRXHeader = new BuildTRXHeaderViewModel()
                {
                    Form_Number = this.BuildServant.CreateBuildFormNumber(_Type),
                    Transaction_Status = "0", //事務處理狀態
                    Transaction_Datetime_Entered = DateTime.Now
                },
                Assets = new List<AssetMPViewModel>()
            };
            return View(BuildHelper.GetBuildView("BuildMP"), vm);
        }

        /// <summary>
        /// 資產搜尋頁面
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildMPMaintain")]
        [ExceptionHandle]
        public ActionResult Asset_Search(BuildTRXHeaderViewModel header)
        {
            TempData["Header"] = header;
            if (!String.IsNullOrEmpty(header.Office_Branch))
            {
                header.Office_Branch = header.Office_Branch.Split('.')[1];
            }
            header.Transaction_Type = _Type;
            AssetsMPIndexViewModel vm = new AssetsMPIndexViewModel()
            {
                condition = new AssetsMPSearchViewModel()
                {
                    Page = 1,
                    PageSize = PageSize
                },
                Items = new List<AssetMPViewModel>()
            };
            return View(BuildHelper.GetBuildView("AssetMPSearch"), vm);
        }

        /// <summary>
        /// 資產搜尋
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildMPMaintain")]
        [ExceptionHandle]
        public ActionResult Asset_Query(AssetsMPSearchViewModel condition)
        {
            TempData["QueryCondition"] = condition;
            List<string> UseAsset = TempData["UseAsset"] != null ? TempData["UseAsset"] as List<string> : new List<string>();
            condition.PageSize = PageSize;
            condition.Page = 1;
            condition.UseAssetNumber = UseAsset;

            var data = this.BuildServant.SearchAssetsMP(BuildHelper.ToAssetsMPSearchModel(condition));
            AssetsMPIndexViewModel vm = new AssetsMPIndexViewModel()
            {
                Items = data.Items.Select(o => BuildHelper.ToAssetMPViewModel(o)),
                condition = condition,
                TotalAmount = data.TotalAmount
            };

            var Header = (TempData["Header"] != null) ? TempData["Header"] : null;
            TempData["Header"] = Header;
            return View(BuildHelper.GetBuildView("AssetMPSearch"), vm);
        }

        /// <summary>
        /// 將資產新增至表單
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildMPMaintain")]
        [ExceptionHandle]
        public ActionResult AssetsToBuildMP(string AssetsList)
        {
            BuildTRXHeaderViewModel header = null;
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            if (TempData["Header"] != null)
            {
                header = TempData["Header"] as BuildTRXHeaderViewModel;
                //header.Transaction_Type = (header.Transaction_Type == null) ? Type : header.Transaction_Type;
                //header.Transaction_Status = "";
                //header.Transaction_Datetime_Entered = DateTime.Now;
                header.Office_Branch = header.Office_Branch ?? string.Empty;
                header.Description = header.Description ?? string.Empty;
                header.Amotized_Adjustment_Flag = header.Amotized_Adjustment_Flag ?? "0";  //會計註記 暫時設定0
                header.Auto_Post = header.Auto_Post ?? 1; //拋轉帳務
                header.Created_By = user.ID;
                header.Created_By_Name = user.Name;
                header.Create_Time = DateTime.Now;
                header.Last_UpDatetimed_Time = null;
                header.Last_UpDatetimed_By = null;
                header.Last_UpDatetimed_By_Name = string.Empty;
            }

            BuildMPModel BuildMP = new BuildMPModel()
            {
                Header = new BuildHeaderModel(),
                TRXHeader = BuildHelper.ToBuildTRXHeaderModel(header),
                Assets = AssetsList.Split(',').Select(o => BuildHelper.ToAssetMPModel(new AssetMPViewModel()
                {
                    Asset_Number = o
                }))
            };

            var Result = this.BuildServant.CreateBuildMP(BuildMP);
            TempData["message"] = "資產導入成功!";
            return RedirectToAction("Edit", new { id = Result.Form_Number });
        }

        /// <summary>
        /// 編輯單據畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildMPMaintain")]
        [ExceptionHandle]
        public ActionResult Edit(string id, string signmode)
        {
            var Detail = this.BuildServant.GetMPDetail(id);

            var vm = new BuildMPViewModel()
            {
                TRXHeader = BuildHelper.ToBuildTRXHeaderViewModel(Detail.TRXHeader),
            };

            if (Detail.Assets != null)
                vm.Assets = Detail.Assets.Select(m => BuildHelper.ToAssetMPViewModel(m));
            else
                vm.Assets = new List<AssetMPViewModel>();

            if (!string.IsNullOrEmpty(signmode))
            {
                ViewBag.SignMode = true;
            }

            TempData["UseAsset"] = vm.Assets == null ? new List<string>() : vm.Assets.Select(m => m.Asset_Number).ToList(); //已選擇的資產
            return View(BuildHelper.GetBuildView("BuildMP"), vm);
        }

        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildMPMaintain")]
        [ExceptionHandle]
        public ActionResult Asset_Edit(int id)
        {
            BuildTRXHeaderModel header;
            var vm = this.BuildServant.GetAssetMPDetail(id, out header);
            TempData["Header"] = (header != null) ? BuildHelper.ToBuildTRXHeaderViewModel(header) : null;
            ViewBag.Title = "重大組成明細-編輯";
            ViewBag.FunctionFrom = (TempData["FunctionFrom"] != null) ? Convert.ToString(TempData["FunctionFrom"]) : "";
            return View(BuildHelper.GetBuildView("AssetMP"), BuildHelper.ToAssetMPViewModel(vm));
        }


        /// <summary>
        /// 編輯資產
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildMPMaintain")]
        [ExceptionHandle]
        public ActionResult Asset_Update(AssetMPViewModel asset)
        {
            BuildTRXHeaderViewModel header = TempData["Header"] as BuildTRXHeaderViewModel;
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            header.Description = header.Description ?? string.Empty;
            header.Last_UpDatetimed_By = user.ID;
            header.Last_UpDatetimed_By_Name = user.Name;
            header.Last_UpDatetimed_Time = DateTime.Now;

            asset.Last_UpDatetimed_By = user.ID;
            asset.Last_UpDatetimed_By_Name = user.Name;
            asset.Last_UpDatetimed_Time = DateTime.Now;
            asset.NeedRecord = true;
            var Result = this.BuildServant.Create_AssetMP(new BuildMPModel()
            {
                Header = new BuildHeaderModel(),
                TRXHeader = BuildHelper.ToBuildTRXHeaderModel(header),
                Assets = new List<AssetMPViewModel>() { asset }.Select(o => BuildHelper.ToAssetMPModel(o))
            });
            TempData["message"] = "重大組成明細編輯成功!";
            return RedirectToAction("Edit", new { id = Result.Form_Number });
        }


        /// <summary>
        /// 跑簽核流程
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildMPMaintain")]
        [ExceptionHandle]
        public ActionResult CreateFlow(BuildTRXHeaderViewModel header)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            int DocID;
            string message = "提交成功!";
            if (BuildServant.GetDocID(_Type.Substring(0, _Type.Length - 1) + "H", out DocID))
            {
                this.BuildServant.UpdateFlowStatus(header.Form_Number, "1");
                LocalApprovalTodoServant.Create(user.ID, DocID, header.Form_Number, "BuildMPMaintain");
                LocalApprovalTodoServant.Start(user.ID, header.Form_Number, header.Form_Number);
            }
            else
            {
                message = "查無此表單!!";
            }
            TempData["message"] = message;
            return RedirectToAction("Edit", new { id = header.Form_Number });
        }
        /// <summary>
        /// 刪除表頭單據以及資產
        /// </summary>
        /// <param name="id">AS_TRX_Headers.Form_Number</param>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildMPMaintain")]
        [ExceptionHandle]
        public ActionResult HeaderAndAsset_Del(string id)
        {
            bool result = this.BuildServant.HeaderAndMPAsset_Del(id);

            if (result)
                TempData["message"] = "資料刪除成功!";
            else
                TempData["message"] = "資料刪除失敗!";

            return RedirectToAction("Index");
        }
        /// <summary>
        /// 刪除資產
        /// </summary>
        /// <param name="id">AS_Assets_Build_Record.ID</param>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildMPMaintain")]
        [ExceptionHandle]
        public ActionResult Asset_Del(int id)
        {
            BuildTRXHeaderViewModel header = new BuildTRXHeaderViewModel();
            StringResult Result = this.BuildServant.MPAsset_Del(id);

            if (Result.IsSuccess)
            {
                TempData["message"] = "資產刪除成功!";
                return RedirectToAction("Edit", new { id = Result.StrResult });
            }
            TempData["message"] = "資產刪除失敗!";
            return RedirectToAction("Index");
        }
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildMPMaintain")]
        [ExceptionHandle]
        public ActionResult SearchUserData(string BranchCode)
        {
            List<BuildUserViewModel> result = new List<BuildUserViewModel>();
            AssetsMPIndexModel data = this.BuildServant.SearchUserDataByBranch(BranchCode);
            result = data.BuildUserModel.Select(m => BuildHelper.ToBuildUserViewModel(m)).ToList();

            return Json(result);
        }
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildMPMaintain")]
        [ExceptionHandle]
        public ActionResult SearchAseetList(AssetsSearchViewModel condition) //string City_Code, string District_Code, string Section_Code, string SearchBuildNumber, string SearchAssetNumber
        {
            var data = this.BuildServant.SearchAssets(BuildHelper.ToAssetsSearchModel(condition));
            List<AssetViewModel> Items = new List<AssetViewModel>();
            Items = data.Items.Select(o => BuildHelper.ToAssetViewModel(o)).ToList();
            return Json(Items);
        }
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildMPMaintain")]
        [ExceptionHandle]
        public JsonResult AssetDetailQuery(string AssetNumber)
        {
            var vm = this.BuildServant.GetAssetDetailByAssetNumber(AssetNumber);
            return Json(vm);
        }

        [Login]
        [FunctionAuthorize(ID = "BuildMPMaintain")]
        [ExceptionHandle]
        public ActionResult AssetsUpdatePage(int page)
        {
            AssetsMPSearchViewModel condition = TempData["QueryCondition"] as AssetsMPSearchViewModel;

            condition.PageSize = PageSize;
            condition.Page = page;

            var data = this.BuildServant.SearchAssetsMP(BuildHelper.ToAssetsMPSearchModel(condition));
            AssetsMPIndexViewModel vm = new AssetsMPIndexViewModel()
            {
                Items = data.Items.Select(o => BuildHelper.ToAssetMPViewModel(o)),
                condition = condition,
                TotalAmount = data.TotalAmount
            };

            var Header = (TempData["Header"] != null) ? TempData["Header"] : null;
            TempData["Header"] = Header;
            return View(BuildHelper.GetBuildView("AssetMPSearch"), vm);
        }
    }
}