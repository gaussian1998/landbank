﻿using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.Build;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.Build;
using Library.Servant.Servant.Build.BuildModels;
using Library.Servant.Servant.Land;
using Library.Servant.Servant.UserInformation.Models;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Repository.Build;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Servant.MPBuild.BuildModels;

namespace InspiniaWebAdminTemplate.Controllers.Build
{
    /// <summary>
    /// 房屋新增
    /// </summary>
    public class BuildAddedController : Controller
    {
        private IBuildServant BuildServant = ServantAbstractFactory.Build();
        private ILandServant LandServant = ServantAbstractFactory.Land();
        private string _Type = BuildGeneral.GetFormCode("BuildAdded");
        int PageSize = 10;

        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            SearchModel condition = new SearchModel()
            {
                Transaction_Type = _Type,
                Page = 1,
                PageSize = PageSize
            };
            IndexModel IndexQuery = this.BuildServant.Index(condition);

            IndexViewModel vm = new IndexViewModel()
            {
                condition = BuildHelper.ToSearchViewModel(IndexQuery.condition),
                Items = IndexQuery.Items.Select(o => BuildHelper.ToBuildTRXHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(BuildHelper.GetBuildView("Index"),vm);
        }

        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexQuery(SearchViewModel condition)
        {
            condition.Transaction_Type = _Type;
            condition.Page = 1;
            condition.PageSize = PageSize;
            IndexModel IndexQuery = this.BuildServant.Index(BuildHelper.ToSearchModel(condition));

            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => BuildHelper.ToBuildTRXHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(BuildHelper.GetBuildView("Index"), vm);
        }

        /// <summary>
        /// 分頁Action
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        public ActionResult UpdatePage(int page)
        {
            SearchViewModel condition = new SearchViewModel() { Transaction_Type = _Type, Page = page, PageSize = PageSize };
            IndexModel IndexQuery = this.BuildServant.Index(BuildHelper.ToSearchModel(condition));

            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => BuildHelper.ToBuildTRXHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(BuildHelper.GetBuildView("Index"), vm);
        }


        /// <summary>
        /// 新增單據
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        public ActionResult New()
        {
            BuildViewModel vm = new BuildViewModel()
            {
                TRXHeader = new BuildTRXHeaderViewModel()
                {
                    Form_Number = this.BuildServant.CreateBuildFormNumber(_Type),
                    Transaction_Status = "0", //事務處理狀態
                    Transaction_Datetime_Entered = DateTime.Now
                },
                Assets = new List<AssetViewModel>()
            };
            return View(BuildHelper.GetBuildView("Build"), vm);
        }

        /// <summary>
        /// 編輯單據畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        public ActionResult Edit(string id, string signmode)
        {
            var Detail = this.BuildServant.GetDetail(id);

            BuildViewModel vm;
            vm = new BuildViewModel()
            {
                TRXHeader = BuildHelper.ToBuildTRXHeaderViewModel(Detail.TRXHeader),
            };
            if (Detail.Assets != null)
            {
                vm.Assets = Detail.Assets.Select(m => BuildHelper.ToAssetViewModel(m));
            }
            else
            {
                vm.Assets = new List<AssetViewModel>();
            }

            //var vm = new BuildViewModel ()
            //{
            //    TRXHeader = BuildHelper.ToBuildTRXHeaderViewModel(Detail.TRXHeader),
            //    Assets = Detail.Assets.Select(m => BuildHelper.ToAssetViewModel(m))
            //};

            if (!string.IsNullOrEmpty(signmode))
            {
                ViewBag.SignMode = true;
            }

            return View(BuildHelper.GetBuildView("Build"),vm);
        }

        //[HttpPost]
        //[Login]
        //[FunctionAuthorize(ID = "BuildAdded")]
        //[ExceptionHandle]
        //public ActionResult Asset_New(BuildHeaderViewModel header)
        //{
        //    ViewBag.FunctionFrom = (TempData["FunctionFrom"] != null) ? Convert.ToString(TempData["FunctionFrom"]) : "";
        //    TempData["Header"] = header;
        //    ViewBag.Title = "房屋明細-新增";
        //    return View(BuildHelper.GetBuildView("Asset"),
        //                new AssetViewModel()
        //                {
        //                    Date_Placed_In_Service = DateTime.Now,
        //                    BuildDetails = new List<AssetDetailViewModel>(),
        //                    BuildLands = new List<BuildLandViewModel>()
        //                });
        //}

        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        public ActionResult Asset_New(BuildTRXHeaderViewModel header)
        {
            if(!String.IsNullOrEmpty(header.Office_Branch))
            {
                header.Office_Branch = header.Office_Branch.Split('.')[1];
            }
            else
            {
                header.Office_Branch = string.Empty;
            }
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            header.Book_Type_Code = "NTD_RE_IFRS";
            header.Transaction_Type = (header.Transaction_Type == null) ? _Type : header.Transaction_Type;
            header.Description = header.Description ?? string.Empty;
            header.Amotized_Adjustment_Flag = header.Amotized_Adjustment_Flag ?? "0";  //會計註記 暫時設定0
            header.Auto_Post = header.Auto_Post ?? 1; //拋轉帳務
            header.Created_By = user.ID;
            header.Created_By_Name = user.Name;
            header.Create_Time = DateTime.Now;
            header.Last_UpDatetimed_Time = null;
            header.Last_UpDatetimed_By = null;
            header.Last_UpDatetimed_By_Name = string.Empty;

            //todo: 寫入單頭表據主檔 , 註解暫不寫入
            //var Result = this.BuildServant.Create_Asset(new BuildModel()
            //{
            //    TRXHeader = BuildHelper.ToBuildTRXHeaderModel(header),
            //    Header = new BuildHeaderModel(), //之後要刪掉
            //    Assets = new List<AssetModel>()
            //});

            ViewBag.FunctionFrom = (TempData["FunctionFrom"] != null) ? Convert.ToString(TempData["FunctionFrom"]) : "";
            TempData["Header"] = header;
            ViewBag.Title = "房屋明細-新增";
            return View(BuildHelper.GetBuildView("Asset"),
            new AssetViewModel()
            {
                Date_Placed_In_Service = DateTime.Now,
                BuildDetails = new List<AssetDetailViewModel>(),
                BuildLands = new List<BuildLandViewModel>()
            });
        }

        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        public ActionResult AssetBuildCreate(AssetViewModel asset)
        {
            BuildTRXHeaderViewModel header = null;
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);

            if(TempData["Header"] != null)
            {
                header = TempData["Header"] as BuildTRXHeaderViewModel;
            }

            //if (TempData["Header"] != null)
            //{
            //    header = TempData["Header"] as BuildHeaderViewModel;
            //    header.Transaction_Type = (header.Transaction_Type == null) ? Type : header.Transaction_Type;
            //    header.Description = (header.Description == null) ? "" : header.Description;
            //    header.Created_By = user.ID.ToString();
            //    header.Create_Time = DateTime.Now;
            //    header.Last_Updated_By = "";
            //}

            if (!string.IsNullOrEmpty(asset.BuildDetailsJsonStr))
            {
                asset.BuildDetails = JsonConvert.DeserializeObject<List<AssetDetailViewModel>>(asset.BuildDetailsJsonStr);
            }
            if (!string.IsNullOrEmpty(asset.BuildLandJsonStr))
            {
                asset.BuildLands = JsonConvert.DeserializeObject<List<BuildLandViewModel>>(asset.BuildLandJsonStr);
            }
            asset.Building_STRU = asset.Building_STRU ?? string.Empty;
            asset.Remark1 = asset.Remark1 ?? string.Empty;
            asset.Remark2 = asset.Remark2 ?? string.Empty;
            asset.Created_By = user.ID;
            asset.Create_Time = DateTime.Now;
            asset.Last_Updated_By = null;

            var Build = new BuildModel()
            {
                Assets = new List<AssetViewModel>() { asset }.Select(o => BuildHelper.ToAssetModel(o)),
                Header = new BuildHeaderModel(), //之後要刪掉
            };
            if (header != null)
            {
                Build.TRXHeader = BuildHelper.ToBuildTRXHeaderModel(header);
            };

            var Result = this.BuildServant.Create_Asset(Build);

            TempData["message"] = "房屋新增成功!";
            return RedirectToAction("Edit", new { id = Result.Form_Number });
        }

        /// <summary>
        /// 土地搜尋頁面
        /// </summary>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        [HttpPost]
        public ActionResult Asset_LandSearch(AssetViewModel asset)
        {
            var Header = (TempData["Header"] != null) ? TempData["Header"] : null;
            TempData["Header"] = Header;
            if (!string.IsNullOrEmpty(asset.BuildDetailsJsonStr))
            {
                asset.BuildDetails = JsonConvert.DeserializeObject<List<AssetDetailViewModel>>(asset.BuildDetailsJsonStr);
            }
            if (!string.IsNullOrEmpty(asset.BuildLandJsonStr))
            {
                asset.BuildLands = JsonConvert.DeserializeObject<List<BuildLandViewModel>>(asset.BuildLandJsonStr);
            }
            TempData["Asset"] = asset;
            return View(BuildHelper.GetBuildView("BuildLandSearch"), new InspiniaWebAdminTemplate.Models.Land.AssetsIndexViewModel()
            {
                condition = new InspiniaWebAdminTemplate.Models.Land.AssetsSearchViewModel()
                {
                    Page = 1,
                    PageSize = PageSize
                },
                Items = new List<InspiniaWebAdminTemplate.Models.Land.AssetLandViewModel>()
            });
        }

        /// <summary>
        /// 土地搜尋
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        public ActionResult Asset_LandQuery(InspiniaWebAdminTemplate.Models.Land.AssetsSearchViewModel condition)
        {
            condition.PageSize = PageSize;
            condition.Page = 1;
            var data = this.LandServant.SearchAssets(InspiniaWebAdminTemplate.Models.Land.LandHelper.ToAssetsSearchModel(condition));
            InspiniaWebAdminTemplate.Models.Land.AssetsIndexViewModel vm = new InspiniaWebAdminTemplate.Models.Land.AssetsIndexViewModel()
            {
                Items = data.Items.Select(o => InspiniaWebAdminTemplate.Models.Land.LandHelper.ToAssetLandViewModel(o)),
                condition = condition,
                TotalAmount = data.TotalAmount
            };

            //取的表頭及房屋物件
            var Header = (TempData["Header"] != null) ? TempData["Header"] : null;
            TempData["Header"] = Header;
            var Asset = (TempData["Asset"] != null) ? TempData["Asset"] : null;
            TempData["Asset"] = Asset;

            return View(BuildHelper.GetBuildView("BuildLandSearch"),vm);
        }

        /// <summary>
        /// 分頁Action
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        public ActionResult LandQueryUpdatePage(int page)
        {
            InspiniaWebAdminTemplate.Models.Land.AssetsSearchViewModel condition = new InspiniaWebAdminTemplate.Models.Land.AssetsSearchViewModel()
            {
                Page = page,
                PageSize = PageSize
            };

            var data = this.LandServant.SearchAssets(InspiniaWebAdminTemplate.Models.Land.LandHelper.ToAssetsSearchModel(condition));
            InspiniaWebAdminTemplate.Models.Land.AssetsIndexViewModel vm = new InspiniaWebAdminTemplate.Models.Land.AssetsIndexViewModel()
            {
                Items = data.Items.Select(o => InspiniaWebAdminTemplate.Models.Land.LandHelper.ToAssetLandViewModel(o)),
                condition = condition,
                TotalAmount = data.TotalAmount
            };

            var Header = (TempData["Header"] != null) ? TempData["Header"] : null;
            TempData["Header"] = Header;
            return View(InspiniaWebAdminTemplate.Models.Land.LandHelper.GetLandView("AssetSearch"), vm);
        }

        /// <summary>
        /// 刪除表頭單據以及資產
        /// </summary>
        /// <param name="id">AS_TRX_Headers.Form_Number</param>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        public ActionResult HeaderAndAsset_Del(string id)
        {
            bool result = this.BuildServant.HeaderAndAsset_Del(id);

            if (result)
                TempData["message"] = "資料刪除成功!";
            else
                TempData["message"] = "資料刪除失敗!";

            return RedirectToAction("Index");
        }

        /// <summary>
        /// 刪除資產
        /// </summary>
        /// <param name="id">AS_Assets_Build_Record.ID</param>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        public ActionResult Asset_Del(int id)
        {
            BuildTRXHeaderViewModel header = new BuildTRXHeaderViewModel();
            StringResult Result = this.BuildServant.Asset_Del(id);

            if (Result.IsSuccess)
            {
                TempData["message"] = "資產刪除成功!";
                return RedirectToAction("Edit", new { id = Result.StrResult });
            }
            TempData["message"] = "資產刪除失敗!";
            return RedirectToAction("Index");
        }

        //新增土地關聯至房屋
        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        [HttpPost]
        public ActionResult Asset_AddLandToBuild(string LandList)
        {
            BuildTRXHeaderViewModel header = null;
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            if (TempData["Header"] != null)
            {
                header = TempData["Header"] as BuildTRXHeaderViewModel;
                header.Transaction_Type = (header.Transaction_Type == null) ? _Type : header.Transaction_Type;
                header.Description = (header.Description == null) ? "" : header.Description;
                header.Transaction_Datetime_Entered = DateTime.Now;
                header.Created_By = user.ID;
                header.Created_By_Name = user.Name;
                header.Create_Time = DateTime.Now;
                header.Last_UpDatetimed_Time = null;
                header.Last_UpDatetimed_By = null;
                header.Last_UpDatetimed_By_Name = string.Empty;
            }
            AssetViewModel asset = null;
            if(TempData["Asset"] != null)
            {
                asset = TempData["Asset"] as AssetViewModel;
            }

            BuildModel Build = new BuildModel()
            {
                TRXHeader = BuildHelper.ToBuildTRXHeaderModel(header)
            };

            AssetModel Asset = BuildHelper.ToAssetModel(asset);
            if(LandList.Split(',').Length > 0)
            {
                Asset.BuildLands = LandList.Split(',').Select(o => new BuildLandModel() {
                     ID = Convert.ToInt32(o)
                });
            }

            Build.Assets = new List<AssetModel>() { Asset };
            AssetHandleResult Result = this.BuildServant.Create_Asset(Build);

            TempData["message"] = "土地導入成功!";
            //回資產編輯頁
            return RedirectToAction("Asset_Edit", new { id = Result.AssetID });
        }

        /// <summary>
        /// 新增資產
        /// </summary> 
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        public ActionResult Asset_Create(AssetViewModel asset)
        {
            BuildHeaderViewModel header = null;
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            if (TempData["Header"] != null)
            {
                header = TempData["Header"] as BuildHeaderViewModel;
                header.Transaction_Type = (header.Transaction_Type == null) ? _Type : header.Transaction_Type;
                header.Description = (header.Description == null) ? "" : header.Description;
                header.Created_By = user.ID.ToString();
                header.Create_Time = DateTime.Now;
                header.Last_Updated_By = "";
            }
            if(!string.IsNullOrEmpty(asset.BuildDetailsJsonStr))
            {
                asset.BuildDetails = JsonConvert.DeserializeObject<List<AssetDetailViewModel>>(asset.BuildDetailsJsonStr);
            }
            if(!string.IsNullOrEmpty(asset.BuildLandJsonStr))
            {
                asset.BuildLands = JsonConvert.DeserializeObject<List<BuildLandViewModel>>(asset.BuildLandJsonStr);
            }
            
            asset.Created_By = user.ID;
            asset.Create_Time = DateTime.Now;
            asset.Created_By_Name = user.Name;
            asset.Last_Updated_By = null;

            var Build = new BuildModel()
            {
                Assets = new List<AssetViewModel>() { asset }.Select(o => BuildHelper.ToAssetModel(o))
            };
            if (header != null)
            {
                Build.Header = BuildHelper.ToBuildHeaderModel(header);
            };


            var Result = this.BuildServant.Create_Asset(Build);

            TempData["message"] = "房屋新增成功!";
            return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }


        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        public ActionResult Asset_Edit(int id)
        {
            BuildTRXHeaderModel header;
            var vm = this.BuildServant.GetAssetDetail(id,out header);
            TempData["Header"] = BuildHelper.ToBuildTRXHeaderViewModel(header);
            ViewBag.Title = "編輯房屋";
            ViewBag.FunctionFrom = (TempData["FunctionFrom"] != null) ? Convert.ToString(TempData["FunctionFrom"]) : "";
            return View(BuildHelper.GetBuildView("Asset"), BuildHelper.ToAssetViewModel(vm));
        }

        /// <summary>
        /// 編輯資產
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        public ActionResult Asset_Update(AssetViewModel asset)
        {
            BuildTRXHeaderViewModel header = TempData["Header"] as BuildTRXHeaderViewModel;
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            if (header.Description == null)
            {
                header.Description = "";
            }
            asset.Last_Updated_By = user.ID;
            asset.Last_Updated_Time = DateTime.Now;
            if (!string.IsNullOrEmpty(asset.BuildDetailsJsonStr))
            {
                asset.BuildDetails = JsonConvert.DeserializeObject<List<AssetDetailViewModel>>(asset.BuildDetailsJsonStr);
            }
            if (!string.IsNullOrEmpty(asset.BuildLandJsonStr))
            {
                asset.BuildLands = JsonConvert.DeserializeObject<List<BuildLandViewModel>>(asset.BuildLandJsonStr);
            }
            var Result = this.BuildServant.Create_Asset(new BuildModel()
            {
                Header = new BuildHeaderModel(), //之後要移除
                TRXHeader = BuildHelper.ToBuildTRXHeaderModel(header),
                Assets = new List<AssetViewModel>() { asset }.Select(o => BuildHelper.ToAssetModel(o))
            });
            TempData["message"] = "房屋編輯成功!";
            return RedirectToAction("Edit", new { id = Result.Form_Number });
        }

        /// <summary>
        /// 跑簽核流程
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        public ActionResult CreateFlow(BuildTRXHeaderViewModel header)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            int DocID;
            string message = "提交成功!";
            //房屋都先跑總行
            if (BuildServant.GetDocID(_Type.Substring(0, _Type.Length - 1) + "H", out DocID))
            {
                this.BuildServant.UpdateFlowStatus(header.Form_Number, "1");
                LocalApprovalTodoServant.Create(user.ID, DocID, header.Form_Number, "BuildAdded");
                LocalApprovalTodoServant.Start(user.ID, header.Form_Number, header.Form_Number);
            }
            else
            {
                message = "查無此表單!!";
            }

            //Test
            //BuildServant.CreateToBuild(header.TRX_Header_ID);
            TempData["message"] = message;
            return RedirectToAction("Edit", new { id = header.Form_Number });
        }

        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildAdded")]
        [ExceptionHandle]
        public string GetAssetNumber(string MainKind, string DetailKind)
        {
            return this.LandServant.CreateAssetNumber(MainKind, DetailKind);
        }
    }
}