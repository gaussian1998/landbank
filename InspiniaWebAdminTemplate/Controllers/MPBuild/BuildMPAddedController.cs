﻿using InspiniaWebAdminTemplate.Attributes;
using InspiniaWebAdminTemplate.Models.Build;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Repository.Build;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.Build;
using Library.Servant.Servant.Build.BuildModels;
using Library.Servant.Servant.Land;
using Library.Servant.Servant.MPBuild.BuildModels;
using Library.Servant.Servant.UserInformation.Models;
using Library.Servant.Servants.AbstractFactory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Build
{
    public class BuildMPAddedController : Controller
    {
        private IBuildServant BuildServant = ServantAbstractFactory.Build();
        private ILandServant LandServant = ServantAbstractFactory.Land();
        private string _Type = BuildGeneral.GetFormCode("BuildMPAdded");
        int PageSize = 10;

        [Login]
        [FunctionAuthorize(ID = "BuildMPAdded")]
        [ExceptionHandle]
        // GET: BuildMPAdded
        public ActionResult Index()
        {
            SearchModel condition = new SearchModel()
            {
                Transaction_Type = _Type
            };
            IndexModel IndexQuery = this.BuildServant.Index(condition);

            IndexViewModel vm = new IndexViewModel()
            {
                condition = BuildHelper.ToSearchViewModel(IndexQuery.condition),
                Items = IndexQuery.Items.Select(o => BuildHelper.ToBuildTRXHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(BuildHelper.GetBuildView("Index"), vm);
        }

        [Login]
        [FunctionAuthorize(ID = "BuildMPAdded")]
        [ExceptionHandle]
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexQuery(SearchViewModel condition)
        {
            condition.Transaction_Type = _Type;
            condition.Page = 1;
            condition.PageSize = PageSize;
            IndexModel IndexQuery = this.BuildServant.Index(BuildHelper.ToSearchModel(condition));
            IndexViewModel vm = new IndexViewModel()
            {
                condition = condition,
                Items = IndexQuery.Items.Select(o => BuildHelper.ToBuildTRXHeaderViewModel(o)),
                TotalAmount = IndexQuery.TotalAmount
            };
            return View(BuildHelper.GetBuildView("Index"), vm);
        }

        /// <summary>
        /// 新增單據
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildMPAdded")]
        [ExceptionHandle]
        public ActionResult New()
        {
            BuildMPViewModel vm = new BuildMPViewModel()
            {
                TRXHeader = new BuildTRXHeaderViewModel()
                {
                    Form_Number = this.BuildServant.CreateBuildFormNumber(_Type),
                    Transaction_Status = "0", //事務處理狀態
                    Transaction_Datetime_Entered = DateTime.Now
                },
                Assets = new List<AssetMPViewModel>()
            };
            return View(BuildHelper.GetBuildView("BuildMP"), vm);
        }

        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildMPAdded")]
        [ExceptionHandle]
        public ActionResult Asset_New(BuildTRXHeaderViewModel header)
        {
            if (!String.IsNullOrEmpty(header.Office_Branch))
                header.Office_Branch = header.Office_Branch.Split('.')[1];
            else
                header.Office_Branch = string.Empty;

            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);

            #region 單據表頭
            header.Book_Type_Code = "NTD_RE_IFRS";
            header.Transaction_Type = (header.Transaction_Type == null) ? _Type : header.Transaction_Type;
            header.Description = header.Description ?? string.Empty;
            header.Amotized_Adjustment_Flag = header.Amotized_Adjustment_Flag ?? "0";  //會計註記 暫時設定0
            header.Auto_Post = header.Auto_Post ?? 1; //拋轉帳務
            header.Created_By = user.ID;
            header.Created_By_Name = user.Name;
            header.Create_Time = DateTime.Now;
            header.Last_UpDatetimed_Time = null;
            header.Last_UpDatetimed_By = null;
            header.Last_UpDatetimed_By_Name = string.Empty;
            #endregion

            ViewBag.FunctionFrom = (TempData["FunctionFrom"] != null) ? Convert.ToString(TempData["FunctionFrom"]) : "";
            TempData["Header"] = header;
            ViewBag.Title = "重大組成明細-新增";

            //查詢房屋資料
            var vm = this.BuildServant.SearchAssets(new AssetsSearchModel());

            ViewBag.Build = new AssetsIndexViewModel()
            {
                Items = vm.Items.Select(o => BuildHelper.ToAssetViewModel(o)),
                //BuildUserViewModel = vm.BuildUserModel.Select(u=>BuildHelper.ToBuildUserViewModel(u)).ToList()
            };

            return View(BuildHelper.GetBuildView("AssetMP"),
                        new AssetMPViewModel()
                        {
                            Date_Placed_In_Service = DateTime.Now,
                            Transaction_Date = DateTime.Now
                        });
        }


        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildMPAdded")]
        [ExceptionHandle]
        public JsonResult AssetDetailQuery(string AssetNumber)
        {
            var vm = this.BuildServant.GetAssetDetailByAssetNumber(AssetNumber);
            return Json(vm);
        }


        /// <summary>
        /// 新增資產
        /// </summary> 
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildMPAdded")]
        [ExceptionHandle]
        public ActionResult Asset_Create(AssetMPViewModel asset)
        {
            BuildTRXHeaderViewModel header = null;
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);

            if (TempData["Header"] != null)
            {
                header = TempData["Header"] as BuildTRXHeaderViewModel;
            }
            asset.Book_Type = "NTD_RE_IFRS";
            asset.Created_By = user.ID;
            asset.Create_Time = DateTime.Now;
            asset.Created_By_Name = user.Name;
            //asset.Last_Updated_By = null;

            var BuildMP = new BuildMPModel()
            {
                Assets = new List<AssetMPViewModel>() { asset }.Select(o => BuildHelper.ToAssetMPModel(o))
            };
            if (header != null)
            {
                BuildMP.TRXHeader = BuildHelper.ToBuildTRXHeaderModel(header);
            };

            var Result = this.BuildServant.Create_AssetMP(BuildMP);

            TempData["message"] = "重大組成新增成功!";
            return RedirectToAction("Edit", new { id = Result.Form_Number });

            //BuildHeaderViewModel header = null;
            //if (TempData["Header"] != null)
            //{
            //    header = TempData["Header"] as BuildHeaderViewModel;
            //    header.Transaction_Type = (header.Transaction_Type == null) ? _Type : header.Transaction_Type;
            //    header.Transaction_Status = "";
            //    header.Description = (header.Description == null) ? "" : header.Description;
            //    header.Transaction_Datetime_Entered = DateTime.Now;
            //    header.Created_By = "";
            //    header.Create_Time = DateTime.Now;
            //    header.Last_Updated_By = "";
            //}

            //asset.Created_By = null;
            //asset.Create_Time = DateTime.Now;
            //asset.Last_Updated_By = null;

            //var BuildMP = new BuildMPModel()
            //{
            //    Assets = new List<AssetMPViewModel>() { asset }.Select(o => BuildHelper.ToAssetMPModel(o))
            //};
            //if (header != null)
            //{
            //    BuildMP.Header = BuildHelper.ToBuildHeaderModel(header);
            //};


            //var Result = this.BuildServant.Create_AssetMP(BuildMP);

            //TempData["message"] = "重大組成新增成功!";
            //return RedirectToAction("Edit", new { id = Result.TRX_Header_ID });
        }


        /// <summary>
        /// 編輯單據畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildMPAdded")]
        [ExceptionHandle]
        public ActionResult Edit(string id, string signmode)
        {
            var Detail = this.BuildServant.GetMPDetail(id);

            var vm = new BuildMPViewModel()
            {
                TRXHeader = BuildHelper.ToBuildTRXHeaderViewModel(Detail.TRXHeader),
                Assets = Detail.Assets.Select(m => BuildHelper.ToAssetMPViewModel(m))
            };

            if (!string.IsNullOrEmpty(signmode))
            {
                ViewBag.SignMode = true;
            }

            return View(BuildHelper.GetBuildView("BuildMP"), vm);
        }


        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildMPAdded")]
        [ExceptionHandle]
        public ActionResult Asset_Edit(int id)
        {
            BuildTRXHeaderModel header;
            var vm = this.BuildServant.GetAssetMPDetail(id, out header);
            TempData["Header"] = (header != null) ? BuildHelper.ToBuildTRXHeaderViewModel(header) : null;
            ViewBag.Title = "重大組成明細-編輯";
            ViewBag.FunctionFrom = (TempData["FunctionFrom"] != null) ? Convert.ToString(TempData["FunctionFrom"]) : "";

            //查詢房屋資料
            //var builds= this.BuildServant.SearchAssets(new AssetsSearchModel());
            //ViewBag.Build = new AssetsIndexViewModel()
            //{
            //    Items = builds.Items.Select(o => BuildHelper.ToAssetViewModel(o))
            //};

            var view = BuildHelper.ToAssetMPViewModel(vm);
            //讀取母(主)資產資訊
            var ParentData = this.BuildServant.GetAssetDetailByAssetNumber(vm.Parent_Asset_Number);
            if(ParentData != null)
            {
                view.Asset_Category_Code_Show = ParentData.Asset_Category_Code;
                view.Asset_Category_Code_Name_Show = ParentData.Asset_Category_Code_Name;
                view.AssetInfoShow = ParentData.Description;
            }
            return View(BuildHelper.GetBuildView("AssetMP"), view);
        }


        /// <summary>
        /// 編輯資產
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildMPAdded")]
        [ExceptionHandle]
        public ActionResult Asset_Update(AssetMPViewModel asset)
        {
            BuildTRXHeaderViewModel header = TempData["Header"] as BuildTRXHeaderViewModel;
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            header.Description = header.Description ?? "";
            header.Last_UpDatetimed_By = user.ID;
            header.Last_UpDatetimed_By_Name = user.Name;
            header.Last_UpDatetimed_Time = DateTime.Now;

            asset.Last_UpDatetimed_By = user.ID;
            asset.Last_UpDatetimed_By_Name = user.Name;
            asset.Last_UpDatetimed_Time = DateTime.Now;
            var Result = this.BuildServant.Create_AssetMP(new BuildMPModel()
            {
                Header = new BuildHeaderModel(),
                TRXHeader = BuildHelper.ToBuildTRXHeaderModel(header),
                Assets = new List<AssetMPViewModel>() { asset }.Select(o => BuildHelper.ToAssetMPModel(o))
            });
            TempData["message"] = "重大組成明細編輯成功!";
            return RedirectToAction("Edit", new { id = Result.Form_Number });
        }


        /// <summary>
        /// 跑簽核流程
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildMPAdded")]
        [ExceptionHandle]
        public ActionResult CreateFlow(BuildTRXHeaderViewModel header)
        {
            UserInfor user = UserInfoServant.Details(HttpContext.ApplicationInstance.Context);
            int DocID;
            string message = "提交成功!";
            //都先跑總行
            if (BuildServant.GetDocID(_Type.Substring(0, _Type.Length - 1) + "H", out DocID))
            {
                this.BuildServant.UpdateFlowStatus(header.Form_Number, "1");
                LocalApprovalTodoServant.Create(user.ID, DocID, header.Form_Number, "BuildMPAdded");
                LocalApprovalTodoServant.Start(user.ID, header.Form_Number, header.Form_Number);
            }
            else
            {
                message = "查無此表單!!";
            }
            TempData["message"] = message;
            return RedirectToAction("Edit", new { id = header.Form_Number });
        }
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildMPAdded")]
        [ExceptionHandle]
        public ActionResult SearchUserData(string BranchCode)
        {
            List<BuildUserViewModel> result = new List<BuildUserViewModel>();
            AssetsMPIndexModel data = this.BuildServant.SearchUserDataByBranch(BranchCode);
            result = data.BuildUserModel.Select(m => BuildHelper.ToBuildUserViewModel(m)).ToList();

            return Json(result);
        }
        [HttpPost]
        [Login]
        [FunctionAuthorize(ID = "BuildMPAdded")]
        [ExceptionHandle]
        public ActionResult SearchAseetList(AssetsSearchViewModel condition) //string City_Code, string District_Code, string Section_Code, string SearchBuildNumber, string SearchAssetNumber
        {
            var data = this.BuildServant.SearchAssets(BuildHelper.ToAssetsSearchModel(condition));
            List<AssetViewModel> Items = new List<AssetViewModel>();
            Items = data.Items.Select(o => BuildHelper.ToAssetViewModel(o)).ToList();
            return Json(Items);
        }
        /// <summary>
        /// 刪除表頭單據以及資產
        /// </summary>
        /// <param name="id">AS_TRX_Headers.Form_Number</param>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildMPAdded")]
        [ExceptionHandle]
        public ActionResult HeaderAndAsset_Del(string id)
        {
            bool result = this.BuildServant.HeaderAndMPAsset_Del(id);

            if (result)
                TempData["message"] = "資料刪除成功!";
            else
                TempData["message"] = "資料刪除失敗!";

            return RedirectToAction("Index");
        }
        /// <summary>
        /// 刪除資產
        /// </summary>
        /// <param name="id">AS_Assets_Build_Record.ID</param>
        /// <returns></returns>
        [HttpGet]
        [Login]
        [FunctionAuthorize(ID = "BuildMPAdded")]
        [ExceptionHandle]
        public ActionResult Asset_Del(int id)
        {
            BuildTRXHeaderViewModel header = new BuildTRXHeaderViewModel();
            StringResult Result = this.BuildServant.MPAsset_Del(id);

            if (Result.IsSuccess)
            {
                TempData["message"] = "資產刪除成功!";
                return RedirectToAction("Edit", new { id = Result.StrResult });
            }
            TempData["message"] = "資產刪除失敗!";
            return RedirectToAction("Index");
        }
    }
}