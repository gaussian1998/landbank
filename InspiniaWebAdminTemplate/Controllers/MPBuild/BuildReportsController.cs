﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.MPBuild
{
    public class BuildReportsController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "BuildReports")]
        [ExceptionHandle]
        public ActionResult Reports_1()
        {
            return View();
        }

        [Login]
        [FunctionAuthorize(ID = "BuildReports")]
        [ExceptionHandle]
        public ActionResult Reports_1_A()
        {
            return View();
        }
    }
}