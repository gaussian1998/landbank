﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Policy
{
    public class PolicyReportController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "PolicyReport")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }
    }
}