﻿using InspiniaWebAdminTemplate.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers.Policy
{
    public class InsuranceAddedController : Controller
    {
        [Login]
        [FunctionAuthorize(ID = "InsuranceAdded")]
        [ExceptionHandle]
        public ActionResult Index()
        {
            return View();
        }
    }
}