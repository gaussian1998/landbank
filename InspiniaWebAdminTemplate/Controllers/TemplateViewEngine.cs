﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Controllers
{
    public class TemplateViewEngine : RazorViewEngine
    {

        public TemplateViewEngine() : base()
        {
            AreaViewLocationFormats = new[] {
                "~/Areas/{2}/Views/{1}/{0}.csa",
                "~/Areas/{2}/Views/Shared/{0}.csa"
            };

            AreaMasterLocationFormats = new[] {
                "~/Areas/{2}/Views/{1}/{0}.csa",
                "~/Areas/{2}/Views/Shared/{0}.csa",
            };

            AreaPartialViewLocationFormats = new[] {
                "~/Areas/{2}/Views/{1}/{0}.csa",
                "~/Areas/{2}/Views/Shared/{0}.csa",
            };

            ViewLocationFormats = new[] {
                "~/Views/{1}/{0}.csa",
                "~/Views/Shared/{0}.csa"
            };

            MasterLocationFormats = new[] {
                "~/Views/{1}/{0}.cshtml",
                "~/Views/Shared/{0}.cshtml",
            };

            PartialViewLocationFormats = new[] {
                "~/Views/{1}/{0}.csa",
                "~/Views/Shared/{0}.csa",
            };
        }

        protected override IView CreatePartialView(ControllerContext controllerContext, string partialPath)
        {
            var nameSpace = controllerContext.Controller.GetType().Namespace;
            return base.CreatePartialView(controllerContext, partialPath.Replace("%1", nameSpace));
        }

        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
        {
            var nameSpace = controllerContext.Controller.GetType().Namespace;
            return base.CreateView(controllerContext, viewPath.Replace("%1", nameSpace), masterPath.Replace("%1", nameSpace));
        }

        protected override bool FileExists(ControllerContext controllerContext, string virtualPath)
        {
            var nameSpace = controllerContext.Controller.GetType().Namespace;
            return base.FileExists(controllerContext, virtualPath.Replace("%1", nameSpace));
        }
    }
}
