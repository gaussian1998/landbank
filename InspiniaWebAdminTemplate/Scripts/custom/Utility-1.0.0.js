var Utility;
(function (Utility) {
    var Ajax = (function () {
        function Ajax() {
        }
        /**
         * Ajax get
         * @param url 目標網址
         * @param data 參數
         * @param successCallBackFn 成功要執行的fn
         * @param errorCallBackFn 失敗要執行的fn
         */
        Ajax.prototype.GET = function (url, successCallBackFn, errorCallBackFn) {
            var JQryAjxSetting = {
                url: url,
                type: "GET",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true,
                error: function (xhr, status, error) {
                    if (typeof errorCallBackFn === 'function') {
                        errorCallBackFn(xhr, status, error);
                    }
                    else {
                        Utility.ErrorProcess.AjaxGetError(xhr, status, error);
                    }
                },
                success: function (data) {
                    if (typeof successCallBackFn === 'function') {
                        successCallBackFn(data);
                    }
                }
            };
            $.ajax(JQryAjxSetting);
        };
        /**
         * Ajax post
         * @param url 目標網址
         * @param Data json 物件
         * @param successCallBackFn 成功要執行的fn
         * @param errorCallBackFn 失敗要執行的fn
         */
        Ajax.prototype.POSTData = function (url, Data, successCallBackFn, errorCallBackFn) {
            $.ajax({
                type: "POST",
                url: url,
                data: Data,
                cache: false,
                success: function (data) {
                    if (typeof successCallBackFn === 'function') {
                        successCallBackFn(data);
                    }
                },
                error: function (xhr, status, error) {
                    if (typeof errorCallBackFn === 'function') {
                        errorCallBackFn(xhr, status, error);
                    }
                    else {
                        Utility.ErrorProcess.AjaxGetError(xhr, status, error);
                    }
                }
            });
        };
        /**
         * Ajax post
         * @param url 目標網址
         * @param jsonData json 物件
         * @param successCallBackFn 成功要執行的fn
         * @param errorCallBackFn 失敗要執行的fn
         */
        Ajax.prototype.POSTJsonData = function (url, jsonData, successCallBackFn, errorCallBackFn) {
            var JQryAjxSetting = {
                url: url,
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: jsonData,
                cache: false,
                error: function (xhr, status, error) {
                    if (typeof errorCallBackFn === 'function') {
                        errorCallBackFn(xhr, status, error);
                    }
                    else {
                        Utility.ErrorProcess.AjaxGetError(xhr, status, error);
                    }
                },
                success: function (data) {
                    if (typeof successCallBackFn === 'function') {
                        successCallBackFn(data);
                    }
                }
            };
            $.ajax(JQryAjxSetting);
        };
        /**
         * Ajax post
         * @param url 目標網址
         * @param jsonData json 物件
         * @param successCallBackFn 成功要執行的fn
         * @param errorCallBackFn 失敗要執行的fn
         */
        Ajax.prototype.POSTJsonDataDownloadfile = function (url, jsonData, successCallBackFn, errorCallBackFn) {
            var JQryAjxSetting = {
                url: url,
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: jsonData,
                headers: {
                    Accept: "text/csv; charset=utf-8",
                },
                cache: false,
                error: function (xhr, status, error) {
                    if (typeof errorCallBackFn === 'function') {
                        errorCallBackFn(xhr, status, error);
                    }
                    else {
                        Utility.ErrorProcess.AjaxGetError(xhr, status, error);
                    }
                },
                success: function (data) {
                    if (typeof successCallBackFn === 'function') {
                        successCallBackFn(data);
                    }
                }
            };
            $.ajax(JQryAjxSetting);
        };
        return Ajax;
    }());
    Utility.Ajax = Ajax;
    /**
     * 遮罩對話框
     */
    var LightBox = (function () {
        function LightBox() {
        }
        /**
         * 確認對話框
         * @param title 標題
         * @param content 內容
         * @param leftBtnText 左邊按鈕文字
         * @param rightBtnText 右邊按鈕文字
         * @param leftBtnCallBackFunc 左邊按鈕按鈕事件
         * @param rightBtnCallBackFunc 右邊按鈕按鈕事件
         */
        LightBox.confirm = function (title, content, leftBtnText, rightBtnText, leftBtnCallBackFunc, rightBtnCallBackFunc) {
            var confirmModal = $('<div class="modal fade" id="new_Detail" tabindex="-1" role="dialog" aria-labelledby="detailLabel" aria-hidden="true">' +
                '    <div class="modal-dialog">' +
                '        <div class="modal-content">' +
                '            <div class="modal-header">' +
                '                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '                <h4 class="modal-title" id="detailLabel">' + title + '</h4>' +
                '            </div>' +
                '            <div class="modal-body">' +
                '                <p class="text-center">' + content + '</p>' +
                '            </div>' +
                '            <div class="modal-footer">' +
                '                <button type="button" id="leftBtn" class="btn btn-default" data-dismiss="modal">' + leftBtnText + '</button>' +
                '                <button type="button"  id="rightBtn" class="btn btn-primary" data-dismiss="modal" onclick="">' + rightBtnText + '</button>' +
                '            </div>' +
                '        </div><!-- /.modal-content -->' +
                '    </div><!-- /.modal -->' +
                '</div>');
            confirmModal.find('#leftBtn').click(function (event) {
                if (typeof leftBtnCallBackFunc === 'function') {
                    leftBtnCallBackFunc();
                }
                confirmModal.modal('hide');
            });
            confirmModal.find('#rightBtn').click(function (event) {
                if (typeof rightBtnCallBackFunc === 'function') {
                    rightBtnCallBackFunc();
                }
                confirmModal.modal('hide');
            });
            confirmModal.modal('show');
        };
        /**
         * 提醒對話框
         * @param title 標題
         * @param content 內容
         * @param okButtonTxt 按鈕文字
         * @param okCallBackFunction 按鈕事件
         * @returns
         */
        LightBox.alert = function (title, content, okButtonTxt, okCallBackFunction) {
            var result = '';
            var wait = true;
            var alertModel = $('<div class="modal fade" id="new_Detail" tabindex="-1" role="dialog" aria-labelledby="detailLabel" aria-hidden="true">' +
                '    <div class="modal-dialog">' +
                '        <div class="modal-content">' +
                '            <div class="modal-header">' +
                '                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '                <h4 class="modal-title" id="detailLabel">' + title + '</h4>' +
                '            </div>' +
                '            <div class="modal-body">' +
                '                <p class="text-center">' + content + '</p>' +
                '            </div>' +
                '            <div class="modal-footer">' +
                '                <button type="button" id="okButton" class="btn btn-default" data-dismiss="modal">' + okButtonTxt + '</button>' +
                '            </div>' +
                '        </div><!-- /.modal-content -->' +
                '    </div><!-- /.modal -->' +
                '</div>');
            alertModel.find('#okButton').click(function (event) {
                alertModel.modal('hide');
                if (typeof okCallBackFunction === 'function') {
                    okCallBackFunction();
                }
            });
            alertModel.modal('show');
            return alertModel;
        };
        return LightBox;
    }());
    Utility.LightBox = LightBox;
    /**
     * 當呼叫Ajax的時所要套用的錯誤處理預設function
     * @param xhr
     * @param status
     * @param error
     */
    var ErrorProcess = (function () {
        function ErrorProcess() {
        }
        ErrorProcess.AjaxGetError = function (xhr, status, error) {
            if (xhr.status === 440) {
                Utility.LightBox.alert("訊息", xhr.responseText, "確定", function () {
                    location.href = "/Account/Login";
                });
            }
            else {
                Utility.LightBox.alert("訊息", xhr.responseText.replace(/\\r\\n/g, "<br>"), "確定", null);
            }
        };
        return ErrorProcess;
    }());
    Utility.ErrorProcess = ErrorProcess;
})(Utility || (Utility = {}));
//# sourceMappingURL=Utility-1.0.0.js.map