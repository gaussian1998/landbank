﻿//Class
function TableCheckBox(headCheckbox, checkboxes) {
    var self = this;

    self.checkedValueSet = new Set();

    self.GetCheckedValues = function () {
        return $(checkboxes).filter(':checked').map(function () {
            return $(this).val();
        }).get();
    };

    self.GetUnCheckedValues = function () {
        return $(checkboxes).not(':checked').map(function () {
            return $(this).val();
        }).get();
    };

    self.UpdateCheckedValue = function () {
        //Add checked value
        var currentPageCheckedValue = self.GetCheckedValues();
        currentPageCheckedValue.forEach(function (v, i) {
            checkedValueSet.add(v);
        });

        //Remove unchecked value
        var currentPageUnCheckedValues = self.GetUnCheckedValues();
        currentPageUnCheckedValues.forEach(function (v, i) {
            checkedValueSet.delete(v);
        });
    };

    self.ChangeCurrentPageCheckboxStatus = function () {
        $(checkboxes).filter(function (i, v) {
            return checkedValueSet.has(v.value);
        }).prop('checked', true);

        $(headCheckbox).prop('checked', $(checkboxes).not(':checked').size() == 0);
    };

    self.CheckBoxsEventHandler = function () {
        $(headCheckbox).click(function () {
            $(checkboxes).prop('checked', $(headCheckbox).is(':checked'));
            self.UpdateCheckedValue();
        });

        $(checkboxes).click(function () {
            $(headCheckbox).prop('checked', $(checkboxes).not(':checked').length == 0);
            self.UpdateCheckedValue();
        });
    };
}