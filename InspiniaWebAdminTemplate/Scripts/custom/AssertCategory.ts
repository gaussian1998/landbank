﻿
namespace AS_LandBank {
    export class AssetClassifies {
        static GetMainKinds(successFn) {
            var JQryAjxSetting: JQueryAjaxSettings = {
                url: "/AssetCategory/GetMainKindList",
                type: "GET",
                contentType: "application/json; charset=utf-8",
                async: true,
                error: function (xhr, status, error) { alert(error); },
                success: function (data) {
                    if (typeof successFn === "function") {
                        successFn(data);
                    }
                },
            };
            $.ajax(JQryAjxSetting);
        }
        static GetDetailKinds(mainKindNo, successFn) {
            var JQryAjxSetting: JQueryAjaxSettings = {
                url: "/AssetCategory/GetDetailKindList?" + "&mainKindNo=" + mainKindNo,
                type: "GET",
                contentType: "application/json; charset=utf-8",
                async: true,
                error: function (xhr, status, error) { alert(error); },
                success: function (data) {
                    if (typeof successFn === "function") {
                        successFn(data);
                    }
                },
            };
            $.ajax(JQryAjxSetting);
        }
        static GetUseTypes(successFn) {
            var JQryAjxSetting: JQueryAjaxSettings = {
                url: "/AssetCategory/GetUseTypeList",
                type: "GET",
                contentType: "application/json; charset=utf-8",
                async: true,
                error: function (xhr, status, error) { alert(error); },
                success: function (data) {
                    if (typeof successFn === "function") {
                        successFn(data);
                    }
                },
            };
            $.ajax(JQryAjxSetting);
        }
        static AddNewMainKind(data, successFn) {
            alert(JSON.stringify(data));
        }
        static AddNewDetailKind(data, successFn) {
            alert(JSON.stringify(data));
        }
        static AddNewUseType(data, successFn) {
            alert(JSON.stringify(data));
        }
        static AddNewCategory(data, successFn) {
            alert(JSON.stringify(data));
        }
    }
}
