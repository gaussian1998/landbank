﻿//Ajax for PagedList PartialView 
//fetchPage(selector, url[, callBack])
var fetchPage = function (selector, url, callBack) {
    callBack = callBack || function () { };

    $.get(url, function (htmlStr) {
        $(selector).html(htmlStr);
        
        $('.pagination li a', selector).each(function (i, item) {
            var pagedUrl = $(item).attr('href');
            if (pagedUrl) {
                $(item).click(function (event) {
                    event.preventDefault();
                    fetchPage(selector, pagedUrl, callBack);
                }).attr('href', '#');
            }
        });
        
        callBack();
    });
};

