﻿namespace Utility {
    export class Ajax {
        constructor() { }
        /**
         * Ajax get
         * @param url 目標網址
         * @param data 參數
         * @param successCallBackFn 成功要執行的fn
         * @param errorCallBackFn 失敗要執行的fn
         */
        GET(url, successCallBackFn, errorCallBackFn) {
            var JQryAjxSetting: JQueryAjaxSettings = {
                url: url,
                type: "GET",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: true,
                error: function (xhr, status, error) {
                    if (typeof errorCallBackFn === 'function') {
                        errorCallBackFn(xhr, status, error);
                    }
                    else {
                        Utility.ErrorProcess.AjaxGetError(xhr, status, error);
                    }
                },
                success: function (data) {
                    if (typeof successCallBackFn === 'function') {
                        successCallBackFn(data);
                    }
                }
            };
            $.ajax(JQryAjxSetting);
        }
        /**
         * Ajax post
         * @param url 目標網址
         * @param Data json 物件
         * @param successCallBackFn 成功要執行的fn
         * @param errorCallBackFn 失敗要執行的fn
         */
        POSTData(url, Data, successCallBackFn, errorCallBackFn) {
            $.ajax({
                type: "POST",
                url: url,
                data: Data,
                cache: false,
                success: function (data) {
                    if (typeof successCallBackFn === 'function') {
                        successCallBackFn(data);
                    }
                },
                error: function (xhr, status, error) {
                    if (typeof errorCallBackFn === 'function') {
                        errorCallBackFn(xhr, status, error);
                    }
                    else {
                        Utility.ErrorProcess.AjaxGetError(xhr, status, error);
                    }
                }
            });
        }
        /**
         * Ajax post
         * @param url 目標網址
         * @param jsonData json 物件
         * @param successCallBackFn 成功要執行的fn
         * @param errorCallBackFn 失敗要執行的fn
         */
        POSTJsonData(url, jsonData, successCallBackFn, errorCallBackFn) {
            var JQryAjxSetting: JQueryAjaxSettings = {
                url: url,
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: jsonData,
                cache: false,
                error: function (xhr, status, error) {
                    if (typeof errorCallBackFn === 'function') {
                        errorCallBackFn(xhr, status, error);
                    }
                    else {
                        Utility.ErrorProcess.AjaxGetError(xhr, status, error);
                    }
                },
                success: function (data) {
                    if (typeof successCallBackFn === 'function') {
                        successCallBackFn(data);
                    }
                }
            };
            $.ajax(JQryAjxSetting);

        }
        /**
         * Ajax post
         * @param url 目標網址
         * @param jsonData json 物件
         * @param successCallBackFn 成功要執行的fn
         * @param errorCallBackFn 失敗要執行的fn
         */
        POSTJsonDataDownloadfile(url, jsonData, successCallBackFn, errorCallBackFn) {
            var JQryAjxSetting: JQueryAjaxSettings = {
                url: url,
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: jsonData,
                headers: {
                    Accept: "text/csv; charset=utf-8",
                },
                cache: false,
                error: function (xhr, status, error) {
                    if (typeof errorCallBackFn === 'function') {
                        errorCallBackFn(xhr, status, error);
                    }
                    else {
                        Utility.ErrorProcess.AjaxGetError(xhr, status, error);
                    }
                },
                success: function (data) {
                    if (typeof successCallBackFn === 'function') {
                        successCallBackFn(data);
                    }
                }
            };
            $.ajax(JQryAjxSetting);

        }

    }
    /**
     * 遮罩對話框
     */
    export class LightBox {
        /**
         * 確認對話框
         * @param title 標題
         * @param content 內容
         * @param leftBtnText 左邊按鈕文字
         * @param rightBtnText 右邊按鈕文字
         * @param leftBtnCallBackFunc 左邊按鈕按鈕事件
         * @param rightBtnCallBackFunc 右邊按鈕按鈕事件
         */
        static confirm(title, content, leftBtnText, rightBtnText, leftBtnCallBackFunc, rightBtnCallBackFunc) {
            let confirmModal = $('<div class="modal fade" id="new_Detail" tabindex="-1" role="dialog" aria-labelledby="detailLabel" aria-hidden="true">' +
                '    <div class="modal-dialog">' +
                '        <div class="modal-content">' +
                '            <div class="modal-header">' +
                '                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '                <h4 class="modal-title" id="detailLabel">' + title + '</h4>' +
                '            </div>' +
                '            <div class="modal-body">' +
                '                <p class="text-center">' + content + '</p>' +
                '            </div>' +
                '            <div class="modal-footer">' +
                '                <button type="button" id="leftBtn" class="btn btn-default" data-dismiss="modal">' + leftBtnText + '</button>' +
                '                <button type="button"  id="rightBtn" class="btn btn-primary" data-dismiss="modal" onclick="">' + rightBtnText + '</button>' +
                '            </div>' +
                '        </div><!-- /.modal-content -->' +
                '    </div><!-- /.modal -->' +
                '</div>');
            confirmModal.find('#leftBtn').click(function (event) {
                if (typeof leftBtnCallBackFunc === 'function') {
                    leftBtnCallBackFunc();
                }
                (<any>confirmModal).modal('hide');
            });
            confirmModal.find('#rightBtn').click(function (event) {
                if (typeof rightBtnCallBackFunc === 'function') {
                    rightBtnCallBackFunc();
                }
                (<any>confirmModal).modal('hide');
            });
            (<any>confirmModal).modal('show');
        }
        /**
         * 提醒對話框
         * @param title 標題
         * @param content 內容
         * @param okButtonTxt 按鈕文字
         * @param okCallBackFunction 按鈕事件
         * @returns
         */
        static alert(title, content, okButtonTxt, okCallBackFunction) {
            let result = '';
            let wait = true;
            let alertModel = $('<div class="modal fade" id="new_Detail" tabindex="-1" role="dialog" aria-labelledby="detailLabel" aria-hidden="true">' +
                '    <div class="modal-dialog">' +
                '        <div class="modal-content">' +
                '            <div class="modal-header">' +
                '                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '                <h4 class="modal-title" id="detailLabel">' + title + '</h4>' +
                '            </div>' +
                '            <div class="modal-body">' +
                '                <p class="text-center">' + content + '</p>' +
                '            </div>' +
                '            <div class="modal-footer">' +
                '                <button type="button" id="okButton" class="btn btn-default" data-dismiss="modal">' + okButtonTxt + '</button>' +
                '            </div>' +
                '        </div><!-- /.modal-content -->' +
                '    </div><!-- /.modal -->' +
                '</div>');
            alertModel.find('#okButton').click(function (event) {
                (<any>alertModel).modal('hide');
                if (typeof okCallBackFunction === 'function') {
                    okCallBackFunction();
                }
            });
            (<any>alertModel).modal('show');
            return alertModel;
        }
    }
    /**
     * 當呼叫Ajax的時所要套用的錯誤處理預設function
     * @param xhr
     * @param status
     * @param error
     */
    export class ErrorProcess {
        static AjaxGetError(xhr, status, error) {
            if (xhr.status === 440) { //逾時
                Utility.LightBox.alert("訊息", xhr.responseText, "確定", function () {
                    location.href = "/Account/Login";
                });
            }
            else {
                Utility.LightBox.alert("訊息", xhr.responseText.replace(/\\r\\n/g, "<br>"), "確定", null);
            }
        }
    }
}

