var AS_LandBank;
(function (AS_LandBank) {
    var AssetClassifies = (function () {
        function AssetClassifies() {
        }
        AssetClassifies.GetMainKinds = function (successFn) {
            var JQryAjxSetting = {
                url: "/AssetCategory/GetMainKindList",
                type: "GET",
                contentType: "application/json; charset=utf-8",
                async: true,
                error: function (xhr, status, error) { alert(error); },
                success: function (data) {
                    if (typeof successFn === "function") {
                        successFn(data);
                    }
                },
            };
            $.ajax(JQryAjxSetting);
        };
        AssetClassifies.GetDetailKinds = function (mainKindNo, successFn) {
            var JQryAjxSetting = {
                url: "/AssetCategory/GetDetailKindList?" + "&mainKindNo=" + mainKindNo,
                type: "GET",
                contentType: "application/json; charset=utf-8",
                async: true,
                error: function (xhr, status, error) { alert(error); },
                success: function (data) {
                    if (typeof successFn === "function") {
                        successFn(data);
                    }
                },
            };
            $.ajax(JQryAjxSetting);
        };
        AssetClassifies.GetUseTypes = function (successFn) {
            var JQryAjxSetting = {
                url: "/AssetCategory/GetUseTypeList",
                type: "GET",
                contentType: "application/json; charset=utf-8",
                async: true,
                error: function (xhr, status, error) { alert(error); },
                success: function (data) {
                    if (typeof successFn === "function") {
                        successFn(data);
                    }
                },
            };
            $.ajax(JQryAjxSetting);
        };
        AssetClassifies.AddNewMainKind = function (data, successFn) {
            alert(JSON.stringify(data));
        };
        AssetClassifies.AddNewDetailKind = function (data, successFn) {
            alert(JSON.stringify(data));
        };
        AssetClassifies.AddNewUseType = function (data, successFn) {
            alert(JSON.stringify(data));
        };
        AssetClassifies.AddNewCategory = function (data, successFn) {
            alert(JSON.stringify(data));
        };
        return AssetClassifies;
    }());
    AS_LandBank.AssetClassifies = AssetClassifies;
})(AS_LandBank || (AS_LandBank = {}));
//# sourceMappingURL=AssertCategory.js.map