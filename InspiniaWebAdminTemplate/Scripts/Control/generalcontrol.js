﻿//Bind選項
function BindItem(target, codeclass, selectedvalue, mode) {
    $.ajax({
        url: G_GetItemUrl,
        data: 'CodeClass=' + codeclass,
        type: 'post',
        dataType: 'json',
        success: function (data) {
            //先清
            $("#" + target + " option").remove();
            $("#" + target).append($("<option></option>").attr("value", "").text("請選擇"));

            $.each(data, function (index, value) {
                //純顯示Code
                if (mode && mode == "C")
                {
                    $("#" + target).append($("<option></option>").attr("value", value.Value).html(value.Value));
                }
                else
                {
                    $("#" + target).append($("<option></option>").attr("value", value.Value).html(value.Value + value.Text));
                }
            });

            if (selectedvalue != '') {
                $("#" + target).val(selectedvalue);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

//Bind CheckBoxList
function BindItemToCheckBox(target, codeclass, selectedvalue) {
    $.ajax({
        url: G_GetItemUrl,
        data: 'CodeClass=' + codeclass,
        type: 'post',
        dataType: 'json',
        success: function (data) {
            //先清
            $("input[type='checkbox'][group='" + target + "']").remove();
            //加checkbox
            $.each(data, function (index, value) {
                $("#" + target).append('<input type="checkbox" group="' + target + '"  value="' + value.Value + '" >' + value.Text);
            });
            //寫入事件
            $('#' + target + ' input[type="checkbox"]').change(function () {
                var allvalue = '';
                $('#' + target + ' input[type="checkbox"]:checked').each(function () {
                    allvalue = allvalue + $(this).val() + ',';
                });
                $('#hd' + target).val(allvalue);
            });

            if (selectedvalue != '') {
                
                var arry = selectedvalue.split(",");
                for (var i = 0; i < arry.length; i++)
                {
                   $('#' + target + ' input[type="checkbox"][value="' + arry[i] + '"]').prop('checked', true);
                }
                $('#hd' + target).val(selectedvalue);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}


//Bind MainKind
function BindMain_Kind(targetid, mainkind, detailkind, accounttargetid, assetmainkindtype) {
    $.ajax({
        url: G_GetMain_Kind,
        data: 'Type=' + assetmainkindtype,
        type: 'post',
        success: function (data) {
            //先清
            $("#" + targetid + "_Main_Kind option").remove();
            $("#" + targetid + "_Main_Kind").append($("<option></option>").attr("value", "").text("請選擇"));
            //加MainKind
            $.each(data, function (index, value) {
                $("#" + targetid + "_Main_Kind").append($("<option></option>").attr("value", value.Value).html(value.Value + value.Text));
            });

            if (mainkind != '') {
                $("#" + targetid + "_Main_Kind").val(mainkind).change();
                BindDetail_Kind(targetid, mainkind, detailkind, accounttargetid);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

//Bind Detail_Kind
function BindDetail_Kind(targetid, mainkind, detailkind, accounttargetid) {
    $.ajax({
        url: G_GetAssetDetail_Kind,
        data: 'MainKind=' + mainkind,
        type: 'post',
        success: function (data) {
            //先清
            $("#" + targetid + "_Detail_Kind option").remove();
            $("#" + targetid + "_Detail_Kind").append($("<option></option>").attr("value", "").text("請選擇"));
            //加DetailKind
            $.each(data, function (index, value) {
                $("#" + targetid + "_Detail_Kind").append($("<option></option>").attr("value", value.Value).html(value.Value + value.Text));
            });

            if (detailkind != '') {
                $("#" + targetid + "_Detail_Kind").val(detailkind).change();
                GetAccountItem(accounttargetid, detailkind);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

//Bind Use_Types
function BindUse_Types(targetid, usetype) {
    $.ajax({
        url: G_GetAssetUse_Types,
        type: 'post',
        dataType: 'json',
        success: function (data) {
            //先清
            $("#" + targetid + "_Use_Types option").remove();
            $("#" + targetid + "_Use_Types").append($("<option></option>").attr("value", "").text("請選擇"));

            $.each(data, function (index, value) {
                $("#" + targetid + "_Use_Types").append($("<option></option>").attr("value", value.Value).html(value.Value + value.Text));
            });

            if (usetype != '') {
                $("#" + targetid + "_Use_Types").val(usetype).change();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

//Get AccountItem
function GetAccountItem(targetid, detailkind) {
    $.ajax({
        url: G_GetAccountItem,
        data: 'AssetDetailKind=' + detailkind,
        type: 'post',
        success: function (data) {
            //console.log(data);
            $('#' + targetid).html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

//Update Category Hidden
function UpdateCategoryHidden(targetid) {
    var main = $('#' + targetid + '_Main_Kind').val();
    var detail = $('#' + targetid + '_Detail_Kind').val();
    var usertype = $('#' + targetid + '_Use_Types').val();
    //塞完值直接觸發change事件
    $('#hd' + targetid).val(main + '.' + detail + '.' + usertype).change();
}

//SearchAssets
function SearchAssets(searchresultid, categorycode) {
    $.ajax({
        url: G_SearchAssets,
        type: 'post',
        data: 'CategoryCode=' + categorycode,
        dataType: 'json',
        success: function (data) {
            //先清
            $("#" + searchresultid + " option").remove();
            $("#" + searchresultid).append($("<option></option>").attr("value", "").text("請選擇"));

            $.each(data, function (index, value) {
                $("#" + searchresultid).append($("<option></option>").attr("value", value.Value).html(value.Value + value.Text));
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

//ResetForm
function ResetForm()
{
    $('input,select,textarea').not('[readonly],[disabled],:button').val('').prop('checked',false);
}
    


