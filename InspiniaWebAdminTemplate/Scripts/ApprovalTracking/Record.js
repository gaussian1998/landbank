﻿var app = new Vue({
    el: '#root',
    data: {
        items: [
            {
                No: 1,
                Date: '2017-04-15',
                DocNo: '060415A2010000001',
                FormType: '動產',
                FormName: 'A201動產新增單',
                Branch: '002 基隆分行',
                Applicant: '李先生',
                StartTime: '2017-04-15 8:30',
                EndTime: '2017-04-15 16:30',
                FormStatus: {
                    id: 2,
                    text: '通過'
                },
                ApprovalStatus: '簽審中',
                Remarks: ''
            },
            {
                No: 2,
                Date: '2017-04-15',
                DocNo: '060415A2010000002',
                FormType: '動產',
                FormName: 'A201動產新增單',
                Branch: '002 基隆分行',
                Applicant: '王科長',
                StartTime: '2017-04-15 16:30',
                EndTime: '2017-04-15 18:00',
                FormStatus: {
                    id: 2,
                    text: '通過'
                },
                ApprovalStatus: '簽審中',
                Remarks: ''
            },
            {
                No: 3,
                Date: '2017-04-15',
                DocNo: '060415A2010000003',
                FormType: '動產',
                FormName: 'A201動產新增單',
                Branch: '002 基隆分行',
                Applicant: '陳小姐',
                StartTime: '2017-04-15 18:00',
                EndTime: '',
                FormStatus: {
                    id: 0,
                    text: '進行中'
                },
                ApprovalStatus: '簽審中',
                Remarks: ''
            },
            {
                No: 4,
                Date: '2017-04-15',
                DocNo: '060415A2010000004',
                FormType: '動產',
                FormName: 'A201動產新增單',
                Branch: '002 基隆分行',
                Applicant: '李科長',
                StartTime: '',
                EndTime: '',
                FormStatus: {
                    id: 1,
                    text: ''
                },
                ApprovalStatus: '',
                Remarks: ''
            }
        ]
    },
    methods: {
        iconClass(status) {
            switch (status) {
                case 0:
                    return "glyphicon glyphicon-flag fa-2x";
                case 1:
                    return "glyphicon glyphicon-time fa-2x";
                case 2:
                    return "glyphicon glyphicon-ok fa-2x text-info";
                case 3:
                    return "glyphicon glyphicon-remove fa-2x text-danger";

            }
        },
        listClass(status) {
            return status ? null : "active";
        },
        listStyle(status) {
            return status ? null : { borderLeft: 'none', background: 'none' };
        }
    }
})
