﻿var app = new Vue({
    el: '#root',
    data: {
        items: [
            {
                No: 1,
                DispatchDate: "2017-04-05",
                DocNo: '060415A2010000001',
                FormType: '動產',
                FormName: 'A201動產新增單',
                Branch: '002基隆分行',
                Applicant: '李先生',
                CurrentApprover: '經辦一',
                NextApprover: '複核一',
                ApprovalState: '簽審中',
                ApprovalFlow: 'Aset001動產基本範本',
                Remarks: '退回申請者'
            },
            {
                No: 2,
                DispatchDate: "2017-04-05",
                DocNo: '060415A2010000002',
                FormType: '動產',
                FormName: 'A201動產新增單',
                Branch: '002基隆分行',
                Applicant: '王小姐',
                CurrentApprover: '經辦一',
                NextApprover: '複核一',
                ApprovalState: '簽審中',
                ApprovalFlow: 'Aset001動產基本範本',
                Remarks: null
            }
        ]
    },
    methods: {
    }
})
