var app = new Vue({
    el: '#app',
    template: '#app-template',
    data: {
        model: {
            table1: {
                attr1: "",
                attr2: "",
                attr3: "",
                attr4: "",
            },
            table2: [
                {
                    col1: '2016/03/25',
                    col2: '2016/05/01'
                },
                {
                    col1: "2016/04/21",
                    col2: "2016/06/02"
                },
            ], 
        }
    },
    methods: {
        addNewItem2Table2: function() {
            this.model.table2.push({
                col1: "",
                col2: "",
            })
        },
        submitTest: function(para) {
            alert(para)
            console(para)
        }
    }
})