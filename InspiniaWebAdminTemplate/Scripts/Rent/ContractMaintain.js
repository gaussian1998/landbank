﻿const BUILDING = 'building';
const LAND = 'land';
const PARKING_SPACE = 'parkingSpace';
const leasedAssetClassMap = {
[BUILDING]: [0, 1, 2, 4],
[LAND]: [0, 1, 2, 5],
[PARKING_SPACE]: [0, 1, 3],
};

var app = new Vue({
    el: '#root',
    data: {
        tabs: [BUILDING, LAND, PARKING_SPACE],
        activeTab: BUILDING,
        leasedAssetClass: 0,
        leasedAssetClassMap,
    },
    methods: {
        handleLeasedAssetClassChange() {
            for (var i = 0; i < this.tabs.length; i++) {
                if (this.leasedAssetClassMap[this.tabs[i]].indexOf(this.leasedAssetClass) > -1) {
                    this.activeTab = this.tabs[i];
                    break;
                }
            }
        },
    },
});
