var app = new Vue({
    el: '#app',
    template: '#app-template',
    data: {
        model: {
            table1: {
                attr1: "",
                attr2: "",
                attr3: "",
                attr4: "",
            },
            table2: [
                {
                    col1: '2016/03/25',
                    col2: '2016/05/01'
                },
                {
                    col1: "2016/04/21",
                    col2: "2016/06/02"
                },
            ],
            table3: [],
            table4: [],
            table5: [],
            table6: [],            
            table7: [],            
            table8: [],
            table9: [
                {
                    col1: 'aaa1',
                    col2: 'bbb1'
                },
                {
                    col1: "aaa2",
                    col2: "bbb2"
                },
            ],
            table10: [
                {
                    col1: 'aaa1',
                    col2: 'bbb1',
                    col3: 'ccc1',
                },
                {
                    col1: "aaa2",
                    col2: "bbb2",
                    col3: 'ccc2',
                },
            ],
        }
    },
    methods: {
        addNewItem2Table2: function() {
            this.model.table2.push({
                col1: "",
                col2: "",
            })
        },
        addNewItem2Table3: function() {
            this.model.table3.push({
                col1: "",
                col2: "",
            })
        },
        addNewItem2Table4: function() {
            this.model.table4.push({
                col1: "",
                col2: "",
            })
        },
        addNewItem2Table5: function() {
            this.model.table5.push({
                col1: "",
                col2: "",
            })
        },
        addNewItem2Table6: function() {
            this.model.table6.push({
                col1: "",
                col2: "",
            })
        },
        addNewItem2Table7: function() {
            this.model.table7.push({
                col1: "",
                col2: "",
            })
        },
        addNewItem2Table8: function() {
            this.model.table8.push({
                col1: "",
                col2: "",
            })
        },
        addNewItem2Table9: function() {
            this.model.table9.push({
                col1: "ccc",
                col2: "ddd",
            })
        },
        addNewItem2Table10: function() {
            this.model.table10.push({
                col1: "",
                col2: "new",
                col3: "new",
            })
        },
    }
})