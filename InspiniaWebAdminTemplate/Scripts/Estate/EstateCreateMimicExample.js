var app = new Vue({
    el: '#app',
    template: '#mimic-app-template',
    data: {
        model: {
            table1: {
                attr1: "",
                attr2: "",
                attr3: "",
                attr4: "",
            },
            table2: [],
            table3: [],
        }
    },
    methods: {
        addNewItem2Table2: function() {
            this.model.table2.push({
                col1: "",
                col2: "",
            })
        },
        addNewItem2Table3: function() {
            this.model.table3.push({
                col1: "",
                col2: "",
            })
        },
    }
})