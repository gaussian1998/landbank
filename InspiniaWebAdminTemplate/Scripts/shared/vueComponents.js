Vue.component('vue-datepicker', {
    template: '#vue-datepicker-template',
    props: ['value'],
    data: function() {
        return {
            formattedDate: ""
        }
    },
    mounted: function() {
        var vm = this;
        if (this.value != "" && this.value != undefined) {
            this.formattedDate = moment(new Date(vm.value)).format('YYYY/MM/DD');
            this.$emit('input', this.formattedDate);
        }

        $(this.$el)
            .val(vm.formattedDate)
            // init datepicker
            .datepicker({ autoclose: true, format: 'yyyy/mm/dd' })
            // emit event on change.
            .on('change', function() {
                vm.$emit('input', this.value);
            })
    },
    watch: {
        value: function(value) {
            if (value != "" && value != undefined) {
                //this.formattedDate = moment(new Date(vm.date)).format('YYYY/MM/DD');
                this.formattedDate = moment(new Date(value)).format('YYYY/MM/DD');
                $(this.$el).datepicker('val', moment(new Date(value)).format('YYYY/MM/DD'));
                this.$emit('input', this.formattedDate);
            } else {
                $(this.$el).datepicker('val', '');
                this.$emit('input', '');
            }
            // update value                    
            //$(this.$el).datepicker('val', value)
        }
    },
    destroyed: function() {
        $(this.$el).off().datepicker('destroy')
    }
})


Vue.component('vue-modal', {
    template: '#vue-modal-template',
    props: {

    },
    data: function() {
        return {
            modalId: ""
        }
    },
    mounted: function() {
        this.modalId = "modal-" + Math.uuid();
        $(this.$el).modal()
    },
    watch: {

    },
    destroyed: function() {

    }
})

Vue.component('app-ibox', {
    template: '#app-ibox',
    props: {
      title: {
        type: String,
      },
      collapsible: {
        type: Boolean,
        default: false,
      },
    }
})

Vue.component('app-formfield', {
    template: '#app-formfield',
    props: {
        col: {
            type: Number,
            default: 6,
        },
        title: {
            type: String,
        },
        required: {
            type: Boolean,
        },
    }
})