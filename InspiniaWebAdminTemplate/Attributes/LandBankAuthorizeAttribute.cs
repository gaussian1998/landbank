﻿using System.Web.Mvc;
using System.Web.Routing;
using Library.Servant.Servant.Authentication.Models;

namespace InspiniaWebAdminTemplate.Attributes
{
    public class LandBankAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (NotPass(filterContext))
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "Account", action = "Index" }));
            }
        }

        private static bool NotPass(AuthorizationContext filterContext)
        {
            return filterContext.HttpContext.User.Identity.Name == LoginResult.Anonymous().Name;
        }
    }
}