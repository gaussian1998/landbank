﻿using System.Web.Mvc;
using System.Linq;
using System.Web.Routing;

namespace InspiniaWebAdminTemplate.Attributes
{
    public class FunctionAuthorizeAttribute : AuthorizeAttribute
    {
        public string ID { get; set; }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if ( NotPass(filterContext) )
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "Account", action = "Index" }));
            }
        }

        private bool NotPass(AuthorizationContext filterContext)
        {
            return filterContext.HttpContext.User.IsInRole(ID) == false;
        } 
    }
}