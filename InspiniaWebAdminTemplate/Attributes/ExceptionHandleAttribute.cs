﻿using System.Data.Entity.Validation;
using System.Web.Mvc;
using System.Data.Entity.Infrastructure;
using Library.Common.Extension;
using Library.Common.Interface;
using Library.Entity.Extension;
using System.Text;
using System;
using Library.Servant.Servants.AbstractFactory;

namespace InspiniaWebAdminTemplate.Attributes
{
    public class ExceptionHandleAttribute : HandleErrorAttribute, 
        IVistor<DbEntityValidationException, DbUpdateException, Exception>
    {
        private string errorMessage = string.Empty;
        private ActionResult actionResult = null;
        public override void OnException(ExceptionContext context)
        {
            /*if (context.Exception.Accept(this))
            {
                context.ExceptionHandled = true;
                context.Result = IsAjax(context) ? AjaxRequestResult() : actionResult;
            }*/
        }

        public void visit(DbUpdateException ex)
        {
            errorMessage = "error# 0 ===>" + ex.Parser();
            actionResult = NormalRequestResult();
        }

        public void visit(DbEntityValidationException ex)
        {
            errorMessage = "error# 1 ===>" + ex.Parser();
            actionResult = NormalRequestResult();
        }

        public void visit(Exception ex)
        {
            errorMessage = ex.Message;
            actionResult = ServantAbstractFactory.ShowDetailsException() ? NormalRequestResult() : Error500();
        }

        private  ActionResult AjaxRequestResult()
        {
            return new ContentResult { Content = errorMessage, ContentEncoding = Encoding.UTF8 };
        }

        private  ActionResult NormalRequestResult()
        {
            var data = new ViewDataDictionary();
            data["ErrorMessage"] = errorMessage;
            return new ViewResult()
            {
                ViewName = "Error",
                ViewData = data
            };
        }

        private static ActionResult Error500()
        {
            return new ViewResult()
            {
                ViewName = "500"
            }; 
        }

        private bool IsAjax(ExceptionContext filterContext)
        {
            return filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
        }
    }
}