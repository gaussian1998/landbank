﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Attributes
{
    public class DetailsInformationAttribute : ActionFilterAttribute
    {
        private string parms;

        public DetailsInformationAttribute(string parms)
        {
            this.parms = parms;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            string details = JsonConvert.SerializeObject(filterContext.ActionParameters);
        }
    }
}