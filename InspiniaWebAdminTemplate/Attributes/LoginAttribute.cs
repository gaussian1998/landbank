﻿using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Servant.Servant.UserInformation.Models;
using System.Security.Principal;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace InspiniaWebAdminTemplate.Attributes
{
    public class LoginAttribute : ActionFilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            //HttpContext.Current.Session["MyValue"] = "Something";
            UserInfor user = UserInfoServant.Details(filterContext.HttpContext.ApplicationInstance.Context);
            filterContext.Principal = NewPrincipal(user);
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        { }

        private static IPrincipal NewPrincipal(UserInfor user)
        {
            var newIdentity = new GenericIdentity(user.Name);
            var newRoles = user.Functions.ToArray();

            return new GenericPrincipal(newIdentity, newRoles);
        }
    }
}