﻿using FastReport.Web;
using Library.Utility;
using System;
using System.Collections.Generic;
using Library.Common.Extension;

namespace InspiniaWebAdminTemplate.Extension
{
    public static class WebReportExtension
    {
        public static void Bind<Model>(this WebReport webReport, string FrxPath, List<Model> models)
        {
            webReport.Report.Load( FrxPath );
            webReport.RegisterData( MapProperty.ToDataSet(models) );
            webReport.Report.SetParameterValue( "PrintDate", DateTime.Now.ShortDescription());
        }
    }
}