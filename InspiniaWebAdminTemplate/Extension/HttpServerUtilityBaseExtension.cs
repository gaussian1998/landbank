﻿using System.Web;

namespace InspiniaWebAdminTemplate.Extension
{
    public static class HttpServerUtilityBaseExtension
    {
        public static string FrxPath(this HttpServerUtilityBase Server, string fileName)
        {
            return System.IO.Path.Combine(Server.MapPath("~/App_Data/Reports"), fileName);
        }
    }
}