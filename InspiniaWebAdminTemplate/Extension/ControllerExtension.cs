﻿using InspiniaWebAdminTemplate.Servants.Search;
using InspiniaWebAdminTemplate.Servants.UserInformation;
using Library.Common.Interface;
using Library.Servant.Servant.UserInformation.Models;
using System;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Extension
{
    public static class ControllerExtension
    {
        public static SearchViewModel GetSearchCondition<SearchViewModel>(this Controller controller)
            where SearchViewModel : IValidator, new()
        {
            return SearchConditionServant.Details<SearchViewModel>(controller.HttpContext.ApplicationInstance.Context);
        }

        public static void ModifySearchCondition<SearchViewModel>(this Controller controller, Action<SearchViewModel> modify)
            where SearchViewModel : IValidator, new()
        {
            SearchConditionServant.Update(controller.HttpContext.ApplicationInstance.Context, modify);
        }

        public static void UpdateSearchCondition<SearchViewModel>(this Controller controller, SearchViewModel vm)
        {
            SearchConditionServant.Update(vm, controller.HttpContext.ApplicationInstance.Context);
        }

        public static void ClearSearchCondition<SearchViewModel>(this Controller controller)
            where SearchViewModel : new()
        {
            SearchConditionServant.Update( new SearchViewModel(), controller.HttpContext.ApplicationInstance.Context);
        }

        public static int UserID(this Controller controller)
        {
            return UserInfoServant.Details(controller.HttpContext.ApplicationInstance.Context).ID;
        }

        public static UserInfor UserInformation(this Controller controller)
        {
            return UserInfoServant.Details(controller.HttpContext.ApplicationInstance.Context);
        }
    }
}