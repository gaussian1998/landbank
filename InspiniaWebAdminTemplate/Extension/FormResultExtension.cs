﻿using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servant.ApprovalEventListener.models;
using Newtonsoft.Json;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Extension
{
    public static class FormResultExtension
    {

        public static ActionResult Accept(this FormResult result ,FormResultVistor vistor)
        {
            switch (result.Type)
            {
                case 1:
                    return vistor.Visit(JsonConvert.DeserializeObject<PartialViewModel>(result.Data));

                case 2:
                    return vistor.Visit(JsonConvert.DeserializeObject<HttpGetViewModel>(result.Data));

                case 3:
                    return vistor.Visit(JsonConvert.DeserializeObject<FastReportViewModel>(result.Data));

                default:
                    return vistor.Visit(JsonConvert.DeserializeObject<NullViewModel>(result.Data));
            }
        }
    }

    public interface FormResultVistor
    {
        ActionResult Visit(PartialViewModel model);
        ActionResult Visit(HttpGetViewModel model);
        ActionResult Visit(FastReportViewModel model);
        ActionResult Visit(NullViewModel model);
    }
}