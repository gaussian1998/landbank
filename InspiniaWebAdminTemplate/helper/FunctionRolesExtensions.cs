﻿using System.Security.Principal;
using System.Web.Mvc;
using System.Linq;

namespace InspiniaWebAdminTemplate.helper
{
    public static class FunctionRolesExtensions
    {
        public static bool IsDisplay(this HtmlHelper helper, IPrincipal User, string FunctionID)
        {
            return User.IsInRole(FunctionID);
        }

        public static bool IsDisplay(this HtmlHelper helper, IPrincipal User, string[] FunctionsID)
        {
            return FunctionsID.Any(functionsID => User.IsInRole(functionsID));
        }
    }
}