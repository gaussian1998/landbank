﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Build
{
    public class BuildUserViewModel
    {
        public int ID { get; set; }
        public string User_Code { get; set; }
        public string User_Name { get; set; }
        public string Office_Branch { get; set; }
        public bool? Is_Active { get; set; }
    }
}