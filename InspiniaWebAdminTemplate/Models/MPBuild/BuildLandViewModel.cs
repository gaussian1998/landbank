﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Build
{
    public class BuildLandViewModel
    {
        public int ID { get; set; }
        public string Asset_Number { get; set; }
        public string Section { get; set; }
        public string Parent_Land_Number { get; set; }
        public string Filial_Land_Number { get; set; }
        public decimal Area_Size { get; set; }
        public decimal Authorized_Scope_Molecule { get; set; }
        public decimal Authorized_Scope_Denomminx { get; set; }
        public decimal Authorized_Area { get; set; }
    }
}