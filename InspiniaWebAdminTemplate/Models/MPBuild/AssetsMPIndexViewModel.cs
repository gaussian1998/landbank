﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Build
{
    public class AssetsMPIndexViewModel
    {
        public IEnumerable<AssetMPViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public AssetsMPSearchViewModel condition { get; set; }
        public List<BuildMPRetireViewModel> BuildMPRetireViewModel { get; set; }
        public List<BuildMPCategoryViewModel> BuildMPCategoryViewModel { get; set; }
        public List<BuildMPYearViewModel> BuildMPYearViewModel { get; set; }
        //public List<BuildMPUserViewModel> BuildUserViewModel { get; set; }
    }
}