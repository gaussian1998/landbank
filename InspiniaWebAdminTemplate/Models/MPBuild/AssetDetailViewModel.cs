﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Build
{
    public class AssetDetailViewModel
    {
        public int ID { get; set; }
        public int Assets_Land_Build_ID { get; set; }
        public string City_Code { get; set; }
        public string District_Code { get; set; }
        public string Section_Code { get; set; }
        public string Sub_Sectioni_Name { get; set; }
        public string Build_Number { get; set; }
        public string Build_Address { get; set; }
        public string Building_STRU { get; set; }
        public int Building_Total_Floor { get; set; }
        public string Authorization_Number { get; set; }
        public string Authorization_Name { get; set; }
        public string Asset_Type { get; set; }
        public string Floor_Uses { get; set; }
        public decimal Area_Size { get; set; }
        public decimal Authorized_Scope_Moldcule { get; set; }
        public decimal Authorized_Scope_Denominx { get; set; }
        public decimal Authorized_Area { get; set; }
        public System.DateTime Create_Time { get; set; }
        public string Created_By { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public string Last_Updated_By { get; set; }
    }
}