﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Build
{
    public class AssetsIndexViewModel
    {
        public IEnumerable<AssetViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public AssetsSearchViewModel condition { get; set; }
        public List<BuildRetireViewModel> BuildRetireViewModel { get; set; }
        public List<BuildCategoryViewModel> BuildCategoryViewModel { get; set; }
        public List<BuildYearViewModel> BuildYearViewModel { get; set; }
        public List<BuildUserViewModel> BuildUserViewModel { get; set; }


    }
}