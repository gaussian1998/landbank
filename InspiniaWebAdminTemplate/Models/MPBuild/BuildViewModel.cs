﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Build
{
    public class BuildViewModel
    {
        public BuildHeaderViewModel Header { get; set; }
        public BuildTRXHeaderViewModel TRXHeader { get; set; } //From AS_TRX_Headers
        public string OfficeBranch { get; set; }
        public IEnumerable<AssetViewModel> Assets { get; set; }
        public IEnumerable<BuildRetireViewModel> Retire { get; set; }
        public IEnumerable<BuildCategoryViewModel> Category { get; set; }
        public IEnumerable<BuildYearViewModel> Year { get; set; }
    }
}