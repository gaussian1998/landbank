﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Build
{
    public class BuildDisplayName
    {
        public static string GetBuildDisplayName(string FieldName)
        {
            string result = string.Empty;

            switch(FieldName)
            {
                case "Asset_Category_Code":
                    result = "資產分類";
                    break;
                case "Deprn_Method_Code":
                    result = "折舊方式";
                    break;
                case "Life_Years":
                    result = "年限年數";
                    break;
                case "Life_Months":
                    result = "年限月數";
                    break;
                case "Location_Disp":
                    result = "放置地點";
                    break;
                case "Description":
                    result = "摘要";
                    break;
                case "Urban_Renewal":
                    result = "都更案號";
                    break;
                case "City_Code":
                    result = "縣市代碼";
                    break;
                case "District_Code":
                    result = "鄉鎮市區代碼";
                    break;
                case "Section_Code":
                    result = "段代碼";
                    break;
                case "Sub_Sectioni_Name":
                    result = "小段名稱";
                    break;
                case "Build_Number":
                    result = "建號";
                    break;
                case "Building_Total_Floor":
                    result = "本建物總層數";
                    break;
                case "Own_Area":
                    result = "自用面積";
                    break;
                case "Building_STRU":
                    result = "構造";
                    break;
                case "Authorization_Number":
                    result = "權狀字號";
                    break;
                case "Authorization_Name":
                    result = "地政事務所";
                    break;
                case "Build_Address":
                    result = "房屋門牌";
                    break;
                case "Used_Type":
                    result = "業務別";
                    break;
                case "Used_Status":
                    result = "使用現況";
                    break;
                case "Obtained_Method":
                    result = "取得方式";
                    break;
                case "Original_Cost":
                    result = "取得成本";
                    break;
                case "Salvage_Value":
                    result = "殘值";
                    break;
                case "Current_Cost":
                    result = "帳面金額";
                    break;
                case "Business_Area_Size":
                    result = "營業用面積";
                    break;
                case "Remark1":
                    result = "備註事項1";
                    break;
                case "Remark2":
                    result = "備註事項2";
                    break;
                case "NEW_Asset_Category_Code":
                    result = "異動後資產分類";
                    break;
                case "New_Life_Years":
                    result = "異動後年限年數";
                    break;
                case "New_Life_Months":
                    result = "異動後年限月數";
                    break;
                default:
                    result = FieldName;
                    break;
            }

            return result;
        }
    }
}