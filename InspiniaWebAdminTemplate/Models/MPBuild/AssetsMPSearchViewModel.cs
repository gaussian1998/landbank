﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Build
{
    public class AssetsMPSearchViewModel
    {
        public string Search_Asset_Category_Code { get; set; }
        public string Search_Parent_Asset_Category_Code { get; set; }
        public int? Page { get; set; }
        public int PageSize { get; set; }
        public List<string> UseAssetNumber { get; set; }

    }
}