﻿using Library.Servant.Servant.Build.BuildModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Build
{
    public class BuildYearViewModel
    {
        public int ID { get; set; }
        public int TRX_Header_ID { get; set; }
        public int Asset_Build_ID { get; set; }
        public decimal Serial_Number { get; set; }
        public string Asset_Number { get; set; }
        public decimal Old_Life_Years { get; set; }
        public Nullable<decimal> Old_Life_Months { get; set; }
        public Nullable<decimal> New_Life_Years { get; set; }
        public Nullable<decimal> New_Life_Months { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> Create_Time { get; set; }
        public Nullable<decimal> Created_By { get; set; }
        public string Created_By_Name { get; set; }
        public Nullable<System.DateTime> Last_UpDatetimed_Time { get; set; }
        public Nullable<decimal> Last_UpDatetimed_By { get; set; }
        public string Last_UpDatetimed_By_Name { get; set; }
        public List<BuildChangeRecord> BuildChangeRecord { get; set; }
    }
}