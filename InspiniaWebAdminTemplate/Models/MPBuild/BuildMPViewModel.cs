﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Build
{
    public class BuildMPViewModel
    {
        public BuildTRXHeaderViewModel TRXHeader { get; set; } //From AS_TRX_Headers
        public IEnumerable<AssetMPViewModel> Assets { get; set; }
        public IEnumerable<BuildMPRetireViewModel> Retire { get; set; }
        public IEnumerable<BuildMPCategoryViewModel> Category { get; set; }
        public IEnumerable<BuildMPYearViewModel> Year { get; set; }
        public BuildHeaderViewModel Header { get; set; }
    }
}