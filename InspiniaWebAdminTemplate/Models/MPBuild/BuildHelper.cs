﻿using Library.Servant.Servant.Build.BuildModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Build
{
    public class BuildHelper
    {
        #region== 共用 ==
        /// <summary>
        /// Controller取的特定的View
        /// </summary>
        /// <param name="ViewName">View名稱</param>
        /// <returns></returns>
        public static string GetBuildView(string ViewName)
        {
            return "~/Views/MPBuild/" + ViewName + ".cshtml";
        }

        /// <summary>
        /// 取得功能名稱
        /// </summary>
        /// <param name="Action"></param>
        /// <returns></returns>
        public static string GetFunctionTitle(string Action)
        {
            string FunctionName = "";
            switch (Action)
            {
                case "BuildAdded":
                    FunctionName = "房屋新增";
                    break;
                case "BuildMaintain":
                    FunctionName = "房屋資產維護";
                    break;
                case "BuildDisposal":
                    FunctionName = "房屋處分";
                    break;
                case "BuildReclassify":
                    FunctionName = "房屋重分類";
                    break;
                case "BuildLifeChange":
                    FunctionName = "房屋使用年限異動";
                    break;
                case "BuildMPAdded":
                    FunctionName = "重大組成新增";
                    break;
            }
            return FunctionName;
        }
        #endregion

        #region ==ViewModel To Model==
        public static AssetModel ToAssetModel(AssetViewModel viewmodel)
        {
            return new AssetModel()
            {
                ID = viewmodel.ID,
                Asset_Number = !string.IsNullOrEmpty(viewmodel.Asset_Number) ? viewmodel.Asset_Number : "",
                Asset_Category_Code = !string.IsNullOrEmpty(viewmodel.Asset_Category_Code) ? viewmodel.Asset_Category_Code : "",
                Deprn_Method_Code = !string.IsNullOrEmpty(viewmodel.Deprn_Method_Code) ? viewmodel.Deprn_Method_Code : "",
                Life_Years = viewmodel.Life_Years,
                Life_Months = viewmodel.Life_Months,
                Location_Disp = !string.IsNullOrEmpty(viewmodel.Location_Disp) ? viewmodel.Location_Disp : "",
                Parent_Asset_Number = !string.IsNullOrEmpty(viewmodel.Parent_Asset_Number) ? viewmodel.Parent_Asset_Number : "",
                Old_Asset_Number = !string.IsNullOrEmpty(viewmodel.Old_Asset_Number) ? viewmodel.Old_Asset_Number : "",
                Description = !string.IsNullOrEmpty(viewmodel.Description) ? viewmodel.Description : "",
                Authorization_Name = !string.IsNullOrEmpty(viewmodel.Authorization_Name) ? viewmodel.Authorization_Name : "",
                Authorization_Number = !string.IsNullOrEmpty(viewmodel.Authorization_Number) ? viewmodel.Authorization_Number : "",
                Building_STRU = !string.IsNullOrEmpty(viewmodel.Building_STRU) ? viewmodel.Building_STRU : "",
                Building_Total_Floor = viewmodel.Building_Total_Floor,
                Build_Address = !string.IsNullOrEmpty(viewmodel.Build_Address) ? viewmodel.Build_Address : "",
                Build_Number = !string.IsNullOrEmpty(viewmodel.Build_Number) ? viewmodel.Build_Number : "",
                City_Code = !string.IsNullOrEmpty(viewmodel.City_Code) ? viewmodel.City_Code : "",
                District_Code = !string.IsNullOrEmpty(viewmodel.District_Code) ? viewmodel.District_Code : "",
                Section_Code = !string.IsNullOrEmpty(viewmodel.Section_Code) ? viewmodel.Section_Code : "",
                Sub_Sectioni_Name = !string.IsNullOrEmpty(viewmodel.Sub_Sectioni_Name) ? viewmodel.Sub_Sectioni_Name : "",
                Used_Type = !string.IsNullOrEmpty(viewmodel.Used_Type) ? viewmodel.Used_Type : "",
                Used_Status = !string.IsNullOrEmpty(viewmodel.Used_Status) ? viewmodel.Used_Status : "",
                Obtained_Method = !string.IsNullOrEmpty(viewmodel.Obtained_Method) ? viewmodel.Obtained_Method : "",
                Original_Cost = viewmodel.Original_Cost,
                Salvage_Value = viewmodel.Salvage_Value,
                Deprn_Amount = viewmodel.Deprn_Amount,
                Reval_Adjustment_Amount = viewmodel.Reval_Adjustment_Amount,
                Business_Area_Size = viewmodel.Business_Area_Size,
                NONBusiness_Area_Size = viewmodel.NONBusiness_Area_Size,
                Current_Cost = viewmodel.Current_Cost,
                Reval_Reserve = viewmodel.Reval_Reserve,
                Business_Book_Amount = viewmodel.Business_Book_Amount,
                NONBusiness_Book_Amount = viewmodel.NONBusiness_Book_Amount,
                Deprn_Reserve = viewmodel.Deprn_Reserve,
                Business_Deprn_Reserve = viewmodel.Business_Deprn_Reserve,
                NONBusiness_Deprn_Reserve = viewmodel.NONBusiness_Deprn_Reserve,
                Date_Placed_In_Service = (viewmodel.Date_Placed_In_Service != null) ? viewmodel.Date_Placed_In_Service : DateTime.Now,
                Remark1 = !string.IsNullOrEmpty(viewmodel.Remark1) ? viewmodel.Remark1 : "",
                Remark2 = !string.IsNullOrEmpty(viewmodel.Remark2) ? viewmodel.Remark2 : "",
                Current_Units = viewmodel.Current_Units,
                Delete_Reason = !string.IsNullOrEmpty(viewmodel.Delete_Reason) ? viewmodel.Delete_Reason : "",
                Urban_Renewal = !string.IsNullOrEmpty(viewmodel.Urban_Renewal) ? viewmodel.Urban_Renewal : "",
                Not_Deprn_Flag = viewmodel.Not_Deprn_Flag,
                DEPRN_Counts = viewmodel.DEPRN_Counts,
                Create_Time = DateTime.Now,
                Created_By = viewmodel.Created_By,
                //Created_By = !string.IsNullOrEmpty(viewmodel.Created_By) ? viewmodel.Created_By : "",
                Last_Updated_Time = viewmodel.Last_Updated_Time,
                Last_Updated_By = viewmodel.Last_Updated_By,
                //Last_Updated_By = !string.IsNullOrEmpty(viewmodel.Last_Updated_By)?viewmodel.Last_Updated_By:"",
                Created_By_Name = viewmodel.Created_By_Name,
                Last_UpDatetimed_By_Name = viewmodel.Last_UpDatetimed_By_Name,
                BuildLands = (viewmodel.BuildLands != null) ? viewmodel.BuildLands.Select(o => new BuildLandModel()
                {
                    ID = o.ID,
                    Area_Size = o.Area_Size,
                    Asset_Number = o.Asset_Number,
                    Authorized_Area = o.Authorized_Area,
                    Authorized_Scope_Denomminx = o.Authorized_Scope_Denomminx,
                    Authorized_Scope_Molecule = o.Authorized_Scope_Molecule,
                    Filial_Land_Number = o.Filial_Land_Number,
                    Parent_Land_Number = o.Parent_Land_Number,
                    Section = o.Section
                }) : new List<BuildLandModel>(),
                BuildDetails = (viewmodel.BuildDetails != null) ? viewmodel.BuildDetails.Select(o=>new AssetDetailModel()
                {
                   Asset_Type = o.Asset_Type,
                   Floor_Uses = o.Floor_Uses,
                   Area_Size = o.Area_Size,
                   Authorized_Scope_Moldcule = o.Authorized_Scope_Moldcule,
                   Authorized_Scope_Denominx = o.Authorized_Scope_Denominx,
                   Authorized_Area = o.Authorized_Area
                }) : new List<AssetDetailModel>(),
                NeedRecord = viewmodel.NeedRecord

            };
        }

        public static AssetMPModel ToAssetMPModel(AssetMPViewModel viewmodel)
        {
            return new AssetMPModel()
            {
                ID = viewmodel.ID,
                Asset_Build_ID = viewmodel.Asset_Build_ID,
                Assets_Build_MP_ID = viewmodel.Assets_Build_MP_ID,
                Asset_Number = viewmodel.Asset_Number,
                Parent_Asset_Number = !string.IsNullOrEmpty(viewmodel.Parent_Asset_Number) ? viewmodel.Parent_Asset_Number : "",
                Asset_Category_Code = viewmodel.Asset_Category_Code,
                Old_Asset_Number = viewmodel.Old_Asset_Number,
                Book_Type = viewmodel.Book_Type,
                Date_Placed_In_Service = viewmodel.Date_Placed_In_Service,
                Current_Units = viewmodel.Current_Units,
                Deprn_Method_Code = viewmodel.Deprn_Method_Code,
                Life_Years = viewmodel.Life_Years,
                Life_Months = viewmodel.Life_Months,
                Location_Disp = !string.IsNullOrEmpty(viewmodel.Location_Disp) ? viewmodel.Location_Disp : "" ,
                Description = viewmodel.Description,
                PO_Number = viewmodel.PO_Number,
                PO_Destination = viewmodel.PO_Destination,
                Model_Number = viewmodel.Model_Number,
                Transaction_Date = viewmodel.Transaction_Date,
                Assets_Unit = viewmodel.Assets_Unit,
                Accessory_Equipment = viewmodel.Accessory_Equipment,
                Assigned_NUM = viewmodel.Assigned_NUM,
                Asset_Category_NUM = viewmodel.Asset_Category_NUM,
                Asset_Category_Name = viewmodel.Asset_Category_Name,
                Asset_Structure = viewmodel.Asset_Structure,
                Current_Cost = viewmodel.Current_Cost,
                Deprn_Reserve = viewmodel.Deprn_Reserve,
                Salvage_Value = viewmodel.Salvage_Value,
                Remark = !string.IsNullOrEmpty(viewmodel.Remark) ? viewmodel.Remark : "",
                Deprn_Type = viewmodel.Deprn_Type,
                Not_Deprn_Flag = viewmodel.Not_Deprn_Flag,
                Create_Time = viewmodel.Create_Time,
                Created_By = viewmodel.Created_By,
                Created_By_Name = viewmodel.Created_By_Name,
                Last_UpDatetimed_Time = viewmodel.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = viewmodel.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = viewmodel.Last_UpDatetimed_By_Name,
                NeedRecord = viewmodel.NeedRecord
            };

        }

        public static BuildTRXHeaderModel ToBuildTRXHeaderModel(BuildTRXHeaderViewModel viewmodel)
        {
            //dbo.AS_TRX_Headers
            return new BuildTRXHeaderModel()
            {
                ID = viewmodel.ID,
                Form_Number = viewmodel.Form_Number,
                Office_Branch = viewmodel.Office_Branch,
                Book_Type_Code = viewmodel.Book_Type_Code,
                Transaction_Type = viewmodel.Transaction_Type,
                Transaction_Status = viewmodel.Transaction_Status,
                Transaction_Datetime_Entered = viewmodel.Transaction_Datetime_Entered,
                Accounting_Datetime = viewmodel.Accounting_Datetime,
                Description = viewmodel.Description,
                Amotized_Adjustment_Flag = viewmodel.Amotized_Adjustment_Flag,
                Create_Time = viewmodel.Create_Time,
                Created_By = viewmodel.Created_By,
                Created_By_Name = viewmodel.Created_By_Name,
                Last_UpDatetimed_Time = viewmodel.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = viewmodel.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = viewmodel.Last_UpDatetimed_By_Name,
            };
        }

        public static BuildHeaderModel ToBuildHeaderModel(BuildHeaderViewModel viewmodel)
        {
            return new BuildHeaderModel()
            {
                ID = viewmodel.ID,
                TRX_Header_ID = viewmodel.TRX_Header_ID,
                Transaction_Type = viewmodel.Transaction_Type,
                Transaction_Status = viewmodel.Transaction_Status,
                Book_Type = viewmodel.Book_Type,
                Transaction_Datetime_Entered = viewmodel.Transaction_Datetime_Entered,
                Accounting_Datetime = viewmodel.Accounting_Datetime,
                Office_Branch = viewmodel.Office_Branch,
                Description = viewmodel.Description,
                Create_Time = viewmodel.Create_Time,
                Created_By = viewmodel.Created_By,
                Last_Updated_Time = viewmodel.Last_Updated_Time,
                Last_Updated_By = viewmodel.Last_Updated_By
            };
        }

        public static AssetsSearchModel ToAssetsSearchModel(AssetsSearchViewModel viewmodel)
        {
            return new AssetsSearchModel()
            {
                 AssetNumber = viewmodel.AssetNumber,
                 City_Code = viewmodel.City_Code,
                 District_Code = viewmodel.District_Code,
                 Build_Number = viewmodel.Build_Number,
                 Section_Code = viewmodel.Section_Code,
                 Page = viewmodel.Page,
                 PageSize = viewmodel.PageSize,
                 UseAssetNumber=viewmodel.UseAssetNumber
            };
        }

        public static AssetsMPSearchModel ToAssetsMPSearchModel(AssetsMPSearchViewModel viewmodel)
        {
            return new AssetsMPSearchModel()
            {
                Search_Asset_Category_Code = viewmodel.Search_Asset_Category_Code,
                Search_Parent_Asset_Category_Code = viewmodel.Search_Parent_Asset_Category_Code,
                Page = viewmodel.Page,
                PageSize = viewmodel.PageSize,
                UseAssetNumber = viewmodel.UseAssetNumber,
            };
        }

        public static SearchModel ToSearchModel(SearchViewModel viewmodel)
        {
            return new SearchModel()
            {
                 Accounting_Datetime = viewmodel.Accounting_Datetime,
                 Book_Type_Code = viewmodel.Book_Type_Code,
                 Description = viewmodel.Description,
                 Office_Branch = viewmodel.Office_Branch,
                 Page = viewmodel.Page,
                 PageSize = viewmodel.PageSize,
                 Transaction_Datetime_Entered = viewmodel.Transaction_Datetime_Entered,
                 Transaction_Status = viewmodel.Transaction_Status,
                 Transaction_Type = viewmodel.Transaction_Type,
                 TRX_Header_ID = viewmodel.TRX_Header_ID,
                 Form_Number = viewmodel.Form_Number
            };
        }

        public static BuildRetireModel ToBuildRetireModel(BuildRetireViewModel viewmodel)
        {
            return new BuildRetireModel()
            {
                ID = viewmodel.ID,
                TRX_Header_ID = viewmodel.TRX_Header_ID,
                Asset_Build_ID = viewmodel.Asset_Build_ID,
                Serial_Number = viewmodel.Serial_Number,
                Asset_Number = viewmodel.Asset_Number,
                Parent_Asset_Number = viewmodel.Parent_Asset_Number,
                Old_Cost = viewmodel.Old_Cost,
                Retire_Cost = viewmodel.Retire_Cost,
                Sell_Amount = viewmodel.Sell_Amount,
                Sell_Cost = viewmodel.Sell_Cost,
                New_Cost = viewmodel.New_Cost,
                Reval_Adjustment_Amount = viewmodel.Reval_Adjustment_Amount,
                Reval_Reserve = viewmodel.Reval_Reserve,
                Reval_Land_VAT = viewmodel.Reval_Land_VAT,
                Reason_Code = viewmodel.Reason_Code,
                Description = viewmodel.Description,
                Account_Type = viewmodel.Account_Type,
                Receipt_Type = viewmodel.Receipt_Type,
                Retire_Cost_Account = viewmodel.Retire_Cost_Account,
                Sell_Amount_Account = viewmodel.Sell_Amount_Account,
                Sell_Cost_Account = viewmodel.Sell_Cost_Account,
                Reval_Adjustment_Amount_Account = viewmodel.Reval_Adjustment_Amount_Account,
                Reval_Reserve_Account = viewmodel.Reval_Reserve_Account,
                Reval_Land_VAT_Account = viewmodel.Reval_Land_VAT_Account,
                Create_Time = viewmodel.Create_Time,
                Created_By = viewmodel.Created_By,
                Created_By_Name = viewmodel.Created_By_Name,
                Last_UpDatetimed_Time = viewmodel.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = viewmodel.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = viewmodel.Last_UpDatetimed_By_Name
            };
        }

        public static BuildCategoryModel ToBuildCategoryModel(BuildCategoryViewModel viewmodel)
        {
            return new BuildCategoryModel()
            {
                ID = viewmodel.ID,
                TRX_Header_ID = viewmodel.TRX_Header_ID,
                Asset_Build_ID = viewmodel.Asset_Build_ID,
                Serial_Number = viewmodel.Serial_Number,
                Asset_Number = viewmodel.Asset_Number,
                Old_Asset_Category_Code = viewmodel.Old_Asset_Category_Code,
                NEW_Asset_Category_Code = viewmodel.NEW_Asset_Category_Code,
                Description = viewmodel.Description,
                Expense_Account = viewmodel.Expense_Account,
                Create_Time = viewmodel.Create_Time,
                Created_By = viewmodel.Created_By,
                Created_By_Name = viewmodel.Created_By_Name,
                Last_UpDatetimed_Time = viewmodel.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = viewmodel.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = viewmodel.Last_UpDatetimed_By_Name,

                BuildChangeRecord = viewmodel.BuildChangeRecord
            };
        }
        public static BuildYearModel ToBuildYearModel(BuildYearViewModel viewmodel)
        {
            return new BuildYearModel()
            {
                ID = viewmodel.ID,
                TRX_Header_ID = viewmodel.TRX_Header_ID,
                Asset_Build_ID = viewmodel.Asset_Build_ID,
                Serial_Number = viewmodel.Serial_Number,
                Asset_Number = viewmodel.Asset_Number,
                Old_Life_Years = viewmodel.Old_Life_Years,
                Old_Life_Months = viewmodel.Old_Life_Months,
                New_Life_Years = viewmodel.New_Life_Years,
                New_Life_Months = viewmodel.New_Life_Months,
                Description = viewmodel.Description,
                Create_Time = viewmodel.Create_Time,
                Created_By = viewmodel.Created_By,
                Created_By_Name = viewmodel.Created_By_Name,
                Last_UpDatetimed_Time = viewmodel.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = viewmodel.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = viewmodel.Last_UpDatetimed_By_Name,

                BuildChangeRecord = viewmodel.BuildChangeRecord
            };
        }
        public static BuildMPRetireModel ToBuildMPRetireModel(BuildMPRetireViewModel viewmodel)
        {
            return new BuildMPRetireModel()
            {
                ID = viewmodel.ID,
                TRX_Header_ID = viewmodel.TRX_Header_ID,
                Assets_Build_MP_ID = viewmodel.Assets_Build_MP_ID,
                Serial_Number = viewmodel.Serial_Number,
                Asset_Number = viewmodel.Asset_Number,
                Parent_Asset_Number = viewmodel.Parent_Asset_Number,
                Old_Cost = viewmodel.Old_Cost,
                Retire_Cost = viewmodel.Retire_Cost,
                Sell_Amount = viewmodel.Sell_Amount,
                Sell_Cost = viewmodel.Sell_Cost,
                New_Cost = viewmodel.New_Cost,
                Reval_Adjustment_Amount = viewmodel.Reval_Adjustment_Amount,
                Reval_Reserve = viewmodel.Reval_Reserve,
                Reval_Land_VAT = viewmodel.Reval_Land_VAT,
                Reason_Code = viewmodel.Reason_Code,
                Description = viewmodel.Description,
                Account_Type = viewmodel.Account_Type,
                Receipt_Type = viewmodel.Receipt_Type,
                Create_Time = viewmodel.Create_Time,
                Created_By = viewmodel.Created_By,
                Created_By_Name = viewmodel.Created_By_Name,
                Last_UpDatetimed_Time = viewmodel.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = viewmodel.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = viewmodel.Last_UpDatetimed_By_Name
            };
        }
        public static BuildMPCategoryModel ToBuildMPCategoryModel(BuildMPCategoryViewModel viewmodel)
        {
            return new BuildMPCategoryModel()
            {
                ID = viewmodel.ID,
                TRX_Header_ID = viewmodel.TRX_Header_ID,
                Asset_Build_MP_ID = viewmodel.Asset_Build_MP_ID,
                Serial_Number = viewmodel.Serial_Number,
                Asset_Number = viewmodel.Asset_Number,
                Old_Asset_Category_Code = viewmodel.Old_Asset_Category_Code,
                NEW_Asset_Category_Code = viewmodel.NEW_Asset_Category_Code,
                Description = viewmodel.Description,
                Expense_Account = viewmodel.Expense_Account,
                Create_Time = viewmodel.Create_Time,
                Created_By = viewmodel.Created_By,
                Created_By_Name = viewmodel.Created_By_Name,
                Last_UpDatetimed_Time = viewmodel.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = viewmodel.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = viewmodel.Last_UpDatetimed_By_Name,

                BuildMPChangeRecord = viewmodel.BuildMPChangeRecord
            };
        }
        public static BuildMPYearModel ToBuildMPYearModel(BuildMPYearViewModel viewmodel)
        {
            return new BuildMPYearModel()
            {
                ID = viewmodel.ID,
                TRX_Header_ID = viewmodel.TRX_Header_ID,
                Asset_Build_MP_ID = viewmodel.Asset_Build_MP_ID,
                Serial_Number = viewmodel.Serial_Number,
                Asset_Number = viewmodel.Asset_Number,
                Old_Life_Years = viewmodel.Old_Life_Years,
                Old_Life_Months = viewmodel.Old_Life_Months,
                New_Life_Years = viewmodel.New_Life_Years,
                New_Life_Months = viewmodel.New_Life_Months,
                Description = viewmodel.Description,
                Create_Time = viewmodel.Create_Time,
                Created_By = viewmodel.Created_By,
                Created_By_Name = viewmodel.Created_By_Name,
                Last_UpDatetimed_Time = viewmodel.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = viewmodel.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = viewmodel.Last_UpDatetimed_By_Name,

                BuildMPChangeRecord = viewmodel.BuildMPChangeRecord
            };
        }
        #endregion

        #region== Model To ViewModel ==
        public static BuildTRXHeaderViewModel ToBuildTRXHeaderViewModel(BuildTRXHeaderModel model)
        {
            return new BuildTRXHeaderViewModel()
            {
                ID = model.ID,
                Form_Number = model.Form_Number,
                Office_Branch = "001." + model.Office_Branch,
                Book_Type_Code = model.Book_Type_Code,
                Transaction_Type = model.Transaction_Type,
                Transaction_Status = model.Transaction_Status,
                Transaction_Status_Name = !string.IsNullOrEmpty(model.Transaction_Status) ? Library.Servant.Enums.Extend.GetName(model.Transaction_Status) : string.Empty,
                Transaction_Datetime_Entered = model.Transaction_Datetime_Entered,
                Accounting_Datetime = model.Accounting_Datetime,
                Description = model.Description,
                Amotized_Adjustment_Flag = model.Amotized_Adjustment_Flag,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Created_By_Name = model.Created_By_Name,
                Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = model.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name,
                Auto_Post = model.Auto_Post,
                OfficeBranchName = model.OfficeBranchName
            };
        }

        public static BuildHeaderViewModel ToBuildHeaderViewModel(BuildHeaderModel model)
        {
            return new BuildHeaderViewModel()
            {
                ID = model.ID,
                TRX_Header_ID = model.TRX_Header_ID,
                Transaction_Type = model.Transaction_Type,
                Transaction_Status = model.Transaction_Status,
                Book_Type = model.Book_Type,
                Transaction_Datetime_Entered = model.Transaction_Datetime_Entered,
                Accounting_Datetime = model.Accounting_Datetime,
                Office_Branch = model.Office_Branch,
                Description = model.Description,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By,
                Office_Branch_Name = model.Office_Branch_Name,
                Transaction_Status_Name = model.Transaction_Status_Name
            };
        }

        public static AssetViewModel ToAssetViewModel(AssetModel model)
        {
            return new AssetViewModel()
            {
                ID = model.ID,
                Asset_Number = model.Asset_Number,
                Asset_Category_Code = model.Asset_Category_Code,
                Deprn_Method_Code = model.Deprn_Method_Code,
                Life_Years = model.Life_Years,
                Life_Months = model.Life_Months,
                Location_Disp = model.Location_Disp,
                Parent_Asset_Number = model.Parent_Asset_Number,
                Old_Asset_Number = model.Old_Asset_Number,
                Description = model.Description,
                Authorization_Name = model.Authorization_Name,
                Authorization_Number = model.Authorization_Number,
                Building_STRU = model.Building_STRU,
                Building_Total_Floor = model.Building_Total_Floor,
                Build_Address = model.Build_Address,
                Build_Number = model.Build_Number,
                City_Code = model.City_Code,
                District_Code = model.District_Code,
                Section_Code = model.Section_Code,
                Sub_Sectioni_Name = model.Section_Code,
                Used_Type = model.Used_Type,
                Used_Status = model.Used_Status,
                Obtained_Method = model.Obtained_Method,
                Original_Cost = model.Original_Cost,
                Salvage_Value = model.Salvage_Value,
                Deprn_Amount = model.Deprn_Amount,
                Reval_Adjustment_Amount = model.Reval_Adjustment_Amount,
                Business_Area_Size = model.Business_Area_Size,
                NONBusiness_Area_Size = model.NONBusiness_Area_Size,
                Current_Cost = model.Current_Cost,
                Reval_Reserve = model.Reval_Reserve,
                Business_Book_Amount = model.Business_Book_Amount,
                NONBusiness_Book_Amount = model.NONBusiness_Book_Amount,
                Deprn_Reserve = model.Deprn_Reserve,
                Business_Deprn_Reserve = model.Business_Deprn_Reserve,
                NONBusiness_Deprn_Reserve = model.NONBusiness_Deprn_Reserve,
                Date_Placed_In_Service = model.Date_Placed_In_Service,
                Remark1 = model.Remark1,
                Remark2 = model.Remark2,
                Current_Units = model.Current_Units,
                Delete_Reason = model.Delete_Reason,
                Urban_Renewal = model.Urban_Renewal,
                Not_Deprn_Flag = model.Not_Deprn_Flag,
                DEPRN_Counts = model.DEPRN_Counts,
                BuildDetails = (model.BuildDetails != null && model.BuildDetails.Count() > 0) ? model.BuildDetails.Select(o=> ToAssetDetailViewModel(o)) : new List<AssetDetailViewModel>(),
                BuildLands = (model.BuildLands != null) ?  model.BuildLands.Select(o=>new BuildLandViewModel() {
                      ID  = o.ID,
                      Area_Size = o.Area_Size,
                      Asset_Number = o.Asset_Number,
                      Authorized_Area = o.Authorized_Area,
                      Authorized_Scope_Denomminx = o.Authorized_Scope_Denomminx,
                      Authorized_Scope_Molecule = o.Authorized_Scope_Molecule,
                      Filial_Land_Number = o.Filial_Land_Number,
                      Parent_Land_Number = o.Parent_Land_Number,
                      Section = o.Section
                }) : new List<BuildLandViewModel>(),
                BuildCategory = (model.BuildCategory != null )? ToBuildCategoryViewModel(model.BuildCategory) : new BuildCategoryViewModel(),
                BuildRetire = (model.BuildRetire != null )? ToBuildRetireViewModel(model.BuildRetire) : new BuildRetireViewModel(),
                BuildYear = (model.BuildYear != null) ? ToBuildYearViewModel(model.BuildYear) : new BuildYearViewModel(),
                BuildChangeRecord = (model.BuildChangeRecord != null) ? model.BuildChangeRecord : new List<BuildChangeRecord>(),
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By,
                Created_By_Name = model.Created_By_Name,
                Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name
            };
        }

        public static AssetMPViewModel ToAssetMPViewModel(AssetMPModel model)
        {
            return new AssetMPViewModel()
            {
                ID = model.ID,
                Asset_Build_ID = model.Asset_Build_ID,
                Assets_Build_MP_ID = model.Assets_Build_MP_ID,
                Asset_Number = model.Asset_Number,
                Parent_Asset_Number = model.Parent_Asset_Number,
                Asset_Category_Code = model.Asset_Category_Code,
                Old_Asset_Number = model.Old_Asset_Number,
                Book_Type = model.Book_Type,
                Date_Placed_In_Service = model.Date_Placed_In_Service,
                Current_Units = model.Current_Units,
                Deprn_Method_Code = model.Deprn_Method_Code,
                Life_Years = model.Life_Years,
                Life_Months = model.Life_Months,
                Location_Disp = model.Location_Disp,
                Description = model.Description,
                PO_Number = model.PO_Number,
                PO_Destination = model.PO_Destination,
                Model_Number = model.Model_Number,
                Transaction_Date = model.Transaction_Date,
                Assets_Unit = model.Assets_Unit,
                Accessory_Equipment = model.Accessory_Equipment,
                Assigned_NUM = model.Assigned_NUM,
                Asset_Category_NUM = model.Asset_Category_NUM,
                Asset_Category_Name= model.Asset_Category_Name,
                Asset_Structure = model.Asset_Structure,
                Current_Cost = model.Current_Cost,
                Deprn_Reserve = model.Deprn_Reserve,
                Salvage_Value = model.Salvage_Value,
                Remark = model.Remark,
                Deprn_Type = model.Deprn_Type,
                Not_Deprn_Flag = model.Not_Deprn_Flag,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Created_By_Name = model.Created_By_Name,
                Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = model.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name,
                BuildRetire = (model.BuildRetire != null )? ToBuildMPRetireViewModel(model.BuildRetire) : new BuildMPRetireViewModel(),
                BuildCategory = (model.BuildCategory != null )? ToBuildMPCategoryViewModel(model.BuildCategory) : new BuildMPCategoryViewModel(),
                BuildYear = (model.BuildYear != null )? ToBuildMPYearViewModel(model.BuildYear) : new BuildMPYearViewModel(),
                BuildMPChangeRecord = (model.BuildMPChangeRecord != null) ? model.BuildMPChangeRecord : new List<BuildMPChangeRecord>()
            };
        }

        public static AssetDetailViewModel ToAssetDetailViewModel(AssetDetailModel model)
        {
            return new AssetDetailViewModel()
            {
                Asset_Type = model.Asset_Type,
                Floor_Uses = model.Floor_Uses,
                Area_Size = model.Area_Size,
                Authorized_Scope_Moldcule = model.Authorized_Scope_Moldcule,
                Authorized_Scope_Denominx = model.Authorized_Scope_Denominx,
                Authorized_Area = model.Authorized_Area
            };
        }

        public static SearchViewModel ToSearchViewModel(SearchModel model)
        {
            return new SearchViewModel()
            {
                 Transaction_Type = model.Transaction_Type,
                 Accounting_Datetime = model.Accounting_Datetime,
                 Book_Type = model.Book_Type,
                 Description = model.Description,
                 Office_Branch = model.Office_Branch,
                 Page = model.Page,
                 PageSize = model.PageSize,
                 Transaction_Datetime_Entered = model.Transaction_Datetime_Entered,
                 Transaction_Status = model.Transaction_Status,
                 Form_Number = model.Form_Number
            };
        }

        public static BuildRetireViewModel ToBuildRetireViewModel(BuildRetireModel model)
        {
            return new BuildRetireViewModel()
            {
                ID = model.ID,
                TRX_Header_ID = model.TRX_Header_ID,
                Asset_Build_ID = model.Asset_Build_ID,
                Serial_Number = model.Serial_Number,
                Asset_Number = model.Asset_Number,
                Parent_Asset_Number = model.Parent_Asset_Number,
                Old_Cost = model.Old_Cost,
                Retire_Cost = model.Retire_Cost,
                Sell_Amount = model.Sell_Amount,
                Sell_Cost = model.Sell_Cost,
                New_Cost = model.New_Cost,
                Reval_Adjustment_Amount = model.Reval_Adjustment_Amount,
                Reval_Reserve = model.Reval_Reserve,
                Reval_Land_VAT = model.Reval_Land_VAT,
                Reason_Code = model.Reason_Code,
                Description = model.Description,
                Account_Type = model.Account_Type,
                Receipt_Type = model.Receipt_Type,
                Retire_Cost_Account = model.Retire_Cost_Account,
                Sell_Amount_Account = model.Sell_Amount_Account,
                Sell_Cost_Account = model.Sell_Cost_Account,
                Reval_Adjustment_Amount_Account = model.Reval_Adjustment_Amount_Account,
                Reval_Reserve_Account = model.Reval_Reserve_Account,
                Reval_Land_VAT_Account = model.Reval_Land_VAT_Account,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Created_By_Name = model.Created_By_Name,
                Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = model.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name
            };
        }

        public static BuildCategoryViewModel ToBuildCategoryViewModel(BuildCategoryModel model)
        {
            return new BuildCategoryViewModel()
            {
                ID = model.ID,
                TRX_Header_ID = model.TRX_Header_ID,
                Asset_Build_ID = model.Asset_Build_ID,
                Serial_Number = model.Serial_Number,
                Asset_Number = model.Asset_Number,
                Old_Asset_Category_Code = model.Old_Asset_Category_Code,
                NEW_Asset_Category_Code = model.NEW_Asset_Category_Code,
                Description = model.Description,
                Expense_Account = model.Expense_Account,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Created_By_Name = model.Created_By_Name,
                Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = model.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name
            };
        }

        public static BuildYearViewModel ToBuildYearViewModel(BuildYearModel model)
        {
            return new BuildYearViewModel()
            {
                ID = model.ID,
                TRX_Header_ID = model.TRX_Header_ID,
                Asset_Build_ID = model.Asset_Build_ID,
                Serial_Number = model.Serial_Number,
                Asset_Number = model.Asset_Number,
                Old_Life_Years = model.Old_Life_Years,
                Old_Life_Months = model.Old_Life_Months,
                New_Life_Years = model.New_Life_Years,
                New_Life_Months = model.New_Life_Months,
                Description = model.Description,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Created_By_Name = model.Created_By_Name,
                Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = model.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name
            };
        }
        public static BuildUserViewModel ToBuildUserViewModel(BuildUserModel model)
        {
            return new BuildUserViewModel()
            {
                ID = model.ID,
                User_Code = model.User_Code,
                User_Name = model.User_Name,
                Office_Branch = model.Office_Branch,
                Is_Active = model.Is_Active,

            };
        }
        public static BuildMPRetireViewModel ToBuildMPRetireViewModel(BuildMPRetireModel model)
        {
            return new BuildMPRetireViewModel()
            {
                ID = model.ID,
                TRX_Header_ID = model.TRX_Header_ID,
                Assets_Build_MP_ID = model.Assets_Build_MP_ID,
                Serial_Number = model.Serial_Number,
                Asset_Number = model.Asset_Number,
                Parent_Asset_Number = model.Parent_Asset_Number,
                Old_Cost = model.Old_Cost,
                Retire_Cost = model.Retire_Cost,
                Sell_Amount = model.Sell_Amount,
                Sell_Cost = model.Sell_Cost,
                New_Cost = model.New_Cost,
                Reval_Adjustment_Amount = model.Reval_Adjustment_Amount,
                Reval_Reserve = model.Reval_Reserve,
                Reval_Land_VAT = model.Reval_Land_VAT,
                Reason_Code = model.Reason_Code,
                Description = model.Description,
                Account_Type = model.Account_Type,
                Receipt_Type = model.Receipt_Type,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Created_By_Name = model.Created_By_Name,
                Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = model.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name
            };
        }
        public static BuildMPCategoryViewModel ToBuildMPCategoryViewModel(BuildMPCategoryModel model)
        {
            return new BuildMPCategoryViewModel()
            {
                ID = model.ID,
                TRX_Header_ID = model.TRX_Header_ID,
                Asset_Build_MP_ID = model.Asset_Build_MP_ID,
                Serial_Number = model.Serial_Number,
                Asset_Number = model.Asset_Number,
                Old_Asset_Category_Code = model.Old_Asset_Category_Code,
                NEW_Asset_Category_Code = model.NEW_Asset_Category_Code,
                Description = model.Description,
                Expense_Account = model.Expense_Account,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Created_By_Name = model.Created_By_Name,
                Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = model.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name
            };
        }
        public static BuildMPYearViewModel ToBuildMPYearViewModel(BuildMPYearModel model)
        {
            return new BuildMPYearViewModel()
            {
                ID = model.ID,
                TRX_Header_ID = model.TRX_Header_ID,
                Asset_Build_MP_ID = model.Asset_Build_MP_ID,
                Serial_Number = model.Serial_Number,
                Asset_Number = model.Asset_Number,
                Old_Life_Years = model.Old_Life_Years,
                Old_Life_Months = model.Old_Life_Months,
                New_Life_Years = model.New_Life_Years,
                New_Life_Months = model.New_Life_Months,
                Description = model.Description,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Created_By_Name = model.Created_By_Name,
                Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = model.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name
            };
        }
        #endregion
    }
}