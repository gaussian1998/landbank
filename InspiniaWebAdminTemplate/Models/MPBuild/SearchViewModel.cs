﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Build
{
    public class SearchViewModel
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }
        public string Transaction_Type { get; set; }
        public string Form_Number { get; set; }
        public string TRX_Header_ID { get; set; }
        public string Transaction_Status { get; set; }
        public string Book_Type { get; set; }
        public string Book_Type_Code { get; set; }
        public DateTime? Transaction_Datetime_Entered { get; set; }
        public DateTime? Accounting_Datetime { get; set; }
        public string Office_Branch { get; set; }
        public string Description { get; set; }
    }
}