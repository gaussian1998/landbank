﻿using Library.Servant.Servant.Build.BuildModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Build
{
    public class AssetMPViewModel
    {
        public int ID { get; set; }
        public int Asset_Build_ID { get; set; }
        public int Assets_Build_MP_ID { get; set; } //By AS_Assets_Build_MP_Record
        public int TRX_Header_ID { get; set; } //By AS_Assets_Build_MP_Record
        public string Asset_Number { get; set; }
        public string Parent_Asset_Number { get; set; }
        public string Asset_Category_Code { get; set; }
        public string Old_Asset_Number { get; set; }
        public string Book_Type { get; set; }
        public Nullable<System.DateTime> Date_Placed_In_Service { get; set; }
        public decimal Current_Units { get; set; }
        public string Deprn_Method_Code { get; set; }
        public decimal Life_Years { get; set; }
        public decimal Life_Months { get; set; }
        public string Location_Disp { get; set; }
        public string Description { get; set; }
        public string PO_Number { get; set; }
        public string PO_Destination { get; set; }
        public string Model_Number { get; set; }
        public System.DateTime Transaction_Date { get; set; }
        public string Assets_Unit { get; set; }
        public string Accessory_Equipment { get; set; }
        public string Assigned_NUM { get; set; }
        public string Asset_Category_NUM { get; set; }
        public string Asset_Structure { get; set; }
        public decimal Current_Cost { get; set; }
        public decimal Deprn_Reserve { get; set; }
        public decimal Salvage_Value { get; set; }
        public string Remark { get; set; }
        public System.DateTime Create_Time { get; set; }
        public decimal Created_By { get; set; }
        public string Created_By_Name { get; set; }
        public Nullable<System.DateTime> Last_UpDatetimed_Time { get; set; }
        public decimal Last_UpDatetimed_By { get; set; }
        public string Last_UpDatetimed_By_Name { get; set; }
        public Nullable<int> Deprn_Type { get; set; }
        public bool Not_Deprn_Flag { get; set; }
        public string Asset_Category_Name { get; set; }
        //------------------------------------------
        public string AssetInfoShow { get; set; }
        public string Asset_Category_Code_Name_Show { get; set; }
        public string Asset_Category_Code_Show { get; set; }
        public string Assigned_Name { get; set; }
        public BuildMPRetireViewModel BuildRetire { get; set; }
        public BuildMPCategoryViewModel BuildCategory { get; set; }
        public BuildMPYearViewModel BuildYear { get; set; }

        public List<BuildMPChangeRecord> BuildMPChangeRecord = new List<BuildMPChangeRecord>();
        public bool NeedRecord = false;

    }
}