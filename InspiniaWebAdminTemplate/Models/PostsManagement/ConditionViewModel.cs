﻿using Library.Servant.Servant.PostsManagement.Models;

namespace InspiniaWebAdminTemplate.Models.PostsManagement
{
    public class ConditionViewModel
    {
        public bool IsSaved { get; set; }
        public bool IsSubmited { get; set; }

        public static explicit operator ConditionViewModel(ConditionResult result)
        {
            return new ConditionViewModel
            {
                IsSaved = result.IsSaved,
                IsSubmited = result.IsSubmited
            };
        }
    }
}