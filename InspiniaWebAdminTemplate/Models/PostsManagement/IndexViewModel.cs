﻿using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.PostsManagement
{
    public class IndexViewModel
    {
        public IndexViewModel()
        {
            Condition = new SearchViewModel();
            Items = new List<ItemViewModel>();
        }

        public List<ItemViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public SearchViewModel Condition { get; set; }
    }
}