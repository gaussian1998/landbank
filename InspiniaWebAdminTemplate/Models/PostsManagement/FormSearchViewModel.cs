﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.PostsManagement.Models;

namespace InspiniaWebAdminTemplate.Models.PostsManagement
{
    public class FormSearchViewModel : AbstractPageModel<FormSearchViewModel>
    {
        public FormSearchViewModel()
        {
            Items = new List<SearchItemViewModel>();
            Condtiton = new SearchConditionViewModel();
        }

        public string FlowCode { get; set; }
        public List<SearchItemViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public SearchConditionViewModel Condtiton { get; set; }

        public static explicit operator FormSearchViewModel(SearchResult from)
        {
            return new FormSearchViewModel
            {
                Items = from.Items.Select(m => (SearchItemViewModel)m).ToList(),
                TotalAmount = from.TotalAmount
            };
        }
    }
}