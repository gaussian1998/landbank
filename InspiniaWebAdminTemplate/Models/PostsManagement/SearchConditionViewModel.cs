﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InspiniaWebAdminTemplate.Models.Common;

namespace InspiniaWebAdminTemplate.Models.PostsManagement
{
    public class SearchConditionViewModel
    {
        public SearchConditionViewModel() {
            BranchRange = new RangeViewModel<string>();
            PostRange = new RangeViewModel<string>();
            AnnounceRange = new RangeViewModel<DateTime>
            {
                Start = DateTime.Now,
                End = DateTime.Now
            };
        }

        public RangeViewModel<string> BranchRange { get; set; }
        public RangeViewModel<string> PostRange { get; set; }
        public RangeViewModel<DateTime> AnnounceRange { get; set; }
        public int DocumentType { get; set; }
    }
}