﻿using System;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Enums;
using Library.Servant.Servant.PostsManagement.Models;

namespace InspiniaWebAdminTemplate.Models.PostsManagement
{
    public class HeaderViewModel : AbstractPageModel<HeaderViewModel>
    {
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string FlowCode { get; set; }
        public DateTime FlowDate { get; set; }
        public FormType FlowType { get; set; }
        public string FlowName { get; set; }
        public int CreateBy { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }

        public static explicit operator HeaderModel(HeaderViewModel vm)
        {
            return new HeaderModel
            {
                BranchCode = vm.BranchCode,
                FlowCode = vm.FlowCode,
                FlowDate = vm.FlowDate,
                FlowType = vm.FlowType,
                FlowName = vm.FlowName,
                CreateBy = vm.CreateBy,
                ApprovalStatus = vm.ApprovalStatus
            };
        }

        public static explicit operator HeaderViewModel(HeaderModel from)
        {
            return new HeaderViewModel
            {
                BranchCode = from.BranchCode,
                FlowCode = from.FlowCode,
                FlowDate = from.FlowDate,
                FlowType = from.FlowType,
                FlowName = from.FlowName,
                CreateBy = from.CreateBy,
                ApprovalStatus = from.ApprovalStatus
            };
        }
    }
}