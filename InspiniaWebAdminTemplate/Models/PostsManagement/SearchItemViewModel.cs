﻿using System;
using Library.Servant.Servant.PostsManagement.Models;

namespace InspiniaWebAdminTemplate.Models.PostsManagement
{
    public class SearchItemViewModel
    {
        public string PostCode { get; set; }
        public string FlowCode { get; set; }
        public DateTime AnnounceDate { get; set; }
        public string Type { get; set; }
        public string AnnounceTitle { get; set; }
        public string AnnounceDepartment { get; set; }
        public string Principal { get; set; }

        public static explicit operator SearchItemViewModel(SearchItemResult from)
        {
            return new SearchItemViewModel
            {
                PostCode = from.PostCode,
                FlowCode = from.FlowCode,
                AnnounceDate = from.AnnounceDate, //todo_julius
                AnnounceDepartment = from.AnnounceDepartment,
                AnnounceTitle = from.AnnounceTitle,
                Principal = from.PrincipalName,
                Type = from.Type
            };
        }
    }
}