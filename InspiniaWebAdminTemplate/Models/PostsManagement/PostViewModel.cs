﻿using System;
using System.Collections.Generic;
using System.Linq;
using InspiniaWebAdminTemplate.Models.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.PostsManagement.Models;

namespace InspiniaWebAdminTemplate.Models.PostsManagement
{
    public class PostViewModel
    {
        public PostViewModel() {
            AnnounceRange = new RangeViewModel<DateTime>
            {
                Start = DateTime.Now,
                End = DateTime.Now.AddDays(1)
            };
            Links = new List<LinkViewModel>();
        }

        public string FlowCode { get; set; }
        public string NewFlowCode { get; set; }
        public string PostCode { get; set; }
        public string DocumentType { get; set; }
        public string AnnounceDepartment { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string FileName { get; set; }
        public RangeViewModel<DateTime> AnnounceRange { get; set; }
        public string Principal { get; set; }
        public List<LinkViewModel> Links { get; set; }

        public static explicit operator PostViewModel(PostInfo from)
        {
            return new PostViewModel
            {
                FlowCode = from.FlowCode,
                NewFlowCode = from.NewFlowCode,
                PostCode = from.PostCode,
                DocumentType = from.DocumentType,
                AnnounceDepartment = from.AnnounceDepartment,
                Title = from.Title,
                Content = from.Content,
                FileName = from.FileName,
                AnnounceRange = (RangeViewModel<DateTime>)from.AnnounceRange,
                Principal = from.Principal,
                Links = from.Links.Select(m => (LinkViewModel)m).ToList()
            };
        }

        public static explicit operator PostInfo(PostViewModel vm)
        {
            return new PostInfo
            {
                FlowCode = vm.FlowCode,
                NewFlowCode = vm.NewFlowCode,
                PostCode = vm.PostCode,
                DocumentType = vm.DocumentType,
                AnnounceDepartment = vm.AnnounceDepartment,
                Title = vm.Title,
                Content = vm.Content,
                FileName = vm.FileName,
                AnnounceRange = (RangeModel<DateTime>)vm.AnnounceRange,
                PrincipalID = vm.Principal,
                Links = vm.Links.Select(m => new OptionModel<string, string> { Text=m.Name, Value=m.Url }).ToList()
            };
        }
    }
}