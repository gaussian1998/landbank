﻿using System;
using System.Collections.Generic;
using System.Linq;
using InspiniaWebAdminTemplate.Models.Common;
using Library.Servant.Servant.PostsManagement.Models;

namespace InspiniaWebAdminTemplate.Models.PostsManagement
{
    public class FormIndexViewModel
    {
        public FormIndexViewModel() {
            Items = new List<ItemViewModel>();
            HeaderInfo = new HeaderViewModel();
            Conditions = new ConditionViewModel();
        }

        public List<ItemViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public HeaderViewModel HeaderInfo { get; set; }
        public ConditionViewModel Conditions { get; set; }

        public static explicit operator FormIndexViewModel(IndexResult from)
        {
            return new FormIndexViewModel
            {
                Items = from.Items.Select(m => (ItemViewModel)m).ToList(),
                TotalAmount = from.TotalAmount,
                HeaderInfo = (HeaderViewModel)from.HeaderInfo,
                Conditions = (ConditionViewModel)from.Conditions
            };
        }
    }
}