﻿using System;
using System.Linq;
using Library.Servant.Enums;
using Library.Servant.Servant.PostsManagement.Models;

namespace InspiniaWebAdminTemplate.Models.PostsManagement
{
    public class ItemViewModel
    {
        public string PostCode { get; set; }
        public string FlowCode { get; set; }
        public ApprovalStatus Status { get; set; }
        public DateTime AnnounceDate { get; set; }
        public string Type { get; set; }
        public string AnnounceTitle { get; set; }
        public string AnnounceDepartment { get; set; }
        public string Principal { get; set; }

        public static explicit operator ItemViewModel(ItemResult from)
        {
            return new ItemViewModel
            {
                PostCode = from.PostCode,
                FlowCode = from.FlowCode,
                Status = from.Status,
                AnnounceDate = from.AnnounceDate,
                Type = from.Type,
                AnnounceDepartment = from.AnnounceDepartment,
                AnnounceTitle = from.AnnounceTitle,
                Principal = from.PrincipalName
            };
        }
    }
}