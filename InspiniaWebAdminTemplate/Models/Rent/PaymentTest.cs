﻿namespace PrizeDemo1.Models.Rent
{ 
    public abstract class PaymentTest
    {
        public abstract int Inventory { get; set; }
        public abstract int Attribute { get; set; }
        public abstract bool Publish { get; set; }
        public string AttributeName
        {
            get
            {
                return Attribute == 0 ? "虛擬" : "實體";
            }
        }
        public int InventoryName
        {
            get
            {
                return Attribute == 1 ? Inventory : 9999;
            }
        }

        public string PublishName
        {
            get
            {
                return Publish ? "上架" : "下架";
            }
        }
    }
}