﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.UserConfig.Models;

namespace InspiniaWebAdminTemplate.Models.UserConfig
{
    public class IndexViewModel
    {
        public IndexViewModel()
        {
            StartDeputy = DateTime.Now;
            EndDeputy = DateTime.Now;
        }

        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string BranchName { get; set; }
        public string Department { get; set; }
        public bool IsEnable { get; set; }
        public bool IsManager { get; set; }
        public string Title { get; set; }
        public DateTime LeaveDate { get; set; }
        public string Email { get; set; }
        public string Remark { get; set; }
        public int? DeputyRecordID { get; set; }
        public string DeputyID { get; set; }
        public string DeputyName { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public Nullable<DateTime> StartDeputy { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public Nullable<DateTime> EndDeputy { get; set; }
        public List<OptionModel<string, string>> TotalRoles { get; set; }

        public static explicit operator IndexViewModel(UserInfoModel from)
        {
            return new IndexViewModel
            {
                UserCode = from.UserCode,
                UserName = from.UserName,
                BranchName = from.BranchName,
                Department = from.Department,
                IsEnable = from.IsEnable,
                IsManager = from.IsManager,
                Title = from.Title,
                LeaveDate = from.LeaveDate,
                Email = from.Email,
                Remark = from.Remark,
                DeputyRecordID = from.DeputyRecordID,
                DeputyID = from.DeputyID,
                DeputyName = from.DeputyName,
                StartDeputy = from.StartDeputy,
                EndDeputy = from.EndDeputy,
                TotalRoles = from.TotalRoles
            };
        }
    }
}