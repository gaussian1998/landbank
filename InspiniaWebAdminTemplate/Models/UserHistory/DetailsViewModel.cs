﻿using System;
using Library.Servant.Servant.UserHistory.Models;

namespace InspiniaWebAdminTemplate.Models.UserHistory
{
    public class DetailsViewModel
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string DepartmentName { get; set; }
        public Nullable<DateTime> LoginDateTime { get; set; }
        public Nullable<DateTime> LastDateTime { get; set; }
        public Nullable<DateTime> LogoutDateTime { get; set; }
        public string IP { get; set; }

        public static explicit operator DetailsViewModel(DetailsResult from)
        {
            return new DetailsViewModel
            {
                ID = from.ID,
                UserName = from.UserName,
                DepartmentName = from.DepartmentName,
                LoginDateTime = from.LoginDateTime,
                LastDateTime = from.LastDateTime,
                LogoutDateTime = from.LogoutDateTime,
                IP = from.IP
            };
        }
    }
}