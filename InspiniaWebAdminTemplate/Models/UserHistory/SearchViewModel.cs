﻿using System;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.UserHistory.Models;

namespace InspiniaWebAdminTemplate.Models.UserHistory
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public SearchViewModel()
        {
            Start = DateTime.Now;
            End = DateTime.Now;
        }

        public string UserCode { get; set; }
        public string BranchCode { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string DepartmentID { get; set; }

        public static explicit operator SearchModel(SearchViewModel vm)
        {
            //return MapProperty.Mapping<SearchModel, SearchViewModel>(vm);
            return new SearchModel
            {
                Page = vm.Page,
                PageSize = vm.PageSize,
                UserCode = vm.UserCode,
                BranchCode = vm.BranchCode,
                DepartmentID = vm.DepartmentID,
                Start = vm.Start,
                End = vm.End
            };
        }
    }
}