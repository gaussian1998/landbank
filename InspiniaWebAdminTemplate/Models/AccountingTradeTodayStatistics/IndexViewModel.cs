﻿
using Library.Servant.Servant.CodeTable.Models;

namespace InspiniaWebAdminTemplate.Models.AccountingTradeTodayStatistics
{
    public class IndexViewModel
    {
        public BranchDepartmentOptions Options { get; set; }
    }
}