﻿
using Library.Servant.Servant.CodeTable.Models;

namespace InspiniaWebAdminTemplate.Models.AccountingEntryList
{
    public class IndexViewModel
    {
        public BranchDepartmentOptions Options { get; set; }
    }
}