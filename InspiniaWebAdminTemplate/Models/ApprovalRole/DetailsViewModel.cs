﻿namespace InspiniaWebAdminTemplate.Models.ApprovalRole
{
    public class DetailsViewModel
    {
        public int ID { get; set; }
        public string Class { get; set; }
        public string ClassName { get; set; }
        public string CodeID { get; set; }
        public string Text { get; set; }
        public bool IsActive { get; set; }
        public bool CancelCode { get; set; }
        public decimal? Value1 { private get; set; }
        public string Remark { get; set; }
        public bool Supervisor
        {
            get
            {
                return Value1.HasValue;
            }
        }
        public string OperationType { get; set; }
        public string OperationTypeName { get; set; }
    }
}