﻿using System.ComponentModel.DataAnnotations;
using Library.Servant.Servant.ApprovalRole.Models;
using System.Collections.Generic;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.ApprovalRole
{
    public class EditViewModel
    {
        public int ID { get; set; }
        public string ClassName { get; set; }
        [Required]
        public string CodeID { get; set; }
        [Required]
        public string Text { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public bool CancelCode { get; set; }
        public string Remark { get; set; }
        public decimal? Value1 { get; set; }
        public decimal? Value2 { get; set; }
        public string OperationType { get; set; }  
        public List<OptionModel<int, string>> SelectedUsersOptions { get; set; }
        public List<int> SelectedUsers { get; set; }
        public bool Supervisor
        {
            get
            {
                return Value1.HasValue;
            }
        }

        public bool Email
        {
            get
            {
                return Value2.HasValue;
            }
        }

        public static explicit operator EditViewModel(DetailsResult from)
        {
            return new EditViewModel
            {
                ID = from.ID,
                ClassName = from.ClassName,
                CodeID = from.CodeID,
                Text = from.Text,
                IsActive = from.IsActive,
                CancelCode = from.CancelCode,
                Remark = from.Remark,
                Value1 = from.Value1,
                Value2 = from.Value2,
                OperationType = from.OperationType,
                SelectedUsersOptions = from.SelectedUsersOptions
            };
        }
    }
}