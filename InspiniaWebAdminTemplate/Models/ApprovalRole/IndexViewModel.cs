﻿
using System.Collections.Generic;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.ApprovalRole
{
    public class IndexViewModel
    {
        public List<DetailsViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public SearchViewModel SearchCondition { get; set; }
        public List<OptionModel<string, string>> ActiveOptions { get; set; }
        public List<OptionModel<string, string>> CancelOptions { get; set; }
    }
}