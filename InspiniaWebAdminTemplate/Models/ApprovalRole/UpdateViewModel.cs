﻿using Library.Servant.Servant.ApprovalRole.Models;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.ApprovalRole
{
    public class UpdateViewModel
    {
        public int ID { get; set; }
        public string CodeID { get; set; }
        public string Text { get; set; }
        public bool IsActive { get; set; }
        public bool CancelCode { get; set; }
        public string Remark { get; set; }
        public decimal? Value1 { get; set; }
        public decimal? Value2 { get; set; }
        public string OperationType { get; set; }
        public List<OptionModel<int, string>> SelectedUsersOptions { get; set; }
        public List<int> SelectedUsers { get; set; }
        public bool Supervisor
        {
            set
            {
                if(value)
                    Value1 = 1;
            }
            get
            {
                return Value1.HasValue;
            }
        }

        public bool Email
        {
            set
            {
                if (value)
                    Value2 = 1;
            }
            get
            {
                return Value2.HasValue;
            }
        }

        public static explicit operator UpdateModel(UpdateViewModel vm)
        {
            return new UpdateModel
            {
                ID = vm.ID,
                CodeID = vm.CodeID,
                Text = vm.Text,
                IsActive = vm.IsActive,
                CancelCode = vm.CancelCode,
                Remark = vm.Remark,
                Value1 = vm.Value1,
                Value2 = vm.Value2,
                Parameter1 = vm.OperationType,
                SelectedUsers= vm.SelectedUsers
            };
        }
    }
}