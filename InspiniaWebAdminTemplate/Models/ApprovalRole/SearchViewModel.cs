﻿using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.ApprovalRole.Models;

namespace InspiniaWebAdminTemplate.Models.ApprovalRole
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public string CodeID { get; set; }
        public bool? IsActive { get; set; }
        public bool? CancelCode { get; set; }
        public string OperationType { get; set; }

        public static explicit operator SearchModel(SearchViewModel vm)
        {
            //return MapProperty.Mapping<SearchModel, SearchViewModel>(vm);
            return new SearchModel
            {
                CodeID = vm.CodeID,
                IsActive = vm.IsActive,
                CancelCode = vm.CancelCode,
               OperationType =vm.OperationType,
        Page = vm.Page,
                PageSize = vm.PageSize
            };
        }
    }
}
