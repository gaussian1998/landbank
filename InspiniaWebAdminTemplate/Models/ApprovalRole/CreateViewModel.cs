﻿using Library.Servant.Servant.ApprovalRole.Models;
using Library.Servant.Servant.Common.Models;
using Library.Utility;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InspiniaWebAdminTemplate.Models.ApprovalRole
{
    public class CreateViewModel
    {
        public string ClassName { get; set; }
        [Required]
        public string CodeID { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public bool CancelCode { get; set; }
        [Required]
        public string Text { get; set; }
        public string Remark { get; set; }
        public decimal? Value1 { get; set; }
        public decimal? Value2 { get; set; }
        public string OperationType { get; set; }
        public List<int> SelectedUsers { get; set; }
        public bool Supervisor
        {
            get
            {
                return Value1.HasValue;
            }
            set
            {
                if (value)
                    Value1 = 1;
            }
        }

        public bool Email
        {
            get
            {
                return Value2.HasValue;
            }
            set
            {
                if (value)
                    Value2 = 1;
            }
        }

        public static explicit operator CreateViewModel(NewResult from)
        {
            return new CreateViewModel
            {
                ClassName = from.ClassName,
                IsActive = from.IsActive,
                CancelCode = from.CancelCode
            };
        }

        public static explicit operator CreateModel(CreateViewModel vm)
        {
            return new CreateModel
            {
                CodeID = vm.CodeID,
                IsActive = vm.IsActive,
                CancelCode = vm.CancelCode,
                Text = vm.Text,
                Remark = vm.Remark,
                Value1 = vm.Value1,
                Value2 = vm.Value2,
                Parameter1 = vm.OperationType,
                SelectedUsers= vm.SelectedUsers
            };
        }
    }
}