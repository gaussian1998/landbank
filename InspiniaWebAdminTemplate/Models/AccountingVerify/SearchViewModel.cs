﻿using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.AccountingVerify.Models;

namespace InspiniaWebAdminTemplate.Models.AccountingVerify
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public string TradeKind { get; set; }
        public string TradeDistinguish { get; set; }
        public string TransSeq { get; set; }
        public string MasterNo { get; set; }
        public string AssetNumber { get; set; }
        public static explicit operator SearchModel(SearchViewModel vm)
        {
            return new SearchModel
            {
                Page = vm.Page,
                PageSize = vm.PageSize,
                TradeKind = vm.TradeKind,
                TradeDistinguish = vm.TradeDistinguish
            };
        }
    }
}