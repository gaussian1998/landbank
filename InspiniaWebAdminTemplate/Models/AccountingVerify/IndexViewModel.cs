﻿using Library.Common.Models;
using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Models;

namespace InspiniaWebAdminTemplate.Models.AccountingVerify
{
    public class IndexViewModel
    {
        public TradeOptions TradeOptions { get; set; }
        public SearchViewModel Condition { get; set; }
        public PageList<AccountEntryDetailResult> Pages { get; set; }
    }
}