﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Integration
{
    public class BuildViewModel
    {
        public int ID { get; set; }
        public int LEASE_HEADER_ID { get; set; }//承租契約表頭ID
        public string LEASE_NUMBER { get; set; }//承租契約單號
        public string CITY_NAME { get; set; }//縣市
        public string DISTRICT_NAME { get; set; }//鄉鎮市區
        public string SECTION_NAME { get; set; }//段        
        public string SUBSECTION_NAME { get; set; }//小段
        public string ADDRESS { get; set; }//地址
        public string BUSINESS_USE_TYPE { get; set; }//營業用途
        public string ATTACHED_BUILD { get; set; }//附屬建物
        public string HOUSE_ARCADE { get; set; }//屋突
        public string CONVEX { get; set; }//夾層
        public string ARCADE { get; set; }//騎樓
        public string COMMON_PART { get; set; }//共同部分
        public string DOORPLATE_LANE { get; set; }//建物門牌號碼
        public string BUILDING_STRU { get; set; }//構造
        public string RIGHT_AREA { get; set; }//權利面積
        public string LEASES_AREA { get; set; }//承租面積
        public string FLOOR { get; set; }//樓層
        public int SHARE_PUBLIC_AREA { get; set; }//分攤公社面積
        public int TOTAL_FLOOR { get; set; }//總樓層
        public int TOTAL_AREA { get; set; }//總面積(平方公尺) 
        public string ARCADE_AREA { get; set; }//騎樓面積
        public string DESCRIPTION { get; set; }//備註
        public List<string> FILES { get; set; }//上傳檔案        
        public string Created_By { get; set; }
        public string Created_ByName { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public string Last_Updated_By { get; set; }
    }
}