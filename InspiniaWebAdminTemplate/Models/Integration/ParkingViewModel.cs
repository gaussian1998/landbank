﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Integration
{
    public class ParkingViewModel
    {
        public int ID { get; set; }
        public int LEASE_HEADER_ID { get; set; }//承租契約表頭ID
        public string LEASE_NUMBER { get; set; }//承租契約單號
        public string CITY_NAME { get; set; }//縣市
        public string DISTRICT_NAME { get; set; }//鄉鎮市區
        public string SECTION_NAME { get; set; }//段
        public string SUBSECTION_NAME { get; set; }//小段
        public string ADDRESS { get; set; }//地址
        public string PARKING_NO { get; set; }//車位編號
        public string PARKING_USE_TYPE { get; set; }//停車位使用類別        
        public string FLOOR { get; set; }//樓層
        public int RENTAL_AMOUNT { get; set; }//租金
        public string DESCRIPTION { get; set; }//備註
        public List<string> FILES { get; set; }//上傳檔案        
        public string Created_By { get; set; }
        public string Created_ByName { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public string Last_Updated_By { get; set; }      
    }
}