﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Integration
{
    public class InsuranceViewModel
    {
        public int ID { get; set; }
        public int LEASE_HEADER_ID { get; set; }//承租契約表頭ID
        public string LEASE_NUMBER { get; set; }//承租契約單號
        public string INSURANCE_TYPE { get; set; }//保險類別
        public string COMPANY { get; set; }//保險公司
        public string INSURANCE_NO { get; set; }//保單號碼        
        public string COMPANY_CONTACT { get; set; }//保險公司聯絡人
        public string COMPANY_PHONE { get; set; }//保險公司聯絡電話
        public string APPLICANT { get; set; }//投保人
        public string INSURANT { get; set; }//被保險人
        public DateTime? START_DATE { get; set; }//保險開始期間
        public DateTime? END_DATE { get; set; }//保險結束期間
        public int PRICE { get; set; }//保險費
        public string DESCRIPTION { get; set; }//備註
        public string Created_By { get; set; }
        public string Created_ByName { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public string Last_Updated_By { get; set; }
    }
}