﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Integration
{
    public class IntegrationViewModel
    {
        public bool CanEdit { get; set; }

        public HeaderViewModel Header { get; set; }

        public IEnumerable<ContactViewModel> Contracts { get; set; }

        #region==批次匯入審查欄位==
        //分行
        public string Assigned_Branch { get; set; }
        //經辦
        public string HandledBy { get; set; }
        //經辦電話
        public string Phone { get; set; }
        //經辦Email
        public string Email { get; set; }
        #endregion
    }
}