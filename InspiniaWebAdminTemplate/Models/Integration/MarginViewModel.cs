﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Integration
{
    public class MarginViewModel
    {
        public int ID { get; set; }
        public int LEASE_HEADER_ID { get; set; }//承租契約表頭ID
        public string LEASE_NUMBER { get; set; }//承租契約單號
        public string RECEIVED_DATE { get; set; }//保證金收款日期
        public string RETURN_DATE { get; set; }//保證金退還日期
        public string BANK { get; set; }//保證銀行及字號        
        public int PRICE { get; set; }//保證金
        public string CURRENCY { get; set; }//幣別  
        public string CURRENCY_VALUE { get; set; }//幣別匯率
        public string Created_By { get; set; }
        public string Created_ByName { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public string Last_Updated_By { get; set; }
    }
}