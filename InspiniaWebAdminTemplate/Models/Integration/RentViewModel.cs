﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Integration
{
    public class RentViewModel
    {
        public int ID { get; set; }
        public int LEASE_HEADER_ID { get; set; }//承租契約表頭ID
        public string LEASE_NUMBER { get; set; }//承租契約單號
        public DateTime? START_DATE { get; set; }//租賃開始時間
        public DateTime? END_DATE { get; set; }//租賃結束時間
        public string CALCULATE_METHOD { get; set; }//租金計算
        public string PAYMENT_PERIOD { get; set; }//繳款週期
        public string DUE_DAY { get; set; }//繳款期限
        public string LAST_PAYMENT { get; set; }//上期繳款日
        public string NEXT_PAYMENT { get; set; }//預計下期繳款日
        public DateTime? EFFECTIVE_DATE { get; set; }//租金修改生效日期
        public int PRICE { get; set; }//每期租金
        public string CURRENCY { get; set; }//幣別
        public string CURRENCY_VALUE { get; set; }//幣別匯率
        public string DESCRIPTION { get; set; }//備註
        public string Created_By { get; set; }
        public string Created_ByName { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public string Last_Updated_By { get; set; }
    }
}