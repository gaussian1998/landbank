﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Integration
{
    public class ContactDetailViewModel
    {
        public bool isNew { get; set; }
        public bool canEdit { get; set; }
        public string TRX_Header_ID { get; set; }
        public string Transaction_Type { get; set; }
        public string OldNumber { get; set; }
        public ContactViewModel Contract { get; set; }       
    }
}