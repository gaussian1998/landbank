﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Integration
{
    public class ContractsIndexViewModel
    {
        public IEnumerable<ContactViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public ContractSearchViewModel condition { get; set; }
    }
}