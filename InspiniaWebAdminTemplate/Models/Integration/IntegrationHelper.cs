﻿using Library.Entity.Edmx;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using Library.Servant.Servant.Integration.IntegrationModels;

namespace InspiniaWebAdminTemplate.Models.Integration
{
    public class IntegrationHelper
    {
        #region== Model To ViewModel ==
        public static ContractSearchViewModel ToContractSearchViewModel(ContractSearchModel model)
        {
            return new ContractSearchViewModel()
            {
                Page = model.Page,
                PageSize = model.PageSize,
                Type = model.Type,
                Business_Type = model.Business_Type,
                Category = model.Category,
                TRANSACTION_TYPE = model.TRANSACTION_TYPE,
                Status = model.Status,
                RateStart = model.RateStart,
                RateEnd = model.RateEnd,
                TRXHeaderIDBgn = model.TRXHeaderIDBgn,
                TRXHeaderIDEnd = model.TRXHeaderIDEnd,
                OldTRXHeaderIDBgn = model.OldTRXHeaderIDBgn,
                OldTRXHeaderIDEnd = model.OldTRXHeaderIDEnd,
                BeginDate = model.BeginDate,
                EndDate = model.EndDate,
                ApplyBeginDate = model.ApplyBeginDate,
                ApplyEndDate = model.ApplyEndDate,
                OfficeBranch = model.OfficeBranch,
                Account = model.Account
            };
        }

        public static HeaderViewModel ToIntegrationHeaderViewModel(IntegrationHeaderModel model)
        {
            return new HeaderViewModel()
            {
                ID = model.ID,
                TRX_Header_ID = model.TRX_Header_ID,
                Transaction_Type = model.Transaction_Type,
                Transaction_TypeName = model.Transaction_TypeName,
                Transaction_Time = model.Transaction_Time,
                Flow_Status = model.Flow_Status,
                Source = model.Source,
                SourceName = model.SourceName,
                Book_Type = model.Book_Type,
                Office_Branch = model.Office_Branch,
                Remark = model.Remark,
                AssetsCount = model.AssetsCount,
                DisposeReason = model.DisposeReason,
                OriOffice_Branch = model.OriOffice_Branch,
                Created_ByName = model.Created_ByName,
                Flow_StatusName = model.Flow_StatusName,
                OriOffice_BranchName = model.OriOffice_BranchName,
                Office_BranchName = model.Office_BranchName,
                Create_Time = model.Create_Time
            };
        }

        public static ContactViewModel ToContactsViewModel(ContactModel model)
        {
            if (model == null) {
                return new ContactViewModel();
            }
            ContactViewModel value = new ContactViewModel();
            value.ID = model.ID;
            value.TRX_Header_ID = model.TRX_Header_ID;
            value.LEASE_NUMBER = model.LEASE_NUMBER;
            value.OLD_LEASE_NUMBER = model.OLD_LEASE_NUMBER;
            value.TRANSACTION_TYPE = model.TRANSACTION_TYPE;
            value.OFFICER_BRANCH = model.OFFICER_BRANCH;
            value.LEASE_TYPE = model.LEASE_TYPE;
            value.LEASE_TRANSACTION_TYPE = model.LEASE_TRANSACTION_TYPE;
            value.LEASE_STATUS = model.LEASE_STATUS;
            value.PAYMENT_METHOD = model.PAYMENT_METHOD;
            value.BUSINESS_TYPE = model.BUSINESS_TYPE;
            value.PAYMENT_TYPE = model.PAYMENT_TYPE;
            value.CHECK_TYPE = model.CHECK_TYPE;
            value.CREATED_BY = model.CREATED_BY;
            value.LEASE_INCEPTION_DATE = model.LEASE_INCEPTION_DATE;
            value.LEASE_TERMINATION_DATE = model.LEASE_TERMINATION_DATE;
            value.ACCOUNT_NUMBER = model.ACCOUNT_NUMBER;
            value.DESCRIPTION = model.DESCRIPTION;
            value.OfficeBranchName = model.OfficeBranchName;
            value.ACCEPT_DATE = model.ACCEPT_DATE;
            value.LEASE_CATEGORY = model.LEASE_CATEGORY;
            value.LEASE_AREA = model.LEASE_AREA;
            value.LEASE_RATE = model.LEASE_RATE;
            value.CREATED_TIME = model.CREATED_TIME;
            value.RentList = new List<RentViewModel>();
            value.ManageList = new List<ManageExpenseViewModel>();
            value.LandList = new List<LandViewModel>();
            value.MarginList = new List<MarginViewModel>();
            value.FireList = new List<InsuranceViewModel>();
            value.PublicList = new List<InsuranceViewModel>();
            value.BuildList = new List<BuildViewModel>();
            value.ParkingList = new List<ParkingViewModel>();
            value.PaymentList = new List<PaymentAccountViewModel>();
            value.NoteList = new List<NoteViewModel>();
            if (model.NewContract != null)
            {
                ContactDetailViewModel newContract = new ContactDetailViewModel();                
                newContract.Contract = ToContactsViewModel(model.NewContract);
                value.NewContract = newContract;
            }
            if (model.RentList != null) { 
                foreach (RentModel item in model.RentList) {
                    value.RentList.Add(ToRentViewModel(item));
                }
            }
            if (model.ManageList != null)
            {
                foreach (ManageExpenseModel item in model.ManageList)
                {
                    value.ManageList.Add(ToManageExpenseViewModel(item));
                }
            }
            if (model.LandList != null)
            {
                foreach (LandModel item in model.LandList)
                {
                    value.LandList.Add(ToLandViewModel(item));
                }
            }
            if (model.MarginList != null)
            {
                foreach (MarginModel item in model.MarginList)
                {
                    value.MarginList.Add(ToMarginViewModel(item));
                }
            }
            if (model.FireList != null)
            {
                foreach (InsuranceModel item in model.FireList)
                {
                    value.FireList.Add(ToInsuranceViewModel(item));
                }
            }
            if (model.PublicList != null)
            {
                foreach (InsuranceModel item in model.PublicList)
                {
                    value.PublicList.Add(ToInsuranceViewModel(item));
                }
            }
            if (model.BuildList != null)
            {
                foreach (BuildModel item in model.BuildList)
                {
                    value.BuildList.Add(ToBuildViewModel(item));
                }
            }
            if (model.ParkingList != null)
            {
                foreach (ParkingModel item in model.ParkingList)
                {
                    value.ParkingList.Add(ToParkingViewModel(item));
                }
            }
            if (model.PaymentList != null)
            {
                foreach (PaymentAccountModel item in model.PaymentList)
                {
                    value.PaymentList.Add(ToPaymentAccountViewModel(item));
                }
            }
            if (model.NoteList != null)
            {
                foreach (NoteModel item in model.NoteList)
                {
                    value.NoteList.Add(ToNoteViewModel(item));
                }
            }
            return value;
        }

        public static RentViewModel ToRentViewModel(RentModel model)
        {
            if (model == null)
            {
                return new RentViewModel();
            }
            return new RentViewModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                START_DATE = model.START_DATE,
                END_DATE = model.END_DATE,
                CALCULATE_METHOD = model.CALCULATE_METHOD,
                PAYMENT_PERIOD = model.PAYMENT_PERIOD,
                DUE_DAY = model.DUE_DAY,
                LAST_PAYMENT = model.LAST_PAYMENT,
                NEXT_PAYMENT = model.NEXT_PAYMENT,
                EFFECTIVE_DATE = model.EFFECTIVE_DATE,
                PRICE = model.PRICE,
                CURRENCY = model.CURRENCY,
                CURRENCY_VALUE = model.CURRENCY_VALUE,
                DESCRIPTION = model.DESCRIPTION,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }

        public static ManageExpenseViewModel ToManageExpenseViewModel(ManageExpenseModel model)
        {
            if (model == null)
            {
                return new ManageExpenseViewModel();
            }
            return new ManageExpenseViewModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                START_DATE = model.START_DATE,
                END_DATE = model.END_DATE,
                CALCULATE_METHOD = model.CALCULATE_METHOD,
                PAYMENT_PERIOD = model.PAYMENT_PERIOD,
                DUE_DAY = model.DUE_DAY,
                LAST_PAYMENT = model.LAST_PAYMENT,
                NEXT_PAYMENT = model.NEXT_PAYMENT,
                EFFECTIVE_DATE = model.EFFECTIVE_DATE,
                PRICE = model.PRICE,
                CURRENCY = model.CURRENCY,
                CURRENCY_VALUE = model.CURRENCY_VALUE,
                DESCRIPTION = model.DESCRIPTION,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }

        public static LandViewModel ToLandViewModel(LandModel model)
        {
            if (model == null)
            {
                return new LandViewModel();
            }
            return new LandViewModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                CITY_NAME = model.CITY_NAME,
                DISTRICT_NAME = model.DISTRICT_NAME,
                SECTION_NAME = model.SECTION_NAME,
                SUBSECTION_NAME = model.SUBSECTION_NAME,
                PARENT_LAND_NUMBER = model.PARENT_LAND_NUMBER,
                FILIAL_LAND_NUMBER = model.FILIAL_LAND_NUMBER,
                LAND_USE_TYPE = model.LAND_USE_TYPE,
                AUTHORIZED_AREA = model.AUTHORIZED_AREA,
                ANNOUNCE_PRICE = model.ANNOUNCE_PRICE,
                LESSEE_AREA = model.LESSEE_AREA,
                LEASE_RATE = model.LEASE_RATE,
                RENTAL_AMOUNT = model.RENTAL_AMOUNT,
                DESCRIPTION = model.DESCRIPTION,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }

        public static InsuranceViewModel ToInsuranceViewModel(InsuranceModel model)
        {
            if (model == null)
            {
                return new InsuranceViewModel();
            }
            return new InsuranceViewModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                INSURANCE_TYPE = model.INSURANCE_TYPE,
                COMPANY = model.COMPANY,
                INSURANCE_NO = model.INSURANCE_NO,
                COMPANY_CONTACT = model.COMPANY_CONTACT,
                COMPANY_PHONE = model.COMPANY_PHONE,
                APPLICANT = model.APPLICANT,
                INSURANT = model.INSURANT,
                START_DATE = model.START_DATE,
                END_DATE = model.END_DATE,
                PRICE = model.PRICE,
                DESCRIPTION = model.DESCRIPTION,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }

        public static BuildViewModel ToBuildViewModel(BuildModel model)
        {
            if (model == null)
            {
                return new BuildViewModel();
            }
            return new BuildViewModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                CITY_NAME = model.CITY_NAME,
                DISTRICT_NAME = model.DISTRICT_NAME,
                SECTION_NAME = model.SECTION_NAME,
                SUBSECTION_NAME = model.SUBSECTION_NAME,
                ADDRESS = model.ADDRESS,
                BUSINESS_USE_TYPE = model.BUSINESS_USE_TYPE,
                ATTACHED_BUILD = model.ATTACHED_BUILD,
                HOUSE_ARCADE = model.HOUSE_ARCADE,
                CONVEX = model.CONVEX,
                ARCADE = model.ARCADE,
                COMMON_PART = model.COMMON_PART,
                DOORPLATE_LANE = model.DOORPLATE_LANE,
                BUILDING_STRU = model.BUILDING_STRU,
                RIGHT_AREA = model.RIGHT_AREA,
                LEASES_AREA = model.LEASES_AREA,
                FLOOR = model.FLOOR,
                TOTAL_AREA = model.TOTAL_AREA,
                ARCADE_AREA = model.ARCADE_AREA,
                SHARE_PUBLIC_AREA = model.SHARE_PUBLIC_AREA,
                TOTAL_FLOOR = model.TOTAL_FLOOR,
                DESCRIPTION = model.DESCRIPTION,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }

        public static NoteViewModel ToNoteViewModel(NoteModel model)
        {
            if (model == null)
            {
                return new NoteViewModel();
            }
            return new NoteViewModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                NOTE_TYPE = model.NOTE_TYPE,
                CALC_START_DATE = model.CALC_START_DATE,
                CALC_END_DATE = model.CALC_END_DATE,
                CALCULATE_METHOD = model.CALCULATE_METHOD,
                PRICE = model.PRICE,
                CURRENCY = model.CURRENCY,
                PAYMENT_PERIOD = model.PAYMENT_PERIOD,
                DESCRIPTION = model.DESCRIPTION,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By,
                Create_Time = model.Create_Time
            };
        }

        public static MarginViewModel ToMarginViewModel(MarginModel model)
        {
            if (model == null)
            {
                return new MarginViewModel();
            }
            return new MarginViewModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                RECEIVED_DATE = model.RECEIVED_DATE,
                RETURN_DATE = model.RETURN_DATE,
                BANK = model.BANK,
                PRICE = model.PRICE,
                CURRENCY = model.CURRENCY,
                CURRENCY_VALUE = model.CURRENCY_VALUE,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }

        public static PaymentAccountViewModel ToPaymentAccountViewModel(PaymentAccountModel model)
        {
            if (model == null)
            {
                return new PaymentAccountViewModel();
            }
            return new PaymentAccountViewModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                ACCOUNT = model.ACCOUNT,
                BANK = model.BANK,
                ACCOUNT_NAME = model.ACCOUNT_NAME,
                PAYMENT_METHOD = model.PAYMENT_METHOD,
                DESCRIPTION = model.DESCRIPTION,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }

        public static ParkingViewModel ToParkingViewModel(ParkingModel model)
        {
            if (model == null)
            {
                return new ParkingViewModel();
            }
            return new ParkingViewModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                CITY_NAME = model.CITY_NAME,
                DISTRICT_NAME = model.DISTRICT_NAME,
                SECTION_NAME = model.SECTION_NAME,
                SUBSECTION_NAME = model.SUBSECTION_NAME,
                PARKING_NO = model.PARKING_NO,
                PARKING_USE_TYPE = model.PARKING_USE_TYPE,
                FLOOR = model.FLOOR,
                RENTAL_AMOUNT = model.RENTAL_AMOUNT,
                DESCRIPTION = model.DESCRIPTION,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By,
                ADDRESS = model.ADDRESS
            };
        }
        #endregion

        #region ==ViewModel To Model==

        public static ContractSearchModel ToContractSearchModel(ContractSearchViewModel model)
        {
            return new ContractSearchModel()
            {
                Page = model.Page,
                PageSize = model.PageSize,
                Type = model.Type,
                Business_Type = model.Business_Type,
                Category = model.Category,
                TRANSACTION_TYPE = model.TRANSACTION_TYPE,
                Status = model.Status,
                RateStart = model.RateStart,
                RateEnd = model.RateEnd,
                TRXHeaderIDBgn = model.TRXHeaderIDBgn,
                TRXHeaderIDEnd = model.TRXHeaderIDEnd,
                OldTRXHeaderIDBgn = model.OldTRXHeaderIDBgn,
                OldTRXHeaderIDEnd = model.OldTRXHeaderIDEnd,
                BeginDate = model.BeginDate,
                EndDate = model.EndDate,
                ApplyBeginDate = model.ApplyBeginDate,
                ApplyEndDate = model.ApplyEndDate,
                OfficeBranch = model.OfficeBranch,
                Account = model.Account
            };
        }

        public static IntegrationHeaderModel ToIntegrationHeaderModel(HeaderViewModel viewModel)
        {           
            return new IntegrationHeaderModel()
            {
                ID = viewModel.ID,
                TRX_Header_ID = viewModel.TRX_Header_ID,
                Transaction_Type = viewModel.Transaction_Type,
                Transaction_Time = viewModel.Transaction_Time,
                Flow_Status = viewModel.Flow_Status,
                Source = viewModel.Source,
                Book_Type = viewModel.Book_Type,
                Office_Branch = viewModel.Office_Branch,
                Remark = viewModel.Remark,
                AssetsCount = viewModel.AssetsCount,
                DisposeReason = viewModel.DisposeReason,
                OriOffice_Branch = viewModel.OriOffice_Branch,
                Created_By = viewModel.Created_By,
                Create_Time = viewModel.Create_Time,
                Last_Updated_By = viewModel.Last_Updated_By,
                Last_Updated_Time = viewModel.Last_Updated_Time
            };
        }

        public static ContactModel ToContactModel(ContactViewModel model)
        {
            ContactModel value = new ContactModel()
            {
                ID = model.ID,
                TRX_Header_ID = model.TRX_Header_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                OLD_LEASE_NUMBER = model.OLD_LEASE_NUMBER,
                TRANSACTION_TYPE = model.TRANSACTION_TYPE,
                OFFICER_BRANCH = model.OFFICER_BRANCH,
                LEASE_TYPE = model.LEASE_TYPE,
                LEASE_TRANSACTION_TYPE = model.LEASE_TRANSACTION_TYPE,
                LEASE_STATUS = model.LEASE_STATUS,
                PAYMENT_METHOD = model.PAYMENT_METHOD,
                BUSINESS_TYPE = model.BUSINESS_TYPE,
                PAYMENT_TYPE = model.PAYMENT_TYPE,
                CHECK_TYPE = model.CHECK_TYPE,
                CREATED_BY = model.CREATED_BY,
                LEASE_INCEPTION_DATE = model.LEASE_INCEPTION_DATE,
                LEASE_TERMINATION_DATE = model.LEASE_TERMINATION_DATE,
                ACCOUNT_NUMBER = model.ACCOUNT_NUMBER,
                DESCRIPTION = model.DESCRIPTION,
                OfficeBranchName = model.OfficeBranchName,
                ACCEPT_DATE = model.ACCEPT_DATE,
                LEASE_CATEGORY = model.LEASE_CATEGORY,
                LEASE_AREA = model.LEASE_AREA,
                LEASE_RATE = model.LEASE_RATE,
                CREATED_TIME = model.CREATED_TIME,
                Last_Updated_By = model.Last_Updated_By,
                Last_Updated_Time = model.Last_Updated_Time

            };

            value.ID = model.ID;
            value.TRX_Header_ID = model.TRX_Header_ID;
            value.LEASE_NUMBER = model.LEASE_NUMBER;
            value.OLD_LEASE_NUMBER = model.OLD_LEASE_NUMBER;
            value.TRANSACTION_TYPE = model.TRANSACTION_TYPE;
            value.OFFICER_BRANCH = model.OFFICER_BRANCH;
            value.LEASE_TYPE = model.LEASE_TYPE;
            value.LEASE_TRANSACTION_TYPE = model.LEASE_TRANSACTION_TYPE;
            value.LEASE_STATUS = model.LEASE_STATUS;
            value.PAYMENT_METHOD = model.PAYMENT_METHOD;
            value.BUSINESS_TYPE = model.BUSINESS_TYPE;
            value.PAYMENT_TYPE = model.PAYMENT_TYPE;
            value.CHECK_TYPE = model.CHECK_TYPE;
            value.CREATED_BY = model.CREATED_BY;
            value.LEASE_INCEPTION_DATE = model.LEASE_INCEPTION_DATE;
            value.LEASE_TERMINATION_DATE = model.LEASE_TERMINATION_DATE;
            value.ACCOUNT_NUMBER = model.ACCOUNT_NUMBER;
            value.DESCRIPTION = model.DESCRIPTION;
            value.OfficeBranchName = model.OfficeBranchName;
            value.ACCEPT_DATE = model.ACCEPT_DATE;
            value.LEASE_CATEGORY = model.LEASE_CATEGORY;
            value.LEASE_AREA = model.LEASE_AREA;
            value.LEASE_RATE = model.LEASE_RATE;
            value.CREATED_TIME = model.CREATED_TIME;
            value.RentList = new List<RentModel>();
            value.ManageList = new List<ManageExpenseModel>();
            value.LandList = new List<LandModel>();
            value.MarginList = new List<MarginModel>();
            value.FireList = new List<InsuranceModel>();
            value.PublicList = new List<InsuranceModel>();
            value.BuildList = new List<BuildModel>();
            value.ParkingList = new List<ParkingModel>();
            value.PaymentList = new List<PaymentAccountModel>();
            value.NoteList = new List<NoteModel>();
            if (model.RentList != null)
            {
                foreach (RentViewModel item in model.RentList)
                {
                    value.RentList.Add(ToRentModel(item));
                }
            }
            if (model.ManageList != null)
            {
                foreach (ManageExpenseViewModel item in model.ManageList)
                {
                    value.ManageList.Add(ToManageExpenseModel(item));
                }
            }
            if (model.LandList != null)
            {
                foreach (LandViewModel item in model.LandList)
                {
                    value.LandList.Add(ToLandModel(item));
                }
            }
            if (model.MarginList != null)
            {
                foreach (MarginViewModel item in model.MarginList)
                {
                    value.MarginList.Add(ToMarginModel(item));
                }
            }
            if (model.FireList != null)
            {
                foreach (InsuranceViewModel item in model.FireList)
                {
                    value.FireList.Add(ToInsuranceModel(item));
                }
            }
            if (model.PublicList != null)
            {
                foreach (InsuranceViewModel item in model.PublicList)
                {
                    value.PublicList.Add(ToInsuranceModel(item));
                }
            }
            if (model.BuildList != null)
            {
                foreach (BuildViewModel item in model.BuildList)
                {
                    value.BuildList.Add(ToBuildModel(item));
                }
            }
            if (model.ParkingList != null)
            {
                foreach (ParkingViewModel item in model.ParkingList)
                {
                    value.ParkingList.Add(ToParkingModel(item));
                }
            }
            if (model.PaymentList != null)
            {
                foreach (PaymentAccountViewModel item in model.PaymentList)
                {
                    value.PaymentList.Add(ToPaymentAccountModel(item));
                }
            }
            if (model.NoteList != null)
            {
                foreach (NoteViewModel item in model.NoteList)
                {
                    value.NoteList.Add(ToNoteModel(item));
                }
            }

            return value;
        }

        public static RentModel ToRentModel(RentViewModel model)
        {
            if (model == null) {
                return new RentModel();
            }
            return new RentModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                START_DATE = model.START_DATE,
                END_DATE = model.END_DATE,
                CALCULATE_METHOD = model.CALCULATE_METHOD,
                PAYMENT_PERIOD = model.PAYMENT_PERIOD,
                DUE_DAY = model.DUE_DAY,
                LAST_PAYMENT = model.LAST_PAYMENT,
                NEXT_PAYMENT = model.NEXT_PAYMENT,
                EFFECTIVE_DATE = model.EFFECTIVE_DATE,
                PRICE = model.PRICE,
                CURRENCY = model.CURRENCY,
                CURRENCY_VALUE = model.CURRENCY_VALUE,
                DESCRIPTION = model.DESCRIPTION,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }

        public static ManageExpenseModel ToManageExpenseModel(ManageExpenseViewModel model) {
            if (model == null)
            {
                return new ManageExpenseModel();
            }
            return new ManageExpenseModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                START_DATE = model.START_DATE,
                END_DATE = model.END_DATE,
                CALCULATE_METHOD = model.CALCULATE_METHOD,
                PAYMENT_PERIOD = model.PAYMENT_PERIOD,
                DUE_DAY = model.DUE_DAY,
                LAST_PAYMENT = model.LAST_PAYMENT,
                NEXT_PAYMENT = model.NEXT_PAYMENT,
                EFFECTIVE_DATE = model.EFFECTIVE_DATE,
                PRICE = model.PRICE,
                CURRENCY = model.CURRENCY,
                CURRENCY_VALUE = model.CURRENCY_VALUE,
                DESCRIPTION = model.DESCRIPTION,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }

        public static LandModel ToLandModel(LandViewModel model)
        {
            if (model == null)
            {
                return new LandModel();
            }
            return new LandModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                CITY_NAME = model.CITY_NAME,
                DISTRICT_NAME = model.DISTRICT_NAME,
                SECTION_NAME = model.SECTION_NAME,
                SUBSECTION_NAME = model.SUBSECTION_NAME,
                PARENT_LAND_NUMBER = model.PARENT_LAND_NUMBER,
                FILIAL_LAND_NUMBER = model.FILIAL_LAND_NUMBER,
                LAND_USE_TYPE = model.LAND_USE_TYPE,
                AUTHORIZED_AREA = model.AUTHORIZED_AREA,
                ANNOUNCE_PRICE = model.ANNOUNCE_PRICE,
                LESSEE_AREA = model.LESSEE_AREA,
                LEASE_RATE = model.LEASE_RATE,
                RENTAL_AMOUNT = model.RENTAL_AMOUNT,
                DESCRIPTION = model.DESCRIPTION,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }

        public static InsuranceModel ToInsuranceModel(InsuranceViewModel model)
        {
            if (model == null)
            {
                return new InsuranceModel();
            }
            return new InsuranceModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                INSURANCE_TYPE = model.INSURANCE_TYPE,
                COMPANY = model.COMPANY,
                INSURANCE_NO = model.INSURANCE_NO,
                COMPANY_CONTACT = model.COMPANY_CONTACT,
                COMPANY_PHONE = model.COMPANY_PHONE,
                APPLICANT = model.APPLICANT,
                INSURANT = model.INSURANT,
                START_DATE = model.START_DATE,
                END_DATE = model.END_DATE,
                PRICE = model.PRICE,
                DESCRIPTION = model.DESCRIPTION,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }

        public static BuildModel ToBuildModel(BuildViewModel model)
        {
            if (model == null)
            {
                return new BuildModel();
            }
            return new BuildModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                CITY_NAME = model.CITY_NAME,
                DISTRICT_NAME = model.DISTRICT_NAME,
                SECTION_NAME = model.SECTION_NAME,
                SUBSECTION_NAME = model.SUBSECTION_NAME,
                ADDRESS = model.ADDRESS,
                BUSINESS_USE_TYPE = model.BUSINESS_USE_TYPE,
                ATTACHED_BUILD = model.ATTACHED_BUILD,
                HOUSE_ARCADE = model.HOUSE_ARCADE,
                CONVEX = model.CONVEX,
                ARCADE = model.ARCADE,
                COMMON_PART = model.COMMON_PART,
                DOORPLATE_LANE = model.DOORPLATE_LANE,
                BUILDING_STRU = model.BUILDING_STRU,
                RIGHT_AREA = model.RIGHT_AREA,
                LEASES_AREA = model.LEASES_AREA,
                FLOOR = model.FLOOR,
                TOTAL_AREA = model.TOTAL_AREA,
                ARCADE_AREA = model.ARCADE_AREA,
                SHARE_PUBLIC_AREA = model.SHARE_PUBLIC_AREA,
                TOTAL_FLOOR = model.TOTAL_FLOOR,
                DESCRIPTION = model.DESCRIPTION,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }

        public static NoteModel ToNoteModel(NoteViewModel model)
        {
            if (model == null)
            {
                return new NoteModel();
            }
            return new NoteModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                NOTE_TYPE = model.NOTE_TYPE,
                CALC_START_DATE = model.CALC_START_DATE,
                CALC_END_DATE = model.CALC_END_DATE,
                CALCULATE_METHOD = model.CALCULATE_METHOD,
                PRICE = model.PRICE,
                CURRENCY = model.CURRENCY,
                PAYMENT_PERIOD = model.PAYMENT_PERIOD,
                DESCRIPTION = model.DESCRIPTION,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By,
                Create_Time = model.Create_Time
            };
        }

        public static MarginModel ToMarginModel(MarginViewModel model)
        {
            if (model == null)
            {
                return new MarginModel();
            }
            return new MarginModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                RECEIVED_DATE = model.RECEIVED_DATE,
                RETURN_DATE = model.RETURN_DATE,
                BANK = model.BANK,
                PRICE = model.PRICE,
                CURRENCY = model.CURRENCY,
                CURRENCY_VALUE = model.CURRENCY_VALUE,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }

        public static PaymentAccountModel ToPaymentAccountModel(PaymentAccountViewModel model)
        {
            if (model == null)
            {
                return new PaymentAccountModel();
            }
            return new PaymentAccountModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                ACCOUNT = model.ACCOUNT,
                BANK = model.BANK,
                ACCOUNT_NAME = model.ACCOUNT_NAME,
                PAYMENT_METHOD = model.PAYMENT_METHOD,
                DESCRIPTION = model.DESCRIPTION,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }

        public static ParkingModel ToParkingModel(ParkingViewModel model)
        {
            if (model == null)
            {
                return new ParkingModel();
            }
            return new ParkingModel()
            {
                ID = model.ID,
                LEASE_HEADER_ID = model.LEASE_HEADER_ID,
                LEASE_NUMBER = model.LEASE_NUMBER,
                CITY_NAME = model.CITY_NAME,
                DISTRICT_NAME = model.DISTRICT_NAME,
                SECTION_NAME = model.SECTION_NAME,
                SUBSECTION_NAME = model.SUBSECTION_NAME,
                PARKING_NO = model.PARKING_NO,
                PARKING_USE_TYPE = model.PARKING_USE_TYPE,
                FLOOR = model.FLOOR,
                RENTAL_AMOUNT = model.RENTAL_AMOUNT,
                DESCRIPTION = model.DESCRIPTION,
                Created_By = model.Created_By,
                Created_ByName = model.Created_ByName,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By,
                ADDRESS = model.ADDRESS
            };
        }

        #endregion

        #region== 共用 ==
        /// <summary>
        /// Controller取的特定的View
        /// </summary>
        /// <param name="ViewName">View名稱</param>
        /// <returns></returns>
        public static string GetIntegrationView(string ViewName)
        {
            return "~/Views/Integration/" + ViewName + ".cshtml";
        }


        public static string GetIntegrationRenewalView(string ViewName)
        {
            return "~/Views/IntegrationRenewal/" + ViewName + ".cshtml";
        }

        // <summary>
        /// 取得表單編號
        /// </summary>
        /// <returns></returns>
        public static string GetFormCode(string Type)
        {
            string result = "";
            using (var db = new AS_LandBankEntities())
            {
                if (db.AS_VW_Code_Table.Any(o => o.Class == "IntegrationForm" && o.Class_Name == Type))
                {
                    result = db.AS_VW_Code_Table.First(o => o.Class == "IntegrationForm" && o.Class_Name == Type).Code_ID;
                }
            }
            return result;
        }

        public static SearchModel ToSearchModel(SearchViewModel viewModel)
        {
            return new SearchModel()
            {
                Page = viewModel.Page,
                Transaction_Type = viewModel.Transaction_Type,
                PageSize = viewModel.PageSize,
                TRXHeaderIDBgn = viewModel.TRXHeaderIDBgn,
                TRXHeaderIDEnd = viewModel.TRXHeaderIDEnd,
                BookTypeCode = viewModel.BookTypeCode,
                //AssignedBranch = viewModel.AssignedBranch,
                OfficeBranch = viewModel.OfficeBranch,
                Remark = viewModel.Remark,
                BeginDate = viewModel.BeginDate,
               // DisposeReason = viewModel.DisposeReason,
                EndDate = viewModel.EndDate,
                FlowStatus = viewModel.FlowStatus,
               // OriOfficeBranch = viewModel.OriOfficeBranch,
                Source = viewModel.Source
            };
        }

        public static SearchViewModel ToSearchViewModel(SearchModel model)
        {
            return new SearchViewModel()
            {
                Page = model.Page,
                Transaction_Type = model.Transaction_Type,
                PageSize = model.PageSize,
                TRXHeaderIDBgn = model.TRXHeaderIDBgn,
                TRXHeaderIDEnd = model.TRXHeaderIDEnd,
                BookTypeCode = model.BookTypeCode,               
                OfficeBranch = model.OfficeBranch,
                Remark = model.Remark
            };
        }

        /// <summary>
        /// 取得功能名稱
        /// </summary>
        /// <param name="Action"></param>
        /// <returns></returns>
        public static string GetFunctionTitle(string Action)
        {
            string FunctionName = "";
            switch (Action)
            {
                case "IntegrationSearch":
                    FunctionName = "承租契約查詢";
                    break;
                case "IntegrationAdded":
                    FunctionName = "承租契約新增";
                    break;
                case "IntegrationMaintain":
                    FunctionName = "承租契約維護";
                    break;
                case "IntegrationRenewal":
                    FunctionName = "承租續租換約";
                    break;
                case "IntegrationPay":
                    FunctionName = "承租租金繳納";
                    break;
                case "IntegrationTrack":
                    FunctionName = "承租進度追蹤";
                    break;
                
            }
            return FunctionName;
        }

        public static string GetWebRoot()
        {
            return ((HttpContext.Current.Request.ApplicationPath == "/") ? "" : HttpContext.Current.Request.ApplicationPath);
        }

        public static DataTable LinqQueryToDataTable<T>(IEnumerable<T> query)
        {
            DataTable tbl = new DataTable();
            PropertyInfo[] props = null;
            foreach (T item in query)
            {
                if (props == null) //尚未初始化
                {
                    Type t = item.GetType();
                    props = t.GetProperties();
                    foreach (PropertyInfo pi in props)
                    {
                        Type colType = pi.PropertyType;
                        //針對Nullable<>特別處理
                        if (colType.IsGenericType
                            && colType.GetGenericTypeDefinition() == typeof(Nullable<>))
                            colType = colType.GetGenericArguments()[0];
                        //建立欄位
                        tbl.Columns.Add(pi.Name, colType);
                    }
                }
                DataRow row = tbl.NewRow();
                foreach (PropertyInfo pi in props)
                    row[pi.Name] = pi.GetValue(item, null) ?? DBNull.Value;
                tbl.Rows.Add(row);
            }
            return tbl;
        }

        #endregion
    }
}