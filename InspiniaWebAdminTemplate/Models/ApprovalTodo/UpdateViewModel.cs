﻿namespace InspiniaWebAdminTemplate.Models.ApprovalTodo
{
    public class UpdateViewModel
    {
        public int ID { get; set; }
        public int Action { get; set; }
        public int RejectToStep { get; set; }
        public string Remark { get; set; }
    }
}