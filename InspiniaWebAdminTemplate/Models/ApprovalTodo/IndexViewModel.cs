﻿using InspiniaWebAdminTemplate.Models.Common;
using Library.Common.Models;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.ApprovalTodo
{
    public class IndexViewModel
    {
        public PageList<FlowOpenViewModel> Page { get; set; }
        public FlowSearchOptionsModel Options { get; set; }
        public SearchViewModel SearchCondition { get; set; }
    }
}