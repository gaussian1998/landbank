﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.MP
{
    public class AssetsIndexViewModel
    {
        public IEnumerable<AssetViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public AssetsSearchViewModel condition { get; set; }
    }
}