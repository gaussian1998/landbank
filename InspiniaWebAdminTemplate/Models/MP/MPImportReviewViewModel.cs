﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.MP
{
    public class MPImportReviewViewModel
    {
        public string IRID { get; set; }

        public IEnumerable<MPImportViewModel> Imports { get; set; }

        public IEnumerable<MPViewModel> MPs { get; set; }
    }
}