﻿using Library.Servant.Servant.MP.MPModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.MP
{
    public static class MPHelper
    {
        #region ==ViewModel To Model==
        public static MPHeaderModel ToMPHeaderModel(MPHeaderViewModel viewModel)
        {
            return new MPHeaderModel()
            {
                ID = viewModel.ID,
                TRX_Header_ID = viewModel.TRX_Header_ID,
                Transaction_Type = viewModel.Transaction_Type,
                Flow_Status = viewModel.Flow_Status,
                Source = viewModel.Source,
                Book_Type = viewModel.Book_Type,
                Office_Branch = viewModel.Office_Branch,
                Remark = viewModel.Remark,
                AssetsCount = viewModel.AssetsCount,
                DisposeReason = viewModel.DisposeReason,
                OriOffice_Branch = viewModel.OriOffice_Branch,
                Created_By = viewModel.Created_By,
                Create_Time = viewModel.Create_Time,
                Last_Updated_By = viewModel.Last_Updated_By,
                Last_Updated_Time = viewModel.Last_Updated_Time,
                SummarySDate = viewModel.SummarySDate,
                SummaryEDate = viewModel.SummaryEDate
            };
        }

        public static AssetModel ToAssetModel(AssetViewModel viewModel)
        {
            return new AssetModel()
            {
                ID = viewModel.ID,
                Assets_Parent_Number = viewModel.Assets_Parent_Number,
                IsOversea = viewModel.IsOversea,
                Asset_Category_Code = viewModel.Asset_Category_Code,
                Assets_Name = viewModel.Assets_Name,
                Assets_Alias = viewModel.Assets_Alias,
                Asset_Number = viewModel.Asset_Number,
                type1 = viewModel.type1,
                type2 = viewModel.type2,
                type3 = viewModel.type3,
                Assets_Original_ID = viewModel.Assets_Original_ID,
                Life_Years = viewModel.Life_Years,
                Assets_Category_ID = viewModel.Assets_Category_ID,
                Deprn_Method_Code = viewModel.Deprn_Method_Code,
                Date_Placed_In_Service = viewModel.Date_Placed_In_Service,
                Assets_Unit = viewModel.Assets_Unit,
                Location_Disp = viewModel.Location_Disp,
                Assigned_Branch = viewModel.Assigned_Branch,
                Assigned_ID = viewModel.Assigned_ID,
                PO_Number = viewModel.PO_Number,
                PO_Description = viewModel.PO_Description,
                Model_Number = viewModel.Model_Number,
                Transaction_Date = viewModel.Transaction_Date,
                Assets_Fixed_Cost = viewModel.Assets_Fixed_Cost,
                Deprn_Reserve = viewModel.Deprn_Reserve,
                Salvage_Value = viewModel.Salvage_Value,
                Currency = viewModel.Currency,
                Description = viewModel.Description,
                AccessoryEquipment = viewModel.AccessoryEquipment,
                PropertyUnit = viewModel.PropertyUnit,
                MachineCode = viewModel.MachineCode,
                LicensePlateNumber = viewModel.LicensePlateNumber,
                InventoryCheck = viewModel.InventoryCheck,
                InventoryCheckMemo = viewModel.InventoryCheckMemo,
                Post_Date = viewModel.Post_Date,
                ImportNum = viewModel.ImportNum,
                Created_By = viewModel.Created_By,
                Create_Time = viewModel.Create_Time,
                Last_Updated_By = viewModel.Last_Updated_By,
                Last_Updated_Time = viewModel.Last_Updated_Time
            };
        }

        public static SearchModel ToSearchModel(SearchViewModel viewModel)
        {
            return new SearchModel()
            {
                Page = viewModel.Page,
                Transaction_Type = viewModel.Transaction_Type,
                PageSize = viewModel.PageSize,
                TRXHeaderIDBgn = viewModel.TRXHeaderIDBgn,
                TRXHeaderIDEnd = viewModel.TRXHeaderIDEnd,
                BookTypeCode = viewModel.BookTypeCode,
                AssignedBranch = viewModel.AssignedBranch,
                OfficeBranch = viewModel.OfficeBranch,
                Remark = viewModel.Remark,
                BeginDate = viewModel.BeginDate,
                DisposeReason = viewModel.DisposeReason,
                EndDate = viewModel.EndDate,
                FlowStatus = viewModel.FlowStatus,
                OriOfficeBranch = viewModel.OriOfficeBranch,
                Source = viewModel.Source
            };
        }

        public static ImportSearchModel ToImportSearchModel(ImportSearchViewModel viewModel)
        {
            return new ImportSearchModel()
            {
                 ImportBeginDate = viewModel.ImportBeginDate,
                 ImportEndDate = viewModel.ImportEndDate,
                 ImportMonth = viewModel.ImportMonth,
                 ImportNum = viewModel.ImportNum,
                 ImportNumBegin = viewModel.ImportNumBegin,
                 ImportNumEnd = viewModel.ImportNumEnd,
                 ImportYear = viewModel.ImportYear,
                 Page = viewModel.Page,
                 PageSize = viewModel.PageSize,
                 PONumberBegin = viewModel.PONumberBegin,
                 PONumberEnd = viewModel.PONumberEnd,
                 Status = viewModel.Status,
                 LastRecord = viewModel.LastRecord
            };
        }

        public static AssetsSearchModel ToAssetsSearchModel(AssetsSearchViewModel viewModel)
        {
            return new AssetsSearchModel()
            {
                Assets_Alias = viewModel.Assets_Alias,
                Assets_Category_ID = viewModel.Assets_Category_ID,
                Assets_Name = viewModel.Assets_Name,
                Asset_Category_Code = viewModel.Asset_Category_Code,
                Asset_Number = viewModel.Asset_Number,
                Assigned_Branch = viewModel.Assigned_Branch,
                Assigned_ID = viewModel.Assigned_ID,
                Date_Placed_In_Service_End = viewModel.Date_Placed_In_Service_End,
                Date_Placed_In_Service_Start = viewModel.Date_Placed_In_Service_Start,
                Deprn_Date = viewModel.Deprn_Date,
                Location_Disp = viewModel.Location_Disp,
                Model_Number = viewModel.Model_Number,
                PO_Number = viewModel.PO_Number,
                Remark = viewModel.Remark,
                Salvage_Value_End = viewModel.Salvage_Value_End,
                Salvage_Value_Start = viewModel.Salvage_Value_Start,
                Page = viewModel.Page,
                PageSize = viewModel.PageSize,
                Asset_NumberBegin = viewModel.Asset_NumberBegin,
                Asset_NumberEnd = viewModel.Asset_NumberEnd,
                Assets_Category_IDBegin = viewModel.Assets_Category_IDBegin,
                Assets_Category_IDEnd = viewModel.Assets_Category_IDEnd,
                IsOversea = viewModel.IsOversea
            };

        }

        public static MPImportModel ToMPImportModel(MPImportViewModel viewmodel)
        {
            return new MPImportModel()
            {
                Import_Num = viewmodel.Import_Num,
                Import_Month = viewmodel.Import_Month,
                Import_Year = viewmodel.Import_Year,
                PO_Number = viewmodel.PO_Number,
                Office_Branch = viewmodel.Office_Branch,
                Source = viewmodel.Source,
                Status = viewmodel.Status,
                Remark = (!string.IsNullOrEmpty(viewmodel.Remark)) ? viewmodel.Remark : "",
                Created_By = viewmodel.Created_By,
                Create_Time = viewmodel.Create_Time,
                Last_Updated_By = (!string.IsNullOrEmpty(viewmodel.Last_Updated_By)) ? viewmodel.Last_Updated_By : "",
                Last_Updated_Time = viewmodel.Last_Updated_Time
            };
        }
        #endregion

        #region== Model To ViewModel ==
        public static MPHeaderViewModel ToMPHeaderViewModel(MPHeaderModel model)
        {
            return new MPHeaderViewModel()
            {
                ID = model.ID,
                TRX_Header_ID = model.TRX_Header_ID,
                Transaction_Type = model.Transaction_Type,
                Transaction_TypeName = model.Transaction_TypeName,
                Flow_Status = model.Flow_Status,
                Source = model.Source,
                SourceName = model.SourceName,
                Book_Type = model.Book_Type,
                Office_Branch = model.Office_Branch,
                Remark = model.Remark,
                AssetsCount = model.AssetsCount,
                DisposeReason = model.DisposeReason,
                OriOffice_Branch = model.OriOffice_Branch,
                Created_ByName = model.Created_ByName,
                Flow_StatusName = model.Flow_StatusName,
                OriOffice_BranchName = model.OriOffice_BranchName,
                Office_BranchName = model.Office_BranchName,
                Create_Time = model.Create_Time
            };
        }

        public static AssetViewModel ToAssetViewModel(AssetModel model)
        {
            AssetViewModel Result = null;
            if(model != null)
            {
                Result = new AssetViewModel()
                {
                    ID = model.ID,
                    Assets_Parent_Number = model.Assets_Parent_Number,
                    IsOversea = model.IsOversea,
                    Asset_Category_Code = model.Asset_Category_Code,
                    Assets_Name = model.Assets_Name,
                    Assets_Alias = model.Assets_Alias,
                    Asset_Number = model.Asset_Number,
                    Assets_Original_ID = model.Assets_Original_ID,
                    Life_Years = model.Life_Years,
                    Assets_Category_ID = model.Assets_Category_ID,
                    Deprn_Method_Code = model.Deprn_Method_Code,
                    Date_Placed_In_Service = model.Date_Placed_In_Service,
                    Assets_Unit = model.Assets_Unit,
                    Location_Disp = model.Location_Disp,
                    Assigned_Branch = model.Assigned_Branch,
                    Assigned_BranchName = model.Assigned_BranchName,
                    Assigned_ID = model.Assigned_ID,
                    Assigned_IDName = model.Assigned_IDName,
                    Assigned_IDEmail = model.Assigned_IDEmail,
                    Assigned_IDPhone = model.Assigned_IDPhone,
                    PO_Number = model.PO_Number,
                    PO_Description = model.PO_Description,
                    Model_Number = model.Model_Number,
                    Transaction_Date = model.Transaction_Date,
                    Assets_Fixed_Cost = model.Assets_Fixed_Cost,
                    Deprn_Reserve = model.Deprn_Reserve,
                    Salvage_Value = model.Salvage_Value,
                    Currency = model.Currency,
                    Description = model.Description,
                    AccessoryEquipment = model.AccessoryEquipment,
                    PropertyUnit = model.PropertyUnit,
                    MachineCode = model.MachineCode,
                    LicensePlateNumber = model.LicensePlateNumber,
                    InventoryCheck = model.InventoryCheck,
                    InventoryCheckMemo = model.InventoryCheckMemo,
                    Post_Date = model.Post_Date,
                    ImportNum = model.ImportNum,
                    Created_By = model.Created_By,
                    Create_Time = model.Create_Time,
                    Last_Updated_By = model.Last_Updated_By,
                    Last_Updated_Time = model.Last_Updated_Time,
                    Not_Deprn_Flag = model.Not_Deprn_Flag
                };
            }
            return Result;
        }

        public static SearchViewModel ToSearchViewModel(SearchModel model)
        {
            return new SearchViewModel()
            {
                Page = model.Page,
                Transaction_Type = model.Transaction_Type,
                PageSize = model.PageSize,
                TRXHeaderIDBgn = model.TRXHeaderIDBgn,
                TRXHeaderIDEnd = model.TRXHeaderIDEnd,
                BookTypeCode = model.BookTypeCode,
                AssignedBranch = model.AssignedBranch,
                OfficeBranch = model.OfficeBranch,
                Remark = model.Remark
            };
        }

        public static MPImportViewModel ToMPImportViewModel(MPImportModel model)
        {
            return new MPImportViewModel()
            {
                ID = model.ID,
                Import_Num = model.Import_Num,
                Import_Month = model.Import_Month,
                Import_Year = model.Import_Year,
                PO_Number = model.PO_Number,
                Source = model.Source,
                Status = model.Status,
                Remark = (!string.IsNullOrEmpty(model.Remark)) ? model.Remark : "",
                Created_By = model.Created_By,
                Create_Time = model.Create_Time,
                Last_Updated_By = (!string.IsNullOrEmpty(model.Last_Updated_By)) ? model.Last_Updated_By : "",
                Last_Updated_Time = model.Last_Updated_Time,
                ImportAssetsCount = model.ImportAssetsCount,
                Created_ByName = model.Created_ByName,
                Office_Branch = model.Office_Branch,
                Office_BranchName = model.Office_BranchName,
                StatusName = model.StatusName
            };
        }
        #endregion

        #region== 共用 ==
        /// <summary>
        /// Controller取的特定的View
        /// </summary>
        /// <param name="ViewName">View名稱</param>
        /// <returns></returns>
        public static string GetMPView(string ViewName)
        {
            return "~/Views/MP/"+ ViewName + ".cshtml";
        }

        /// <summary>
        /// 取得功能名稱
        /// </summary>
        /// <param name="Action"></param>
        /// <returns></returns>
        public static string GetFunctionTitle(string Action)
        {
            string FunctionName = "";
            switch (Action)
            {
                case "MPArchive":
                    FunctionName = "資產查詢";
                    break;
                case "MPAdded":
                    FunctionName = "動產新增";
                    break;
                case "MPMaintain":
                    FunctionName = "動產維護";
                    break;
                case "MPTransfer":
                    FunctionName = "動產移轉";
                    break;
                case "MPDisposal":
                    FunctionName = "動產處分";
                    break;
                case "MPScrappedNotice":
                    FunctionName = "動產例行性報廢通知";
                    break;
                case "MPScrappedReciprocation":
                    FunctionName = "動產例行性報廢回報";
                    break;
                case "MPDeprn":
                    FunctionName = "動產折舊";
                    break;
                case "MPInventoryNotice":
                    FunctionName = "動產盤點通知單";
                    break;
                case "MPInventoryReciprocation":
                    FunctionName = "動產盤點回報單";
                    break;
                case "MPInventorySummary":
                    FunctionName = "動產盤點匯整單";
                    break;
                case "LeaseRightsAdded":
                    FunctionName = "租賃權益新增";
                    break;
                case "LeaseRightsMaintain":
                    FunctionName = "租賃權益維護";
                    break;
                case "LeaseRightsSearch":
                    FunctionName = "租賃權益查詢";
                    break;
                case "LeaseRightsDisposal":
                    FunctionName = "租賃權益處分";
                    break;
                case "LeaseRightsDeprn":
                    FunctionName = "租賃權益攤銷";
                    break;
                case "MPDeprnSetting":
                    FunctionName = "租賃攤銷設定";
                    break;
            }
            return FunctionName;
        }

        public static string GetWebRoot()
        {
            return ((HttpContext.Current.Request.ApplicationPath == "/") ? "" : HttpContext.Current.Request.ApplicationPath);
        }

        public static DataTable LinqQueryToDataTable<T>(IEnumerable<T> query)
        {
            DataTable tbl = new DataTable();
            PropertyInfo[] props = null;
            foreach (T item in query)
            {
                if (props == null) //尚未初始化
                {
                    Type t = item.GetType();
                    props = t.GetProperties();
                    foreach (PropertyInfo pi in props)
                    {
                        Type colType = pi.PropertyType;
                        //針對Nullable<>特別處理
                        if (colType.IsGenericType
                            && colType.GetGenericTypeDefinition() == typeof(Nullable<>))
                            colType = colType.GetGenericArguments()[0];
                        //建立欄位
                        tbl.Columns.Add(pi.Name, colType);
                    }
                }
                DataRow row = tbl.NewRow();
                foreach (PropertyInfo pi in props)
                    row[pi.Name] = pi.GetValue(item, null) ?? DBNull.Value;
                tbl.Rows.Add(row);
            }
            return tbl;
        }

        public static string GetDefaultBookType(string BranchCode)
        {
            string result = "";
            // --人民幣
            if (BranchCode == "504" || BranchCode == "505" || BranchCode == "506" || BranchCode == "507" || BranchCode == "508")
            {
                result = "CNY_MP_IFRS";
            }
            //--新加坡
            else if (BranchCode == "503")
            {
                result = "SGD_MP_IFRS";
            }
            //--港幣
            else if(BranchCode == "502")
            {
                result = "HKD_MP_IFRS";
            }
            //--美金
            else if (BranchCode == "501")
            {
                result = "USD_MP_IFRS";
            }
            else
            {
                result = "NTD_MP_IFRS";
            }
            return result;
   
        }

        public static string GetDefaultCurrency(string BookType)
        {
            string result = "";
            // --人民幣
            if (BookType == "CNY_MP_IFRS")
            {
                result = "CNY";
            }
            //--新加坡
            else if (BookType == "SGD_MP_IFRS")
            {
                result = "SGD";
            }
            //--港幣
            else if (BookType == "HKD_MP_IFRS")
            {
                result = "HKD";
            }
            //--美金
            else if (BookType == "USD_MP_IFRS")
            {
                result = "USD";
            }
            else
            {
                result = "NTD";
            }
            return result;

        }
        #endregion
    }
}