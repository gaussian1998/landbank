﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.MP
{
    public class ImportIndexViewModel
    {
        public IEnumerable<MPImportViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public ImportSearchViewModel condition { get; set; }
    }
}