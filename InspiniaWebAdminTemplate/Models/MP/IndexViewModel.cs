﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.MP
{
    public class IndexViewModel
    {
        public IEnumerable<MPHeaderViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public SearchViewModel condition { get; set; }
    }
}