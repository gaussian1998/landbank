﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.MP
{
    public class AssetsSearchViewModel
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }
        public string Asset_Category_Code { get; set; }

        public string Assets_Name { get; set; }

        public string Assets_Alias { get; set; }

        public string Asset_Number { get; set; }

        public string Asset_NumberBegin { get; set; }
        public string Asset_NumberEnd { get; set; }

        public string Assets_Category_ID { get; set; }

        public string Assets_Category_IDBegin { get; set; }

        public string Assets_Category_IDEnd { get; set; }

        public DateTime? Date_Placed_In_Service_Start { get; set; }

        public DateTime? Date_Placed_In_Service_End { get; set; }

        public DateTime? Deprn_Date { get; set; }

        public decimal? Salvage_Value_Start { get; set; }

        public decimal? Salvage_Value_End { get; set; }

        public string Location_Disp { get; set; }

        public string Assigned_Branch { get; set; }

        public string Assigned_ID { get; set; }

        public string PO_Number { get; set; }

        public string Model_Number { get; set; }

        public string Remark { get; set; }

        public string IsOversea { get; set; }
    }
}