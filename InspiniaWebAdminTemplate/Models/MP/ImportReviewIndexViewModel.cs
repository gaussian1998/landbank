﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.MP
{
    public class ImportReviewIndexViewModel
    {
        public IEnumerable<MPImportReviewViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
    }
}