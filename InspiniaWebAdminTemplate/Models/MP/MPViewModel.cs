﻿using System.Collections.Generic;
using System;
using Library.Servant.Servant.MP.MPModels;

namespace InspiniaWebAdminTemplate.Models.MP
{
    public class MPViewModel
    {
        public MPHeaderViewModel Header { get; set; }

        public IEnumerable<AssetViewModel> Assets { get; set; }

        //目前登入使用者
        public UserDetail User { get; set; }

        #region==批次匯入審查欄位==
        //分行
        public string Assigned_Branch { get; set; }
        //經辦
        public string HandledBy { get; set; }
        //經辦電話
        public string Phone { get; set; }
        //經辦Email
        public string Email { get; set; }
        #endregion

        #region==動產盤點彙總==
        //盤點符合
        public IEnumerable<MPViewModel> OKList { get; set; }
        //盤點不符合
        public IEnumerable<MPViewModel> NOList { get; set; }
        #endregion
    }
}