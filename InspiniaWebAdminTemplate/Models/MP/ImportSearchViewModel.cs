﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.MP
{
    public class ImportSearchViewModel
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }

        public string ImportNum { get; set; }

        public string ImportNumBegin { get; set; }

        public string ImportNumEnd { get; set; }

        public DateTime? ImportBeginDate { get; set; }

        public DateTime? ImportEndDate { get; set; }

        public string PONumberBegin { get; set; }

        public string PONumberEnd { get; set; }

        public int? ImportYear { get; set; }

        public int? ImportMonth { get; set; }

        public string Status { get; set; }

        public bool LastRecord { get; set; }
    }
}