﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InspiniaWebAdminTemplate.Models.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.HistoryData.Models;

namespace InspiniaWebAdminTemplate.Models.HistoryDataExport
{
    public class IndexSearchViewModel : AbstractPageModel<IndexSearchViewModel>
    {
        public IndexSearchViewModel()
        {
            ImportRange = new RangeViewModel<DateTime>
            {
                Start = DateTime.Now,
                End = DateTime.Now
            };
        }

        public string TransactionCode { get; set; }
        public string TableName { get; set; }
        public RangeViewModel<DateTime> ImportRange { get; set; }

        public static explicit operator HistoryCondition(IndexSearchViewModel vm)
        {
            return new HistoryCondition {
                TransactionCode = vm.TransactionCode,
                TableName = vm.TableName,
                ImportRange = new RangeModel<DateTime>
                {
                    Start = vm.ImportRange.Start,
                    End = vm.ImportRange.End
                }
            };
        }
    }
}