﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.HistoryData.Models;

namespace InspiniaWebAdminTemplate.Models.HistoryDataExport
{
    public class IndexItemViewModel
    {
        public string HistoryTableName { get; set; }
        public string TableName { get; set; }
        public Nullable<DateTime> LatestUpdateDateTime { get; set; }
        public int TotoalAmount { get; set; }

        public static explicit operator IndexItemViewModel(HistoryItemsInfo from)
        {
            return new IndexItemViewModel
            {
                HistoryTableName = from.HistoryTableName,
                TableName = from.TableName,
                LatestUpdateDateTime = from.LatestUpdateDateTime,
                TotoalAmount = from.TotoalAmount
            };
        }
    }
}