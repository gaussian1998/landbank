﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Models.HistoryDataExport
{
    public class IndexViewModel
    {
        public IndexSearchViewModel Condition { get; set; }
        public List<IndexItemViewModel> Items { get; set; }
        public int TotoalAmount { get; set; }
        public SelectList TransactionOptions { get; set; }
        public SelectList TableOptions { get; set; }
    }
}