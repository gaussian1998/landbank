﻿using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.Example
{
    public class ExampleViewModel
    {
        public RentBase RentBase { get; set; }
        public RentDateTime RentDateTime { get; set; }
    }

    public class RentBase
    {
        public string unit { get; set; }
        public int number { get; set; }
        public string date { get; set; }
        public RentBase()
        {
            unit = "土銀";
            number = 500;
            date = "2016/03/24";
        }
    }

    /// <summary>
    /// 測試資料
    /// </summary>
    public class RentDateTime
    {
        public IEnumerable<TimeRegion> table
        {
            get
            {
                yield return new TimeRegion { start="2016/03/24", end = "2016/05/01" };
                yield return new TimeRegion { start = "2016/04/21", end = "2016/06/03" };
            }
        }
    }

    public class TimeRegion
    {
        public string start { get; set; }
        public string end { get; set; }
    }
}