﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InspiniaWebAdminTemplate.Models.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.PostsManagement.Models;

namespace InspiniaWebAdminTemplate.Models.PostReport
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public SearchViewModel()
        {
            CodeRange = new RangeViewModel<string>();
            PostDateTime = new RangeViewModel<DateTime>();
        }

        public string  Manager { get; set; }
        public RangeViewModel<string> CodeRange { get; set; }
        public RangeViewModel<DateTime> PostDateTime { get; set; }

        public static explicit operator ReportSearchModel(SearchViewModel vm)
        {
            return new ReportSearchModel
            {
                Manager = vm.Manager,
                CodeRange = (RangeModel<string>)vm.CodeRange,
                PostDateTime = (RangeModel<DateTime>)vm.PostDateTime
            };
        }
    }
}