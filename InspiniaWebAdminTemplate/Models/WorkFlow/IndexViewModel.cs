﻿using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.WorkFlow
{
    public class IndexViewModel
    {
        public List<DetailsViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public SearchViewModel SearchCondition { get; set; }
    }
}