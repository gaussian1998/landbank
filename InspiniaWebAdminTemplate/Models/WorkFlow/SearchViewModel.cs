﻿using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.WorkFlow.Models;

namespace InspiniaWebAdminTemplate.Models.WorkFlow
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public int FlowID { get; set; }
        public string FlowCode { get; set; }
        public bool? IsActive { get; set; }
        public bool? CancelCode { get; set; }
        public string OperationType { get; set;}

        public static explicit operator SearchModel(SearchViewModel vm)
        {
            return new SearchModel
            {
                Page = vm.Page,
                PageSize = vm.PageSize,
                FlowCode = vm.FlowCode,
                IsActive = vm.IsActive,
                CancelCode = vm.CancelCode,
                OperationType=vm.OperationType,
                FlowID=vm.FlowID
            };
        }
    }
}