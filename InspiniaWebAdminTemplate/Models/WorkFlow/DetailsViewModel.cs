﻿using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.WorkFlow
{
    public class DetailsViewModel
    {
        public int ID { get; set; }
        public string FlowCode { get; set; }
        public string FlowType { get; set; }
        public string FlowName { get; set; }
        public bool IsActive { get; set; }
        public bool CancelCode { get; set; }
        public List<string> FlowRoles { get; set; }
        public string Remark { get; set; }
        public string OperationTypeName { get; set; }
    }
}