﻿using Library.Utility;
using Library.Servant.Servant.WorkFlow.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.WorkFlow
{
    public class CreateViewModel
    {
        [Required]
        public string FlowCode { get; set; }
        [Required]
        public string FlowType { get; set; }
        [Required]
        public string FlowName { get; set; }
        public bool IsActive { get; set; }
        public bool CancelCode { get; set; }
        public decimal Version { get; set; }
        public string Remark { get; set; }
        public List<OptionModel<int,string>> CadidateFlowRoles { get; set; }
        public List<int> Roles { get; set; }

        public static explicit operator CreateViewModel(NewResult from)
        {
            return new CreateViewModel
            {
                IsActive = from.IsActive,
                CancelCode = from.CancelCode,
                Version = from.Version,
                CadidateFlowRoles = from.CadidateFlowRoles
            };
        }

        public static explicit operator CreateModel(CreateViewModel vm)
        {
            return new CreateModel
            {
                FlowCode = vm.FlowCode,
                FlowType = vm.FlowType,
                FlowName = vm.FlowName,
                IsActive = vm.IsActive,
                CancelCode = vm.CancelCode,
                Remark = vm.Remark,
                Roles = vm.Roles
            };
        }
    }


}