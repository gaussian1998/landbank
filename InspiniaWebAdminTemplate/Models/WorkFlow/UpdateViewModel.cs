﻿using Library.Utility;
using Library.Servant.Servant.WorkFlow.Models;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.WorkFlow
{
    public class UpdateViewModel
    {
        public int ID { get; set; }
        public string FlowCode { get; set; }
        public string FlowType { get; set; }
        public string FlowName { get; set; }
        public bool IsActive { get; set; }
        public bool CancelCode { get; set; }
        public string Remark { get; set; }
        public List<int> Roles { get; set; }

        public static explicit operator UpdateModel(UpdateViewModel vm)
        {
            //return MapProperty.Mapping<UpdateModel, UpdateViewModel>(vm);
            return new UpdateModel
            {
                ID = vm.ID,
                FlowCode = vm.FlowCode,
                FlowType = vm.FlowType,
                FlowName = vm.FlowName,
                IsActive = vm.IsActive,
                CancelCode = vm.CancelCode,
                Remark = vm.Remark,
                Roles = vm.Roles
            };
        }
    }
}