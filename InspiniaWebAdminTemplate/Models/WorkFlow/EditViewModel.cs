﻿using System.ComponentModel.DataAnnotations;
using Library.Servant.Servant.WorkFlow.Models;
using Library.Utility;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.WorkFlow
{
    public class EditViewModel
    {
        public int ID { get; set; }
        [Required]
        public string FlowCode { get; set; }
        [Required]
        public string FlowType { get; set; }
        [Required]
        public string FlowName { get; set; }
        public bool IsActive { get; set; }
        public bool CancelCode { get; set; }
        public decimal? Version { get; set; }
        public string Remark { get; set; }
        public List<OptionModel<int,string>> CadidateFlowRoles { get; set; }
        public List<int> Roles { get; set; }

        public static explicit operator EditViewModel(EditResult from)
        {
            //return MapProperty.Mapping<EditViewModel, EditResult>(from);
            return new EditViewModel
            {
                ID = from.ID,
                FlowCode = from.FlowCode,
                FlowType = from.FlowType,
                FlowName = from.FlowName,
                IsActive = from.IsActive,
                CancelCode = from.CancelCode,
                Version = from.Version,
                Remark = from.Remark,
                CadidateFlowRoles = from.CadidateFlowRoles,
                Roles = from.Roles
            };
        }
    }
}