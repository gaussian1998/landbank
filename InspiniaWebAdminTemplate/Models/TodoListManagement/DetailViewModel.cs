﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InspiniaWebAdminTemplate.Models.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.TodoListManagement.Models;

namespace InspiniaWebAdminTemplate.Models.TodoListManagement
{
    public class DetailViewModel
    {
        public string TodoCode { get; set; }
        public string Status { get; set; }
        public string StatusName { get; set; }
        public string Type { get; set; }
        public int AssignUserID { get; set; }
        public string AssignUser { get; set; }
        public List<TodoUserViewModel> TodoUsers { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public List<LinkViewModel> Links { get; set; }
        public RangeViewModel<DateTime> TodoPeriod { get; set; }
        public int RemindDays { get; set; }
        public List<AuditViewModel> AuditingFlowList { get; set; }
        public int UpdateUser { get; set; }
        public string UpdateUserName { get; set; }
        public HttpPostedFileBase File { get; set; }
        public string FileName { get; set; }

        public static explicit operator DetailViewModel(TodoListInfo from)
        {
            return new DetailViewModel
            {
                TodoCode = from.TodoCode,
                Status = from.Status,
                Type = from.Type,
                AssignUserID = from.AssignUserID,
                AssignUser = from.AssignUser,
                TodoUsers = from.TodoUsers?.Select(m => (TodoUserViewModel)m).ToList() ?? new List<TodoUserViewModel>(),
                Title = from.Title,
                Content = from.Content,
                Links = from.Links?.Select(m => (LinkViewModel)m).ToList() ?? new List<LinkViewModel>(),
                TodoPeriod = (RangeViewModel<DateTime>)from.TodoPeriod,
                RemindDays = from.RemindDays,
                AuditingFlowList = from.AuditingFlowList?.Select(m => (AuditViewModel)m).ToList() ?? new List<AuditViewModel>(),
                UpdateUser = from.UpdateUser,
                UpdateUserName = from.UpdateUserName,
                FileName = from.FileName
            };
        }

        public static explicit operator TodoListInfo(DetailViewModel vm)
        {
            return new TodoListInfo
            {
                TodoCode = vm.TodoCode,
                Status = vm.Status,
                Type = vm.Type,
                AssignUser = vm.AssignUser,
                AssignUserID = vm.AssignUserID,
                TodoUsers = vm.TodoUsers?.Select(m => (TodoUserInfo)m).ToList() ?? new List<TodoUserInfo>(),
                Title = vm.Title,
                Content = vm.Content,
                Links = vm.Links?.Select(m => new OptionModel<string, string> { Text = m.Name, Value = m.Url }).ToList() ?? new List<OptionModel<string, string>>(),
                TodoPeriod = (RangeModel<DateTime>)vm.TodoPeriod,
                RemindDays = vm.RemindDays,
                AuditingFlowList = vm.AuditingFlowList?.Select(m => (AuditFlowModel)m).ToList() ?? new List<AuditFlowModel>(),
                UpdateUser = vm.UpdateUser,
                UpdateUserName = vm.UpdateUserName,
                FileName = vm.FileName
            };
        }
    }
}