﻿using Library.Servant.Servant.TodoListManagement.Models;

namespace InspiniaWebAdminTemplate.Models.TodoListManagement
{
    public class AuditViewModel
    {
        public string FlowCode { get; set; }
        public string FlowStatus { get; set; }
        public int CurrentApproal { get; set; }
        public int CreateUser { get; set; }
        public string CurrentApproalName { get; set; }

        public static explicit operator AuditFlowModel(AuditViewModel vm)
        {
            return new AuditFlowModel
            {
                FlowCode = vm.FlowCode,
                FlowStatus = vm.FlowStatus,
                CreateUser = vm.CreateUser,
                CurrentApproal = vm.CurrentApproal
            };
        }

        public static explicit operator AuditViewModel(AuditFlowModel from)
        {
            return new AuditViewModel
            {
                FlowCode = from.FlowCode,
                FlowStatus = from.FlowStatus,
                CreateUser = from.CreateUser,
                CurrentApproal = from.CurrentApproal,
                CurrentApproalName = from.CurrentApproalName
            };
        }
    }
}