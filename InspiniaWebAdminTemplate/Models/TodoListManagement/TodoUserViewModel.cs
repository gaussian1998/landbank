﻿using Library.Servant.Servant.TodoListManagement.Models;

namespace InspiniaWebAdminTemplate.Models.TodoListManagement
{
    public class TodoUserViewModel
    {
        public string BranchCode { get; set; }
        public string DepartmentCode { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public bool IsNotifiedOnCreate { get; set; }
        public bool IsNotifiedForAlert { get; set; }
        public bool IsNotifiedOnFinish { get; set; }

        public static explicit operator TodoUserViewModel(TodoUserInfo from)
        {
            return new TodoUserViewModel
            {
                BranchCode = from.BranchCode,
                DepartmentCode = from.DepartmentCode,
                UserID = from.UserID,
                UserName = from.UserName,
                IsNotifiedForAlert = from.IsNotifiedForAlert,
                IsNotifiedOnCreate = from.IsNotifiedOnCreate,
                IsNotifiedOnFinish = from.IsNotifiedOnFinish
            };
        }

        public static explicit operator TodoUserInfo(TodoUserViewModel vm)
        {
            return new TodoUserInfo
            {
                BranchCode = vm.BranchCode,
                DepartmentCode = vm.DepartmentCode,
                UserID = vm.UserID,
                UserName = vm.UserName,
                IsNotifiedForAlert = vm.IsNotifiedForAlert,
                IsNotifiedOnCreate = vm.IsNotifiedOnCreate,
                IsNotifiedOnFinish = vm.IsNotifiedOnFinish
            };
        }
    }
}