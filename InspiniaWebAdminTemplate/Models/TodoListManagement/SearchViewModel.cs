﻿using InspiniaWebAdminTemplate.Models.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.TodoListManagement.Models;

namespace InspiniaWebAdminTemplate.Models.TodoListManagement
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public SearchViewModel()
        {
            BranchRange = new RangeViewModel<string>();
            TodoListCodeRange = new RangeViewModel<string>();
        }

        public RangeViewModel<string> BranchRange { get; set; }
        public RangeViewModel<string> TodoListCodeRange { get; set; }
        public string Status { get; set; }

        public static explicit operator SearchModel(SearchViewModel vm)
        {
            return new SearchModel
            {
                Page = vm.Page,
                PageSize = vm.PageSize,
                BranchRange = (RangeModel<string>)vm.BranchRange,
                TodoCodeRange = (RangeModel<string>)vm.TodoListCodeRange,
                Status = vm.Status
            };
        }
    }
}