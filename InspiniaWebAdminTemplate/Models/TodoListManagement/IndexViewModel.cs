﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.TodoListManagement
{
    public class IndexViewModel
    {
        public IndexViewModel()
        {
            Conditions = new SearchViewModel();
            Items = new List<ItemViewModel>();
        }

        public List<ItemViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public SearchViewModel Conditions { get; set; }
    }
}