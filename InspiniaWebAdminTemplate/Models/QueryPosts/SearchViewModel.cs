﻿using System;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Enums;
using Library.Servant.Servant.PostsManagement.Models;
using InspiniaWebAdminTemplate.Models.Common;

namespace InspiniaWebAdminTemplate.Models.QueryPosts
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public SearchViewModel()
        {
            BranchRange = new RangeViewModel<string>();
            FlowRange = new RangeViewModel<string>();
            AnnounceRange = new RangeViewModel<DateTime> {
                Start = DateTime.Now,
                End = DateTime.Now
            };
        }

        public RangeViewModel<string> BranchRange { get; set; }
        public RangeViewModel<string> FlowRange { get; set; }
        public RangeViewModel<DateTime> AnnounceRange { get; set; }
        public FormType DocumentType { get; set; }
        public string ApprovalStatus { get; set; }
        public int currentUser { get; set; }

        public static explicit operator SearchModel(SearchViewModel vm)
        {
            //return MapProperty.Mapping<SearchModel, SearchViewModel>(vm);
            return new SearchModel
            {
                Page = vm.Page,
                PageSize = vm.PageSize,                
                Start = vm.AnnounceRange.Start,
                End = vm.AnnounceRange.End,
                DocumentType = vm.DocumentType,
                Status = vm.ApprovalStatus,
                currentUser = vm.currentUser
            };
        }
    }
}