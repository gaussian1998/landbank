﻿using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.QueryPosts
{
    public class IndexViewModel
    {
        public IndexViewModel()
        {
            Condtiton = new SearchViewModel();
            Items = new List<ItemViewModel>();
        }

        public List<ItemViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public SearchViewModel Condtiton { get; set; }
    }
}