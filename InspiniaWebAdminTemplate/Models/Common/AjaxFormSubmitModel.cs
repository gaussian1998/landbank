namespace InspiniaWebAdminTemplate.Models.Common
{
    public class AjaxFormSubmitModel
    {
        public string PostUrl { get; set; }
        public string FormID { get; set; }
        public string SubmitButtonID { get; set; }
        public string DivRegionID { get; set; }
    }
}