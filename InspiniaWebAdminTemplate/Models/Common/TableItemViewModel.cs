﻿using System;
using System.Collections.Generic;


namespace InspiniaWebAdminTemplate.Models.Common
{
    public class TableItemViewModel
    {
        public int Index { get; set; }
        public List<string> Items { get; set; }
    }
}