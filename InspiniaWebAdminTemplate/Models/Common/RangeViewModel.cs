﻿using System;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.Common
{
    public class RangeViewModel<T>
    {
        public T Start { get; set; }
        public T End { get; set; }

        public static explicit operator RangeViewModel<T>(RangeModel<T> from)
        {
            return new RangeViewModel<T>
            {
                Start = from.Start,
                End = from.End
            };
        }

        public static explicit operator RangeModel<T>(RangeViewModel<T> from)
        {
            return new RangeModel<T>
            {
                Start = from.Start,
                End = from.End
            };
        }
    }
}