﻿using Library.Common.Extension;
using System;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.Common
{
    public class FlowOpenViewModel
    {
        public int ID { get; set; }
        public string FormNo { get; set; }
        public string DocTypeName { get; set; }
        public string DocName { get; set; }
        public string FlowTemplateName { get; set; }
        public List<string> FlowRoles { get; set; }
        public string BranchName { get; set; }
        public string ApplyUserName { get; set; }
        public string CurrentApprovalUserName { get; set; }
        public string NextApproval { get; set; }
        public decimal FlowStatus { get; set; }
        public string ApprovalStatus { get; set; }
        public bool IsApplyUser { get; set; }
        public string Remark { get; set; }
        public DateTime IssueTime
        {
            set
            {
                IssueTimeDescription = value.Description();
            }
        }
        public string IssueTimeDescription { get; set; }
    }
}