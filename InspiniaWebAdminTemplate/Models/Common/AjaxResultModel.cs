namespace InspiniaWebAdminTemplate.Models.Common
{
    public class AjaxResultModel
    {
        public string status { get; set; }
        public string message { get; set; }
    }
}