﻿using System.Collections.Generic;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.Common
{
    public class LinkViewModel
    {
        public string Name { get; set; }
        public string Url { get; set; }

        public static explicit operator LinkViewModel(OptionModel<string, string> from)
        {
            return new LinkViewModel
            {
                Name = from.Text,
                Url = from.Value
            };
        }
    }
}