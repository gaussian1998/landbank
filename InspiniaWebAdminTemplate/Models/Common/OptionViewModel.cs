﻿namespace InspiniaWebAdminTemplate.Models.Common
{
    public class OptionViewModel
    {
        public int Value { get; set; }
        public string Text { get; set; }
    }
}