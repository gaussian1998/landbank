﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.CodeTable
{
    public class IndexViewModel
    {
        public IndexSearchViewModel Condition { get; set; }
        public List<IndexItemViewModel> Items { get; set; }
        public int TotoalAmount { get; set; }
    }
}