﻿using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.CodeTableSetting.Models;

namespace InspiniaWebAdminTemplate.Models.CodeTable
{
    public class IndexSearchViewModel : AbstractPageModel<IndexSearchViewModel>
    {
        public string CodeClass { get; set; }
        public string Code { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }

        public static explicit operator SearchModel(IndexSearchViewModel vm)
        {
            return new SearchModel
            {
                Page = vm.Page,
                PageSize = vm.PageSize,
                CodeClass = vm.CodeClass,
                Code = vm.Code,
                IsActive = vm.IsActive,
                IsDeleted = vm.IsDeleted
            };
        }
    }
}