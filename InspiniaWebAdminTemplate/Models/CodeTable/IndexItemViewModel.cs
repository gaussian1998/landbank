﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.CodeTableSetting.Models;

namespace InspiniaWebAdminTemplate.Models.CodeTable
{
    public class IndexItemViewModel
    {
        public bool IsSelected { get; set; }
        public string CodeClassName { get; set; }
        public string CodeClass { get; set; }
        public string Code { get; set; }
        public string CodeName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string Parameter1 { get; set; }
        public string Parameter2 { get; set; }
        public decimal? Setting1 { get; set; }
        public decimal? Setting2 { get; set; }
        public decimal? Setting3 { get; set; }
        public string Remark { get; set; }

        public static explicit operator IndexItemViewModel(CodeTableInfo from)
        {
            return new IndexItemViewModel
            {
                CodeClass = from.CodeClass,
                CodeClassName = from.CodeClassName,
                Code = from.Code,
                CodeName = from.CodeName,
                IsActive = from.IsActive,
                IsDeleted = from.IsDeleted,
                Parameter1 = from.Parameter1,
                Parameter2 = from.Parameter2,
                Setting1 = from.Setting1,
                Setting2 = from.Setting2,
                Setting3 = from.Setting3,
                Remark = from.Remark
            };
        }

        public static explicit operator CodeTableInfo(IndexItemViewModel vm)
        {
            return new CodeTableInfo
            {
                CodeClass = vm.CodeClass,
                CodeClassName = vm.CodeClassName,
                Code = vm.Code,
                CodeName = vm.CodeName,
                IsActive = vm.IsActive,
                IsDeleted = vm.IsDeleted,
                Parameter1 = vm.Parameter1,
                Parameter2 = vm.Parameter2,
                Setting1 = vm.Setting1,
                Setting2 = vm.Setting2,
                Setting3 = vm.Setting3,
                Remark = vm.Remark
            };
        }
    }
}