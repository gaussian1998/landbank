﻿using Library.Servant.Servant.ApprovalMember.Models;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.ApprovalMember
{
    public class DetailsViewModel
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string DepartmentName { get; set; }
        public List<ApprovalRoleModel> ApprovalRoles { get; set; }

        public static explicit operator DetailsViewModel(DetailsResult from)
        {
            return new DetailsViewModel
            {
                UserID = from.UserID,
                UserName = from.UserName,
                BranchCode = from.BranchCode,
                BranchName = from.BranchName,
                DepartmentName = from.DepartmentName,
                ApprovalRoles = from.ApprovalRoles
            };
        }
    }
}