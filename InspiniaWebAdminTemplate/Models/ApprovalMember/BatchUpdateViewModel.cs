﻿

using Library.Servant.Servant.ApprovalMember.Models;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.ApprovalMember
{
    public class BatchUpdateViewModel
    {
        public List<int> UpdateUsers { get; set; }
        public int UpdateRoleID { get; set; }


        public static explicit operator BatchUpdateModel(BatchUpdateViewModel vm)
        {
            return new BatchUpdateModel
            {
                UpdateUsers = vm.UpdateUsers,
                UpdateRoleID = vm.UpdateRoleID
            };
        }
    }
}