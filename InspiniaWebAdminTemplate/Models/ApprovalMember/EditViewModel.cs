﻿using InspiniaWebAdminTemplate.Models.Common;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;


namespace InspiniaWebAdminTemplate.Models.ApprovalMember
{
    public class EditViewModel
    {
        /// <summary>
        /// Razor 範例
        /// </summary>
        public int UserID { get; set; }
        public string BranchName { get; set; }
        public string DepartmentName { get; set; }
        public string UserName { get; set; }

        /// <summary>
        ///  Vue js參數
        /// </summary>
        public IEnumerable<OptionModel<int, string>> Cadidate { get; set; }
        public IEnumerable<OptionModel<int, string>> Selected { get; set; }
    }


}