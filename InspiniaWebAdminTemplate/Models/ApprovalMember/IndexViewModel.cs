﻿using Library.Common.Models;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.ApprovalMember
{
    public class IndexViewModel
    {
        public PageList<DetailsViewModel> Page { get; set; }
        public List<OptionModel<string, string>> TotalDepartments { get; set; }
        public List<OptionModel<int, string>> TotalFlowRoles { get; set; }
        public SearchViewModel Search { get; set; }
    }
}