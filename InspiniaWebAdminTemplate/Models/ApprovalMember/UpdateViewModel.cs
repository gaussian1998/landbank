﻿using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.ApprovalMember
{
    public class UpdateViewModel
    {
        public int UserID { get; set; }
        public List<int> ApprovalRoles { get; set; }
        public List<int> ApprovalRoles1 { get; set; }
        public List<int> ApprovalRoles2 { get; set; }
        public List<int> ApprovalRoles3 { get; set; }
        public List<int> ApprovalRoles4 { get; set; }
    }
}