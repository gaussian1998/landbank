﻿
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.ApprovalMember
{
    public class BatchEditViewModel
    {
        public List< OptionModel<int,string> > Users { get; set; }
        public List<OptionModel<int, string>> TotalFlowRoles { get; set; }
        public string SearchRoleName { get; set; }
        public string SearchDepartmentName { get; set; }
    }
}