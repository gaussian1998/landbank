﻿using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.ApprovalMember.Models;
using System;

namespace InspiniaWebAdminTemplate.Models.ApprovalMember
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public string DepartmentCodeID { get; set; }
        public int? FlowRoleID { get; set; }
        public string UserName { get; set; }
        public string BranchCodeID { get; set; }

        public void Clear()
        {
            DepartmentCodeID = string.Empty;
            FlowRoleID = new Nullable<int>();
            UserName = string.Empty;
        }

        public static explicit operator SearchModel(SearchViewModel vm)
        {
            return new SearchModel
            {
                Page = vm.Page,
                PageSize = vm.PageSize,
                DepartmentCodeID = vm.DepartmentCodeID,
                FlowRoleID = vm.FlowRoleID,
                UserName = vm.UserName,
                BranchCodeID= vm.BranchCodeID
            };
        }
    }
}