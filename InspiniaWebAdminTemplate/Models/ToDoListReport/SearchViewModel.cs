﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InspiniaWebAdminTemplate.Models.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.TodoListManagement.Models;

namespace InspiniaWebAdminTemplate.Models.TodoListReport
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public SearchViewModel()
        {
            AssignDateTime = new RangeViewModel<DateTime>();
            AssignNumberRange = new RangeViewModel<string>();
        }

        public string AssignUser { get; set; }
        public RangeViewModel<string> AssignNumberRange { get; set; }
        public RangeViewModel<DateTime> AssignDateTime { get; set; }
        public bool IsNotFinished { get; set; }
        public string StatusCode { get; set; }

        public static explicit operator ReportSearchModel(SearchViewModel from)
        {
            return new ReportSearchModel
            {
                AssignUser = from.AssignUser,
                AssignNumberRange = (RangeModel<string>)from.AssignNumberRange,
                AssignDateTime = (RangeModel<DateTime>)from.AssignDateTime,
                IsNotFinished = from.IsNotFinished,
                StatusCode = from.StatusCode
            };
        }
    }
}