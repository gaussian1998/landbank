﻿
using System.Collections.Generic;
using System.Web.Mvc;

namespace InspiniaWebAdminTemplate.Models.HistoryDataStatistics
{
    public class IndexViewModel
    {
        public string SelectedType { get; set; }

        public IEnumerable<SelectListItem> TypeList { get; set; }
    }
}