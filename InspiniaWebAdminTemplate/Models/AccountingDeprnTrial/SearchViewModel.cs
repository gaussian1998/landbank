﻿using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.AccountingDeprnTrial
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public List<string> TradeKinds { get; set; }
        public SearchViewModel()
        {
            TradeKinds = new List<string> { };
        }
    }
}