﻿
using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.AccountingDeprnTrial
{
    public class IndexViewModel
    {
        public bool IsApproval { get; set; }
        public TradeOptions Options { get; set; }
        public List<AccountEntryDetailResult> Items { get; set; }
        public SearchViewModel Condition { get; set; }
    }
}