﻿using Library.Servant.Servant.CodeTable.Models;

namespace InspiniaWebAdminTemplate.Models.AccountingEntryStatistics
{
    public class IndexViewModel
    {
        public BranchDepartmentOptions Options { get; set; }
    }
}