﻿using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.HistoryData.Models;

namespace InspiniaWebAdminTemplate.Models.HistoryDataSetting
{
    public class IndexSearchViewModel : AbstractPageModel<IndexSearchViewModel>
    {
        public string TrasactionCode { get; set; }

        public static explicit operator SettingCondition(IndexSearchViewModel vm)
        {
            return new SettingCondition
            {
                TrasactionCode = vm.TrasactionCode,
                Page = vm.Page,
                PageSize = vm.PageSize
            };
        }
    }
}