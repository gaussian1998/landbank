﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.HistoryData.Models;

namespace InspiniaWebAdminTemplate.Models.HistoryDataSetting
{
    public class IndexItemViewModel
    {
        public string Table { get; set; }
        public string TransactionType { get; set; }
        public string TransactionTypeName { get; set; }
        public string TableName { get; set; }
        public bool IsKeepFormal { get; set; }
        public int KeepMonthFormal { get; set; }
        public bool IsKeepHistory { get; set; }
        public int KeepMonthHistory { get; set; }

        public static explicit operator IndexItemViewModel(SettingItemsInfo from)
        {
            return new IndexItemViewModel
            {
                Table = from.Table,
                TransactionType = from.TransactionType,
                TransactionTypeName = from.TransactionTypeName,
                TableName = from.TableName,
                IsKeepFormal = from.IsKeepFormal,
                KeepMonthFormal = from.KeepMonthFormal,
                IsKeepHistory = from.IsKeepHistory,
                KeepMonthHistory = from.KeepMonthHistory
            };
        }

        public static explicit operator SettingItemsInfo(IndexItemViewModel vm)
        {
            return new SettingItemsInfo
            {
                Table = vm.Table,
                TransactionType = vm.TransactionType,
                TableName = vm.TableName,
                IsKeepFormal = vm.IsKeepFormal,
                KeepMonthFormal = vm.KeepMonthFormal,
                IsKeepHistory = vm.IsKeepHistory,
                KeepMonthHistory = vm.KeepMonthHistory
            };
        }
    }
}