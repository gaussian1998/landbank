﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Servant.HistoryData.Models;

namespace InspiniaWebAdminTemplate.Models.HistoryDataSetting
{
    public class IndexViewModel
    {
        public IndexSearchViewModel Condition { get; set; }
        public List<IndexItemViewModel> Items { get; set; }
        public int TotoalAmount { get; set; }
        public SelectList TrasactionList { get; set; }

        public static explicit operator IndexViewModel(IndexResult<SettingItemsInfo> from)
        {
            return new IndexViewModel
            {
                Items = from.Items.Select(m => (IndexItemViewModel)m).ToList(),
                TotoalAmount = from.TotalAmount
            };
        }
    }
}