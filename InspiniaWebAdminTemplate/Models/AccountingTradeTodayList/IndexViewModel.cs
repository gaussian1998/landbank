﻿
using Library.Servant.Servant.CodeTable.Models;

namespace InspiniaWebAdminTemplate.Models.AccountingTradeTodayList
{
    public class IndexViewModel
    {
        public BranchDepartmentOptions Options { get; set; }
    }
}