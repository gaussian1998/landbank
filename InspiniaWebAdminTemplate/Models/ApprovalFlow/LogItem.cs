﻿using Library.Common.Extension;
using System;

namespace InspiniaWebAdminTemplate.Models.ApprovalFlow
{
    public class LogItem
    {
        public string BranchName { get; set; }
        public string ApplyUserName { get; set; }
        public string UpdateTimeDescription { get; set; }
        public DateTime UpdateTime
        {
            set
            {
                UpdateTimeDescription = value.Description();
            }
        }
        public string Result { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
    }
}