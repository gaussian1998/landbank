﻿using System;
using System.Collections.Generic;


namespace InspiniaWebAdminTemplate.Models.ApprovalFlow
{
    public class LogsViewModel
    {
        public string FormNo { get; set; }
        public string DocType { get; set; }
        public string DocName { get; set; }
        public List<LogItem> Logs { get; set; }
    }
}