﻿using InspiniaWebAdminTemplate.Models.Common;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.ApprovalFlow
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public FlowSelectedOptionsModel SelectedOptions { get; set; }
        public SearchViewModel()
        {
            SelectedOptions = new FlowSelectedOptionsModel();
        }
    }
}