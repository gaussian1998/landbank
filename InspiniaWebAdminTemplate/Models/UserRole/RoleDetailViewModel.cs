﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.QueryUserRole.Models;

namespace InspiniaWebAdminTemplate.Models.QueryUserRole
{
    public class RoleDetailViewModel
    {
        public bool IsChecked { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public string DocType { get; set; }
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public List<string> Programs { get; set; }

        public static explicit operator RoleDetailViewModel(RoleDetailModel from)
        {
            return new RoleDetailViewModel
            {
                IsChecked = from.IsChecked,
                ID = from.ID,
                Name = from.Name,
                DocType = from.DocType,
                GroupID = from.GroupID,
                GroupName = from.GroupName,
                Programs = from.Programs
            };
        }

        public static explicit operator RoleDetailModel(RoleDetailViewModel vm)
        {
            return new RoleDetailModel
            {
                IsChecked = vm.IsChecked,
                ID = vm.ID,
                Name = vm.Name,
                DocType = vm.DocType,
                GroupID = vm.GroupID,
                GroupName = vm.GroupName,
                Programs = vm.Programs
            };
        }
    }
}