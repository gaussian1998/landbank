﻿using System;
using System.Collections.Generic;
using Library.Servant.Enums;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.UserRole.Models;

namespace InspiniaWebAdminTemplate.Models.UserRole
{
    public class ItemViewModel
    {
        public string Branch { get; set; }
        public string Department { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public List<string> LoginRoleNames { get; set; }

        public static explicit operator ItemViewModel(ListItemResult from)
        {
            return new ItemViewModel {
                Branch = from.Branch,
                Department = from.Department,
                UserCode = from.UserCode,
                UserName = from.UserName,
                LoginRoleNames = from.LoginRoleNames
            };
        }
    }
}