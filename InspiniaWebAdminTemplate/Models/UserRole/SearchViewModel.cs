﻿using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.UserRole.Models;

namespace InspiniaWebAdminTemplate.Models.UserRole
{
    public class SearchViewModel : AbstractPageModel<HeaderViewModel>
    {
        public string Branch { get; set; }
        public string Department { get; set; }

        public static explicit operator SearchModel(SearchViewModel vm)
        {
            //return MapProperty.Mapping<SearchModel, SearchViewModel>(vm);
            return new SearchModel
            {
                Branch = vm.Branch,
                Department = vm.Department,
                Page = vm.Page,
                PageSize = vm.PageSize
            };
        }
    }
}