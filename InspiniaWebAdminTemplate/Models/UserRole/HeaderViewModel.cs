﻿using Library.Servant.Enums;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.UserRole.Models;

namespace InspiniaWebAdminTemplate.Models.UserRole
{
    public class HeaderViewModel : AbstractPageModel<HeaderViewModel>
    {
        public int AppliedUserId { get; set; }
        public string FlowCode { get; set; }
        public string Status { get; set; }
        public FormType Type { get; set; }
        public string Reason { get; set; }
        public bool IsModified { get; set; }

        public static explicit operator HeaderModel(HeaderViewModel vm)
        {
            //return MapProperty.Mapping<SearchModel, SearchViewModel>(vm);
            return new HeaderModel
            {
                AppliedUserId = vm.AppliedUserId,
                FlowCode = vm.FlowCode,
                Status = vm.Status,
                Type = vm.Type,
                Reason = vm.Reason,
                IsModified = vm.IsModified
            };
        }

        public static explicit operator HeaderViewModel(HeaderModel from)
        {
            return new HeaderViewModel
            {
                AppliedUserId = from.AppliedUserId,
                FlowCode = from.FlowCode,
                Status = from.Status,
                Type = from.Type,
                Reason = from.Reason,
                IsModified = from.IsModified
            };
        }
    }
}