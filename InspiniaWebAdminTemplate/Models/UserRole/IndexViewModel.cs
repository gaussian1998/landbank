﻿using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;
using Library.Servant.Servant.UserRole.Models;

namespace InspiniaWebAdminTemplate.Models.UserRole
{
    public class IndexViewModel
    {
        public List<ItemViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public SearchViewModel Condition { get; set; }
    }
}