﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.UserRole.Models;

namespace InspiniaWebAdminTemplate.Models.UserRole
{
    public class DetailViewModel
    {
        public DetailViewModel()
        {
            HeaderInfo = new HeaderViewModel();
            TotalRoles = new List<RoleDetailViewModel>();
        }
        
        public HeaderViewModel HeaderInfo { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string BranchName { get; set; }
        public string Department { get; set; }
        public List<RoleDetailViewModel> TotalRoles { get; set; }
        public string FlowCode { get; set; }

        public static explicit operator DetailViewModel(DetailModel from)
        {
            return new DetailViewModel
            {
                UserCode = from.UserCode,
                UserName = from.UserName,
                BranchName = from.BranchName,
                Department = from.Department,
                HeaderInfo = (HeaderViewModel)from.HeaderInfo,
                TotalRoles = from.TotalRoles.Select(m => (RoleDetailViewModel)m).ToList()
            };
        }

        public static explicit operator DetailModel(DetailViewModel vm)
        {
            return new DetailModel
            {
                UserCode = vm.UserCode,
                UserName = vm.UserName,
                BranchName = vm.BranchName,
                Department = vm.Department,
                HeaderInfo = (HeaderModel)vm.HeaderInfo,
                TotalRoles = vm.TotalRoles.Select(m => (RoleDetailModel)m).ToList()
            };
        }
    }
}