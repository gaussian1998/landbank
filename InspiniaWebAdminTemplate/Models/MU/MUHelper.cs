﻿using Library.Servant.Servant.MU.MUModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;


namespace InspiniaWebAdminTemplate.Models.MU
{
    public class MUHelper
    {
        public static string GetIni(string path,string v1,string v2)
        {
            SetupIniIP ini = new SetupIniIP();
           return ini.IniReadValue(v1, v2, path);
        }
        public class SetupIniIP
        {
            public string path;
            [DllImport("kernel32", CharSet = CharSet.Unicode)]
            private static extern long WritePrivateProfileString(string section,
            string key, string val, string filePath);
            [DllImport("kernel32", CharSet = CharSet.Unicode)]
            private static extern int GetPrivateProfileString(string section,
            string key, string def, StringBuilder retVal,
            int size, string filePath);
           
            public string IniReadValue(string Section, string Key, string inipath)
            {
                StringBuilder temp = new StringBuilder(255);
                int i = GetPrivateProfileString(Section, Key, "", temp, 255, inipath);
                return temp.ToString();
            }
        }
        #region ==共用==
        /// <summary>
        /// Controller取的特定的View
        /// </summary>
        /// <param name="ViewName">View名稱</param>
        /// <returns></returns>
        public static string GetMUView(string ViewName)
        {
            return "~/Views/MU/" + ViewName + ".cshtml";
        }

        /// <summary>
        /// 取得功能名稱
        /// </summary>
        /// <param name="Action"></param>
        /// <returns></returns>
        public static string GetFunctionTitle(string Action)
        {
            string FunctionName = "";
            switch (Action)
            {
                case "MU":
                    FunctionName = "資產主檔更新";
                    break;
            }
            return FunctionName;
        }
        #endregion

        #region ==Model to ViewModel==
        public static SearchViewModel ToSearchViewModel(SearchModel model)
        {
            return new SearchViewModel()
            {
                Transaction_Type = model.Transaction_Type
            };
        }

        public static MUHeaderViewModel ToMUHeaderViewModel(MUHeaderModel model)
        {
            return new MUHeaderViewModel()
            {
                 Accounting_Datetime = model.Accounting_Datetime,
                 Book_Type = model.Book_Type,
                 Created_By = model.Created_By,
                 Create_Time = model.Create_Time,
                 Description = model.Description,
                 ID = model.ID,
                 Last_Updated_By = model.Last_Updated_By,
                 Last_Updated_Time = model.Last_Updated_Time,
                 Office_Branch = model.Office_Branch,
                 Transaction_Datetime_Entered = model.Transaction_Datetime_Entered,
                 Transaction_Status = model.Transaction_Status,
                 Transaction_Type = model.Transaction_Type,
                 TRX_Header_ID = model.TRX_Header_ID
            };

        }

        public static AssetViewModel ToAssetViewModel(AssetModel model)
        {
            return new AssetViewModel()
            {
                ID = model.ID,
                Asset_Number = model.Asset_Number,
                Current_units = model.Current_units,
                Datetime_Placed_In_Service = model.Datetime_Placed_In_Service,
                Old_Asset_Number = model.Old_Asset_Number,
                Asset_Category_code = model.Asset_Category_code,
                Location_Disp = model.Location_Disp,
                Description = model.Description,
                Urban_Renewal = model.Urban_Renewal,
                City_Code = model.City_Code,
                District_Code = model.District_Code,
                Section_Code = model.Section_Code,
                Sub_Section_Name = model.Sub_Section_Name,
                Parent_Land_Number = model.Parent_Land_Number,
                Filial_Land_Number = model.Filial_Land_Number,
                Authorization_Number = model.Authorization_Number,
                Authorized_name = model.Authorized_name,
                Area_Size = model.Area_Size,
                Authorized_Scope_Molecule = model.Authorized_Scope_Molecule,
                Authorized_Scope_Denomminx = model.Authorized_Scope_Denomminx,
                Authorized_Area = model.Authorized_Area,
                Used_Type = model.Used_Type,
                Used_Status = model.Used_Status,
                Obtained_Method = model.Obtained_Method,
                Used_Partition = model.Used_Partition,
                Is_Marginal_Land = model.Is_Marginal_Land,
                Own_Area = model.Own_Area,
                NONOwn_Area = model.NONOwn_Area,
                Original_Cost = model.Original_Cost,
                Deprn_Amount = model.Deprn_Amount,
                Current_Cost = model.Current_Cost,
                Reval_Adjustment_Amount = model.Reval_Adjustment_Amount,
                Reval_Land_VAT = model.Reval_Land_VAT,
                Reval_Reserve = model.Reval_Reserve,
                Business_Area_Size = model.Business_Area_Size,
                Business_Book_Amount = model.Business_Book_Amount,
                Business_Reval_Reserve = model.Business_Reval_Reserve,
                NONBusiness_Area_Size = model.NONBusiness_Area_Size,
                NONBusiness_Book_Amount = model.NONBusiness_Book_Amount,
                NONBusiness_Reval_Reserve = model.NONBusiness_Reval_Reserve,
                Year_Number = model.Year_Number,
                Announce_Amount = model.Announce_Amount,
                Announce_Price = model.Announce_Price,
                Tax_Type = model.Tax_Type,
                Reduction_reason = model.Reduction_reason,
                Transfer_Price = model.Transfer_Price,
                Reduction_Area = model.Reduction_Area,
                Declared_Price = model.Declared_Price,
                Dutiable_Amount = model.Dutiable_Amount,
                Remark1 = model.Remark1,
                Remark2 = model.Remark2,
                Retire_Cost = model.Retire_Cost,
                Sell_Amount = model.Sell_Amount,
                Sell_Cost = model.Sell_Cost,
                Delete_Reason = model.Delete_Reason,
                Land_Item = model.Land_Item,
                CROP = model.CROP,
                ReNewPart = model.ReNewPart,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By,
                Ori_Announce_Amount = model.Ori_Announce_Amount,
                Ori_Area_Size = model.Ori_Area_Size
            };
        }
        #endregion

        #region ==ViewModel to Model==
        public static MUHeaderModel ToMUHeaderModel(MUHeaderViewModel viewmodel)
        {
            return new MUHeaderModel()
            {
                Accounting_Datetime = viewmodel.Accounting_Datetime,
                Book_Type = viewmodel.Book_Type,
                Created_By = viewmodel.Created_By,
                Create_Time = viewmodel.Create_Time,
                Description = viewmodel.Description,
                ID = viewmodel.ID,
                Last_Updated_By = viewmodel.Last_Updated_By,
                Last_Updated_Time = viewmodel.Last_Updated_Time,
                Office_Branch = viewmodel.Office_Branch,
                Transaction_Datetime_Entered = viewmodel.Transaction_Datetime_Entered,
                Transaction_Status = viewmodel.Transaction_Status,
                Transaction_Type = viewmodel.Transaction_Type,
                TRX_Header_ID = viewmodel.TRX_Header_ID
            };
        }

        public static AssetModel ToAssetModel(AssetViewModel viewmodel)
        {
            return new AssetModel()
            {
                Asset_Number = viewmodel.Asset_Number,
                Current_units = viewmodel.Current_units,
                Datetime_Placed_In_Service = viewmodel.Datetime_Placed_In_Service,
                Old_Asset_Number = viewmodel.Old_Asset_Number,
                Asset_Category_code = viewmodel.Asset_Category_code,
                Location_Disp = viewmodel.Location_Disp,
                Description = viewmodel.Description,
                Urban_Renewal = viewmodel.Urban_Renewal,
                City_Code = viewmodel.City_Code,
                District_Code = viewmodel.District_Code,
                Section_Code = viewmodel.Section_Code,
                Sub_Section_Name = viewmodel.Sub_Section_Name,
                Parent_Land_Number = viewmodel.Parent_Land_Number,
                Filial_Land_Number = viewmodel.Filial_Land_Number,
                Authorization_Number = viewmodel.Authorization_Number,
                Authorized_name = viewmodel.Authorized_name,
                Area_Size = viewmodel.Area_Size,
                Authorized_Scope_Molecule = viewmodel.Authorized_Scope_Molecule,
                Authorized_Scope_Denomminx = viewmodel.Authorized_Scope_Denomminx,
                Authorized_Area = viewmodel.Authorized_Area,
                Used_Type = viewmodel.Used_Type,
                Used_Status = viewmodel.Used_Status,
                Obtained_Method = viewmodel.Obtained_Method,
                Used_Partition = viewmodel.Used_Partition,
                Is_Marginal_Land = viewmodel.Is_Marginal_Land,
                Own_Area = viewmodel.Own_Area,
                NONOwn_Area = viewmodel.NONOwn_Area,
                Original_Cost = viewmodel.Original_Cost,
                Deprn_Amount = viewmodel.Deprn_Amount,
                Current_Cost = viewmodel.Current_Cost,
                Reval_Adjustment_Amount = viewmodel.Reval_Adjustment_Amount,
                Reval_Land_VAT = viewmodel.Reval_Land_VAT,
                Reval_Reserve = viewmodel.Reval_Reserve,
                Business_Area_Size = viewmodel.Business_Area_Size,
                Business_Book_Amount = viewmodel.Business_Book_Amount,
                Business_Reval_Reserve = viewmodel.Business_Reval_Reserve,
                NONBusiness_Area_Size = viewmodel.NONBusiness_Area_Size,
                NONBusiness_Book_Amount = viewmodel.NONBusiness_Book_Amount,
                NONBusiness_Reval_Reserve = viewmodel.NONBusiness_Reval_Reserve,
                Year_Number = viewmodel.Year_Number,
                Announce_Amount = viewmodel.Announce_Amount,
                Announce_Price = viewmodel.Announce_Price,
                Tax_Type = viewmodel.Tax_Type,
                Reduction_reason = viewmodel.Reduction_reason,
                Transfer_Price = viewmodel.Transfer_Price,
                Reduction_Area = viewmodel.Reduction_Area,
                Declared_Price = viewmodel.Declared_Price,
                Dutiable_Amount = viewmodel.Dutiable_Amount,
                Remark1 = viewmodel.Remark1,
                Remark2 = viewmodel.Remark2,
                Retire_Cost = viewmodel.Retire_Cost,
                Sell_Amount = viewmodel.Sell_Amount,
                Sell_Cost = viewmodel.Sell_Cost,
                Delete_Reason = viewmodel.Delete_Reason,
                Land_Item = viewmodel.Land_Item,
                CROP = viewmodel.CROP,
                ReNewPart = !string.IsNullOrEmpty(viewmodel.ReNewPart)? viewmodel.ReNewPart:"",
                Create_Time = viewmodel.Create_Time,
                Created_By = viewmodel.Created_By,
                Last_Updated_Time = viewmodel.Last_Updated_Time,
                Last_Updated_By = viewmodel.Last_Updated_By
            };
        }
        #endregion
    }
}