﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.MU
{
    public class MUViewModel
    {
        public MUHeaderViewModel Header { get; set; }

        public IEnumerable<AssetViewModel> Assets { get; set; }
    }
}