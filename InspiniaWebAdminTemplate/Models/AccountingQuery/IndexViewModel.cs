﻿using Library.Common.Models;
using Library.Servant.Servant.Accounting;
using Library.Servant.Servant.Accounting.Models;
using Library.Servant.Servant.AccountingQueryDeprn.Model;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.AccountingQuery
{
    public class IndexViewModel
    {
        public List<Option> TransTypes { get; set; }
        public List<Option> TradeTypes { get; set; }
        public PageList<IndexItem> Page { get; set; }
        public _AccountingQueryModel Search { get; set; }
    }
}