﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InspiniaWebAdminTemplate.Models.Common;

namespace InspiniaWebAdminTemplate.Models.Activation
{
    public class SearchViewModel
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }
        /*
        /// <summary>單據類型</summary>
        public string Transaction_Type { get; set; }

        /// <summary>來源類型</summary>
        public string Source { get; set; }
        /// <summary>單據編號-起</summary>
        public string TRXHeaderIDBgn { get; set; }

        /// <summary>單據編號-迄</summary>
        public string TRXHeaderIDEnd { get; set; }

        /// <summary>帳本</summary>
        public string BookTypeCode { get; set; }

        /// <summary>單據處理日期-起</summary>
        public DateTime? BeginDate { get; set; }

        /// <summary>單據處理日期-迄</summary>
        public DateTime? EndDate { get; set; }

        /// <summary>保管單位</summary>
        public string AssignedBranch { get; set; }

        /// <summary>管轄單位</summary>
        public string OfficeBranch { get; set; }

        /// <summary>移出行(原管轄單位)/// </summary>
        public string OriOfficeBranch { get; set; }

        /// <summary>狀態</summary>
        public string FlowStatus { get; set; }

        /// <summary>備註(關鍵字)</summary>
        public string Remark { get; set; }

        //<summary>處分原因</summary>
        public string DisposeReason { get; set; }*/
    }
}