﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Activation
{
    public class ActivationViewModel
    {
        public long ID { get; set; }
        public Nullable<long> ParantID { get; set; }
        public Nullable<long> Document_Type { get; set; }
        public Nullable<int> Source_Type { get; set; }
        public string Activation_No { get; set; }
        public string BOOK_TYPE_CODE { get; set; }
        public Nullable<System.DateTime> BeginDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Jurisdiction { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<long> Activation_Target { get; set; }
        public string TEL { get; set; }
        public string Email { get; set; }
        public string Remark { get; set; }
        public Nullable<System.DateTime> Create_Time { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public Nullable<int> Last_Updated_By { get; set; }
    }

}