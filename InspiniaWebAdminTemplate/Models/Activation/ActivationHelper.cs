﻿using Library.Servant.Servant.Activation.ActivationModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Activation
{
    public static class ActivationHelper
    {
        #region ==ViewModel To Model==
 
        public static ActivationModel ToAssetModel(ActivationViewModel viewModel)
        {
            return new ActivationModel()
            {
                ID = viewModel.ID,
                
            };
        }

        #endregion

        #region== Model To ViewModel ==
        public static SearchViewModel ToSearchViewModel(SearchModel model)
        {
            return new SearchViewModel()
            {
                Page = model.Page
                
            };
        }
        public static ActivationViewModel ToActivationViewModel(ActivationModel model)
        {
            return new ActivationViewModel()
            {
                ID = model.ID,
                ParantID = model.ParantID,
                Document_Type = model.Document_Type,
                Source_Type = model.Source_Type,
                Activation_No = model.Activation_No,
                BOOK_TYPE_CODE = model.BOOK_TYPE_CODE,
                BeginDate = model.BeginDate,
                EndDate = model.EndDate,
                Jurisdiction = model.Jurisdiction,
                Type = model.Type,
                Activation_Target = model.Activation_Target,
                TEL = model.TEL,
                Email = model.Email,
                Remark = model.Remark,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }
        public static ActivationViewModel ToAssetViewModel(ActivationModel model)
        {
            ActivationViewModel Result = null;
            if(model != null)
            {
                Result = new ActivationViewModel()
                {
                    ID = model.ID,
                    ParantID = model.ParantID,
                    Document_Type = model.Document_Type,
                    Source_Type = model.Source_Type,
                    Activation_No = model.Activation_No,
                    BOOK_TYPE_CODE = model.BOOK_TYPE_CODE,
                    BeginDate = model.BeginDate,
                    EndDate = model.EndDate,
                    Jurisdiction = model.Jurisdiction,
                    Type = model.Type,
                    Activation_Target = model.Activation_Target,
                    TEL = model.TEL,
                    Email = model.Email,
                    Remark = model.Remark,
                    Create_Time = model.Create_Time,
                    Created_By = model.Created_By,
                    Last_Updated_Time = model.Last_Updated_Time,
                    Last_Updated_By = model.Last_Updated_By

                };
            }
            return Result;
        }
        
        #endregion

        #region== 共用 ==
        /// <summary>
        /// Controller取的特定的View
        /// </summary>
        /// <param name="ViewName">View名稱</param>
        /// <returns></returns>
        public static string GetActivationView(string ViewName)
        {
            return "~/Views/AssetsActivation/" + ViewName + ".cshtml";
        }

        /// <summary>
        /// 取得功能名稱
        /// </summary>
        /// <param name="Action"></param>
        /// <returns></returns>
        public static string GetFunctionTitle(string Action)
        {
            string FunctionName = "";
            switch (Action)
            {
                case "MPArchive":
                    FunctionName = "資產查詢";
                    break;
                case "MPAdded":
                    FunctionName = "動產新增";
                    break;
                case "MPMaintain":
                    FunctionName = "動產維護";
                    break;
                case "MPTransfer":
                    FunctionName = "動產移轉";
                    break;
                case "MPDisposal":
                    FunctionName = "動產處分";
                    break;
                case "MPScrappedNotice":
                    FunctionName = "動產例行性報廢通知";
                    break;
                case "MPScrappedReciprocation":
                    FunctionName = "動產例行性報廢回報";
                    break;
                case "MPDeprn":
                    FunctionName = "動產折舊";
                    break;
                case "AssetsActivation":
                    FunctionName = "分行活化規劃";
                    break;
            }
            return FunctionName;
        }

        public static string GetWebRoot()
        {
            return ((HttpContext.Current.Request.ApplicationPath == "/") ? "" : HttpContext.Current.Request.ApplicationPath);
        }

        public static DataTable LinqQueryToDataTable<T>(IEnumerable<T> query)
        {
            DataTable tbl = new DataTable();
            PropertyInfo[] props = null;
            foreach (T item in query)
            {
                if (props == null) //尚未初始化
                {
                    Type t = item.GetType();
                    props = t.GetProperties();
                    foreach (PropertyInfo pi in props)
                    {
                        Type colType = pi.PropertyType;
                        //針對Nullable<>特別處理
                        if (colType.IsGenericType
                            && colType.GetGenericTypeDefinition() == typeof(Nullable<>))
                            colType = colType.GetGenericArguments()[0];
                        //建立欄位
                        tbl.Columns.Add(pi.Name, colType);
                    }
                }
                DataRow row = tbl.NewRow();
                foreach (PropertyInfo pi in props)
                    row[pi.Name] = pi.GetValue(item, null) ?? DBNull.Value;
                tbl.Rows.Add(row);
            }
            return tbl;
        }
        #endregion
    }
}