﻿using System;
using Library.Servant.Enums;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.QueryUserRole.Models;

namespace InspiniaWebAdminTemplate.Models.QueryUserRole
{
    public class ItemViewModel
    {
        public string FlowCode { get; set; }
        public DateTime FlowDate { get; set; }
        public string Type { get; set; }
        public int Count { get; set; }
        public string AppliedUserCode { get; set; }
        public string AppliedUser { get; set; }
        public string Status { get; set; }

        public static explicit operator ItemViewModel(ListItemResult from)
        {
            return new ItemViewModel {
                FlowCode = from.FlowCode,
                FlowDate = from.FlowDate,
                Type = from.Type,
                Count = from.Count,
                AppliedUserCode = from.AppliedUser,
                AppliedUser = from.AppliedUserName,
                Status = from.StatusName
            };
        }
    }
}