﻿using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.QueryUserRole.Models;

namespace InspiniaWebAdminTemplate.Models.QueryUserRole
{
    public class SearchViewModel : AbstractPageModel<HeaderViewModel>
    {
        public string FlowCode { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }

        public static explicit operator SearchModel(SearchViewModel vm)
        {
            //return MapProperty.Mapping<SearchModel, SearchViewModel>(vm);
            return new SearchModel
            {
                FlowCode = vm.FlowCode,
                Status = vm.Status,
                Type = vm.Type,
                Branch = vm.Branch,
                Department = vm.Department,
                Page = vm.Page,
                PageSize = vm.PageSize
            };
        }
    }
}