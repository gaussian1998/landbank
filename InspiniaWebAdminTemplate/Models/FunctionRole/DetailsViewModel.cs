﻿using Library.Servant.Servant.FunctionRole.Models;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.FunctionRole
{
    public class DetailsViewModel
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> LoginRoleNames { get; set; }

        public static explicit operator DetailsViewModel(DetailsResult from)
        {
            //return MapProperty.Mapping<DetailsViewModel, DetailsResult>(from);
            return new DetailsViewModel
            {
                ID = from.ID,
                Code = from.Code,
                Name = from.Name,
                Description = from.Description,
                LoginRoleNames = from.LoginRoleNames
            };
        }
    }
}