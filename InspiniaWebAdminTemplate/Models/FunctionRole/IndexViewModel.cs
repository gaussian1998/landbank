﻿using System.Collections.Generic;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.FunctionRole
{
    public class IndexViewModel
    {
        public List<DetailsViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public List<OptionModel<string, string>> TotalFunctions { get; set; }
        public SearchViewModel condition { get; set; }
    }
}