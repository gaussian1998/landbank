﻿using System.Collections.Generic;
using Library.Servant.Servant.FunctionRole.Models;

namespace InspiniaWebAdminTemplate.Models.FunctionRole
{
    public class CreateViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<int> SelectedRolesID { get; set; }

        public static explicit operator CreateModel(CreateViewModel from)
        {
            return new CreateModel
            {
                Name = from.Name,
                Description = from.Description,
                SelectedRolesID = from.SelectedRolesID == null ? new List<int>() : from.SelectedRolesID
            };
        }
    }
}