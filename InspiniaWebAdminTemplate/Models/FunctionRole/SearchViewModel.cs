﻿using Library.Servant.Servant.Common.Models;

using Library.Servant.Servant.FunctionRole.Models;

namespace InspiniaWebAdminTemplate.Models.FunctionRole
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public string FunctionID { get; set; }
        public string DocType { get; set; }
        public static explicit operator SearchModel(SearchViewModel vm)
        {
            //return MapProperty.Mapping<SearchModel, SearchViewModel>(vm);
            return new SearchModel
            {
                Page = vm.Page,
                PageSize = vm.PageSize,
                FunctionID = vm.FunctionID,
                DocType = vm.DocType
            };
        }
    }
}