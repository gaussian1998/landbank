﻿using InspiniaWebAdminTemplate.Models.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InspiniaWebAdminTemplate.Models.FunctionRole
{
    public class NewViewModel
    {
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public List< OptionViewModel > CadidateRoles { get; set; }
    }
}