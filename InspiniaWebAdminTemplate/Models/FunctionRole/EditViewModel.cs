﻿using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.FunctionRole.Models;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.FunctionRole
{
    public class EditViewModel
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List< OptionModel<int,string> > CadidateLoginRoles { get; set; }
        public List< OptionModel<int, string>> SelectedLoginRoles { get; set; }

        public static explicit operator EditViewModel(EditResult from)
        {
            return new EditViewModel
            {
                ID = from.ID,
                Code = from.Code,
                Name = from.Name,
                Description = from.Description,
                SelectedLoginRoles = from.SelectedLoginRoles,
                CadidateLoginRoles = from.CadidateLoginRoles
            };
        }
    }
}