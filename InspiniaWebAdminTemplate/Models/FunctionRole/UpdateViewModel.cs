﻿using Library.Servant.Servant.FunctionRole.Models;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.FunctionRole
{
    public class UpdateViewModel
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public List<int> SelectedLoginRolesID { get; set; }

        public static explicit operator UpdateModel(UpdateViewModel from)
        {
            return new UpdateModel
            {
                ID = from.ID,
                Description = from.Description,
                SelectedLoginRolesID = from.SelectedLoginRolesID == null ? new List<int>() : from.SelectedLoginRolesID
            };
        }
    }
}