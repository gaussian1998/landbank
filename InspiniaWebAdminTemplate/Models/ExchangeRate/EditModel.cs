﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.ExchangeRate.Model;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.ExchangeRate
{
    public class EditModel
    {
        public List<SelectedModel> Branches { get; set; }
        public List<SelectedModel> AccountingBooks { get; set; }

        public List<SelectedModel> Currencys { get; set; }

        public DetailModel Data { get; set; }
    }
}