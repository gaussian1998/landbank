﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.ExchangeRate.Model;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.ExchangeRate
{
    public class IndexModel
    {
        public List<SelectedModel> Branches { get; set; }
        public List<SelectedModel> AccountingBooks { get; set; }
        public int BranchCodeId { get; set; }
        public string AccountingBookCodeId { get; set; }

        public IndexResult QueryResult { get; set; }
    }
}