﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.DocName
{
    public class DetailsViewModel
    {
        public DetailsViewModel()
        {
            Editable = false;
        }
        public int ID { get; set; }
        public string DocType { get; set; }
        public string DocCode { get; set; }
        public string DocName { get; set; }
        public bool IsActive { get; set; }
        public bool CancelCode { get; set; }
        public string Remark { get; set; }
        public bool Editable { get; set; }
    }
}