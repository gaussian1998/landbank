﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.DocName.Models;

namespace InspiniaWebAdminTemplate.Models.DocName
{
    public class CreateViewModel
    {
        [Required]
        public string DocCode { get; set; }
        [Required]
        public string DocType { get; set; }
        [Required]
        public string DocName { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public bool CancelCode { get; set; }
        public List<IDSelectedModel> DocTypeOptions { get; set; }
        public string Remark { get; set; }

        public static explicit operator CreateViewModel(NewResult defaultValue)
        {
            //return MapProperty.Mapping<CreateViewModel, DefaultValue>(defaultValue);
            return new CreateViewModel
            {
                IsActive = defaultValue.IsActive,
                CancelCode = defaultValue.CancelCode,
                DocTypeOptions = defaultValue.DocTypeOptions,
            };
        }

        public static explicit operator CreateModel(CreateViewModel vm)
        {
            //快速但會降低維護性的作法,快速建構系統用
            //return MapProperty.Mapping<CreateModel,CreateViewModel>(vm);

            //慢速但會提升維護性的作法,維護期使用
            return new CreateModel
            {
                DocCode = vm.DocCode,
                DocType = vm.DocType,
                DocName = vm.DocName,
                IsActive = vm.IsActive,
                CancelCode = vm.CancelCode,
                Remark = vm.Remark
            };
        }
    }
}