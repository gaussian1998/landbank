﻿using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.DocName.Models;

namespace InspiniaWebAdminTemplate.Models.DocName
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public string DocCode { get; set; }
        public string DocType { get; set; }
        public bool? IsActive { get; set; }
        public bool? CancelCode { get; set; }


        public static explicit operator SearchModel(SearchViewModel vm)
        {
            return new SearchModel
            {
                DocCode = vm.DocCode,
                DocType = vm.DocType,
                IsActive = vm.IsActive,
                CancelCode = vm.CancelCode,
                Page = vm.Page,
                PageSize = vm.PageSize
            };
        }
    }
}