﻿using Library.Servant.Servant.DocName.Models;
using Library.Utility;
using System.ComponentModel.DataAnnotations;

namespace InspiniaWebAdminTemplate.Models.DocName
{
    public class EditViewModel
    {
        public int ID { get; set; }
        public string Class { get; set; }
        [Required]
        public string ClassName { get; set; }
        [Required]
        public string CodeID { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public bool CancelCode { get; set; }
        public string Remark { get; set; }

        public static explicit operator EditViewModel(DetailsResult from)
        {
            //快速但會降低維護性的作法,快速建構系統用
            //return MapProperty.Mapping<EditViewModel, DetailsResult>(from);

            //慢速但會提升維護性的作法,維護期使用
            return new EditViewModel
            {
                ID = from.ID,
                IsActive = from.IsActive,
                CancelCode = from.CancelCode,
                Remark = from.Remark
            };
        }
    }
}