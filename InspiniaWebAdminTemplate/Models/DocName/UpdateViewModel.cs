﻿
using Library.Servant.Servant.DocName.Models;

namespace InspiniaWebAdminTemplate.Models.DocName
{
    public class UpdateViewModel
    {
        public int ID { get; set; }

        public static explicit operator UpdateModel(UpdateViewModel vm)
        {
            //快速但會降低維護性的作法,快速建構系統用
            //return MapProperty.Mapping<UpdateModel,UpdateViewModel>(vm);

            //慢速但會提升維護性的作法,維護期使用
            return new UpdateModel
            {
                ID = vm.ID
            };
        }
    }
}