﻿using System.Collections.Generic;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.DocName
{
    public class IndexViewModel
    {
        public List<IDSelectedModel> DocTypeOptions;


        public List<DetailsViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public SearchViewModel SearchCondition { get; set; }
        public List<SelectedModel> DocCodeOptions { get; set; }
    }
}