﻿using System.Collections.Generic;
using Library.Servant.Servant.Common.Models;
using Library.Common.Models;
using Library.Servant.Servant.AgentFlow.Models;

namespace InspiniaWebAdminTemplate.Models.AgentFlow
{
    public class IndexViewModel
    {
        public PageList<DetailsResult> Page { get; set; }
        public SearchViewModel SearchCondition { get; set; }
        public List<OptionModel<string, string>> DocTypeOptions { get; internal set; }
        public List<SelectedModel> DocCodeOptions { get; internal set; }
    }
}