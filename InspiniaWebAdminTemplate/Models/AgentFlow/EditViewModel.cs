﻿using Library.Servant.Servant.AgentFlow.Models;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.AgentFlow
{
    public class EditViewModel
    {
        public int ID { get; set; }
        public string DocType { get; set; }
        public string DocTypeName { get; set; }
        public string DocName { get; set; }
        public int NewestVersion { get; set; }
        public string FlowTemplateName { get; set; }
        public List<string> FlowTemplateDescription { get; set; }
        public List<OptionModel<int, string>> FlowNameOptions { get; set; }
        public Dictionary<string, OptionModel<int, string>> FlowNameGroupFlowTemplateOptions { get; set; }
        
        public static explicit operator EditViewModel(EditResult from)
        {
            return new EditViewModel
            {
                ID = from.ID,
                DocType = from.DocType,
                DocTypeName= from.DocTypeName,
                DocName = from.DocName,
                NewestVersion = from.NewestVersion,
                FlowTemplateName = from.FlowTemplateName,
                FlowTemplateDescription = from.FlowTemplateDescription,
                FlowNameOptions = from.FlowNameOptions,
                FlowNameGroupFlowTemplateOptions = from.FlowNameGroupFlowTemplateOptions,
            };
        }
    }
}