﻿using Library.Servant.Servant.AgentFlow.Models;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.AgentFlow
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public SearchViewModel()
        {
            this.IsSet = true;
            this.IsLatest = true;
        }

        public string DocCode { get; set; }
        public string DocType { get; set; }
        public bool IsSet { get; set; }
        public bool IsLatest { get; set; }

        public static explicit operator SearchModel(SearchViewModel vm)
        {
            return new SearchModel
            {
                DocCode = vm.DocCode,
                DocType = vm.DocType,
                IsSet = vm.IsSet,
                IsLatest = vm.IsLatest,
                Page = vm.Page,
                PageSize = vm.PageSize
            };
        }
    }
}
