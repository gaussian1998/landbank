﻿using System.Collections.Generic;
using Library.Servant.Servant.AgentFlow.Models;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.AgentFlow
{
    public class NewViewModel
    {
        public List<OptionModel<string, string>> DocTypeOptions { get; set; }
        public Dictionary<string, List<OptionModel<int, string>>> DocTypeGroupDocNameOptions { get; set; }

        public List<OptionModel<int, string>> FlowNameOptions { get; set; }
        public Dictionary<string, OptionModel<int, string>> FlowNameGroupFlowTemplateOptions { get; set; }


        public static explicit operator NewViewModel(NewResult from)
        {
            return new NewViewModel
            {
                DocTypeOptions = from.DocTypeOptions,
                DocTypeGroupDocNameOptions = from.DocTypeGroupDocNameOptions,

                FlowNameOptions = from.FlowNameOptions,
                FlowNameGroupFlowTemplateOptions = from.FlowNameGroupFlowTemplateOptions,
            };
        }
    }
}