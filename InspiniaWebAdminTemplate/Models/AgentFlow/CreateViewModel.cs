﻿using System;
namespace InspiniaWebAdminTemplate.Models.AgentFlow
{
    public class CreateViewModel
    {
        public int? DocID { get; set; }
        public int? FlowTemplateID { get; set; }
    }
}