﻿using Library.Servant.Servant.AgentFlow.Models;

namespace InspiniaWebAdminTemplate.Models.AgentFlow
{
    public class UpdateViewModel
    {
        public int ID { get; set; }
        public int? FlowTemplateID { get; set; }
        public static explicit operator UpdateModel(UpdateViewModel vm)
        {
            return new UpdateModel
            {
                ID = vm.ID,
                FlowTemplateID = vm.FlowTemplateID.Value
            };
        }
    }
}