﻿using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.AccountingDeprnVerify
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public List<string> TradeKinds { get; set; }
        public HashSet<string> VerifyAccount { get; set; }
        public SearchViewModel()
        {
            TradeKinds = new List<string> { };
            VerifyAccount = new HashSet<string> { };
        }
    }
}