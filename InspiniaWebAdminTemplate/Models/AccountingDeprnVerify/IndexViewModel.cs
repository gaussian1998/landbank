﻿using Library.Common.Models;
using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.AccountingDeprnVerify
{
	public class IndexViewModel
	{
        public TradeOptions TradeOptions { get; set; }
        public SearchViewModel Condition { get; set; }
        public PageList<AccountEntryDetailResult> Page { get; set; }
    }
}