﻿using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.LoginRole.Models;

namespace InspiniaWebAdminTemplate.Models.LoginRole
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public string RoleID { get; set; }
        public string DocType { get; set; }

        public static explicit operator SearchModel(SearchViewModel vm)
        {
            //return MapProperty.Mapping<SearchModel, SearchViewModel>(vm);
            return new SearchModel
            {
                Page = vm.Page,
                PageSize = vm.PageSize,
                RoleID = vm.RoleID,
                DocType = vm.DocType
            };
        }
    }
}