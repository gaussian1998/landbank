﻿using Library.Servant.Servant.LoginRole.Models;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;
using Library.Utility;
using System.ComponentModel.DataAnnotations;
using InspiniaWebAdminTemplate.Models.Common;
using System.Linq;
using System;

namespace InspiniaWebAdminTemplate.Models.LoginRole
{
    public class DetailViewModel
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public DateTime ExpireDate { get; set; }
        public string DocType { get; set; }
        public string OperatingType { get; set; }
        public string BranchType { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public List<OptionViewModel> CadidateFunctions { get; set; }
        public List<OptionViewModel> SelectedFunctions { get; set; }

        public static explicit operator DetailViewModel(EditResult from)
        {
            //return MapProperty.Mapping<EditViewModel, EditResult>(from);
            return new DetailViewModel
            {
                ID = from.ID,
                Code = from.Code,
                ExpireDate = from.ExpireDate,
                DocType = from.DocType,
                OperatingType = from.OperatingType,
                BranchType = from.BranchType,
                Name = from.Name,
                Description = from.Description,
                CadidateFunctions = from.CadidateFunctions.Select(m => new OptionViewModel { Text=m.Text, Value=m.Value }).ToList(),
                SelectedFunctions = from.SelectedFunctions.Select(m => new OptionViewModel { Text = m.Text, Value = m.Value }).ToList()
            };
        }

        public static explicit operator DetailViewModel(NewResult from)
        {
            //return MapProperty.Mapping<EditViewModel, EditResult>(from);
            return new DetailViewModel
            {
                ExpireDate = new DateTime(),
                CadidateFunctions = from.CadidateFunctions.Select(m => new OptionViewModel { Value = m.Value, Text = m.Text }).ToList(),
                SelectedFunctions = new List<OptionViewModel>()
            };
        }
    }
}