﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Library.Servant.Servant.LoginRole.Models;

namespace InspiniaWebAdminTemplate.Models.LoginRole
{
    public class CreateViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<int> SelectedFunctionsID { get; set; }

        public static explicit operator CreateModel(CreateViewModel from)
        {
            return new CreateModel
            {
                Code = from.Code,
                Name = from.Name,
                Description = from.Description,
                SelectedProgramID = from.SelectedFunctionsID == null ? new List<int>() : from.SelectedFunctionsID
            };
        }
    }
}