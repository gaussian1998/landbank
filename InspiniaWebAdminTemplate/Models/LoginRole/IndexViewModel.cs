﻿using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;


namespace InspiniaWebAdminTemplate.Models.LoginRole
{
    public class IndexViewModel
    {
        public List<DetailsViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public List<OptionModel<string, string>> TotalRoles { get; set; }
        public SearchViewModel condition { get; set; }
    }
}