﻿using System;
using System.Collections.Generic;
using Library.Servant.Servant.LoginRole.Models;

namespace InspiniaWebAdminTemplate.Models.LoginRole
{
    public class UpdateViewModel
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public DateTime ExpireDate { get; set; }
        public string DocType { get; set; }
        public string OperatingType { get; set; }
        public string BranchType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<int> SelectedFunctionsID { get; set; }

        public static explicit operator UpdateModel(UpdateViewModel from)
        {
            return new UpdateModel
            {
                ID = from.ID,
                Code = from.Code,
                ExpireDate = from.ExpireDate,
                DocType = from.DocType,
                OperatingType = from.OperatingType,
                BranchType = from.BranchType,
                Name = from.Name,
                Description = from.Description,
                SelectedFunctionsID = from.SelectedFunctionsID == null ? new List<int>() : from.SelectedFunctionsID
            };
        }
    }
}