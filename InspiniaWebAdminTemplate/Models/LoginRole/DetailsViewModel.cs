﻿using Library.Servant.Servant.LoginRole.Models;
using System.Collections.Generic;

namespace InspiniaWebAdminTemplate.Models.LoginRole
{
    public class DetailsViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> FunctionNames { get; set; }

        public static explicit operator DetailsViewModel(DetailsResult from)
        {
            //return MapProperty.Mapping<DetailsViewModel, DetailsResult>(from);
            return new DetailsViewModel
            {
                ID = from.ID,
                Name = from.Name,
                Description = from.Description,
                FunctionNames = from.FunctionNames
            };
        }
    }
}