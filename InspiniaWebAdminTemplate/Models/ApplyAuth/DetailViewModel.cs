﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.UserRole.Models;

namespace InspiniaWebAdminTemplate.Models.ApplyAuth
{
    public class DetailViewModel
    {
        public DetailViewModel()
        {
        }
        
        [Required]
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string BranchName { get; set; }
        public string Department { get; set; }
        public List<RoleDetailViewModel> TotalRoles { get; set; }
        public string FlowCode { get; set; }

        public static explicit operator DetailViewModel(DetailModel from)
        {
            return new DetailViewModel
            {
                UserCode = from.UserCode,
                UserName = from.UserName,
                BranchName = from.BranchName,
                Department = from.Department,
                TotalRoles = from.TotalRoles.Select(m => (RoleDetailViewModel)m).ToList()
            };
        }
    }
}