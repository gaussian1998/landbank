﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.UserRole.Models;

namespace InspiniaWebAdminTemplate.Models.ApplyAuth
{
    public class RoleDetailViewModel
    {
        public bool IsChecked { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public List<string> Programs { get; set; }

        public static explicit operator RoleDetailViewModel(RoleDetailModel from)
        {
            return new RoleDetailViewModel
            {
                IsChecked = from.IsChecked,
                ID = from.ID,
                Name = from.Name,
                Programs = from.Programs
            };
        }
    }
}