﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace InspiniaWebAdminTemplate.Models.DossierRight
{
    public class RecordIndexViewModel
    {

        public IEnumerable<DossierRightMainViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public RecordSearchViewModel condition { get; set; }
    }
}