﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.DossierRight
{
    public class DossierRightRecordViewModel
    {
        public int ID { get; set; }
        public string AS_Assets_Right_ID { get; set; }
        public string Authorization_Category { get; set; }
        public string Authorization_Category_Name { get; set; }
        public string AS_Assets_Right_Header_ID { get; set; }
        public string Asset_Number { get; set; }
        public string City_Code { get; set; }
        public string City_Name { get; set; }
        public string District_Code { get; set; }
        public string District_Name { get; set; }
        public string Section_Code { get; set; }
        public string Section_Name { get; set; }
        public string Sub_Section_Name { get; set; }
        public string Parent_Land_Number { get; set; }
        public string Filial_Land_Number { get; set; }
        public string Build_Number { get; set; }
        public string Build_Address { get; set; }
        public string Building_STRU { get; set; }
        public string Used_Type { get; set; }
        public decimal? Building_Total_Floor { get; set; }
        public System.DateTime? Registration_Date { get; set; }
        public System.DateTime? RightBook_Date { get; set; }
        public string Authorization_Number { get; set; }
        public string Right_UploadFile { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> Create_Time { get; set; }
        public Nullable<System.Decimal> Created_By { get; set; }
        public string Created_By_Name { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public Nullable<System.Decimal> Last_Updated_By { get; set; }
        public string Last_Updated_By_Name { get; set; }
    }
}