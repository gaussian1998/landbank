﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.DossierRight
{
    public class HeaderViewModel
    {
        public int ID { get; set; }
        public string Form_Number { get; set; }
        public string Source { get; set; }
        public string SourceName { get; set; }
        public string Transaction_Type { get; set; }
        public string Transaction_TypeName { get; set; }
        public string Transaction_Status { get; set; }
        public string Transaction_StatusName { get; set; }
        public System.DateTime Transaction_Datetime_Entered { get; set; }
        public string Office_Branch_Code { get; set; }
        public string Office_Branch_Code_Name { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> Create_Time { get; set; }
        public Nullable<System.Decimal> Created_By { get; set; }
        public string Created_By_Name { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public Nullable<System.Decimal> Last_Updated_By { get; set; }
        public string Last_Updated_By_Name { get; set; }
    }
}