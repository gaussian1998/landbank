﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Repository.DossierRight;
using Library.Servant.Servant.DossierRight.DossierRightModels;


namespace InspiniaWebAdminTemplate.Models.DossierRight
{
    public class Helper
    {
        public static SearchViewModel ToSearchViewModel(SearchModel model)
        {
            return new SearchViewModel()
            {

                Page = model.Page,
                PageSize = model.PageSize,
                Transaction_Type = model.Transaction_Type,
                Source = model.Source,
                Form_Number_Begin = model.Form_Number_Begin,
                Form_Number_End = model.Form_Number_End,
                Office_Branch = model.Office_Branch,
                BeginDate = model.BeginDate,
                EndDate = model.EndDate,
                Authorization_Category_L = model.Authorization_Category_L,
                Authorization_Category_B = model.Authorization_Category_B,
                FlowStatus = model.FlowStatus,
                Asset_Number = model.Asset_Number,
                Remark = model.Remark,
                DocCode = model.DocCode
            };
        }

        public static RecordSearchViewModel ToSearchRecordViewModel(RecordSearchModel model)
        {
            return new RecordSearchViewModel()
            {

                Page = model.Page,
                PageSize = model.PageSize,
                Authorization_Category = model.Authorization_Category,
                Asset_Number = model.Asset_Number,
                Land_Number = model.Land_Number,
                Build_Number = model.Build_Number,
                Authorization_Number = model.Authorization_Number
            };
        }
        public static HeaderViewModel ToHeaderViewModel(HeaderModel model)
        {
            string Office_Branch_Code = "", Office_Branch_Code_Name = "";
            string CreateUserName = "", UpdateUserName = "";
            if (!string.IsNullOrEmpty(model.Office_Branch_Code))
            {
                Office_Branch_Code = model.Office_Branch_Code;
                using (var db = new Library.Entity.Edmx.AS_LandBankEntities())
                {
                    Library.Entity.Edmx.AS_Keep_Position position = db.AS_Keep_Position.FirstOrDefault(p => p.Keep_Position_Code == Office_Branch_Code);
                    if (position != null)
                    {
                        Office_Branch_Code = position.Keep_Branch_Code;
                        Office_Branch_Code_Name = position.Keep_Branch_Name;
                    }
                    if (model.Created_By.HasValue)
                    {
                        Library.Entity.Edmx.AS_Users user = db.AS_Users.FirstOrDefault(p => p.ID == model.Created_By);
                        if (user != null)
                            CreateUserName = user.User_Name;
                    }
                    if (model.Last_Updated_By.HasValue)
                    {
                        Library.Entity.Edmx.AS_Users user = db.AS_Users.FirstOrDefault(p => p.ID == model.Last_Updated_By);
                        if(user != null)
                            UpdateUserName = user.User_Name;
                    }
                }
            }
            string SourceName = DossierRightGeneral.GetSourceName(model.Source);
            string Transaction_TypeName = DossierRightGeneral.GetFormName(model.Transaction_Type);
            return new HeaderViewModel()
            {
                ID = model.ID,
                Form_Number = model.Form_Number,
                Source = model.Source,
                SourceName = SourceName,
                Transaction_Type = model.Transaction_Type,
                Transaction_TypeName = model.Transaction_TypeName,
                Transaction_Status = model.Transaction_Status,
                Transaction_StatusName = model.Transaction_StatusName,
                Transaction_Datetime_Entered = model.Transaction_Datetime_Entered,
                Office_Branch_Code = model.Office_Branch_Code,
                Office_Branch_Code_Name = Office_Branch_Code_Name,
                Description = model.Description,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Created_By_Name = CreateUserName,
                Last_Updated_By = model.Last_Updated_By,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By_Name = UpdateUserName,
            };
        }
        public static DossierRightRecordViewModel ToRecordViewModel(DossierRightRecordModel model)
        {
            if (model == null)
                return null;
            return new DossierRightRecordViewModel()
            {
                ID = model.ID,
                AS_Assets_Right_ID = model.AS_Assets_Right_ID,
                Authorization_Category = model.Authorization_Category,
                Authorization_Category_Name = model.Authorization_Category == "L"? "土地所有權狀" : "建物所有權狀",
                AS_Assets_Right_Header_ID = model.AS_Assets_Right_Header_ID,
                Asset_Number = model.Asset_Number,
                City_Code = model.City_Code,
                City_Name = model.City_Name,
                District_Code = model.District_Code,
                District_Name = model.District_Name,
                Section_Code = model.Section_Code,
                Section_Name = model.Section_Name,
                Sub_Section_Name = model.Sub_Section_Name,
                Parent_Land_Number = model.Parent_Land_Number,
                Filial_Land_Number = model.Filial_Land_Number,
                Build_Number = model.Build_Number,
                Build_Address = model.Build_Address,
                Building_STRU = model.Building_STRU,
                Used_Type = model.Used_Type,
                Registration_Date = model.Registration_Date,
                RightBook_Date = model.RightBook_Date,
                Authorization_Number = model.Authorization_Number,
                Right_UploadFile = model.Right_UploadFile,
                Description = model.Description,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }
        public static DossierRightMainViewModel ToRecordMainViewModel(DossierRightMainModel model)
        {
            if (model == null)
                return null;
            return new DossierRightMainViewModel()
            {
                ID = model.ID,
                Authorization_Category = model.Authorization_Category,
                Authorization_Category_Name = model.Authorization_Category == "L" ? "土地所有權狀" : "建物所有權狀",
                Asset_Number = model.Asset_Number,
                City_Code = model.City_Code,
                City_Name = model.City_Name,
                District_Code = model.District_Code,
                District_Name = model.District_Name,
                Section_Code = model.Section_Code,
                Section_Name = model.Section_Name,
                Sub_Section_Name = model.Sub_Section_Name,
                Parent_Land_Number = model.Parent_Land_Number,
                Filial_Land_Number = model.Filial_Land_Number,
                Build_Number = model.Build_Number,
                Build_Address = model.Build_Address,
                Building_STRU = model.Building_STRU,
                Used_Type = model.Used_Type,
                Registration_Date = model.Registration_Date,
                RightBook_Date = model.RightBook_Date,
                Authorization_Number = model.Authorization_Number,
                Right_UploadFile = model.Right_UploadFile,
                Description = model.Description,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }
        public static HeaderModel ToHeaderModel(HeaderViewModel viewModel)
        {
            return new HeaderModel()
            {
                ID = viewModel.ID,
                Form_Number = viewModel.Form_Number,
                Office_Branch_Code = viewModel.Office_Branch_Code,
                Source = viewModel.Source,
                Transaction_Type = viewModel.Transaction_Type,
                Transaction_Status = viewModel.Transaction_Status,
                Transaction_Datetime_Entered = viewModel.Transaction_Datetime_Entered,
                Description = viewModel.Description,
                Create_Time = viewModel.Create_Time,
                Created_By = viewModel.Created_By,
                Created_By_Name = viewModel.Created_By_Name,
                Last_Updated_Time = viewModel.Last_Updated_Time,
                Last_Updated_By = viewModel.Last_Updated_By,
                Last_Updated_By_Name = viewModel.Last_Updated_By_Name
            };
        }
        public static DossierRightRecordModel ToRecordModel(DossierRightRecordViewModel viewModel)
        {
            return new DossierRightRecordModel()
            {
                ID = viewModel.ID,
                AS_Assets_Right_ID = viewModel.AS_Assets_Right_ID,
                Authorization_Category = viewModel.Authorization_Category,
                AS_Assets_Right_Header_ID = viewModel.AS_Assets_Right_Header_ID,
                Asset_Number = viewModel.Asset_Number,
                City_Code = viewModel.City_Code,
                City_Name = viewModel.City_Name,
                District_Code = viewModel.District_Code,
                District_Name = viewModel.District_Name,
                Section_Code = viewModel.Section_Code,
                Section_Name = viewModel.Section_Name,
                Sub_Section_Name = viewModel.Sub_Section_Name,
                Parent_Land_Number = viewModel.Parent_Land_Number,
                Filial_Land_Number = viewModel.Filial_Land_Number,
                Build_Number = viewModel.Build_Number,
                Build_Address = viewModel.Build_Address,
                Building_STRU = viewModel.Building_STRU,
                Used_Type = viewModel.Used_Type,
                Registration_Date = viewModel.Registration_Date,
                RightBook_Date = viewModel.RightBook_Date,
                Authorization_Number = viewModel.Authorization_Number,
                Right_UploadFile = viewModel.Right_UploadFile,
                Description = viewModel.Description,
                Create_Time = viewModel.Create_Time,
                Created_By = viewModel.Created_By,
                Last_Updated_Time = viewModel.Last_Updated_Time,
                Last_Updated_By = viewModel.Last_Updated_By
            };
        }

        public static SearchModel ToSearchModel(SearchViewModel viewModel)
        {
            return new SearchModel()
            {
                Page = viewModel.Page,
                PageSize = viewModel.PageSize,
                Transaction_Type = viewModel.Transaction_Type,
                Source = viewModel.Source,
                Form_Number_Begin = viewModel.Form_Number_Begin,
                Form_Number_End = viewModel.Form_Number_End,
                Office_Branch = viewModel.Office_Branch,
                BeginDate = viewModel.BeginDate,
                EndDate = viewModel.EndDate,
                Authorization_Category_L = viewModel.Authorization_Category_L,
                Authorization_Category_B = viewModel.Authorization_Category_B,
                FlowStatus = viewModel.FlowStatus,
                Asset_Number = viewModel.Asset_Number,
                Remark = viewModel.Remark,
                DocCode = viewModel.DocCode
            };
        }
        public static RecordSearchModel ToSearchRecordModel(RecordSearchViewModel viewModel)
        {
            return new RecordSearchModel()
            {
                Page = viewModel.Page,
                PageSize = viewModel.PageSize,
                Authorization_Category = viewModel.Authorization_Category,
                Asset_Number = viewModel.Asset_Number,
                Land_Number = viewModel.Land_Number,
                Build_Number = viewModel.Build_Number,
                Authorization_Number = viewModel.Authorization_Number
            };
        }
        /// <summary>
        /// Controller取的特定的View(/Views/DossierRight/下的View)
        /// </summary>
        /// <param name="ViewName">View名稱</param>
        /// <returns></returns>
        public static string GetView(string ViewName)
        {
            return "~/Views/DossierRight/" + ViewName + ".cshtml";
        }
        /// <summary>
        /// 取得功能名稱
        /// </summary>
        /// <param name="Action"></param>
        /// <returns></returns>
        public static string GetFunctionTitle(string Action)
        {
            return  DossierRightGeneral.GetFormName(Action);
        }
    }
}