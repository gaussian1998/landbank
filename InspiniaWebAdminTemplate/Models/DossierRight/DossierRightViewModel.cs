﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.DossierRight
{
    public class DossierRightViewModel
    {
        public HeaderViewModel Header { get; set; }

        public IEnumerable<DossierRightRecordViewModel> Records { get; set; }
    }
}