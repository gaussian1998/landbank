﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Accounting
{
    public class AccountingDepreciationExportQueryModel
    {
        public string ExportBeginCode { get; set; }
        public string ExportEndCode { get; set; }
        public string ExportAssetName { get; set; }
    }
}