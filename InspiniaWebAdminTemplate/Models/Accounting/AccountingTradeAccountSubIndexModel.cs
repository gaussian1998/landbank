﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.AccountingTradeAccount.Models;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.Accounting
{
    public class AccountingTradeAccountSubIndexModel
    {
        public SubQueryModel Conditions { get; set; }
        public SubIndexResult Results { get; set; }
    }
}