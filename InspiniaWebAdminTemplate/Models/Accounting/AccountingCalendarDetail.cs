﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.AccountingCalendar.Models;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.Accounting
{
    public class AccountingCalendarDetail
    {
        public List<SelectedModel> AccountingBooks { get; set; }
        public DetailModel Results { get; set; }
    }
}