﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.AccountingCalendar.Models;

namespace InspiniaWebAdminTemplate.Models.Accounting
{
    public class AccountingCalendarIndex
    {
        public List<SelectedModel> AccountingYears { get; set; }
        public List<SelectedModel> AccountingBooks { get; set; }
        public IndexResult Results { get; set; }
    }
}