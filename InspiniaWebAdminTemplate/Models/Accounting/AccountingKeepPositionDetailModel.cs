﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.KeepPosition.Models;

namespace InspiniaWebAdminTemplate.Models.Accounting
{
    public class AccountingKeepPositionDetailModel
    {
        public List<SelectedModel> SubDepts { get; set; }
        public List<SelectedModel> Depts { get; set; }
        public List<SelectedModel> Branchs { get; set; }
        public DetailModel Result { get; set; }

    }
}