﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.AccountingSubject.Models;

namespace InspiniaWebAdminTemplate.Models.Accounting
{
    public class AccountingSubjectDetailModel
    {
        public List<SelectedModel> AccountingBooks { get; set; }
        public DetailModel Result { get; set; }
    }
}