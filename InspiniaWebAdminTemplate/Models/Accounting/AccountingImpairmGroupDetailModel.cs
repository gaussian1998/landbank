﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.AccountingImpairmGroup.Models;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.Accounting
{
    public class AccountingImpairmGroupDetailModel
    {
        public DetailModel Result { get; set; }
        public List<SelectedModel> MainKinds { get; set; }
        public List<SelectedModel> DetailKinds { get; set; }
        public List<SelectedModel> UseTypes { get; set; }
    }
}