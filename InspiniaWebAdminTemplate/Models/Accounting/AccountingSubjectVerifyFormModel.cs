﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.AccountingSubject.Models;

namespace InspiniaWebAdminTemplate.Models.Accounting
{
    public class AccountingSubjectVerifyFormModel
    {
        public DetailModel Current { get; set; }
        public DetailModel Last { get; set; }
    }
}