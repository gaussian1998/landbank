﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.AccountingBook.Models;
using Library.Common.Models;

namespace InspiniaWebAdminTemplate.Models.Accounting
{
    public class AccountingBookList
    {
        public PageList<DetailResult> Items { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public string AccountBookCode { get; set; }
        public string AccountBookType { get; set; }
        public int IsActive { get; set; }
        public int IsCancel { get; set; }
    }
}