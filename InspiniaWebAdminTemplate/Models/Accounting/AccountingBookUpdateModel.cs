﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Accounting
{
    public class AccountingBookUpdateModel
    {
        public int ID { get; set; }
        public string CodeID { get; set; }
        public string Text { get; set; }
        public bool IsActive { get; set; }
        public bool CancelCode { get; set; }
        public string Remark { get; set; }

    }
}