﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.AccountingDeprnSetting.Models;
namespace InspiniaWebAdminTemplate.Models.Accounting
{
    public class AccountingDerpnSettingIndexModel
    {
        public List<SelectedModel> MainKinds { get; set; }
        public List<SelectedModel> DetailKinds { get; set; }
        public IndexResult Results { get; set; }
    }
}