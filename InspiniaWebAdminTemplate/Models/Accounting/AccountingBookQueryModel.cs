﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Accounting
{
    public class AccountingBookQueryModel
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public string AccountBookCode { get; set; }
        public string AccountBookType { get; set; }
        public int IsActive { get; set; }
        public int IsCancel { get; set; }

    }
}