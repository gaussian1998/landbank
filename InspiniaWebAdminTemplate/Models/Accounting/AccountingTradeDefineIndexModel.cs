﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.AccountingTradeDefine.Models;

namespace InspiniaWebAdminTemplate.Models.Accounting
{
    public class AccountingTradeDefineIndexModel
    {
        public List<SelectedModel> TransTypes { get; set; }
        public List<SelectedModel> TradeTypes { get; set; }
        public IndexResult Results { get; set; }
    }
}