﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Accounting
{
    public class AccountingBookBatchUpdateModel
    {
        public int ID { get; set; }
        public bool IsActive { get; set; }
        public bool IsCancel { get; set; }
    }
}