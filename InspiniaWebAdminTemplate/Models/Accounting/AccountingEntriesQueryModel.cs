﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InspiniaWebAdminTemplate.Models.Accounting
{
    public class AccountingEntriesQueryModel
    {


        /// <summary>
        /// 入帳開始日期
        /// </summary>
        public DateTime BeginOpenDate { get; set; } = DateTime.Today;
        /// <summary>
        /// 入帳結束日期
        /// </summary>
        public DateTime EndOpenDate { get; set; } = DateTime.Today;
        /// <summary>
        /// 交易序號
        /// </summary>
        public string TransSeq { get; set; }
        /// <summary>
        /// 交易類別  ddl source: code table Class = V03
        /// </summary>
        public string TransType { get; set; }
        /// <summary>
        /// 交易區分 ddl source: code table Class = V04 CodID Not In 03 04
        /// </summary>
        public string TradeType { get; set; }
        /// <summary>
        /// 分錄編號
        /// </summary>
        public int? MasterNo { get; set; }
        /// <summary>
        /// 資產編號
        /// </summary>
        public string AssetNo { get; set; }
    }
}
