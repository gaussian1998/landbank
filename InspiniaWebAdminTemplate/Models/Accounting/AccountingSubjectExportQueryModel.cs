﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Accounting
{
    public class AccountingSubjectExportQueryModel
    {
        public int ExportAL { get; set; }
        public string ExportBeginSubject { get; set; }
        public string ExportEndSubject { get; set; }
        public DateTime ExportFromDate { get; set; }
        public DateTime ExportToDate { get; set; }
    }
}