﻿using Library.Common.Models;
using Library.Servant.Servant.CodeTable.Models;

namespace InspiniaWebAdminTemplate.Models.ApprovalReassignEdit
{
    public class IndexViewModel
    {
        public int FlowOpenID { get; set; }
        public BranchDepartmentOptions Options { get; set; }
        public SearchViewModel Search { get; set; }
        public PageList<IndexItem> Page { get; set; }
    }
}