﻿using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.ApprovalReassignEdit
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public int ID { get; set; }
        public string BranchCodeID { get; set; }
        public string BranchName { get; set; }
        public string DepartmentCodeID { get; set; }
        public string DepartmentName { get; set; }
    }
}