﻿namespace InspiniaWebAdminTemplate.Models.ApprovalReassignEdit
{
    public class IndexItem
    {
        public int UserID { get; set; }
        public string BranchName { get; set; }
        public string DepartmentName { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
    }
}