﻿using System;
using Library.Servant.Servant.TodoListManagement.Models;

namespace InspiniaWebAdminTemplate.Models.QueryTodoList
{
    public class ItemViewModel
    {
        public string TodoListCode { get; set; }
        public string TodoTitile { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string TodoUser { get; set; }
        public string Status { get; set; }

        public static explicit operator ItemViewModel(ItemResult from)
        {
            return new ItemViewModel
            {
                TodoListCode = from.TodoListCode,
                TodoTitile = from.TodoTitile,
                CreateDateTime = from.CreateDateTime,
                TodoUser = from.TodoUserName,
                Status = from.Status   
            };
        }
    }
}