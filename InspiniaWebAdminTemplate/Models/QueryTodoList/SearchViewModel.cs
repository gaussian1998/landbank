﻿using InspiniaWebAdminTemplate.Models.Common;
using Library.Servant.Servant.TodoListManagement.Models;
using Library.Servant.Servant.Common.Models;

namespace InspiniaWebAdminTemplate.Models.QueryTodoList
{
    public class SearchViewModel : AbstractPageModel<SearchViewModel>
    {
        public SearchViewModel() {
            BranchRange = new RangeViewModel<string>();
            TodoListCodeRange = new RangeViewModel<string>();
        }

        public RangeViewModel<string> BranchRange { get; set; }
        public RangeViewModel<string> TodoListCodeRange { get; set; }
        public string Status { get; set; }

        public static explicit operator SearchModel(SearchViewModel vm)
        {
            return new SearchModel
            {
                PageSize = vm.PageSize,
                Page = vm.Page,
                BranchRange = new RangeModel<string>
                {
                    Start = vm.BranchRange.Start,
                    End = vm.BranchRange.End
                },
                TodoCodeRange = new RangeModel<string>
                {
                    Start = vm.TodoListCodeRange.Start,
                    End = vm.TodoListCodeRange.End
                },
                Status = vm.Status
            };
        }
    }
}