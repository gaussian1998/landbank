﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InspiniaWebAdminTemplate.Models.Common;

namespace InspiniaWebAdminTemplate.Models.QueryTodoList
{
    public class IndexViewModel
    {
        public IndexViewModel()
        {
            Conditions = new SearchViewModel();
            Items = new List<ItemViewModel>();
        }

        public SearchViewModel Conditions { get; set; }
        public List<ItemViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
    }
}