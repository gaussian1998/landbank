﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InspiniaWebAdminTemplate.Models.Land;

namespace InspiniaWebAdminTemplate.Models.Land
{
    public class AssetsIndexViewModel
    {
        public IEnumerable<AssetLandViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public AssetsSearchViewModel condition { get; set; }
    }
}