﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Land
{
    public class AssetsSearchViewModel
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }
        public string City_Code { get; set; }
        public string District_Code { get; set; }
        public string Section_Code { get; set; }

        public string Filial_Land_Number_Begin { get; set; }
        public string Filial_Land_Number_End { get; set; }

        public string AssetNumber { get; set; }
    }
}