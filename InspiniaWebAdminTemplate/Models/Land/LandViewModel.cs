﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Land
{
    public class LandViewModel
    {
        public LandHeaderViewModel Header { get; set; }

        public IEnumerable<AssetLandViewModel> Assets { get; set; }

        /// <summary>
        /// 土地分割or土地合併資訊
        /// </summary>
        public IEnumerable<AssetLandViewModel> ExtraAssets { get; set; }

        /// <summary>
        /// 土地處分
        /// </summary>
        public IEnumerable<AssetLandRetireViewModel> RetireAssets { get; set; }

        /// <summary>
        /// 土地異動資料
        /// </summary>
        public IEnumerable<AssetLandChangeViewModel> ChangeAssets { get; set; }
    }
}