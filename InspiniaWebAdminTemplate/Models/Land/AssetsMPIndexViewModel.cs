﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InspiniaWebAdminTemplate.Models.Land;

namespace InspiniaWebAdminTemplate.Models.Land
{
    public class AssetsMPIndexViewModel
    {
        public IEnumerable<AssetLandMPViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public AssetsMPSearchViewModel condition { get; set; }
    }
}