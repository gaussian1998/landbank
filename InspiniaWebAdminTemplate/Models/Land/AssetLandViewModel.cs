﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Land
{
    public class AssetLandViewModel
    {
        public int ID { get; set; }
        public string Office_Branch { get; set; }
        public string Book_Type_Code { get; set; }
        public string Asset_Number { get; set; }
        public string Asset_Category_code { get; set; }
        public string New_Asset_Category_code { get; set; } //For 土地重分類用
        public string Location_Disp { get; set; }
        public string Assets_Land_ID { get; set; }
        public string Old_Asset_Number { get; set; }
        public string Old_Asset_Category_code { get; set; }
        public string Description { get; set; }
        public string City_Code { get; set; }
        public string City_Name { get; set; }
        public string District_Code { get; set; }
        public string District_Name { get; set; }
        public string Section_Code { get; set; }
        public string Section_Name { get; set; }
        public string Sub_Section_Name { get; set; }
        public string LandMark
        {
            get
            {
                return (!string.IsNullOrEmpty(this.City_Name) ? this.City_Name : "") + (!string.IsNullOrEmpty(this.District_Name) ? this.District_Name : "") + (!string.IsNullOrEmpty(this.Section_Name) ? this.Section_Name : "") + (!string.IsNullOrEmpty(this.Sub_Section_Name) ? this.Sub_Section_Name : "");
            }
        }
        public string Parent_Land_Number { get; set; }
        public string Filial_Land_Number { get; set; }
        public string Authorization_Number { get; set; }
        public string Authorized_name { get; set; }
        public decimal Area_Size { get; set; }
        public decimal Authorized_Scope_Molecule { get; set; }
        public decimal Authorized_Scope_Denomminx { get; set; }
        public decimal Authorized_Area { get; set; }
        public string Used_Type { get; set; }
        public string Used_Status { get; set; }
        public string Obtained_Method { get; set; }
        public string Used_Partition { get; set; }
        public string Is_Marginal_Land { get; set; }
        public decimal Own_Area { get; set; }
        public decimal NONOwn_Area { get; set; }
        public decimal Original_Cost { get; set; }
        public decimal Deprn_Amount { get; set; }
        public decimal Reval_Land_VAT { get; set; }
        public decimal Reval_Adjustment_Amount { get; set; }
        public decimal Business_Area_Size { get; set; }
        public decimal NONBusiness_Area_Size { get; set; }
        public decimal Current_Cost { get; set; }
        public decimal Reval_Reserve { get; set; }
        public decimal Business_Book_Amount { get; set; }
        public decimal NONBusiness_Book_Amount { get; set; }
        public decimal Business_Reval_Reserve { get; set; }
        public decimal NONBusiness_Reval_Reserve { get; set; }
        public System.DateTime Datetime_Placed_In_Service { get; set; }
        public decimal Year_Number { get; set; }
        public decimal Announce_Amount { get; set; }
        public decimal Announce_Price { get; set; }
        public string Tax_Type { get; set; }
        public string Reduction_reason { get; set; }
        public decimal Reduction_Area { get; set; }
        public decimal Declared_Price { get; set; }
        public decimal Dutiable_Amount { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public decimal Current_units { get; set; }
        public decimal Retire_Cost { get; set; }
        public decimal Sell_Amount { get; set; }
        public decimal Sell_Cost { get; set; }
        public decimal Transfer_Price { get; set; }
        public string Delete_Reason { get; set; }
        public string Urban_Renewal { get; set; }
        public string Land_Item { get; set; }
        public string CROP { get; set; }
        public System.DateTime Create_Time { get; set; }
        public string Created_By { get; set; }
        public string Created_By_Name { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public string Last_Updated_By { get; set; }
        public string Last_Updated_By_Name { get; set; }


        #region ==成本維護==
        public decimal AddCost { get; set; }
        public decimal LessCost { get; set; }
        #endregion

        /// <summary>
        /// 土地異動資料
        /// </summary>
        public IEnumerable<AssetLandChangeViewModel> ChangeAssets { get; set; }
    }
}