﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Land
{
    public class LandMPViewModel
    {
        public LandHeaderViewModel Header { get; set; }

        public IEnumerable<AssetLandMPViewModel> MPAssets { get; set; }
        /// <summary>
        /// 處分
        /// </summary>
        public IEnumerable<AssetLandMPRetireViewModel> RetireAssets { get; set; }

        /// <summary>
        /// 土地異動資料
        /// </summary>
        public IEnumerable<AssetLandMPChangeViewModel> ChangeAssets { get; set; }
    }
}