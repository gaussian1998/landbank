﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Land
{
    public class SearchViewModel
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }

        /// <summary>單據類型</summary>
        public string Transaction_Type { get; set; }

        public string Form_Number { get; set; }

        /// <summary>狀態</summary>
        public string FlowStatus { get; set; }

        /// <summary>帳本</summary>
        public string BookTypeCode { get; set; }

        /// <summary>單據處理日期-起</summary>
        public DateTime? BeginDate { get; set; }

        /// <summary>單據處理日期-迄</summary>
        public DateTime? EndDate { get; set; }

        /// <summary>入帳日期</summary>
        public DateTime? PostDate { get; set; }

        /// <summary>管轄單位</summary>
        public string OfficeBranch { get; set; }

        /// <summary>備註(關鍵字)</summary>
        public string Remark { get; set; }
    }
}