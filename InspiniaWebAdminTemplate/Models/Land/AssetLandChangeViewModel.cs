﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Land
{
    public class AssetLandChangeViewModel
    {
        public int ID { get; set; }
        public string TRX_Header_ID { get; set; }
        public string AS_Assets_Land_ID { get; set; }
        public string UPD_File { get; set; }
        public string Field_Name { get; set; }
        public string Field_Title { get; set; }
        public string Modify_Type { get; set; }
        public string Before_Data { get; set; }
        public string After_Data { get; set; }
        public Nullable<System.DateTime> Create_Time { get; set; }
        public string Created_By { get; set; }
        public string Created_By_Name { get; set; }
        public Nullable<System.DateTime> Last_UpDatetimed_Time { get; set; }
        public string Last_UpDatetimed_By { get; set; }
        public string Last_UpDatetimed_By_Name { get; set; }
    }
}