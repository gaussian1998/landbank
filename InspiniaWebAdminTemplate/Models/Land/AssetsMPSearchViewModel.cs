﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Land
{
    public class AssetsMPSearchViewModel
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }
        public string Asset_Number { get; set; }
        public string Parent_Asset_Number { get; set; }
    }
}