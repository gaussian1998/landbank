﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Land
{
    public class AssetLandMPViewModel
    {
        public int ID { get; set; }
        public string Assets_Land_MP_ID { get; set; }
        public string TRX_Header_ID { get; set; }
        public string Asset_Number { get; set; }
        public string Parent_Asset_Number { get; set; }
        public string New_Asset_Category_Code { get; set; }
        public string Asset_Category_Code { get; set; }
        public string Old_Asset_Number { get; set; }
        public DateTime Date_Placed_In_Service { get; set; }
        public decimal Current_units { get; set; }
        public string Deprn_Method_Code { get; set; }
        public decimal Life_Years { get; set; }
        public decimal Life_Months { get; set; }
        public decimal New_Life_Years { get; set; }
        public decimal New_Life_Months { get; set; }
        public string Location_Disp { get; set; }
        public string PO_Number { get; set; }
        public string PO_Destination { get; set; }
        public string Model_Number { get; set; }
        public DateTime? Transaction_Date { get; set; }
        public string Assets_Unit { get; set; }
        public string Accessory_Equipment { get; set; }
        public string Assigned_NUM { get; set; }
        public string Asset_Category_Num { get; set; }
        public string Description { get; set; }
        public string Asset_Structure { get; set; }
        public decimal Current_Cost { get; set; }
        public decimal Deprn_Reserve { get; set; }
        public decimal Salvage_Value { get; set; }
        public string Remark { get; set; }
        public System.DateTime Create_Time { get; set; }
        public string Created_By { get; set; }
        public string Created_By_Name { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public string Last_Updated_By { get; set; }
        public string Last_Updated_By_Name { get; set; }
        ///// <summary>
        ///// 土地異動資料
        ///// </summary>
        //public IEnumerable<AssetLandChangeViewModel> ChangeAssets { get; set; }
    }
}