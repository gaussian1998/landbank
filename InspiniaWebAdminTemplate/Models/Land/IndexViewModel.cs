﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InspiniaWebAdminTemplate.Models.Land
{
    public class IndexViewModel
    {
        public IEnumerable<LandHeaderViewModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public SearchViewModel condition { get; set; }
    }
}