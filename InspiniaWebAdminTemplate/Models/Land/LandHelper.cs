﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Servant.Servant.Land.LandModels;

namespace InspiniaWebAdminTemplate.Models.Land
{
    public class LandHelper
    {
        #region ==ViewModel To Model==
        public static LandHeaderModel ToLandHeaderModel(LandHeaderViewModel viewModel)
        {
            return new LandHeaderModel()
            {
                ID = viewModel.ID,
                Form_Number = viewModel.Form_Number,
                Transaction_Type = viewModel.Transaction_Type,
                Transaction_Status = viewModel.Transaction_Status,
                Book_Type_Code = viewModel.Book_Type_Code,
                Transaction_Datetime_Entered = viewModel.Transaction_Datetime_Entered,
                Accounting_Datetime = viewModel.Accounting_Datetime,
                Office_Branch = viewModel.Office_Branch,
                Auto_Post = viewModel.Auto_Post,
                Amotized_Adjustment_Flag = viewModel.Amotized_Adjustment_Flag,
                Description = viewModel.Description,
                Create_Time = viewModel.Create_Time,
                Created_By = viewModel.Created_By,
                Created_By_Name = viewModel.Created_By_Name,
                Last_UpDatetimed_Time = viewModel.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = viewModel.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = viewModel.Last_UpDatetimed_By_Name
            };
        }

        public static AssetLandModel ToAssetLandModel(AssetLandViewModel viewModel)
        {
            return new AssetLandModel()
            {
                ID = viewModel.ID,
                Asset_Number = viewModel.Asset_Number,
                Current_units = viewModel.Current_units,
                Datetime_Placed_In_Service = viewModel.Datetime_Placed_In_Service,
                Old_Asset_Number = viewModel.Old_Asset_Number,
                Asset_Category_code = viewModel.Asset_Category_code == null ? "" : viewModel.Asset_Category_code,
                New_Asset_Category_code = viewModel.New_Asset_Category_code == null ? "" : viewModel.New_Asset_Category_code,
                Location_Disp = viewModel.Location_Disp == null ? "" : viewModel.Location_Disp,
                Description = viewModel.Description == null ? "" : viewModel.Description,
                Urban_Renewal = viewModel.Urban_Renewal == null?"": viewModel.Urban_Renewal,
                City_Code = viewModel.City_Code == null ? "" : viewModel.City_Code,
                City_Name = viewModel.City_Name == null ? "" : viewModel.City_Name,
                District_Code = viewModel.District_Code == null ? "" : viewModel.District_Code,
                District_Name = viewModel.District_Name == null ? "" : viewModel.District_Name,
                Section_Code = viewModel.Section_Code == null ? "" : viewModel.Section_Code,
                Section_Name = viewModel.Section_Name == null ? "" : viewModel.Section_Name,
                Sub_Section_Name = viewModel.Sub_Section_Name == null ? "" : viewModel.Sub_Section_Name,
                Parent_Land_Number = viewModel.Parent_Land_Number == null ? "" : viewModel.Parent_Land_Number,
                Filial_Land_Number = viewModel.Filial_Land_Number == null ? "" : viewModel.Filial_Land_Number,
                Authorization_Number = viewModel.Authorization_Number == null ? "" : viewModel.Authorization_Number,
                Authorized_name = viewModel.Authorized_name == null ? "" : viewModel.Authorized_name,
                Area_Size = viewModel.Area_Size,
                Authorized_Scope_Molecule = viewModel.Authorized_Scope_Molecule,
                Authorized_Scope_Denomminx = viewModel.Authorized_Scope_Denomminx,
                Authorized_Area = viewModel.Authorized_Area,
                Used_Type = viewModel.Used_Type == null ? "" : viewModel.Used_Type,
                Used_Status = viewModel.Used_Status == null ? "" : viewModel.Used_Status,
                Obtained_Method = viewModel.Obtained_Method == null ? "" : viewModel.Obtained_Method,
                Used_Partition = viewModel.Used_Partition == null ? "" : viewModel.Used_Partition,
                Is_Marginal_Land = viewModel.Is_Marginal_Land == null ? "" : viewModel.Is_Marginal_Land,
                Own_Area = viewModel.Own_Area,
                NONOwn_Area = viewModel.NONOwn_Area,
                Original_Cost = viewModel.Original_Cost,
                Deprn_Amount = viewModel.Deprn_Amount,
                Current_Cost = viewModel.Current_Cost,
                Reval_Adjustment_Amount = viewModel.Reval_Adjustment_Amount,
                Reval_Land_VAT = viewModel.Reval_Land_VAT,
                Reval_Reserve = viewModel.Reval_Reserve,
                Business_Area_Size = viewModel.Business_Area_Size,
                Business_Book_Amount = viewModel.Business_Book_Amount,
                Business_Reval_Reserve = viewModel.Business_Reval_Reserve,
                NONBusiness_Area_Size = viewModel.NONBusiness_Area_Size,
                NONBusiness_Book_Amount = viewModel.NONBusiness_Book_Amount,
                NONBusiness_Reval_Reserve = viewModel.NONBusiness_Reval_Reserve,
                Year_Number = viewModel.Year_Number,
                Announce_Amount = viewModel.Announce_Amount,
                Announce_Price = viewModel.Announce_Price,
                Tax_Type = viewModel.Tax_Type == null ? "" : viewModel.Tax_Type,
                Reduction_reason = viewModel.Reduction_reason == null ? "" : viewModel.Reduction_reason,
                Transfer_Price = viewModel.Transfer_Price,
                Reduction_Area = viewModel.Reduction_Area,
                Declared_Price = viewModel.Declared_Price,
                Dutiable_Amount = viewModel.Dutiable_Amount,
                Remark1 = viewModel.Remark1 == null ? "" : viewModel.Remark1,
                Remark2 = viewModel.Remark2 == null ? "" : viewModel.Remark2,
                Retire_Cost = viewModel.Retire_Cost,
                Sell_Amount = viewModel.Sell_Amount,
                Sell_Cost = viewModel.Sell_Cost,
                Delete_Reason = (viewModel.Delete_Reason != null) ? viewModel.Delete_Reason : "",
                Land_Item = (viewModel.Land_Item != null) ? viewModel.Land_Item : "",
                CROP = (viewModel.CROP != null) ? viewModel.CROP : "",
                Create_Time = viewModel.Create_Time,
                Created_By = viewModel.Created_By == null ? "" : viewModel.Created_By,
                Created_By_Name = viewModel.Created_By_Name == null ? "" : viewModel.Created_By_Name,
                Last_Updated_Time = viewModel.Last_Updated_Time,
                Last_Updated_By = viewModel.Last_Updated_By == null ? "" : viewModel.Last_Updated_By,
                Last_Updated_By_Name = viewModel.Last_Updated_By_Name == null ? "" : viewModel.Last_Updated_By_Name,
                AddCost = viewModel.AddCost,
                LessCost = viewModel.LessCost,
                 Book_Type = viewModel.Book_Type_Code==null?"":viewModel.Book_Type_Code,
                 TRXHeaderID ="",
                  Expense_Account = "",
                   Office_Branch = viewModel.Office_Branch,
                    Old_Asset_Category_code = viewModel.Old_Asset_Category_code,
                     SerialNo = ""
            };
        }

        public static AssetLandMPModel SearchDataToAssetLandMPModel(AssetLandMPViewModel viewModel)
        {
            //依MP載入至MP_Record
            using (var db = new Library.Entity.Edmx.AS_LandBankEntities())
            {
                Library.Entity.Edmx.AS_Assets_Land_MP asset_MP = db.AS_Assets_Land_MP.FirstOrDefault(p => p.Asset_Number == viewModel.Asset_Number);
                if (asset_MP != null)
                {
                    return new AssetLandMPModel()
                    {
                        ID = asset_MP.ID,
                        Assets_Land_MP_ID = "",
                        TRX_Header_ID = viewModel.TRX_Header_ID,
                        Asset_Number = asset_MP.Asset_Number,
                        Parent_Asset_Number = asset_MP.Parent_Asset_Number,
                        Asset_Category_Code = asset_MP.Asset_Category_code,
                        Old_Asset_Number = asset_MP.Old_Asset_Number,
                        Date_Placed_In_Service = asset_MP.Datetime_Placed_In_Service,
                        Current_units = asset_MP.Current_units.HasValue?asset_MP.Current_units.Value:0,
                        Deprn_Method_Code = asset_MP.Deprn_Method_Code,
                        Life_Years = asset_MP.Life_Years.HasValue ? asset_MP.Life_Years.Value : 0,
                        Life_Months = asset_MP.Life_Months.HasValue ? asset_MP.Life_Months.Value : 0,
                        Location_Disp = asset_MP.Location_Disp,
                        PO_Number = asset_MP.PO_Number,
                        PO_Destination = asset_MP.PO_Destination,
                        Model_Number = asset_MP.Model_Number,
                        Transaction_Date = asset_MP.Transaction_Date,
                        Assets_Unit = asset_MP.Assets_Unit,
                        Accessory_Equipment = asset_MP.Accessory_Equipment,
                        Assigned_NUM = asset_MP.Assigned_NUM,
                        Asset_Category_Num = asset_MP.Asset_Category_NUM,
                        Description = asset_MP.Remark,
                        Asset_Structure = asset_MP.Asset_Structure,
                        Current_Cost = asset_MP.Current_Cost.HasValue ? asset_MP.Current_Cost.Value : 0,
                        Deprn_Reserve = asset_MP.Deprn_Reserve.HasValue ? asset_MP.Deprn_Reserve.Value : 0,
                        Salvage_Value = asset_MP.Salvage_Value.HasValue ? asset_MP.Salvage_Value.Value : 0,
                        Remark = asset_MP.Remark,
                        Create_Time = viewModel.Create_Time,
                        Created_By = viewModel.Created_By == null ? "" : viewModel.Created_By,
                        Created_By_Name = viewModel.Created_By_Name == null ? "" : viewModel.Created_By_Name,
                        Last_Updated_Time = viewModel.Last_Updated_Time,
                        Last_Updated_By = viewModel.Last_Updated_By == null ? "" : viewModel.Last_Updated_By,
                        Last_Updated_By_Name = viewModel.Last_Updated_By_Name == null ? "" : viewModel.Last_Updated_By_Name
                    };
                }
            }
            return null;
        }
        public static AssetLandMPRetireModel SearchDataToAssetLandMPRetireModel(AssetLandMPRetireViewModel viewModel)
        {
            //依MP載入至MP_Record_retire
            using (var db = new Library.Entity.Edmx.AS_LandBankEntities())
            {
                Library.Entity.Edmx.AS_Assets_Land_MP asset_MP = db.AS_Assets_Land_MP.FirstOrDefault(p => p.Asset_Number == viewModel.Asset_Number);
                if (asset_MP != null)
                {
                    return new AssetLandMPRetireModel()
                    {
                        ID = viewModel.ID,
                        TRX_Header_ID = viewModel.TRX_Header_ID,
                        Assets_Land_MP_ID = "",
                        Serial_Number = "",
                        Asset_Number = asset_MP.Asset_Number,
                        Parent_Asset_Number = asset_MP.Parent_Asset_Number,
                        Old_Cost = asset_MP.Current_Cost.HasValue?asset_MP.Current_Cost.Value:0,
                        Reason_Code = viewModel.Reason_Code,
                        Retire_Cost = viewModel.Retire_Cost,
                        Sell_Amount = viewModel.Sell_Amount,
                        Sell_Cost = viewModel.Sell_Cost,
                        New_Cost = viewModel.New_Cost,
                        Reval_Adjustment_Amount = viewModel.Reval_Adjustment_Amount,
                        Reval_Reserve = viewModel.Reval_Reserve,
                        Reval_Land_VAT = viewModel.Reval_Land_VAT,
                        Description = viewModel.Description,
                        Account_Type = viewModel.Account_Type,
                        Receipt_Type = viewModel.Receipt_Type,
                        Create_Time = viewModel.Create_Time.HasValue ? asset_MP.Create_Time.Value : DateTime.Now,
                        Created_By = viewModel.Created_By == null ? "" : viewModel.Created_By,
                        Created_By_Name = viewModel.Created_By_Name == null ? "" : viewModel.Created_By_Name,
                        Last_UpDatetimed_Time = viewModel.Last_UpDatetimed_Time,
                        Last_UpDatetimed_By = viewModel.Last_UpDatetimed_By == null ? "" : viewModel.Last_UpDatetimed_By,
                        Last_UpDatetimed_By_Name = viewModel.Last_UpDatetimed_By_Name == null ? "" : viewModel.Last_UpDatetimed_By_Name
                    };
                }
            }
            return null;
        }
        public static AssetLandMPModel ToAssetLandMPModel(AssetLandMPViewModel viewModel)
        {
            return new AssetLandMPModel()
            {
                ID = viewModel.ID,
                Assets_Land_MP_ID = viewModel.Assets_Land_MP_ID,
                TRX_Header_ID = viewModel.TRX_Header_ID,
                Asset_Number = viewModel.Asset_Number,
                Parent_Asset_Number = viewModel.Parent_Asset_Number,
                New_Asset_Category_code=viewModel.New_Asset_Category_Code,
                Asset_Category_Code = viewModel.Asset_Category_Code,
                Old_Asset_Number = viewModel.Old_Asset_Number,
                Date_Placed_In_Service = viewModel.Date_Placed_In_Service,
                Current_units = viewModel.Current_units,
                Deprn_Method_Code = viewModel.Deprn_Method_Code,
                Life_Years = viewModel.Life_Years,
                Life_Months = viewModel.Life_Months,
                New_Life_Years = viewModel.New_Life_Years,
                New_Life_Months = viewModel.New_Life_Months,
                Location_Disp = viewModel.Location_Disp,
                PO_Number = viewModel.PO_Number,
                PO_Destination = viewModel.PO_Destination,
                Model_Number = viewModel.Model_Number,
                Transaction_Date = viewModel.Transaction_Date,
                Assets_Unit = viewModel.Assets_Unit,
                Accessory_Equipment = viewModel.Accessory_Equipment,
                Assigned_NUM = viewModel.Assigned_NUM,
                Asset_Category_Num = viewModel.Asset_Category_Num,
                Description = viewModel.Description,
                Asset_Structure = viewModel.Asset_Structure,
                Current_Cost = viewModel.Current_Cost,
                Deprn_Reserve = viewModel.Deprn_Reserve,
                Salvage_Value = viewModel.Salvage_Value,
                Remark = viewModel.Remark,
                Create_Time = viewModel.Create_Time,
                Created_By = viewModel.Created_By == null ? "" : viewModel.Created_By,
                Created_By_Name = viewModel.Created_By_Name == null ? "" : viewModel.Created_By_Name,
                Last_Updated_Time = viewModel.Last_Updated_Time,
                Last_Updated_By = viewModel.Last_Updated_By == null ? "" : viewModel.Last_Updated_By,
                Last_Updated_By_Name = viewModel.Last_Updated_By_Name == null ? "" : viewModel.Last_Updated_By_Name
            };
        }
        public static AssetLandRetireModel ToAssetLandRetireModel(AssetLandRetireViewModel viewModel)
        {
            return new AssetLandRetireModel()
            {
                ID = viewModel.ID,
                TRX_Header_ID = viewModel.TRX_Header_ID,
                Asset_Land_ID = viewModel.Asset_Land_ID,
                Serial_Number = viewModel.Serial_Number,
                Asset_Number = viewModel.Asset_Number,
                Parent_Asset_Number = viewModel.Parent_Asset_Number,
                City_Code = viewModel.City_Code == null ? "" : viewModel.City_Code,
                City_Name = viewModel.City_Name == null ? "" : viewModel.City_Name,
                District_Code = viewModel.District_Code == null ? "" : viewModel.District_Code,
                District_Name = viewModel.District_Name == null ? "" : viewModel.District_Name,
                Section_Code = viewModel.Section_Code == null ? "" : viewModel.Section_Code,
                Section_Name = viewModel.Section_Name == null ? "" : viewModel.Section_Name,
                Sub_Section_Name = viewModel.Sub_Section_Name == null ? "" : viewModel.Sub_Section_Name,
                Parent_Land_Number = viewModel.Parent_Land_Number == null ? "" : viewModel.Parent_Land_Number,
                Filial_Land_Number = viewModel.Filial_Land_Number == null ? "" : viewModel.Filial_Land_Number,
                Build_Address = viewModel.Build_Address == null ? "" : viewModel.Build_Address,
                Build_Number = viewModel.Build_Number == null ? "" : viewModel.Build_Number,
                Old_Cost = viewModel.Old_Cost,
                Retire_Cost = viewModel.Retire_Cost,
                Sell_Amount = viewModel.Sell_Amount,
                Sell_Cost = viewModel.Sell_Cost,
                New_Cost = viewModel.New_Cost,
                Reval_Adjustment_Amount = viewModel.Reval_Adjustment_Amount,
                Reval_Reserve = viewModel.Reval_Reserve,
                Reval_Land_VAT = viewModel.Reval_Land_VAT,
                Reason_Code = viewModel.Reason_Code == null ? "" : viewModel.Reason_Code,
                Description = viewModel.Description == null ? "" : viewModel.Description,
                Account_Type = viewModel.Account_Type,
                Receipt_Type = viewModel.Receipt_Type,
                Retire_Cost_Account = viewModel.Retire_Cost_Account == null ? "" : viewModel.Retire_Cost_Account,
                Sell_Amount_Account = viewModel.Sell_Amount_Account == null ? "" : viewModel.Sell_Amount_Account,
                Sell_Cost_Account = viewModel.Sell_Cost_Account == null ? "" : viewModel.Sell_Cost_Account,
                Reval_Adjustment_Amount_Account = viewModel.Reval_Adjustment_Amount_Account == null ? "" : viewModel.Reval_Adjustment_Amount_Account,
                Reval_Reserve_Account = viewModel.Reval_Reserve_Account == null ? "" : viewModel.Reval_Reserve_Account,
                Reval_Land_VAT_Account = viewModel.Reval_Land_VAT_Account,
                Create_Time = viewModel.Create_Time,
                Created_By = viewModel.Created_By == null ? "" : viewModel.Created_By,
                Created_By_Name = viewModel.Created_By_Name == null ? "" : viewModel.Created_By_Name,
                Last_UpDatetimed_By = viewModel.Last_UpDatetimed_By == null ? "" : viewModel.Last_UpDatetimed_By,
                Last_UpDatetimed_Time = viewModel.Last_UpDatetimed_Time,
                Last_UpDatetimed_By_Name = viewModel.Last_UpDatetimed_By_Name == null ? "" : viewModel.Last_UpDatetimed_By_Name
            };
        }
        public static AssetLandMPRetireModel ToAssetLandMPRetireModel(AssetLandMPRetireViewModel viewModel)
        {
            return new AssetLandMPRetireModel()
            {
                ID = viewModel.ID,
                TRX_Header_ID = viewModel.TRX_Header_ID,
                Serial_Number = viewModel.Serial_Number,
                Asset_Number = viewModel.Asset_Number,
                Parent_Asset_Number = viewModel.Parent_Asset_Number,
                Old_Cost = viewModel.Old_Cost,
                Retire_Cost = viewModel.Retire_Cost,
                Sell_Amount = viewModel.Sell_Amount,
                Sell_Cost = viewModel.Sell_Cost,
                New_Cost = viewModel.New_Cost,
                Reval_Adjustment_Amount = viewModel.Reval_Adjustment_Amount,
                Reval_Reserve = viewModel.Reval_Reserve,
                Reval_Land_VAT = viewModel.Reval_Land_VAT,
                Reason_Code = viewModel.Reason_Code == null ? "" : viewModel.Reason_Code,
                Description = viewModel.Description == null ? "" : viewModel.Description,
                Account_Type = viewModel.Account_Type,
                Receipt_Type = viewModel.Receipt_Type,
                Create_Time = viewModel.Create_Time,
                Created_By = viewModel.Created_By == null ? "" : viewModel.Created_By,
                Created_By_Name = viewModel.Created_By_Name == null ? "" : viewModel.Created_By_Name,
                Last_UpDatetimed_By = viewModel.Last_UpDatetimed_By == null ? "" : viewModel.Last_UpDatetimed_By,
                Last_UpDatetimed_Time = viewModel.Last_UpDatetimed_Time,
                Last_UpDatetimed_By_Name = viewModel.Last_UpDatetimed_By_Name == null ? "" : viewModel.Last_UpDatetimed_By_Name
            };
        }
        public static AssetLandChangeModel ToAssetLandChangeModel(AssetLandChangeViewModel viewModel)
        {
            return new AssetLandChangeModel()
            {
                ID = viewModel.ID,
                TRX_Header_ID = viewModel.TRX_Header_ID,
                AS_Assets_Land_ID = viewModel.AS_Assets_Land_ID,
                UPD_File = viewModel.UPD_File,
                Field_Name = viewModel.Field_Name,
                Modify_Type = viewModel.Modify_Type,
                Before_Data = viewModel.Before_Data,
                After_Data = viewModel.After_Data,
                Create_Time = viewModel.Create_Time,
                Created_By = viewModel.Created_By == null ? "" : viewModel.Created_By,
                Created_By_Name = viewModel.Created_By_Name == null ? "" : viewModel.Created_By_Name,
                Last_UpDatetimed_By = viewModel.Last_UpDatetimed_By == null ? "" : viewModel.Last_UpDatetimed_By,
                Last_UpDatetimed_Time = viewModel.Last_UpDatetimed_Time,
                Last_UpDatetimed_By_Name = viewModel.Last_UpDatetimed_By_Name == null ? "" : viewModel.Last_UpDatetimed_By_Name
            };
        }
        public static SearchModel ToSearchModel(SearchViewModel viewModel)
        {
            return new SearchModel()
            {
                BeginDate = viewModel.BeginDate,
                BookTypeCode = viewModel.BookTypeCode,
                EndDate = viewModel.EndDate,
                FlowStatus = viewModel.FlowStatus,
                OfficeBranch = viewModel.OfficeBranch,
                Page = viewModel.Page,
                PageSize = viewModel.PageSize,
                PostDate = viewModel.PostDate,
                Remark = viewModel.Remark,
                Transaction_Type = viewModel.Transaction_Type,
                TRXHeaderID = viewModel.Form_Number
            };
        }

        public static AssetsSearchModel ToAssetsSearchModel(AssetsSearchViewModel viewModel)
        {
            return new AssetsSearchModel()
            {
                 AssetNumber = viewModel.AssetNumber,
                 City_Code = viewModel.City_Code,
                 District_Code = viewModel.District_Code,
                 Filial_Land_Number_Begin = viewModel.Filial_Land_Number_Begin,
                 Filial_Land_Number_End = viewModel.Filial_Land_Number_End,
                 Page = viewModel.Page,
                 PageSize = viewModel.PageSize,
                 Section_Code = viewModel.Section_Code
            };

        }
        public static AssetsMPSearchModel ToAssetsMPSearchModel(AssetsMPSearchViewModel viewModel)
        {
            return new AssetsMPSearchModel()
            {
                 Asset_Number = viewModel.Asset_Number,
                 Parent_Asset_Number = viewModel.Parent_Asset_Number,
                 Page = viewModel.Page,
                 PageSize = viewModel.PageSize
            };

        }

        #endregion

        #region== Model To ViewModel ==
        public static LandHeaderViewModel ToLandHeaderViewModel(LandHeaderModel model)
        {
            string Office_Branch_Code = "";
            if (!string.IsNullOrEmpty(model.Office_Branch))
            {
                Office_Branch_Code = model.Office_Branch;
                using (var db = new Library.Entity.Edmx.AS_LandBankEntities())
                {
                    Library.Entity.Edmx.AS_Keep_Position position = db.AS_Keep_Position.FirstOrDefault(p => p.Keep_Dept == Office_Branch_Code);
                    if (position != null)
                    {
                        Office_Branch_Code = position.Keep_Position_Code;
                    }
                }
            }
            return new LandHeaderViewModel()
            {
                ID = model.ID,
                Form_Number = model.Form_Number,
                Transaction_Type = model.Transaction_Type,
                Transaction_Status = model.Transaction_Status,
                Book_Type_Code = model.Book_Type_Code,
                Transaction_Datetime_Entered = model.Transaction_Datetime_Entered,
                Accounting_Datetime = model.Accounting_Datetime,
                Office_Branch = model.Office_Branch,
                Office_Branch_Code= Office_Branch_Code,
                Description = model.Description,
                Auto_Post = model.Auto_Post,
                Amotized_Adjustment_Flag = model.Amotized_Adjustment_Flag,
                Amotized_Adjustment_Flag_Name = model.Amotized_Adjustment_Flag_Name,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Created_By_Name = model.Created_By_Name,
                Last_UpDatetimed_By = model.Last_UpDatetimed_By,
                Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name,
                AssetsCount = model.AssetsCount,
                OfficeBranchName = model.OfficeBranchName,
                TransactionStatusName = model.TransactionStatusName,
                Transaction_TypeName = model.Transaction_TypeName
            };
        }

        public static AssetLandViewModel ToAssetLandViewModel(AssetLandModel model)
        {
            AssetLandViewModel Result = null;
            if (model != null)
            {
                string city_Name = model.City_Name, district_Name = model.District_Name,
                    section_Name = model.Section_Name;
                if (string.IsNullOrEmpty(city_Name) ||
                    string.IsNullOrEmpty(district_Name) ||
                    string.IsNullOrEmpty(section_Name))
                {
                    using (var db = new Library.Entity.Edmx.AS_LandBankEntities())
                    {
                        if (string.IsNullOrEmpty(city_Name))
                        {
                            var result = (from m in db.AS_Land_Code
                                          where m.City_Code == model.City_Code
                                          select new Library.Servant.Servant.Land.LandModels.CodeItem()
                                            {
                                                    Value = m.City_Code,
                                                  Text = m.City_Name
                                              }).Distinct().ToList();
                            if (result != null && result.Count() > 0)
                                city_Name = result.First().Text;
                        }
                        if (string.IsNullOrEmpty(district_Name))
                        {
                            var result = (from m in db.AS_Land_Code
                                      where m.City_Code == model.City_Code &&
                                            m.District_Code == model.District_Code
                                          select new Library.Servant.Servant.Land.LandModels.CodeItem()
                                          {
                                              Value = m.District_Code,
                                              Text = m.District_Name
                                          }).Distinct().ToList();
                            if (result != null && result.Count() > 0)
                                district_Name = result.First().Text;
                        }
                        if (string.IsNullOrEmpty(section_Name))
                        {
                            var result = (from m in db.AS_Land_Code
                                          where m.City_Code == model.City_Code &&
                                                m.District_Code == model.District_Code &&
                                                m.Section_Code == model.Section_Code
                                          select new Library.Servant.Servant.Land.LandModels.CodeItem()
                                          {
                                              Value = m.Section_Code,
                                              Text = m.Sectioni_Name
                                          }).Distinct().ToList();
                            if (result != null && result.Count() > 0)
                                section_Name = result.First().Text;
                        }
                    }
                }
                string New_Asset_Category_code = "";
                if (!string.IsNullOrEmpty(model.TRXHeaderID))
                {
                    using (var db = new Library.Entity.Edmx.AS_LandBankEntities())
                    {

                        string changeFieldName = "Asset_Category_code";
                        var result = db.AS_Assets_Land_Change.FirstOrDefault(
                            o => o.TRX_Header_ID == model.TRXHeaderID
                                && o.Field_Name == changeFieldName);
                        if (result != null)
                            New_Asset_Category_code = result.After_Data;
                    }
                }
                Result = new AssetLandViewModel()
                {
                    ID = model.ID,
                    Asset_Number = model.Asset_Number,
                    Current_units = model.Current_units,
                    Datetime_Placed_In_Service = model.Datetime_Placed_In_Service,
                    Assets_Land_ID = model.Asset_Land_ID,
                    Old_Asset_Category_code = model.Old_Asset_Category_code,
                    New_Asset_Category_code = New_Asset_Category_code,
                    Office_Branch = model.Office_Branch,
                    Book_Type_Code = model.Book_Type,
                    Old_Asset_Number = model.Old_Asset_Number,
                    Asset_Category_code = model.Asset_Category_code,
                    Location_Disp = model.Location_Disp,
                    Description = model.Description,
                    Urban_Renewal = model.Urban_Renewal,
                    City_Code = model.City_Code,
                    City_Name = city_Name,
                    District_Code = model.District_Code,
                    District_Name = district_Name,
                    Section_Code = model.Section_Code,
                    Section_Name = section_Name,
                    Sub_Section_Name = model.Sub_Section_Name,
                    Parent_Land_Number = model.Parent_Land_Number,
                    Filial_Land_Number = model.Filial_Land_Number,
                    Authorization_Number = model.Authorization_Number,
                    Authorized_name = model.Authorized_name,
                    Area_Size = model.Area_Size,
                    Authorized_Scope_Molecule = model.Authorized_Scope_Molecule,
                    Authorized_Scope_Denomminx = model.Authorized_Scope_Denomminx,
                    Authorized_Area = model.Authorized_Area,
                    Used_Type = model.Used_Type,
                    Used_Status = model.Used_Status,
                    Obtained_Method = model.Obtained_Method,
                    Used_Partition = model.Used_Partition,
                    Is_Marginal_Land = model.Is_Marginal_Land,
                    Own_Area = model.Own_Area,
                    NONOwn_Area = model.NONOwn_Area,
                    Original_Cost = model.Original_Cost,
                    Deprn_Amount = model.Deprn_Amount,
                    Current_Cost = model.Current_Cost,
                    Reval_Adjustment_Amount = model.Reval_Adjustment_Amount,
                    Reval_Land_VAT = model.Reval_Land_VAT,
                    Reval_Reserve = model.Reval_Reserve,
                    Business_Area_Size = model.Business_Area_Size,
                    Business_Book_Amount = model.Business_Book_Amount,
                    Business_Reval_Reserve = model.Business_Reval_Reserve,
                    NONBusiness_Area_Size = model.NONBusiness_Area_Size,
                    NONBusiness_Book_Amount = model.NONBusiness_Book_Amount,
                    NONBusiness_Reval_Reserve = model.NONBusiness_Reval_Reserve,
                    Year_Number = model.Year_Number,
                    Announce_Amount = model.Announce_Amount,
                    Announce_Price = model.Announce_Price,
                    Tax_Type = model.Tax_Type,
                    Reduction_reason = model.Reduction_reason,
                    Transfer_Price = model.Transfer_Price,
                    Reduction_Area = model.Reduction_Area,
                    Declared_Price = model.Declared_Price,
                    Dutiable_Amount = model.Dutiable_Amount,
                    Remark1 = model.Remark1,
                    Remark2 = model.Remark2,
                    Retire_Cost = model.Retire_Cost,
                    Sell_Amount = model.Sell_Amount,
                    Sell_Cost = model.Sell_Cost,
                    Delete_Reason = model.Delete_Reason,
                    Land_Item = model.Land_Item,
                    CROP = model.CROP,
                    Create_Time = model.Create_Time,
                    Created_By = model.Created_By,
                    Created_By_Name = model.Created_By_Name,
                    Last_Updated_Time = model.Last_Updated_Time,
                    Last_Updated_By = model.Last_Updated_By,
                    Last_Updated_By_Name = model.Last_Updated_By_Name,
                    AddCost = model.AddCost,
                    LessCost = model.LessCost
                };
                if (!string.IsNullOrEmpty(model.TRXHeaderID))
                {
                    using (var db = new Library.Entity.Edmx.AS_LandBankEntities())
                    {

                        var result = from AS_Assets_Land_Change in db.AS_Assets_Land_Change
                                     where AS_Assets_Land_Change.TRX_Header_ID == model.TRXHeaderID
                                     select AS_Assets_Land_Change;
                        if (result != null)
                        {
                            List<AssetLandChangeViewModel> changeViewModeList = new List<AssetLandChangeViewModel>();
                            foreach (Library.Entity.Edmx.AS_Assets_Land_Change record in result)
                            {
                                AssetLandChangeViewModel changeViewModel = new AssetLandChangeViewModel(){
                                    ID = record.ID,
                                    TRX_Header_ID = record.TRX_Header_ID,
                                    AS_Assets_Land_ID = record.AS_Assets_Land_ID.HasValue?record.AS_Assets_Land_ID.Value.ToString():"",
                                    UPD_File = record.UPD_File,
                                    Field_Name = record.Field_Name,
                                    Field_Title = LandHelper.GetFieldTitle( record.Field_Name),
                                    Modify_Type = record.Modify_Type,
                                    Before_Data = record.Before_Data,
                                    After_Data = record.After_Data,
                                    Create_Time = record.Create_Time,
                                    Created_By = record.Created_By,
                                    Created_By_Name = record.Created_By_Name,
                                    Last_UpDatetimed_Time = record.Last_UpDatetimed_Time,
                                    Last_UpDatetimed_By = record.Last_UpDatetimed_By.HasValue?record.Last_UpDatetimed_By.Value.ToString():"",
                                    Last_UpDatetimed_By_Name = record.Last_UpDatetimed_By_Name,

                                };
                                changeViewModeList.Add(changeViewModel);
                            }
                            Result.ChangeAssets = changeViewModeList;
                        }
                    }
                }
            }
            return Result;
        }
        public static AssetLandMPViewModel ToAssetLandMPViewModel(AssetLandMPModel model)
        {
            AssetLandMPViewModel Result = null;
            if (model != null)
            {
                string New_Asset_Category_Code = "";
                decimal New_Life_Years = 0, New_Life_Months = 0;
                if (!string.IsNullOrEmpty(model.TRX_Header_ID))
                {
                    using (var db = new Library.Entity.Edmx.AS_LandBankEntities())
                    {

                        string changeFieldName = "Asset_Category_code";
                        var result = db.AS_Assets_Land_MP_Change_Record.FirstOrDefault(
                            o => o.TRX_Header_ID == model.TRX_Header_ID
                                && o.Field_Name == changeFieldName);
                        if (result != null)
                            New_Asset_Category_Code = result.After_Data;

                        string changeFieldName_Life_YY = "Life_Years";
                        var result_Y = db.AS_Assets_Land_MP_Change_Record.FirstOrDefault(
                            o => o.TRX_Header_ID == model.TRX_Header_ID
                                && o.Field_Name == changeFieldName_Life_YY);
                        if (result_Y != null && !string.IsNullOrEmpty(result_Y.After_Data))
                            New_Life_Years = decimal.Parse(result_Y.After_Data);
                        string changeFieldName_Life_MM = "Life_Months";
                        var result_M = db.AS_Assets_Land_MP_Change_Record.FirstOrDefault(
                            o => o.TRX_Header_ID == model.TRX_Header_ID
                                && o.Field_Name == changeFieldName_Life_MM);
                        if (result_M != null && !string.IsNullOrEmpty(result_M.After_Data))
                            New_Life_Months = decimal.Parse(result_M.After_Data);
                    }
                }
                Result = new AssetLandMPViewModel()
                {
                    ID = model.ID,
                    Assets_Land_MP_ID = model.Assets_Land_MP_ID,
                    TRX_Header_ID = model.TRX_Header_ID,
                    Asset_Number = model.Asset_Number,
                    Parent_Asset_Number = model.Parent_Asset_Number,
                    New_Asset_Category_Code = New_Asset_Category_Code,
                    Asset_Category_Code = model.Asset_Category_Code,
                    Old_Asset_Number = model.Old_Asset_Number,
                    Date_Placed_In_Service = model.Date_Placed_In_Service,
                    Current_units = model.Current_units,
                    Deprn_Method_Code = model.Deprn_Method_Code,
                    New_Life_Years = New_Life_Years,
                    New_Life_Months = New_Life_Months,
                    Life_Years = model.Life_Years,
                    Life_Months = model.Life_Months,
                    Location_Disp = model.Location_Disp,
                    PO_Number = model.PO_Number,
                    PO_Destination = model.PO_Destination,
                    Model_Number = model.Model_Number,
                    Transaction_Date = model.Transaction_Date,
                    Assets_Unit = model.Assets_Unit,
                    Accessory_Equipment = model.Accessory_Equipment,
                    Assigned_NUM = model.Assigned_NUM,
                    Asset_Category_Num = model.Asset_Category_Num,
                    Description = model.Description,
                    Asset_Structure = model.Asset_Structure,
                    Current_Cost = model.Current_Cost,
                    Deprn_Reserve = model.Deprn_Reserve,
                    Salvage_Value = model.Salvage_Value,
                    Remark = model.Remark,
                    Create_Time = model.Create_Time,
                    Created_By = model.Created_By,
                    Created_By_Name = model.Created_By_Name,
                    Last_Updated_Time = model.Last_Updated_Time,
                    Last_Updated_By = model.Last_Updated_By,
                    Last_Updated_By_Name = model.Last_Updated_By_Name
                };
            }
            return Result;
        }
        public static AssetLandRetireViewModel ToAssetLandRetireViewModel(AssetLandRetireModel model)
        {
            AssetLandRetireViewModel Result = null;
            if (model != null)
            {
                Result = new AssetLandRetireViewModel()
                {
                    ID = model.ID,
                    TRX_Header_ID = model.TRX_Header_ID,
                    Asset_Land_ID = model.Asset_Land_ID,
                    Serial_Number = model.Serial_Number,
                    Asset_Number = model.Asset_Number,
                    Parent_Asset_Number = model.Parent_Asset_Number,
                    City_Code = model.City_Code,
                    City_Name = model.City_Name,
                    District_Code = model.District_Code,
                    District_Name = model.District_Name,
                    Section_Code = model.Section_Code,
                    Section_Name = model.Section_Name,
                    Sub_Section_Name = model.Sub_Section_Name,
                    Parent_Land_Number = model.Parent_Land_Number,
                    Filial_Land_Number = model.Filial_Land_Number,
                    Build_Address = model.Build_Address,
                    Build_Number = model.Build_Number,
                    Old_Cost = model.Old_Cost,
                    Retire_Cost = model.Retire_Cost,
                    Sell_Amount = model.Sell_Amount,
                    Sell_Cost = model.Sell_Cost,
                    New_Cost = model.New_Cost,
                    Reval_Adjustment_Amount = model.Reval_Adjustment_Amount,
                    Reval_Reserve = model.Reval_Reserve,
                    Reval_Land_VAT = model.Reval_Land_VAT,
                    Reason_Code = model.Reason_Code,
                    Description = model.Description,
                    Account_Type = model.Account_Type,
                    Receipt_Type = model.Receipt_Type,
                    Sell_Amount_Account = model.Sell_Amount_Account,
                    Sell_Cost_Account = model.Sell_Cost_Account,
                    Reval_Adjustment_Amount_Account = model.Reval_Adjustment_Amount_Account,
                    Reval_Reserve_Account = model.Reval_Reserve_Account,
                    Reval_Land_VAT_Account = model.Reval_Land_VAT_Account,
                    Create_Time = model.Create_Time,
                    Created_By = model.Created_By,
                    Created_By_Name = model.Created_By_Name,
                    Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                    Last_UpDatetimed_By = model.Last_UpDatetimed_By,
                    Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name
                };
            }
            return Result;
        }
        public static AssetLandMPRetireViewModel ToAssetLandMPRetireViewModel(AssetLandMPRetireModel model)
        {
            AssetLandMPRetireViewModel Result = null;
            if (model != null)
            {
                Result = new AssetLandMPRetireViewModel()
                {
                    ID = model.ID,
                    TRX_Header_ID = model.TRX_Header_ID,
                    Asset_Land_ID = model.Assets_Land_MP_ID,
                    Serial_Number = model.Serial_Number,
                    Asset_Number = model.Asset_Number,
                    Parent_Asset_Number = model.Parent_Asset_Number,
                    Old_Cost = model.Old_Cost,
                    Retire_Cost = model.Retire_Cost,
                    Sell_Amount = model.Sell_Amount,
                    Sell_Cost = model.Sell_Cost,
                    New_Cost = model.New_Cost,
                    Reval_Adjustment_Amount = model.Reval_Adjustment_Amount,
                    Reval_Reserve = model.Reval_Reserve,
                    Reval_Land_VAT = model.Reval_Land_VAT,
                    Reason_Code = model.Reason_Code,
                    Description = model.Description,
                    Account_Type = model.Account_Type,
                    Receipt_Type = model.Receipt_Type,
                    Create_Time = model.Create_Time,
                    Created_By = model.Created_By,
                    Created_By_Name = model.Created_By_Name,
                    Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                    Last_UpDatetimed_By = model.Last_UpDatetimed_By,
                    Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name
                };
            }
            return Result;
        }
        public static AssetLandChangeViewModel ToAssetLandChangeViewModel(AssetLandChangeModel model)
        {
            AssetLandChangeViewModel Result = null;
            if (model != null)
            {
                Result = new AssetLandChangeViewModel()
                {
                    ID = model.ID,
                    TRX_Header_ID = model.TRX_Header_ID,
                    AS_Assets_Land_ID = model.AS_Assets_Land_ID,
                    UPD_File = model.UPD_File,
                    Field_Name = model.Field_Name,
                    Modify_Type = model.Modify_Type,
                    Before_Data = model.Before_Data,
                    After_Data = model.After_Data,
                    Create_Time = model.Create_Time,
                    Created_By = model.Created_By,
                    Created_By_Name = model.Created_By_Name,
                    Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                    Last_UpDatetimed_By = model.Last_UpDatetimed_By,
                    Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name
                };
            }
            return Result;
        }
        public static AssetLandMPChangeViewModel ToAssetLandMPChangeViewModel(AssetLandMPChangeModel model)
        {
            AssetLandMPChangeViewModel Result = null;
            if (model != null)
            {
                Result = new AssetLandMPChangeViewModel()
                {
                    ID = model.ID,
                    TRX_Header_ID = model.TRX_Header_ID,
                    AS_Assets_Land_MP_Record_ID = model.AS_Assets_Land_MP_Record_ID,
                    UPD_File = model.UPD_File,
                    Field_Name = model.Field_Name,
                    Modify_Type = model.Modify_Type,
                    Before_Data = model.Before_Data,
                    After_Data = model.After_Data,
                    Create_Time = model.Create_Time,
                    Created_By = model.Created_By,
                    Created_By_Name = model.Created_By_Name,
                    Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                    Last_UpDatetimed_By = model.Last_UpDatetimed_By,
                    Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name
                };
            }
            return Result;
        }

        public static SearchViewModel ToSearchViewModel(SearchModel model)
        {
            return new SearchViewModel()
            {
                 BeginDate = model.BeginDate,
                 BookTypeCode = model.BookTypeCode,
                 EndDate = model.EndDate,
                 FlowStatus = model.FlowStatus,
                 OfficeBranch = model.OfficeBranch,
                 Page = model.Page,
                 PageSize = model.PageSize,
                 PostDate = model.PostDate,
                 Remark = model.Remark,
                 Transaction_Type = model.Transaction_Type,
                 Form_Number = model.TRXHeaderID
            };
        }
        #endregion

        #region== 共用 ==
        /// <summary>
        /// Controller取的特定的View
        /// </summary>
        /// <param name="ViewName">View名稱</param>
        /// <returns></returns>
        public static string GetLandView(string ViewName)
        {
            return "~/Views/Land/" + ViewName + ".cshtml";
        }

        /// <summary>
        /// 取得功能名稱
        /// </summary>
        /// <param name="Action"></param>
        /// <returns></returns>
        public static string GetFunctionTitle(string Action)
        {
            string FunctionName = "";
            switch (Action)
            {
                case "LandAdded":
                    FunctionName = "土地新增";
                    break;
                case "LandAssets":
                    FunctionName = "土地資產維護";
                    break;
                case "LandCost":
                    FunctionName = "土地成本維護";
                    break;
                case "LandSplit":
                    FunctionName = "土地分割";
                    break;
                case "LandConsolidation":
                    FunctionName = "土地合併";
                    break;
                case "LandDisposal":
                    FunctionName = "土地處分";
                    break;
                case "LandReclassify":
                    FunctionName = "土地資產分類異動";
                    break;
                case "LandImprovedAdded":
                    FunctionName = "資產改良新增";
                    break;
                case "LandImprovedAssets":
                    FunctionName = "資產改良維護";
                    break;
                case "LandImprovedDisposal":
                    FunctionName = "資產改良處分";
                    break;
                case "LandImprovedReclassify":
                    FunctionName = "資產改良分類異動";
                    break;
                case "LandImprovedLifeChange":
                    FunctionName = "資產改良使用年限異動";
                    break;
            }
            return FunctionName;
        }
        /// <summary>
        /// 取得功能名稱
        /// </summary>
        /// <param name="Action"></param>
        /// <returns></returns>
        public static string GetFieldTitle(string fieldName)
        {
            string FunctionName = "";
            switch (fieldName)
            {
                case "Asset_Land_ID":
                    FunctionName = "土地主檔id";
                    break;
                case "Serial_Number":
                    FunctionName = "項次";
                    break;
                case "Asset_Number":
                    FunctionName = "財產編號(資產編號)";
                    break;
                case "Asset_Category_code":
                    FunctionName = "資產分類";
                    break;
                case "Location_Disp":
                    FunctionName = "放置地點";
                    break;
                case "Old_Asset_Number":
                    FunctionName = "原資產編號";
                    break;
                case "Description":
                    FunctionName = "摘要";
                    break;
                case "City_Code":
                    FunctionName = "標示-縣市代碼";
                    break; ;
                case "City_Name":
                    FunctionName = "標示-縣市名稱";
                    break;
                case "District_Code":
                    FunctionName = "標示-鄉鎮市區代碼";
                    break; ;
                case "District_Name":
                    FunctionName = "標示-鄉鎮市區名稱";
                    break;
                case "Section_Code":
                    FunctionName = "標示-段代碼";
                    break; ;
                case "Section_Name":
                    FunctionName = "標示-段名稱";
                    break;
                case "Sub_Section_Name":
                    FunctionName = "標示-小段名稱";
                    break;
                case "Parent_Land_Number":
                    FunctionName = "母地號";
                    break;
                case "Filial_Land_Number":
                    FunctionName = "子地號";
                    break;
                case "Authorization_Number":
                    FunctionName = "權狀字號";
                    break;
                case "Authorized_name":
                    FunctionName = "地政事務所";
                    break;
                case "Area_Size":
                    FunctionName = "整筆面積";
                    break;
                case "Authorized_Scope_Molecule":
                    FunctionName = "權利範圍(分子)";
                    break;
                case "Authorized_Scope_Denomminx":
                    FunctionName = "權利範圍(分母)";
                    break;
                case "Authorized_Area":
                    FunctionName = "權利面積";
                    break;
                case "Used_Type":
                    FunctionName = "使用別(業務別)";
                    break;
                case "Used_Status":
                    FunctionName = "使用現況";
                    break;
                case "Obtained_Method":
                    FunctionName = "取得方式";
                    break;
                case "Used_Partition":
                    FunctionName = "使用分區";
                    break;
                case "Is_Marginal_Land":
                    FunctionName = "畸零地否";
                    break;
                case "Own_Area":
                    FunctionName = "自用面積";
                    break;
                case "NONOwn_Area":
                    FunctionName = "非自用面積";
                    break;
                case "Original_Cost":
                    FunctionName = "取得成本";
                    break;
                case "Deprn_Amount":
                    FunctionName = "累計減損";
                    break;
                case "Reval_Land_VAT":
                    FunctionName = "估計應付土地增值稅";
                    break;
                case "Reval_Adjustment_Amount":
                    FunctionName = "未實現重估增值";
                    break;
                case "Business_Area_Size":
                    FunctionName = "營業用面積";
                    break;
                case "NONBusiness_Area_Size":
                    FunctionName = "非營業用面積";
                    break;
                case "Current_Cost":
                    FunctionName = "帳面金額";
                    break;
                case "Reval_Reserve":
                    FunctionName = "帳面金額-重估增值";
                    break;
                case "Business_Book_Amount":
                    FunctionName = "營業用帳面金額";
                    break;
                case "NONBusiness_Book_Amount":
                    FunctionName = "非營業用帳面金額";
                    break;
                case "Business_Reval_Reserve":
                    FunctionName = "營業用帳面金額-重估增值";
                    break;
                case "NONBusiness_Reval_Reserve":
                    FunctionName = "非營業用帳面金額-重估增值";
                    break;
                case "Datetime_Placed_In_Service":
                    FunctionName = "啟用日期";
                    break;
                case "Year_Number":
                    FunctionName = "年度";
                    break;
                case "Announce_Amount":
                    FunctionName = "公告現值";
                    break;
                case "Announce_Price":
                    FunctionName = "公告地價";
                    break;
                case "Tax_Type":
                    FunctionName = "稅種";
                    break;
                case "Reduction_reason":
                    FunctionName = "減免原因";
                    break;
                case "Reduction_Area":
                    FunctionName = "減稅面積";
                    break;
                case "Declared_Price":
                    FunctionName = "申報地價";
                    break;
                case "Dutiable_Amount":
                    FunctionName = "應繳稅額";
                    break;
                case "Remark1":
                    FunctionName = "備註事項1";
                    break;
                case "Remark2":
                    FunctionName = "備註事項2";
                    break;
                case "Current_units":
                    FunctionName = "單位量";
                    break;
                case "Transfer_Price":
                    FunctionName = "移轉地價";
                    break;
                case "Urban_Renewal":
                    FunctionName = "都更案號";
                    break;
                case "LAND_ITEM":
                    FunctionName = "地目";
                    break;
                case "CROP":
                    FunctionName = "等則";
                    break;
                case "Expense_Account":
                    FunctionName = "會計科目";
                    break;
            }
            return FunctionName;
        }
        #endregion
    }
}