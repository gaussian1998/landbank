﻿using Library.Common.Interface;
using Library.Common.Web.Utility;
using System;
using System.Web;

namespace InspiniaWebAdminTemplate.Servants.Search
{
    public static class SearchConditionServant
    {
        public static ViewModel Details<ViewModel>(HttpContext context)
            where ViewModel : IValidator, new()
        {
            var cookie = new EncryptionCookie(context);
            try
            {
                var vm = cookie.Get<ViewModel>(key);
                if (vm == null || vm.IsValid() == false)
                    return new ViewModel();
                else
                    return vm;
            }
            catch
            {
                return new ViewModel();
            }
        }

        public static void Update<ViewModel>(ViewModel vm, HttpContext context)
        {
            var cookie = new EncryptionCookie(context);

            cookie.Update(key, vm);
        }

        public static void Update<ViewModel>(HttpContext context, Action<ViewModel> modify)
            where ViewModel : IValidator, new()
        {
            ViewModel condition = Details<ViewModel>(context);
            modify(condition);
            Update(condition, context);
        }

        private static readonly string key = "searchinfo".GetHashCode().ToString();
    }
}