﻿using Library.Interface;
using System;
using System.IO;
using System.Net;
using System.Web;

namespace InspiniaWebAdminTemplate.Service
{
    public static class StorageServant
    {
        public static IStorageService Create(HttpServerUtilityBase utility)
        {
            /*switch (IWebConfigure.Storage())
            {
                case "local":
                    return new LocalStorage(utility);

                case "database":
                    return new DataBase();

                case "ftp":
                    return new FTP();

                default:
                    return new NullStorage();
            }*/

            return new LocalStorage(utility);
        }

        public static byte[] ReadToEnd(this Stream stream)
        {
            byte[] buffer = new byte[4096];
            using (MemoryStream ms = new MemoryStream())
            {
                int read = 0;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                    ms.Write(buffer, 0, read);

                return ms.ToArray();
            }
        }

        public static void CopyTo(this Stream source, Stream target)
        {
            byte[] buffer = new byte[4096];

            int read = 0;
            while ((read = source.Read(buffer, 0, buffer.Length)) > 0)
                target.Write(buffer, 0, read);
        }
    }

    class FTP : IStorageService
    {
        public void Save(Stream stream, string filename)
        {
            FtpWebRequest request = CreateRequest(filename);
            request.Method = WebRequestMethods.Ftp.UploadFile;

            using (Stream upload = request.GetRequestStream())
            {
                stream.CopyTo(upload);
            }
        }

        public byte[] Load(string filename)
        {
            FtpWebRequest request = CreateRequest(filename);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            using (Stream download = request.GetResponse().GetResponseStream())
            {
                return download.ReadToEnd();
            }
        }

        public void Remove(string filename)
        {
            FtpWebRequest request = CreateRequest(filename);
            request.Method = WebRequestMethods.Ftp.DeleteFile;

            request.GetResponse();
        }

        private static FtpWebRequest CreateRequest(string filename)
        {
            FtpWebRequest request = FtpWebRequest.Create("ftp://139.162.121.174/upload/" + filename) as FtpWebRequest;//私有,一定可連
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.KeepAlive = true;
            request.Credentials = new NetworkCredential("binodata", "binodata");
            request.UseBinary = true;
            request.UsePassive = true;
            request.Timeout = 60 * 1000;

            return request;
        }
    }




    class LocalStorage : IStorageService
    {
        private readonly HttpServerUtilityBase utility;
        public LocalStorage(HttpServerUtilityBase utility)
        {
            this.utility = utility;
        }

        public void Save(Stream stream, string filename)
        {
            string filePath = ServerPath(filename);

            using (FileStream file = new FileStream(filePath, FileMode.CreateNew))
            {
                stream.CopyTo(file);
            }
        }

        public byte[] Load(string filename)
        {
            string filePath = ServerPath(filename);

            using (FileStream file = new FileStream(filePath, FileMode.Open))
            {
                return file.ReadToEnd();
            }
        }

        public void Remove(string filename)
        {
            string filePath = ServerPath(filename);

            File.Delete(filePath);
        }

        private string ServerPath(string filename)
        {
            return utility.MapPath(string.Format("~/Content/upload/{0}", filename));
        }
    }

    class DataBase : IStorageService
    {
        public void Save(Stream stream, string filename)
        {
            throw new NotImplementedException();
        }

        public byte[] Load(string filename)
        {
            throw new NotImplementedException();
        }

        public void Remove(string filename)
        {
            throw new NotImplementedException();
        }
    }

    class S3 : IStorageService
    {
        public byte[] Load(string filename)
        {
            throw new NotImplementedException();
        }

        public void Remove(string filename)
        {
            throw new NotImplementedException();
        }

        public void Save(Stream stream, string filename)
        {
            throw new NotImplementedException();
        }
    }

    class Azure : IStorageService
    {
        public byte[] Load(string filename)
        {
            throw new NotImplementedException();
        }

        public void Remove(string filename)
        {
            throw new NotImplementedException();
        }

        public void Save(Stream stream, string filename)
        {
            throw new NotImplementedException();
        }
    }

    class NullStorage : IStorageService
    {
        public byte[] Load(string filename)
        {
            throw new NotImplementedException();
        }

        public void Remove(string filename)
        {
            throw new NotImplementedException();
        }

        public void Save(Stream stream, string filename)
        {
            throw new NotImplementedException();
        }
    }
}