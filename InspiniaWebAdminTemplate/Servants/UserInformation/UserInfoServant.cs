﻿using Library.Common.Web.Utility;
using Library.Servant.Servant.UserInformation.Models;
using System.Web;

namespace InspiniaWebAdminTemplate.Servants.UserInformation
{
    public static class UserInfoServant
    {
        public static UserInfor Details(HttpContext context)
        {
            var cookie = new EncryptionCookie(context);
            var user = cookie.Get<UserInfor>(key);

            if (user == null)
                return UserInfor.Default();
            else
                return user;
        }
                
        public static void Update(UserInfor user, HttpContext context)
        {
            var cookie = new EncryptionCookie(context);

            cookie.Update( key, user);
        }

        public static void Delete(HttpContext context)
        {
            var cookie = new EncryptionCookie(context);

            cookie.Delete(key);
        }
        
        private static readonly string key = "userinfo".GetHashCode().ToString();
    }
}
