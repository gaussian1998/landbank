﻿using Library.Servant.Servant.Authentication.Models;
using System.Collections.Generic;

namespace Library.Servant.Servant.UserInformation.Models
{
    public class UserInfor
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public List<string> Functions { get; set; }

        public static UserInfor Default()
        {
            var anonymous = LoginResult.Anonymous();
            return new UserInfor {

                Name = anonymous.Name,
                Functions = anonymous.Functions
            };
        }

        public static explicit operator UserInfor(LoginResult from)
        {
            return new UserInfor
            {
                ID = from.ID,
                Name = from.Name,
                Functions = from.Functions
            };
        }
    }
}
