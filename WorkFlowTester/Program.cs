﻿using System;
using System.Linq;
using Library.Common.Utility;
using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.ApprovalTodo.Models;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servant.ApprovalTodo.Constants;
using Library.Servant.Servant.ApprovalFlow;

namespace WorkFlowTester
{
    class DB : IDisposable, IRunnable
    {
        public void run()
        {
            using (AS_LandBankEntities dbContext = new AS_LandBankEntities())
            {
                TestUtility.AssertTrue(dbContext.Database.Exists(), "本測試需要資料庫支援");
            }
        }

        public DB()
        { }

        public void Dispose()
        { }
    }

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TestSuit.Run<DB, HappyRule, StartFlow>();

                TestSuit.Run<
                    RevokeDraftFlow, 
                    RevokeNewFlow, 
                    RevokeApprovaleFlow, 
                    RevokeApprovaleFlow2, 
                    RevokeFinshFlow>();

                TestSuit.Run<
                    WithdrawNewFlow, 
                    WithdrawStartFlow,
                    WithdrawApprovalFlow, 
                    WithdrawFinishFlow>();

                TestSuit.Run<
                    RejectNewFlow, 
                    RejectStartFlow, 
                    RejectApprovalFlow, 
                    RejectFinishFlow>();

                TestSuit.Run<RejectToApproval, RejectToForbiden, RejectToNewFlow>();

                TestSuit.Run<Ressign, ReReRessign>();

                TestSuit.Run<
                    IndexDraftState,
                    IndexReject,
                    IndexWithdraw,
                    IndexWithdrawPage,
                    IndexNewState, 
                    IndexApprovalState_1, 
                    IndexApprovalState_2,
                    IndexReassign,
                    IndexPassState,
                    IndexRevokeState>();

                TestSuit.Run<CarryWithdraw>();
                
                TestSuit.Run<
                    DifferentBranchApproval,
                    DifferentBranchApply, 
                    DifferentBranchRevoke, 
                    DifferentBranchReject,
                    DifferentBranchRessign>();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    class VirtualEventListener : IEventListener
    {
        public bool Finish { get; set; }
        public bool Approval { get; set; }
        public bool Revoke { get; set; }
        public bool Reject { get; set; }
        public bool Withdraw { get; set; }
        public string Carray { get; set; }
       
        public bool OnApproval(string formNo, string carry, AS_LandBankEntities context)
        {
            Carray = carry;
            return Approval = true;
        }

        public bool OnReject(string formNo, string carry, AS_LandBankEntities context)
        {
            return Reject = true;
        }

        public bool OnRevoke(string formNo, string carry, AS_LandBankEntities context)
        {
            Carray = carry;
            return Revoke = true;
        }

        public bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            Carray = carry;
            return Finish = true;
        }

        public bool OnWithdraw(string formNo, string carry, AS_LandBankEntities context)
        {
            Carray = carry;
            return Withdraw = true;
        }
    }
    
    class Context : IDisposable
    {
        /// <summary>
        ///  每回測試開始都會重新初始化
        /// </summary>
        protected VirtualEventListener Listener = new VirtualEventListener();
        protected readonly string FormNo;
        protected readonly int FlowOpenID;

        /// <summary>
        ///  所使用到的Servant
        /// </summary>
        protected static readonly LocalApprovalTodoServant Servant = new LocalApprovalTodoServant();
        protected static readonly LocalApprovalFlowServant LogsServant = new LocalApprovalFlowServant();

        /// <summary>
        ///  用來測試的單據
        /// </summary>
        protected static readonly string DocNo = AS_Doc_Name_Records.First(m => m.Doc_No == "T110").Doc_No;
        protected static readonly string ApplyBranchCode = AS_Users_Records.First(m => m.User_Name == "王勝測").Branch_Code;

        /// <summary>
        /// 各種使用者
        /// </summary>
        protected static readonly int ApplyUser = AS_Users_Records.First(m => m.User_Name == "王勝測").ID;
        protected static readonly int ApprovalUser_1 = AS_Users_Records.First(m => m.User_Name == "李士測").ID;
        protected static readonly int ApprovalUser_2 = AS_Users_Records.First(m => m.User_Name == "宋測試").ID;
        protected static readonly int ApprovalUser_3 = AS_Users_Records.First(m => m.User_Name == "吳孟測").ID;
        protected static readonly int ReassignUser_1= AS_Users_Records.First(m => m.User_Name == "丁測發").ID;
        protected static readonly int ReassignUser_2 = AS_Users_Records.First(m => m.User_Name == "黃測試").ID;


        public Context()
        {
            ResetListener();

            FormNo = LocalApprovalTodoServant.Create(ApplyUser, DocNo, "M200", "Listener");
            FlowOpenID = LocalApprovalTodoServant.Details(FormNo).ID;
        }

        public virtual void Dispose()
        {
            AS_Flow_Open_Log_Records.Delete(m => true);
            AS_Flow_Open_Records.Delete(m => true);

            EventListener.Map.Remove("Listener");
        }


        /// <summary>
        /// 直接簽到剩最後一個
        /// </summary>
        protected void RemainLastOne()
        {
            TestUtility.AssertTrue(LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Listener"), "簽核流程啟動失敗");
            User_1_Approval();
            User_2_Approval();
            TestUtility.AssertTrue(LogsCountEqual(4), "快樂簽核有問題2");
            TestUtility.AssertTrue(LogsServant.Logs(FlowOpenID).Logs.Last().FlowStatus == FlowStateServant.Approval, "簽核態");
        }

        protected void HappyRule()
        {
            RemainLastOne();

            User_3_Approval();
            TestUtility.AssertTrue(LogsCountEqual(5), "快樂簽核有問題2");
            TestUtility.AssertTrue(LogsServant.Logs(FlowOpenID).Logs.Last().FlowStatus == FlowStateServant.Pass, "簽核未放行");
        }

       
        protected void ResetListener()
        {
            EventListener.Map["Listener"] = Listener = new VirtualEventListener();
        }

        protected bool StartFlow(int UserID)
        {
            return LocalApprovalTodoServant.Start(UserID, FormNo, "Listener");
        }

        protected bool ApplyUser_Approval()
        {
            return Servant.Approval(new ApprovalModel { ID = FlowOpenID, UserID = ApplyUser });
        }

        protected bool ApplyUser_Reject()
        {
            return Servant.Reject(new RejectModel { ID = FlowOpenID, UserID = ApplyUser });
        }

        protected bool User_1_Approval()
        {
            return Servant.Approval(new ApprovalModel { ID = FlowOpenID, UserID = ApprovalUser_1 });
        }

        protected bool User_1_Reject()
        {
            return Servant.Reject(new RejectModel { ID = FlowOpenID, UserID = ApprovalUser_1 });
        }

        protected bool User_2_Approval()
        {
            return Servant.Approval(new ApprovalModel { ID = FlowOpenID, UserID = ApprovalUser_2 });
        }

        protected bool User_3_Approval()
        {
            return Servant.Approval(new ApprovalModel { ID = FlowOpenID, UserID = ApprovalUser_3 });
        }

        protected bool User_3_Reject()
        {
            return Servant.Reject(new RejectModel { ID = FlowOpenID, UserID = ApprovalUser_3 });
        }

        protected bool Reassign_User_1_Approval()
        {
            return Servant.Approval(new ApprovalModel { ID = FlowOpenID, UserID = ReassignUser_1 });
        }

        protected int User_3_Can_See()
        {
            return Servant.IndexApproval(new SearchModel { UserID = ApprovalUser_3, Page = 1, PageSize = 1000 }).Page.Items.Count;
        }

        protected int Reassign_User_1_Can_See()
        {
            return Servant.IndexApproval(new SearchModel { UserID = ReassignUser_1, Page = 1, PageSize = 1000 }).Page.Items.Count;
        }

        protected bool LogsCountEqual(int compare)
        {
            return LogsServant.Logs(FlowOpenID).Logs.Count == compare;
        }

        protected int IndexApproval(int UserID, int Page = 1, int PageSize = 1000)
        {
            return Servant.IndexApproval(new SearchModel { UserID = UserID, Page = Page, PageSize = PageSize }).Page.Items.Count;
        }

        protected int IndexWithdraw(int UserID, int Page = 1, int PageSize = 1000)
        {
            return Servant.IndexWithdraw(new SearchModel { UserID = UserID, Page = Page, PageSize = PageSize }).Page.Items.Count;
        }

        protected int IndexReassign(int UserID, int Page = 1, int PageSize = 1000)
        {
            return Servant.IndexReassign(new SearchModel { UserID = UserID, Page = Page, PageSize = PageSize }).Page.Items.Count;
        }

        protected int IndexRevoke(int UserID, int Page = 1, int PageSize = 1000)
        {
            return Servant.IndexRevoke(new SearchModel { UserID = UserID, Page = Page, PageSize = PageSize }).Page.Items.Count;
        }

        protected bool FlowStateEqual(decimal state)
        {
            return Servant.Edit(FlowOpenID).FlowStatus == state &&
                        LogsServant.Logs(FlowOpenID).Logs.Last().FlowStatus == state;
        }

        protected void Reassign(int UserID, int ReassignID, int FlowID)
        {
            Servant.Reassign( new ReassignModel {

                UserID = UserID,
                ReassignUserID = ReassignID,
                FlowID = FlowID
            } );
        }
    }

    class HappyRule : Context, IRunnable
    {
        public void run()
        {
            TestNew();
            TestStart();

            TestApproval_1();
            TestApproval_2();
            TestFinish();
        }

        private void TestNew()
        {
            ResetListener();
            TestUtility.AssertTrue(LogsCountEqual(1), "草稿錯誤");
            TestUtility.AssertFalse(Listener.Finish, "OnFinish事件現階段不會發生");
            TestUtility.AssertFalse(Listener.Approval, "沒簽核不會有OnApproval事件啦");
        }

        private void TestStart()
        {
            ResetListener();
            User_1_Approval();
            TestUtility.AssertTrue(LogsCountEqual(1), "簽核流程尚未起動,不能簽");
            TestUtility.AssertTrue(FlowStateEqual( FlowStateServant.Draft), "必須為草稿態");
            TestUtility.AssertFalse(Listener.Finish, "OnFinish事件現階段不會發生");
            TestUtility.AssertFalse(Listener.Approval, "沒簽核不會有OnApproval事件啦");

            ResetListener();
            TestUtility.AssertTrue(StartFlow(ApplyUser), "簽核流程啟動失敗");
            TestUtility.AssertTrue(LogsCountEqual(2), "簽核流程必須起動");
            TestUtility.AssertTrue(FlowStateEqual( FlowStateServant.New), "必須為新增態");
            TestUtility.AssertFalse(Listener.Finish, "OnFinish事件現階段不會發生");
            TestUtility.AssertFalse(Listener.Approval, "沒簽核不會有OnApproval事件啦");

            ResetListener();
            TestUtility.AssertFalse(StartFlow(ApplyUser), "簽核流程不能被啟動兩次");
            TestUtility.AssertTrue(LogsCountEqual(2), "簽核流程不能被啟動兩次");
            TestUtility.AssertFalse(Listener.Finish, "OnFinish事件現階段不會發生");
            TestUtility.AssertFalse(Listener.Approval, "沒簽核不會有OnApproval事件啦");
        }

        private void TestApproval_1()
        {
            ResetListener();
            User_1_Approval();
            TestUtility.AssertTrue(LogsCountEqual(3), "簽核錯誤1");
            TestUtility.AssertTrue(FlowStateEqual( FlowStateServant.Approval ), "必須為簽核中");
            TestUtility.AssertFalse(Listener.Finish, "OnFinish事件現階段不會發生");
            TestUtility.AssertTrue(Listener.Approval, "OnApproval沒有被呼叫");
            TestUtility.AssertTrue(Listener.Carray == "Listener", "Carry未傳入");

            ResetListener();
            User_1_Approval();
            TestUtility.AssertTrue(LogsCountEqual(3), "簽核角色不符,不能簽");
            TestUtility.AssertFalse(Listener.Finish, "OnFinish事件現階段不會發生");
            TestUtility.AssertFalse(Listener.Approval, "沒簽核不會有OnApproval事件啦");
        }

        private void TestApproval_2()
        {
            ResetListener();
            User_2_Approval();
            TestUtility.AssertTrue(LogsCountEqual(4), "簽核錯誤2");
            TestUtility.AssertTrue(FlowStateEqual( FlowStateServant.Approval ), "必須為簽核中");
            TestUtility.AssertFalse(Listener.Finish, "OnFinish事件現階段不會發生");
            TestUtility.AssertTrue(Listener.Approval, "OnApproval沒有被呼叫");
            TestUtility.AssertTrue(Listener.Carray == "Listener", "Carry未傳入");

            ResetListener();
            User_2_Approval();
            TestUtility.AssertTrue(LogsCountEqual(4), "簽核角色不符,不能簽");
            TestUtility.AssertFalse(Listener.Finish, "OnFinish事件現階段不會發生");
            TestUtility.AssertFalse(Listener.Approval, "沒簽核不會有OnApproval事件啦");
        }

        private void TestFinish()
        {
            ResetListener();
            User_3_Approval();
            TestUtility.AssertTrue(LogsCountEqual(5), "簽核錯誤3");
            TestUtility.AssertTrue(FlowStateEqual( FlowStateServant.Pass ), "必須為放行");
            TestUtility.AssertTrue(Listener.Finish, "OnFinish沒有被呼叫");
            TestUtility.AssertTrue(Listener.Approval, "OnApproval沒有被呼叫");
            TestUtility.AssertTrue(Listener.Carray == "Listener", "Carry未傳入");

            ResetListener();
            User_1_Approval();
            TestUtility.AssertTrue(LogsCountEqual(5), "完成簽核的文件不能再簽");
            TestUtility.AssertFalse(Listener.Finish, "OnFinish不能被呼叫兩次");
            TestUtility.AssertFalse(Listener.Approval, "沒簽核不會有OnApproval事件啦");
            TestUtility.AssertTrue( string.IsNullOrEmpty(Listener.Carray), "沒事件有Carry,怪怪");
        }
    }

    class StartFlow : Context, IRunnable
    {
        public void run()
        {
            TestUtility.AssertFalse( StartFlow(ApprovalUser_1), "只有經辦可以啟動簽核1");
            TestUtility.AssertFalse( StartFlow(ApprovalUser_2), "只有經辦可以啟動簽核2");
            TestUtility.AssertFalse( StartFlow(ApprovalUser_3), "只有經辦可以啟動簽核3");
            TestUtility.AssertTrue(LogsCountEqual(1), "只有經辦可以啟動簽核4");

            TestUtility.AssertTrue( StartFlow(ApplyUser), "經辦可以啟動簽核1");
            TestUtility.AssertTrue(LogsCountEqual(2), "經辦可以啟動簽核2");
        }
    }

    class RevokeDraftFlow : Context, IRunnable
    {
        public void run()
        {
            Servant.Revoke( new RevokeModel { UserID = ApplyUser, ID = FlowOpenID } );
            TestUtility.AssertTrue(LogsCountEqual(2), "草稿可被經辦廢棄");
            TestUtility.AssertTrue(FlowStateEqual( FlowStateServant.Revoke ), "應為廢棄態");
            TestUtility.AssertTrue(Listener.Revoke, "OnRevoke 必須被回呼");
            TestUtility.AssertTrue( string.IsNullOrEmpty( Listener.Carray ), "草稿尚未有Carry");
        }
    }

    class RevokeNewFlow : Context, IRunnable
    {
        public void run()
        {
            TestUtility.AssertTrue(StartFlow(ApplyUser), "簽核流程啟動失敗");
            TestUtility.AssertTrue(LogsCountEqual(2), "簽核流程啟動要寫Log");

            Servant.Revoke(new RevokeModel { UserID = ApprovalUser_2, ID = FlowOpenID });
            Servant.Revoke(new RevokeModel { UserID = ApprovalUser_3, ID = FlowOpenID });
            TestUtility.AssertTrue(LogsCountEqual(2), "下個簽核者可廢棄");
            TestUtility.AssertFalse(Listener.Revoke, "OnRevoke 不會發生");

            Servant.Revoke(new RevokeModel { UserID = ApprovalUser_1, ID = FlowOpenID });
            TestUtility.AssertTrue(LogsCountEqual(3), "被廢棄也要寫Log");
            TestUtility.AssertTrue(FlowStateEqual( FlowStateServant.Revoke ), "應為廢棄態");
            TestUtility.AssertTrue(Listener.Revoke, "OnRevoke 必須被回呼");
            TestUtility.AssertTrue(Listener.Carray == "Listener", "Carry未傳入");
        }
    }

    class RevokeApprovaleFlow : Context, IRunnable
    {
        public void run()
        {
            TestUtility.AssertTrue(StartFlow(ApplyUser), "簽核流程啟動失敗");
            TestUtility.AssertTrue(LogsCountEqual(2), "簽核流程啟動要寫Log");

            User_1_Approval();
            TestUtility.AssertTrue(LogsCountEqual(3), "簽核流程簽核要寫Log");

            Servant.Revoke(new RevokeModel { UserID = ApprovalUser_1, ID = FlowOpenID });
            Servant.Revoke(new RevokeModel { UserID = ApprovalUser_3, ID = FlowOpenID });
            TestUtility.AssertTrue(LogsCountEqual(3), "只有提交人或下個簽核者可廢棄");
            TestUtility.AssertFalse(Listener.Revoke, "OnRevoke 不會發生");

            Servant.Revoke(new RevokeModel { UserID = ApprovalUser_2, ID = FlowOpenID });
            TestUtility.AssertTrue(LogsCountEqual(4), "下個簽核者可廢棄");
            TestUtility.AssertTrue(FlowStateEqual( FlowStateServant.Revoke ), "應為廢棄態");
            TestUtility.AssertTrue(Listener.Revoke, "OnRevoke 必須被回呼");
            TestUtility.AssertTrue(Listener.Carray == "Listener", "Carry未傳入");
        }
    }

    class RevokeApprovaleFlow2 : Context, IRunnable
    {
        public void run()
        {
            TestUtility.AssertTrue(StartFlow(ApplyUser), "簽核流程啟動失敗");
            TestUtility.AssertTrue(LogsCountEqual(2), "簽核流程啟動要寫Log");

            User_1_Approval();
            TestUtility.AssertTrue(LogsCountEqual(3), "簽核流程簽核要寫Log");

            User_2_Approval();
            TestUtility.AssertTrue(LogsCountEqual(4), "簽核流程簽核要寫Log");

            Servant.Revoke(new RevokeModel { UserID = ApprovalUser_1, ID = FlowOpenID });
            Servant.Revoke(new RevokeModel { UserID = ApprovalUser_2, ID = FlowOpenID });
            TestUtility.AssertTrue(LogsCountEqual(4), "只有提交人或下個簽核者可廢棄");

            Servant.Revoke(new RevokeModel { UserID = ApprovalUser_3, ID = FlowOpenID });
            TestUtility.AssertTrue(LogsCountEqual(5), "被廢棄也要寫Log");
            TestUtility.AssertTrue(FlowStateEqual( FlowStateServant.Revoke ), "應為廢棄態");
        }
    }

    class RevokeFinshFlow : Context, IRunnable
    {
        public void run()
        {
            HappyRule();

            Servant.Revoke(new RevokeModel { UserID = ApprovalUser_1, ID = FlowOpenID });
            Servant.Revoke(new RevokeModel { UserID = ApprovalUser_2, ID = FlowOpenID });
            Servant.Revoke(new RevokeModel { UserID = ApprovalUser_3, ID = FlowOpenID });
            Servant.Revoke(new RevokeModel { UserID = ApplyUser, ID = FlowOpenID });
            TestUtility.AssertTrue(LogsCountEqual(5), "已放行不可廢棄");
        }
    }

    class WithdrawNewFlow : Context, IRunnable
    {
        public void run()
        {
            Servant.Withdraw(  new WithdrawModel { UserID  = ApplyUser, ID = FlowOpenID } );
            TestUtility.AssertTrue (LogsCountEqual(1), "新開流程抽回是沒有意義的");
            TestUtility.AssertFalse(Listener.Withdraw, "新開流程不會有OnWithdraw事件啦");
        }
    }

    class WithdrawStartFlow : Context, IRunnable
    {
        public void run()
        {
            TestUtility.AssertTrue( StartFlow(ApplyUser), "簽核流程啟動失敗");
            TestUtility.AssertTrue(LogsCountEqual(2), "簽核流程啟動要寫Log");


            Servant.Withdraw(new WithdrawModel { UserID = ApprovalUser_1, ID = FlowOpenID });
            TestUtility.AssertTrue(LogsCountEqual(2), "只有本人可抽回");
            TestUtility.AssertFalse(Listener.Withdraw, "非本人不會有OnWithdraw事件啦");

            Servant.Withdraw(new WithdrawModel { UserID = ApplyUser, ID = FlowOpenID });
            TestUtility.AssertTrue(LogsCountEqual(3), "抽回也要寫Log");
            TestUtility.AssertTrue(FlowStateEqual( FlowStateServant.Withdraw ), "抽回應為抽回態");
            TestUtility.AssertTrue(Listener.Withdraw, "必須有OnWithdraw事件");
            TestUtility.AssertTrue(Listener.Carray == "Listener", "Carry未傳入");
        }
    }

    class WithdrawApprovalFlow : Context, IRunnable
    {
        public void run()
        {
            TestUtility.AssertTrue(StartFlow(ApplyUser), "簽核流程啟動失敗");
            TestUtility.AssertTrue(LogsCountEqual(2), "簽核流程啟動要寫Log");

            User_1_Approval();
            TestUtility.AssertTrue(LogsCountEqual(3), "簽核流程啟動要寫Log");

            Servant.Withdraw(new WithdrawModel { UserID = ApprovalUser_2, ID = FlowOpenID });
            TestUtility.AssertTrue(LogsCountEqual(3), "只有本人可抽回");
            TestUtility.AssertFalse(Listener.Withdraw, "非本人不會有OnWithdraw事件啦");

            Servant.Withdraw(new WithdrawModel { UserID = ApplyUser, ID = FlowOpenID });
            TestUtility.AssertTrue(LogsCountEqual(4), "抽回也要寫Log");
            TestUtility.AssertTrue(LogsServant.Logs(FlowOpenID).Logs.Last().Status == "抽回", "抽回應為抽回態");
            TestUtility.AssertTrue(Listener.Withdraw, "必須有OnWithdraw事件");
            TestUtility.AssertTrue(Listener.Carray == "Listener", "Carry未傳入");

            TestUtility.AssertFalse(StartFlow(ApprovalUser_1), "非本人不能啟動1");
            TestUtility.AssertTrue(LogsCountEqual(4), "非本人不能啟動2");

            TestUtility.AssertTrue(LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Listener Again"), "抽回可再啟動");
            TestUtility.AssertTrue(LogsCountEqual(5), "簽核流程啟動要寫Log");
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.New), "新增態");

            User_1_Approval();
            User_2_Approval();
            User_3_Approval();
            TestUtility.AssertTrue(LogsCountEqual(8), "簽核流程錯誤");
            TestUtility.AssertTrue(FlowStateEqual( FlowStateServant.Pass ), "簽核未放行");
            TestUtility.AssertTrue(Listener.Carray == "Listener Again", "Carry未傳入");
        }
    }

    class WithdrawFinishFlow : Context, IRunnable
    {
        public void run()
        {
            HappyRule();

            Servant.Withdraw(new WithdrawModel { UserID = ApplyUser, ID = FlowOpenID });
            TestUtility.AssertTrue(LogsCountEqual(5), "放行無法抽回");
        }
    }

    class RejectNewFlow : Context, IRunnable
    {
        public void run()
        {
            ApplyUser_Reject();
            TestUtility.AssertTrue(LogsCountEqual(1), "新開流程退回是沒有意義的");
            TestUtility.AssertTrue(FlowStateEqual( FlowStateServant.Draft ), "新開流程退件是沒有意義的");
            TestUtility.AssertFalse(Listener.Reject, "退件事件不會發生");
        }
    }

    class RejectStartFlow : Context, IRunnable
    {
        public void run()
        {
            TestUtility.AssertTrue(LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Listener"), "簽核流程啟動失敗");
            TestUtility.AssertTrue(LogsCountEqual(2), "簽核流程啟動要寫Log");
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.New), "須為新增態");

            TestUtility.AssertFalse(LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Listener"), "簽核流程不可啟動兩次");
            TestUtility.AssertTrue(LogsCountEqual(2), "簽核流程啟動要寫Log");

            ApplyUser_Approval();
            TestUtility.AssertTrue(LogsCountEqual(2), "只有簽核者有資格退回1");

            User_1_Reject();
            TestUtility.AssertTrue(LogsCountEqual(3), "簽核者可退回原本經辦");
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.Reject), "必須為退件態");
            TestUtility.AssertTrue(Listener.Reject, "退件事件應被回呼");

            TestUtility.AssertFalse(LocalApprovalTodoServant.Start( ApprovalUser_1, FormNo, "Listener"), "非本人不能啟動1");
            TestUtility.AssertTrue(LogsCountEqual(3), "非本人不能啟動2");

            TestUtility.AssertTrue(LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Listener"), "退件可再啟動");
            TestUtility.AssertTrue(LogsCountEqual(4), "簽核流程啟動要寫Log");
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.New), "須為新增態");
        }
    }

    class RejectApprovalFlow : Context, IRunnable
    {
        public void run()
        {
            RemainLastOne();

            User_3_Reject();
            TestUtility.AssertTrue(LogsCountEqual(5), "簽核中可退回原本經辦");
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.Reject), "必須為退件態");
            TestUtility.AssertTrue(Listener.Reject, "退件事件應被回呼");

            TestUtility.AssertTrue(LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Listener"), "退件可再啟動");
            TestUtility.AssertTrue(LogsCountEqual(6), "簽核流程啟動要寫Log");
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.New), "須為新增態");

            User_1_Approval();
            User_2_Approval();
            User_3_Approval();
            TestUtility.AssertTrue(LogsCountEqual(9), "簽核流程錯誤");
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.Pass), "簽核未放行");
        }
    }

    class RejectFinishFlow : Context, IRunnable
    {
        public void run()
        {
            HappyRule();

            ApplyUser_Reject();
            TestUtility.AssertTrue(LogsCountEqual(5), "已放行文件不能退件");
            TestUtility.AssertFalse(Listener.Reject, "退件事件不會再發生");
        }
    }

    class RejectToApproval: Context, IRunnable
    {
        public void run()
        {
            RemainLastOne();
            TestUtility.AssertTrue(LogsCountEqual(4), "最後一關到達不能");

            Servant.RejectTo(new RejectToModel { ID = FlowOpenID, UserID = ApprovalUser_3, RejectToStep = 1 });
            TestUtility.AssertTrue(LogsCountEqual(5), "退回至第一關要寫Log");

            User_1_Approval();
            TestUtility.AssertTrue(LogsCountEqual(6), "第一關重簽");

            User_2_Approval();
            TestUtility.AssertTrue(LogsCountEqual(7), "第二關重簽");

            User_3_Approval();
            TestUtility.AssertTrue(LogsCountEqual(8), "第三關通過");
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.Pass), "須為通過態");

            Servant.RejectTo(new RejectToModel { ID = FlowOpenID, UserID = ApprovalUser_3, RejectToStep = 1 });
            TestUtility.AssertTrue(LogsCountEqual(8), "通過了就不能再退件了");
        }
    }

    class RejectToForbiden : Context, IRunnable
    {
        public void run()
        {
            RemainLastOne();
            TestUtility.AssertTrue(LogsCountEqual(4), "最後一關到達不能");

            Servant.RejectTo(new RejectToModel { ID = FlowOpenID, UserID = ApprovalUser_1, RejectToStep = 1 });
            TestUtility.AssertTrue(LogsCountEqual(4), "非簽核者不能退回");

            Servant.RejectTo(new RejectToModel { ID = FlowOpenID, UserID = ApprovalUser_3, RejectToStep = 3 });
            TestUtility.AssertTrue(LogsCountEqual(4), "不能超前退");
        }
    }

    class RejectToNewFlow : Context, IRunnable
    {
        public void run()
        {
            LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Listener");
            TestUtility.AssertTrue(LogsCountEqual(2), "新開流程不能");

            Servant.RejectTo( new RejectToModel { ID = FlowOpenID, UserID = ApprovalUser_1, RejectToStep = 0 });
            TestUtility.AssertTrue(LogsCountEqual(2), "新開流程退簽是沒有意義的");
        }
    }

    class Ressign : Context, IRunnable
    {
        public void run()
        {
            RemainLastOne();
            TestUtility.AssertTrue(User_3_Can_See() == 1, "Approval_3應該是最後一個簽核者");

            Reassign(ApprovalUser_1, ReassignUser_1, FlowOpenID);
            TestUtility.AssertTrue(LogsCountEqual(4), "只有簽核者可以改派2");

            Reassign(ApprovalUser_3, ReassignUser_1, FlowOpenID);
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.Reassign), "需為改派態");
            TestUtility.AssertTrue(LogsCountEqual(5), "改派需新增Log");
            TestUtility.AssertTrue(User_3_Can_See() == 0, "改派後應該看不見了XD");

            TestUtility.AssertTrue(Reassign_User_1_Can_See() == 1, "改派人應該能看見");
            Reassign_User_1_Approval();
            TestUtility.AssertTrue(LogsCountEqual(6), "改派簽核也要Log");
            TestUtility.AssertTrue(Reassign_User_1_Can_See() == 0, "改派人簽完後應該看不見了");
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.Pass), "需為放行態");

            Console.WriteLine(LocalApprovalTodoServant.Details(FormNo).ApprovalStatus);
        }
    }

    class ReReRessign : Context, IRunnable
    {
        public void run()
        {
            RemainLastTwo();
            Reassign(ApprovalUser_2, ReassignUser_2, FlowOpenID);
            Reassign(ReassignUser_2, ReassignUser_1, FlowOpenID);

            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.Reassign), "需為改派態");
            TestUtility.AssertTrue(LogsCountEqual(5), "改派需新增Log");

            Reassign_User_1_Approval();
            TestUtility.AssertTrue(LogsCountEqual(6), "改派者應可簽核");
            TestUtility.AssertTrue(Reassign_User_1_Can_See() == 0, "改派人簽完後應該看不見了");

            User_3_Approval();
            TestUtility.AssertTrue(LogsCountEqual(7), "改派簽完,下一個為正常流程");
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.Pass), "需為放行態");
        }

        /// <summary>
        /// 簽到剩兩個
        /// </summary>
        private void RemainLastTwo()
        {
            TestUtility.AssertTrue(LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Listener"), "簽核流程啟動失敗");
            Servant.Approval(new ApprovalModel { ID = FlowOpenID, UserID = ApprovalUser_1 });
            TestUtility.AssertTrue(LogsCountEqual(3), "快樂簽核有問題1");
            TestUtility.AssertTrue(LogsServant.Logs(FlowOpenID).Logs.Last().FlowStatus == FlowStateServant.Approval, "簽核態");
        }
    }

    class IndexDraftState : Context, IRunnable
    {
        public void run()
        {
            TestUtility.AssertTrue(IndexApproval(ApplyUser ) == 0, "草稿的單未進簽核流程沒人可以簽1");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_1) == 0, "草稿的單未進簽核流程沒人可以簽2");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_2) == 0, "草稿的單未進簽核流程沒人可以簽3");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_3) == 0, "草稿的單未進簽核流程沒人可以簽4");

            TestUtility.AssertTrue( IndexRevoke(ApplyUser) == 1, "草稿可被經辦廢棄");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_1) == 0, "草稿只有經辦可廢棄1");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_2) == 0, "草稿只有經辦可廢棄2");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_3) == 0, "草稿只有經辦可廢棄3");

            TestUtility.AssertTrue(IndexReassign( ApplyUser) == 0, "草稿的單未進簽核流程沒人可以改派1");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_1 ) == 0, "草稿的單未進簽核流程沒人可以改派2");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_2 ) == 0, "草稿的單未進簽核流程沒人可以改派3");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_3 ) == 0, "草稿的單未進簽核流程沒人可以改派4");

            TestUtility.AssertTrue(IndexWithdraw( ApplyUser ) == 0, "草稿不用抽");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_1 ) == 0, "只有經辦可以抽1");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_2 ) == 0, "只有經辦可以抽2");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_3 ) == 0, "只有經辦可以抽3");
        }
    }

    class IndexReject : Context, IRunnable
    {
        public void run()
        {
            LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Listener");
            User_1_Reject();
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.Reject), "應該可以駁回");

            TestUtility.AssertTrue(IndexApproval(ApplyUser) == 0, "駁回沒人可以簽1");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_1) == 0, "駁回沒人可以簽2");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_2) == 0, "駁回沒人可以簽3");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_3) == 0, "駁回沒人可以簽4");

            TestUtility.AssertTrue(IndexRevoke(ApplyUser) == 1, "駁回經辦可以廢棄");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_1) == 0, "駁回沒人可以廢棄1");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_2) == 0, "駁回沒人可以廢棄2");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_3) == 0, "駁回沒人可以廢棄3");

            TestUtility.AssertTrue(IndexReassign(ApplyUser) == 0, "駁回沒人可以改派1");
            TestUtility.AssertTrue(IndexReassign(ApprovalUser_1) == 0, "駁回沒人可以改派2");
            TestUtility.AssertTrue(IndexReassign(ApprovalUser_2) == 0, "駁回沒人可以改派3");
            TestUtility.AssertTrue(IndexReassign(ApprovalUser_3) == 0, "駁回沒人可以改派4");

            TestUtility.AssertTrue(IndexWithdraw(ApplyUser) == 0, "駁回不用抽");
            TestUtility.AssertTrue(IndexWithdraw(ApprovalUser_1) == 0, "駁回不用抽1");
            TestUtility.AssertTrue(IndexWithdraw(ApprovalUser_2) == 0, "駁回不用抽2");
            TestUtility.AssertTrue(IndexWithdraw(ApprovalUser_3) == 0, "駁回不用抽3");
        }
    }

    class IndexWithdraw : Context, IRunnable
    {
        public void run()
        {
            LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Listener");
            Servant.Withdraw(new WithdrawModel { ID = FlowOpenID, UserID = ApplyUser });
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.Withdraw), "應該可以抽回");

            TestUtility.AssertTrue(IndexApproval(ApplyUser) == 0, "抽回沒人可以簽1");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_1) == 0, "抽回沒人可以簽2");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_2) == 0, "抽回沒人可以簽3");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_3) == 0, "抽回沒人可以簽4");

            TestUtility.AssertTrue(IndexRevoke(ApplyUser) == 1, "抽回經辦可以廢棄");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_1) == 0, "抽回廢棄1");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_2) == 0, "抽回廢棄2");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_3) == 0, "抽回可以廢棄3");

            TestUtility.AssertTrue(IndexReassign(ApplyUser) == 0, "抽回沒人可以改派1");
            TestUtility.AssertTrue(IndexReassign(ApprovalUser_1) == 0, "抽回沒人可以改派2");
            TestUtility.AssertTrue(IndexReassign(ApprovalUser_2) == 0, "抽回沒人可以改派3");
            TestUtility.AssertTrue(IndexReassign(ApprovalUser_3) == 0, "抽回沒人可以改派4");

            TestUtility.AssertTrue(IndexWithdraw(ApplyUser) == 0, "抽回不能再抽1");
            TestUtility.AssertTrue(IndexWithdraw(ApprovalUser_1) == 0, "抽回不能再抽2");
            TestUtility.AssertTrue(IndexWithdraw(ApprovalUser_2) == 0, "抽回不能再抽3");
            TestUtility.AssertTrue(IndexWithdraw(ApprovalUser_3) == 0, "抽回不能再抽4");
        }
    }

        class IndexWithdrawPage : Context, IRunnable
    {
        public void run()
        {
            for(int index=1; index<=15; ++index)
            {
                string FormNo = LocalApprovalTodoServant.Create(ApplyUser, DocNo, "Test500", "Listener");
                LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Data");
            }

            TestUtility.AssertTrue( IndexWithdraw( ApplyUser) == 15, "可抽回的表單總數目不符1");
            TestUtility.AssertTrue(IndexWithdraw(ApplyUser, 1, 5) == 5, "可抽回的表單總數目不符2");
            TestUtility.AssertTrue(IndexWithdraw(ApplyUser, 2, 5) == 5, "可抽回的表單總數目不符3");
            TestUtility.AssertTrue(IndexWithdraw(ApplyUser, 3, 5) == 5, "可抽回的表單總數目不符4");
            TestUtility.AssertTrue(IndexWithdraw(ApplyUser, 4, 5) == 0, "沒有第四頁");
        }
    }

    class IndexNewState : Context, IRunnable
    {
        public void run()
        {
            LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Listener");
            TestUtility.AssertTrue( IndexApproval(ApplyUser) == 0, "不甘你的事1");
            TestUtility.AssertTrue( IndexApproval(ApprovalUser_1) == 1, "第一關你要簽");
            TestUtility.AssertTrue( IndexApproval(ApprovalUser_2) == 0, "不甘你的事2");
            TestUtility.AssertTrue( IndexApproval(ApprovalUser_3) == 0, "不甘你的事3");

            TestUtility.AssertTrue(IndexRevoke(ApplyUser) == 0, "一進簽核流程,經辦就不能廢了");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_1) == 1, "第一關可以廢");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_2) == 0, "不甘你的事5");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_3) == 0, "不甘你的事6");

            TestUtility.AssertTrue(IndexReassign( ApplyUser) == 0, "不甘你的事7");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_1 ) == 1, "第一關可改派");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_2 ) == 0, "不甘你的事8");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_3 ) == 0, "不甘你的事9");

            TestUtility.AssertTrue(IndexWithdraw(ApplyUser) == 1, "經辦可以抽");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_1 ) == 0, "只有經辦可以抽1");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_2 ) == 0, "只有經辦可以抽2");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_3 ) == 0, "只有經辦可以抽3");
        }
    }

    class IndexApprovalState_1 : Context, IRunnable
    {
        public void run()
        {
            LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Listener");
            User_1_Approval();

            TestUtility.AssertTrue(IndexApproval(ApplyUser) == 0, "不甘你的事1");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_1) == 0, "不甘你的事2");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_2) == 1, "第二關你要簽");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_3) == 0, "不甘你的事3");

            TestUtility.AssertTrue(IndexRevoke(ApplyUser) == 0, "一進簽核流程,經辦就不能廢了");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_1) == 0, "不甘你的事4");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_2) == 1, "第二關可以廢");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_3) == 0, "不甘你的事5");

            TestUtility.AssertTrue(IndexReassign( ApplyUser) == 0, "不甘你的事6");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_1 ) == 0, "不甘你的事7");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_2 ) == 1, "第二關可改派");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_3 ) == 0, "不甘你的事8");

            TestUtility.AssertTrue(IndexWithdraw(ApplyUser) == 1, "經辦可以抽");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_1 ) == 0, "只有經辦可以抽1");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_2 ) == 0, "只有經辦可以抽2");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_3 ) == 0, "只有經辦可以抽3");
        }
    }

    class IndexApprovalState_2 : Context, IRunnable
    {
        public void run()
        {
            LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Listener");
            User_1_Approval();
            User_2_Approval();

            TestUtility.AssertTrue(IndexApproval(ApplyUser) == 0, "不甘你的事1");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_1) == 0, "不甘你的事2");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_2) == 0, "不甘你的事3");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_3) == 1, "第三關你要簽");

            TestUtility.AssertTrue(IndexRevoke(ApplyUser) == 0, "一進簽核流程,經辦就不能廢了");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_1) == 0, "不甘你的事4");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_2) == 0, "不甘你的事5");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_3) == 1, "第三關可以廢");

            TestUtility.AssertTrue(IndexReassign( ApplyUser) == 0, "不甘你的事6");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_1 ) == 0, "不甘你的事7");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_2 ) == 0, "不甘你的事8");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_3 ) == 1, "第三關可改派");

            TestUtility.AssertTrue( IndexWithdraw( ApplyUser ) == 1, "經辦可以抽");
            TestUtility.AssertTrue( IndexWithdraw( ApprovalUser_1 ) == 0, "只有經辦可以抽1");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_2 ) == 0, "只有經辦可以抽2");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_3 ) == 0, "只有經辦可以抽3");
        }
    }



    class IndexReassign : Context, IRunnable
    {
        public void run()
        {
            LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Listener");
            Reassign( ApprovalUser_1, ReassignUser_2, FlowOpenID);
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.Reassign), "應該可以重派");

            TestUtility.AssertTrue(IndexApproval(ApplyUser) == 0, "不甘你的事1");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_1) == 0, "不甘你的事2");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_2) == 0, "不甘你的事3");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_3) == 0, "不甘你的事4");
            TestUtility.AssertTrue(IndexApproval(ReassignUser_2) == 1, "改派給你簽");

            TestUtility.AssertTrue(IndexRevoke(ApplyUser) == 0, "不甘你的事5");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_1) == 0, "不甘你的事6");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_2) == 0, "不甘你的事7");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_3) == 0, "不甘你的事8");
            TestUtility.AssertTrue(IndexRevoke(ReassignUser_2) == 1, "你老大你決定");

            TestUtility.AssertTrue(IndexReassign(ApplyUser) == 0, "不甘你的事9");
            TestUtility.AssertTrue(IndexReassign(ApprovalUser_1) == 0, "不甘你的事10");
            TestUtility.AssertTrue(IndexReassign(ApprovalUser_2) == 0, "不甘你的事11");
            TestUtility.AssertTrue(IndexReassign(ApprovalUser_3) == 0, "不甘你的事12");
            TestUtility.AssertTrue(IndexReassign(ReassignUser_2) == 1, "交給你了大大");

            TestUtility.AssertTrue(IndexWithdraw(ApplyUser) == 1, "經辦可抽");
            TestUtility.AssertTrue(IndexWithdraw(ApprovalUser_1) == 0, "不甘你的事12");
            TestUtility.AssertTrue(IndexWithdraw(ApprovalUser_2) == 0, "不甘你的事13");
            TestUtility.AssertTrue(IndexWithdraw(ApprovalUser_3) == 0, "不甘你的事14");
            TestUtility.AssertTrue(IndexWithdraw(ReassignUser_2) == 0, "不甘你的事15");
        }
    }
    
    class IndexPassState : Context, IRunnable
    {
        public void run()
        {
            LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Listener");
            User_1_Approval();
            User_2_Approval();
            User_3_Approval();

            TestUtility.AssertTrue(IndexApproval(ApplyUser) == 0, "已經過關了,簽啥鳥1");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_1) == 0, "已經過關了,簽啥鳥2");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_2) == 0, "已經過關了,簽啥鳥3");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_3) == 0, "已經過關了,簽啥鳥4");

            TestUtility.AssertTrue(IndexRevoke(ApplyUser) == 0, "已經過關了,廢啥鳥1");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_1) == 0, "已經過關了,廢啥鳥2");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_2) == 0, "已經過關了,廢啥鳥3");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_3) == 0, "已經過關了,廢啥鳥4");

            TestUtility.AssertTrue(IndexReassign( ApplyUser) == 0, "已經過關了,改派啥鳥1");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_1 ) == 0, "已經過關了,改派啥鳥2");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_2 ) == 0, "已經過關了,改派啥鳥3");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_3 ) == 0, "已經過關了,改派啥鳥4");

            TestUtility.AssertTrue(IndexWithdraw(ApplyUser) == 0, "已經過關了,抽啥鳥1");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_1 ) == 0, "已經過關了,抽啥鳥2");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_2 ) == 0, "已經過關了,抽啥鳥3");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_3 ) == 0, "已經過關了,抽啥鳥4");
        }
    }

    class IndexRevokeState : Context, IRunnable
    {
        public void run()
        {
            LocalApprovalTodoServant.Start(ApplyUser, FormNo, "Listener");
            Servant.Revoke(  new RevokeModel { UserID = ApprovalUser_1, ID= FlowOpenID } );
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.Revoke), "應該可以廢棄");

            TestUtility.AssertTrue(IndexApproval(ApplyUser) == 0, "廢棄就沒得救1");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_1) == 0, "廢棄就沒得救2");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_2) == 0, "廢棄就沒得救3");
            TestUtility.AssertTrue(IndexApproval(ApprovalUser_3) == 0, "廢棄就沒得救4");

            TestUtility.AssertTrue(IndexRevoke(ApplyUser) == 0, "廢棄就沒得救5");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_1) == 0, "廢棄就沒得救6");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_2) == 0, "廢棄就沒得救7");
            TestUtility.AssertTrue(IndexRevoke(ApprovalUser_3) == 0, "廢棄就沒得救8");

            TestUtility.AssertTrue(IndexReassign( ApplyUser) == 0, "廢棄就沒得救9");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_1 ) == 0, "廢棄就沒得救10");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_2 ) == 0, "廢棄就沒得救11");
            TestUtility.AssertTrue(IndexReassign( ApprovalUser_3 ) == 0, "廢棄就沒得救12");

            TestUtility.AssertTrue(IndexWithdraw(ApplyUser) == 0, "廢棄就沒得救13");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_1 ) == 0, "廢棄就沒得救14");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_2 ) == 0, "廢棄就沒得救15");
            TestUtility.AssertTrue(IndexWithdraw( ApprovalUser_3 ) == 0, "廢棄就沒得救16");
        }
    }



    

    class CarryWithdraw : Context, IRunnable
    {
        public void run()
        {
            Start();
            ReStart();
        }

        private void Start()
        {
            LocalApprovalTodoServant.Start(ApplyUser, FormNo, "123");

            User_1_Approval();
            TestUtility.AssertTrue(Listener.Carray == "123", "123未傳入");
        }

        private void ReStart()
        {
            Servant.Withdraw(new WithdrawModel { ID = FlowOpenID, UserID = ApplyUser });
            LocalApprovalTodoServant.Start(ApplyUser, FormNo, "456");

            User_1_Approval();
            TestUtility.AssertTrue(Listener.Carray == "456", "456未傳入");
        }
    }

    class DifferentBranchApproval : Context, IRunnable
    {
        public DifferentBranchApproval()
        {
            // 調走
            AS_Users_Records.Update(m => m.ID == ApprovalUser_3, entity =>
            {
                entity.Branch_Code = "xxx";
            });
        }

        public override void Dispose()
        {
            // 調回來
            AS_Users_Records.Update(m => m.ID == ApprovalUser_3, entity =>
            {
                entity.Branch_Code = ApplyBranchCode;
            });
            base.Dispose();
        }

        public void run()
        {
            RemainLastOne();

            User_3_Approval();
            TestUtility.AssertFalse(LogsCountEqual(5), "不同分行不能簽01");
            TestUtility.AssertTrue(LogsServant.Logs(FlowOpenID).Logs.Last().FlowStatus == FlowStateServant.Approval, "應該維持簽核態");
        }
    }

    class DifferentBranchApply : Context, IRunnable
    {
        public DifferentBranchApply()
        {
            //調走
            AS_Users_Records.Update(m => m.ID == ApplyUser, entity =>
            {
                entity.Branch_Code = "xxx";
            });
        }

        public override void Dispose()
        {
            // 調回來
            AS_Users_Records.Update(m => m.ID == ApplyUser, entity =>
            {
                entity.Branch_Code = ApplyBranchCode;
            });
            base.Dispose();
        }

        public void run()
        {
            string L_FormNo = LocalApprovalTodoServant.Create(ApplyUser, DocNo, "M200", "Listener");
            int L_FlowOpenID = LocalApprovalTodoServant.Details(L_FormNo).ID;

            TestUtility.AssertTrue(LogsServant.Logs(L_FlowOpenID).Logs.Count == 1, "簽核草稿失敗");
            TestUtility.AssertTrue(LocalApprovalTodoServant.Start(ApplyUser, L_FormNo, "Listener"), "簽核流程啟動失敗");
            TestUtility.AssertTrue(LogsServant.Logs(L_FlowOpenID).Logs.Count == 2, "簽核啟動失敗");


            Servant.Approval(new ApprovalModel { ID = L_FlowOpenID, UserID = ApprovalUser_1 });
            TestUtility.AssertFalse(LogsServant.Logs(L_FlowOpenID).Logs.Count == 3, "不同部門不能簽");
        }
    }

    class DifferentBranchRevoke : Context, IRunnable
    {
        public DifferentBranchRevoke()
        {
            // 調走
            AS_Users_Records.Update(m => m.ID == ApprovalUser_3, entity =>
            {
                entity.Branch_Code = "xxx";
            });
        }

        public override void Dispose()
        {
            // 調回來
            AS_Users_Records.Update(m => m.ID == ApprovalUser_3, entity =>
            {
                entity.Branch_Code = ApplyBranchCode;
            });
            base.Dispose();
        }

        public void run()
        {
            RemainLastOne();

            Servant.Revoke(new RevokeModel { UserID = ApprovalUser_3, ID = FlowOpenID });
            TestUtility.AssertFalse(LogsCountEqual(5), "換分行可以廢棄就倒了1");
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.Approval), "換分行可以廢棄就倒了2");
        }
    }

    class DifferentBranchReject : Context, IRunnable
    {
        public DifferentBranchReject()
        {
            // 調走
            AS_Users_Records.Update(m => m.ID == ApprovalUser_3, entity =>
            {
                entity.Branch_Code = "xxx";
            });
        }

        public override void Dispose()
        {
            // 調回來
            AS_Users_Records.Update(m => m.ID == ApprovalUser_3, entity =>
            {
                entity.Branch_Code = ApplyBranchCode;
            });
            base.Dispose();
        }

        public void run()
        {
            RemainLastOne();

            Servant.Reject(new RejectModel { UserID = ApprovalUser_3, ID = FlowOpenID });
            TestUtility.AssertFalse(LogsCountEqual(5), "換分行可以駁回就倒了1");
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.Approval), "換分行可以駁回就倒了2");
        }
    }


    class DifferentBranchRessign : Context, IRunnable
    {
        public DifferentBranchRessign()
        {
            // 調走
            AS_Users_Records.Update(m => m.ID == ApprovalUser_3, entity =>
            {
                entity.Branch_Code = "xxx";
            });
        }

        public override void Dispose()
        {
            // 調回來
            AS_Users_Records.Update(m => m.ID == ApprovalUser_3, entity =>
            {
                entity.Branch_Code = ApplyBranchCode;
            });
            base.Dispose();
        }

        public void run()
        {
            RemainLastOne();

            Reassign( ApprovalUser_3, ReassignUser_1, FlowOpenID);
            TestUtility.AssertTrue(FlowStateEqual(FlowStateServant.Approval), "不同分行不得改派1");
            TestUtility.AssertTrue(LogsCountEqual(4), "不同分行不得改派2");
        }
    }
}


