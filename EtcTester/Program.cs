﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;

namespace EtcTester
{
    class Data
    {
        public byte[] Value { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            string json = JsonConvert.SerializeObject( new Data { Value = new byte[] { 1, 2, 3, 4, 5, 6, 7 } } );
            byte[] data = JsonConvert.DeserializeObject<Data>(json).Value;

            Console.WriteLine( data.SequenceEqual(new byte[] { 1, 2, 3, 4, 5, 6, 7 }) );
        }
    }
}
