﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

namespace Web.EDOCService
{
    /// <summary>
    ///EDOCService 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
     [System.Web.Script.Services.ScriptService]
    [System.Web.Services.Protocols.SoapDocumentService(RoutingStyle = SoapServiceRoutingStyle.RequestElement)]
    public class EDOCService : System.Web.Services.WebService, IEDOCServiceSoapBinding
    {
        string IEDOCServiceSoapBinding.applyEDOC(string xml)
        {
            string ss = "" +
                "<EDOCAPPLY version = \"1.0\" >" +
                 "<RespMsg Code = \"0\" > 送件成功 </RespMsg >" +
                  "</EDOCAPPLY >";

             //throw new NotImplementedException();
            return ss;
        }

        string IEDOCServiceSoapBinding.applyQuery(string bioid)
        {
            string ss = "" +
              "<?xml version=\"1.0\" encoding=\"Big5\"?>" +
                "<EDOCQuery version=\"1.0\">" +
                "<RespMsg Code=\"0\">查詢成功</RespMsg>" +
                "<Result>" +
                "<EDOCData>" +
                "<ApplyDate>2005/10/31 14:34:57</ApplyDate>" +
                "<City ID=\"376550000A\">花蓮縣</City>" +
                "<Area ID=\"03\">玉里鎮</Area>" +
                "<Section ID=\"0302\">玉昌段</Section>" +
                "<IR00 ID=\"C\">地</IR00>" +
                "<IR49>00010000</IR49>" +
                "<LIDN></LIDN>" +
                "<Status Code=\"D\" Desc=\"已下載\"></Status>" +
                "<GUID>" +
                "094UC006711PIC142F76A8F42E3BE32A29DEF87FDF8</GUID>" +
                "<DocName>094玉里電謄006711</DocName>" +
                "<DocType Code=\"PIC1\">地籍圖謄本</DocType>" +
                "<Page>1</Page>" +
                "<URL>" +
                "http://203.73.41.34:8080/LBWebServices/System/DownloadPDF.jsp?m=5&amp; g=094UC006711PIC142F76A8F42E3BE32A29DEF87FDF8" +
                "</URL>" +
                "<BIOID>9410310010</BIOID>" +
                "<OID></OID>" +
                "<DownloadDate>2005/10/31 14:36:48</DownloadDate>" +
                "<TransCert>http://127.0.0.1:8089/evidence/prn_evi.asp?APPLYCODE=094UC006711</TransCert>" +
                "<ApplyUser ID=\"Test\">測試人員</ApplyUser>" +
                "<ApplyUnit ID=\"SI\">華安聯網</ApplyUnit>" +
                "</EDOCData>" +
                "</Result>" +
                "</EDOCQuery>";

            //throw new NotImplementedException();
            return ss;
        }

        string IEDOCServiceSoapBinding.applyQuery2(string BIOID, string OID, string CityID, string AreaID, string SectionID, string IR00, string IR49, string LIDN, string StartDate, string EndDate, string ApplyUser)
        {
            throw new NotImplementedException();
        }

        string IEDOCServiceSoapBinding.exportEDOCData(string guid)
        {
            string ss = "" +
                "<?xml version=\"1.0\" encoding=\"big5\"?>" +
                "<EDOCExport version=\"1.0\">" +
                "  <RespMsg Code=\"0\">謄本資料匯出成功</RespMsg>" +
                "  <EDOCData>" +
                "    <BuildREG>" +
                "      <City ID=\"379160000A\">臺北市</City>" +
                "      <Area ID=\"11\">文山區</Area>" +
                "      <Section ID=\"0126\">木柵段二小段</Section>" +
                "      <BuildNo>02575000</BuildNo>" +
                "      <RegisterReason Date=\"0841229\" ID=\"xx\">第一次登記</RegisterReason>" +
                "      <TotalAreaSize>19.89</TotalAreaSize>" +
                "      <Address>木新路２段２７號十四樓</Address>" +
                "      <Purpose ID=\"A\">住家用</Purpose>" +
                "      <Material ID=\"04\">鋼筋混凝土造</Material>" +
                "      <BuildLevel>014</BuildLevel>" +
                "      <FinishDate>0841024</FinishDate>" +
                "      <BuildLocation>" +
                "        <Section ID=\"0126\">木柵段二小段</Section>" +
                "        <LandNo>01390000</LandNo>" +
                "      </BuildLocation>" +
                "      <FloorInfo>" +
                "        <BuildType Type=\"M\">建物分層</BuildType>" +
                "        <Purpose ID=\"014\">十四層</Purpose>" +
                "        <AreaSize>19.89</AreaSize>" +
                "      </FloorInfo>" +
                "      <BTOD31>" +
                "        <MainBuildNo SectionID=\"0126\">02575000</MainBuildNo>" +
                "        <CommonUseBuildNo SectionID=\"0126\">02676000</CommonUseBuildNo>" +
                "        <AreaSize>1922.95</AreaSize>" +
                "        <RightsRange>" +
                "          <Denominator>100000</Denominator>" +
                "          <Numerator>180</Numerator>" +
                "        </RightsRange>" +
                "        <OtherRegister ID=\"nn\">XXXXXX</OtherRegister>" +
                "        <ParkInfo>" +
                "          <ParkNum>地下一層59</ParkNum>" +
                "          <RightsRange>" +
                "            <Denominator>100000</Denominator>" +
                "            <Numerator>993</Numerator>" +
                "          </RightsRange>" +
                "        </ParkInfo>" +
                "      </BTOD31>" +
                "      <OtherRegister ID=\"nn\">XXXXXX</OtherRegister>" +
                "      <BTEBOW>" +
                "        <RegisterOrder>0002</RegisterOrder>" +
                "        <RegisterReason Date=\"0940816\" HDate=\"0940816\" ID=\"xx\">買賣</RegisterReason>" +
                "        <Owner ID=\"A221822XXX\">林X秀</Owner>" +
                "        <OwnerAddress></OwnerAddress>" +
                "        <RightsRange>" +
                "          <RightsReason></RightsReason>" +
                "          <Denominator>1</Denominator>" +
                "          <Numerator>1</Numerator>" +
                "        </RightsRange>" +
                "        <B16>091北重建字第019867號</B16>" +
                "        <ORRegisterOrder>0002000</ORRegisterOrder>" +
                "        <OtherRegister ID=\"nn\">XXXXXX</OtherRegister>" +
                "      </BTEBOW>" +
                "      <BTCLOR>" +
                "        <RegisterOrder Order=\"1\">0002000</RegisterOrder>" +
                "        <ReceiveNumber>091壢登字第062270號</ReceiveNumber>" +
                "        <RegisterReason Date=\"0940816\" ID=\"xx\">設定</RegisterReason>" +
                "        <Obligee ID=\"86521704\">XXX銀行股份有限公司</Obligee>" +
                "        <ObligeeAddress></ObligeeAddress>" +
                "        <RightsValueRange>" +
                "          <Denominator>1</Denominator>" +
                "          <Numerator>1</Numerator>" +
                "        </RightsValueRange>" +
                "        <TargetRegisterOrder></TargetRegisterOrder>" +
                "        <RightsTargetType ID=\"A\">所有權</RightsTargetType>" +
                "        <RightsRange>" +
                "          <Denominator>1</Denominator>" +
                "          <Numerator>1</Numerator>" +
                "        </RightsRange>" +
                "        <C16>095北板他字第002788號</C16>" +
                "        <Debtor>林X秀</Debtor>" +
                "        <SetObligor>林X秀</SetObligor>" +
                "        <RightsType ID=\"B\">抵押權</RightsType>" +
                "        <RightsValueType type=\"C\">本金最高限額新台幣</RightsValueType>" +
                "        <RightsValue>2640000</RightsValue>" +
                "        <RightsKind>OOOOOOOOO</RightsKind>" +
                "        <ExistPeriod>" +
                "          <Start>0940805</Start>" +
                "          <End>1240804</End>" +
                "        </ExistPeriod>" +
                "        <DischargeDate></DischargeDate>" +
                "        <Interest></Interest>" +
                "        <DelayInterest>" +
                "      </DelayInterest >" +
                "        <Penalty></Penalty>" +
                "        <RightsDate>OOOOOOOOO</RightsDate>" +
                "        <RightsOtherKind></RightsOtherKind>" +
                "        <RightsQuota></RightsQuota>" +
                "        <RightsPromise></RightsPromise>" +
                "        <BTTOGHC>" +
                "          <Section ID=\"0126\">木柵段二小段</Section>" +
                "          <LandNo>06310000</LandNo>" +
                "          <BuildNo>06310000</BuildNo>" +
                "        </BTTOGHC>" +
                "        <OtherRegister ID=\"nn\">XXXXXX</OtherRegister>" +
                "        <UsufructuarySet></UsufructuarySet>" +
                "        <UsufructuaryRent></UsufructuaryRent>" +
                "        <UsufructuaryUse></UsufructuaryUse>" +
                "        <UsufructuaryLease></UsufructuaryLease>" +
                "        <UsufructuarySale></UsufructuarySale>" +
                "      </BTCLOR>" +
                "    </BuildREG>" +
                "    <LandREG>" +
                "      <City ID=\"379160000A\">臺北市</City>" +
                "      <Area ID=\"11\">文山區</Area>" +
                "      <Section ID=\"0126\">木柵段二小段</Section>" +
                "      <LandNo>02310000</LandNo>" +
                "      <RegisterReason Date=\"0841108\" ID=\"xx\">合併</RegisterReason>" +
                "      <OutLine ID=\"B\">田</OutLine>" +
                "      <UseType ID=\"EE\">農牧用地</UseType>" +
                "      <AreaSize>4530.00</AreaSize>" +
                "      <UseArea ID=\"AF\">山坡地保育區</UseArea>" +
                "      <PublicValue Date=\"09501\">104416</PublicValue>" +
                "      <FloorBuildingSum></FloorBuildingSum>" +
                "      <FloorBuilding>" +
                "        <Section ID=\"0126\">木柵段二小段</Section>" +
                "        <BuildNo>01390000</BuildNo>" +
                "      </FloorBuilding>" +
                "      <OtherRegister ID=\"nn\">XXXXXX</OtherRegister>" +
                "      <BTBLOW>" +
                "        <RegisterOrder>0342</RegisterOrder>" +
                "        <RegisterReason Date=\"0940816\" HDate=\"0940816\" ID=\"xx\">買賣</RegisterReason>" +
                "        <Owner ID=\"A221822XXX\">林X秀</Owner>" +
                "        <OwnerAddress></OwnerAddress>" +
                "        <RightsRange>" +
                "          <RightsReason></RightsReason>" +
                "          <Denominator>10000</Denominator>" +
                "          <Numerator>18</Numerator>" +
                "        </RightsRange>" +
                "        <B16>091北重建字第019867號</B16>" +
                "        <DeclaredValue Date=\"09301\">29362.4</DeclaredValue>" +
                "        <ORRegisterOrder>0372000</ORRegisterOrder>" +
                "        <BTPRCE>" +
                "          <LastDate>09407</LastDate>" +
                "          <LastValue>96018.0</LastValue>" +
                "          <Denominator>10000</Denominator>" +
                "          <Numerator>18</Numerator>" +
                "        </BTPRCE>" +
                "        <OtherRegister ID=\"nn\">XXXXXX</OtherRegister>" +
                "      </BTBLOW>" +
                "      <BTCLOR>" +
                "        <RegisterOrder Order=\"1\">0002000</RegisterOrder>" +
                "        <ReceiveNumber>壢登字第062270號</ReceiveNumber>" +
                "        <RegisterReason Date=\"0940816\" ID=\"xx\">設定</RegisterReason>" +
                "        <Obligee ID=\"86521704\">XXX銀行股份有限公司</Obligee>" +
                "        <ObligeeAddress></ObligeeAddress>" +
                "        <RightsValueRange>" +
                "          <Denominator>1</Denominator>" +
                "          <Numerator>1</Numerator>" +
                "        </RightsValueRange>" +
                "        <TargetRegisterOrder></TargetRegisterOrder>" +
                "        <RightsTargetType ID=\"A\">所有權</RightsTargetType>" +
                "        <RightsRange>" +
                "          <Denominator>1</Denominator>" +
                "          <Numerator>1</Numerator>" +
                "        </RightsRange>" +
                "        <C16>095北板他字第002788號</C16>" +
                "        <Debtor>林X秀</Debtor>" +
                "        <SetObligor>林X秀</SetObligor>" +
                "        <RightsType ID=\"B\">抵押權</RightsType>" +
                "        <RightsValueType type=\"C\">本金最高限額新台幣</RightsValueType>" +
                "        <RightsValue>2640000</RightsValue>" +
                "        <RightsKind>OOOOOOOOO</RightsKind>" +
                "        <ExistPeriod>" +
                "          <Start>0940805</Start>" +
                "          <End>1240804</End>" +
                "        </ExistPeriod>" +
                "        <DischargeDate>" +
                "      </DischargeDate>" +
                "        <Interest></Interest>" +
                "        <DelayInterest>" +
                "      </DelayInterest >" +
                "        <Penalty></Penalty>" +
                "        <RightsDate>OOOOOOOOO</RightsDate>" +
                "        <RightsOtherKind></RightsOtherKind>" +
                "        <RightsQuota></RightsQuota>" +
                "        <RightsPromise></RightsPromise>" +
                "        <BTTOGHC>" +
                "          <Section ID=\"0126\">木柵段二小段</Section>" +
                "          <LandNo>06310000</LandNo>" +
                "          <BuildNo>06310000</BuildNo>" +
                "        </BTTOGHC>" +
                "        <OtherRegister ID=\"nn\">XXXXXX</OtherRegister>" +
                "        <UsufructuarySet></UsufructuarySet>" +
                "        <UsufructuaryRent></UsufructuaryRent>" +
                "        <UsufructuaryUse></UsufructuaryUse>" +
                "        <UsufructuaryRang></UsufructuaryRang>" +
                "        <UsufructuaryLease></UsufructuaryLease>" +
                "        <UsufructuarySale></UsufructuarySale>" +
                "      </BTCLOR>" +
                "    </LandREG>" +
                "    <LandREG>" +
                "      <City ID=\"379160000A\">臺北市</City>" +
                "      <Area ID=\"11\">文山區</Area>" +
                "      <Section ID=\"0126\">木柵段二小段</Section>" +
                "      <LandNo>02580000</LandNo>" +
                "      <RegisterReason Date=\"0841108\" ID=\"xx\">合併</RegisterReason>" +
                "      <OutLine ID=\"B\">田</OutLine>" +
                "      <UseType ID=\"EE\">農牧用地</UseType>" +
                "      <AreaSize>4530.00</AreaSize>" +
                "      <UseArea ID=\"AF\">山坡地保育區</UseArea>" +
                "      <PublicValue Date=\"09501\">104416</PublicValue>" +
                "      <FloorBuildingSum></FloorBuildingSum>" +
                "      <FloorBuilding>" +
                "        <Section ID=\"0126\">木柵段二小段</Section>" +
                "        <BuildNo>01390000</BuildNo>" +
                "      </FloorBuilding>" +
                "      <OtherRegister ID=\"nn\">XXXXXX</OtherRegister>" +
                "      <BTBLOW>" +
                "        <RegisterOrder>0342</RegisterOrder>" +
                "        <RegisterReason Date=\"0940816\" HDate=\"0940816\" ID=\"xx\">買賣</RegisterReason>" +
                "        <Owner ID=\"A221822XXX\">林X秀</Owner>" +
                "        <OwnerAddress></OwnerAddress>" +
                "        <RightsRange>" +
                "          <RightsReason></RightsReason>" +
                "          <Denominator>10000</Denominator>" +
                "          <Numerator>18</Numerator>" +
                "        </RightsRange>" +
                "        <B16>091北重建字第019867號</B16>" +
                "        <DeclaredValue Date=\"09301\">29362.4</DeclaredValue>" +
                "        <ORRegisterOrder>0372000</ORRegisterOrder>" +
                "        <BTPRCE>" +
                "          <LastDate>09407</LastDate>" +
                "          <LastValue>96018.0</LastValue>" +
                "          <Denominator>10000</Denominator>" +
                "          <Numerator>18</Numerator>" +
                "        </BTPRCE>" +
                "        <OtherRegister ID=\"nn\">XXXXXX</OtherRegister>" +
                "      </BTBLOW>" +
                "      <BTCLOR>" +
                "        <RegisterOrder Order=\"1\">0002000</RegisterOrder>" +
                "        <ReceiveNumber>壢登字第062270號</ReceiveNumber>" +
                "        <RegisterReason Date=\"0940816\" ID=\"xx\">設定</RegisterReason>" +
                "        <Obligee ID=\"86521704\">XXX銀行股份有限公司</Obligee>" +
                "        <ObligeeAddress></ObligeeAddress>" +
                "        <RightsValueRange>" +
                "          <Denominator>1</Denominator>" +
                "          <Numerator>1</Numerator>" +
                "        </RightsValueRange>" +
                "        <TargetRegisterOrder></TargetRegisterOrder>" +
                "        <RightsTargetType ID=\"A\">所有權</RightsTargetType>" +
                "        <RightsRange>" +
                "          <Denominator>1</Denominator>" +
                "          <Numerator>1</Numerator>" +
                "        </RightsRange>" +
                "        <C16>095北板他字第002788號</C16>" +
                "        <Debtor>林X秀</Debtor>" +
                "        <SetObligor>林X秀</SetObligor>" +
                "        <RightsType ID=\"B\">抵押權</RightsType>" +
                "        <RightsValueType type=\"C\">本金最高限額新台幣</RightsValueType>" +
                "        <RightsValue>2640000</RightsValue>" +
                "        <RightsKind>OOOOOOOOO</RightsKind>" +
                "        <ExistPeriod>" +
                "          <Start>0940805</Start>" +
                "          <End>1240804</End>" +
                "        </ExistPeriod>" +
                "        <DischargeDate>" +
                "      </DischargeDate>" +
                "        <Interest></Interest>" +
                "        <DelayInterest>" +
                "      </DelayInterest >" +
                "        <Penalty></Penalty>" +
                "        <RightsDate>OOOOOOOOO</RightsDate>" +
                "        <RightsOtherKind></RightsOtherKind>" +
                "        <RightsQuota></RightsQuota>" +
                "        <RightsPromise></RightsPromise>" +
                "        <BTTOGHC>" +
                "          <Section ID=\"0126\">木柵段二小段</Section>" +
                "          <LandNo>06310000</LandNo>" +
                "          <BuildNo>06310000</BuildNo>" +
                "        </BTTOGHC>" +
                "        <OtherRegister ID=\"nn\">XXXXXX</OtherRegister>" +
                "        <UsufructuarySet></UsufructuarySet>" +
                "        <UsufructuaryRent></UsufructuaryRent>" +
                "        <UsufructuaryUse></UsufructuaryUse>" +
                "        <UsufructuaryRang></UsufructuaryRang>" +
                "        <UsufructuaryLease></UsufructuaryLease>" +
                "        <UsufructuarySale></UsufructuarySale>" +
                "      </BTCLOR>" +
                "    </LandREG>" +
                "  </EDOCData>" +
                "</EDOCExport>";

            //throw new NotImplementedException();
            return ss;
        }

        string IEDOCServiceSoapBinding.getCity()
        {
            throw new NotImplementedException();
        }

        string IEDOCServiceSoapBinding.getSection(string cityID)
        {
            throw new NotImplementedException();
        }

        string IEDOCServiceSoapBinding.getVersion()
        {
            throw new NotImplementedException();
        }
    }
}
