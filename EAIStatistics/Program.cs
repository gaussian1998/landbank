﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Xml;

namespace EAIStatistics
{
    static class Program
    {
        static readonly string DataSource = "c:/bt/send.txt";

        static void Main(string[] args)
        {
            Dictionary<string, HashSet<string>> statistics = new Dictionary<string, HashSet<string>>();

            statistics.Collect( AllTags() );
            statistics.Print();
        }

        static void Print(this Dictionary<string, HashSet<string>> statistics)
        {
            foreach (var tag in statistics)
            {
                Console.WriteLine(tag.Key + ": 共 " + tag.Value.Count + " 個");
                if(tag.Value.Count<=300)
                {
                    foreach( var value in tag.Value)
                    {
                        Console.Write( string.IsNullOrEmpty(value) ? "(空)" : value );
                        Console.Write("  ");
                    }
                }
                else
                {
                    Console.Write("太多不列舉");
                }
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
            }
        }

        static List<string> AllTags()
        {
            HashSet<string> result = new HashSet<string>();
            Parser(DataSource).ForEach(token => {

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(token);

                var nodes = doc.SelectNodes("CommMsg/*");
                for(int index=0; index<nodes.Count; ++index)
                    result.Add(nodes[index].Name);
            });

            return result.ToList();
        }

        static void Collect(this Dictionary<string, HashSet<string>> statistics, List<string> tags)
        {
            Parser(DataSource).ForEach(token => {

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(token);

                foreach (var tag in tags)
                    statistics.Collect(doc, tag);
            });
        }

        static void Collect(this Dictionary<string, HashSet<string>> statistics, XmlDocument doc, string tag)
        {
            var node = doc.SelectSingleNode("CommMsg/" + tag );
            if (statistics.ContainsKey(tag) == false)
                statistics[tag] = new HashSet<string>();

            if  (node!=null)
                statistics[tag].Add(node.InnerText);
        }

        static List<string> Parser(string path)
        {
            List<string> result = new List<string>();

            {
                StringBuilder buffer = new StringBuilder();
                foreach (var line in File.ReadLines(path))
                {
                    buffer.Append(line);
                    if (line.StartsWith("</CommMsg>"))
                    {
                        result.Add(buffer.ToString());
                        buffer.Clear();
                    }
                }
            }
            return result;
        }
    }
}
