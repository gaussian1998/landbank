﻿using System;
using Library.Common.Utility;
using Library.Utility;
using Library.Servant.Servant.EAI;
using Library.Servant.Servant.EAI.ResponseModels;
using Library.Servant.Servant.EAI.TemplateMethod;

namespace EAITester
{
    public class Context
    {
        static void Main(string[] args)
        {
            try
            {
                TestSuit.Run<
                    ClientDt, DesMethod, 
                    GenerateOldPassword, GenerateMacKey, 
                    EAIResponse>();

                TestSuit.Run<ExchangeKeyMethod, C1100_Method, J1400_Method, A1202_2_Method>();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("測試完成2");
            Console.ReadLine();
        }

        /// <summary>
        ///  測試樣本
        /// </summary>
        public static readonly string TestSuitSignKey = "000A27D229DCFFF6";
        public static readonly string EAIGeneratePasswordExample = "B965E3C2FCFBF983";
        public static readonly string EAIGenerateMacKeyExample =  "B492884853407FA4";

        /// <summary>
        ///  以下兩個參數需要視測試機環境設定
        /// </summary>
        
        public static readonly string IBSRNUM1 = "823119";//收付總號範圍823000~823499
        public static readonly string KINBR = "041";
        public static string LastMacKey = string.Empty;
    }

    class ClientDt : IDisposable, IRunnable
    {
        public void run()
        {
            TestUtility<ClientDt>.AssertTrue(EAIUtility.ClientDt(new DateTime(2017, 9, 6, 11, 36, 55)) == "2017/09/06 11:36:55", 1);
            TestUtility<ClientDt>.AssertTrue(EAIUtility.ClientDt(new DateTime(2016, 5, 6, 23, 20, 05)) == "2016/05/06 23:20:05", 2);
        }

        public void Dispose()
        {}
    }

    class DesMethod : IDisposable, IRunnable
    {
        public void run()
        {
            TestUtility<DesMethod>.AssertTrue(EAIUtility.DesEncrypt(Context.TestSuitSignKey, "F20101125164724F") == "AE0277EC22EDC6F7", 1);
            TestUtility<DesMethod>.AssertTrue(EAIUtility.DesEncrypt("000A27D2000A27D2", "000A27D2000A27D2") == "3A01193BAC0DB47C", 2);
            TestUtility<DesMethod>.AssertTrue(EAIUtility.DesEncrypt("29DCFFF629DCFFF6", "29DCFFF629DCFFF6") == "3A1E351EF0DCE6DD", 3);
            TestUtility<DesMethod>.AssertTrue(EAIUtility.DesDecrypt(Context.TestSuitSignKey, "B965E3C2FCFBF983") == "B492884853407FA4", 4);
        }

        public void Dispose()
        {}
    }

    class GenerateOldPassword : IDisposable, IRunnable
    {
        public void run()
        {
            TestUtility<GenerateOldPassword>.AssertTrue(EAIUtility.SeedDt(sample_dt_1)=="F20170906114409F", 1);
            TestUtility<GenerateOldPassword>.AssertTrue(EAIUtility.GenerateOldPassword(Context.TestSuitSignKey, sample_dt_1) == "0D4BC2B3030E8063", 2);
            TestUtility<GenerateOldPassword>.AssertTrue(EAIUtility.GenerateOldPassword(Context.TestSuitSignKey, sample_dt_2) == "84059BA13CA327FE", 3);
        }

        public void Dispose()
        {}

        private DateTime sample_dt_1 = new DateTime(2017, 9, 6, 11, 44, 09);
        private DateTime sample_dt_2 = new DateTime(2017, 9, 6, 12, 05, 30);
    }

    class GenerateMacKey : IDisposable, IRunnable
    {
        public void run()
        {
            TestUtility<GenerateOldPassword>.AssertTrue( EAIUtility.GenerateMacKey(Context.TestSuitSignKey, Context.EAIGeneratePasswordExample) == Context.EAIGenerateMacKeyExample, 1);
        }

        public void Dispose()
        { }
    }

    class EAIResponse : IDisposable, IRunnable
    {
        public void run()
        {
            EAIResponseResult result = new EAIResponseResult();

            result.XML = testResponse.XML;

            TestUtility<EAIResponse>.AssertTrue(result.StatusCode == "1", 1);
            TestUtility<EAIResponse>.AssertTrue(result.StatusDesc == "Bad", 2);
            TestUtility<EAIResponse>.AssertTrue(result.StatusDetail == "SOS", 3);
            TestUtility<EAIResponse>.AssertTrue(result.SPName == "lol", 4);
            TestUtility<EAIResponse>.AssertTrue(result.CustLoginId == "QooLoginAccount", 5);
            TestUtility<EAIResponse>.AssertTrue(result.ServerDt == "Good day too...", 6);
            TestUtility<EAIResponse>.AssertTrue(result.NewPswd == "12345678", 7);
            TestUtility<EAIResponse>.AssertTrue(result.ExpireDt == "Nerver", 8);

            TestUtility<EAIResponse>.AssertTrue(result.CommMsg_StatusCode == "1980", 9);
            TestUtility<EAIResponse>.AssertTrue(result.CommMsg_StatusDesc == "測試", 10);
            TestUtility<EAIResponse>.AssertTrue(result.CommMsg_StatusDetail == "就是測試", 11);
            TestUtility<EAIResponse>.AssertTrue(result.Severity == "Err", 12);
        }

        public void Dispose()
        { }

        private readonly EAIResponseResult testResponse = new EAIResponseResult
        {
            StatusCode = "1",
            StatusDesc = "Bad",
            StatusDetail = "SOS",
            SPName = "lol",
            CustLoginId = "QooLoginAccount",
            ClientDt = "Today is good day",
            ServerDt = "Good day too...",
            NewPswd = "12345678",
            ExpireDt = "Nerver",
            CommMsg_StatusCode = "1980",
            CommMsg_StatusDesc = "測試",
            CommMsg_StatusDetail = "就是測試",
            Severity = "Err"
        };
    }

    class ExchangeKeyMethod : ExchangeKeyTemplateMethod, IDisposable, IRunnable
    {
        public void run()
        {
            var result = this.Call();
            TestUtility.AssertTrue( result, result.ErrorMessage);
            Console.WriteLine( "Generate Password = " + result.Value );

            Context.LastMacKey = result.Value;
        }


        public void Dispose()
        { }
    }


    class C1100_Method : CommMsgTemplateMethod, IDisposable, IRunnable
    {
        public void run()
        {
            var result  = this.Call();
            Console.WriteLine( result.CommMsg_StatusCode );
            Console.WriteLine( result.CommMsg_StatusDesc);
            Console.WriteLine(result.CommMsg_StatusDetail);
            Console.WriteLine(result.Severity);

            TestUtility.AssertTrue(result.CommMsg_StatusCode == "0", "C1100 " +  result.CommMsg_StatusDesc);
        }

        public void Dispose()
        { }

        public override string MacKey()
        {
            return Context.LastMacKey;
        }

        public override string RqUID()
        {
            return Guid.NewGuid().ToString();
        }

        public override string MsgId()
        {
            return "C1100";//帳務主機開機交易
        }

        public override string TXTNO()
        {
            return DateTime.Now.ToString("yyMMddHHmmss");
        }

        public override string KINBR()
        {
            return Context.KINBR;
        }

        public override string TRMSEQ()
        {
            return "1543";
        }

        public override string TLRNO()
        {
            return "082353";
        }
    }

    class J1400_Method : CommMsgTemplateMethod, IDisposable, IRunnable
    {
        public void run()
        {
            var result = this.Call();
            Console.WriteLine(result.CommMsg_StatusCode);
            Console.WriteLine(result.CommMsg_StatusDesc);
            Console.WriteLine(result.CommMsg_StatusDetail);
            Console.WriteLine(result.Severity);

            TestUtility.AssertTrue(result.CommMsg_StatusCode == "0", "J1400 " + result.CommMsg_StatusDesc);
        }

        public void Dispose()
        { }

        public override string MacKey()
        {
            return Context.LastMacKey;
        }

        public override string RqUID()
        {
            return Guid.NewGuid().ToString();
        }

        public override string MsgId()
        {
            return "J1400";//自動化設備聯行帳交易
        }

        public override string TXTNO()
        {
            return DateTime.Now.AddSeconds(1).ToString("yyMMddHHmmss");
        }

        public override string KINBR()
        {
            return Context.KINBR;
        }

        public override string TRMSEQ()
        {
            return "1543";
        }

        public override string TLRNO()
        {
            return "082353";
        }

        /*public override string APTYPE()
        {
            return "J";
        }*/

        /*public override string PTYPE()
        {
            return "4";
        }*/

        public override string TXNO()
        {
            return "14";
        }

        public override string STXNO()
        {
            return "00";
        }
        
        public override string TXTYPE()
        {
            return "H";
        }

        public override string CRDB()
        {
            return "1";
        }

        public override string HCODE()
        {
            return "0";
        }

        public override string YCODE()
        {
            return "";
        }

        public override string CURCD()
        {
            return "0";
        }

        public override string TXAMT()
        {
            return "0";
        }

        public override string MRKEY()
        {
            return "07205801";
        }

        public override string TITA_TXDAY()
        {
            return "20170920";
        }

        public override string TITA_TXTIME()
        {
            return "11021011";
        }

        public override string ISDAY()
        {
            return "1060920";
        }

        public override string CNT()
        {
            return "1";
        }

        // 這個參數不能重複,會回返已有該筆交易紀錄
        public override string IBSRNUM1()
        {
            return Context.IBSRNUM1;
        }

        public override string IDSCPT1()
        {
            return "CHAS";
        }

        public override string ITXAMT1()
        {
            return "125729";
        }

        public override string DEPNO()
        {
            return "00";
        }

        public override string ACCNO()
        {
            return "1405049900";
        }
    }

    class A1202_2_Method : CommMsgTemplateMethod, IDisposable, IRunnable
    {
        public void run()
        {
            var result = this.Call();
            Console.WriteLine(result.CommMsg_StatusCode);
            Console.WriteLine(result.CommMsg_StatusDesc);
            Console.WriteLine(result.CommMsg_StatusDetail);
            Console.WriteLine(result.Severity);

            TestUtility.AssertTrue(result.CommMsg_StatusCode == "0", "A1202_2 " + result.CommMsg_StatusDesc);
        }

        public void Dispose()
        { }

        public override string MacKey()
        {
            //return "8B431FB213B37D51";
            return Context.LastMacKey;
        }

        public override string RqUID()
        {
            return Guid.NewGuid().ToString();
        }

        public override string MsgId()
        {
            return "A1012_2";//傳票登錄交易
        }

        public override string SPName()
        {
            return "AS";
        }


        public override string TXTNO()
        {
            //return "15430051";
            return DateTime.Now.AddSeconds(2).ToString("yyMMddHHmmss");
        }

        public override string KINBR()
        {
            return Context.KINBR;
        }

        public override string TRMSEQ()
        {
            return "1543";
        }

        public override string TLRNO()
        {
            return "";
        }
        /*
        public override string APTYPE()
        {
            return "A";
        }

        public override string PTYPE()
        {
            return "6";
        }

        public override string TXNO()
        {
            return "10";
        }

        public override string STXNO()
        {
            return "12";
        }



        public override string TXTYPE()
        {
            return "H";
        }

        public override string HCODE()
        {
            return "0";
        }*/

        public override string MRKEY()
        {
            return "041";
        }

        public override string CNT()
        {
            return "1";
        }

        public override string CRDB()
        {
            return "1";
        }/*

        public override string TITA_TXDAY()
        {
            return "20171015";
        }

        public override string TITA_TXTIME()
        {
            return "14055464";
        }*/

        public override string DEPNO()
        {
            return "00";
        }

        public override string ACCNO()
        {
            return "1405049900";
        }
    }
}