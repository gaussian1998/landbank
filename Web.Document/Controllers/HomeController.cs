﻿using System.Web.Mvc;

namespace Web.Document.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "基礎架構";
            return View();
        }

        public ActionResult Approval()
        {
            ViewBag.Title = "簽核api";
            return View();
        }

        public ActionResult Accounting()
        {
            ViewBag.Title = "帳務";
            return View();
        }

        public ActionResult CheckList()
        {
            ViewBag.Title = "檢核表";
            return View();
        }

        public ActionResult EAI()
        {
            ViewBag.Title = "EAI界接";
            return View();
        }

        public ActionResult Adws()
        {
            ViewBag.Title = "Adws界接與登入架構";
            return View();
        }

        public ActionResult Email()
        {
            ViewBag.Title = "Email界接";
            return View();
        }

        public ActionResult FileSystem()
        {
            ViewBag.Title = "檔案上傳";
            return View();
        }

        public ActionResult UpdateSOP()
        {
            ViewBag.Title = "土銀上版SOP";
            return View();
        }

        public ActionResult Vue()
        {
            ViewBag.Title = "Vue.js指導手冊";
            return View();
        }

        public ActionResult Scan()
        {
            ViewBag.Title = "原碼掃描";
            return View();
        }

        public ActionResult  Layer3()
        {
            ViewBag.Title = "三層式架構";
            return View();
        }


        public ActionResult VersionControlFlow()
        {
            ViewBag.Title = "版控流程";
            return View();
        }
    }
}