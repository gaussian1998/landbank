﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.Common.Utility;
using Library.Entity.Edmx;
using Library.Servant.Servant.FileServant;


namespace FileSystemTester
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TestSuit.Run<NoSuchFile, Create, CreateDirectory, RW> ();
                TestSuit.Run<NoSuchFile_R, Create_R, CreateDirectory_R, RW_R> ();
                TestSuit.Run<CreateBigFile>();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    class NoSuchFile : IDisposable, IRunnable
    {
        public void run()
        {
            var servant = new LocalFileServant();
            TestUtility<NoSuchFile>.AssertExceptionHappen(  () => servant.Open("not_exist.png"), 0);
        }

        public void Dispose()
        {
            AS_File_System_Records.Delete( m => true );
        }
    }

    class Create : IDisposable, IRunnable
    {
        public void run()
        {
            IFileServant servant = new LocalFileServant();

            TestUtility<Create>.AssertNoException( () => servant.Create("not_exist.png"), 0 ) ;
            TestUtility<Create>.AssertNoException( () => servant.Open("not_exist.png") , 1 );
        }

        public void Dispose()
        {
            AS_File_System_Records.Delete(m => true);
        }
    }

    class CreateDirectory : IDisposable, IRunnable
    {
        public void run()
        {
            IFileServant servant = new LocalFileServant();
            servant.Create("image/1.png");

            TestUtility<CreateDirectory>.AssertNoException( () => servant.Open("image"), 1);
            TestUtility<CreateDirectory>.AssertNoException( () => servant.Open("image/1.png"), 2);
            TestUtility<CreateDirectory>.AssertExceptionHappen( () => servant.Open("1.png"), 3);
        }

        public void Dispose()
        {
            AS_File_System_Records.Delete(m => true);
        }
    }

    class RW : IDisposable, IRunnable
    {
        public void run()
        {
            IFileServant servant = new LocalFileServant();

            servant.Create("1.png");
            int fd = servant.Open("1.png");
            TestUtility<RW>.AssertTrue(servant.Write(fd, new byte[] { 1, 2, 3 }), 0);
            TestUtility<RW>.AssertTrue(servant.Read(fd).SequenceEqual(new byte[] { 1, 2, 3 }), 1);

            servant.Create("2.png");
            int fd2 = servant.Open("2.png");
            TestUtility<RW>.AssertTrue(servant.Write(fd2, new byte[] { 4, 5, 6 }), 2);
            TestUtility<RW>.AssertTrue(servant.Read(fd2).SequenceEqual(new byte[] { 4, 5, 6 }), 3);

            servant.Create("xyz/1.png");
            int fd3 = servant.Open("xyz/1.png");
            TestUtility<RW>.AssertTrue(servant.Write(fd3, new byte[] { 7, 8, 9 }), 4);
            TestUtility<RW>.AssertTrue(servant.Read(fd3).SequenceEqual(new byte[] { 7, 8, 9 }), 5);

            servant.Create("1/2/3/4/1.doc");
            int fd4 = servant.Open("1/2/3/4/1.doc");
            TestUtility<RW>.AssertTrue(servant.Write(fd4, new byte[] { 10,11,12 }), 6);
            TestUtility<RW>.AssertTrue(servant.Read(fd4).SequenceEqual(new byte[] { 10,11,12 }), 7);

            servant.Create("1\\2\\3\\4\\2.doc");
            int fd5 = servant.Open("1\\2\\3\\4\\2.doc");
            TestUtility<RW>.AssertTrue(servant.Write(fd5, new byte[] { 13, 14, 15 }), 8);
            TestUtility<RW>.AssertTrue(servant.Read(fd5).SequenceEqual(new byte[] { 13, 14, 15 }), 9);

            TestUtility<RW>.AssertFalse(servant.Write(9999, new byte[] { 1, 2, 3 }), 10);
            TestUtility<RW>.AssertFalse(servant.Write(-1, new byte[] { 1, 2, 3 }), 11);
            TestUtility<RW>.AssertExceptionHappen(() => servant.Read(-1), 12);
        }

        public void Dispose()
        {
            AS_File_System_Records.Delete(m => true);
        }
    }


    class NoSuchFile_R : IDisposable, IRunnable
    {
        public void run()
        {
            IFileServant servant = new RemoteFileServant();
            TestUtility<NoSuchFile_R>.AssertExceptionHappen( () => servant.Open("not_exist2.png"), 0);
        }

        public void Dispose()
        {
            AS_File_System_Records.Delete(m => true);
        }
    }

    class Create_R : IDisposable, IRunnable
    {
        public void run()
        {
            IFileServant servant = new RemoteFileServant();

            TestUtility<Create_R>.AssertTrue(servant.Create("exist.png")!=-1, 0);
            TestUtility<Create_R>.AssertNoException( () => servant.Open("exist.png") , 1);
        }

        public void Dispose()
        {
            AS_File_System_Records.Delete(m => true);
        }
    }

    class CreateDirectory_R : IDisposable, IRunnable
    {
        public void run()
        {
            IFileServant servant = new RemoteFileServant();
            servant.Create("a/b/c/1.png");

            TestUtility<CreateDirectory_R>.AssertNoException( () => servant.Open("a") , 1);
            TestUtility<CreateDirectory_R>.AssertNoException(() => servant.Open("a/b"), 2);
            TestUtility<CreateDirectory_R>.AssertNoException(() => servant.Open("a/b/c"), 3);
            TestUtility<CreateDirectory_R>.AssertNoException(() => servant.Open("a/b/c/1.png"), 4);
        }

        public void Dispose()
        {
            AS_File_System_Records.Delete(m => true);
        }
    }

    class RW_R : IDisposable, IRunnable
    {
        public void run()
        {
            IFileServant servant = new RemoteFileServant();

            servant.Create("1.png");
            int fd = servant.Open("1.png");
            TestUtility<RW_R>.AssertTrue(servant.Write(fd, new byte[] { 1, 2, 3 }), 0);
            TestUtility<RW_R>.AssertTrue(servant.Read(fd).SequenceEqual(new byte[] { 1, 2, 3 }), 1);

            servant.Create("2.png");
            int fd2 = servant.Open("2.png");
            TestUtility<RW_R>.AssertTrue(servant.Write(fd, new byte[] { 4, 5, 6 }), 2);
            TestUtility<RW_R>.AssertTrue(servant.Read(fd).SequenceEqual(new byte[] { 4, 5, 6 }), 3);

            TestUtility<RW_R>.AssertFalse(servant.Write(9999, new byte[] { 1, 2, 3 }), 4);
            TestUtility<RW_R>.AssertFalse(servant.Write(-1, new byte[] { 1, 2, 3 }), 5);
            TestUtility<RW_R>.AssertExceptionHappen(() => servant.Read(-1), 6);
        }

        public void Dispose()
        {
            AS_File_System_Records.Delete(m => true);
        }
    }

    class CreateBigFile : IDisposable, IRunnable
    {
        public void run()
        {
            IFileServant servant = new RemoteFileServant();
            servant.Create("/test/big.file");

            List<byte> big = new List<byte>();
            for (int index = 1; index <= 1024 * 1024 * 10; ++index)
                big.Add((byte)(index % 255));

            int fd = servant.Open("/test/big.file");
            servant.Write(fd, big.ToArray());

            var read = servant.Read(fd);
            for (int index = 1; index <= 1024 * 1024 * 10; ++index)
                TestUtility<CreateBigFile>.AssertTrue(read[index - 1] == (byte)(index % 255), 0);
        }

        public void Dispose()
        {
            AS_File_System_Records.Delete(m => true);
        }
    }
}


