﻿using NHttp;
using System;
using System.IO;
using System.Net;

namespace HttpTester
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var server = new HttpServer())
            {
                server.RequestReceived += Request;
                server.EndPoint = new IPEndPoint(IPAddress.Loopback, 8080);
                server.Start();

                Test();
            }
        }

        static void Request(object sender, HttpRequestEventArgs e)
        {
            if(e.Request.InputStream != null)
            {
                using (var r = new StreamReader(e.Request.InputStream))
                {
                    Console.WriteLine(r.ReadToEnd());
                }
            }
            
            using (var w = new StreamWriter(e.Response.OutputStream))
            {
                w.WriteLine("123");
            }
        }

        static void Test()
        {
            Console.ReadLine();
        }
    }
}
