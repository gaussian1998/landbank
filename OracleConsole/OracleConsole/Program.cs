﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Oracle.ManagedDataAccess.Client;

namespace OracleConsole
{

    /// <summary>
    /// 友情提供
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            OracleConnection oracleConn = new OracleConnection();
            oracleConn.ConnectionString = File.ReadAllText("Connection String.txt");
            oracleConn.Open();

            List<string> all_result = Excute(oracleConn, ReadSqlCmd());
            File.AppendAllLines("c:/bt/xml.txt", all_result);

            oracleConn.Close();
        }

        static string ReadSqlCmd()
        {
            string result = string.Empty;
            do
            {
                result += Console.ReadLine().Trim();

                var tmp = result.Split(';');
                if (tmp.Count() == 2 && !string.IsNullOrEmpty(tmp[1].Trim()))
                {
                    result = tmp[0] + ';';

                    var sub = tmp[1].Trim().Split('>');
                }

            } while (result.Last() != ';');

            Console.WriteLine(result);
            return result;
        }

        static int TableIndex(string[] temp)
        {
            int tableIdx = 0;
            foreach (var t in temp)
            {
                tableIdx++;
                if (string.Compare(t, "FROM", true) == 0)
                    return tableIdx;
            }

            return tableIdx;
        }

        static int  WhereIndex(string[] temp)
        {
            int whereIdx = 0;
            foreach (var t in temp)
            {
                whereIdx++;
                if (string.Compare(t, "WHERE", true) == 0)
                    return whereIdx;
            }
            return whereIdx;
        }

        static List<string> ColNames(OracleConnection oracleConn, string sql)
        {
            string[] temp = sql.Split(' ');
            int tableIdx = TableIndex(temp);
            int whereIdx = WhereIndex(temp);

            List<string> result = new List<string>();
            if (temp.Contains("all_tab_cols"))
            {
                for (int i = 0; i < 20; i++)
                    result.Add(i.ToString());
            }
            else if (!temp.Contains("*"))
            {
                for (int i = 1; i < tableIdx - 1; i++)
                {
                    if (string.Compare(temp[i], "DISTINCT", true) == 0)
                        continue;

                    result.Add(temp[i]);
                }
            }
            else if (string.Compare(temp[0], "SELECT", true) == 0)
            {
                for (int i = tableIdx; i < whereIdx; i++)
                {
                    string colSQL = string.Format("SELECT column_name FROM all_tab_cols WHERE table_name='{0}' ORDER BY column_id", temp[i].TrimEnd(',').TrimEnd(';'));

                    OracleCommand getCol = new OracleCommand(colSQL.TrimEnd(';'), oracleConn);
                    getCol.CommandType = CommandType.Text;

                    OracleDataReader getColDr = getCol.ExecuteReader();

                    while (getColDr.Read())
                    {
                        object[] tmp = new object[1];

                        getColDr.GetValues(tmp);

                        result.Add(tmp[0].ToString());
                    }
                }
            }
            return result;
        }

        static List<string> Excute(OracleConnection oracleConn, string sql)
        {
            List<string> result = new List<string>();

            OracleCommand cmd = new OracleCommand(sql.TrimEnd(';'), oracleConn);
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();

            List<string> allCols = ColNames(oracleConn,sql);
            while (dr.Read())
            {
                object[] vals = new object[allCols.Count];
                dr.GetValues(vals);

                for (int i = 0; i < allCols.Count; i++)
                {
                    //Console.Write("{0}:{1}｜", allCols[i], vals[i]);
                    result.Add(vals[i].ToString());
                }
            }
            return result;
        }


    }
}
