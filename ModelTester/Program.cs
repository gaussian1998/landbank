﻿using System;
using Library.Entity.Edmx;
using Library.Servant.Servant.Common.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using Library.Servant.Servant.ApprovalFlow.Models;
using System.Linq;

namespace ModelTester
{
    class Program
    {

        static void Main(string[] args)
        {

        }


        static void Test01()
        {
            var query = AS_Flow_Open_Records.FindModify<FlowOpenIndexItem>(m => true, result =>
            {

                return result;
            });

            string json = JsonConvert.SerializeObject(query);
            Console.WriteLine(json);

            List<FlowOpenIndexItem> acceptor = JsonConvert.DeserializeObject<List<FlowOpenIndexItem>>(json);
            Console.WriteLine(acceptor.Count);
        }

        static void Test02()
        {
            var result = AS_Flow_Open_Records.First<LogsResult>(m => m.ID == 1);

            string json = JsonConvert.SerializeObject(result);
            Console.WriteLine(json);

            LogsResult acceptor = JsonConvert.DeserializeObject<LogsResult>(json);
            Console.WriteLine(acceptor.ID);
        }

        static void Test03()
        {
            var query = AS_Flow_Open_Records.Find<Library.Servant.Servant.ApprovalTodo.Models.DetailsResult>(m => true);

            string json = JsonConvert.SerializeObject(query);
            Console.WriteLine(json);

            List<FlowOpenIndexItem> acceptor = JsonConvert.DeserializeObject<List<FlowOpenIndexItem>>(json);
            Console.WriteLine(acceptor.First().ApprovalStatus);
        }
    }
}
