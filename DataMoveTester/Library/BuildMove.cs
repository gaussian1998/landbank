﻿using Library.Entity.Edmx;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMoveTester.Library
{
    public class BuildMove
    {
        private static ILog logger = LogManager.GetLogger("BuildMove");
        private CommonLiber m_commonLiber = new CommonLiber();
        public bool MoveBuildData()
        {

            //房屋主檔
            //this.MoveAsAssetsBuildData();
            //this.MoveAsAssetsBuildDetailData();
            //this.MoveAsAssetsBuildLandData();
            //重大組成
            this.MoveAsAssetsBuildMPData();
            return true;
        }
        private bool MoveAsAssetsBuildData()
        {
            bool _return = true;
            DateTime _moveDateTime = DateTime.Now;
            OracleDBAccess _oracleDBA = new OracleDBAccess();
            //string _sql = "select * from CUX_FA_ASSETS_BUILD";
            string _sql = "select * from CUX_FA_ASSETS_BUILD_C";
            DataTable _dt = _oracleDBA.GetDataTable(_sql);
            //List<AS_Users> _localDB = AS_Users_Records.FindAll();
            //進行資料塞的動作   
            int _insertCount = 0;
            List<AS_Land_Code> _listASLandCode = AS_Land_Code_Records.FindAll().ToList();
            foreach (DataRow _dr in _dt.Rows)
            {
                //List<AS_Users> _localDB = AS_Users_Records.FindAll();
                if (AS_Assets_Build_Records.FindAll().Where(x => x.ID == int.Parse(_dr[0].ToString())).Count() == 0)
                {
                    _insertCount++;
                    AS_Assets_Build _asBuild = new AS_Assets_Build();
                    _asBuild.ID = int.Parse(_dr[0].ToString());
                    _asBuild.Asset_Number = _dr[1].ToString();
                    _asBuild.Book_Type = _dr[4].ToString();
                    _asBuild.Asset_Category_Code = _dr[5].ToString();
                    _asBuild.Deprn_Method_Code = _dr[9].ToString();
                    _asBuild.Life_Years = int.Parse(_dr[10].ToString());
                    _asBuild.Life_Months = int.Parse(_dr[11].ToString());
                    //Life_Years和Months要減一個月
                    if (_asBuild.Life_Years > 0 && _asBuild.Life_Months == 0)
                    {
                        _asBuild.Life_Years = _asBuild.Life_Years - 1;
                        _asBuild.Life_Months = 11;
                    }
                    else if (_asBuild.Life_Months > 0)
                    {
                        _asBuild.Life_Months = _asBuild.Life_Months - 1;
                    }
                    _asBuild.Location_Disp = _dr[15].ToString();
                    _asBuild.Old_Asset_Number = _dr[38].ToString();
                    _asBuild.Description = _dr[3].ToString();
                    _asBuild.Used_Type = _dr[16].ToString();
                    _asBuild.Used_Status = _dr[17].ToString();
                    _asBuild.Obtained_Method = _dr[18].ToString();
                    _asBuild.Original_Cost = decimal.Parse(_dr[19].ToString());
                    if (_dr[20] != null && !(_dr[20] is DBNull))
                    {
                        _asBuild.Salvage_Value = decimal.Parse(_dr[20].ToString());
                    }
                    else
                    {
                        _asBuild.Salvage_Value = 0;
                    }
                    if (_dr[56] != null && !(_dr[56] is DBNull))
                    { _asBuild.Deprn_Amount = decimal.Parse(_dr[56].ToString()); }
                    else
                    {
                        _asBuild.Deprn_Amount = 0;
                    }
                    if (_dr[57] != null && !(_dr[57] is DBNull))
                    { _asBuild.Reval_Adjustment_Amount = decimal.Parse(_dr[57].ToString()); }
                    else
                    {
                        _asBuild.Reval_Adjustment_Amount = 0;
                    }
                    _asBuild.Business_Area_Size = decimal.Parse(_dr[21].ToString());
                    _asBuild.NONBusiness_Area_Size = decimal.Parse(_dr[22].ToString());
                    _asBuild.Current_Cost = decimal.Parse(_dr[23].ToString());
                    if (_dr[58] != null && !(_dr[58] is DBNull))
                    { _asBuild.Reval_Reserve = decimal.Parse(_dr[58].ToString()); }
                    else
                    {
                        _asBuild.Reval_Reserve = 0;
                    }
                    _asBuild.Business_Book_Amount = decimal.Parse(_dr[24].ToString());
                    _asBuild.NONBusiness_Book_Amount = decimal.Parse(_dr[25].ToString());
                    _asBuild.Deprn_Reserve = decimal.Parse(_dr[26].ToString());
                    if (_dr[27] != null && !(_dr[27] is DBNull))
                    { _asBuild.Business_Deprn_Reserve = decimal.Parse(_dr[27].ToString()); }
                    else
                    {
                        _asBuild.Business_Deprn_Reserve = 0;
                    }
                    if (_dr[28] != null && !(_dr[28] is DBNull))
                    { _asBuild.NONBusiness_Deprn_Reserve = decimal.Parse(_dr[28].ToString()); }
                    else
                    {
                        _asBuild.NONBusiness_Deprn_Reserve = 0;
                    }
                    _asBuild.Date_Placed_In_Service = DateTime.Parse(_dr[7].ToString());
                    if (_dr[29] != null) { _asBuild.Remark1 = _dr[29].ToString(); }
                    else
                    {
                        _asBuild.Remark1 = "0";
                    }
                    if (_dr[30] != null) { _asBuild.Remark2 = _dr[30].ToString(); }
                    else
                    {
                        _asBuild.Remark2 = "0";
                    }
                    _asBuild.Current_Units = int.Parse(_dr[8].ToString());
                    //_asBuild.Sub_Sectioni_Name = _dr[3].ToString();
                    if (_dr[34] != null)
                    {
                        _asBuild.Created_By = _dr[34].ToString();
                    }
                    if (_dr[32] != null)
                    {
                        _asBuild.Last_Updated_By = _dr[32].ToString();
                    }
                    _asBuild.Create_Time = _moveDateTime;
                    _asBuild.Last_Updated_Time = _moveDateTime;
                    //依照BuildId取得Line資料
                    string _slineSql = "select * from CUX_FA_ASSETS_BUILD_LINES Where ASSET_BUILD_SEQ_ID=" + _asBuild.ID.ToString();
                    DataTable _dtline = _oracleDBA.GetDataTable(_slineSql);
                    foreach (DataRow _drline in _dtline.Rows)
                    {
                        _asBuild.Authorization_Number = _drline[1].ToString();
                        _asBuild.Authorization_Name = _drline[3].ToString();
                        _asBuild.Sub_Sectioni_Name = _drline[7].ToString();
                        _asBuild.Build_Number = _drline[9].ToString();
                        _asBuild.Build_Address = _drline[10].ToString() + _drline[11].ToString();
                        _asBuild.Building_Total_Floor = int.Parse(_drline[12].ToString());
                        _asBuild.Building_STRU = _drline[13].ToString();
                        //先移轉進入 到時候再轉   
                        string _cityName = null;
                        string _distinctName = null;
                        string _sectionName = null;
                        string _subSectionName = null;
                        if (!(_drline[4] is DBNull) && _drline[4].ToString() != null) _cityName = _drline[4].ToString();
                        if (!(_drline[5] is DBNull) && _drline[5].ToString() != null) _distinctName = _drline[5].ToString();
                        if (!(_drline[6] is DBNull) && _drline[6].ToString() != null) _sectionName = _drline[6].ToString();
                        if (!(_drline[7] is DBNull) && _drline[7].ToString() != null) _subSectionName = _drline[7].ToString();
                        _distinctName = m_commonLiber.DistrictProcess(_cityName, _distinctName);
                        _sectionName = m_commonLiber.SectionProcess(_sectionName);
                        _subSectionName = m_commonLiber.SubSectionProcess(_sectionName, _subSectionName);
                        AS_Land_Code _asLandCode = _listASLandCode.Where(x => x.City_Name == _cityName).FirstOrDefault();
                        if (_asLandCode != null && _asLandCode.ID > 0) { _asBuild.City_Code = _asLandCode.City_Code; } else { _asBuild.City_Code = "0"; }
                        _asBuild.City_Name = _drline[4].ToString();
                        _asLandCode = _listASLandCode.Where(x => x.City_Name == _cityName && x.District_Name == _distinctName).FirstOrDefault();
                        if (_asLandCode != null && _asLandCode.ID > 0) { _asBuild.District_Code = _asLandCode.District_Code; } else { _asBuild.District_Code = "0"; }
                        /*_asBuild.District_Code = "0";*/
                        _asBuild.District_Name = _drline[5].ToString();
                        _asLandCode = _listASLandCode.Where(x => x.City_Name == _cityName && x.District_Name == _distinctName && x.Sectioni_Name ==_sectionName && x.Sub_Sectioni_Name == _subSectionName).FirstOrDefault();
                        if (_asLandCode != null && _asLandCode.ID > 0) { _asBuild.Section_Code = _asLandCode.Section_Code; } else { _asBuild.Section_Code = "0"; }
                        //_asBuild.Section_Code = "0";
                        _asBuild.Sectioni_Name = _drline[6].ToString();
                    }
                    //順利轉資料先亂塞
                    _asBuild.Officer_Branch = "0";
                    _asBuild.Parent_Asset_Number = "0";
                    _asBuild.Delete_Reason = "0";
                    _asBuild.Urban_Renewal = "0";
                    _asBuild.Old_Asset_Category_Code = "0";
                    AS_Assets_Build_Records.Create(_asBuild);
                }
            }
            logger.Debug("新增AS_Assets_Build筆數" + _insertCount);
            return _return;
        }
        private bool MoveAsAssetsBuildDetailData()
        {
            bool _return = true;
            DateTime _moveDateTime = DateTime.Now;
            //取得Location所有房屋主檔
            List<AS_Assets_Build> _localBuild = AS_Assets_Build_Records.FindAll().OrderBy(x => x.ID).ToList();
            OracleDBAccess _oracleDBA = new OracleDBAccess();
            int _insertCount = 0;
            foreach (AS_Assets_Build _asAssetsBuild in _localBuild)
            {
                //string _sql = "select * from CUX_FA_ASSETS_BUILD_DETAILS Where ASSET_BUILD_SEQ_ID="+ _asAssetsBuild.ID.ToString();
                string _sql = "select * from CUX_FA_ASSETS_BUILD_DETAILS_C Where ASSET_BUILD_SEQ_ID=" + _asAssetsBuild.ID.ToString();
                DataTable _dt = _oracleDBA.GetDataTable(_sql);
                foreach (DataRow _dr in _dt.Rows)
                {
                    //List<AS_Users> _localDB = AS_Users_Records.FindAll();
                    if (AS_Assets_Build_Detail_Records.FindAll().Where(x => x.ID == int.Parse(_dr[0].ToString())).Count() == 0)
                    {
                        _insertCount++;
                        AS_Assets_Build_Detail _asBuildDetail = new AS_Assets_Build_Detail();
                        _asBuildDetail.ID = int.Parse(_dr[0].ToString());
                        _asBuildDetail.Assets_Build_ID = int.Parse(_dr[2].ToString());
                        _asBuildDetail.Asset_Type = _dr[3].ToString();
                        _asBuildDetail.Floor_Uses = _dr[4].ToString();
                        _asBuildDetail.Area_Size = decimal.Parse(_dr[5].ToString());
                        _asBuildDetail.Authorized_Scope_Moldcule = decimal.Parse(_dr[6].ToString());
                        _asBuildDetail.Authorized_Scope_Denominx = decimal.Parse(_dr[7].ToString());
                        _asBuildDetail.Authorized_Area = decimal.Parse(_dr[8].ToString());
                        _asBuildDetail.Own_Area = 0;
                        _asBuildDetail.NONOwn_Area = 0;
                        //_asBuild.Sub_Sectioni_Name = _dr[3].ToString();
                        if (_dr[12] != null)
                        {
                            _asBuildDetail.Created_By = _dr[12].ToString();
                        }
                        if (_dr[10] != null)
                        {
                            _asBuildDetail.Last_Updated_By = _dr[10].ToString();
                        }
                        _asBuildDetail.Create_Time = _moveDateTime;
                        _asBuildDetail.Last_Updated_Time = _moveDateTime;
                        AS_Assets_Build_Detail_Records.Create(_asBuildDetail);
                    }
                }
            }
            //取得此房屋主檔在Oracle的明細

            //List<AS_Users> _localDB = AS_Users_Records.FindAll();

            logger.Debug("新增AS_Assets_Build_Detail筆數" + _insertCount);
            return _return;
        }
        private bool MoveAsAssetsBuildLandData()
        {
            bool _return = true;
            DateTime _moveDateTime = DateTime.Now;
            //取得Location所有房屋主檔
            List<AS_Assets_Build> _localBuild = AS_Assets_Build_Records.FindAll();
            OracleDBAccess _oracleDBA = new OracleDBAccess();
            int _insertCount = 0;
            foreach (AS_Assets_Build _asAssetsBuild in _localBuild)
            {
                //string _sql = "select * from CUX_FA_ASSETS_BUILD_LANDS Where ASSET_BUILD_SEQ_ID=" + _asAssetsBuild.ID.ToString();
                string _sql = "select * from CUX_FA_ASSETS_BUILD_LANDS_C Where ASSET_BUILD_SEQ_ID=" + _asAssetsBuild.ID.ToString();
                DataTable _dt = _oracleDBA.GetDataTable(_sql);
                foreach (DataRow _dr in _dt.Rows)
                {
                    //List<AS_Users> _localDB = AS_Users_Records.FindAll();
                    if (AS_Assets_Build_Land_Records.FindAll().Where(x => x.Asset_Build_ID == int.Parse(_dr[0].ToString()) && x.Asset_Land_ID == int.Parse(_dr[3].ToString())).Count() == 0)
                    {
                        _insertCount++;
                        AS_Assets_Build_Land _asBuildLand = new AS_Assets_Build_Land();
                        _asBuildLand.ID = int.Parse(_dr[0].ToString());
                        _asBuildLand.Asset_Build_ID = int.Parse(_dr[2].ToString());
                        _asBuildLand.Asset_Land_ID = int.Parse(_dr[3].ToString());
                        //_asBuild.Sub_Sectioni_Name = _dr[3].ToString();
                        if (_dr[7] != null)
                        {
                            _asBuildLand.Created_By = _dr[7].ToString();
                        }
                        if (_dr[5] != null)
                        {
                            _asBuildLand.Last_Updated_By = _dr[5].ToString();
                        }
                        _asBuildLand.Create_Time = _moveDateTime;
                        _asBuildLand.Last_Updated_Time = _moveDateTime;
                        AS_Assets_Build_Land_Records.Create(_asBuildLand);
                    }
                }
            }
            //取得此房屋主檔在Oracle的明細

            //List<AS_Users> _localDB = AS_Users_Records.FindAll();

            logger.Debug("新增AS_Assets_Build_Land_Records筆數" + _insertCount);
            return _return;
        }
        private bool MoveAsAssetsBuildMPData()
        {
            bool _return = true;
            DateTime _moveDateTime = DateTime.Now;
            OracleDBAccess _oracleDBA = new OracleDBAccess();
            //string _sql = "select * from CUX_FA_ASSETS_BUILD";
            string _sql = "select * from cux_fa_assets_mp where "+ 
                " Asset_seq_id not in (select Asset_seq_id from cux_fa_assets_mp where " +
" (asset_category_code like '3%' or asset_category_code like '4%' or "+
" asset_category_code like '5%' or asset_category_code like '8%') ) " + 
" And Asset_seq_id not in (50899,50904,50905) "+
" and not (70472 < Asset_seq_id  and Asset_seq_id < 70645) and not (76693 < Asset_seq_id  and Asset_seq_id < 83087) "+
" and asset_category_code like '2%' ";
            DataTable _dt = _oracleDBA.GetDataTable(_sql);
            //List<AS_Users> _localDB = AS_Users_Records.FindAll();
            //進行資料塞的動作   
            int _insertCount = 0;
            List<AS_Assets_Build> _listasAssetsBuild = AS_Assets_Build_Records.FindAll().ToList();
            foreach (DataRow _dr in _dt.Rows)
            {
                //List<AS_Users> _localDB = AS_Users_Records.FindAll();
                if (AS_Assets_Build_MP_Records.FindAll().Where(x => x.Asset_Number == _dr[2].ToString()).Count() == 0)
                {
                    _insertCount++;
                    AS_Assets_Build_MP _asBuildMP = new AS_Assets_Build_MP();
                    _asBuildMP.Asset_Build_ID = _listasAssetsBuild.Where(x => x.Asset_Number == _dr[26].ToString()).FirstOrDefault().ID;
                    _asBuildMP.Asset_Number = _dr[2].ToString();
                    _asBuildMP.Parent_Asset_Number = _dr[26].ToString();
                    _asBuildMP.Asset_Category_Code = _dr[5].ToString();
                    _asBuildMP.Old_Asset_Number = _dr[2].ToString();
                    _asBuildMP.Book_Type = _dr[19].ToString();
                    _asBuildMP.Date_Placed_In_Service = DateTime.Parse(_dr[16].ToString());
                    _asBuildMP.Current_Units = decimal.Parse(_dr[14].ToString());
                    _asBuildMP.Deprn_Method_Code = _dr[20].ToString();
                    if (_dr[21] != null && !(_dr[21] is DBNull))
                    {
                        _asBuildMP.Life_Years = int.Parse(_dr[21].ToString());
                    }
                    else
                    {
                        _asBuildMP.Life_Years = 0;
                    }
                    if (_dr[22] != null && !(_dr[22] is DBNull))
                    {
                        _asBuildMP.Life_Months = int.Parse(_dr[22].ToString());
                    }
                    else
                    {
                        _asBuildMP.Life_Months = 0;
                    }
                    //Life_Years和Months要減一個月
                    if (_asBuildMP.Life_Years > 0 && _asBuildMP.Life_Months == 0)
                    {
                        _asBuildMP.Life_Years = _asBuildMP.Life_Years - 1;
                        _asBuildMP.Life_Months = 11;
                    }
                    else if (_asBuildMP.Life_Months > 0)
                    {
                        _asBuildMP.Life_Months = _asBuildMP.Life_Months - 1;
                    }
                    if (_dr[25] != null && !(_dr[25] is DBNull))
                    {
                        _asBuildMP.Location_Disp = _dr[25].ToString();
                    }
                    else {
                        _asBuildMP.Location_Disp = "0";
                    }
                    if (_dr[17] != null && !(_dr[17] is DBNull))
                    {
                        _asBuildMP.PO_Number = _dr[17].ToString();
                    }else {
                        _asBuildMP.PO_Number = "0";
                        }
                    if (_dr[18] != null && !(_dr[18] is DBNull))
                    {
                        _asBuildMP.PO_Destination = _dr[18].ToString();
                    }
                    else
                    {
                        _asBuildMP.PO_Destination = "0";
                    }
                    if (_dr[6] != null && !(_dr[6] is DBNull))
                    {
                        _asBuildMP.Model_Number = _dr[6].ToString();
                    }
                    else
                    {
                        _asBuildMP.Model_Number = "0";
                    }
                    if (_dr[15] != null && !(_dr[15] is DBNull))
                    {
                        DateTime _date = DateTime.Parse(_dr[15].ToString());
                        if (_date.Year < 1000)
                        {
                            _asBuildMP.Transaction_Date = new DateTime(_date.Year + 1911, _date.Month, _date.Day);
                        }
                        else
                        {
                            _asBuildMP.Transaction_Date = _date;
                        }
                    }
                    else
                    {
                        _asBuildMP.Transaction_Date = _moveDateTime;
                    }
                    if (_dr[3] != null && !(_dr[3] is DBNull))
                    {
                        _asBuildMP.Assets_Unit = _dr[3].ToString();
                    }
                    else
                    {
                        _asBuildMP.Assets_Unit = "0";
                    }
                    if (_dr[7] != null && !(_dr[7] is DBNull))
                    {
                        _asBuildMP.Accessory_Equipment = _dr[7].ToString();
                    }
                    else
                    {
                        _asBuildMP.Accessory_Equipment = "0";
                    }
                    if (_dr[61] != null && !(_dr[61] is DBNull))
                    {
                        _asBuildMP.Assigned_NUM = _dr[61].ToString();
                    }
                    else
                    {
                        _asBuildMP.Assigned_NUM = "0";
                    }
                    if (_dr[28] != null && !(_dr[28] is DBNull))
                    {
                        _asBuildMP.Asset_Category_NUM = _dr[28].ToString();
                    }
                    else
                    {
                        _asBuildMP.Asset_Category_NUM = "0";
                    }
                    if (_dr[62] != null && !(_dr[62] is DBNull))
                    {
                        _asBuildMP.Asset_Category_Name = _dr[62].ToString();
                    }
                    else
                    {
                        _asBuildMP.Asset_Category_Name = "0";
                    }
                    if (_dr[27] != null && !(_dr[27] is DBNull))
                    {
                        _asBuildMP.Asset_Structure = _dr[27].ToString();
                    }
                    else
                    {
                        _asBuildMP.Asset_Structure = "0";
                    }
                    if (_dr[59] != null && !(_dr[59] is DBNull))
                    {
                        _asBuildMP.Current_Cost = decimal.Parse(_dr[59].ToString());
                    }
                    else
                    {
                        _asBuildMP.Current_Cost = 0;
                    }
                    if (_dr[9] != null && !(_dr[9] is DBNull))
                    {
                        _asBuildMP.Deprn_Reserve = decimal.Parse(_dr[9].ToString());
                    }
                    else
                    {
                        _asBuildMP.Deprn_Reserve = 0;
                    }
                    if (_dr[11] != null && !(_dr[11] is DBNull))
                    {
                        _asBuildMP.Salvage_Value = decimal.Parse(_dr[11].ToString());
                    }
                    else
                    {
                        _asBuildMP.Salvage_Value = 0;
                    }
                    if (_dr[13] != null && !(_dr[13] is DBNull))
                    {
                        _asBuildMP.Remark = _dr[13].ToString();
                    }
                    else
                    {
                        _asBuildMP.Remark = "0";
                    }
                    if (_asBuildMP.Deprn_Method_Code == "STL" || _asBuildMP.Deprn_Method_Code == "FSTL")
                    {
                        _asBuildMP.Not_Deprn_Flag = false;
                        _asBuildMP.Deprn_Type = 1;
                    }
                    else
                    {
                        _asBuildMP.Not_Deprn_Flag = true;
                        _asBuildMP.Deprn_Type = 0;
                    }
                    if (_dr[51] != null && !(_dr[51] is DBNull))
                    {
                        _asBuildMP.Officer_Branch = _dr[51].ToString();
                    }
                    if (_dr[4] != null && !(_dr[4] is DBNull))
                    {
                        _asBuildMP.Description = _dr[4].ToString();
                    }
                    else {
                        _asBuildMP.Description = "0";
                    }
                    if (_dr[32] != null)
                    {
                        _asBuildMP.Created_By = decimal.Parse(_dr[32].ToString());
                    }
                    if (_dr[30] != null)
                    {
                        _asBuildMP.Last_UpDatetimed_By = decimal.Parse(_dr[30].ToString());
                    }
                    if (_dr[31] != null && !(_dr[31] is DBNull))
                    {
                        _asBuildMP.Create_Time = DateTime.Parse(_dr[31].ToString());
                    }
                    else
                    {
                        _asBuildMP.Create_Time = _moveDateTime;
                    }
                    if (_dr[29] != null && !(_dr[29] is DBNull))
                    {
                        _asBuildMP.Last_UpDatetimed_Time = DateTime.Parse(_dr[29].ToString());
                    }
                    else
                    {
                        _asBuildMP.Last_UpDatetimed_Time = _moveDateTime;
                    }
                    AS_Assets_Build_MP_Records.Create(_asBuildMP);
                }
            }
            logger.Debug("新增AS_Assets_Build_MP筆數" + _insertCount);
            return _return;
        }
    }
}
