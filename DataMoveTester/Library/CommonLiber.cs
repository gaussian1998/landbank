﻿using Library.Entity.Edmx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMoveTester.Library
{
    public class CommonLiber
    {
        private List<AS_Land_Code> m_listASLandCode = new List<AS_Land_Code>();
        public CommonLiber(){
            m_listASLandCode = AS_Land_Code_Records.Find(x => ("段").Contains(x.Sectioni_Name)).ToList();
            }
        public string DistrictProcess(string cityName ,string districtName) {
            if (cityName == null || districtName == null) return districtName;
            string _districtName = districtName;
            switch (cityName) {
                case "台南市":
                    //台南市中區改成中西區
                    if (districtName == "中區") {
                        _districtName = "中西區";
                    }
                    break;
                case "台東縣":
                    if (districtName == "台東市" || districtName == "臺東市")
                    {
                        _districtName = "台東市";
                    }
                    break;
                case "台中市":
                    if (districtName == "梧棲區" || districtName == "梧棲鎮")
                    {
                        _districtName = "梧棲區";
                    }
                    break;
                case "嘉義縣":
                    if (districtName == "朴子市" || districtName == "朴子鎮")
                    {
                        _districtName = "朴子市";
                    }
                    break;
                case "新竹市":
                    if (districtName == "-" || districtName == "新竹市")
                    {
                        _districtName = "新竹市";
                    }
                    break;
                case "嘉義市":
                    if (districtName == "-" || districtName == "嘉義市")
                    {
                        _districtName = "嘉義市";
                    }
                    break;
            }
            return _districtName;
        }

        public string SectionProcess(string sectionName)
        {
            if (sectionName == null) return sectionName;
            string _sectionName = sectionName;
            //只有它是符合LandCode中段的名稱不然就去掉段
            if (m_listASLandCode.Where(x => x.Sectioni_Name == _sectionName).Count() == 0) {
                _sectionName = _sectionName.Replace("段","");
                _sectionName = _sectionName.Trim();
            }
            if (_sectionName == "台東" || _sectionName == "臺東") {
                _sectionName = "台東";
            }
            return _sectionName;
        }
        public string SubSectionProcess(string section ,string subSectionName)
        {
            if (subSectionName == null) return subSectionName;
                string _subSectionName = subSectionName;
            //A小段直接去掉
            if (_subSectionName == "A小段" )
            {
                _subSectionName = "";
            }
            if (_subSectionName != null && _subSectionName != "")
            {
                _subSectionName = _subSectionName.Replace("小段", "");
                _subSectionName = _subSectionName.Trim();
            }
            //孔廟沒有Subsection
            if (section != null && section == "孔廟") {
                _subSectionName = "";
            }
          
            return _subSectionName;
        }
    }
}
