﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMoveTester.Library
{
    public class OracleDBAccess
    {
        private static ILog logger = LogManager.GetLogger("DBAccess.Log");
        private static ILog logQuery = LogManager.GetLogger("DBAccess.QueryLog");

        private OracleConnection m_myConn = new OracleConnection();
        private OracleConnection m_myTransConn = new OracleConnection();
        private OracleDataAdapter m_myDap = new OracleDataAdapter();
        private string m_sConn;
        private string m_sTransConn;
        private DataSet m_dsResult = new DataSet();
        private DataRow m_drResult;
        private Array m_aryResult;
        private OracleCommand m_myCmd;
        private OracleDataReader m_myDR;
        private OracleTransaction m_myTrans;
        private int m_iEffectRowCount;
        private bool m_bEnableTransaction = false;
        const string ConnectionString = "OracleConnectionString";
        private static string m_SecretCnStr = "";
        private static string m_PlainCnStr = "";

        /// <summary>
        /// Desc: 取得連線物件
        /// </summary>
        public OracleConnection GetConnection() { return m_myConn; }

        public OracleConnection getTransConnection() { return m_myTransConn; }
        public OracleConnection GetConnection(string ConnString) {
            m_sConn = ConnString;
            m_myConn = new OracleConnection(this.GetConnectionString);
            try {
                m_myConn.Open();
                m_myConn.Close();
            } catch (Exception ex) {
                //SetErrorMessage("連線發生錯誤，請檢查！", ex);
                if (m_myConn.State == ConnectionState.Open)
                {
                    m_myConn.Close();
                }
            }
            return m_myConn;
        }

        /// <summary>
        /// Desc: 取得使用中的連線字串
        /// </summary>
        public string GetConnectionString {
            get { return m_sConn; }
        }

        #region "建構式"

        public OracleDBAccess() {
            //先抓Web.config，沒有的話抓 app.config
            try {
                if (System.Web.Configuration.WebConfigurationManager.ConnectionStrings[ConnectionString] != null)
                {
                    m_sConn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings[ConnectionString].ConnectionString;
                }
                else {
                    m_sConn = System.Configuration.ConfigurationManager.AppSettings[ConnectionString];
                }
                m_myConn = new OracleConnection(this.GetConnectionString);
                m_myConn.Open();
                m_myConn.Close();
                this.ResetEffectRowCount();
            }
            catch (Exception ex) {
                //SetErrorMessage("連線發生錯誤，請檢查！", ex)
                if (m_myConn.State == ConnectionState.Open) {
                    m_myConn.Close();
                }
            }
        }

        public OracleDBAccess(string ConnStr)
        {
            m_sConn = ConnStr;
            m_myConn = new OracleConnection(this.GetConnectionString);
            try
            {
                m_myConn.Open();
                m_myConn.Close();
                this.ResetEffectRowCount();
            }
            catch (Exception ex)
            {
                if (m_myConn.State == ConnectionState.Open)
                {
                    m_myConn.Close();
                }
            }
        }

        public OracleDBAccess(OracleConnection myConnection) {
            try {
                m_myConn = myConnection;
                m_sConn = m_myConn.ConnectionString;
                this.ResetEffectRowCount();
            }
            catch (Exception ex) {
                //SetErrorMessage("連線發生錯誤，請檢查！", ex)
                if (m_myConn.State == ConnectionState.Open)
                {
                    m_myConn.Close();
                }
            }
        }
        #endregion

        #region "增刪改-執行交易"

        /// <summary>
        /// Desc: 取得使用中的「交易連線物件」之連線字串
        /// </summary>
        public string GetTransConnectionString {
            get {
                return m_sTransConn;
            }
        }

        /// <summary>
        /// Desc: 啟用並取得交易連線物件
        /// </summary>
        /// <param name="isTGOff">Set DB session id into temp table</param>
        /// <returns>已經 Open 連線的交易連線物件</returns>
        public OracleTransaction GetTransaction(bool isTGOff, string className = "") {
            //m_sTransConn = ConnString
            m_myTransConn = new OracleConnection(this.GetConnectionString);
            try {
                string _logMessage = "GetTransaction - " + isTGOff.ToString() + " , " + className;
                logger.Debug(_logMessage);
                m_myTransConn.Open();
                m_myTrans = m_myTransConn.BeginTransaction();
                this.EnableTransaction(m_myTrans);
                if (isTGOff) {
                    //this.EnableTGKiller(className);
                }
            } catch (Exception ex) {
                //Me.SetErrorMessage("連線發生錯誤，請檢查！", ex)
                if (m_myTransConn.State == ConnectionState.Open) {
                    m_myTransConn.Close();
                }
                this.DisableTransaction();
            }
            // 要啟用交易，連線必須開啟
            return m_myTrans;
        }

        /// <summary>
        /// Desc: 取得目前啟用中的交易物件
        /// </summary>
        public OracleTransaction CurrentTransaction {
            get {
                return m_myTrans;
            }
        }

        /// <summary>
        /// Desc: 完成交易並關閉連線
        /// </summary>
        /// <param name="myTrans">使用中的交易連線物件</param>
        public void CommitTransaction(OracleTransaction myTrans) {
            try {
                if (m_myTrans != null) {
                    logger.Debug("CommitTransaction - " + myTrans.Connection.State.ToString());
                    myTrans.Commit();
                    if (m_myTransConn.State == ConnectionState.Open) {
                        m_myTransConn.Close();
                    }
                } else {
                    throw new Exception("無法執行Commit，因為交易物件不存在！");
                }
            }
            catch (Exception ex) {
                //Me.SetErrorMessage("無法執行Commit，因為交易物件不存在！", ex)
                throw ex;
            }
        }

        /// <summary>
        /// Desc: 還原交易並關閉連線
        /// </summary>
        public void RollbackTransaction(OracleTransaction myTrans) {
            try {
                if (m_myTrans != null) {
                    logger.Debug("RollbackTransaction - " + myTrans.Connection.State.ToString());
                    myTrans.Rollback();
                    if (m_myTransConn.State == ConnectionState.Open)
                    {
                        m_myTransConn.Close();
                    }
                }

                else {
                    throw new Exception("無法執行RollBack，因為交易物件不存在！");
                }
            }
            catch (Exception ex) {
                //Me.SetErrorMessage("無法執行RollBack，因為交易物件不存在！", ex)
                throw ex;
            }
        }
        /// <summary>
        /// Desc: 在 DBAccess 內部啟用交易連線
        /// </summary>
        /// <param name="myTransaction">交易物件</param>
        public void EnableTransaction(OracleTransaction myTransaction) {
            m_myTrans = myTransaction;
            m_bEnableTransaction = true;
        }

        /// <summary>
        /// Desc: 在 DBAccess 內部啟用交易並置入session id於temp table 以供識別
        /// </summary>
        //public void EnableTGKiller(string className ) {
        //    this.RunOracleCommand("INSERT INTO NWIS_SESSION_TMP VALUES ( userenv(//sessionid//),SYSDATE,//" + className + "//)");
        //    }

        /// <summary>
        /// Desc: 在 DBAccess 內部停用交易連線
        /// </summary>
        public void DisableTransaction()
        {
            m_myTrans = null;
            m_bEnableTransaction = false;
            if (m_myTransConn != null)
            {
                if (m_myTransConn.State == ConnectionState.Open)
                {
                    m_myTransConn.Close();
                }
            }
        }

        /// <summary>
        /// Desc: 執行增刪改工作
        /// </summary>
        /// <param name="myCmd">增刪改的Command物件</param>
        /// <returns>受影響的累計筆數</returns>
        public int RunOracleCommand(OracleCommand myCmd)
        {

            try
            {
                myCmd.Connection = m_myConn;
                if (m_bEnableTransaction)
                {
                    myCmd.Transaction = m_myTrans;
                    myCmd.Connection = m_myTransConn;
                }
                else if (m_myConn.State != ConnectionState.Open)
                {
                    m_myConn.Open();
                }
                //LogObjectValue("DBAccess RunOracleCommand", myCmd, logger)
                this.EffectRowCount = myCmd.ExecuteNonQuery();
                if (m_myConn.State == ConnectionState.Open)
                {
                    m_myConn.Close();
                }
                return this.EffectRowCount;
            }
            catch (Exception ex)
            {
                //Me.SetErrorMessage("Exception in  RunOracleCommand : ", ex);
                if (m_myConn.State == ConnectionState.Open)
                {
                    m_myConn.Close();
                }
                throw ex;
            }
        }

        /// <summary>
        /// Desc: 執行增刪改工作
        /// </summary>
        /// <param name="SQLStr">增刪改的 SQL 語法</param>
        /// <returns>受影響的累計筆數</returns>
        public int RunOracleCommand(string SQLStr)
        {
            OracleConnection conn = new OracleConnection();
            try
            {
                if (m_bEnableTransaction)
                {
                    conn = m_myTransConn;
                }
                else
                {
                    conn = m_myConn;
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }
                }

                if (m_myCmd == null)
                {
                    m_myCmd = new OracleCommand(SQLStr);
                }
                else
                {
                    m_myCmd.CommandText = SQLStr;
                }
                m_myCmd.Connection = conn;
                if (m_bEnableTransaction)
                {
                    m_myCmd.Transaction = m_myTrans;
                }
                logger.Debug(SQLStr);
                this.EffectRowCount = m_myCmd.ExecuteNonQuery();
                if (m_myConn.State == ConnectionState.Open)
                {
                    m_myConn.Close();
                }
                return this.EffectRowCount;
            }
            catch (Exception ex)
            {
                //Me.SetErrorMessage("Exception in  RunOracleCommand : ", ex)
                if (m_myConn.State == ConnectionState.Open)
                {
                    m_myConn.Close();
                }
                throw ex;
            }
        }

        /// <summary>
        /// Desc: 執行增刪改工作
        /// </summary>
        /// <param name="myDS">要更新的DataSet</param>
        /// <returns>受影響的累計筆數</returns>
        public int RunOracleCommand(DataSet myDS, string TableName) {
            //Dim conn As OracleConnection
            try {
                m_myDap = new OracleDataAdapter("Select * from " + TableName + " Where 1 = 0 ", m_myConn);
                //LogObjectValue("DBAccess RunOracleCommand", myDS, logger);
                this.EffectRowCount = m_myDap.Update(myDS.Tables[TableName]);
                return this.EffectRowCount;
            } catch (Exception ex) {
                //conn = IIf(m_bEnableTransaction, m_myTransConn, m_myConn)
                //Me.SetErrorMessage("Exception in  RunOracleCommand : ", ex)
                throw ex;
            }

        }

        /// <summary>
        /// Desc: 透過DataAdapter執行有包裝交易之增刪改工作
        /// </summary>
        /// <param name="dap">已包裝SelectCommand的DAP</param>
        /// <param name="trans">已經啟用的交易物件</param>
        /// <param name="ds">要更新的DataSet</param>
        /// <returns>受影響的累計筆數</returns>
        public int RunOracleCommand(OracleDataAdapter dap, OracleTransaction trans, DataSet ds) {
            int iEffRows = 0;

            for (int i = 0; i <= ds.Tables.Count - 1; i++) {
                iEffRows += this.RunOracleCommand(dap, trans, ds.Tables[i]);
            }
            return iEffRows;
        }

        /// <summary>
        /// Desc: 透過DataAdapter執行有包裝交易之增刪改工作
        /// </summary>
        /// <param name="dap">已包裝SelectCommand的DAP</param>
        /// <param name="trans">已經啟用的交易物件</param>
        /// <param name="dt">要更新的DataTable</param>
        /// <returns>受影響的累計筆數</returns>
        public int RunOracleCommand(OracleDataAdapter dap, OracleTransaction trans, DataTable dt) {
            if (dap.SelectCommand == null || String.Empty.Equals(dap.SelectCommand.CommandText))
            {
                throw new Exception("無法取得SelectCommand");
            }
            try {
                dap = this.CreateDapTransactionCommand(dap, trans);
                //AddHandler dap.RowUpdating, AddressOf DapRowUpdatingEvent;
                //LogObjectValue("DBAccess RunOracleCommand", dt, logger)
                this.EffectRowCount = dap.Update(dt);
                return this.EffectRowCount;
            }
            catch (Exception ex) {
                //Me.SetErrorMessage("Exception in  RunOracleCommand : ", ex)
                throw ex;
            }
            //Catch ex As Exception
        }

        private OracleDataAdapter CreateDapTransactionCommand(OracleDataAdapter dap, OracleTransaction trans) {
            //建立交易Command物件
            OracleCommandBuilder cmdBuilder = new OracleCommandBuilder(dap);
            dap.SelectCommand.Connection = trans.Connection;
            dap.SelectCommand.Transaction = trans;
            dap.UpdateCommand = cmdBuilder.GetUpdateCommand();
            dap.UpdateCommand.Connection = trans.Connection;
            dap.UpdateCommand.Transaction = trans;
            dap.InsertCommand = cmdBuilder.GetInsertCommand();
            dap.InsertCommand.Connection = trans.Connection;
            dap.InsertCommand.Transaction = trans;
            dap.DeleteCommand = cmdBuilder.GetDeleteCommand();
            dap.DeleteCommand.Connection = trans.Connection;
            dap.DeleteCommand.Transaction = trans;
            return dap;
        }

        /// <summary>
        /// Desc: DataAdapter的RowUpdating事件
        /// </summary>
        //private void DapRowUpdatingEvent(Object sender , OracleRowUpdatingEventArgs e )
        //        LogObjectValue("DapRowUpdatingEvent", e.Command, logger);
        //    }

        #endregion

        #region "查詢資料"

        /// <summary>
        /// Desc: 取得查詢結果DataSet
        /// </summary>
        /// <param name="SqlString">SQL Command Text</param>
        /// <returns>查詢結果DataSet</returns>
        public DataSet GetDataSet(string SqlString)
        {
            //判斷有無啟用交易決定連線要不要開啟
            if (m_bEnableTransaction)
            {
                m_myDap = new OracleDataAdapter(SqlString, m_myTransConn);
                m_myDap.SelectCommand.Transaction = m_myTrans;
            }
            else
            {
                m_myDap = new OracleDataAdapter(SqlString, m_myConn);
            }

            try
            {
                m_dsResult.Tables.Clear();
                logQuery.Debug("GetDataSet : " + SqlString);
                m_myDap.Fill(m_dsResult);
                //090724 by Leo: 把 DBNull 轉成 ""
                for (int iTable = 0; iTable <= m_dsResult.Tables.Count - 1; iTable++)
                {
                    this.DBNull2Nothing(m_dsResult.Tables[iTable]);
                }

                //    if(m_dsResult!=null && m_dsResult.Tables[0]!=null &&( m_dsResult.Tables[0].Rows.Count <= 3) {
                //    LogObjectValue("GetDataSet", m_dsResult.Tables(0), logQuery)
                //}
                m_dsResult.AcceptChanges();
                return m_dsResult.Copy();
            }

            catch (Exception ex)
            {
                logger.Debug("GetDataSet : " + SqlString);
                //SetErrorMessage("讀取資料表時發生錯誤，請檢查！", ex)
                return null;
            }
        }

        /// <summary>
        /// Desc: 取得查詢結果DataSet
        /// </summary>
        /// <param name="myCmd">OracleCommand</param>
        /// <returns>查詢結果DataSet</returns>
        public DataSet GetDataSet(OracleCommand myCmd)
        {

            m_myDap = new OracleDataAdapter();
            m_myDap.SelectCommand = myCmd;

            //判斷有無啟用交易決定連線要不要開啟
            if (m_bEnableTransaction)
            {
                m_myDap.SelectCommand.Connection = m_myTransConn;
                myCmd.Transaction = m_myTrans;
            }
            else
            {
                m_myDap.SelectCommand.Connection = m_myConn;
            }
            try
            {
                m_dsResult.Tables.Clear();
                //LogObjectValue("GetDataSet : ", myCmd, logQuery);
                m_myDap.Fill(m_dsResult);
                //090724 by Leo: 把 DBNull 轉成 ""
                for (int iTable = 0; iTable <= m_dsResult.Tables.Count - 1; iTable++)
                {
                    this.DBNull2Nothing(m_dsResult.Tables[iTable]);
                }
                //if Not IsNothing(m_dsResult) AndAlso Not IsNothing(m_dsResult.Tables(0)) AndAlso m_dsResult.Tables(0).Rows.Count <= 3 {
                //    LogObjectValue("GetDataSet", m_dsResult.Tables(0), logQuery)
                //    }
                m_dsResult.AcceptChanges();
                return m_dsResult.Copy();
            }
            catch (Exception ex)
            {
                //LogObjectValue("GetDataSet : ", myCmd, logQuery)
                //SetErrorMessage("讀取資料表時發生錯誤，請檢查！", ex)
                return null;
            }
        }

        /// <summary>
        /// Desc: 取得查詢結果DataTable
        /// </summary>
        /// <param name="SqlString">SQL Command Text</param>
        /// <returns>查詢結果DataTable，不使用Dap，並加入 Transaction 整合控制</returns>
        public DataTable GetDataTable(string SqlString) {

            OracleConnection oraConn = m_myConn;

            DataTable dtResult = new DataTable();
            OracleCommand oraCmd = new OracleCommand(SqlString);

            try
            {
                //判斷有無啟用交易決定連線要不要開啟
                if (m_bEnableTransaction)
                {
                    oraConn = m_myTransConn;
                    oraCmd.Transaction = m_myTrans;
                }
                if (oraConn.State != ConnectionState.Open)
                {
                    oraConn.Open();
                }
                oraCmd.Connection = oraConn;
                logQuery.Debug("GetDataTable : " + SqlString);
                m_myDR = oraCmd.ExecuteReader();

                //取得資料表結構和欄位值
                if (m_myDR != null)
                {
                    for (int iCount = 0; iCount <= m_myDR.FieldCount - 1; iCount++)
                    {
                        DataColumn myColumn = new DataColumn();
                        myColumn.ColumnName = m_myDR.GetName(iCount);
                        dtResult.Columns.Add(myColumn);
                    }

                    bool bSetColumnDataType = false;
                    while (m_myDR.Read())
                    {
                        //欄位型別設定
                        //if bSetColumnDataType = False {
                        //    For i As Integer = 0 To m_myDR.FieldCount - 1
                        //        if Not IsDBNull(m_myDR.GetValue(i)) AndAlso _
                        //        dtResult.Columns(i).DataType.Equals(m_myDR.Item(i).GetType) = False {
                        //            dtResult.Columns(i).DataType = m_myDR.Item(i).GetType
                        //        }
                        //    Next
                        //    bSetColumnDataType = True
                        //}

                        m_drResult = dtResult.NewRow();
                        for (int iCount = 0; iCount <= m_myDR.FieldCount - 1; iCount++)
                        {
                            if (m_myDR[iCount] == null)
                            {
                                m_drResult[iCount] = "";
                            }
                            else
                            {
                                m_drResult[iCount] = m_myDR.GetValue(iCount);
                            }
                        }
                        dtResult.Rows.Add(m_drResult);
                    }
                    //關閉 DataReader
                    if (m_myDR.IsClosed == false)
                    {
                        m_myDR.Close();
                    }
                }

                //判斷有無啟用交易決定連線不要關閉
                if (m_bEnableTransaction == false && oraConn.State == ConnectionState.Open)
                {
                    oraConn.Close();
                }
                //if (dtResult!=null && dtResult.Rows.Count <= 3) {
                //LogObjectValue("GetDataTable", dtResult, logQuery)
                //}

                dtResult.AcceptChanges();

                return dtResult;

            }
            catch (Exception ex)
            {
                logger.Debug("GetDataSet : " + SqlString);
                //SetErrorMessage("讀取資料表時發生錯誤，請檢查！", ex)
                //有啟用交易就丟 ex 讓呼叫者rollback
                if (m_bEnableTransaction)
                {
                    throw ex;
                }
                else
                {
                    return null;
                }
            }
            finally
            {
                if (m_bEnableTransaction == false && oraConn.State == ConnectionState.Open)
                {
                    oraConn.Close();
                }
            }
        }

        /// <summary>
        /// Desc: 取得查詢結果DataTable
        /// </summary>
        /// <param name="ocmCommand">SQL Command Text</param>
        /// <returns>查詢結果DataTable，不使用Dap，並加入 Transaction 整合控制</returns>
        public DataTable GetDataTable(OracleCommand ocmCommand)
        {

            OracleConnection oraConn = m_myConn;

            DataTable dtResult = new DataTable();

            try
            {
                //判斷有無啟用交易決定連線要不要開啟
                if (m_bEnableTransaction)
                {
                    oraConn = m_myTransConn;
                    ocmCommand.Transaction = m_myTrans;
                }
                if (oraConn.State != ConnectionState.Open)
                {
                    oraConn.Open();
                }
                ocmCommand.Connection = oraConn;
                //LogObjectValue("GetDataTable : ", ocmCommand, logQuery)
                m_myDR = ocmCommand.ExecuteReader();

                //取得資料表結構和欄位值
                if (m_myDR != null)
                {
                    for (int iCount = 0; iCount <= m_myDR.FieldCount - 1; iCount++)
                    {
                        DataColumn myColumn = new DataColumn();
                        myColumn.ColumnName = m_myDR.GetName(iCount);
                        dtResult.Columns.Add(myColumn);
                    }

                    bool bSetColumnDataType = false;
                    while (m_myDR.Read())
                    {
                        //欄位型別設定
                        //if bSetColumnDataType = False {
                        //    For i As Integer = 0 To m_myDR.FieldCount - 1
                        //        if Not IsDBNull(m_myDR.GetValue(i)) AndAlso _
                        //        dtResult.Columns(i).DataType.Equals(m_myDR.Item(i).GetType) = False {
                        //            dtResult.Columns(i).DataType = m_myDR.Item(i).GetType
                        //        }
                        //    Next
                        //    bSetColumnDataType = True
                        //}

                        m_drResult = dtResult.NewRow();
                        for (int iCount = 0; iCount <= m_myDR.FieldCount - 1; iCount++)
                        {
                            if (m_myDR[iCount] == null)
                            {
                                m_drResult[iCount] = "";
                            }
                            else
                            {
                                m_drResult[iCount] = m_myDR.GetValue(iCount);
                            }
                        }
                        dtResult.Rows.Add(m_drResult);
                    }
                    //關閉 DataReader
                    if (m_myDR.IsClosed == false)
                    {
                        m_myDR.Close();
                    }
                }

                //判斷有無啟用交易決定連線不要關閉
                if (m_bEnableTransaction == false && oraConn.State == ConnectionState.Open)
                {
                    oraConn.Close();
                }
                //    if (dtResult!=null && dtResult.Rows.Count <= 3) {
                //LogObjectValue("GetDataTable", dtResult, logQuery);
                //    }

                dtResult.AcceptChanges();

                return dtResult;
            }
            catch (Exception ex)
            {
                //LogObjectValue("GetDataSet : ", ocmCommand, logQuery)
                //SetErrorMessage("讀取資料表時發生錯誤，請檢查！", ex)
                //有啟用交易就丟 ex 讓呼叫者rollback
                if (m_bEnableTransaction)
                {
                    throw ex;
                }
                else
                {
                    return null;
                }
            }
            finally
            {
                if (m_bEnableTransaction == false && oraConn.State == ConnectionState.Open)
                {
                    oraConn.Close();
                }
            }
        }

        /// <summary>
        /// Desc: 取得查詢結果的第一列DataRow
        /// </summary>
        /// <param name="SqlString">SQL Command Text</param>
        /// <returns>查詢結果DataRow</returns>
        public DataRow GetDataRow(string SqlString)
        {

            try
            {
                DataTable dtReturn = this.GetDataTable(SqlString);
                if (dtReturn == null || dtReturn.Rows.Count == 0)
                {
                    return null;
                }
                else
                {
                    return dtReturn.Rows[0];
                }

            }
            catch (Exception ex)
            {
                //SetErrorMessage("讀取資料表時發生錯誤，請檢查！", ex)
                //有啟用交易就丟 ex 讓呼叫者rollback
                if (m_bEnableTransaction)
                {
                    throw ex;
                }
                else
                {
                    return null;
                }
            }
            finally
            {
                if (m_bEnableTransaction == false && m_myConn.State == ConnectionState.Open)
                {
                    m_myConn.Close();
                }
            }
        }

        /// <summary>
        /// Desc: 取得查詢結果的第一列DataRow
        /// </summary>
        /// <param name="ocmCommand">SQL Command Text</param>
        /// <returns>查詢結果DataRow</returns>
        public DataRow GetDataRow(OracleCommand ocmCommand)
        {

            try
            {
                DataTable dtReturn = this.GetDataTable(ocmCommand);
                if (dtReturn == null || dtReturn.Rows.Count == 0)
                {
                    return null;
                }
                else
                {
                    return dtReturn.Rows[0];
                }

            }
            catch (Exception ex)
            {
                //SetErrorMessage("讀取資料表時發生錯誤，請檢查！", ex)
                //有啟用交易就丟 ex 讓呼叫者rollback
                if (m_bEnableTransaction)
                {
                    throw ex;
                }
                else
                {
                    return null;
                }
            }
            finally
            {
                if (m_bEnableTransaction == false && m_myConn.State == ConnectionState.Open)
                {
                    m_myConn.Close();
                }
            }
        }

        /// <summary>
        /// Desc: 取得單一值（第一最第一欄）、取得查詢筆數
        /// </summary>
        /// <param name="SqlString">SQL Command Text</param>
        /// <returns>查詢筆數，不使用Dap，並加入 Transaction 整合控制</returns>
        public string GetOneValue(string SqlString) {

            string sResult = "";

            Object objValue;

            OracleCommand oraCmd = new OracleCommand(SqlString);
            OracleConnection oraConn = m_myConn;
            try
            {
                if (m_bEnableTransaction)
                {
                    oraConn = m_myTransConn;
                    oraCmd.Transaction = m_myTrans;
                }
                if (oraConn.State != ConnectionState.Open)
                {
                    oraConn.Open();
                }
                oraCmd.Connection = oraConn;
                logQuery.Debug("GetOneValue : " + SqlString);
                objValue = oraCmd.ExecuteScalar();
                if (objValue.GetType() is string || objValue.GetType() is ValueType)
                {
                    try
                    {
                        sResult = objValue.ToString();
                    }
                    catch (Exception)
                    {
                        return "";
                    }
                }

                //判斷有無啟用交易決定連線不要關閉
                if (m_bEnableTransaction == false && oraConn.State == ConnectionState.Open)
                {
                    oraConn.Close();
                }

                //決定回傳值
                if (sResult == null || sResult == null)
                {
                    logQuery.Debug("GetOneValue - return Value is String.Empty !!!");
                    return null;
                }
                else
                {
                    logQuery.Debug("GetOneValue - return Value : " + sResult);
                    return sResult.ToString();
                }

            } catch (Exception ex) {
                logger.Debug("GetOneValue : " + SqlString);
                //SetErrorMessage("讀取資料表時發生錯誤，請檢查！", ex)
                //有啟用交易就丟 ex 讓呼叫者rollback
                //121213 by Yuki
                if (m_bEnableTransaction) {
                    throw ex;
                }
                else {
                    return null;
                }
            }
            finally {
                if (m_bEnableTransaction == false && oraConn.State == ConnectionState.Open) {
                    oraConn.Close();
                }
            }
            //return null;
        }

        /// <summary>
        /// Desc: 取得單一值（第一最第一欄）、取得查詢筆數
        /// </summary>
        /// <param name="ocmCommand">SQL Command Text</param>
        /// <returns>查詢筆數，不使用Dap，並加入 Transaction 整合控制</returns>
        public string GetOneValue(OracleCommand ocmCommand)
        {

            string sResult = "";

            Object objValue;

            OracleConnection oraConn = m_myConn;


            try
            {
                if (m_bEnableTransaction)
                {
                    oraConn = m_myTransConn;
                    ocmCommand.Transaction = m_myTrans;
                }
                if (oraConn.State != ConnectionState.Open)
                {
                    oraConn.Open();
                }
                ocmCommand.Connection = oraConn;
                //LogObjectValue("GetOneValue : ", ocmCommand, logQuery)
                objValue = ocmCommand.ExecuteScalar();
                if (objValue.GetType() is string || objValue.GetType() is ValueType)
                {
                    try
                    {
                        sResult = objValue.ToString();
                    }
                    catch (Exception)
                    {
                        return "";
                    }
                }
                //判斷有無啟用交易決定連線不要關閉
                if (m_bEnableTransaction == false && oraConn.State == ConnectionState.Open)
                {
                    oraConn.Close();
                }

                //決定回傳值
                if (sResult == null || sResult == null)
                {
                    logQuery.Debug("GetOneValue - return Value is String.Empty !!!");
                    return null;
                }
                else
                {
                    logQuery.Debug("GetOneValue - return Value : " + sResult);
                    return sResult.ToString();
                }
            }
            catch (Exception ex)
            {
                //LogObjectValue("GetDataSet : ", ocmCommand, logQuery)
                //SetErrorMessage("讀取資料表時發生錯誤，請檢查！", ex)
                //有啟用交易就丟 ex 讓呼叫者rollback
                if (m_bEnableTransaction)
                {
                    throw ex;
                }
                else
                {
                    return null;
                }
            }
            finally
            {
                if (m_bEnableTransaction == false && oraConn.State == ConnectionState.Open)
                {
                    oraConn.Close();
                }
            }
        }

        /// <summary>
        /// Desc: 取得查詢結果第一列的陣列
        /// </summary>
        /// <param name="SqlString">SQL Command Text</param>
        /// <returns>查詢結果陣列</returns>
        public Array GetArray(string SqlString)
        {

            try
            {
                DataTable dtReturn = this.GetDataTable(SqlString);
                if (dtReturn == null || dtReturn.Rows.Count == 0)
                {
                    return null;
                }
                else
                {
                    return dtReturn.Rows[0].ItemArray;
                }
            }
            catch (Exception ex)
            {
                //SetErrorMessage("讀取資料表時發生錯誤，請檢查！", ex)
                //有啟用交易就丟 ex 讓呼叫者rollback
                if (m_bEnableTransaction)
                {
                    throw ex;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Desc: 取得查詢結果第一列的陣列
        /// </summary>
        /// <param name="ocmCommand">SQL Command Text</param>
        /// <returns>查詢結果陣列</returns>
        public Array GetArray(OracleCommand ocmCommand)
        {
            try
            {
                DataTable dtReturn = this.GetDataTable(ocmCommand);
                if (dtReturn == null || dtReturn.Rows.Count == 0)
                {
                    return null;
                }
                else
                {
                    return dtReturn.Rows[0].ItemArray;
                }
            }
            catch (Exception ex)
            {
                //SetErrorMessage("讀取資料表時發生錯誤，請檢查！", ex)
                //有啟用交易就丟 ex 讓呼叫者rollback
                if (m_bEnableTransaction)
                {
                    throw ex;
                }
                else
                {
                    return null;
                }

            }
        }

        #endregion

        public void ResetEffectRowCount() {
            m_iEffectRowCount = 0;
        }

        public int EffectRowCount {
            get { return m_iEffectRowCount; }
            set {
                if (value > 0) {
                    m_iEffectRowCount += value;
                }
            }

        }

        /// <summary>
        /// Desc: 將傳入的DataTable每個DBNull的欄位值改為VB.NET的空值""
        /// </summary>
        public DataTable DBNull2Nothing(DataTable dtSrc) {
            for (int iCount = 0; iCount <= dtSrc.Rows.Count - 1; iCount++) {
                for (int k = 0; k <= dtSrc.Columns.Count - 1; k++)
                {
                    if (dtSrc.Columns[k].DataType.Name == "String")
                    {
                        if (dtSrc.Rows[iCount][k] == null)
                        {
                            //090724 by Leo: 把 DBNull 轉成 ""，排除換值錯誤的狀況，只寫 Log
                            try
                            {
                                dtSrc.Rows[iCount][k] = "";
                            }
                            catch (Exception ex)
                            {
                                logger.Error("DBNull2Nothing 發生錯誤");
                                return dtSrc;
                            }
                        }
                    }
                }
            }
            return dtSrc;
     }

}
}
