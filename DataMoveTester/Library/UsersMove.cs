﻿using Library.Entity.Edmx;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMoveTester.Library
{
    public class UsersMove
    {
        private static ILog logger = LogManager.GetLogger("UsersMove");
        //IAS_UsersRepository repoasUsers = RepositoryHelper.GetAS_UsersRepository();
        public bool MoveUsersData()
        {
            bool _return = true;
            DateTime _moveDateTime = DateTime.Now;
            OracleDBAccess _oracleDBA = new OracleDBAccess();
            string _sql = "select * from CUX_AD_USER";
            DataTable _dt = _oracleDBA.GetDataTable(_sql);
            //List<AS_Users> _localDB = AS_Users_Records.FindAll();
            //進行資料塞的動作   
            int _insertCount = 0;
            foreach (DataRow _dr in _dt.Rows)
            {
                //List<AS_Users> _localDB = AS_Users_Records.FindAll();
                if (AS_Users_Records.FindAll().Where(x => x.User_Code == _dr[0].ToString()).Count() == 0)
                {
                    _insertCount++;
                    AS_Users _asUsers = new AS_Users();
                    _asUsers.User_Code = _dr[0].ToString();
                    _asUsers.User_Name = _dr[1].ToString();
                    _asUsers.Branch_Code = _dr[4].ToString();
                    _asUsers.Department_Code = _dr[3].ToString();
                    _asUsers.Office_Branch = _dr[4].ToString();
                    _asUsers.Is_Active = true;
                    _asUsers.Email = _dr[0].ToString() + "landbank.com.tw";
                    if (_dr[8] != null)
                    {
                        string _s = _dr[8].ToString();
                        _asUsers.Created_By = int.Parse(_s);
                    }
                    if (_dr[6] != null)
                    {
                        string _s = _dr[6].ToString();
                        _asUsers.Last_Updated_By = int.Parse(_s);
                    }
                    _asUsers.Last_Updated_Time = _moveDateTime;
                    _asUsers.Delete_Flag = false;
                    //_asUsers.Title = _dr[2].ToString();
                    //_asUsers.Sub_Dept_Code = "0";
                    AS_Users_Records.Create(_asUsers);
                }
                //else {
                //    AS_Users _asUsers=AS_Users_Records.Find(x => x.User_Code == _dr[0].ToString()).FirstOrDefault();
                //    if (_asUsers != null && _asUsers.User_Code == _dr[0].ToString()) {
                //        //asUsers.Title= _dr[2].ToString();
                //        //asUsers.Sub_Dept_Code = "0";
                //        AS_Users_Records.Update(m => m.User_Code == _dr[0].ToString(), entity => {
                //            entity.Title = _dr[2].ToString();
                //            entity.Sub_Dept_Code ="0";
                //        });
                //    }
                //}
            }
            //logger.Debug("新增使用者筆數" + _insertCount);
            //進行資料二次整檔(整檔分行名稱和部門名稱)
            //List<AS_Users> _listASUsers = AS_Users_Records.FindAll().ToList();
            //List<AS_Code_Table> _listBranch = AS_Code_Table_Records.Find(x => x.Class == "C01").ToList();
            //List<AS_Code_Table> _listDeptment = AS_Code_Table_Records.Find(x => x.Class == "C02").ToList();
            //foreach (AS_Users _asUsers in _listASUsers) {
            //    AS_Code_Table _branch =_listBranch.Where(x => x.Code_ID == _asUsers.Branch_Code).FirstOrDefault();
            //    if(_branch!=null && _branch.Code_ID== _asUsers.Branch_Code) _asUsers.Branch_Name = _branch.Text;
            //    if (_asUsers.Branch_Code == "001")
            //    {
            //        AS_Code_Table _department = _listDeptment.Where(x => x.Code_ID == _asUsers.Department_Code).FirstOrDefault();
            //        if (_department != null && _department.Code_ID == _asUsers.Department_Code) _asUsers.Department_Name = _department.Text;
            //    }
            //    else {
            //        AS_Code_Table _department = _listBranch.Where(x => x.Code_ID == _asUsers.Branch_Code).FirstOrDefault();
            //        if (_department != null && _department.Code_ID == _asUsers.Department_Code) _asUsers.Department_Name = _department.Text;
            //    }
            //    AS_Users_Records.Update(m => m.ID == _asUsers.ID, entity => {
            //        entity.Branch_Name = _asUsers.Branch_Name;
            //        entity.Department_Name = _asUsers.Department_Name;
            //    });
            //}
            return _return;
        }

    }
}
