﻿using Library.Entity.Edmx;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMoveTester.Library
{
    public class LandMove
    {
        private static ILog logger = LogManager.GetLogger("LandMove");
        private CommonLiber m_commonLiber = new CommonLiber();
        public bool MoveLandData()
        {

            //土地主檔
            //this.MoveAsAssetsLandData();
            //this.MoveAsAssetsBuildLandData();
            //土地改良
            this.MoveAsAssetsLandMPData();
            return true;
        }
        private bool MoveAsAssetsLandData()
        {
            bool _return = true;
            DateTime _moveDateTime = DateTime.Now;
            try
            {
                OracleDBAccess _oracleDBA = new OracleDBAccess();
                string _sql = "select * from CUX_FA_ASSETS_LAND_TB";
                DataTable _dt = _oracleDBA.GetDataTable(_sql);
                //List<AS_Users> _localDB = AS_Users_Records.FindAll();
                //進行資料塞的動作   
                int _insertCount = 0;
                List<AS_Land_Code> _listASLandCode = AS_Land_Code_Records.FindAll().ToList();
                foreach (DataRow _dr in _dt.Rows)
                {
                    //List<AS_Users> _localDB = AS_Users_Records.FindAll();
                    if (AS_Assets_Land_Records.FindAll().Where(x => x.ID == int.Parse(_dr[0].ToString())).Count() == 0)
                    {
                        _insertCount++;
                        AS_Assets_Land _asLand = new AS_Assets_Land();
                        _asLand.ID = int.Parse(_dr[0].ToString());
                        _asLand.Office_Branch = _dr[2].ToString();
                       _asLand.Book_Type_Code = _dr[43].ToString();
                        _asLand.Asset_Number = _dr[82].ToString();
                        _asLand.Asset_Category_code = _dr[14].ToString();
                        _asLand.Location_Disp = _dr[79].ToString();
                        _asLand.Old_Asset_Number = _dr[57].ToString();
                        _asLand.Old_Asset_Category_code = "0";
                        _asLand.Description = _dr[42].ToString();
                        //縣市CODE先帶預設
                        string _cityName = null;
                        string _distinctName = null;
                        string _sectionName = null;
                        string _subSectionName = null;
                        if (!(_dr[4] is DBNull) && _dr[6].ToString() != null) _cityName = _dr[6].ToString();
                        if (!(_dr[5] is DBNull) && _dr[7].ToString() != null) _distinctName = _dr[7].ToString();
                        if (!(_dr[6] is DBNull) && _dr[8].ToString() != null) _sectionName = _dr[8].ToString();
                        if (!(_dr[7] is DBNull) && _dr[9].ToString() != null) _subSectionName = _dr[9].ToString();
                        _distinctName = m_commonLiber.DistrictProcess(_cityName, _distinctName);
                        _sectionName = m_commonLiber.SectionProcess(_sectionName);
                        _subSectionName = m_commonLiber.SubSectionProcess(_sectionName, _subSectionName);
                        AS_Land_Code _asLandCode = _listASLandCode.Where(x => x.City_Name == _cityName).FirstOrDefault();
                        if (_asLandCode != null && _asLandCode.ID > 0) { _asLand.City_Code = _asLandCode.City_Code; } else { _asLand.City_Code = "0"; }
                        _asLandCode = _listASLandCode.Where(x => x.City_Name == _cityName && x.District_Name ==  _distinctName).FirstOrDefault();
                        if (_asLandCode != null && _asLandCode.ID > 0) { _asLand.District_Code = _asLandCode.District_Code; } else { _asLand.District_Code = "0"; }
                        _asLandCode = _listASLandCode.Where(x => x.City_Name == _cityName && x.District_Name == _distinctName && x.Sectioni_Name == _sectionName && x.Sub_Sectioni_Name ==_subSectionName).FirstOrDefault();
                        if (_asLandCode != null && _asLandCode.ID > 0) { _asLand.Section_Code = _asLandCode.Section_Code; }
                        else { _asLand.Section_Code = "0"; }
                        //_asLand.City_Code = "0";
                        _asLand.City_Name = _dr[6].ToString();
                        //_asLand.District_Code = "0";
                        _asLand.District_Name = _dr[7].ToString();
                        //_asLand.Section_Code = "0";
                        _asLand.Section_Name = _dr[8].ToString();

                        _asLand.Sub_Section_Name = _dr[9].ToString();
                        _asLand.Parent_Land_Number = _dr[10].ToString();
                        _asLand.Filial_Land_Number = _dr[11].ToString();
                        _asLand.Authorization_Number = _dr[4].ToString();
                        _asLand.Authorized_name = _dr[5].ToString();
                        if (_dr[12] != null && !(_dr[12] is DBNull))
                        {
                            _asLand.Area_Size = decimal.Parse(_dr[12].ToString());
                        }
                        else
                        {
                            _asLand.Area_Size = 0;
                        }
                        if (_dr[76] != null && !(_dr[76] is DBNull))
                        {
                            _asLand.Authorized_Scope_Molecule = decimal.Parse(_dr[76].ToString());
                        }
                        else
                        {
                            _asLand.Authorized_Scope_Molecule = 0;
                        }
                        if (_dr[77] != null && !(_dr[77] is DBNull))
                        {
                            _asLand.Authorized_Scope_Denomminx = decimal.Parse(_dr[77].ToString());
                        }
                        else
                        {
                            _asLand.Authorized_Scope_Denomminx = 0;
                        }
                        if (_dr[13] != null && !(_dr[13] is DBNull))
                        {
                            _asLand.Authorized_Area = decimal.Parse(_dr[13].ToString());
                        }
                        else
                        {
                            _asLand.Authorized_Area = 0;
                        }
                        _asLand.Used_Type = _dr[16].ToString();
                        _asLand.Used_Status = _dr[17].ToString();
                        _asLand.Obtained_Method = _dr[19].ToString();
                        _asLand.Used_Partition = _dr[18].ToString();
                        _asLand.Is_Marginal_Land = _dr[88].ToString();
                        if (_dr[20] != null && !(_dr[20] is DBNull))
                        {
                            _asLand.Original_Cost = decimal.Parse(_dr[20].ToString());
                        }
                        else
                        {
                            _asLand.Original_Cost = 0;
                        }
                        if (_dr[23] != null && !(_dr[23] is DBNull))
                        {
                            _asLand.Deprn_Amount = decimal.Parse(_dr[23].ToString());
                        }
                        else
                        {
                            _asLand.Deprn_Amount = 0;
                        }
                        if (_dr[24] != null && !(_dr[24] is DBNull))
                        {
                            _asLand.Reval_Land_VAT = decimal.Parse(_dr[24].ToString());
                        }
                        else
                        {
                            _asLand.Reval_Land_VAT = 0;
                        }
                        if (_dr[25] != null && !(_dr[25] is DBNull))
                        {
                            _asLand.Reval_Adjustment_Amount = decimal.Parse(_dr[25].ToString());
                        }
                        else
                        {
                            _asLand.Reval_Adjustment_Amount = 0;
                        }
                        if (_dr[26] != null && !(_dr[26] is DBNull))
                        {
                            _asLand.Business_Area_Size = decimal.Parse(_dr[26].ToString());
                        }
                        else
                        {
                            _asLand.Business_Area_Size = 0;
                        }
                        if (_dr[29] != null && !(_dr[29] is DBNull))
                        {
                            _asLand.NONBusiness_Area_Size = decimal.Parse(_dr[29].ToString());
                        }
                        else
                        {
                            _asLand.NONBusiness_Area_Size = 0;
                        }
                        if (_dr[21] != null && !(_dr[21] is DBNull))
                        {
                            _asLand.Current_Cost = decimal.Parse(_dr[29].ToString());
                        }
                        else
                        {
                            _asLand.Current_Cost = 0;
                        }
                        if (_dr[22] != null && !(_dr[22] is DBNull))
                        {
                            _asLand.Reval_Reserve = decimal.Parse(_dr[29].ToString());
                        }
                        else
                        {
                            _asLand.Reval_Reserve = 0;
                        }
                        if (_dr[27] != null && !(_dr[27] is DBNull))
                        {
                            _asLand.Business_Book_Amount = decimal.Parse(_dr[27].ToString());
                        }
                        else
                        {
                            _asLand.Business_Book_Amount = 0;
                        }
                        if (_dr[30] != null && !(_dr[30] is DBNull))
                        {
                            _asLand.NONBusiness_Book_Amount = decimal.Parse(_dr[30].ToString());
                        }
                        else
                        {
                            _asLand.NONBusiness_Book_Amount = 0;
                        }
                        if (_dr[28] != null && !(_dr[28] is DBNull))
                        {
                            _asLand.Business_Reval_Reserve = decimal.Parse(_dr[28].ToString());
                        }
                        else
                        {
                            _asLand.Business_Reval_Reserve = 0;
                        }
                        if (_dr[31] != null && !(_dr[31] is DBNull))
                        {
                            _asLand.NONBusiness_Reval_Reserve = decimal.Parse(_dr[31].ToString());
                        }
                        else
                        {
                            _asLand.NONBusiness_Reval_Reserve = 0;
                        }
                        _asLand.Datetime_Placed_In_Service = DateTime.Parse(_dr[44].ToString());
                        if (_dr[32] != null && !(_dr[32] is DBNull))
                        {
                            _asLand.Year_Number = decimal.Parse(_dr[32].ToString());
                        }
                        else
                        {
                            _asLand.Year_Number = 0;
                        }
                        if (_dr[33] != null && !(_dr[33] is DBNull))
                        {
                            _asLand.Announce_Amount = decimal.Parse(_dr[33].ToString());
                        }
                        else
                        {
                            _asLand.Announce_Amount = 0;
                        }
                        if (_dr[34] != null && !(_dr[34] is DBNull))
                        {
                            _asLand.Announce_Price = decimal.Parse(_dr[34].ToString());
                        }
                        else
                        {
                            _asLand.Announce_Price = 0;
                        }
                        if (_dr[35] != null && !(_dr[35] is DBNull))
                        {
                            _asLand.Tax_Type = _dr[35].ToString();
                        }
                        else
                        {
                            _asLand.Tax_Type = "0";
                        }
                        if (_dr[36] != null && !(_dr[36] is DBNull))
                        {
                            _asLand.Reduction_reason = _dr[36].ToString();
                        }
                        else
                        {
                            _asLand.Reduction_reason = "0";
                        }
                        if (_dr[37] != null && !(_dr[37] is DBNull))
                        {
                            _asLand.Reduction_Area = decimal.Parse(_dr[34].ToString());
                        }
                        else
                        {
                            _asLand.Reduction_Area = 0;
                        }
                        if (_dr[38] != null && !(_dr[38] is DBNull))
                        {
                            _asLand.Declared_Price = decimal.Parse(_dr[34].ToString());
                        }
                        else
                        {
                            _asLand.Declared_Price = 0;
                        }
                        if (_dr[39] != null && !(_dr[39] is DBNull))
                        {
                            _asLand.Dutiable_Amount = decimal.Parse(_dr[39].ToString());
                        }
                        else
                        {
                            _asLand.Dutiable_Amount = 0;
                        }
                        if (_dr[40] != null && !(_dr[40] is DBNull)) { _asLand.Remark1 = _dr[40].ToString(); }
                        else
                        {
                            _asLand.Remark1 = "0";
                        }
                        if (_dr[41] != null && !(_dr[41] is DBNull)) { _asLand.Remark2 = _dr[41].ToString(); }
                        else
                        {
                            _asLand.Remark2 = "0";
                        }
                        if (_dr[45] != null && !(_dr[45] is DBNull))
                        {
                            _asLand.Current_units = decimal.Parse(_dr[45].ToString());
                        }
                        else
                        {
                            _asLand.Current_units = 0;
                        }
                        if (_dr[85] != null && !(_dr[85] is DBNull))
                        {
                            _asLand.Retire_Cost = decimal.Parse(_dr[85].ToString());
                        }
                        else
                        {
                            _asLand.Retire_Cost = 0;
                        }
                        if (_dr[86] != null && !(_dr[86] is DBNull))
                        {
                            _asLand.Sell_Amount = decimal.Parse(_dr[86].ToString());
                        }
                        else
                        {
                            _asLand.Sell_Amount = 0;
                        }
                        if (_dr[87] != null && !(_dr[87] is DBNull))
                        {
                            _asLand.Sell_Cost = decimal.Parse(_dr[87].ToString());
                        }
                        else
                        {
                            _asLand.Sell_Cost = 0;
                        }
                        if (_dr[75] != null && !(_dr[75] is DBNull))
                        {
                            _asLand.Transfer_Price = decimal.Parse(_dr[75].ToString());
                        }
                        else
                        {
                            _asLand.Transfer_Price = 0;
                        }
                        //_asBuild.Sub_Sectioni_Name = _dr[3].ToString();
                        if (_dr[54] != null)
                        {
                            _asLand.Created_By = _dr[54].ToString();
                        }
                        if (_dr[52] != null)
                        {
                            //_asLand.Last_UpDatetimed_By = _dr[52].ToString();
                        }
                        _asLand.Create_Time = _moveDateTime;
                        _asLand.Last_UpDatetimed_Time = _moveDateTime;

                        //順利轉資料先亂塞
                        _asLand.Urban_Renewal = "0";
                        _asLand.Own_Area = 0;
                        _asLand.NONOwn_Area = 0;
                        _asLand.CROP = "0";
                        _asLand.Delete_Reason = "0";
                        _asLand.Land_Item = "0";
                        AS_Assets_Land_Records.Create(_asLand);
                    }
                }
                logger.Debug("新增AS_Assets_Land筆數" + _insertCount);
            }
            catch (Exception ex)
            {
                logger.Debug(ex.Message.ToString());
            }
            return _return;
        }
        private bool MoveAsAssetsLandMPData()
        {
            bool _return = true;
            DateTime _moveDateTime = DateTime.Now;
            OracleDBAccess _oracleDBA = new OracleDBAccess();
            //string _sql = "select * from CUX_FA_ASSETS_BUILD";
            string _sql = "select * from cux_fa_assets_mp where " +
                " Asset_seq_id not in (select Asset_seq_id from cux_fa_assets_mp where " +
" (asset_category_code like '3%' or asset_category_code like '4%' or " +
" asset_category_code like '5%' or asset_category_code like '8%') ) " +
" And Asset_seq_id not in (50899,50904,50905) " +
" and not (70472 < Asset_seq_id  and Asset_seq_id < 70645) and not (76693 < Asset_seq_id  and Asset_seq_id < 83087) " +
" and asset_category_code like '9%' and length(Parent_Asset_Number)=3 ";
            DataTable _dt = _oracleDBA.GetDataTable(_sql);
            //List<AS_Users> _localDB = AS_Users_Records.FindAll();
            //進行資料塞的動作   
            int _insertCount = 0;
            //List<AS_Assets_Build> _listasAssetsBuild = AS_Assets_Build_Records.FindAll().ToList();
            foreach (DataRow _dr in _dt.Rows)
            {
                //List<AS_Users> _localDB = AS_Users_Records.FindAll();
                if (AS_Assets_Land_MP_Records.FindAll().Where(x => x.Old_Asset_Number == _dr[2].ToString()).Count() == 0)
                {
                    _insertCount++;
                    AS_Assets_Land_MP _asLandMP = new AS_Assets_Land_MP();
                    _asLandMP.Assets_Land_ID = 0;
                    string[] _spliteString = _dr[2].ToString().Split('-');
                    string _first = _spliteString[0].Substring(0, 1);
                    string _second = _spliteString[0].Substring(1, _spliteString[0].Length - 1).PadLeft(6, '0');
                    _asLandMP.Asset_Number = _first + _second + "-" + _spliteString[1];
                    _asLandMP.Parent_Asset_Number = _dr[26].ToString();
                    _asLandMP.Asset_Category_code = _dr[5].ToString();
                    _asLandMP.Old_Asset_Number = _dr[2].ToString();
                    _asLandMP.Book_Type_Code = _dr[19].ToString();
                    if (_dr[16].ToString() != null && !(_dr[16] is DBNull))
                    {
                        _asLandMP.Date_Placed_In_Service = DateTime.Parse(_dr[16].ToString());
                        _asLandMP.Datetime_Placed_In_Service = DateTime.Parse(_dr[16].ToString());
                    }
                    else {
                        _asLandMP.Date_Placed_In_Service = _moveDateTime;
                        _asLandMP.Datetime_Placed_In_Service = _moveDateTime;
                    }
                    _asLandMP.Current_units = decimal.Parse(_dr[14].ToString());
                    _asLandMP.Deprn_Method_Code = _dr[20].ToString();
                    if (_dr[21] != null && !(_dr[21] is DBNull))
                    {
                        _asLandMP.Life_Years = int.Parse(_dr[21].ToString());
                    }
                    else
                    {
                        _asLandMP.Life_Years = 0;
                    }
                    if (_dr[22] != null && !(_dr[22] is DBNull))
                    {
                        _asLandMP.Life_Months = int.Parse(_dr[22].ToString());
                    }
                    else
                    {
                        _asLandMP.Life_Months = 0;
                    }
                    //Life_Years和Months要減一個月
                    if (_asLandMP.Life_Years > 0 && _asLandMP.Life_Months == 0)
                    {
                        _asLandMP.Life_Years = _asLandMP.Life_Years - 1;
                        _asLandMP.Life_Months = 11;
                    }
                    else if (_asLandMP.Life_Months > 0)
                    {
                        _asLandMP.Life_Months = _asLandMP.Life_Months - 1;
                    }
                    if (_dr[25] != null && !(_dr[25] is DBNull))
                    {
                        _asLandMP.Location_Disp = _dr[25].ToString();
                    }
                    else
                    {
                        _asLandMP.Location_Disp = "0";
                    }
                    if (_dr[17] != null && !(_dr[17] is DBNull))
                    {
                        _asLandMP.PO_Number = _dr[17].ToString();
                    }
                    else
                    {
                        _asLandMP.PO_Number = "0";
                    }
                    if (_dr[18] != null && !(_dr[18] is DBNull))
                    {
                        _asLandMP.PO_Destination = _dr[18].ToString();
                    }
                    else
                    {
                        _asLandMP.PO_Destination = "0";
                    }
                    if (_dr[6] != null && !(_dr[6] is DBNull))
                    {
                        _asLandMP.Model_Number = _dr[6].ToString();
                    }
                    else
                    {
                        _asLandMP.Model_Number = "0";
                    }
                    if (_dr[15] != null && !(_dr[15] is DBNull))
                    {
                        DateTime _date = DateTime.Parse(_dr[15].ToString());
                        if (_date.Year < 1000)
                        {
                            _asLandMP.Transaction_Date = new DateTime(_date.Year + 1911, _date.Month, _date.Day);
                        }
                        else
                        {
                            _asLandMP.Transaction_Date = _date;
                        }
                    }
                    else
                    {
                        _asLandMP.Transaction_Date = _moveDateTime;
                    }
                    if (_dr[3] != null && !(_dr[3] is DBNull))
                    {
                        _asLandMP.Assets_Unit = _dr[3].ToString();
                    }
                    else
                    {
                        _asLandMP.Assets_Unit = "0";
                    }
                    if (_dr[7] != null && !(_dr[7] is DBNull))
                    {
                        _asLandMP.Accessory_Equipment = _dr[7].ToString();
                    }
                    else
                    {
                        _asLandMP.Accessory_Equipment = "0";
                    }
                    if (_dr[61] != null && !(_dr[61] is DBNull))
                    {
                        _asLandMP.Assigned_NUM = _dr[61].ToString();
                    }
                    else
                    {
                        _asLandMP.Assigned_NUM = "0";
                    }
                    if (_dr[28] != null && !(_dr[28] is DBNull))
                    {
                        _asLandMP.Asset_Category_NUM = _dr[28].ToString();
                    }
                    else
                    {
                        _asLandMP.Asset_Category_NUM = "0";
                    }
                    if (_dr[62] != null && !(_dr[62] is DBNull))
                    {
                        _asLandMP.Asset_Category_Name = _dr[62].ToString();
                    }
                    else
                    {
                        _asLandMP.Asset_Category_Name = "0";
                    }
                    if (_dr[27] != null && !(_dr[27] is DBNull))
                    {
                        _asLandMP.Asset_Structure = _dr[27].ToString();
                    }
                    else
                    {
                        _asLandMP.Asset_Structure = "0";
                    }
                    if (_dr[59] != null && !(_dr[59] is DBNull))
                    {
                        _asLandMP.Current_Cost = decimal.Parse(_dr[59].ToString());
                    }
                    else
                    {
                        _asLandMP.Current_Cost = 0;
                    }
                    if (_dr[9] != null && !(_dr[9] is DBNull))
                    {
                        _asLandMP.Deprn_Reserve = decimal.Parse(_dr[9].ToString());
                    }
                    else
                    {
                        _asLandMP.Deprn_Reserve = 0;
                    }
                    if (_dr[11] != null && !(_dr[11] is DBNull))
                    {
                        _asLandMP.Salvage_Value = decimal.Parse(_dr[11].ToString());
                    }
                    else
                    {
                        _asLandMP.Salvage_Value = 0;
                    }
                    if (_dr[13] != null && !(_dr[13] is DBNull))
                    {
                        _asLandMP.Remark = _dr[13].ToString();
                    }
                    else
                    {
                        _asLandMP.Remark = "0";
                    }
                    if (_asLandMP.Deprn_Method_Code == "STL" || _asLandMP.Deprn_Method_Code == "FSTL")
                    {
                        _asLandMP.Deprn_Type = 1;
                    }
                    else
                    {
                        _asLandMP.Deprn_Type = 0;
                    }
                    if (_dr[51] != null && !(_dr[51] is DBNull))
                    {
                        _asLandMP.Officer_Branch = _dr[51].ToString();
                    }
                    else {
                        _asLandMP.Officer_Branch = "000";
                    }
                    //if (_dr[4] != null && !(_dr[4] is DBNull))
                    //{
                    //    _asLandMP.Description = _dr[4].ToString();
                    //}
                    //else
                    //{
                    //    _asLandMP.Description = "0";
                    //}
                    if (_dr[32] != null)
                    {
                        _asLandMP.Created_By = decimal.Parse(_dr[32].ToString());
                    }
                    if (_dr[30] != null)
                    {
                        _asLandMP.Last_UpDatetimed_By = decimal.Parse(_dr[30].ToString());
                    }
                    if (_dr[31] != null && !(_dr[31] is DBNull))
                    {
                        _asLandMP.Create_Time = DateTime.Parse(_dr[31].ToString());
                    }
                    else
                    {
                        _asLandMP.Create_Time = _moveDateTime;
                    }
                    if (_dr[29] != null && !(_dr[29] is DBNull))
                    {
                        _asLandMP.Last_UpDatetimed_Time = DateTime.Parse(_dr[29].ToString());
                    }
                    else
                    {
                        _asLandMP.Last_UpDatetimed_Time = _moveDateTime;
                    }
                    AS_Assets_Land_MP_Records.Create(_asLandMP);
                }
            }
            logger.Debug("新增AS_Assets_Land_MP筆數" + _insertCount);
            return _return;
        }
    }
}
