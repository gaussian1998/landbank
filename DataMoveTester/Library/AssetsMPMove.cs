﻿using Library.Entity.Edmx;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMoveTester.Library
{
    public class AssetsMPMove
    {
        private static ILog logger = LogManager.GetLogger("AssetsMPMove");
        IAS_Assets_MPRepository repoasAssetsMP = RepositoryHelper.GetAS_Assets_MPRepository();
        List<AS_GL_Trade_Acc_Sub> m_listASGLTradeAccSub = AS_GL_Trade_Acc_Sub_Records.FindAll().ToList();
        public bool MoveAssetsMPData() {
            
            //動產主檔
            this.MoveAsAssetsMPData();
            //this.MoveAsAssetsBuildLandData();
            return true;
        }
        private bool MoveAsAssetsMPData()
        {
            bool _return = true;
            DateTime _moveDateTime = DateTime.Now;
            try
            {
                OracleDBAccess _oracleDBA = new OracleDBAccess();
                string _sql = "select * from cux_fa_assets_mp where Asset_seq_id not in (50899,50904,50905) and not (70472 < Asset_seq_id  and Asset_seq_id < 70645) and not (76693 < Asset_seq_id  and Asset_seq_id < 83087) and (asset_category_code like '3%' or asset_category_code like '4%' or " +
" asset_category_code like '5%' or asset_category_code like '8%') order by Asset_seq_id";
                //                string _sql = " select *  from cux_fa_assets_mp where Asset_seq_id not in (50899,50904,50905) and not (70472 < Asset_seq_id  and Asset_seq_id < 70645) and not " +
                //                           " (76693 < Asset_seq_id  and Asset_seq_id < 83087) and "+
                //" (asset_category_code like '3%' or asset_category_code like '4%' or asset_category_code like '5%' or asset_category_code like '8%') and Asset_seq_id>87862 order by Asset_seq_id ";
                DataTable _dt = _oracleDBA.GetDataTable(_sql);
                List<AS_Assets_MP> _listASAssetsMP = AS_Assets_MP_Records.FindAll().ToList();
                //List<AS_Users> _localDB = AS_Users_Records.FindAll();
                //進行資料塞的動作   
                int _insertCount = 0;
                foreach (DataRow _dr in _dt.Rows)
                {
                    //List<AS_Users> _localDB = AS_Users_Records.FindAll();
                    if (_listASAssetsMP.Where(x => x.ID == int.Parse(_dr[0].ToString())).Count() == 0)
                    {
                        _insertCount++;
                        AS_Assets_MP _asAssetsMP = new AS_Assets_MP();
                        _asAssetsMP.ID = int.Parse(_dr[0].ToString());
                        if (_dr[1] != null && !(_dr[1] is DBNull))
                        {
                            _asAssetsMP.Assets_ID = int.Parse(_dr[1].ToString());
                        }
                        else {
                            _asAssetsMP.Assets_ID = 0;
                        }
                        string[] _spliteString= _dr[2].ToString().Split('-');
                        string _first = _spliteString[0].Substring(0, 1);
                        string _second = _spliteString[0].Substring(1, _spliteString[0].Length-1).PadLeft(6, '0');

                        _asAssetsMP.Asset_Number = _first + _second + "-" + _spliteString[1];
                        _asAssetsMP.Assets_Original_ID = _dr[2].ToString();
                        if (_dr[28] != null && !(_dr[28] is DBNull))
                        {
                            _asAssetsMP.Assets_Category_ID = _dr[28].ToString();
                        }
                        else
                        {
                            _asAssetsMP.Assets_Category_ID = "0";
                        }
                        _asAssetsMP.Assets_Name = _dr[4].ToString();
                        _asAssetsMP.Assets_Alias = "0";
                        _asAssetsMP.Assets_Unit = int.Parse(_dr[14].ToString());
                        if (_dr[26] != null && !(_dr[26] is DBNull))
                        {
                            _asAssetsMP.Assets_Parent_Number = _dr[26].ToString();
                        }
                        else
                        {
                            _asAssetsMP.Assets_Parent_Number = "0";
                        }
                        string _officerBranch = "0";
                        if (_dr[51] != null && !(_dr[51] is DBNull))
                        {
                            _officerBranch = _dr[51].ToString();
                            _asAssetsMP.Assigned_Branch = _dr[51].ToString();
                        }
                        else {
                            _asAssetsMP.Assigned_Branch = "0";
                        }
                        _asAssetsMP.Assigned_ID = "0";
                        if (_officerBranch != "0" && _officerBranch.Substring(0, 1) == "5")
                        {
                            _asAssetsMP.IsOversea = "1";
                        }
                        else {
                            _asAssetsMP.IsOversea = "0";
                        }
                         _spliteString = _dr[5].ToString().Split('.');
                        _asAssetsMP.Asset_Category_Code = _spliteString[0] + "."+ _spliteString[1].PadLeft(6, '0') + "." + _spliteString[2];
                        //_asAssetsMP.Asset_Category_Code = _dr[5].ToString();
                        _asAssetsMP.Location_Disp = _dr[55].ToString();
                        //來源
                        if (_officerBranch == "001")
                        {
                            _asAssetsMP.Source = "H";
                        }
                        else
                        {
                            _asAssetsMP.Source = "B";
                        }
                        if (_dr[21] != null && !(_dr[21] is DBNull))
                        {
                            _asAssetsMP.Life_Years = int.Parse(_dr[21].ToString());
                        }
                        else
                        {
                            _asAssetsMP.Life_Years = 0;
                        }
                        if (_dr[22] != null && !(_dr[22] is DBNull))
                        {
                            _asAssetsMP.LIFE_MONTHS = int.Parse(_dr[22].ToString());
                        }
                        else
                        {
                            _asAssetsMP.LIFE_MONTHS = 0;
                        }
                        //Life_Years和Months要減一個月
                        if (_asAssetsMP.Life_Years > 0 && _asAssetsMP.LIFE_MONTHS == 0)
                        {
                            _asAssetsMP.Life_Years = _asAssetsMP.Life_Years - 1;
                            _asAssetsMP.LIFE_MONTHS = 11;
                        }
                        else if (_asAssetsMP.LIFE_MONTHS > 0)
                        {
                            _asAssetsMP.LIFE_MONTHS = _asAssetsMP.LIFE_MONTHS - 1;
                        }

                        if (_dr[20] != null && !(_dr[20] is DBNull))
                        {
                            _asAssetsMP.Deprn_Method_Code = _dr[20].ToString();
                        }
                        else
                        {
                            _asAssetsMP.Deprn_Method_Code = "0";
                        }
                        if (_dr[16] != null && !(_dr[16] is DBNull))
                        {
                            _asAssetsMP.Date_Placed_In_Service = DateTime.Parse(_dr[16].ToString());
                        }
                        //oracle 資料保管人都為空
                        
                        if (_dr[17] != null && !(_dr[17] is DBNull))
                        {
                            _asAssetsMP.PO_Number = _dr[17].ToString();
                        }
                        else
                        {
                            _asAssetsMP.PO_Number = "0";
                        }
                        if (_dr[18] != null && !(_dr[18] is DBNull))
                        {
                            _asAssetsMP.PO_Description = _dr[18].ToString();
                        }
                        else
                        {
                            _asAssetsMP.PO_Description = "0";
                        }
                        if (_dr[6] != null && !(_dr[6] is DBNull))
                        {
                            _asAssetsMP.Model_Number = _dr[6].ToString();
                        }
                        else
                        {
                            _asAssetsMP.Model_Number = "0";
                        }
                        if (_dr[15] != null && !(_dr[15] is DBNull))
                        {
                            DateTime _date = DateTime.Parse(_dr[15].ToString());
                            if (_date.Year < 1000)
                            {
                                _asAssetsMP.Transaction_Date = new DateTime(_date.Year + 1911, _date.Month, _date.Day);
                            }
                            else
                            {
                                _asAssetsMP.Transaction_Date = _date;
                            }
                        }
                        if (_dr[51] != null && !(_dr[51] is DBNull))
                        {
                            _asAssetsMP.Office_Branch = _dr[51].ToString();
                        }
                        else
                        {
                            _asAssetsMP.Office_Branch = "000";
                        }

                        if (_dr[8] != null && !(_dr[8] is DBNull))
                        {
                            _asAssetsMP.Assets_Fixed_Cost = decimal.Parse(_dr[8].ToString());
                        }
                        if (_dr[9] != null && !(_dr[9] is DBNull))
                        {
                            _asAssetsMP.Deprn_Reserve = decimal.Parse(_dr[9].ToString());
                        }
                        if (_dr[9] != null && !(_dr[9] is DBNull))
                        {
                            _asAssetsMP.Salvage_Value = decimal.Parse(_dr[9].ToString());
                        }
                        if (_dr[13] != null && !(_dr[13] is DBNull)) {
                            _asAssetsMP.Remark = _dr[13].ToString();
                        }
                        if (_dr[4] != null && !(_dr[4] is DBNull))
                        {
                            _asAssetsMP.Description = _dr[4].ToString();
                        }
                        else
                        {
                            _asAssetsMP.Description = "0";
                        }
                        if (_dr[32] != null)
                        {
                            _asAssetsMP.Created_By = _dr[32].ToString();
                        }
                        if (_dr[30] != null)
                        {
                            _asAssetsMP.Last_Updated_By = _dr[30].ToString();
                        }
                        if (_dr[31] != null && !(_dr[31] is DBNull))
                        {
                            _asAssetsMP.Create_Time = DateTime.Parse(_dr[31].ToString());
                        }
                        else
                        {
                            _asAssetsMP.Create_Time = _moveDateTime;
                        }
                        if (_dr[29] != null && !(_dr[29] is DBNull))
                        {
                            _asAssetsMP.Last_Updated_Time = DateTime.Parse(_dr[29].ToString());
                        }
                        else
                        {
                            _asAssetsMP.Last_Updated_Time = _moveDateTime;
                        }
                        //_asAssetsMP.Last_Updated_Time = _moveDateTime;
                        if (_dr[7] != null && !(_dr[7] is DBNull))
                        {
                            _asAssetsMP.ACCESSORY_EQUIPMENT = _dr[7].ToString();
                        }
                        if (_dr[10] != null && !(_dr[10] is DBNull))
                        {
                            _asAssetsMP.YTD_DEPRN = decimal.Parse(_dr[10].ToString());
                        }
                        if (_dr[19] != null && !(_dr[19] is DBNull)) {
                            _asAssetsMP.BOOK_TYPE_CODE = _dr[19].ToString();
                        }
                        string _assetsCategoryCode = _asAssetsMP.Asset_Category_Code.Split('.')[1];
                        _asAssetsMP.EXPENSE_ACCOUNT = m_listASGLTradeAccSub.Where(x => x.Asset_Category_code == _assetsCategoryCode
                        && x.Master_No >= decimal.Parse("303130") && x.Master_No <= decimal.Parse("303150")).FirstOrDefault().Account;
                        _asAssetsMP.Asset_Account = m_listASGLTradeAccSub.Where(x => x.Asset_Category_code == _assetsCategoryCode
                       && x.Master_No >= decimal.Parse("301300") && x.Master_No <= decimal.Parse("301500")).FirstOrDefault().Account;

                        if (_dr[26] != null && !(_dr[26] is DBNull))
                        {
                           _asAssetsMP.PARENT_ASSET_NUMBER_G=  _dr[26].ToString();
                        }
                        if (_dr[27] != null && !(_dr[27] is DBNull))
                        {
                            _asAssetsMP.ASSET_STRUCTURE = _dr[27].ToString();
                        }
                        if (_dr[34] != null && !(_dr[34] is DBNull))
                        {
                            _asAssetsMP.ATTRIBUTE_CATEGORY_CODE = _dr[34].ToString();
                        }
                        if (_dr[52] != null && !(_dr[52] is DBNull))
                        {
                            _asAssetsMP.BUSINESS_REVAL_RESERVE = decimal.Parse(_dr[52].ToString());
                        }
                        if (_dr[53] != null && !(_dr[53] is DBNull))
                        {
                            _asAssetsMP.NONBUSINESS_REVAL_RESERVE = decimal.Parse(_dr[53].ToString());
                        }
                        if (_dr[59] != null && !(_dr[59] is DBNull))
                        {
                            _asAssetsMP.CURRENT_COST = decimal.Parse(_dr[59].ToString());
                        }
                        if (_dr[61] != null && !(_dr[61] is DBNull))
                        {
                            _asAssetsMP.ASSIGNED_Name =_dr[61].ToString();
                        }
                        if (_dr[62] != null && !(_dr[62] is DBNull))
                        {
                            _asAssetsMP.ASSET_CATEGORY_NAME = _dr[62].ToString();
                        }
                        if (_dr[63] != null && !(_dr[63] is DBNull))
                        {
                            _asAssetsMP.UNIT_PRICE = decimal.Parse(_dr[63].ToString());
                        }
                        if (_dr[64] != null && !(_dr[64] is DBNull))
                        {
                            _asAssetsMP.PO_NAME = _dr[64].ToString();
                        }
                        if (_dr[65] != null && !(_dr[65] is DBNull))
                        {
                            _asAssetsMP.DATE_CHECK = DateTime.Parse(_dr[65].ToString());
                            int _year = ((DateTime)_asAssetsMP.DATE_CHECK).Year;
                            if (_year < 1900) {
                                _year = _year + 1911;
                                int _month= ((DateTime)_asAssetsMP.DATE_CHECK).Month;
                                int _day= ((DateTime)_asAssetsMP.DATE_CHECK).Day;
                                _asAssetsMP.DATE_CHECK = DateTime.Parse(_year + "/" + _month + "/" + _day);
                            }
                        }
                        if (_asAssetsMP.Life_Years > 0 || _asAssetsMP.LIFE_MONTHS > 0)
                        {
                            DateTime _endDate = DateTime.Now;
                            DateTime _startDate = DateTime.Now;

                            if (_asAssetsMP.Date_Placed_In_Service != null)
                            {
                                if (((DateTime)_asAssetsMP.Date_Placed_In_Service).Day > 20)
                                {
                                    _startDate = ((DateTime)_asAssetsMP.Date_Placed_In_Service).AddMonths(1); 
                                }
                                else
                                {
                                    _startDate = (DateTime)_asAssetsMP.Date_Placed_In_Service;
                                }
                                _asAssetsMP.DEPRN_Counts = ((_endDate.Year - _startDate.Year) * 12) + (_endDate.Month - _startDate.Month);
                            }
                            else if (_asAssetsMP.Create_Time != null)
                            {
                                if (_asAssetsMP.Create_Time.Day > 20)
                                {
                                    _startDate = _asAssetsMP.Create_Time.AddMonths(1);
                                    
                                }
                                else
                                {
                                    _startDate = _asAssetsMP.Create_Time;
                                }
                                _asAssetsMP.DEPRN_Counts = ((_endDate.Year - _startDate.Year) * 12) + (_endDate.Month - _startDate.Month);
                            }
                        }
                        if (_asAssetsMP.Deprn_Method_Code == "STL" || _asAssetsMP.Deprn_Method_Code == "FSTL")
                        {
                            _asAssetsMP.Not_Deprn_Flag = false;
                        }
                        else {
                            _asAssetsMP.Not_Deprn_Flag = true;
                        }
                           
                        //順利轉資料先亂塞
                        _asAssetsMP.Currency = "0";
                        repoasAssetsMP.Add(_asAssetsMP);
                        //後期新增加欄位
                       

                        if (_insertCount % 10000 == 0) {
                            repoasAssetsMP.UnitOfWork.Commit();
                        }
                    }
                }
                if (_insertCount % 10000 > 0)
                {
                    repoasAssetsMP.UnitOfWork.Commit();
                }
                logger.Debug("新增AS_Assets_MP筆數" + _insertCount);
            }
            catch (Exception ex) {
                logger.Debug(ex.Message.ToString());
            }
            return _return;
        }
    }
}
