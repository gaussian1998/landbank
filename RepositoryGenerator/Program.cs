﻿using System;
using System.IO;

namespace RepositoryGenerator
{
    class Program
    {
        // .\ -> ...RepositoryGenerator\bin\Debug\
        public static string EdmxFolder = "../../../Library.Entity/Edmx";
        public static string RepositoryFolder = ".\\";
        //public static string RepositoryFolder = "..\\..\\..\\Library.Servant\\Repository\\";

        public static string[] ModelNames = { "IndexItemViewModel", "DetailsViewModel", "CreateViewModel",  "UpdateViewModel" };

        static void Main(string[] args)
        {
            // 取得資料夾內所有檔案
            foreach (string fname in System.IO.Directory.GetFiles(EdmxFolder))
            {
                if (fname.Split('\\')[1].Substring(0, 3).Equals("AS_"))
                {
                    string[] lines = System.IO.File.ReadAllLines(@fname);

                    // 取得Model 名稱
                    string fileName = fname.Split('\\')[1].Replace(".cs", "");
                    Console.WriteLine("\n===== Start Creating Entity \""+ fileName + "\" =====");
                    // 建立Local_XXX_Repository.cs
                    CreateLocalRepository(fileName);
                    // 建立Remote_XXX_Repository.cs
                    CreateRemoteRepository(fileName);

                    string ModelContent = "";
                    foreach (string line in lines)
                    {
                        if (line.Contains("{ get; set; }") && !line.Contains("public virtual"))
                        {
                            // Use a tab to indent each line of the file.
                            ModelContent += "\t" + line + "\n";
                        }
                    }
                    // 建立四個Model
                    CreateModels(fileName, ModelContent);
                }
            }
                
            // Keep the console window open in debug mode.
            Console.WriteLine("Press any key to exit.");
            System.Console.ReadKey();
        }

        public static void CreateLocalRepository(string entityName)
        {
            string fileName = entityName.Substring(3);
            string folderName = @RepositoryFolder + fileName;

            System.IO.Directory.CreateDirectory(folderName);
            // To create a string that specifies the path to a subfolder under your 
            // top-level folder, add a name for the subfolder to folderName.
            string pathString = System.IO.Path.Combine(folderName, "Local_" + fileName + "_Repository.cs");
            
            // Check that the file doesn't already exist. If it doesn't exist, create
            // the file and write integers 0 - 99 to it.
            // DANGER: System.IO.File.Create will overwrite the file if it already exists.
            // This could happen even with random file names, although it is unlikely.
            if (!System.IO.File.Exists(pathString))
            {
                using (System.IO.FileStream fs = System.IO.File.Create(pathString)) { };
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@pathString))
                {
                    file.WriteLine("using System; ");
                    file.WriteLine("using System.Linq.Expressions;");
                    file.WriteLine("using Library.Entity.Edmx;");
                    file.WriteLine("using Library.Servant.Repository." + fileName + ".Models;");
                    file.WriteLine("");
                    file.WriteLine("namespace Library.Servant.Repository." + fileName);
                    file.WriteLine("{ ");
                    file.WriteLine("\t public class Local_" + fileName + "_Repository : LocalGenericRepository<" + entityName + ", IndexItemViewModel, DetailsViewModel, CreateViewModel, UpdateViewModel>");
                    file.WriteLine("\t {");
                    file.WriteLine("\t\t public override Expression<Func<" + entityName + ", bool>> Predicate(UpdateViewModel model)");
                    file.WriteLine("\t\t {");
                    file.WriteLine("\t\t\t return Expression(m => m.ID == model.ID);");
                    file.WriteLine("\t\t }");
                    file.WriteLine("\t\t public override Expression<Func<" + entityName + ", bool>> Predicate(int ID)");
                    file.WriteLine("\t\t {");
                    file.WriteLine("\t\t\t return Expression(m => m.ID == ID);");
                    file.WriteLine("\t\t }");
                    file.WriteLine("\t }");
                    file.WriteLine("}");
                }
                // Verify the path that you have constructed.
                Console.WriteLine("{0} Created.", "Local_" + fileName + "_Repository.cs");
            }
            else
            {
                Console.WriteLine("File \"{0}\" already exists.", entityName);
                return;
            }
        }

        public static void CreateRemoteRepository(string entityName)
        {
            string fileName = entityName.Substring(3);
            string folderName = @RepositoryFolder + fileName;

            //System.IO.Directory.CreateDirectory(folderName);
            // To create a string that specifies the path to a subfolder under your 
            // top-level folder, add a name for the subfolder to folderName.
            string pathString = System.IO.Path.Combine(folderName, "Remote_" + fileName + "_Repository.cs");
            

            // Check that the file doesn't already exist. If it doesn't exist, create
            // the file and write integers 0 - 99 to it.
            // DANGER: System.IO.File.Create will overwrite the file if it already exists.
            // This could happen even with random file names, although it is unlikely.
            if (!System.IO.File.Exists(pathString))
            {
                using (System.IO.FileStream fs = System.IO.File.Create(pathString)) { };
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@pathString))
                {
                    file.WriteLine("using Library.Entity.Edmx; ");
                    file.WriteLine("using Library.Servant.Repository." + fileName + ".Models;");
                    file.WriteLine("");
                    file.WriteLine("namespace Library.Servant.Repository." + fileName);
                    file.WriteLine("{ ");
                    file.WriteLine("\t public class Remote_" + fileName + "_Repository : RemoteGenericRepository<" + entityName + ", IndexItemViewModel, DetailsViewModel, CreateViewModel, UpdateViewModel>");
                    file.WriteLine("\t {");
                    file.WriteLine("\t\t public Remote_" + fileName + "_Repository()");
                    file.WriteLine("\t\t\t :base(\"" + fileName + "_Repository\")");
                    file.WriteLine("\t\t {}");
                    file.WriteLine("\t }");
                    file.WriteLine("}");
                }
                // Verify the path that you have constructed.
                Console.WriteLine("{0} Created.", "Remote_" + fileName + "_Repository.cs");
            }
            else
            {
                Console.WriteLine("File \"{0}\" already exists.", entityName);
                return;
            }
        }

        public static void CreateModels(string entityName, string content)
        {
            string fileName = entityName.Substring(3);
            string folderName = @RepositoryFolder + fileName;

            // To create a string that specifies the path to a subfolder under your 
            // top-level folder, add a name for the subfolder to folderName.
            string pathString = System.IO.Path.Combine(folderName, "Models");
            
            System.IO.Directory.CreateDirectory(pathString);

            // Create a file name for the file you want to create. 
            foreach(string modelName in ModelNames)
            {
                // Use Combine again to add the file name to the path.
                string modelPath = System.IO.Path.Combine(pathString, modelName + ".cs");

                // Check that the file doesn't already exist. If it doesn't exist, create
                // the file and write integers 0 - 99 to it.
                // DANGER: System.IO.File.Create will overwrite the file if it already exists.
                // This could happen even with random file names, although it is unlikely.
                if (!System.IO.File.Exists(modelPath))
                {
                    using (System.IO.FileStream fs = System.IO.File.Create(modelPath)) { };
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(@modelPath))
                    {
                        file.WriteLine("using Library.Servant.Repository.Models;");
                        file.WriteLine("using System; ");
                        file.WriteLine("");
                        file.WriteLine("namespace Library.Servant.Repository." + fileName + ".Models");
                        file.WriteLine("{");
                        file.WriteLine("\t public class " + modelName + " : AbstractCommunicateModel");
                        file.WriteLine("\t {");
                        file.WriteLine(content);
                        file.WriteLine("\t }");
                        file.WriteLine("}");
                    }
                    // Verify the path that you have constructed.
                    Console.WriteLine("{0} Created.", modelName + ".cs");
                }
                else
                {
                    Console.WriteLine("File \"{0}\" already exists.", fileName);
                    return;
                }
            }
        }

        public static void ReadEdmx()
        {
            //string filePath = EdmxPath + "AS_TRX_Contents.cs";

            // 取得資料夾內所有檔案
            foreach (string fname in System.IO.Directory.GetFiles(EdmxFolder))
            {
                // Example #2
                // Read each line of the file into a string array. Each element
                // of the array is one line of the file.
                string[] lines = System.IO.File.ReadAllLines(@fname);

                // Display the file contents by using a foreach loop.
                string fileName = fname.Split('\\')[1].Replace(".cs","");
                System.Console.WriteLine(fileName + " Model : ");
                foreach (string line in lines)
                {
                    if (line.Contains("{ get; set; }") && !line.Contains("public virtual"))
                    {
                        // Use a tab to indent each line of the file.
                        Console.WriteLine("\t" + line);
                    }
                    
                }
            }

            // Keep the console window open in debug mode.
            Console.WriteLine("Press any key to exit.");
            System.Console.ReadKey();
        }
        
        public static void FileCreateExample()
        {

            // Specify a name for your top-level folder.
            string folderName = @".\Top-Level Folder"; // .\ -> ...RepositoryGenerator\bin\Debug\

            // To create a string that specifies the path to a subfolder under your 
            // top-level folder, add a name for the subfolder to folderName.
            string pathString = System.IO.Path.Combine(folderName, "SubFolder");

            // You can write out the path name directly instead of using the Combine
            // method. Combine just makes the process easier.
            //string pathString2 = @".\Top-Level Folder\SubFolder2";

            // You can extend the depth of your path if you want to.
            //pathString = System.IO.Path.Combine(pathString, "SubSubFolder");

            // Create the subfolder. You can verify in File Explorer that you have this
            // structure in the C: drive.
            //    Local Disk (C:)
            //        Top-Level Folder
            //            SubFolder
            System.IO.Directory.CreateDirectory(pathString);

            // Create a file name for the file you want to create. 
            string fileName = System.IO.Path.GetRandomFileName();

            // This example uses a random string for the name, but you also can specify
            // a particular name.
            //string fileName = "MyNewFile.txt";

            // Use Combine again to add the file name to the path.
            pathString = System.IO.Path.Combine(pathString, fileName);

            // Verify the path that you have constructed.
            Console.WriteLine("Path to my file: {0}\n", pathString);

            // Check that the file doesn't already exist. If it doesn't exist, create
            // the file and write integers 0 - 99 to it.
            // DANGER: System.IO.File.Create will overwrite the file if it already exists.
            // This could happen even with random file names, although it is unlikely.
            if (!System.IO.File.Exists(pathString))
            {
                using (System.IO.FileStream fs = System.IO.File.Create(pathString))
                {
                    for (byte i = 0; i < 100; i++)
                    {
                        fs.WriteByte(i);
                    }
                }
            }
            else
            {
                Console.WriteLine("File \"{0}\" already exists.", fileName);
                return;
            }

            // Read and display the data from your file.
            try
            {
                byte[] readBuffer = System.IO.File.ReadAllBytes(pathString);
                foreach (byte b in readBuffer)
                {
                    Console.Write(b + " ");
                }
                Console.WriteLine();
            }
            catch (System.IO.IOException e)
            {
                Console.WriteLine(e.Message);
            }

            // Keep the console window open in debug mode.
            System.Console.WriteLine("Press any key to exit.");
            System.Console.ReadKey();
        }

        public static void FileWriteExample()
        {
            // These examples assume a "C:\Users\Public\TestFolder" folder on your machine.
            // You can modify the path if necessary.


            // Example #1: Write an array of strings to a file.
            // Create a string array that consists of three lines.
            string[] lines = { "First line", "Second line", "Third line" };
            // WriteAllLines creates a file, writes a collection of strings to the file,
            // and then closes the file.  You do NOT need to call Flush() or Close().
            System.IO.File.WriteAllLines(@"C:\Users\Public\TestFolder\WriteLines.txt", lines);


            // Example #2: Write one string to a text file.
            string text = "A class is the most powerful data type in C#. Like a structure, " +
                           "a class defines the data and behavior of the data type. ";
            // WriteAllText creates a file, writes the specified string to the file,
            // and then closes the file.    You do NOT need to call Flush() or Close().
            System.IO.File.WriteAllText(@"C:\Users\Public\TestFolder\WriteText.txt", text);

            // Example #3: Write only some strings in an array to a file.
            // The using statement automatically flushes AND CLOSES the stream and calls 
            // IDisposable.Dispose on the stream object.
            // NOTE: do not use FileStream for text files because it writes bytes, but StreamWriter
            // encodes the output as text.
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"C:\Users\Public\TestFolder\WriteLines2.txt"))
            {
                foreach (string line in lines)
                {
                    // If the line doesn't contain the word 'Second', write the line to the file.
                    if (!line.Contains("Second"))
                    {
                        file.WriteLine(line);
                    }
                }
            }

            // Example #4: Append new text to an existing file.
            // The using statement automatically flushes AND CLOSES the stream and calls 
            // IDisposable.Dispose on the stream object.
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"C:\Users\Public\TestFolder\WriteLines2.txt", true))
            {
                file.WriteLine("Fourth line");
            }
        }

        public static void FileReadExample()
        {
            // The files used in this example are created in the topic
            // How to: Write to a Text File. You can change the path and
            // file name to substitute text files of your own.

            // Example #1
            // Read the file as one string.
            string text = System.IO.File.ReadAllText(@"C:\Users\Public\TestFolder\WriteText.txt");

            // Display the file contents to the console. Variable text is a string.
            System.Console.WriteLine("Contents of WriteText.txt = {0}", text);

            // Example #2
            // Read each line of the file into a string array. Each element
            // of the array is one line of the file.
            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\Public\TestFolder\WriteLines2.txt");

            // Display the file contents by using a foreach loop.
            System.Console.WriteLine("Contents of WriteLines2.txt = ");
            foreach (string line in lines)
            {
                // Use a tab to indent each line of the file.
                Console.WriteLine("\t" + line);
            }

            // Keep the console window open in debug mode.
            Console.WriteLine("Press any key to exit.");
            System.Console.ReadKey();
        }
    }
}
