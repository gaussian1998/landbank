﻿using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;

namespace Library.Entity.Extension
{
    public static class DbUpdateExceptionExtension
    {
        public static string Parser(this DbUpdateException ex)
        {
            string result = string.Empty;
            foreach (var eve in ex.Entries)
            {
                result += "---" + eve.Entity.GetType().Name;
                result += " x " + eve.State;
                result += " x " + ex.InnerException.InnerException.Message;
            }

            return result;
        }
    }

    public static class DbEntityValidationExceptionExtension
    {
        public static string Parser(this DbEntityValidationException ex)
        {
            string result = string.Empty;
            foreach (var eve in ex.EntityValidationErrors)
            {
                result += "---" + eve.Entry.Entity.GetType().Name;
                foreach (var ve in eve.ValidationErrors)
                {
                    result += " x " + ve.PropertyName;
                    result += " x " + ve.ErrorMessage;
                }
            }

            return result;
        }
    }
}
