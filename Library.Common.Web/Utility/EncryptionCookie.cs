﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;

namespace Library.Common.Web.Utility
{
    public class EncryptionCookie
    {
        public EncryptionCookie(HttpContext context)
        {
            this.request = context.Request;
            this.response = context.Response;
        }

        public string Get(string id)
        {
            if (request.Cookies.AllKeys.Contains(id))
                return Decode(request.Cookies.Get(id).Value);
            else
                return "";
        }

        public static string Get(HttpCookieCollection cookie, string id)
        {
            if (cookie.AllKeys.Contains(id))
                return Decode(cookie.Get(id).Value);
            else
                return "";
        }

        public T Get<T>(string id)
        {
            string value = Get(id);
            return JsonConvert.DeserializeObject<T>(value);
        }

        public static T Get<T>(HttpCookieCollection cookie, string id)
        {
            string value = Get(cookie,id);
            return JsonConvert.DeserializeObject<T>(value);
        }

        public void Update(string id, string value)
        {
            if (request.Cookies.AllKeys.Contains(id))
                Remove(id);

            Create(id, value);
        }

        public void Update<T>(string id, T obj)
        {
            if (request.Cookies.AllKeys.Contains(id))
                Remove(id);

            Create( id, JsonConvert.SerializeObject(obj));
        }

        public void Delete(string id)
        {
            Remove(id);

            HttpCookie myCookie = new HttpCookie(id) { HttpOnly = true };
            myCookie.Expires = DateTime.Now.AddDays(-1d);
            response.Cookies.Add(myCookie);
        }

        private void Remove(string id)
        {
            response.Cookies.Remove(id);
            request.Cookies.Remove(id);
        }

        private void Create(string id, string value)
        {
            HttpCookie cookie = new HttpCookie(id, Encode(value)) { HttpOnly = true };
            //cookie.Expires = DateTime.Now.AddDays(1d);
            response.Cookies.Add(cookie);
            request.Cookies.Add(cookie);
        }

        private static string Encode(string source)
        {
            byte[] en = MachineKey.Protect( Encoding.UTF8.GetBytes(source), "an cookie token");
            return Convert.ToBase64String(en);
        }

        private static string Decode(string source)
        {
            byte[] en = Convert.FromBase64String(source);
            return Encoding.UTF8.GetString(MachineKey.Unprotect(en, "an cookie token"));
        }

        private readonly HttpRequest request;
        private readonly HttpResponse response;
    }
}