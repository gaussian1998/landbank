﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Xml.Serialization;

namespace Web.Email
{
    /// <summary>
    /// Summary description for MsgHandler
    /// </summary>
    [WebService(Namespace = "http://www.cedar.com.tw/bluestar/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MsgHandler : System.Web.Services.WebService, IMsgHandlerSoap
    {
        public int FlatFile2XmlElement(string MsgName, string Tota, out XmlElement RequestXmlElement)
        {
            throw new NotImplementedException();
        }

        public int FlatFile2XmlString(string MsgName, string Tota, out string XmlString)
        {
            throw new NotImplementedException();
        }

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        public int SubmitFlatFile(string msgName, string request, out string response)
        {
            throw new NotImplementedException();
        }

        public int SubmitXml(XmlElement Any, out XmlElement response)
        {
            throw new NotImplementedException();
        }

        public int SubmitXmlString(string sRequest, out string sResponse)
        {
            StringBuilder response = new StringBuilder();
            response.Append("<BlueStar RqUid=\"b7c92e5a-a693-4045-8e78-9026401f959a\" xmlns=\"http://www.cedar.com.tw/bluestar/\" Status=\"0\">");
                response.Append("<StatusCode>0</StatusCode>");
                response.Append("<StatusDesc>Success</StatusDesc>");
                response.Append("<BSNS_EventID>21</BSNS_EventID>");
            response.Append("</BlueStar>");

            sResponse = response.ToString();
            return 1;
        }

        public void SubmitXmlSync(ref XmlElement Any)
        {
            throw new NotImplementedException();
        }

        public int XmlString2FlatFile(string xmlRequest, out string tita)
        {
            throw new NotImplementedException();
        }
    }
}
