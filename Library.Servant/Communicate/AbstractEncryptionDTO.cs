﻿using Library.Interface;
using Library.Servant.SymmetryEncryption;
using Newtonsoft.Json;

namespace Library.Servant.Communicate
{
    public abstract class AbstractEncryptionDTO
    {
        public virtual IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }

        public string SerialNo { get; set; }

        public static implicit operator bool(AbstractEncryptionDTO thiz)
        {
            return thiz.SerialNo == ValidateSerialNo;
        }

        public string Serial()
        {
            SerialNo = ValidateSerialNo;

            string json = JsonConvert.SerializeObject(this);
            string encodeJson = GetEncryption().Encrypto(json);
            return encodeJson;
        }

        public static SubType DeSerial<SubType>(string encodeJson)
            where SubType : AbstractEncryptionDTO, new()
        {
            string json = (new SubType()).GetEncryption().Decrypto(encodeJson);
            return JsonConvert.DeserializeObject<SubType>(json);
        }

        private const string ValidateSerialNo = "hjKSD8626RFGH#@#!@Yfjhsdu9o51fxcs91wx";
    }

    /// <summary>
    ///  侵入式 加密 DTO
    /// </summary>
    public class InvasionEncryption : AbstractEncryptionDTO
    {
        public override IEncryption GetEncryption()
        {
            //return EncryptionTools.gzip; // 只壓縮
            //return EncryptionTools.authServer; // 壓縮,加密,加鹽
            return EncryptionTools.noEncryption; // 不做任何處理
        }
    }
}
