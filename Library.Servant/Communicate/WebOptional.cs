﻿using Library.Utility;
using Newtonsoft.Json;
using System.Text;

namespace Library.Servant.Communicate
{
    public class WebOptional
    {
        public bool Valid { get; set; }
        public string Encryption { get; set; }
        public string ErrorMessage { get; set; }
        public int ErrorCode { get; set; }

        public static string Error(string errorMessage, int ErrorCode = int.MinValue)
        {
            WebOptional error = new WebOptional { ErrorMessage = errorMessage, Valid = false, ErrorCode = ErrorCode };
            return JsonConvert.SerializeObject(error);
        }


        public static Optional<T> Deserialize<T>(string json)
            where T : AbstractEncryptionDTO, new()
        {
            WebOptional result = JsonConvert.DeserializeObject<WebOptional>(json);
            return result.Get<T>();
        }

        public static Optional<T> Deserialize<T>(byte[] utf8)
            where T : AbstractEncryptionDTO, new()
        {
            return Deserialize<T>(Encoding.UTF8.GetString(utf8));
        }
    }

    public static class WebOptionalExtension
    {
        public static string Stringify(this AbstractEncryptionDTO encryptionDTO)
        {
             WebOptional result = new WebOptional { Encryption = encryptionDTO.Serial(), Valid = true };
            return JsonConvert.SerializeObject(result);
        }

        public static byte[] UTF8(this AbstractEncryptionDTO encryptionDTO)
        {
            return Encoding.UTF8.GetBytes(Stringify(encryptionDTO));
        }

        public static Optional<T> Get<T>(this WebOptional optional)
            where T : AbstractEncryptionDTO, new()
        {
            if (optional.Valid == false)
                return Optional<T>.Error(optional.ErrorMessage, optional.ErrorCode);
                        
            T value = AbstractEncryptionDTO.DeSerial<T>(optional.Encryption);
            if (value == null)
                return Optional<T>.Error( "Opps!! EncryptionDTO Format Error...");
            else
                return Optional<T>.Ok(value);
        }
    }
}
