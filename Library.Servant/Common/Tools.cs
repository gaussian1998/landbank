﻿using System;
using Library.Utility;

namespace Library.Servant.Common
{
    public class Tools
    {
        public static T ExceptionConvert<T>(Optional<T> optional)
        {
            if (optional)
                return optional.Value;
            else
                throw new Exception(optional.ErrorMessage);
        }
    }
}
