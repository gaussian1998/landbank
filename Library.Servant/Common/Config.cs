﻿
using Library.Servant.Servants.AbstractFactory;

namespace Library.Servant.Common
{
    public static class Config
    {
        public static string ServantDomain
        {
            get
            {
                return ServantAbstractFactory.ServantDomain();
            }
        }
    }
}
