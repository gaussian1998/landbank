﻿using System;

namespace Library.Servant.Attributes
{
    internal class FormTypeInfoAttribute : Attribute
    {
        public string PrefixCode { get; private set; }
        public string FormName { get; private set; }

        public FormTypeInfoAttribute(string code, string name)
        {
            PrefixCode = code;
            FormName = name;
        }
    }
}