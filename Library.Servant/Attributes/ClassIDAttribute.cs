﻿using System;

namespace Library.Servant.Attributes
{
    internal class ClassIDAttribute : Attribute
    {
        public string ClassID { get; set; }

        public ClassIDAttribute(string id)
        {
            ClassID = id;
        }
    }
}