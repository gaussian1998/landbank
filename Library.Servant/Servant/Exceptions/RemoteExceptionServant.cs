﻿using Library.Servant.Common;
using Library.Servant.Servant.Common;


namespace Library.Servant.Servant.Exceptions
{
    public class RemoteExceptionServant : IExceptionServant
    {
        public VoidResult Exception()
        {
            var result = ProtocolService.EncryptionPost<VoidModel, VoidResult>
            (
                new VoidModel { },
                Config.ServantDomain + "/ExceptionServant/Index"
            );

            return Tools.ExceptionConvert(result);
        }

        public VoidResult InvalidOperation()
        {
            var result = ProtocolService.EncryptionPost<VoidModel, VoidResult>
            (
                new VoidModel { },
                Config.ServantDomain + "/ExceptionServant/InvalidOperation"
            );

            return Tools.ExceptionConvert(result);
        }

        public VoidResult NotSupported()
        {
            var result = ProtocolService.EncryptionPost<VoidModel, VoidResult>
            (
                new VoidModel { },
                Config.ServantDomain + "/ExceptionServant/NotSupported"
            );

            return Tools.ExceptionConvert(result);
        }

        public VoidResult NOT_NULL()
        {
            var result = ProtocolService.EncryptionPost<VoidModel, VoidResult>
            (
                new VoidModel { },
                Config.ServantDomain + "/ExceptionServant/NOT_NULL"
            );

            return Tools.ExceptionConvert(result);
        }

        public VoidResult UNIQUE()
        {
            var result = ProtocolService.EncryptionPost<VoidModel, VoidResult>
            (
                new VoidModel { },
                Config.ServantDomain + "/ExceptionServant/UNIQUE"
            );

            return Tools.ExceptionConvert(result);
        }
    }
}
