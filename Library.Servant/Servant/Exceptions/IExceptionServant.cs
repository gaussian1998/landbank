﻿using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.Exceptions
{
    public interface IExceptionServant
    {
        VoidResult Exception();
        VoidResult NotSupported();
        VoidResult InvalidOperation();
        VoidResult NOT_NULL();
        VoidResult UNIQUE();
    }
}
