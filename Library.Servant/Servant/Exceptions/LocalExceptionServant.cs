﻿using LandBankEntity.ActiveRecords;
using Library.Entity.Edmx;
using Library.Servant.Servant.Common;
using System;

namespace Library.Servant.Servant.Exceptions
{
    public class LocalExceptionServant : IExceptionServant
    {
        public VoidResult Exception()
        {
            throw new Exception("Normal Exception");
        }

        public VoidResult NotSupported()
        {
            throw new NotSupportedException("NotSupportedException");
        }

        public VoidResult InvalidOperation()
        {
            throw new InvalidOperationException("InvalidOperationException");
        }

        public VoidResult NOT_NULL()
        {
            AS_Exception_Records.Delete( m => true );
            AS_Exception_Records.Create( new AS_Exception { NULL = 1 } );

            return new VoidResult();
        }

        public VoidResult UNIQUE()
        {
            AS_Exception_Records.Delete(m => true);
            AS_Exception_Records.Create(new AS_Exception { NULL = 1, NOT_NULL = DateTime.Now, UNIQUE = 1 });
            AS_Exception_Records.Create(new AS_Exception { NULL = 2, NOT_NULL = DateTime.Now, UNIQUE = 1 });

            return new VoidResult();
        }
    }
}


