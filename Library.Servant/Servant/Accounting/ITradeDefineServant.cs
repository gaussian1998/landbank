﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.Accounting
{
    public interface ITradeDefineServant
    {
        AS_GL_Trade_Define GetTradeDefineByMasterNo(decimal argMasterNo);
        IQueryable<AS_GL_Trade_Define> GetAll();
    }
}
