﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Accounting
{

    public interface IAccountingDDLServant
    {
        /// <summary>
        /// 交易類別
        /// </summary>
        /// <returns></returns>
        IEnumerable<Option> GetTransTypes();
        /// <summary>
        /// 交易區分
        /// </summary>
        /// <returns></returns>
        IEnumerable<Option> GetTradeTypes();
        /// <summary>
        /// 帳簿類別
        /// </summary>
        /// <returns></returns>
        IEnumerable<Option> GetBookTypes();

        /// <summary>
        /// 幣別
        /// </summary>
        /// <returns></returns>
        IEnumerable<Option> GetCurrencies();

        /// <summary>
        /// 分行
        /// </summary>
        /// <returns></returns>
        IEnumerable<Option> GetBranch();

        /// <summary>
        /// 取得部門
        /// </summary>
        /// <returns></returns>
        IEnumerable<Option> GetDepartment();

    }
}
