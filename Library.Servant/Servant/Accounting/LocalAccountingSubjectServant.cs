﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Entity.Edmx;
using Library.Servant.Servant.Accounting.Models;

namespace Library.Servant.Servant.Accounting
{
    public class LocalAccountingSubjectServant : IAccountingSubjectServant
    {
        private readonly IAS_GL_AccountRepository _repoAccounting = RepositoryHelper.GetAS_GL_AccountRepository();

        public AccountingSubjectModel GetAccountInfo(string account)
        {
            AccountingSubjectModel result = null;
            var item = _repoAccounting.All().FirstOrDefault(p => p.Account == account);
            if (item != null)
            {
                result = new AccountingSubjectModel
                {
                    ID = item.ID,
                    BookType = item.Book_Type,
                    AccountType = item.Account_Type,
                    Account = item.Account,
                    AccountName = item.Account_Name,
                    AssetLiablty = item.Asset_Liablty,
                    DBCR = item.DB_CR,
                    IFRSAccount = item.IFRS_Account,
                    LedgerType = item.Ledger_Type

                };
            }

            return result;
        }
    }
}
