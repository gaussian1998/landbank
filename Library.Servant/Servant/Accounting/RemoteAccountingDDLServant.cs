﻿using Library.Servant.Common;
using Library.Servant.Servant.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Accounting
{
    public class RemoteAccountingDDLServant : IAccountingDDLServant
    {
        public IEnumerable<Option> GetBookTypes()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Option> GetBranch()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Option> GetCurrencies()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Option> GetDepartment()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Option> GetTradeTypes()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Option> GetTransTypes()
        {
            throw new NotImplementedException();
        }
    }
}
