﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.Accounting
{
    public class LocalTradeDefineServant : ITradeDefineServant
    {
        private readonly IAS_GL_Trade_DefineRepository _repoTradeDefine = RepositoryHelper.GetAS_GL_Trade_DefineRepository();
        public AS_GL_Trade_Define GetTradeDefineByMasterNo(decimal masterNo)
        {
            return _repoTradeDefine.All().FirstOrDefault(p => p.Master_No == masterNo);
        }

        public IQueryable<AS_GL_Trade_Define> GetAll()
        {
            return _repoTradeDefine.All();
        }
    }
}
