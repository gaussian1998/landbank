﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Entity.Edmx;
using Library.Servant.Servant.Accounting.Models;

namespace Library.Servant.Servant.Accounting
{
    public interface IAccountingSubjectServant
    {
        //AS_GL_Account GetAccountInfo(string account);
        AccountingSubjectModel GetAccountInfo(string account);
    }
}
