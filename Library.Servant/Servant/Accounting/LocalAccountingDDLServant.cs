﻿using Library.Entity.Edmx;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Library.Servant.Servant.Accounting
{


    public class LocalAccountingDDLServant : IAccountingDDLServant
    {
        private readonly IAS_Code_TableRepository _repoCodeTable = RepositoryHelper.GetAS_Code_TableRepository();
        private readonly IAS_CurrencyRepository _repoCurrency = RepositoryHelper.GetAS_CurrencyRepository();

        public List<Option> Get(string type)
        {
            switch (type)
            {
                case "departments":
                    return GetDepartment().ToList();
                case "branchs":
                    return GetBranch().ToList();
                case "currencies":
                    return GetCurrencies().ToList();
                case "transTypes":
                    return GetTransTypes().ToList();
                case "tradeTypes":
                    return GetTradeTypes().ToList();
                case "bookTypes":
                    return GetBookTypes().ToList();
                default:
                    throw new Exception("錯誤的類別");
            }
        }

        public IEnumerable<Option> GetCurrencies()
        {
            return _repoCurrency.All()
                .Select(p => new Option {Key = p.Currency, Value = p.ID.ToString()});
        }

        public IEnumerable<Option> GetBranch()
        {
            return _repoCodeTable.All().Where(p => p.Class == "C01")
                .Select(p => new Option {Key = p.Text, Value = p.Code_ID});
        }

        public IEnumerable<Option> GetDepartment()
        {
            return _repoCodeTable.All().Where(p => p.Class == "C02")
                .Select(p => new Option { Key = p.Text, Value = p.Code_ID });
        }

        public IEnumerable<Option> GetBookTypes()
        {
            return _repoCodeTable.All().Where(p => p.Class == "V01")
                .Select(p => new Option { Key = p.Text, Value = p.Code_ID });
        }

        public IEnumerable<Option> GetTradeTypes()
        {
            var notInCompare = new List<string>() { "03", "04" };
            return _repoCodeTable.All()
                .Where(p => p.Class == "V04" && !notInCompare.Contains(p.Code_ID) && p.Is_Active && !p.Cancel_Code)
                .Select(p => new Option { Key = p.Text, Value = p.Code_ID });
        }

        public IEnumerable<Option> GetTransTypes()
        {

            return _repoCodeTable.All()
                .Where(p => p.Class == "V03" && p.Is_Active && !p.Cancel_Code)
                .Select(p => new Option { Key = p.Text, Value = p.Code_ID });
        }
    }
}
