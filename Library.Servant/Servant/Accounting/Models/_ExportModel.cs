﻿using System;

namespace Library.Servant.Servant.Accounting.Models
{
    public class _ExportModel
    {
        /// <summary>
        /// 是否匯出
        /// </summary>
        public bool DetailChecked { get; set; }
        /// <summary>
        /// 分錄編號
        /// </summary>
        public int? Master_No { get; set; }
        /// <summary>
        /// 會計科目
        /// </summary>
        public string GL_Account { get; set; }

        /// <summary>
        /// 交易類別  ddl source: code table Class = V03
        /// </summary>
        public string TransType { get; set; }
        /// <summary>
        /// 行號
        /// </summary>
        public string BranchCode { get; set; }
        /// <summary>
        /// 部門編號
        /// </summary>
        public string DepartmentCode { get; set; }
        /// <summary>
        /// 資產編號
        /// </summary>
        public string AssetNo { get; set; }
        /// <summary>
        /// 租約編號
        /// </summary>
        public string LeaseNo { get; set; }
        /// <summary>
        /// 匯入或批次序號 Batch_Seq or Import_Num
        /// </summary>
        public string Seq { get; set; }
        /// <summary>
        /// 交易序號
        /// </summary>
        public string TransSeq { get; set; }

    }
}