﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Library.Servant.Servant.Accounting.Models
{
    [XmlRoot(ElementName = "CustId", Namespace = "urn:schema-bluestar-com:multichannel")]
    public class CustId
    {
        [XmlElement(ElementName = "SPName", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string SPName { get; set; }
        [XmlElement(ElementName = "CustLoginId", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string CustLoginId { get; set; }
    }

    [XmlRoot(ElementName = "CustPswd", Namespace = "urn:schema-bluestar-com:multichannel")]
    public class CustPswd
    {
        [XmlElement(ElementName = "CryptType", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string CryptType { get; set; }
        [XmlElement(ElementName = "Pswd", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string Pswd { get; set; }
    }

    [XmlRoot(ElementName = "SignonPswd", Namespace = "urn:schema-bluestar-com:multichannel")]
    public class SignonPswd
    {
        [XmlElement(ElementName = "CustId", Namespace = "urn:schema-bluestar-com:multichannel")]
        public CustId CustId { get; set; }
        [XmlElement(ElementName = "CustPswd", Namespace = "urn:schema-bluestar-com:multichannel")]
        public CustPswd CustPswd { get; set; }
    }

    [XmlRoot(ElementName = "SignonRq", Namespace = "urn:schema-bluestar-com:multichannel")]
    public class SignonRq
    {
        [XmlElement(ElementName = "SignonPswd", Namespace = "urn:schema-bluestar-com:multichannel")]
        public SignonPswd SignonPswd { get; set; }
        [XmlElement(ElementName = "ClientDt", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string ClientDt { get; set; }
    }

    [XmlRoot(ElementName = "Detail", Namespace = "urn:schema-bluestar-com:multichannel")]
    public class Detail
    {
        [XmlElement(ElementName = "DEPNO", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string DEPNO { get; set; }
        [XmlElement(ElementName = "ACCNO", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string ACCNO { get; set; }
        [XmlElement(ElementName = "CRDB", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string CRDB { get; set; }
        [XmlElement(ElementName = "AMT", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string AMT { get; set; }
        [XmlElement(ElementName = "MEMO", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string MEMO { get; set; }
        [XmlElement(ElementName = "NAMENO", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string NAMENO { get; set; }
        [XmlElement(ElementName = "CLNO", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string CLNO { get; set; }
    }

    [XmlRoot(ElementName = "CommMsg", Namespace = "urn:schema-bluestar-com:multichannel")]
    public class CommMsg
    {
        [XmlElement(ElementName = "SPName", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string SPName { get; set; }
        [XmlElement(ElementName = "MsgId", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string MsgId { get; set; }
        [XmlElement(ElementName = "RqUID", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string RqUID { get; set; }
        [XmlElement(ElementName = "AllRecCtrlIn", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string AllRecCtrlIn { get; set; }
        [XmlElement(ElementName = "KINBR", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string KINBR { get; set; }
        [XmlElement(ElementName = "TRMSEQ", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TRMSEQ { get; set; }
        [XmlElement(ElementName = "TXTNO", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TXTNO { get; set; }
        [XmlElement(ElementName = "TTSKID", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TTSKID { get; set; }
        [XmlElement(ElementName = "TRMTYP", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TRMTYP { get; set; }
        [XmlElement(ElementName = "XFG", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string XFG { get; set; }
        [XmlElement(ElementName = "TLRNO", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TLRNO { get; set; }
        [XmlElement(ElementName = "APTYPE", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string APTYPE { get; set; }
        [XmlElement(ElementName = "TXNO", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TXNO { get; set; }
        [XmlElement(ElementName = "STXNO", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string STXNO { get; set; }
        [XmlElement(ElementName = "PTYPE", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string PTYPE { get; set; }
        [XmlElement(ElementName = "DSCPT", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string DSCPT { get; set; }
        [XmlElement(ElementName = "MRKEY", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string MRKEY { get; set; }
        [XmlElement(ElementName = "TITA_UNINO", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TITA_UNINO { get; set; }
        [XmlElement(ElementName = "TITA_UIDERR", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TITA_UIDERR { get; set; }
        [XmlElement(ElementName = "ECKIN", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string ECKIN { get; set; }
        [XmlElement(ElementName = "ECTRMS", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string ECTRMS { get; set; }
        [XmlElement(ElementName = "ECTNO", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string ECTNO { get; set; }
        [XmlElement(ElementName = "TXTYPE", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TXTYPE { get; set; }
        [XmlElement(ElementName = "CRDB", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string CRDB { get; set; }
        [XmlElement(ElementName = "SPCD", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string SPCD { get; set; }
        [XmlElement(ElementName = "SPID", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string SPID { get; set; }
        [XmlElement(ElementName = "NBCD", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string NBCD { get; set; }
        [XmlElement(ElementName = "HCODE", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string HCODE { get; set; }
        [XmlElement(ElementName = "YCODE", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string YCODE { get; set; }
        [XmlElement(ElementName = "CURCD", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string CURCD { get; set; }
        [XmlElement(ElementName = "TXAMT", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TXAMT { get; set; }
        [XmlElement(ElementName = "TITA_ENTDD", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TITA_ENTDD { get; set; }
        [XmlElement(ElementName = "TITA_TXDAY", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TITA_TXDAY { get; set; }
        [XmlElement(ElementName = "TITA_TXTIME", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TITA_TXTIME { get; set; }
        [XmlElement(ElementName = "MTTPSEQ", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string MTTPSEQ { get; set; }
        [XmlElement(ElementName = "COKEY", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string COKEY { get; set; }
        [XmlElement(ElementName = "TITA_SECNO", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TITA_SECNO { get; set; }
        [XmlElement(ElementName = "TITA_SEQFG", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TITA_SEQFG { get; set; }
        [XmlElement(ElementName = "IBFFG", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string IBFFG { get; set; }
        [XmlElement(ElementName = "SBCASH", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string SBCASH { get; set; }
        [XmlElement(ElementName = "BATCHNO", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string BATCHNO { get; set; }
        [XmlElement(ElementName = "TITA_OBUFG", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TITA_OBUFG { get; set; }
        [XmlElement(ElementName = "INBKTXNO", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string INBKTXNO { get; set; }
        [XmlElement(ElementName = "TITA_SYNC", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TITA_SYNC { get; set; }
        [XmlElement(ElementName = "TITA_SECTYPE", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string TITA_SECTYPE { get; set; }
        [XmlElement(ElementName = "SRFLG", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string SRFLG { get; set; }
        [XmlElement(ElementName = "CNT", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string CNT { get; set; }
        [XmlElement(ElementName = "RECNO", Namespace = "urn:schema-bluestar-com:multichannel")]
        public string RECNO { get; set; }
        [XmlElement(ElementName = "Detail", Namespace = "urn:schema-bluestar-com:multichannel")]
        public List<Detail> Detail { get; set; }
    }

    [XmlRoot(ElementName = "LandBankML", Namespace = "urn:schema-bluestar-com:multichannel")]
    public class LandBankML
    {
        [XmlElement(ElementName = "SignonRq", Namespace = "urn:schema-bluestar-com:multichannel")]
        public SignonRq SignonRq { get; set; }
        [XmlElement(ElementName = "CommMsg", Namespace = "urn:schema-bluestar-com:multichannel")]
        public CommMsg CommMsg { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

}
