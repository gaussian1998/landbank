﻿

namespace Library.Servant.Servant.Accounting
{
    public class Option
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
