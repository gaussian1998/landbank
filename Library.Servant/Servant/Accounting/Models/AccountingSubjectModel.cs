﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Accounting.Models
{
    public class AccountingSubjectModel
    {
        public int ID { get; set; }
        public string BookType { get; set; }
        public string AccountType { get; set; }
        public string Account { get; set; }
        public string AccountName { get; set; }
        public string AssetLiablty { get; set; }
        public string DBCR { get; set; }
        public string IFRSAccount { get; set; }
        public string LedgerType { get; set; }
    }
}
