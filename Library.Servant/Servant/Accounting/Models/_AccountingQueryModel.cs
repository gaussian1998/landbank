﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Library.Servant.Servant.Accounting.Models
{
    /// <summary>
    /// 查詢條件
    /// </summary>
    public class _AccountingQueryModel
    {

        /// <summary>
        /// 總筆數
        /// </summary>
        public int Total_Record { get; set; }
        /// <summary>
        /// 當前頁數
        /// </summary>
        public int Page { get; set; } = 1;

        /// <summary>
        /// 1：本會計月 2：其它會計月
        /// </summary>
        public int OptionType { get; set; } = 1;
        /// <summary>
        /// 入帳開始日期
        /// </summary>
        public DateTime BeginOpenDate { get; set; } = DateTime.Today;
        /// <summary>
        /// 入帳結束日期
        /// </summary>
        public DateTime EndOpenDate { get; set; } = DateTime.Today;
        /// <summary>
        /// 交易類別  ddl source: code table Class = V03
        /// </summary>
        public string TransType { get; set; }
        /// <summary>
        /// 交易區分 ddl source: code table Class = V04 CodID Not In 03 04
        /// </summary>
        public string TradeType { get; set; }
        /// <summary>
        /// 分錄編號
        /// </summary>
        public int? MasterNo { get; set; }

        /// <summary>
        /// 匯入或批次序號 Batch_Seq or Import_Num
        /// </summary>
        public string Seq { get; set; }

        /// <summary>
        /// 交易序號
        /// </summary>
        public string TransSeq { get; set; }

        /// <summary>
        /// 資產編號
        /// </summary>
        public string AssetNo { get; set; }

        /// <summary>
        /// 查詢結果
        /// </summary>
        public List<_AccountQueryDetailModel> Details { get; set; }

        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// 出帳序號
        /// </summary>
        public int? PostSeq { get; set; }

        /// <summary>
        /// 租約編號
        /// </summary>
        public string LeaseNo { get; set; }

        /// <summary>
        /// 行號
        /// </summary>
        public string BranchCode { get; set; }
        /// <summary>
        /// 部門編號
        /// </summary>
        public string DepartmentCode { get; set; }

        /// <summary>
        /// 會計科目
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 借方金額
        /// </summary>
        public decimal DBAmount{ get; set; }
        /// <summary>
        /// 貸方金額
        /// </summary>
        public decimal CRAmount { get; set; }
    }

    public class _AccountQueryDetailModel
    {
        public int ID { get; set; }
        [Display(Name = "入帳日")]
        public System.DateTime Open_Date { get; set; }
        [Display(Name = "分錄編號")]
        public decimal Master_No { get; set; }
        [Display(Name = "分錄描述")]
        public string Description { get; set; }
        [Display(Name = "交易序號")]
        public string Trans_Seq { get; set; }
        [Display(Name = "Account Key")]
        public string Account_Key { get; set; }
        [Display(Name = "分行/科子細目/幣別")]
        public string GL_Account { get; set; }
        [Display(Name = "會計科目名稱")]
        public string Account_Name { get; set; }
        [Display(Name = "金額")]
        public decimal Amount { get; set; }
        [Display(Name = "備註1")]
        public string Notes { get; set; }
        [Display(Name = "備註2")]
        public string Notes1 { get; set; }
        [Display(Name = "出帳日")]
        public Nullable<System.DateTime> Post_Date { get; set; }
        [Display(Name = "借貸別")]
        public string DB_CR { get; set; }
        [Display(Name = "分行/部門")]
        public string BranchDepartment { get; set; }
        [Display(Name = "幣別")]
        public string Currency { get; set; }

    }
}
