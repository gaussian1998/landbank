﻿using Library.Servant.Communicate;
using Library.Interface;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;
using System;

namespace Library.Servant.Servant.Accounting
{
    public class OptionsResult : AbstractEncryptionDTO
    {
        public List<Option> Items { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
