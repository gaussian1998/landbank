﻿using Library.Entity.Edmx;
using Library.Servant.Servant.Accounting.Models;
using Library.Servant.Servant.AccountingQueryDeprn.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Library.Servant.Servant.Accounting
{
    public interface IAccountingQueryServant
    {

        int UserId { get; set; }

        IndexModel Index(_AccountingQueryModel search);



        /// <summary>
        /// 取得其它月的資料
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        AS_GL_Acc_Detail GetAccDetailById(int id);

        /// <summary>
        /// 刪除本月的資料
        /// </summary>
        /// <param name="accMonthId"></param>
        /// <param name="message"></param>
        void DeleteAccMonth(int accMonthId, string message);

        /// <summary>
        /// 新增帳務檔
        /// </summary>
        /// <param name="model"></param>
        void AddAccountingAccMonthItem(AS_GL_Acc_Months model);

        /// <summary>
        /// 取得幣別
        /// </summary>
        /// <param name="bookType"></param>
        /// <returns></returns>
        string GetCurrencyByBookType(string bookType);


        /// <summary>
        /// 刪除折舊檔
        /// </summary>
        /// <param name="itidem"></param>
        /// <param name="message"></param>
        void DeleteDeprn(int id, string message);

        IEnumerable<Acc_EntriesByHeader> GetAccountingEntries();
    }
}