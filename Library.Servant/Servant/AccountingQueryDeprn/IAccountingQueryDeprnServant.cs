﻿using System.Collections.Generic;
using Library.Servant.Servant.Accounting.Models;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.AccountingQueryDeprn
{
    public interface IAccountingQueryDeprnServant
    {
        string DeleteBatch(int[] id, string deleteReason, int userId);
        _AccountingQueryModel Detail(_AccountingQueryModel model);
        List<_AccountQueryDetailModel> ExportDetailExcel(IEnumerable<_ExportModel> models);
        _AccountingQueryModel ExportGroupExcel(_AccountingQueryModel model);
        string GetBranchName(string branchCode);
        string GetDepartmentName(string departmentCode);
        List<SelectedModel> GetTradeTypes();
        List<SelectedModel> GetTransTypes();
        _AccountingQueryModel Index(_AccountingQueryModel model);
    }
}