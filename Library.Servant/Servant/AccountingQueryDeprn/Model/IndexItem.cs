﻿using Library.Servant.Servant.Common.Models.Mapper;
using System;

namespace Library.Servant.Servant.AccountingQueryDeprn.Model
{
    public class IndexItem : GLAccMonthsModel
    {
        public string Description { get; set; }

        public int _ID
        {
            get
            {
                return ID;
            }
            set
            {
                ID = value;
            }
        }

        public DateTime OpenDate
        {
            get
            {
                return Open_Date;
            }
            set
            {
                Open_Date = value;
            }
        }

        public decimal MasterNo
        {
            get
            {
                return Master_No;
            }
            set
            {
                Master_No = value;
            }
        }

        public string TransSeq
        {
            get
            {
                return Trans_Seq;
            }
            set
            {
                Trans_Seq = value;
            }
        }

        public string AccountKey
        {
            get
            {
                return Account_Key;
            }
            set
            {
                Account_Key = value;
            }
        }

        public string GLAccount
        {
            get
            {
                return GL_Account;
            }
            set
            {
                GL_Account = value;
            }
        }

        public string AccountName
        {
            get
            {
                return Account_Name;
            }
            set
            {
                Account_Name = value;
            }
        }

        public string DBCR
        {
            get
            {
                return DB_CR;
            }
            set
            {
                DB_CR = value;
            }
        }

        public decimal _Amount
        {
            get
            {
                return Amount;
            }
            set
            {
                Amount = value;
            }
        }

        public string _Notes
        {
            get
            {
                return Notes;
            }
            set
            {
                Notes = value;
            }
        }

        public DateTime? PostDate
        {
            get
            {
                return Post_Date;
            }
            set
            {
                Post_Date = value;
            }
        }
    }
}
