﻿using Library.Servant.Communicate;
using System;

namespace Library.Servant.Servant.AccountingQueryDeprn.Model
{
    public class UpdateModel : InvasionEncryption
    {
        public int _ID { get; set; }
        public decimal _Amount { get; set; }
        public DateTime OpenDate { get; set; }
        public string TransSeq { get; set; }
        public string AccountKey { get; set; }
        public string AssetNumber { get; set; }
        public string LeaseNumber { get; set; }
        public string VirtualAccount { get; set; }
        public string PaymentUser { get; set; }
        public string PaymentUserID { get; set; }
        public decimal MasterNo { get; set; }
        public string BatchSeq { get; set; }
        public string ImportNumber { get; set; }
        public string _Notes { get; set; }
        public string _Notes1 { get; set; }
        public int PostSeq { get; set; }
        public string UpdatedNotes { get; set; }
        public Nullable<System.DateTime> ValueDate { get; set; }
        public Nullable<System.DateTime> SettleDate { get; set; }
        public Nullable<System.DateTime> PostDate { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public decimal LastUpdatedBy { get; set; }
        public System.DateTime LastUpdatedTime { get; set; }
        public Nullable<System.DateTime> StatBalanceDate { get; set; }
        public string Branch_Code { get; set; }
        public string Book_Type { get; set; }
        public string Currency { get; set; }
        public string Trans_Type { get; set; }
        public string Trade_Type { get; set; }
        public string Department { get; set; }
        public string Option_Type { get; set; }
        public string Account { get; set; }
        public string _Status { get; set; }
        public string DB_CR { get; set; }
        public string Open_Type { get; set; }
    }
}
