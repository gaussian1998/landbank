﻿using Library.Servant.Servant.Common.Models.Mapper;
using System;

namespace Library.Servant.Servant.AccountingQueryDeprn.Model
{
    public class MonthDetailModel : GLAccMonthsModel
    {
        public int _ID
        {
            get
            {
                return ID;
            }
            set
            {
                ID = value;
            }
        }

        public decimal _Amount
        {
            get
            {
                return Amount;
            }
            set
            {
                Amount = value;
            }
        }

        public DateTime OpenDate
        {
            get
            {
                return Open_Date;
            }
            set
            {
                Open_Date = value;
            }
        }

        public string TransSeq
        {
            get
            {
                return Trans_Seq;
            }
            set
            {
                Trans_Seq = value;
            }
        }

        public string AccountKey
        {
            get
            {
                return Account_Key;
            }
            set
            {
                Account_Key = value;
            }
        }
        public string AssetNumber
        {
            get
            {
                return Asset_Number;
            }
            set
            {
                Asset_Number = value;
            }
        }
        public string LeaseNumber
        {
            get
            {
                return Lease_Number;
            }
            set
            {
                Lease_Number = value;
            }
        }

        public string VirtualAccount
        {
            get
            {
                return Virtual_Account;
            }
            set
            {
                Virtual_Account = value;
            }
        }

        public string PaymentUser
        {
            get
            {
                return Payment_User;
            }
            set
            {
                Payment_User = value;
            }
        }

        public string PaymentUserID
        {
            get
            {
                return Payment_User_ID;
            }
            set
            {
                Payment_User_ID = value;
            }
        }

        public decimal MasterNo
        {
            get
            {
                return Master_No;
            }
            set
            {
                Master_No = value;
            }
        }

        public string BatchSeq
        {
            get
            {
                return Batch_Seq;
            }
            set
            {
                Batch_Seq = value;
            }
        }
        public string ImportNumber
        {
            get
            {
                return Import_Number;
            }
            set
            {
                Import_Number = value;
            }
        }

        public string _Notes
        {
            get
            {
                return Notes;
            }
            set
            {
                Notes = value;
            }
        }

        public string _Notes1
        {
            get
            {
                return Notes1;
            }
            set
            {
                Notes1 = value;
            }
        }

        public int PostSeq
        {
            get
            {
                return Post_Seq;
            }
            set
            {
                Post_Seq = value;
            }
        }

        public string UpdatedNotes
        {
            get
            {
                return Updated_Notes;
            }
            set
            {
                Updated_Notes = value;
            }
        }

        public Nullable<System.DateTime> ValueDate
        {
            get
            {
                return Value_Date;
            }
            set
            {
                Value_Date = value;
            }
        }
        public Nullable<System.DateTime> SettleDate
        {
            get
            {
                return Settle_Date;
            }
            set
            {
                Settle_Date = value;
            }
        }
        public Nullable<System.DateTime> PostDate
        {
            get
            {
                return Post_Date;
            }
            set
            {
                Post_Date = value;
            }
        }
        public Nullable<System.DateTime> CloseDate
        {
            get
            {
                return Close_Date;
            }
            set
            {
                Close_Date = value;
            }
        }
        public decimal LastUpdatedBy
        {
            get
            {
                return Last_Updated_By;
            }
            set
            {
                Last_Updated_By = value;
            }
        }
        public System.DateTime LastUpdatedTime
        {
            get
            {
                return Last_Updated_Time;
            }
            set
            {
                Last_Updated_Time = value;
            }
        }
        public Nullable<System.DateTime> StatBalanceDate
        {
            get
            {
                return Stat_Balance_Date;
            }
            set
            {
                Stat_Balance_Date = value;
            }
        }
        public string BranchCode
        {
            get
            {
                return Branch_Code;
            }
            set
            {
                Branch_Code = value;
            }
        }
        public string BookType
        {
            get
            {
                return Book_Type;
            }
            set
            {
                Book_Type = value;
            }
        }
        public string _Currency
        {
            get
            {
                return Currency;
            }
            set
            {
                Currency = value;
            }
        }
        public string TransType
        {
            get
            {
                return Trans_Type;
            }
            set
            {
                Trans_Type = value;
            }
        }
        public string TradeType
        {
            get
            {
                return Trade_Type;
            }
            set
            {
                Trade_Type = value;
            }
        }
        public string _Department
        {
            get
            {
                return Department;
            }
            set
            {
                Department = value;
            }
        }
        public string OpenType
        {
            get
            {
                return Open_Type;
            }
            set
            {
                Open_Type = value;
            }
        }
        public string _Account
        {
            get
            {
                return Account;
            }
            set
            {
                Account = value;
            }
        }
        public string _Status
        {
            get
            {
                return Status;
            }
            set
            {
                Status = value;
            }
        }
    }
}
