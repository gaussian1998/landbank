﻿
using Library.Common.Models;
using Library.Servant.Servant.Accounting;
using System.Collections.Generic;

namespace Library.Servant.Servant.AccountingQueryDeprn.Model
{
    public class IndexModel
    {
        public List<Option> TransTypes { get; set; }
        public List<Option> TradeTypes { get; set; }
        public PageList<IndexItem> Page { get; set; }
        public List<IndexItem> Items { get; set; }
    }
}
