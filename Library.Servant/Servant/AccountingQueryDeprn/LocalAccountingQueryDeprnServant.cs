﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common.Models;
using Library.Entity.Edmx;
using Library.Common.Models;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.Accounting;
using Library.Servant.Servant.Accounting.Models;

namespace Library.Servant.Servant.AccountingQueryDeprn
{
    public class LocalAccountingQueryDeprnServant : IAccountingQueryDeprnServant
    {
        private readonly LocalAccountingQueryServant _servant = new LocalAccountingQueryServant();
        private readonly LocalAccountingDDLServant _ddlServant = new LocalAccountingDDLServant();
        private readonly LocalAccountingSubjectServant _accountServant = new LocalAccountingSubjectServant();
        private readonly LocalTradeDefineServant _tradeDefineServer = new LocalTradeDefineServant();
        private readonly AS_GL_Acc_DeprnRepository _repoDeprn = RepositoryHelper.GetAS_GL_Acc_DeprnRepository();
        public _AccountingQueryModel Index(_AccountingQueryModel model)
        {
            //var ddlTransTypes = _ddlServant.GetTransTypes().ToList();
            //var ddlTradeTypes = _ddlServant.GetTradeTypes().ToList();
            //ddlTransTypes.Add(new Option() { Key = "全部", Value = "" });
            //ddlTradeTypes.Add(new Option() { Key = "全部", Value = "" });
            //ViewBag.TransTypes = ddlTransTypes;
            //ViewBag.TradeTypes = ddlTradeTypes;

            //取得清單
            GetDetail(model);
            //群組結果
            GroupDetail(model);

            model.Total_Record = model.Details.Count();
            if (model.Details.Count() > 1000)
            {
                model.Details = model.Details.Take(1000).ToList();
                model.Total_Record = 1000;
                model.ErrorMessage = "筆數超過1000，請使用匯出功能下載。";
            }
            else
            {
                model.ErrorMessage = "";
            }
            model.Details = model.Details.Skip((model.Page - 1) * 20).Take(20).ToList();
            return model;
        }
        /// <summary>
        /// 取得子清單
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private void GetDetail(_AccountingQueryModel model)
        {

            model.Details = (from p in GetAccDeprn(
                                    model.TransType, model.MasterNo, model.AssetNo,
                                    model.LeaseNo, model.Seq, model.PostSeq, model.BranchCode,
                                    model.DepartmentCode, model.Account).ToList()
                             join tradeDefine in _tradeDefineServer.GetAll().ToList() on p.Master_No equals tradeDefine.Master_No into px
                             from tradeDefine in px.DefaultIfEmpty()
                             join branch in _ddlServant.GetBranch().ToList() on p.Branch_Code equals branch.Value into py
                             from branch in py.DefaultIfEmpty()
                             join dep in _ddlServant.GetDepartment().ToList() on p.Department equals dep.Value into pz
                             from dep in pz.DefaultIfEmpty()
                             select new _AccountQueryDetailModel()
                             {
                                 ID = p.ID,
                                 Open_Date = p.Open_Date,
                                 Master_No = p.Master_No,
                                 BranchDepartment = p.Branch_Code + branch?.Key + "-" + p.Department + dep?.Key,
                                 GL_Account = p.Account,
                                 Account_Name = p.Account_Name,
                                 Amount = p.Amount,
                                 DB_CR = p.DB_CR,
                                 Currency = p.Currency,
                                 Notes = p.Notes,
                                 Post_Date = p.Post_Date,
                                 Account_Key = p.Account_Key,
                                 Trans_Seq = p.Trans_Seq,
                                 Description = tradeDefine.Master_Text
                             }).ToList();
            model.Total_Record = model.Details.Count();
            model.DBAmount = model.Details.Where(p => p.DB_CR == "D").Sum(p => p.Amount);
            model.CRAmount = model.Details.Where(p => p.DB_CR == "C").Sum(p => p.Amount);
        }

        private IQueryable<AS_GL_Acc_Deprn> GetAccDeprn(string transType, int? masterNo, string assetNo, string leaseNo, string seq, int? postSeq, string branchCode, string departmentCode, string account)
        {
            var result = _repoDeprn.All();
            if (!string.IsNullOrEmpty(transType))
            {
                result = result.Where(p => p.Trans_Type == transType);
            }
            if (masterNo.HasValue)
            {
                result = result.Where(p => p.Master_No == masterNo.Value);
            }
            if (!string.IsNullOrEmpty(assetNo) || !string.IsNullOrEmpty(leaseNo))
            {
                result = result.Where(p => p.Asset_Number == assetNo || p.Lease_Number == leaseNo);
            }
            if (!string.IsNullOrEmpty(seq))
            {
                result = result.Where(p => p.Batch_Seq == seq || p.Import_Number == seq);
            }
            if (postSeq.HasValue)
            {
                result = result.Where(p => p.Post_Seq == postSeq.Value);
            }
            if (!string.IsNullOrEmpty(branchCode))
            {
                result = result.Where(p => p.Branch_Code == branchCode);
            }
            if (!string.IsNullOrEmpty(departmentCode))
            {
                result = result.Where(p => p.Department == departmentCode);
            }
            if (!string.IsNullOrEmpty(account))
            {
                result = result.Where(p => p.Account == account);
            }
            return result;
        }

        /// <summary>
        /// 將子項目進行Group
        /// </summary>
        /// <param name="model"></param>
        private void GroupDetail(_AccountingQueryModel model)
        {
            var result = from p in model.Details
                         group p by new
                         {
                             p.Post_Date,
                             p.Master_No,
                             p.Description,
                             p.BranchDepartment,
                             p.GL_Account,
                             p.Account_Name,
                             p.DB_CR,
                             p.Currency,
                             p.Notes,
                             p.Open_Date

                         } into g
                         select new _AccountQueryDetailModel()
                         {
                             ID = 0,
                             Post_Date = g.Key.Post_Date,
                             Master_No = g.Key.Master_No,
                             Description = g.Key.Description,
                             BranchDepartment = g.Key.BranchDepartment,
                             GL_Account = g.Key.GL_Account,
                             Account_Name = g.Key.Account_Name,
                             DB_CR = g.Key.DB_CR,
                             Currency = g.Key.Currency,
                             Notes = g.Key.Notes,
                             Open_Date = g.Key.Open_Date,
                             Amount = g.Sum(x => x.Amount)
                         };
            model.Details = result.ToList();
        }
        public List<SelectedModel> GetTransTypes() {
            var _ddlTransTypes = _ddlServant.GetTransTypes().ToList();
            List<SelectedModel> _returnddl = new List<SelectedModel>();
            _returnddl.Add(new SelectedModel { Name = "全部", Value = "" });
            foreach (Option _option in _ddlTransTypes) {
                _returnddl.Add(new SelectedModel { Name = _option.Key, Value = _option.Value });
            }
            return _returnddl;
        }
        public List<SelectedModel> GetTradeTypes()
        {
            var _ddlTradeTypes = _ddlServant.GetTradeTypes().ToList();
            List<SelectedModel> _returnddl = new List<SelectedModel>();
            _returnddl.Add(new SelectedModel { Name = "全部", Value = "" });
            foreach (Option _option in _ddlTradeTypes)
            {
                _returnddl.Add(new SelectedModel { Name = _option.Key, Value = _option.Value });
            }
            return _returnddl;
        }
        public _AccountingQueryModel Detail(_AccountingQueryModel model) {

            //var ddlTransTypes = _ddlServant.GetTransTypes().ToList();
            //var ddlTradeTypes = _ddlServant.GetTradeTypes().ToList();
            //ddlTransTypes.Add(new Option() { Key = "全部", Value = "" });
            //ddlTradeTypes.Add(new Option() { Key = "全部", Value = "" });
            //ViewBag.TransTypes = ddlTransTypes;
            //ViewBag.TradeTypes = ddlTradeTypes;

            //取得清單
            GetDetail(model);

            if (model.Details.Count() > 1000)
            {
                model.Details = model.Details.Take(1000).ToList();
                model.Total_Record = 1000;
                model.ErrorMessage = "筆數超過1000，請使用匯出功能下載。";
            }
            else
            {
                model.ErrorMessage = "";
            }
            model.Details = model.Details.Skip((model.Page - 1) * 20).Take(20).ToList();
            return model;
        }
        public string GetBranchName(string branchCode)
        {
            var result = _ddlServant.GetBranch().FirstOrDefault(p => p.Value == branchCode)?.Key;
            return result ?? "沒有這一個分行";
        }
        public string GetDepartmentName(string departmentCode)
        {
            var result = _ddlServant.GetDepartment().FirstOrDefault(p => p.Value == departmentCode)?.Key;
            return result ?? "沒有這一個部門";
        }
        public _AccountingQueryModel ExportGroupExcel(_AccountingQueryModel model) {
            GetDetail(model);
            GroupDetail(model);
            return model;
        }
        public List<_AccountQueryDetailModel> ExportDetailExcel(IEnumerable<_ExportModel> models)
        {
            var details = new List<_AccountQueryDetailModel>();
            foreach (var model in models.Where(p => p.DetailChecked))
            {
                var accountingModel = new _AccountingQueryModel()
                {
                    Account = model.GL_Account,
                    AssetNo = model.AssetNo,
                    BranchCode = model.BranchCode,
                    DepartmentCode = model.DepartmentCode,
                    LeaseNo = model.LeaseNo,
                    TransType = model.TransType,
                    MasterNo = model.Master_No,
                    Seq = model.Seq,
                    TransSeq = model.TransSeq
                };
                GetDetail(accountingModel);
                details.AddRange(accountingModel.Details);
            }
            return details;
        }
        public string DeleteBatch(int[] id, string deleteReason,int userId ) {
            if (string.IsNullOrEmpty(deleteReason))
            {
                return "沒有輸入刪除原因。";
            }
            _servant.UserId = userId;
            foreach (var item in id)
            {
                var model = GetAccDeprnById(item);
                if (model != null)
                {
                    _servant.DeleteDeprn(item, deleteReason);
                }
            }
            return "";
        }

        private AS_GL_Acc_Deprn GetAccDeprnById(int id)
        {
            return _repoDeprn.All().FirstOrDefault(p => p.ID == id);
        }
    }


}
