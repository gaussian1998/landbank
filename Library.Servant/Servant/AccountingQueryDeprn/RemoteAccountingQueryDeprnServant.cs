﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common.Models;
using Library.Entity.Edmx;
using Library.Common.Models;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.Accounting;
using Library.Servant.Servant.Accounting.Models;

namespace Library.Servant.Servant.AccountingQueryDeprn
{
    public class RemoteAccountingQueryDeprnServant : IAccountingQueryDeprnServant
    {
        public string DeleteBatch(int[] id, string deleteReason, int userId)
        {
            throw new NotImplementedException();
        }

        public _AccountingQueryModel Detail(_AccountingQueryModel model)
        {
            throw new NotImplementedException();
        }

        public List<_AccountQueryDetailModel> ExportDetailExcel(IEnumerable<_ExportModel> models)
        {
            throw new NotImplementedException();
        }

        public _AccountingQueryModel ExportGroupExcel(_AccountingQueryModel model)
        {
            throw new NotImplementedException();
        }

        public string GetBranchName(string branchCode)
        {
            throw new NotImplementedException();
        }
        public string GetDepartmentName(string departmentCode)
        {
            throw new NotImplementedException();
        }
        public List<SelectedModel> GetTradeTypes()
        {
            throw new NotImplementedException();
        }
        public List<SelectedModel> GetTransTypes()
        {
            throw new NotImplementedException();
        }
        public _AccountingQueryModel Index(_AccountingQueryModel model)
        {
            throw new NotImplementedException();
        }
    }
}
