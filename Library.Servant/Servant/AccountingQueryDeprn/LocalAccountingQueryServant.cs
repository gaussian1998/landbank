﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Library.Entity.Edmx;
using Library.Servant.Servant.AccountingQueryDeprn.Model;
using Library.Servant.Servant.Accounting.Models;
using System.Linq.Expressions;
using Library.Common.Utility;

namespace Library.Servant.Servant.Accounting
{
    public class LocalAccountingQueryServant : IAccountingQueryServant
    {
        private readonly AS_GL_Acc_MonthsRepository _repoMonths = RepositoryHelper.GetAS_GL_Acc_MonthsRepository();
        private readonly AS_GL_Acc_DetailRepository _repoDetail = RepositoryHelper.GetAS_GL_Acc_DetailRepository();
        private readonly AS_GL_Acc_DeletedRepository _repoDelete = RepositoryHelper.GetAS_GL_Acc_DeletedRepository();
        private readonly AS_GL_Acc_DeprnRepository _repoDeprn = RepositoryHelper.GetAS_GL_Acc_DeprnRepository();
        private readonly AS_Branch_CurrencyRepository _repoBranchCurrency = RepositoryHelper.GetAS_Branch_CurrencyRepository();
        private readonly AS_GL_AccountRepository _repoAccount = RepositoryHelper.GetAS_GL_AccountRepository();
        private readonly Acc_EntriesByHeaderRepository _repoEntriesGroupByHeader = RepositoryHelper.GetAcc_EntriesByHeaderRepository();
        private readonly LocalAccountingDDLServant ddl = new LocalAccountingDDLServant();
        private readonly LocalTradeDefineServant _tradeDefineServer = new LocalTradeDefineServant();
        private readonly LocalAccountingDDLServant _ddlServant = new LocalAccountingDDLServant();
        private readonly LocalAccountingSubjectServant _accountServant = new LocalAccountingSubjectServant();

        public int UserId { get; set; }

        public IndexModel Index(_AccountingQueryModel search)
        {
             var result = new IndexModel {

                TransTypes = ddl.GetTransTypes().ToList(),
                TradeTypes = ddl.GetTradeTypes().ToList(),
                Page = AS_GL_Acc_Months_Records.Page<IndexItem, int>(search.Page, 20, m => m.ID, search.Expression())
             };

            Dictionary<decimal, AS_GL_Trade_Define> tradeDefines = _tradeDefineServer.GetAll().ToList().ToDictionary(m => m.Master_No.HasValue ? m.Master_No.Value : 0);
            result.Page.Items.ForEach( item => item.Description = tradeDefines[item.MasterNo].Master_Text);
            return result;
        }

        public MonthDetailModel MonthDetail(int id)
        {
            return AS_GL_Acc_Months_Records.First<MonthDetailModel>( m => m.ID == id );
        }

        public AS_GL_Acc_Detail GetAccDetailById(int id)
        {
            return _repoDetail.All().FirstOrDefault(p => p.ID == id);
        }

        public void DeleteAccMonth(int accMonthId, string message)
        {

            _repoDelete.UnitOfWork = _repoMonths.UnitOfWork;
            var record = _repoMonths.All().FirstOrDefault(p => p.ID == accMonthId);
            if (record == null) return;
            _repoDelete.Add(new AS_GL_Acc_Deleted()
            {
                Open_Date = record.Open_Date,
                Master_No = record.Master_No,
                Category = record.Category,
                Trans_Seq = record.Trans_Seq,
                Account_Key = record.Account_Key,
                Account = record.Account,
                Currency = record.Currency,
                Account_Name = record.Account_Name,
                Amount = record.Amount,
                Notes = record.Notes,
                Notes1 = record.Notes1,
                Post_Date = record.Post_Date,
                DB_CR = record.DB_CR,
                Asset_Liablty = record.Asset_Liablty,
                Asset_Number = record.Asset_Number,
                Batch_Seq = record.Batch_Seq,
                Book_Type = record.Book_Type,
                Branch_Code = record.Branch_Code,
                Branch_Ind = record.Branch_Ind,
                Close_Date = record.Close_Date,
                Department = record.Department,
                GL_Account = record.GL_Account,
                Import_Number = record.Import_Number,
                Lease_Number = record.Lease_Number,

                Open_Type = record.Open_Type,
                Payment_User = record.Payment_User,
                Payment_User_ID = record.Payment_User_ID,
                Post_Seq = record.Post_Seq,
                Real_Memo = record.Real_Memo,
                Settle_Date = record.Settle_Date,
                Soft_Lock = record.Soft_Lock,
                Status = record.Status,
                Stat_Balance_Date = record.Stat_Balance_Date,
                Stat_Frequency = record.Stat_Frequency,
                Trade_Type = record.Trade_Type,
                Trans_Type = record.Trans_Type,
                Value_Date = record.Value_Date,
                Virtual_Account = record.Virtual_Account,
                Deleted_Notes = message,
                Deleted_By = UserId,
                Deleted_Time = DateTime.Now,
                Is_Deleted = true,
            });
            _repoMonths.Delete(record);
            _repoMonths.UnitOfWork.Commit();
        }

        public void AddAccountingAccMonthItem(AS_GL_Acc_Months model)
        {
            _repoAccount.UnitOfWork = _repoMonths.UnitOfWork;
            string liablty = model.Account.Substring(0, 1) == "1" ? "A" : model.Account.Substring(0, 1) == "2" ? "L" : "";
            int maxPostSeq = _repoMonths.All().OrderByDescending(p => p.Post_Seq).FirstOrDefault()?.Post_Seq ?? 0;
            _repoMonths.Add(new AS_GL_Acc_Months()
            {

                Book_Type = model.Book_Type,
                Branch_Code = model.Branch_Code,
                Department = string.IsNullOrEmpty(model.Department) ? "" : model.Department,
                Branch_Ind = model.Branch_Code == "001" ? "H" : "B",
                Account = model.Account,
                Account_Name = _repoAccount.All().FirstOrDefault(p => p.Account == model.Account)?.Account_Name,
                Asset_Liablty = liablty,
                Category = "C",
                Currency = model.Currency,
                GL_Account = model.Branch_Code + (string.IsNullOrEmpty(model.Department) ? "" : model.Department) + "/" + model.Account + "/" + model.Currency,
                Master_No = 0,
                Open_Date = DateTime.Today,
                Open_Type = "MA",
                Trans_Type = model.Trans_Type,
                Account_Key = "M330",
                Trans_Seq = "",
                Trade_Type = model.Trade_Type,
                Post_Seq = maxPostSeq + 1,
                DB_CR = model.DB_CR,
                Real_Memo = "R",
                Stat_Balance_Date = DateTime.Parse("1900/01/01"),
                Status = "0",
                Asset_Number = "",
                Lease_Number = "",
                Virtual_Account = "",
                Payment_User = "",
                Payment_User_ID = "",
                Batch_Seq = "",
                Import_Number = "",
                Soft_Lock = false,
                Close_Date = DateTime.Parse("1900/01/01"),
                Stat_Frequency = 0,
                Post_Date = DateTime.Parse("1900/01/01"),
                Value_Date = DateTime.Parse("1900/01/01"),
                Settle_Date = DateTime.Parse("1900/01/01"),
                Amount = model.Amount,
                Notes = model.Notes,
                Notes1 = "",
                Last_Updated_Time = DateTime.Now,
                Last_Updated_By = UserId,
                Is_Updated = false, //true是修改，false是新增
                Updated_Notes = model.Updated_Notes,
            });
            _repoMonths.UnitOfWork.Commit();
        }

        public string GetCurrencyByBookType(string bookType)
        {
            return _repoBranchCurrency.All().FirstOrDefault(p => p.Book_Type == bookType)?.Currency;
        }

        public void Update(UpdateModel model)
        {
            PreCheckUpdate(model);
            AS_GL_Acc_Months_Records.Update(m => m.ID == model._ID, entity =>
            {
                entity.Branch_Code = model.Branch_Code;
                entity.Department = string.IsNullOrEmpty(model.Department) ? "" : model.Department;
                entity.Book_Type = model.Book_Type;
                entity.Currency = model.Currency;
                entity.DB_CR = model.DB_CR;
                entity.Account = model.Account;
                entity.Account_Name = _repoAccount.All().FirstOrDefault(p => p.Account == model.Account)?.Account_Name;
                entity.Trans_Type = model.Trans_Type;
                entity.Trade_Type = model.Trade_Type;
                entity.Amount = model._Amount;
                entity.Open_Type = model.Open_Type;
                entity.GL_Account = model.Branch_Code + (string.IsNullOrEmpty(model.Department) ? "" : model.Department) +
                                    "/" + model.Account + "/" + model.Currency;
                entity.Trans_Seq = model.TransSeq ?? "";
                entity.Account_Key = model.AccountKey ?? "";
                entity.Asset_Number = model.AssetNumber ?? "";
                entity.Lease_Number = model.LeaseNumber ?? "";
                entity.Virtual_Account = model.VirtualAccount ?? "";
                entity.Payment_User_ID = model.PaymentUserID ?? "";
                entity.Payment_User = model.PaymentUser ?? "";
                entity.Master_No = model.MasterNo;
                entity.Import_Number = model.ImportNumber ?? "";
                entity.Batch_Seq = model.BatchSeq ?? "";
                entity.Notes = model._Notes;
                entity.Notes1 = model._Notes1 ?? "";
                entity.Last_Updated_Time = DateTime.Now;
                entity.Last_Updated_By = UserId;
                entity.Is_Updated = true; //true是修改，false是新增
                entity.Updated_Notes = model.UpdatedNotes;
                entity.Asset_Liablty = model.Account.Substring(0, 1) == "1" ? "A" : model.Account.Substring(0, 1) == "2" ? "L" : "";
            });
        }

        private void PreCheckUpdate(UpdateModel model)
        {
            string errorMessage = string.Empty;
            if (_ddlServant.GetBranch().All(p => p.Value != model.Branch_Code))
                errorMessage += "找不到銀行代碼 , ";

            if (_accountServant.GetAccountInfo(model.Account) == null)
                errorMessage += "找不到科子細目編號 , ";

            if (string.IsNullOrEmpty(model._Notes) || string.IsNullOrEmpty(model.UpdatedNotes))
                errorMessage += "請輸入備註及補輸原因 , ";

            if (model._Amount <= 0)
                errorMessage += "請輸入金額 , ";

            if( string.IsNullOrEmpty(errorMessage) == false )
                throw new Exception(errorMessage);
        }


        public void DeleteDeprn(int id, string message)
        {
            _repoDelete.UnitOfWork = _repoDeprn.UnitOfWork;
            var record = _repoDeprn.All().FirstOrDefault(p => p.ID == id);
            if (record == null) return;
            _repoDelete.Add(new AS_GL_Acc_Deleted()
            {
                Open_Date = record.Open_Date,
                Master_No = record.Master_No,
                Category = record.Category,
                Trans_Seq = record.Trans_Seq,
                Account_Key = record.Account_Key,
                Account = record.Account,
                Currency = record.Currency,
                Account_Name = record.Account_Name,
                Amount = record.Amount,
                Notes = record.Notes,
                Notes1 = record.Notes1,
                Post_Date = record.Post_Date,
                DB_CR = record.DB_CR,
                Asset_Liablty = record.Asset_Liablty,
                Asset_Number = record.Asset_Number,
                Batch_Seq = record.Batch_Seq,
                Book_Type = record.Book_Type,
                Branch_Code = record.Branch_Code,
                Branch_Ind = record.Branch_Ind,
                Close_Date = record.Close_Date,
                Department = record.Department,
                GL_Account = record.GL_Account,
                Import_Number = record.Import_Number,
                Lease_Number = record.Lease_Number,

                Open_Type = record.Open_Type,
                Payment_User = record.Payment_User,
                Payment_User_ID = record.Payment_User_ID,
                Post_Seq = record.Post_Seq,
                Real_Memo = record.Real_Memo,
                Settle_Date = record.Settle_Date,
                Soft_Lock = record.Soft_Lock,
                Status = record.Status,
                Stat_Balance_Date = record.Stat_Balance_Date,
                Stat_Frequency = record.Stat_Frequency,
                Trade_Type = record.Trade_Type,
                Trans_Type = record.Trans_Type,
                Value_Date = record.Value_Date,
                Virtual_Account = record.Virtual_Account,
                Deleted_Notes = message,
                Deleted_By = UserId,
                Deleted_Time = DateTime.Now,
                Is_Deleted = true,
            });
            _repoDeprn.Delete(record);
            _repoDeprn.UnitOfWork.Commit();
        }

        public IEnumerable<Acc_EntriesByHeader> GetAccountingEntries()
        {
            return _repoEntriesGroupByHeader.All();
        }
    }

    static class LocalExtension
    {
        public static Expression<Func<AS_GL_Acc_Months, bool>> Expression(this _AccountingQueryModel search)
        {
            bool isSkipTransType = string.IsNullOrWhiteSpace(search.TransType);
            bool isSkipTradeType = string.IsNullOrWhiteSpace(search.TradeType);
            bool isSkipMasterNo = !search.MasterNo.HasValue;
            bool isSkipAssetNo = string.IsNullOrWhiteSpace(search.AssetNo);
            bool isSkipBatchSeq = string.IsNullOrWhiteSpace(search.Seq);
            bool isSkipTransSeq = string.IsNullOrWhiteSpace(search.TransSeq);

            return ExpressionMaker.Make<AS_GL_Acc_Months, bool>(
                m =>
                (search.BeginOpenDate <= m.Open_Date && m.Open_Date <= search.EndOpenDate) &&
                (isSkipTransType || m.Trans_Type == search.TransType) &&
                (isSkipTradeType || m.Trade_Type == search.TradeType) &&
                (isSkipMasterNo || m.Master_No == search.MasterNo) &&
                (isSkipAssetNo || m.Asset_Number == search.AssetNo) &&
                (isSkipBatchSeq || m.Batch_Seq == search.Seq) &&
                (isSkipBatchSeq || m.Import_Number == search.Seq) &&
                (isSkipTransSeq || m.Trans_Seq == search.TransSeq));
        }
    }
}
