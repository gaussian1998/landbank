﻿using Library.Servant.Servant.AgentFlow.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.AgentFlow
{
    public class RemoteAgentFlowServant : IAgentFlowServant
    {
        public NewResult New()
        {
            return RemoteServant.Post<VoidModel, NewResult>(

                new VoidModel { },
                "/AgentFlowServant/New"
            );
        }
        public NewResult New(string oprationType)
        {
            return RemoteServant.Post<StringModel, NewResult>(

                new StringModel {Value= oprationType },
                "/AgentFlowServant/NewByOprationType"
            );
        }
        public IndexResult Index(SearchModel search)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(

                search,
                "/AgentFlowServant/Index"
            );
        }

        public VoidResult Create(CreateModel vo)
        {
            return RemoteServant.Post<CreateModel, VoidResult>(

                vo,
                "/AgentFlowServant/Create"
            );
        }

        public VoidResult Delete(int ID)
        {
            return RemoteServant.Post<IntModel, VoidResult>(

                new IntModel { Value = ID },
                "/AgentFlowServant/Delete"
            );
        }

        public DetailsResult Details(int ID)
        {
            return RemoteServant.Post<IntModel, DetailsResult>(

                new IntModel { Value = ID },
                "/AgentFlowServant/Details"
            );
        }

        public VoidResult Update(UpdateModel vo)
        {
            return RemoteServant.Post<UpdateModel, VoidResult>(

                vo,
                "/AgentFlowServant/Update"
            );
        }

        public EditResult Edit(int ID)
        {
            return RemoteServant.Post<IntModel, EditResult>(

                new IntModel { Value = ID },
                "/AgentFlowServant/Edit"
            );
        }
    }
}
