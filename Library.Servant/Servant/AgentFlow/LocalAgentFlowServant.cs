﻿using System;
using System.Linq.Expressions;
using System.Linq;
using System.Collections.Generic;
using Library.Entity.Edmx;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.AgentFlow.Models;
using Library.Servant.Servant.Common.Models;
using Library.Common.Utility;
using Library.Servant.Servant.CodeTable;

namespace Library.Servant.Servant.AgentFlow
{
    public class LocalAgentFlowServant : IAgentFlowServant
    {
        private IStaticDataServant _staticDataServant = new LocalStaticDataServant();
        public NewResult New()
        {
            var flow_name_options = AS_Flow_Name_Records.FindAll<FlowNameOption>();
            var doc_type_options = LocalCodeTableServant.DocTypeDictionary();

            return new NewResult
            {
                DocTypeOptions = AS_Doc_Name_Records.Find(m => m.AS_Agent_Flow.Count == 0).Select( m => new OptionModel<string, string> { Value = m.Doc_Type, Text = doc_type_options[m.Doc_Type] }).Distinct( new OptionComparer { } ).ToList(),
                DocTypeGroupDocNameOptions = AS_Doc_Name_Records.Group( m => m.AS_Agent_Flow.Count==0 , m => m.Doc_Type, m => new OptionModel<int, string> { Value = m.ID, Text = "[" + m.Doc_No + "] " + m.Doc_Name }),

                FlowNameOptions = flow_name_options.Select(m => new OptionModel<int, string> { Value = m.ID, Text = m.Flow_Name }).ToList(),
                FlowNameGroupFlowTemplateOptions = FlowNameGroupFlowTemplateOptions(flow_name_options)
            };
        }
        public NewResult New(string oprationType)
        {
            var flow_name_options = AS_Flow_Name_Records.Find<FlowNameOption>(x=>x.Flow_Type==oprationType);
            var doc_type_options = LocalCodeTableServant.DocTypeDictionary();
            return new NewResult
            {
                DocTypeOptions = AS_Doc_Name_Records.Find(m => m.AS_Agent_Flow.Count == 0).Select(m => new OptionModel<string, string> { Value = m.Doc_Type, Text = doc_type_options[m.Doc_Type] }).Distinct(new OptionComparer { }).ToList(),
                DocTypeGroupDocNameOptions = AS_Doc_Name_Records.Group(m => m.AS_Agent_Flow.Count == 0, m => m.Doc_Type, m => new OptionModel<int, string> { Value = m.ID, Text = "[" + m.Doc_No + "] " + m.Doc_Name }),

                FlowNameOptions = flow_name_options.Select(m => new OptionModel<int, string> { Value = m.ID, Text = m.Flow_Name }).ToList(),
                FlowNameGroupFlowTemplateOptions = FlowNameGroupFlowTemplateOptions(flow_name_options)
            };
        }

        public IndexResult Index(SearchModel search)
        {
            return search.IsSet ? FindAlreadySet(search) : FindNotSet(search);
        }
        
        public VoidResult Create(CreateModel vo)
        {
            AS_Doc_Name_Records.Update( m => m.ID == vo.DocID, entity => {

                int Version = entity.AS_Agent_Flow.Any() ? entity.AS_Agent_Flow.Max(e => e.Version) + 1 : 1;
                entity.AS_Agent_Flow.Add( new AS_Agent_Flow { Flow_Template_ID = vo.FlowTemplateID, Version = Version } );
            } );

            return new VoidResult();
        }

        public EditResult Edit(int ID)
        {
            List<SelectedModel> _listdoctypes = _staticDataServant.GetDocTypes();
            var result = AS_Doc_Name_Records.First(m => m.ID == ID, entity => new EditResult
            {
                ID = entity.ID,
                DocType = entity.Doc_Type,
                DocTypeName= entity.Doc_Type_Name,
                DocName = "[" + entity.Doc_No + "] " + entity.Doc_Name,
                NewestVersion = entity.AS_Agent_Flow.Any() ? entity.AS_Agent_Flow.Max( e => e.Version ) : 0,
                FlowTemplateName = entity.AS_Agent_Flow.FirstOrDefault(m => m.Version == entity.AS_Agent_Flow.Max(e => e.Version)).AS_Flow_Template.AS_Flow_Name.Flow_Name,
                FlowTemplateDescription = entity.AS_Agent_Flow.FirstOrDefault(m => m.Version == entity.AS_Agent_Flow.Max(e => e.Version)).AS_Flow_Template.AS_Flow_Template_Flow_Role.Select( e => e.AS_Code_Table.Text ).ToList(),
            } );
            result.DocTypeName = _listdoctypes.Where(x => x.Value == result.DocType).FirstOrDefault().Name;
            var flow_name_options = AS_Flow_Name_Records.FindAll<FlowNameOption>();
            result.FlowNameOptions = flow_name_options.Select(m => new OptionModel<int, string> { Value = m.ID, Text = m.Flow_Name }).ToList();
            result.FlowNameGroupFlowTemplateOptions = FlowNameGroupFlowTemplateOptions(flow_name_options);
            return result;
        }

        public VoidResult Update(UpdateModel vo)
        {
            return Create( new CreateModel { DocID = vo.ID, FlowTemplateID = vo.FlowTemplateID } );
        }

        public VoidResult Delete(int ID)
        {
            AS_Agent_Flow_Records.DeleteFirst(m => m.ID == ID);
            return new VoidResult();
        }

        private static Expression<Func<AS_Agent_Flow, DetailsResult>> Convert()
        {
            return ExpressionMaker.Make<AS_Agent_Flow, DetailsResult>(entity => new DetailsResult
            {
                DocID = entity.AS_Doc_Name.ID,
                DocCode = entity.AS_Doc_Name.Doc_No,
                DocName = entity.AS_Doc_Name.Doc_Name,
                DocTypeName = entity.AS_Doc_Name.Doc_Type_Name,
                FlowCodeName = entity.AS_Flow_Template.AS_Flow_Name.Flow_Name,
                Version = entity.Version,
                DocType=entity.AS_Doc_Name.Doc_Type,
                //Descriptions = entity.AS_Flow_Template.AS_Flow_Template_Flow_Role.Select(m => m.AS_Code_Table.Code_ID + m.AS_Code_Table.Text).ToList()
                Descriptions = entity.AS_Flow_Template.AS_Flow_Template_Flow_Role.Select(m =>  m.AS_Code_Table.Text).ToList()
            });
        }

        private  IndexResult FindNewest(SearchModel search)
        {
            bool skipDocCode = string.IsNullOrWhiteSpace(search.DocCode);
            bool skipDocType = string.IsNullOrWhiteSpace( search.DocType );
            IndexResult _indexResult = new IndexResult
            {
                Page = AS_Agent_Flow_Records.Page(

                    search.Page,
                    search.PageSize,
                    m =>  new { m.AS_Doc_Name.Doc_Type, m.AS_Doc_Name.Doc_No},
                    m => (skipDocCode || search.DocCode == m.AS_Doc_Name.Doc_No) &&
                                (skipDocType || search.DocType == m.AS_Doc_Name.Doc_Type) &&
                                (m.Version == m.AS_Doc_Name.AS_Agent_Flow.Max(e => e.Version)),
                                Convert())
            };
            List<SelectedModel> _listdoctypes = _staticDataServant.GetDocTypes();
            List<DetailsResult> _listDetailsResult = new List<DetailsResult>();
            foreach (DetailsResult _detailsResult in _indexResult.Page.Items)
            {
                _detailsResult.DocTypeName = _listdoctypes.Where(x => x.Value == _detailsResult.DocType).FirstOrDefault().Name;
                _listDetailsResult.Add(_detailsResult);
            }
            _indexResult.Page.Items = _listDetailsResult;
            return _indexResult;
        }

        private IndexResult FindAll(SearchModel search)
        {
            bool skipDocCode = string.IsNullOrWhiteSpace(search.DocCode);
            bool skipDocType = string.IsNullOrWhiteSpace( search.DocType );

            IndexResult _indexResult = new IndexResult
            {
                Page = AS_Agent_Flow_Records.Page(

                    search.Page,
                    search.PageSize,
                    m => new { m.AS_Doc_Name.Doc_Type, m.AS_Doc_Name.Doc_No },
                    m => (skipDocCode || search.DocCode == m.AS_Doc_Name.Doc_No) &&
                                (skipDocType || search.DocType == m.AS_Doc_Name.Doc_Type),
                                Convert())
            };
            List<SelectedModel> _listdoctypes = _staticDataServant.GetDocTypes();
            List<DetailsResult> _listDetailsResult = new List<DetailsResult>();
            foreach (DetailsResult _detailsResult in _indexResult.Page.Items)
            {
                _detailsResult.DocTypeName = _listdoctypes.Where(x => x.Value == _detailsResult.DocType).FirstOrDefault().Name;
                _listDetailsResult.Add(_detailsResult);
            }
            _indexResult.Page.Items = _listDetailsResult;
            return _indexResult;
        }

        private  IndexResult FindAlreadySet(SearchModel search)
        { 
                return search.IsLatest ? FindNewest(search) : FindAll(search);
        }

        private IndexResult FindNotSet(SearchModel search)
        {
            bool skipDocCode = string.IsNullOrWhiteSpace(search.DocCode);
            bool skipDocType = string.IsNullOrWhiteSpace( search.DocType );

            IndexResult _indexResult = new IndexResult
            {
                Page = AS_Doc_Name_Records.Page(

                   search.Page,
                   search.PageSize,
                   m => new { m.Doc_Type,m.Doc_No},
                   m => (skipDocCode || search.DocCode == m.Doc_No) &&
                               (skipDocType || search.DocType == m.Doc_Type) &&
                               (m.AS_Agent_Flow.Count == 0),
                   entity => new DetailsResult
                   {
                       DocID = entity.ID,
                       DocCode = entity.Doc_No,
                       DocName = entity.Doc_Name,
                       DocTypeName = entity.Doc_Type_Name,
                       FlowCodeName = "",
                       DocType=entity.Doc_Type,
                       Version = 0
                   })
            };
            List<SelectedModel> _listdoctypes = _staticDataServant.GetDocTypes();
            List<DetailsResult> _listDetailsResult = new List<DetailsResult>();
            foreach (DetailsResult _detailsResult in _indexResult.Page.Items)
            {
                _detailsResult.DocTypeName = _listdoctypes.Where(x => x.Value == _detailsResult.DocType).FirstOrDefault().Name;
                _listDetailsResult.Add(_detailsResult);
            }
            _indexResult.Page.Items = _listDetailsResult;
            return _indexResult;
        }

        private static Dictionary<string, OptionModel<int, string>> FlowNameGroupFlowTemplateOptions(List<FlowNameOption> flow_name_options)
        {
            Dictionary<string, OptionModel<int, string>> result = new Dictionary<string, OptionModel<int, string>>();

            foreach (var option in flow_name_options)
            {
                if (option.NewestTemplateID.HasValue)
                    result.Add(option.ID.ToString(), new OptionModel<int, string> { Value = option.NewestTemplateID.Value, Text = option.FlowRolesDescrption });
                else
                    result.Add(option.ID.ToString(), null);
            }

            return result;
        }
    }

    class OptionComparer : IEqualityComparer<OptionModel<string, string>>
    {
        public bool Equals(OptionModel<string, string> x, OptionModel<string, string> y)
        {
            return x.Value == y.Value;
        }

        public int GetHashCode(OptionModel<string, string> obj)
        {
            return obj.Value.GetHashCode();
        }
    }

    class FlowNameOption
    {
        public int ID { get; set; }
        public string Flow_Name { get; set; }
        public int? NewestTemplateID { get; set; }
        public string FlowRolesDescrption { get; set; }

        public ICollection<AS_Flow_Template> AS_Flow_Template
        {
            set
            {
                if (value.Any())
                {
                    var newest_template = value.First(m => m.Version == value.Max(e => e.Version));

                    NewestTemplateID = newest_template.ID;
                    FlowRolesDescrption = string.Join("-", newest_template.AS_Flow_Template_Flow_Role.Select(m => m.AS_Code_Table.Code_ID + m.AS_Code_Table.Text));
                }
            }
        }

        public FlowNameOption()
        {
            FlowRolesDescrption = string.Empty;
        }
    }
}