﻿using Library.Common.Models;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.AgentFlow.Models
{
    public class IndexResult : InvasionEncryption
    {
        public PageList<DetailsResult> Page { get; set; }
    }
}
