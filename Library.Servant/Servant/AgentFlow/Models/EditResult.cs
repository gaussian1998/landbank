﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Servant.AgentFlow.Models
{
    public class EditResult : AbstractEncryptionDTO
    {
        public int ID { get; set; }
        public string DocType { get; set; }
        public string DocTypeName { get; set; }
        public string DocName { get; set; }
        public int NewestVersion { get; set; }
        public string FlowTemplateName { get; set; }
        public List<string> FlowTemplateDescription { get; set; }
        public List<OptionModel<int, string>> FlowNameOptions { get; set; }
        public Dictionary<string, OptionModel<int, string>> FlowNameGroupFlowTemplateOptions { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
