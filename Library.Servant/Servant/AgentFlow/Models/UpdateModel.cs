﻿using Library.Servant.Communicate;

namespace Library.Servant.Servant.AgentFlow.Models
{
    public class UpdateModel : InvasionEncryption
    {
        public int ID { get; set; }
        public int FlowTemplateID { get; set; }
    }
}
