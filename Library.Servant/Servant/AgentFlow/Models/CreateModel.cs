﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;

namespace Library.Servant.Servant.AgentFlow.Models
{
    public class CreateModel : AbstractEncryptionDTO
    {
        public int DocID { get; set; }
        public int FlowTemplateID { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
