﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Servant.AgentFlow.Models
{
    public class DetailsResult : InvasionEncryption
    {
        public int DocID { get; set; }
        public string DocCode { get; set; }
        public string DocName { get; set; }
        public string DocTypeName { get; set; }
        public string DocType{ get; set; }
        public string FlowCodeName { get; set; }
        public decimal Version { get; set; }
        public List<string> Descriptions { get; set; }
        
    }
}
