﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace Library.Servant.Servant.AgentFlow.Models
{
    public class NewResult : InvasionEncryption
    {
        public List<OptionModel<string, string>> DocTypeOptions { get; set; }
        public Dictionary< string, List<OptionModel<int, string>>> DocTypeGroupDocNameOptions { get; set; }

        public List<OptionModel<int, string>> FlowNameOptions { get; set; }
        public Dictionary<string, OptionModel<int, string>> FlowNameGroupFlowTemplateOptions { get; set; }

    }
}
