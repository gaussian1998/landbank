﻿using Library.Servant.Communicate;

namespace Library.Servant.Servant.AgentFlow.Models
{
    public class SearchModel : InvasionEncryption
    {
        public string DocCode { get; set; }
        public string DocType { get; set; }
        public bool IsSet { get; set; }
        public bool IsLatest { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}
