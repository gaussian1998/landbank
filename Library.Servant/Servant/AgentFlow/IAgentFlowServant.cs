﻿using Library.Servant.Servant.AgentFlow.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.AgentFlow
{
    public interface IAgentFlowServant
    {
        NewResult New();
        IndexResult Index(SearchModel search);
        VoidResult Create(CreateModel vo);
        EditResult Edit(int ID);
        VoidResult Update(UpdateModel vo);
        VoidResult Delete(int ID);
        NewResult New(string oprationType);
    }
}
