﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AccountingImpairmGroup.Models
{
    public class DetailModel : InvasionEncryption
    {
        public int ID { get; set; }
        public int ImpairmGroupNo { get; set; }
        public string AssetCategoryCode { get; set; }
        public string UseType { get; set; }
        public string AssetNumber { get; set; }
        public string Account { get; set; }
        public string AccountName { get; set; }
    }
}
