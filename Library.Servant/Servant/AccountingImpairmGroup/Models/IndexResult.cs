﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Common.Models;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AccountingImpairmGroup.Models
{
    public class IndexResult : InvasionEncryption
    {
        public SearchModel Conditions { get; set; }
        public PageList<DetailModel> Results { get; set; }
    }
}
