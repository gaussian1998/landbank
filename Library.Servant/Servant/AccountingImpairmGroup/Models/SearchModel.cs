﻿using Library.Servant.Communicate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.AccountingImpairmGroup.Models
{
    public class SearchModel : InvasionEncryption
    {
        public int FromNo { get; set; }
        public int ToNo { get; set; }
        public string MainKind { get; set; }
        public string DetailKind { get; set; }
        public string UseType { get; set; }
        public string FromSubject { get; set; }
        public string ToSubject { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
    }
}
