﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingImpairmGroup.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.AccountingImpairmGroup
{
    public class RemoteAccountingImpairmGroupServant : IAccountingImpairmGroupServant
    {
        public MessageModel BatchDelete(BatchDeleteModel BatchDelete)
        {
            return RemoteServant.Post<BatchDeleteModel, MessageModel>(
                           BatchDelete,
                           "/AccountingImpairmGroupServant/BatchDelete"
          );
        }
        public MessageModel Create(DetailModel Create)
        {
            return RemoteServant.Post<DetailModel, MessageModel>(
                           Create,
                           "/AccountingImpairmGroupServant/Create"
          );
        }

        public DetailModel Detail(int Id)
        {
            return RemoteServant.Post<IntModel, DetailModel>(
                           new IntModel { Value=Id},
                           "/AccountingImpairmGroupServant/Detail"
          );
        }

        public IndexResult Index(SearchModel Search)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(
                           Search,
                           "/AccountingImpairmGroupServant/Index"
          );
        }

        public MessageModel Update(DetailModel Update)
        {
            return RemoteServant.Post<DetailModel, MessageModel>(
                           Update,
                           "/AccountingImpairmGroupServant/Update"
          );
        }

    }
}
