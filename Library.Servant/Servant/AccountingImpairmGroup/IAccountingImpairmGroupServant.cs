﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingImpairmGroup.Models;
using Library.Common.Models;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.AccountingImpairmGroup
{
    public interface IAccountingImpairmGroupServant
    {
        IndexResult Index(SearchModel Search);
        MessageModel Create(DetailModel Create);
        MessageModel Update(DetailModel Update);
        MessageModel BatchDelete(BatchDeleteModel BatchDelete);
        DetailModel Detail(int Id);
    }
}
