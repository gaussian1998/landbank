﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingImpairmGroup.Models;
using Library.Servant.Servant.Common.Models;
using Library.Entity.Edmx;
using Library.Common.Models;

namespace Library.Servant.Servant.AccountingImpairmGroup
{
    public class LocalAccountingImpairmGroupServant : IAccountingImpairmGroupServant
    {
        public MessageModel BatchDelete(BatchDeleteModel BatchDelete)
        {
            MessageModel message = new MessageModel { IsSuccess = true };
            foreach (int id in BatchDelete.IDList)
            {
                AS_Impairment_Group_Records.Delete(x => x.ID == id);
            }
            return message;
        }

        public MessageModel Create(DetailModel Create)
        {
            MessageModel message = new MessageModel { IsSuccess = true };
            AS_Impairment_Group_Records.Create(new AS_Impairment_Group
            {
                Impairm_Group_No = Create.ImpairmGroupNo,
                Asset_Category_Code = Create.AssetCategoryCode,
                Use_Type = Create.UseType,
                Asset_Number = Create.AssetNumber,
                Account = Create.Account
            });
            return message;
        }

        public DetailModel Detail(int Id)
        {
            DetailModel result = AS_Impairment_Group_Records.First(x => x.ID == Id, m => new DetailModel {
                ID = m.ID,
                ImpairmGroupNo = (int)(m.Impairm_Group_No ?? 0),
                AssetCategoryCode = m.Asset_Category_Code,
                UseType = m.Use_Type,
                AssetNumber = m.Asset_Number,
                Account = m.Account
            });
            result.AccountName = AS_GL_Account_Records.First(x => x.Account == result.Account).Account_Name;
            return result;
        }

        public IndexResult Index(SearchModel Search)
        {
            bool skipFromNo = false;
            bool skipToNo = false;
            bool skipFromSubject = false;
            bool skipToSubject = false;
            bool skipUseType = false;

            if (Search.FromNo == 0)
            {
                skipFromNo = true;
            }
            if (Search.ToNo == 0)
            {
                skipToNo = true;
            }
            if (String.IsNullOrEmpty(Search.FromSubject))
            {
                skipFromSubject = true;
            }
            if(String.IsNullOrEmpty(Search.ToSubject))
            {
                skipToSubject = true;
            }

            //build asset category
            string assetCate = Search.MainKind;
            if (!String.IsNullOrEmpty(Search.DetailKind))
            {
                assetCate += "." + Search.DetailKind;
            }


            if (String.IsNullOrEmpty(Search.UseType))
            {
                skipUseType = true;
            }

            IndexResult result = new IndexResult
            {
                Conditions = Search,
                Results = new PageList<DetailModel>()
            };

            result.Results = AS_Impairment_Group_Records.Page(
                Search.CurrentPage, 
                Search.PageSize, 
                x => x.Impairm_Group_No, 
                x => (skipFromNo || x.Impairm_Group_No >= Search.FromNo) &&
                     (skipToNo || x.Impairm_Group_No <= Search.ToNo) &&
                     (skipFromSubject || x.Account.CompareTo(Search.FromSubject) >=0) &&
                     (skipToSubject || x.Account.CompareTo(Search.ToSubject) <= 0) &&
                     (skipUseType || x.Use_Type == Search.UseType) &&
                     (x.Asset_Category_Code.Contains(assetCate)),
                m => new DetailModel {
                    ID = m.ID,
                    ImpairmGroupNo = (int)(m.Impairm_Group_No ?? 0),
                    AssetCategoryCode = m.Asset_Category_Code,
                    UseType = m.Use_Type,
                    Account = m.Account,
                    AssetNumber = m.Asset_Number
                });

            return result;
        }

        public MessageModel Update(DetailModel Update)
        {
            MessageModel message = new MessageModel { IsSuccess = true };
            AS_Impairment_Group_Records.Update(x => x.ID == Update.ID, m => {
                m.Impairm_Group_No = Update.ImpairmGroupNo;
                m.Asset_Category_Code = Update.AssetCategoryCode;
                m.Use_Type = Update.UseType;
                m.Asset_Number = Update.AssetNumber;
                m.Account = Update.Account;
            });
            return message;
        }
    }
}
