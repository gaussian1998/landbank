﻿using Library.Servant.Servant.DocName.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace Library.Servant.Servant.DocName
{
    public interface IDocNameServant
    {
        NewResult New();
        IndexResult Index(SearchModel search);
        DetailsResult Details(int ID);
        VoidResult Create(CreateModel vo);
        VoidResult Update(UpdateModel vo);
        VoidResult Delete(int ID);
    }
}
