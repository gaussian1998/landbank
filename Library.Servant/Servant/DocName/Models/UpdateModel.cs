﻿using System;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.DocName.Models
{
    public class UpdateModel : InvasionEncryption
    {
        public int ID { get; set; }
        public string DocType { get; set; }
        public string DocCode { get; set; }
        public string DocName { get; set; }
        public bool IsActive { get; set; }
        public bool CancelCode { get; set; }
        public string Remark { get; set; }
        public Nullable<System.DateTime> LastUpdatedTime { get; set; }
        public Nullable<decimal> LastUpdatedBy { get; set; }
      }
}
