﻿using Library.Interface;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.DocName.Models
{
    public class CreateModel : InvasionEncryption
    {
        public string DocCode { get; set; }
        public string DocType { get; set; }
        public string DocName { get; set; }
        public bool IsActive { get; set; }
        public bool CancelCode { get; set; }
        public string Remark { get; set; }
    }
}
