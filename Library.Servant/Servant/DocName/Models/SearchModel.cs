﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.DocName.Models
{
    public class SearchModel : AbstractEncryptionDTO
    {
        public string DocCode { get; set; }
        public string DocType { get; set; }
        public bool? IsActive { get; set; }
        public bool? CancelCode { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
