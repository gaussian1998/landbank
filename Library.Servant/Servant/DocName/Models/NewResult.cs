﻿using System.Collections.Generic;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.DocName.Models
{
    public class NewResult : AbstractEncryptionDTO
    {
        public bool IsActive { get; set; }
        public bool CancelCode { get; set; }
        public List<IDSelectedModel> DocTypeOptions { get; set; }
        
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }

}
