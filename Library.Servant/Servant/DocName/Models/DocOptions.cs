﻿using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace Library.Servant.Servant.Models
{
    public class DocOptions
    {
        public List<OptionModel<int, string>> Docs { get; set; }
    }
}
