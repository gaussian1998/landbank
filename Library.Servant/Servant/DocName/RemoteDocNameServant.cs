﻿using Library.Servant.Servant.DocName.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.DocName
{
    public class RemoteDocNameServant : IDocNameServant
    {
        public NewResult New()
        {
            return RemoteServant.Post<VoidModel, NewResult>(

                        new VoidModel { },
                        "/DocNameServant/New"
            );
        }

        public IndexResult Index(SearchModel search)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(

                        search,
                        "/DocNameServant/Index"
            );
        }

        public VoidResult Create(CreateModel vo)
        {
            return RemoteServant.Post<CreateModel, VoidResult>(

                        vo,
                        "/DocNameServant/Create"
            );
        }

        public VoidResult Delete(int ID)
        {
            return RemoteServant.Post<IntModel, VoidResult>(

                        new IntModel { Value = ID },
                        "/DocNameServant/Delete"
            );
        }

        public DetailsResult Details(int ID)
        {
            return RemoteServant.Post<IntModel, DetailsResult>(

                        new IntModel { Value = ID },
                        "/DocNameServant/Details"
            );
        }

        public VoidResult Update(UpdateModel vo)
        {
            return RemoteServant.Post<UpdateModel, VoidResult>(

                        vo,
                        "/DocNameServant/Update"
            );
        }    
    }
}
