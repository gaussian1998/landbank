﻿using Library.Entity.Edmx;
using Library.Servant.Servant.CodeTable;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.DocName.Models;
using Library.Servant.Servant.Models;

namespace Library.Servant.Servant.DocName
{
    public class LocalDocNameServant : IDocNameServant
    {
        public static DocOptions Options()
        {
            var result = AS_Doc_Name_Records.Find( m => m.AS_Agent_Flow.Count != 0, entity => new OptionModel<int, string> {

                Value = entity.ID,
                Text = entity.Doc_Name
            });

            return new DocOptions { Docs = result };
        }

        public NewResult New()
        {
            return new NewResult
            {
                IsActive = true,
                CancelCode = false,
                DocTypeOptions = StaticDataServant.getIDOptions("DocType"),
            };
        }

        public IndexResult Index(SearchModel search)
        {
            bool skipDocCode = string.IsNullOrWhiteSpace(search.DocCode);
            bool skipDocType = string.IsNullOrWhiteSpace(search.DocType);
            bool skipIsActive = !search.IsActive.HasValue;
            bool skipCancelCode = !search.CancelCode.HasValue;

            return new IndexResult
            {
                Page = AS_Doc_Name_Records.Page( 
                    search.Page, 
                    search.PageSize, 
                    m => m.ID,
                    m => (skipDocCode || search.DocCode == m.Doc_No) &&
                           (skipDocType || search.DocType == m.Doc_Type) &&
                           (skipIsActive || search.IsActive == m.Is_Active) &&
                           (skipCancelCode || search.CancelCode == m.Cancel_Code),

                     m => new DetailsResult {

                         ID = m.ID,
                         DocCode = m.Doc_No,
                         DocName = m.Doc_Name,
                         DocType = m.Doc_Type,
                         IsActive = m.Is_Active,
                         CancelCode = m.Cancel_Code,
                         LastUpdatedBy = m.Last_Updated_By,
                         LastUpdatedTime = m.Last_Updated_Time,
                         Remark = m.Remark
                     }
                )
            };
        }

        public DetailsResult Details(int ID)
        {
            return AS_Doc_Name_Records.First( m => m.ID == ID, m => new DetailsResult
            {
                ID = m.ID,
                DocCode = m.Doc_No,
                DocName = m.Doc_Name,
                DocType = m.Doc_Type,
                IsActive = m.Is_Active,
                CancelCode = m.Cancel_Code,
                LastUpdatedBy = m.Last_Updated_By,
                LastUpdatedTime = m.Last_Updated_Time,
                Remark = m.Remark
            });
        }

        public VoidResult Create(CreateModel vo)
        {
            AS_Doc_Name_Records.Create( entity => {

                entity.Doc_No = vo.DocCode;
                entity.Doc_Type = vo.DocType;
                entity.Doc_Type_Name = LocalCodeTableServant.DocTypeText(vo.DocType);
                entity.Doc_Name = vo.DocName;
                entity.Cancel_Code = vo.CancelCode;
                entity.Is_Active = vo.IsActive;
                entity.Remark = vo.Remark;
            } );
            return new VoidResult();
        }

        public VoidResult Update(UpdateModel vo)
        {
            AS_Doc_Name_Records.Update( entity => entity.ID == vo.ID, entity => {

                entity.Doc_No = vo.DocCode;
                entity.Doc_Name = vo.DocName;
                entity.Doc_Type = vo.DocType;
                entity.Doc_Type_Name = LocalCodeTableServant.DocTypeText(vo.DocType);
                entity.Is_Active = vo.IsActive;
                entity.Cancel_Code = vo.CancelCode;
                entity.Remark = vo.Remark;
                entity.Last_Updated_By = vo.LastUpdatedBy;
                entity.Last_Updated_Time = vo.LastUpdatedTime;
            });
            return new VoidResult();
        }

        public VoidResult Delete(int ID)
        {
            AS_Doc_Name_Records.DeleteFirst(m => m.ID == ID);
            return new VoidResult();
        }

    }
}
