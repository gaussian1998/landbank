﻿using Library.Entity.Edmx;
using Library.Servant.Communicate;
using System.Collections.Generic;

namespace Library.Servant.Servant.AccountingTradeTodayStatistics.Models
{
    public class IndexResult : InvasionEncryption
    {
        public List<FR_Accounting_Report> Items { get; set; }
        public List<FR_Accounting_Report> GroupsMoney()
        {
           return Items.GroupMoney(item => new { item.FR_Branch, item.FR_Account });
        }
    }
}
