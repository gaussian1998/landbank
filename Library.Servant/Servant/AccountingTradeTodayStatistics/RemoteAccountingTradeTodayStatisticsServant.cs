﻿using Library.Servant.Servant.AccountingTradeTodayStatistics.Models;
using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.AccountingTradeTodayStatistics
{
    public class RemoteAccountingTradeTodayStatisticsServant : IAccountingTradeTodayStatisticsServant
    {
        public BranchDepartmentOptions Options()
        {
            return RemoteServant.Post<VoidModel, BranchDepartmentOptions>(

                            new VoidModel { },
                            "/AccountingTradeTodayStatisticsServant/Options"
           );
        }

        public IndexResult Index(BranchSearchModel model)
        {
            return RemoteServant.Post<BranchSearchModel, IndexResult>(

                            model,
                            "/AccountingTradeTodayStatisticsServant/Index"
            );
        }
    }
}
