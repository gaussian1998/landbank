﻿using Library.Servant.Servant.AccountingTradeTodayStatistics.Models;
using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Models;


namespace Library.Servant.Servant.AccountingTradeTodayStatistics
{
    public interface IAccountingTradeTodayStatisticsServant
    {
        BranchDepartmentOptions Options();
        IndexResult Index(BranchSearchModel model);
    }
}
