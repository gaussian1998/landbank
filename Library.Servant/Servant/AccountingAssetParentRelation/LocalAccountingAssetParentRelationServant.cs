﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingAssetParentRelation.Models;
using Library.Common.Models;
using System.Data.SqlClient;
using Dapper;
using System.Configuration;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.AccountingAssetParentRelation
{
    public class LocalAccountingAssetParentRelationServant : IAccountingAssetParentRelationServant
    {
        public IndexResult Index(SearchModel Search)
        {
            IndexResult result = new IndexResult {
                Conditions = Search,
                Results = new PageList<DetailModel> {
                    TotalAmount = 0,
                    Items = new List<DetailModel>()
                }
            };
            string countSql = "select count(1) from AS_VW_Asset_Family where 0=0 {0}{1}";
            string querySql = "select Layer, Family_Type as FamilyType, Category_Code as AssetCode, Asset_Number as AssetNumber," +
                              " description as AssetName, 1 as DerpnMethod, Life_Years as LifeYears, Salvage_Value as SalvageValue, Not_Deprn_Flag as NotDeprnFlag" +
                              " from AS_VW_Asset_Family where (0 = 0) {0}{1} order by Asset_Number offset {2} rows fetch next {3} rows only;";

            string relationCondition = "";
            string codeCondition = "";
            if (Search.RelationType == 1)
            {
                relationCondition = " and Layer = 1";
            }
            else if (Search.RelationType == 2)
            {
                relationCondition = " and Layer = 2";
            }

            if (!String.IsNullOrEmpty(Search.AssetNumber))
            {
                codeCondition = " and Asset_Number like @p + '%'";
            }
            else
            {
                Search.AssetNumber = "";
            }

            int offset = (Search.CurrentPage - 1) * Search.PageSize;
            countSql = String.Format(countSql, relationCondition, codeCondition);
            querySql = String.Format(querySql, relationCondition, codeCondition, offset, Search.PageSize);

            using (var context = new AS_LandBankEntities())
            {
                var totalAmount = context.Database.SqlQuery<int>(countSql, new SqlParameter("p", Search.AssetNumber)).First();
                List<DetailModel> page = context.Database.SqlQuery<DetailModel>(querySql, new SqlParameter("p", Search.AssetNumber)).ToList();

                result.Results.Items = page;
                result.Results.TotalAmount = Convert.ToInt32(totalAmount);
            }
            
            return result;
        }
    }
}
