﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AccountingAssetParentRelation.Models
{
    public class SearchModel : InvasionEncryption
    {
        /// <summary>
        /// 0:all, 1:parent only; 2:child only
        /// </summary>
        public int RelationType { get; set; }
        public string AssetNumber { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
    }
}
