﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AccountingAssetParentRelation.Models
{
    public class DetailModel : InvasionEncryption
    {
        /// <summary>
        /// 1 for parent, 2 for child
        /// </summary>
        public int Layer { get; set; }
        /// <summary>
        /// 1 : for 母子資產; 2 for 重大組成
        /// </summary>
        public int FamilyType { get; set; }
        public string AssetCode { get; set; }
        public string AssetNumber { get; set; }
        public string AssetName { get; set; }
        public int DerpnMethod { get; set; }
        public decimal LifeYears { get; set; }
        public decimal SalvageValue { get; set; }
        public int NotDeprnFlag { get; set; }

    }
}
