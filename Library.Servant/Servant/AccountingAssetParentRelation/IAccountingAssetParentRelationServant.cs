﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common.Models;
using Library.Common.Models;
using Library.Servant.Servant.AccountingAssetParentRelation.Models;

namespace Library.Servant.Servant.AccountingAssetParentRelation
{
    public interface IAccountingAssetParentRelationServant
    {
        IndexResult Index(SearchModel Search);   
    }
}
