﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingAssetParentRelation;
using Library.Servant.Servant.AccountingAssetParentRelation.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.AccountingAssetParentRelation
{
    public class RemoteAccountingAssetParentRelationServant : IAccountingAssetParentRelationServant
    {
        public IndexResult Index(SearchModel Search)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(
                            Search,
                            "/AccountingAssetParentRelationServant/Index"
           );
        }
    }
}
