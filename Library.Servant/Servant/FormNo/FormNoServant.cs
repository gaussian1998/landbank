﻿using Library.Entity.Edmx;
using System;
using System.Globalization;

namespace Library.Servant.Servant.FormNo
{
    public static class FormNoServant
    {
        public static string Generate(string Tag)
        {
            return Date() + Tag + Serial();
        }

        private static string Date()
        {
            var now = DateTime.Now;
            var calendar = new TaiwanCalendar();

            return string.Format("{0:000}{1:00}{2:00}", calendar.GetYear(now), calendar.GetMonth(now), calendar.GetDayOfMonth(now));
        }

        private static string Serial()
        {
            long serial = AS_Entropy_Records.Create(e => { }).ID % 1000000;

            return string.Format( "{0:000000}", serial);
        }
    }
}
