﻿using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace Library.Servant.Servant.CodeTable.Models
{
    public class BranchDepartmentOptions : InvasionEncryption
    {
        public List<OptionModel<string, string>> BranchOptions { get; set; }
        public List<OptionModel<string, string>> DepartmentOptions { get; set; }
        public static BranchDepartmentOptions Query()
        {
            return new BranchDepartmentOptions
            {
                BranchOptions = LocalCodeTableServant.BranchOptions(),
                DepartmentOptions = LocalCodeTableServant.DepartmentOptions()
            };
        }
    }
}
