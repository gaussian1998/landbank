﻿using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;
using System.Linq;

namespace Library.Servant.Servant.CodeTable.Models
{
    public class TradeOptions
    {
        public List<OptionModel<string,string>> TradeKind { get; private set; }
        public List<OptionModel<string, string>> TradeDistinguish { get; private set; }
        public static TradeOptions Query()
        {
            return new TradeOptions
            {
                TradeKind = LocalCodeTableServant.TradeKindOptions(),
                TradeDistinguish = LocalCodeTableServant.TradeDistinguishOptions()
            };
        }

        public static TradeOptions QueryDeprn()
        {
            var deprnOptions = new string[] { "動產", "房屋", "租賃權益", "土地改良" };
            return new TradeOptions
            {
                TradeKind = LocalCodeTableServant.TradeKindOptions().Where( options => deprnOptions.Contains(options.Text)).ToList(),
                TradeDistinguish = LocalCodeTableServant.TradeDistinguishOptions()
            };
        }
    }
}
