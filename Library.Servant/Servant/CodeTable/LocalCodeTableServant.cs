﻿using Library.Common.Extension;
using Library.Entity.Edmx;
using Library.Servant.Servant.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Library.Servant.Servant.CodeTable
{
    /// <summary>
    ///  所有的常數都會放在AS_Code_Table中
    ///  下面如果發現例外,代表尚未整進Code_Table
    /// </summary>
    public class LocalCodeTableServant
    {
        public static List<OptionModel<string,string>> IsAliveOptions()
        {
            return CodeOptions("IsActive");
        }
        
        public static List<OptionModel<string, string>> CancelOptions()
        {
            return CodeOptions("CancelCode");
        }

        public static List<OptionModel<string, string>> BranchOptions()
        {
            return CodeOptions("C01");
        }

        public static Dictionary<string, string> BranchDictionary()
        {
            return CodeDictionary("C01");
        }

        public static List<OptionModel<string, string>> DepartmentOptions()
        {
            return CodeOptions("C02");
        }

        public static Dictionary<string, string> DepartmentDictionary()
        {
            return CodeDictionary("C02");
        }

        public static List<OptionModel<string, string>> TradeKindOptions()
        {
            return CodeOptions("V03");
        }

        public static List<OptionModel<string, string>> TradeDistinguishOptions()
        {
            return CodeOptions("V04");
        }

        public static List<OptionModel<string, string>> DocTypeOptions()
        {
            return CodeOptions("DocType");
        }

        public static Dictionary<string, string> DocTypeDictionary()
        {
            return CodeDictionary("DocType");
        }

        public static Dictionary<int, string> FlowRoleIdDictionary()
        {
            return IdDictionary("R01");
        }

        public static FlowSearchOptionsModel FlowSearchOptions(Action<FlowSearchOptionsModel> modify=null)
        {
            var result = new FlowSearchOptionsModel {

                StartTime = DateTime.Now.AddMonths(-2).ShortDescription(),
                EndTime = DateTime.Now.ShortDescription(),
                ApplyUserOptions = new List< OptionModel<int,string> >(),
                CurrentApprovalUserOptions = new List<OptionModel<int, string>>(),
                FlowStateOptions = new List<OptionModel<int, string>>(),
                BranchOptions = BranchOptions(),
                DepartmentOptions = DepartmentOptions(),
                DocTypeOptions = DocTypeOptions()
            };

            if (modify != null)
                modify(result);
            return result;
        }

        public static string BranchText(string CodeID)
        {
            return Text("C01", CodeID);
        }

        public static string DepartmentText(string CodeID)
        {
            return Text( "C02", CodeID);
        }

        public static string DocTypeText(string CodeID)
        {
            return Text("DocType", CodeID);
        }



        private static List<OptionModel<string, string>> CodeOptions(string Class)
        {
            return AS_Code_Table_Records.Find(m => m.Class == Class).Select(m => new OptionModel<string, string> { Value = m.Code_ID, Text = m.Text }).ToList();
        }

        private static Dictionary<string, string> CodeDictionary(string Class)
        {
            return AS_Code_Table_Records.Find(m => m.Class == Class).ToDictionary( m => m.Code_ID, m => m.Text );
        }

        private static Dictionary<int, string> IdDictionary(string Class)
        {
            return AS_Code_Table_Records.Find(m => m.Class == Class).ToDictionary(m => m.ID, m => m.Text);
        }

        private static string Text(string Class, string CodeID)
        {
            return AS_Code_Table_Records.First(m => m.Class == Class && m.Code_ID == CodeID).Text;
        }
    }
}
