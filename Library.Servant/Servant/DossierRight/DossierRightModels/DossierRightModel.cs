﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.DossierRight.DossierRightModels
{
   public class DossierRightModel : AbstractEncryptionDTO
    {
        public HeaderModel Header { get; set; }

        public IEnumerable<DossierRightRecordModel> Records { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
