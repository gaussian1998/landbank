﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.DossierRight.DossierRightModels
{
    public class SearchModel : AbstractEncryptionDTO
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }

        /// <summary>單據類型</summary>
        public string Transaction_Type { get; set; }

        /// <summary>來源類別</summary>
        public string Source { get; set; }

        /// <summary>事務處理編號(流程編號FLOW_ID) 啟</summary>
        public string Form_Number_Begin { get; set; }

        /// <summary>事務處理編號(流程編號FLOW_ID) 迄</summary>
        public string Form_Number_End { get; set; }

        /// <summary>管轄單位</summary>
        public string Office_Branch { get; set; }

        /// <summary>單據處理日期-起</summary>
        public DateTime? BeginDate { get; set; }

        /// <summary>單據處理日期-迄</summary>
        public DateTime? EndDate { get; set; }

        /// <summary>權狀/執照類別-L 土地所有權狀</summary>
        public bool Authorization_Category_L { get; set; }

        /// <summary>權狀/執照類別-B 建物所有權狀 </summary>
        public bool Authorization_Category_B { get; set; }

        /// <summary>財產編號</summary>
        public string Asset_Number { get; set; }

        /// <summary>狀態</summary>
        public string FlowStatus { get; set; }

        /// <summary>備註(關鍵字)</summary>
        public string Remark { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }

        /// <summary>表單流程代號(單據類型 + 來源類型)</summary>
        public string DocCode { get; set; }
    }
}
