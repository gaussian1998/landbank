﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.DossierRight.DossierRightModels
{
    public class SelectModel : AbstractEncryptionDTO
    {
        /// <summary>表單編號</summary>
        public string TRXHeaderID { get; set; }
        /// <summary>明細檔 ID</summary>
        public int RecordID { get; set; }
    }
}
