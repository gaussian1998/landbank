﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.DossierRight.DossierRightModels
{
    public class DossierRightHandleResult : AbstractEncryptionDTO
    {
        public string TRX_Header_ID { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
