﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.DossierRight.DossierRightModels
{
    public class RecordIndexModel : AbstractEncryptionDTO
    {
        public IEnumerable<DossierRightMainModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public RecordSearchModel condition { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
