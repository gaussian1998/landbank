﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.DossierRight.DossierRightModels
{
    public class RecordSearchModel : AbstractEncryptionDTO
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }

        /// <summary>權狀類別</summary>
        public string Authorization_Category { get; set; }

        /// <summary>財產編號</summary>
        public string Asset_Number { get; set; }

        /// <summary>地號</summary>
        public string Land_Number { get; set; }

        /// <summary>建號</summary>
        public string Build_Number { get; set; }

        /// <summary>權狀字號</summary>
        public string Authorization_Number { get; set; }


        public string City_Code { get; set; }
        public string District_Code { get; set; }
        public string Section_Code { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
