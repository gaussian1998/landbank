﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.DossierRight.DossierRightModels;
using Library.Servant.Common;
using Library.Entity.Edmx;
namespace Library.Servant.Servant.DossierRight
{
    class RemoteDossierRightServant : IDossierRightServant
    {
        #region 表頭檔相關
        public IndexModel Index(SearchModel condition)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, IndexModel>
            (
                condition,
                Config.ServantDomain + "/DossierRightServant/Index"
            );

            return Tools.ExceptionConvert(result);
        }
        public string CreateTRXID(string Type)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
             (
                  new SearchModel() { Transaction_Type = Type },
                  Config.ServantDomain + "/LandServant/CreateTRXID"
             );
            return Tools.ExceptionConvert(result).StrResult;
        }
        public DossierRightHandleResult CreateHeader(HeaderModel Header)
        {
            var result = ProtocolService.EncryptionPost<HeaderModel, DossierRightHandleResult>
            (
                Header,
                Config.ServantDomain + "/DossierRightServant/CreateHeader"
            );
            return Tools.ExceptionConvert(result);
        }
        public DossierRightModel GetDetail(string TRXHeaderID)
        {
            var result = ProtocolService.EncryptionPost<SelectModel, DossierRightModel>
          (
              new SelectModel() { TRXHeaderID = TRXHeaderID },
              Config.ServantDomain + "/DossierRightServant/GetDetail"
          );

            return Tools.ExceptionConvert(result);
        }
        public DossierRightHandleResult UpdateHeader(HeaderModel Header)
        {
            var result = ProtocolService.EncryptionPost<HeaderModel, DossierRightHandleResult>
            (
                Header,
                Config.ServantDomain + "/DossierRightServant/UpdateHeader"
            );
            return Tools.ExceptionConvert(result);
        }
        public HeaderModel GetHeaderData(string TRXHeaderID)
        {
            var result = ProtocolService.EncryptionPost<SelectModel, HeaderModel>
             (
                  new SelectModel() { TRXHeaderID = TRXHeaderID },
                  Config.ServantDomain + "/DossierRightServant/GetHeaderData"
             );
            return Tools.ExceptionConvert(result);
        }

        #endregion
        #region Record 明細檔
        public DossierRightRecordModel GetRecordDetail(int ID)
        {
            var result = ProtocolService.EncryptionPost<SelectModel, DossierRightRecordModel>
             (
                  new SelectModel() { RecordID = ID},
                  Config.ServantDomain + "/DossierRightServant/GetRecordDetail"
             );
            return Tools.ExceptionConvert(result);
        }
        public DossierRightHandleResult Create_Record(DossierRightModel model)
        {
            var result = ProtocolService.EncryptionPost<DossierRightModel, DossierRightHandleResult>
            (
                model,
                Config.ServantDomain + "/DossierRightServant/Create_Record"
            );
            return Tools.ExceptionConvert(result);
        }
        public DossierRightHandleResult Update_Record(DossierRightModel model)
        {
            var result = ProtocolService.EncryptionPost<DossierRightModel, DossierRightHandleResult>
            (
                model,
                Config.ServantDomain + "/DossierRightServant/Update_Record"
            );
            return Tools.ExceptionConvert(result);
        }
        public DossierRightHandleResult Delete_Record(DossierRightModel model)
        {
            var result = ProtocolService.EncryptionPost<DossierRightModel, DossierRightHandleResult>
            (
                model,
                Config.ServantDomain + "/DossierRightServant/Delete_Record"
            );
            return Tools.ExceptionConvert(result);
        }
        #endregion
        #region 代碼檔相關
        public IEnumerable<CodeItem> GetDistrict(string City)
        {
            var result = ProtocolService.EncryptionPost<CodeSearchModel, CodeItemModel>
             (
                  new CodeSearchModel() { City_Code = City },
                  Config.ServantDomain + "/DossierRightServant/GetDistrict"
             );
            return Tools.ExceptionConvert(result).Items;
        }

        public IEnumerable<CodeItem> GetSection(string City, string District)
        {
            var result = ProtocolService.EncryptionPost<CodeSearchModel, CodeItemModel>
             (
                  new CodeSearchModel() { City_Code = City, District_Code = District },
                  Config.ServantDomain + "/DossierRightServant/GetSection"
             );
            return Tools.ExceptionConvert(result).Items;
        }

        public IEnumerable<CodeItem> GetSubSection(string City, string District, string Section)
        {
            var result = ProtocolService.EncryptionPost<CodeSearchModel, CodeItemModel>
             (
                  new CodeSearchModel() { City_Code = City, District_Code = District, Section_Code = Section },
                  Config.ServantDomain + "/DossierRightServant/GetSubSection"
             );
            return Tools.ExceptionConvert(result).Items;
            throw new NotImplementedException();
        }
        public IEnumerable<CodeItem> GetCodeItem(string CodeClass)
        {
            var result = ProtocolService.EncryptionPost<CodeSearchModel, CodeItemModel>
             (
                  new CodeSearchModel() { CodeClass = CodeClass },
                  Config.ServantDomain + "/DossierRightServant/GetCodeItem"
             );
            return Tools.ExceptionConvert(result).Items;
        }
        #endregion
        #region 搜尋既有的權狀資料
        public RecordIndexModel SearchMainRecords(RecordSearchModel condition)
        {
            var result = ProtocolService.EncryptionPost<RecordSearchModel, RecordIndexModel>
             (
                  condition,
                  Config.ServantDomain + "/DossierRightServant/SearchAssets"
             );
            return Tools.ExceptionConvert(result);
        }
        public DossierRightHandleResult ImportFromRecord(DossierRightModel model)
        {
            var result = ProtocolService.EncryptionPost<DossierRightModel, DossierRightHandleResult>
            (
                model,
                Config.ServantDomain + "/DossierRightServant/ImportFromRecord"
            );
            return Tools.ExceptionConvert(result);
        }
        #endregion
    }
}
