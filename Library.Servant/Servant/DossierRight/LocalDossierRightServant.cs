﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.DossierRight.DossierRightModels;
using Library.Servant.Enums;
using Library.Entity.Edmx;
using System.Transactions;
using Library.Servant.Repository.DossierRight;

namespace Library.Servant.Servant.DossierRight
{
    class LocalDossierRightServant : IDossierRightServant
    {

        #region 表頭檔相關
        public IndexModel Index(SearchModel condition)
        {
            IndexModel result = new IndexModel();
            result.condition = condition;

            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_Right_Headers
                            join mc in
                            (
                               from mr in db.AS_Assets_Right_Headers
                               group mr by new { mr.Form_Number } into mrg
                               select new
                               {
                                   TRX_Header_ID = mrg.Key.Form_Number,
                                   Count = mrg.Count()
                               }
                            ) on mh.Form_Number equals mc.TRX_Header_ID into mhtemp
                            from mc in mhtemp.DefaultIfEmpty()
                            join office in db.AS_Keep_Position on mh.Office_Branch equals office.Keep_Position_Code into toffice
                            //join office in db.AS_Keep_Position.Where(p => p.Parent_ID.Value == 1) on mh.Office_Branch equals office.Keep_Dept into toffice

                            from office in toffice.DefaultIfEmpty()
                            join flow in
                            (
                               from ff in db.AS_Code_Table
                               where ff.Class == "P02"
                               select ff
                            ) on mh.Transaction_Status equals flow.Code_ID into tflow
                            from flow in tflow.DefaultIfEmpty()
                            select new { mh, mc, office, flow };

                if (!string.IsNullOrEmpty(condition.Transaction_Type))
                {
                    query = query.Where(m => m.mh.Transaction_Type == condition.Transaction_Type);
                }
                if (!string.IsNullOrEmpty(condition.Source))
                {
                    query = query.Where(m => m.mh.Source == condition.Source);
                }

                if (!string.IsNullOrEmpty(condition.Form_Number_Begin))
                {
                    int beginNumber = int.Parse(condition.Form_Number_Begin);

                    query = query.Where(m => int.Parse(m.mh.Form_Number.Substring(11)) >= beginNumber);
                }
                if (!string.IsNullOrEmpty(condition.Form_Number_End))
                {
                    int endNumber = int.Parse(condition.Form_Number_End);

                    query = query.Where(m => int.Parse(m.mh.Form_Number.Substring(11)) <= endNumber);
                }

                if (!string.IsNullOrEmpty(condition.FlowStatus))
                {
                    query = query.Where(m => m.mh.Transaction_Status == condition.FlowStatus);
                }

                if (condition.BeginDate != null)
                {
                    query = query.Where(m => m.mh.Transaction_Datetime_Entered >= condition.BeginDate);
                }

                if (condition.EndDate != null)
                {
                    query = query.Where(m => m.mh.Transaction_Datetime_Entered <= condition.EndDate);
                }

                if (!string.IsNullOrEmpty(condition.Office_Branch))
                {
                    query = query.Where(m => m.mh.Office_Branch == condition.Office_Branch);
                }

                if (!string.IsNullOrEmpty(condition.Remark))
                {
                    query = query.Where(m => m.mh.Description == condition.Remark);
                }

                result.TotalAmount = query.Count();
                //分頁
                if (condition.Page != null)
                {
                    query = query.OrderByDescending(m => m.mh.Create_Time).Skip((condition.Page.Value - 1) * Convert.ToInt32(condition.PageSize)).Take(Convert.ToInt32(condition.PageSize));
                }

                result.Items = query.ToList().Select(o => new HeaderModel()
                {
                    ID = o.mh.ID,
                    Form_Number = o.mh.Form_Number,
                    Transaction_Type = o.mh.Transaction_Type,
                    Transaction_Status = o.mh.Transaction_Status,
                    Transaction_Datetime_Entered = o.mh.Transaction_Datetime_Entered,
                    Office_Branch_Code = o.mh.Office_Branch,
                    Description = o.mh.Description,
                    Create_Time = o.mh.Create_Time,
                    Created_By = o.mh.Created_By,
                    Last_Updated_Time = o.mh.Last_Updated_Time,
                    Last_Updated_By = o.mh.Last_Updated_By,
                    Office_Branch_Code_Name = (o.office != null) ? o.office.Keep_Position_Name : "",
                    Transaction_StatusName = (o.flow != null) ? o.flow.Text : "",
                    Source = o.mh.Source,
                    SourceName = Library.Servant.Repository.DossierRight.DossierRightGeneral.GetSourceName(o.mh.Source),
                    SerialNo = "",
                    Transaction_TypeName = DossierRightGeneral.GetFunctionNameByCode(o.mh.Transaction_Type)
                });
            }

            return result;
        }

        public string CreateTRXID(string Type)
        {
            string CToDay = (DateTime.Today.Year - 1911).ToString().PadLeft(3, '0') + DateTime.Today.ToString("MMdd") + Type.PadRight(4, '0');//原編號為B110, 補滿四碼
            string TRXID = "";

            using (var db = new AS_LandBankEntities())
            {
                TRXID = db.AS_Assets_Right_Headers.Where(o => o.Transaction_Type == Type && o.Form_Number.StartsWith(CToDay)).Max(o => o.Form_Number);
            }
            if (TRXID != null)
            {
                int Seq = Convert.ToInt32(TRXID.Substring(11)) + 1;
                TRXID = CToDay + Seq.ToString().PadLeft(6, '0');
            }
            else
            {
                TRXID = CToDay + "000001";
            }

            return TRXID;
        }
        public DossierRightHandleResult CreateHeader(HeaderModel Header)
        {
            DossierRightHandleResult Result = new DossierRightHandleResult()
            {
                TRX_Header_ID = Header.Form_Number
            };

            using (var db = new AS_LandBankEntities())
            {
                //Header
                //Insert
                db.Entry(DossierRightGeneral.To_Header(Header)).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();
            }

            return Result;
        }
        public HeaderModel GetHeaderData(string TRXHeaderID)
        {
            HeaderModel result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from h in db.AS_Assets_Right_Headers
                            where h.Form_Number == TRXHeaderID
                            select h;

                if (query != null)
                {
                    result = query.ToList().Select(o => DossierRightGeneral.ToHeaderModel(o)).FirstOrDefault();
                }
            }
            return result;
        }
        public DossierRightModel GetDetail(string TRXHeaderID)
        {
            DossierRightModel result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in
                            (
                                from h in db.AS_Assets_Right_Headers
                                where h.Form_Number == TRXHeaderID
                                select h
                            )
                            join mas in db.AS_Assets_Right_Record on mh.Form_Number equals mas.AS_Assets_Right_Header_ID into temp
                            from mas in temp.DefaultIfEmpty()
                            select new { mh, mas};

                if (query != null && query.Count() > 0)
                {
                    var queryList = query.ToList();
                    DossierRightModel lmode = new DossierRightModel();
                    lmode.Header = queryList.Select(o => DossierRightGeneral.ToHeaderModel(o.mh)).First();
                    
                    lmode.Records = queryList.Select(o => o.mas != null ? DossierRightGeneral.ToRecordModel(o.mas): null);
                    result = lmode;
                }
            }

            return result;
        }

        public DossierRightHandleResult UpdateHeader(HeaderModel Header)
        {
            DossierRightHandleResult Result = new DossierRightHandleResult()
            {
                TRX_Header_ID = Header.Form_Number
            };

            using (var db = new AS_LandBankEntities())
            {
                //Header

                //Update

                var db_header = db.AS_Assets_Right_Headers.First(h => h.Form_Number == Header.Form_Number);
                db_header.Office_Branch = Header.Office_Branch_Code;
                db_header.Source = Header.Source;
                //db_header.Transaction_Datetime_Entered = Header.Transaction_Datetime_Entered;
                db_header.Description = Header.Description;
                db_header.Last_Updated_By = Header.Last_Updated_By;
                db_header.Last_Updated_Time = DateTime.Now;
                db.Entry(db_header).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }

            return Result;
        }
        #endregion
        #region Record 明細檔

        public DossierRightRecordModel GetRecordDetail(int ID)
        {
            DossierRightRecordModel record = null;
            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_Right_Record db_record = (from a in db.AS_Assets_Right_Record
                                                    where a.ID == ID
                                                   select a).FirstOrDefault();

                if (db_record != null)
                {
                    record = DossierRightGeneral.ToRecordModel(db_record);
                }
            }

            return record;
        }

        public DossierRightHandleResult Create_Record(DossierRightModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    //Header
                    AS_Assets_Right_Headers Header = DossierRightGeneral.To_Header(model.Header);
                    foreach (DossierRightRecordModel record in model.Records)
                    {
                        AS_Assets_Right_Record Record = DossierRightGeneral.To_Record(Header.Form_Number, record);
                        db.Entry(Record).State = System.Data.Entity.EntityState.Added;
                        db.SaveChanges();
                    }
                }

                trans.Complete();
            }
            return new DossierRightHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }
        public DossierRightHandleResult Update_Record(DossierRightModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (var record in model.Records)
                    {
                        AS_Assets_Right_Record Record = db.AS_Assets_Right_Record.FirstOrDefault(m => m.ID == record.ID);
                       
                        Record.Authorization_Category = record.Authorization_Category;
                        Record.Asset_Number = record.Asset_Number;
                        Record.City_Code = !string.IsNullOrEmpty(record.City_Code) ? record.City_Code : "";
                        Record.City_Name = !string.IsNullOrEmpty(record.City_Name) ? record.City_Name : "";
                        Record.District_Code = !string.IsNullOrEmpty(record.District_Code) ? record.District_Code : "";
                        Record.District_Name = !string.IsNullOrEmpty(record.District_Name) ? record.District_Name : "";
                        Record.Section_Code = !string.IsNullOrEmpty(record.Section_Code) ? record.Section_Code : "";
                        Record.Section_Name = !string.IsNullOrEmpty(record.Section_Name) ? record.Section_Name : "";
                        Record.Sub_Section_Name = !string.IsNullOrEmpty(record.Sub_Section_Name) ? record.Sub_Section_Name : "";
                        Record.Parent_Land_Number = !string.IsNullOrEmpty(record.Parent_Land_Number) ? record.Parent_Land_Number : "";
                        Record.Filial_Land_Number = !string.IsNullOrEmpty(record.Filial_Land_Number) ? record.Filial_Land_Number : "";
                        Record.Authorization_Number = !string.IsNullOrEmpty(record.Authorization_Number) ? record.Authorization_Number : "";
                        Record.Parent_Land_Number = !string.IsNullOrEmpty(record.Parent_Land_Number) ? record.Parent_Land_Number : "";
                        Record.Filial_Land_Number = !string.IsNullOrEmpty(record.Filial_Land_Number) ? record.Filial_Land_Number : "";
                        Record.Build_Number = !string.IsNullOrEmpty(record.Build_Number) ? record.Build_Number : "";
                        Record.Build_Address = !string.IsNullOrEmpty(record.Build_Address) ? record.Build_Address : ""; 
                        Record.Building_STRU = !string.IsNullOrEmpty(record.Building_STRU) ? record.Building_STRU : ""; 
                        Record.Used_Type = !string.IsNullOrEmpty(record.Used_Type) ? record.Used_Type : "";
                        Record.Building_Total_Floor = record.Building_Total_Floor.HasValue? record.Building_Total_Floor:0;
                        Record.Registration_Date = record.Registration_Date;
                        Record.RightBook_Date = record.RightBook_Date;
                        Record.Authorization_Number = record.Authorization_Number;
                        Record.Right_UploadFile = !string.IsNullOrEmpty(record.Right_UploadFile) ? record.Right_UploadFile : ""; 
                        Record.Last_Updated_Time = DateTime.Now;
                        Record.Last_Updated_By = record.Last_Updated_By;
                        db.Entry(Record).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                trans.Complete();
            }
            return new DossierRightHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }
        public DossierRightHandleResult Delete_Record(DossierRightModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (var record in model.Records)
                    {
                        AS_Assets_Right_Record Record = db.AS_Assets_Right_Record.FirstOrDefault(m => m.ID == record.ID);

                        db.Entry(Record).State = System.Data.Entity.EntityState.Deleted;
                        db.SaveChanges();
                    }
                }

                trans.Complete();
            }
            return new DossierRightHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }
        #endregion

        #region 搜尋既有的權狀資料
        public RecordIndexModel SearchMainRecords(RecordSearchModel condition)
        {
            RecordIndexModel result = new RecordIndexModel();
            result.condition = condition;

            using (var db = new AS_LandBankEntities())
            {
                var query = from m in db.AS_Assets_Right
                            join citycode in
                           (
                              from cc in db.AS_VW_Code_Table
                              where cc.Class == "City"
                              select cc
                           ) on m.City_Code equals citycode.Code_ID into tcitycode
                            from citycode in tcitycode.DefaultIfEmpty()
                            join districtcode in
                            (
                               from dd in db.AS_Land_Code
                               select new { dd.District_Code, dd.District_Name, dd.City_Code }
                            ).Distinct() on new { m.City_Code, m.District_Code } equals new { districtcode.City_Code, districtcode.District_Code } into tdistrictcode
                            from districtcode in tdistrictcode.DefaultIfEmpty()
                            join sectioncode in
                            (
                               from dd in db.AS_Land_Code
                               select new { dd.District_Code, dd.District_Name, dd.City_Code, dd.Section_Code, dd.Sectioni_Name }
                            ).Distinct() on new { m.City_Code, m.District_Code, m.Section_Code } equals new { sectioncode.City_Code, sectioncode.District_Code, sectioncode.Section_Code } into tsectioncode
                            from sectioncode in tsectioncode.DefaultIfEmpty()
                            select new { m, citycode, districtcode, sectioncode };

                if (!string.IsNullOrEmpty(condition.Asset_Number))
                {
                    query = query.Where(o => o.m.Asset_Number.Contains(condition.Asset_Number));
                }
                if (!string.IsNullOrEmpty(condition.Authorization_Category))
                {
                    query = query.Where(o => o.m.Authorization_Category == condition.Authorization_Category);
                }
                if (!string.IsNullOrEmpty(condition.Land_Number))
                {
                    query = query.Where(o => (o.m.Parent_Land_Number + "-" + o.m.Filial_Land_Number).Contains(condition.Land_Number));
                }
                if (!string.IsNullOrEmpty(condition.Build_Number))
                {
                    query = query.Where(o => o.m.Build_Number.Contains(condition.Build_Number));
                }
                if (!string.IsNullOrEmpty(condition.Authorization_Number))
                {
                    query = query.Where(o => o.m.Authorization_Number.Contains(condition.Authorization_Number));
                }
                if (!string.IsNullOrEmpty(condition.City_Code))
                {
                    query = query.Where(o => o.m.City_Code == condition.City_Code);
                }
                if (!string.IsNullOrEmpty(condition.District_Code))
                {
                    query = query.Where(o => o.m.District_Code == condition.District_Code);
                }
                if (!string.IsNullOrEmpty(condition.Section_Code))
                {
                    query = query.Where(o => o.m.Section_Code == condition.Section_Code);
                }
                result.TotalAmount = query.Count();

                //分頁
                if (condition.Page != null)
                {
                    query = query.OrderByDescending(o => o.m.Create_Time).Skip((condition.Page.Value - 1) * Convert.ToInt32(condition.PageSize)).Take(Convert.ToInt32(condition.PageSize));
                }

                result.Items = (query.Count() > 0) ? query.ToList().Select(o => new DossierRightMainModel()
                {
                    ID = o.m.ID,
                    Authorization_Category= o.m.Authorization_Category,
                    Asset_Number = o.m.Asset_Number,
                    City_Code = o.m.City_Code,
                    City_Name = (o.citycode != null) ? o.citycode.Text : "",
                    District_Code = o.m.District_Code,
                    District_Name = (o.districtcode != null) ? o.districtcode.District_Name : "",
                    Section_Code = o.m.Section_Code,
                    Section_Name = (o.sectioncode != null) ? o.sectioncode.Sectioni_Name : "",
                    Sub_Section_Name = o.m.Sub_Section_Name,
                    Parent_Land_Number = o.m.Parent_Land_Number,
                    Filial_Land_Number = o.m.Filial_Land_Number,
                    Build_Number = o.m.Build_Number,
                    Build_Address = o.m.Build_Address,
                    Building_STRU = o.m.Building_STRU,
                    Used_Type = o.m.Used_Type,
                    Building_Total_Floor = o.m.Building_Total_Floor,
                    Registration_Date = o.m.Registration_Date,
                    RightBook_Date = o.m.RightBook_Date,
                    Authorization_Number = o.m.Authorization_Number,
                    Create_Time = o.m.Create_Time,
                    Created_By = o.m.Created_By,
                    Last_Updated_Time = o.m.Last_Updated_Time,
                    Last_Updated_By = o.m.Last_Updated_By
                }) : new List<DossierRightMainModel>();

            }
            return result;
        }

        public DossierRightHandleResult ImportFromRecord(DossierRightModel dossierRightModel)
        {
            DossierRightModel AddDossierRightModel = new DossierRightModel()
            {
                Header = dossierRightModel.Header
            };
            if (dossierRightModel.Records  != null && dossierRightModel.Records.Any(p => p != null))
            {
                AddDossierRightModel.Records = dossierRightModel.Records.Select(o => GetMainRightByAssetNumber(o.Asset_Number));
            }
            return Create_Record(AddDossierRightModel);
        }

        private DossierRightRecordModel GetMainRightByAssetNumber(string AssetNumber)
        {
            DossierRightRecordModel mainModel = null;

            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_Right AS_Right = (from a in db.AS_Assets_Right
                                             where a.Asset_Number == AssetNumber
                                          select a).FirstOrDefault();
                if (AS_Right != null)
                {
                    mainModel = DossierRightGeneral.MainModelToRecordModel(AS_Right);
                }
            }

            return mainModel;
        }
        #endregion
        #region 代碼檔相關
        public IEnumerable<CodeItem> GetDistrict(string City)
        {
            IEnumerable<CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                result = (from m in db.AS_Land_Code
                          where m.City_Code == City
                          select new CodeItem()
                          {
                              Value = m.District_Code,
                              Text = m.District_Name
                          }).Distinct().ToList();
            }
            return result;
        }
        public IEnumerable<CodeItem> GetSection(string City, string District)
        {
            IEnumerable<CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                result = (from m in db.AS_Land_Code
                          where m.City_Code == City && m.District_Code == District
                          select new CodeItem()
                          {
                              Value = m.Section_Code,
                              Text = m.Sectioni_Name
                          }).Distinct().ToList();
            }
            return result;
        }

        public IEnumerable<CodeItem> GetSubSection(string City, string District, string Section)
        {
            IEnumerable<CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                result = (from m in db.AS_Land_Code
                          where m.City_Code == City && m.District_Code == District && m.Section_Code == Section
                          select new CodeItem()
                          {
                              Value = m.Sub_Sectioni_Name,
                              Text = m.Sub_Sectioni_Name
                          }).Distinct().ToList();
            }
            return result;
        }
        public IEnumerable<CodeItem> GetCodeItem(string CodeClass)
        {
            IEnumerable<CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                result = (from m in db.AS_VW_Code_Table
                          where m.Class == CodeClass
                          select new CodeItem()
                          {
                              Value = m.Code_ID,
                              Text = m.Text
                          }).ToList().OrderBy(o => o.Value);
            }
            return result;
        }

        #endregion
    }
}
