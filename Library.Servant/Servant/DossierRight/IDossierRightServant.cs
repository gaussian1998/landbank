﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.DossierRight.DossierRightModels;

namespace Library.Servant.Servant.DossierRight
{
    public interface IDossierRightServant
    {
        #region 主檔相關
        IndexModel Index(SearchModel condition);

        DossierRightModel GetDetail(string TRXHeaderID);

        string CreateTRXID(string Type);

        DossierRightHandleResult CreateHeader(HeaderModel Header);

        DossierRightHandleResult UpdateHeader(HeaderModel Header);


        HeaderModel GetHeaderData(string TRXHeaderID);
        //bool GetDocID(string DocCode, out int DocID);

        //bool UpdateFlowStatus(string TRX_Header_ID, string FlowStatus);
        #endregion
        #region Record 明細檔
        DossierRightRecordModel GetRecordDetail(int ID);
        DossierRightHandleResult Create_Record(DossierRightModel model);
        DossierRightHandleResult Update_Record(DossierRightModel model);
        DossierRightHandleResult Delete_Record(DossierRightModel model);
        #endregion
        #region 取得代碼的資料
        IEnumerable<CodeItem> GetDistrict(string City);
        IEnumerable<CodeItem> GetSection(string City,string District);
        IEnumerable<CodeItem> GetSubSection(string City, string District ,string Section);        
        IEnumerable<CodeItem> GetCodeItem(string CodeClass);
        #endregion
        #region 搜尋既有的權狀資料
        RecordIndexModel SearchMainRecords(RecordSearchModel condition);

        DossierRightHandleResult ImportFromRecord(DossierRightModel dossierRightModel);
        #endregion
    }
}
