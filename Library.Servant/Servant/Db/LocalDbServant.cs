﻿using Library.Entity.Edmx;

namespace Library.Servant.Servant.Db
{
    public class LocalDbServant : IDbServant
    {
        public bool IsExist()
        {
            using (AS_LandBankEntities dbContext = new AS_LandBankEntities())
            {
                return dbContext.Database.Exists();
             }
        }
    }
}
