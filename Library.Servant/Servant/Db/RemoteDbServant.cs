﻿using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.Db
{
    public class RemoteDbServant : IDbServant
    {
        public bool IsExist()
        {
            return RemoteServant.Post<VoidModel, BoolResult>(

                            new VoidModel { },
                           "/DbServant/IsExist"
           );
        }
    }
}
