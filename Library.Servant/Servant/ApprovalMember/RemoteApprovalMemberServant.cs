﻿using Library.Servant.Servant.ApprovalMember.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.ApprovalMember
{
    public class RemoteApprovalMemberServant : IApprovalMemberServant
    {
        public IndexResult Index(SearchModel condition)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(

                condition,
                "/ApprovalMemberServant/Index"
            );
        }

        public IndexResult IndexAll(SearchModel condition)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(

                condition,
                "/ApprovalMemberServant/IndexAll"
            );
        }

        public EditResult Edit(int UserID)
        {
            return RemoteServant.Post<IntModel, EditResult>(

                new IntModel { Value = UserID },
                "/ApprovalMemberServant/Edit"
            );
        }

        public VoidResult Update(UpdateRoleModel model)
        {
            return RemoteServant.Post<UpdateRoleModel, VoidResult>(

              model,
               "/ApprovalMemberServant/Update"
           );
        }

        public VoidResult BatchUpdate(BatchUpdateModel model)
        {
            return RemoteServant.Post<BatchUpdateModel, VoidResult>(

              model,
               "/ApprovalMemberServant/BatchUpdate"
            );
        }
    }
}
