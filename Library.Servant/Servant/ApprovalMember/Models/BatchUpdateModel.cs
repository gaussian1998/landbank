﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Servant.ApprovalMember.Models
{
    public class BatchUpdateModel : AbstractEncryptionDTO
    {
        public List<int> UpdateUsers { get; set; }
        public int UpdateRoleID { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }

    }
}
