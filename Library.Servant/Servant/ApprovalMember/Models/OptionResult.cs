﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Servant.ApprovalMember.Models
{
    public class OptionResult : AbstractEncryptionDTO
    {
        public List<OptionModel<int, string>> Users { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
