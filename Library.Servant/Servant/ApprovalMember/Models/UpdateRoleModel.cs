﻿
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Servant.ApprovalMember.Models
{
    public class UpdateRoleModel : AbstractEncryptionDTO
    {
        public int UserID { get; set; }
        public List<int> ApprovalRoles { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
