﻿using Library.Common.Models;
using Library.Servant.Communicate;
using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace Library.Servant.Servant.ApprovalMember.Models
{
    public class IndexResult : InvasionEncryption
    {
        public BranchDepartmentOptions Options { get; set; }
        public PageList<DetailsResult> Page { get; set; }
        public List<OptionModel<int, string>> TotalFlowRoles { get; set; }
    }
}
