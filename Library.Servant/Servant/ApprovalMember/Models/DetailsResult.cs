﻿using System.Linq;
using System.Collections.Generic;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common.Models.Mapper;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.ApprovalMember.Models
{
    public class DetailsResult : UserModel
    {
        public ICollection<AS_Users_FlowRole> AS_Users_FlowRole
        {
            set
            {
                if (value == null)
                {
                    ApprovalRoles = new List<ApprovalRoleModel> { };
                    return;
                }

                ApprovalRoles = value.AsQueryable().Select(m => new ApprovalRoleModel
                {
                    ID = m.FlowRoleID,
                    CodeID = m.AS_Code_Table.Code_ID,
                    Text = m.AS_Code_Table.Text
                }).ToList();
            }
        }

        public int UserID
        {
            get
            {
                return ID;
            }
            set
            {
                ID = value;
            }
        }

        public string UserCode
        {
            get
            {
                return User_Code;
            }
            set
            {
                User_Code = value;
            }
        }

        public string UserName
        {
            get
            {
                return User_Name;
            }
            set
            {
                User_Name = value;
            }
        }

        public string BranchCode
        {
            get
            {
                return Branch_Code;
            }
            set
            {
                Branch_Code = value;
            }
        }

        public string BranchName
        {
            get
            {
                return Branch_Name;
            }
            set
            {
                Branch_Name = value;
            }
        }

        public string DepartmentName
        {
            get
            {
                return Department_Name;
            }
            set
            {
                Department_Name = value;
            }
        }

        public List<ApprovalRoleModel> ApprovalRoles { get; set; }
    }
}
