﻿using Library.Entity.Edmx;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common.Models.Mapper;
using System.Collections.Generic;
using System.Linq;

namespace Library.Servant.Servant.ApprovalMember.Models
{
    public class EditResult : UserModel
    {
        public ICollection<AS_Users_FlowRole> AS_Users_FlowRole
        {
            set
            {
                if (value == null)
                {
                    SelectedRoles = new List<OptionModel<int,string>>();
                    return;
                }

                SelectedRoles = value.AsQueryable().Select(m => new OptionModel<int, string>
                {
                    Value = m.AS_Code_Table.ID,
                    Text = (m.AS_Code_Table.Text+"-"+m.AS_Code_Table.Parameter1)
                }).ToList();
            }
        }

        public int UserID
        {
            get
            {
                return ID;
            }
            set
            {
                ID = value;
            }
        }

        public string UserName
        {
            get
            {
                return User_Name;
            }
            set
            {
                User_Name = value;
            }
        }

        public string BranchCode
        {
            get
            {
                return Branch_Code;
            }
            set
            {
                Branch_Code = value;
            }
        }

        public string BranchName
        {
            get
            {
                return Branch_Name;
            }
            set
            {
                Branch_Name = value;
            }
        }

        public string DepartmentCode
        {
            get
            {
                return Department_Code;
            }
            set
            {
                Department_Code = value;
            }
        }

        public string DepartmentName
        {
            get
            {
                return Department_Name;
            }
            set
            {
                Department_Name = value;
            }
        }
        public List<OptionModel<int, string>> SelectedRoles { get; set; }
        public List<OptionModel<int, string>> CadidateRoles { get; set; }
    }
}
