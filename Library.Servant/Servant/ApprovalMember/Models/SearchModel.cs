﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Library.Common.Utility;
using Library.Entity.Edmx;
using Library.Servant.Communicate;


namespace Library.Servant.Servant.ApprovalMember.Models
{
    public class SearchModel : InvasionEncryption
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string BranchCodeID { get; set; }
        public string DepartmentCodeID { get; set; }
        public int? FlowRoleID { get; set; }
        public string UserName { get; set; }

        public Expression<Func<AS_Users, bool>> Expression()
        {
            bool isSkipBranch = string.IsNullOrEmpty(BranchCodeID);
            bool isSkipDepartment = string.IsNullOrEmpty( DepartmentCodeID );
            bool isSkipRole = !FlowRoleID.HasValue;
            bool isSkipUserName = string.IsNullOrEmpty(UserName);
            //非總行不需要查詢部門
            if (!isSkipBranch && BranchCodeID != "001") {
                isSkipDepartment = true;
            }
            return ExpressionMaker.Make<AS_Users, bool>(

                m => (isSkipUserName || m.User_Name.Contains(UserName)) &&
                            (isSkipBranch || m.Branch_Code == BranchCodeID) &&
                            (isSkipDepartment || m.Department_Code == DepartmentCodeID) &&
                            (isSkipRole || m.AS_Users_FlowRole.Select(e => e.AS_Code_Table.ID).Contains(FlowRoleID.Value)));
        }
    }
}
