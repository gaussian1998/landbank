﻿using Library.Servant.Servant.ApprovalMember.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.ApprovalMember
{
    public interface IApprovalMemberServant
    {
        IndexResult Index(SearchModel condition);
        IndexResult IndexAll(SearchModel condition);
        EditResult Edit(int UserID);
        VoidResult Update(UpdateRoleModel model);
        VoidResult BatchUpdate(BatchUpdateModel model);
    }
}
