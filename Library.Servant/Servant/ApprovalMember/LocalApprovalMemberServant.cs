﻿using System.Linq;
using Library.Servant.Servant.ApprovalMember.Models;
using Library.Servant.Servant.Common;
using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalRole;
using Library.Common.Models;
using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace Library.Servant.Servant.ApprovalMember
{
    public class LocalApprovalMemberServant : IApprovalMemberServant
    {
        public IndexResult Index(SearchModel condition)
        {
            var result = AS_Users_Records.Page<DetailsResult,int>(
                condition.Page,
                condition.PageSize,
                m => m.ID,
                condition.Expression());

            return new IndexResult
            {
                Options = BranchDepartmentOptions.Query(),
                TotalFlowRoles = LocalApprovalRoleServant.Options(),
                Page = result
            };
        }

        public IndexResult IndexAll(SearchModel condition)
        {
            var result = AS_Users_Records.Find<DetailsResult>( condition.Expression()  );

            return new IndexResult
            {
                Options = BranchDepartmentOptions.Query(),
                TotalFlowRoles = LocalApprovalRoleServant.Options(),
                Page = new PageList<DetailsResult> {  Items = result, TotalAmount = result.Count  }
            };
        }

        public static DetailsResult Details(int userID)
        {
            return AS_Users_Records.First<DetailsResult>( user => user.ID == userID);
        }

        public EditResult Edit(int UserID)
        {
            return AS_Users_Records.FirstModify<EditResult>( m => m.ID == UserID, result => {

               var roles = result.SelectedRoles.Select(m => m.Value);
                result.CadidateRoles = ApproveRoleOptions().Where(m => !roles.Contains(m.Value)).ToList();

                return result;
            });
        }
        private static List<OptionModel<int, string>> ApproveRoleOptions()
        {
            return AS_Code_Table_Records.Find(m => m.Class == "R01").Select(m => new OptionModel<int, string> { Value = (m.ID), Text = (m.Text+"-"+m.Parameter1) }).ToList();
        }
        public VoidResult Update(UpdateRoleModel model)
        {
            AS_Users_FlowRole_Records.Delete(m => m.UserID == model.UserID);
            AS_Users_Records.Update(m => m.ID == model.UserID, entity => {

                if(model.ApprovalRoles!=null)
                {
                    model.ApprovalRoles.ForEach ( roleID => 
                    
                        entity.AS_Users_FlowRole.Add(new AS_Users_FlowRole { FlowRoleID = roleID })
                   );
                }
            });
            return new VoidResult { };
        }

        public VoidResult BatchUpdate(BatchUpdateModel model)
        {
            AS_Users_Records.Update( u => model.UpdateUsers.Contains( u.ID ) && !u.AS_Users_FlowRole.Any( r => r.FlowRoleID == model.UpdateRoleID  ) , entity => {

                entity.AS_Users_FlowRole.Add(new AS_Users_FlowRole { FlowRoleID = model.UpdateRoleID });
            } );

            return new VoidResult { };
        }
    }
}
