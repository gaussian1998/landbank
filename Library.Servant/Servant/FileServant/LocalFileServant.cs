﻿using System;
using System.Linq;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.FileServant
{
    public class LocalFileServant : IFileServant
    {
        public int Create(string filePath, string Owner="")
        {
            var nodes = filePath.Split(new char[] { '\\', '/' });
            if (nodes.Any())
                return Create(nodes, Owner);
            else
                return -1;
        }

        public int Open(string filePath)
        {
            var nodes = filePath.Split(new char[] { '\\', '/' });
            if (nodes.Any())
                return Open(nodes);
            else
                return -1;
        }

        public bool Write(int fd, byte[] data, string User="")
        {
            return AS_File_System_Records.Update(entity => entity.ID == fd && entity.Owner == User, entity => {

                entity.Data = data;
            }) == 1;
        }

        public byte[] Read(int fd)
        {
             return AS_File_System_Records.First(entity => entity.ID == fd).Data;
        }

        private int Open(string[] nodes)
        {
            string name = nodes[0];
            if (nodes.Length == 1)
            {
                return AS_File_System_Records.First(entity => !entity.Directory.HasValue && entity.FileName == name).ID;
            }
            else
            {
                Array.Resize(ref nodes, nodes.Length - 1);
                int DirectoryID = Open(nodes);

                return AS_File_System_Records.First(entity => entity.Directory == DirectoryID && entity.FileName == name).ID;
            }
        }

        private int Create(string[] nodes, string Owner)
        {
                string name = nodes[0];
                if (nodes.Length == 1)
                {
                    AS_File_System_Records.CreateNotExist(
                        entity => !entity.Directory.HasValue && entity.FileName == name,
                        entity => 
                        {
                            entity.FileName = name;
                            entity.Owner = Owner;
                        });

                    return AS_File_System_Records.First(entity => !entity.Directory.HasValue && entity.FileName == name).ID;
                }
                else
                {
                    Array.Resize( ref nodes, nodes.Length -1 );
                    int DirectoryID = Create(nodes, Owner);
                    AS_File_System_Records.CreateNotExist(

                        entity => entity.Directory == DirectoryID  && entity.FileName == name,
                        entity => {

                            entity.FileName = name;
                            entity.Directory = DirectoryID;
                            entity.Owner = Owner;
                    });

                return AS_File_System_Records.First(entity => entity.Directory == DirectoryID && entity.FileName == name).ID;
            }
        }
        
        private static T ForbidenException<T>( Func<T> func, T defaultValue)
        {
            try
            {
                return func();
            }
            catch
            {
                return defaultValue;
            }
        }
    }
}
