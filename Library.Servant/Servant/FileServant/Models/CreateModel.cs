﻿using Library.Servant.Communicate;

namespace Library.Servant.Servant.FileServant.Models
{
    public class CreateModel : InvasionEncryption
    {
        public string FilePath { get; set; }
        public string Owner { get; set; }
    }
}
