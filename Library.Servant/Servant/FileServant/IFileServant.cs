﻿

namespace Library.Servant.Servant.FileServant
{
    public interface IFileServant
    {
        int Create(string filePath, string Owner = "");
        int Open(string filePath);
        bool Write(int fd, byte[] data, string User = "");
        byte[] Read(int fd);
    }
}
