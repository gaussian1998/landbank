﻿using System;
using System.Linq;
using System.Text;
using Library.Servant.Common;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.FileServant.Models;


namespace Library.Servant.Servant.FileServant
{
    public class RemoteFileServant : IFileServant
    {
        public int Create(string filePath, string owner="")
        {
            return RemoteServant.Post<CreateModel, IntModel>(

                 new CreateModel { FilePath = filePath, Owner = owner },
                 "/FileServant/Create"
            ).Value;
        }

        public int Open(string filePath)
        {
            return RemoteServant.Post<StringModel,IntModel>(

                 new StringModel { Value = filePath },
                 "/FileServant/Open"
            ).Value;
        }

        public bool Write(int fd, byte[] data, string User = "")
        {
            var result = ProtocolService.PostBinary<BoolResult>( Packet(fd,data,User), WriteUrl(), request => {

                request.Timeout = 90*1000;
                request.ContentType = ContentType();
            } );

            return result && result.Value;
        }

        public byte[] Read(int fd)
        {
            return RemoteServant.Post<IntModel, ByteModel>(

                 new IntModel { Value = fd },
                 "/FileServant/Read"
            ).Value;
        }

        private static string WriteUrl()
        {
            return Config.ServantDomain + "/FileServant/Write";
        }

        private static byte[] Packet(int fd, byte[] data, string User)
        {
            return MultipartData("file", data, fd.ToString()).
                Concat(MultipartField("user", User)).
                Concat(MultipartClose()).ToArray();
        }

        private static string ContentType()
        {
            return String.Format("multipart/form-data; boundary={0}", boundary);
        }

        private static byte[] MultipartData(string name, byte[] value, string filename)
        {
            string field = string.Format( "--{2}\r\nContent-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n", name, filename, boundary);
            field += "Content-Type: application/octet-stream\r\n\r\n";

            return Encoding.ASCII.GetBytes(field).Concat(value).Concat(Encoding.ASCII.GetBytes("\r\n")).ToArray();
        }

        private static byte[] MultipartField(string name, string value)
        {
            string field = 
                string.Format("--{2}\r\nContent-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}\r\n", name, value, boundary);

            return Encoding.ASCII.GetBytes(field);
        }

        private static byte[] MultipartClose()
        {
            return Encoding.ASCII.GetBytes(  string.Format("--{0}--\r\n", boundary) );
        }

        private const string boundary = "----WebKitFormBoundary4f8jmXid0BtFmUSp";
    }
}
