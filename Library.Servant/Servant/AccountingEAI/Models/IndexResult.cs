﻿namespace Library.Servant.Servant.AccountingEAI.Models
{
    public class IndexResult
    {
        public int PrepareEAI { get; set; }
        public int TotalEAI { get; set; }
        public int AfterEAI { get; set; }
    }
}
