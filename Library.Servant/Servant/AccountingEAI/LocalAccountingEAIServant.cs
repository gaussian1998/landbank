﻿using Library.Servant.Servant.AccountingEAI.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.AccountingEAI
{
    public class LocalAccountingEAIServant : IAccountingEAIServant
    {
        public IndexResult Index()
        {
            return new IndexResult
            {
                PrepareEAI = 4,
                TotalEAI = 100,
                AfterEAI = 60
            };
        }

        public VoidResult Add()
        {
            return new VoidResult();
        }

        public VoidResult Remove()
        {
            return new VoidResult();
        }
    }
}
