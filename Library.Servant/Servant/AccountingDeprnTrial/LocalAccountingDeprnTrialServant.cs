﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Common.Utility;
using Library.Entity.Edmx;
using Library.Servant.Servant.AccountingDeprnTrial.Models;
using Library.Servant.Servant.ApprovalMember;
using Library.Servant.Servant.CodeTable;
using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Constants;
using Library.Servant.Servant.Common.Models;


namespace Library.Servant.Servant.AccountingDeprnTrial
{
    public class LocalAccountingDeprnTrialServant : IAccountingDeprnTrialServant
    {
        public IndexResult Index(SearchModel search)
        {
            return new IndexResult
            {
                Options = TradeOptions.QueryDeprn(),
                IsApproval = IsApproval(search),
                Items = Associate( Items(search) )
            };
        }
        
        public DetailsResult Details(DetailsModel model)
        {
            var branchs = LocalCodeTableServant.BranchDictionary();
            var departments = LocalCodeTableServant.DepartmentDictionary();

            return new DetailsResult
            {
                Items = AS_GL_Acc_Deprn_Records.FindModify<AccountEntryDetailResult>(

                    DetailsExpression(model), 
                    item =>
                    {
                        item.Branchs = branchs;
                        item.Departments = departments;
                        return item;
                    })
            };
        }
        
        public void DeprnTrial(DeprnTrialModel model)
        {
            Clear(model);
            DeprnTrialCalculate(model);
        }

        public void Clear(DeprnTrialModel model)
        {
            AS_GL_Acc_Deprn_Records.Update(

                ClearExpression(model),
                entity => {

                    entity.Status = AccountStateServant.Delete;
            });
            AS_GL_Acc_Deprn_Records.Delete(entity => entity.Status == AccountStateServant.Delete);
        }

        public void Deprn(DeprnModel model)
        {
            AS_GL_Acc_Deprn_Records.Update(

                DeprnExpression(model), 
                entity => {

                    entity.Status = AccountStateServant.PrepareApproval;
            });
        }
        
        /// <summary>
        ///  假的
        ///  我的眼睛業障重
        /// </summary>
        private static void DeprnTrialCalculate(DeprnTrialModel model)
        {
            using (var context = new AS_LandBankEntities())
            {
                context.Database.ExecuteSqlCommand(@"
INSERT [dbo].[AS_GL_Acc_Deprn] ([Book_Type], [Branch_Code], [Department], [Branch_Ind], [Account], [Account_Name], [Asset_Liablty], [Category], [Currency], [GL_Account], [Master_No], [Open_Date], [Open_Type], [Trans_Type], [Trade_Type], [Account_Key], [Trans_Seq], [Post_Seq], [DB_CR], [Real_Memo], [Stat_Balance_Date], [Status], [Asset_Number], [Lease_Number], [Virtual_Account], [Payment_User], [Payment_User_ID], [Batch_Seq], [Import_Number], [Soft_Lock], [Close_Date], [Stat_Frequency], [Post_Date], [Amount], [Value_Date], [Settle_Date], [Notes], [Notes1], [Last_Updated_Time], [Last_Updated_By], [Is_Updated], [Updated_Notes]) VALUES (N'NTD_MP_IFRS', N'001', N'00E', N'H', N'153101003', N'機械及設備-電腦端末設備', N'A', N'C', N'NTD', N'001-08/153101003/NTD', CAST(301300 AS Numeric(6, 0)), CAST(N'2017-06-21' AS Date), N'ST', N'1', N'1  ', N'A111', N'1060621A111000001', 123, N'D', N'R', CAST(N'1900-01-01' AS Date), N' ', N'1.000031.B.0000001', N'', N'', N'', N'', N'', N'', 0, CAST(N'2017-06-21' AS Date), CAST(0 AS Numeric(6, 0)), CAST(N'2017-06-21' AS Date), CAST(153000.00 AS Numeric(15, 2)), CAST(N'1900-01-01' AS Date), CAST(N'2017-06-21' AS Date), N'資產編號:1.000031.B.0000001', N'', CAST(N'2017-06-21T12:17:15.227' AS DateTime), CAST(85958 AS Numeric(15, 0)), 0, N'')
INSERT [dbo].[AS_GL_Acc_Deprn] ([Book_Type], [Branch_Code], [Department], [Branch_Ind], [Account], [Account_Name], [Asset_Liablty], [Category], [Currency], [GL_Account], [Master_No], [Open_Date], [Open_Type], [Trans_Type], [Trade_Type], [Account_Key], [Trans_Seq], [Post_Seq], [DB_CR], [Real_Memo], [Stat_Balance_Date], [Status], [Asset_Number], [Lease_Number], [Virtual_Account], [Payment_User], [Payment_User_ID], [Batch_Seq], [Import_Number], [Soft_Lock], [Close_Date], [Stat_Frequency], [Post_Date], [Amount], [Value_Date], [Settle_Date], [Notes], [Notes1], [Last_Updated_Time], [Last_Updated_By], [Is_Updated], [Updated_Notes]) VALUES (N'NTD_MP_IFRS', N'001', N'00E', N'H', N'900201006', N'臨時往來-部間往來', N'A', N'C', N'NTD', N'001-08/900201006/NTD', CAST(301300 AS Numeric(6, 0)), CAST(N'2017-06-21' AS Date), N'ST', N'1', N'1  ', N'A111', N'1060621A111000001', 123, N'C', N'R', CAST(N'1900-01-01' AS Date), N' ', N'1.000031.B.0000001', N'', N'', N'', N'', N'', N'', 0, CAST(N'2017-06-21' AS Date), CAST(0 AS Numeric(6, 0)), CAST(N'2017-06-21' AS Date), CAST(153000.00 AS Numeric(15, 2)), CAST(N'1900-01-01' AS Date), CAST(N'2017-06-21' AS Date), N'資產編號:1.000031.B.0000001', N'', CAST(N'2017-06-21T12:21:34.357' AS DateTime), CAST(85958 AS Numeric(15, 0)), 0, N'')
INSERT [dbo].[AS_GL_Acc_Deprn] ([Book_Type], [Branch_Code], [Department], [Branch_Ind], [Account], [Account_Name], [Asset_Liablty], [Category], [Currency], [GL_Account], [Master_No], [Open_Date], [Open_Type], [Trans_Type], [Trade_Type], [Account_Key], [Trans_Seq], [Post_Seq], [DB_CR], [Real_Memo], [Stat_Balance_Date], [Status], [Asset_Number], [Lease_Number], [Virtual_Account], [Payment_User], [Payment_User_ID], [Batch_Seq], [Import_Number], [Soft_Lock], [Close_Date], [Stat_Frequency], [Post_Date], [Amount], [Value_Date], [Settle_Date], [Notes], [Notes1], [Last_Updated_Time], [Last_Updated_By], [Is_Updated], [Updated_Notes]) VALUES (N'NTD_MP_IFRS', N'001', N'00E', N'H', N'154101007', N'交通及運輸設備-汽車類', N'A', N'C', N'NTD', N'001-08/154101007/NTD', CAST(301400 AS Numeric(6, 0)), CAST(N'2017-06-21' AS Date), N'ST', N'1', N'1  ', N'A111', N'1060621A111000002', 124, N'D', N'R', CAST(N'1900-01-01' AS Date), N' ', N'1.000020.B.0000001', N'', N'', N'', N'', N'', N'', 0, CAST(N'2017-06-21' AS Date), CAST(0 AS Numeric(6, 0)), CAST(N'2017-06-21' AS Date), CAST(567000.00 AS Numeric(15, 2)), CAST(N'1900-01-01' AS Date), CAST(N'2017-06-21' AS Date), N'資產編號:1.000020.B.0000001', N'', CAST(N'2017-06-21T12:32:20.293' AS DateTime), CAST(85958 AS Numeric(15, 0)), 0, N'')
INSERT [dbo].[AS_GL_Acc_Deprn] ([Book_Type], [Branch_Code], [Department], [Branch_Ind], [Account], [Account_Name], [Asset_Liablty], [Category], [Currency], [GL_Account], [Master_No], [Open_Date], [Open_Type], [Trans_Type], [Trade_Type], [Account_Key], [Trans_Seq], [Post_Seq], [DB_CR], [Real_Memo], [Stat_Balance_Date], [Status], [Asset_Number], [Lease_Number], [Virtual_Account], [Payment_User], [Payment_User_ID], [Batch_Seq], [Import_Number], [Soft_Lock], [Close_Date], [Stat_Frequency], [Post_Date], [Amount], [Value_Date], [Settle_Date], [Notes], [Notes1], [Last_Updated_Time], [Last_Updated_By], [Is_Updated], [Updated_Notes]) VALUES (N'NTD_MP_IFRS', N'001', N'00E', N'H', N'900201006', N'臨時往來-部間往來', N'A', N'C', N'NTD', N'001-08/900201006/NTD', CAST(301400 AS Numeric(6, 0)), CAST(N'2017-06-21' AS Date), N'ST', N'1', N'1  ', N'A111', N'1060621A111000002', 124, N'C', N'R', CAST(N'1900-01-01' AS Date), N' ', N'1.000020.B.0000001', N'', N'', N'', N'', N'', N'', 0, CAST(N'2017-06-21' AS Date), CAST(0 AS Numeric(6, 0)), CAST(N'2017-06-21' AS Date), CAST(567000.00 AS Numeric(15, 2)), CAST(N'1900-01-01' AS Date), CAST(N'2017-06-21' AS Date), N'資產編號:1.000020.B.0000001', N'', CAST(N'2017-06-21T12:32:20.310' AS DateTime), CAST(85958 AS Numeric(15, 0)), 0, N'')
");
            }
        }

        private static List<AccountEntryDetailResult> Items(SearchModel search)
        {
            return AS_GL_Acc_Deprn_Records.GroupList(

               IndexExpression(search),

               entity => new { entity.Account, entity.DB_CR },
               group => new AccountEntryDetailResult
                {
                    Department = group.FirstOrDefault().Department,
                    Branch_Code = group.FirstOrDefault().Branch_Code,
                    Account = group.FirstOrDefault().Account,
                    Account_Name = group.FirstOrDefault().Account_Name,
                    Amount = group.Sum(m => m.Amount),
                    Currency = group.FirstOrDefault().Currency,
                    DB_CR = group.FirstOrDefault().DB_CR,
                    Last_Updated_Time = group.FirstOrDefault().Last_Updated_Time,
                    TotalNumber = group.Count()
                }
            );
        }
        
        private static List<AccountEntryDetailResult> Associate(List<AccountEntryDetailResult> result)
        {
            var branchs = LocalCodeTableServant.BranchDictionary();
            var departments = LocalCodeTableServant.DepartmentDictionary();

            result.ForEach(item => {

                item.Branchs = branchs;
                item.Departments = departments;
            });
            return result;
        }

        private static bool IsApproval(SearchModel search)
        {
            var user = LocalApprovalMemberServant.Details(search.UserID);
            return AS_GL_Acc_Deprn_Records.Any(

                entity =>
                    entity.Branch_Code == user.BranchCode &&
                    entity.Status.Equals(AccountStateServant.New) == false
            );
        }

        static private Expression<Func<AS_GL_Acc_Deprn, bool>> IndexExpression(SearchModel search)
        {
            var user = LocalApprovalMemberServant.Details(search.UserID);
            return ExpressionMaker.Make<AS_GL_Acc_Deprn, bool>(

                entity =>
                    entity.Branch_Code == user.BranchCode &&
                    search.TradeKinds.Contains(entity.Trade_Type) &&
                    entity.Status.Equals(AccountStateServant.New)
            );
        }

        static private Expression<Func<AS_GL_Acc_Deprn, bool>> DetailsExpression(DetailsModel model)
        {
            var user = LocalApprovalMemberServant.Details(model.UserID);
            return ExpressionMaker.Make<AS_GL_Acc_Deprn, bool>(

                entity =>
                    entity.Branch_Code == user.BranchCode &&
                    entity.Account == model.Account
            );
        }

        static private Expression<Func<AS_GL_Acc_Deprn, bool>> DeprnExpression(DeprnModel model)
        {
            var user = LocalApprovalMemberServant.Details(model.UserID);
            return ExpressionMaker.Make<AS_GL_Acc_Deprn, bool>(

                entity =>
                    entity.Branch_Code == user.BranchCode &&
                    entity.Status.Equals(AccountStateServant.New)
            );
        }

        static private Expression<Func<AS_GL_Acc_Deprn, bool>> ClearExpression(DeprnTrialModel model)
        {
            var user = LocalApprovalMemberServant.Details(model.UserID);
            return ExpressionMaker.Make<AS_GL_Acc_Deprn, bool>(

                entity =>
                    entity.Branch_Code == user.BranchCode &&
                    model.TradeKinds.Contains(entity.Trade_Type) &&
                    entity.Status.Equals(AccountStateServant.New)
            );
        }
    }
}
