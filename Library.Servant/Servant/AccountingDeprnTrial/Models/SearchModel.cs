﻿
using System.Collections.Generic;

namespace Library.Servant.Servant.AccountingDeprnTrial.Models
{
    public class SearchModel
    {
        public int UserID { get; set; }
        public List<string> TradeKinds { get; set; }
    }
}
