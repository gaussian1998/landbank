﻿
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace Library.Servant.Servant.AccountingDeprnTrial.Models
{
    public class DetailsResult
    {
        public List<AccountEntryDetailResult> Items { get; set; }
    }
}
