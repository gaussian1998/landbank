﻿using System.Collections.Generic;

namespace Library.Servant.Servant.AccountingDeprnTrial.Models
{
    public class DeprnTrialModel
    {
        public int UserID { get; set; }
        public List<string> TradeKinds { get; set; }
    }
}
