﻿using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace Library.Servant.Servant.AccountingDeprnTrial.Models
{
    public class IndexResult
    {
        public TradeOptions Options { get; set; }
        public bool IsApproval { get; set; }
        public List<AccountEntryDetailResult> Items { get; set; }
    }
}
