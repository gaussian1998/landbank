﻿namespace Library.Servant.Servant.AccountingDeprnTrial.Models
{
    public class DetailsModel
    {
        public int UserID { get; set; }
        public string Account { get; set; }
    }
}
