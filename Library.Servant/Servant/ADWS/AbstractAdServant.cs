﻿using Library.Servant.Servant.ADWS.Models;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Library.Servant.Servant.ADWS
{
    public abstract class AbstractAdServant
    {
        public AdUserResult Verify(string UserID, string Password)
        {
            var rs = UserService().IdVerify(UserID, Password);
            if (rs.Status.StatusCode == 0)
                return new AdUserResult { USER_ID = rs.Users[0].Id, USER_NAME = rs.Users[0].DisplayName };
            else
                throw new Exception("Verify Error");
        }

        public AdUserResult Details(string UserID)
        {
            var rs = UserService().IdInquiry(UserID, "");
            if (rs.Status.StatusCode == 0)
                return new AdUserResult { USER_ID = rs.Users[0].Id, USER_NAME = rs.Users[0].DisplayName };
            else
                throw new Exception();
        }

        public List<AdUserResult> Index(string departmentID)
        {
            var rs = UserService().AggregateInquiry(departmentID,"");
            if (rs.Status.StatusCode == 0)
                return rs.Users.Select( user => new AdUserResult { USER_ID = user.Id, USER_NAME = user.DisplayName } ).ToList();
            else
                return new List<AdUserResult>();
        }

        public abstract UserServiceEx UserService();
    }
}
