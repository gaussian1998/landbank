﻿namespace Library.Servant.Servant.ADWS.Models
{
    public class AdUserResult
    {
        public string USER_ID { get; set; }
        public string USER_NAME { get; set; }
    }
}
