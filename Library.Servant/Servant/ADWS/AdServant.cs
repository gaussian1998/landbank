﻿using Library.Servant.Servants.AbstractFactory;

namespace Library.Servant.Servant.ADWS
{
    public class AdServant : AbstractAdServant
    {
        public override UserServiceEx UserService()
        {
            var proxy = new UserServiceEx();
            proxy.Url = ServantAbstractFactory.AdwsServer();

            return proxy;
        }
    }
}
