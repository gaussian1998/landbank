﻿using Library.Entity.Edmx;
using Library.Servant.Servant.Activation.ActivationModels;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Activation
{
    public interface IActivationServant
    {
        IndexModel Index(SearchModel condition);
        EJObject UpdateOrder(EJObject condition);
        EJObject testIndex(EJObject condition);
        EJObject J200IndexList(EJObject condition);
        EJObject GetMaxNo(EJObject condition);
        EJObject GetBuild(EJObject condition);
        EJObject GetLand(EJObject condition);
        EJObject GetPB(EJObject condition);
        EJObject GetAPB(EJObject condition);
        EJObject GetAB(EJObject condition);
        EJObject GetAL(EJObject condition);
        EJObject GetTarget(SearchModel condition);
        EJObject GetTargetE(EJObject condition);
        EJObject DocumentType(EInt type); 
        EJObject DocumentTypeOne(EInt type);
        EJObject GetDPM(EInt type);
        EJObject GetCode(EString type);
        EJObject GetOrder(EString type);
        EJObject EDOCServiceTest(EString type);
        EJObject GetDocID(EJObject condition);
        // AssetHandleResult UpdateOrder(EJObject model);
    }
}
