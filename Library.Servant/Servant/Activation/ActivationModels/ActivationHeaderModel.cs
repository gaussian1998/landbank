﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Activation.ActivationModels
{
    public class ActivationHeaderModel : AbstractEncryptionDTO
    {
        public long ID { get; set; }
        public Nullable<long> ParantID { get; set; }
        public Nullable<long> Document_Type { get; set; }
        public Nullable<int> Source_Type { get; set; }
        public string Activation_No { get; set; }
        public string BOOK_TYPE_CODE { get; set; }
        public Nullable<System.DateTime> BeginDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Jurisdiction { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<long> Activation_Target { get; set; }
        public string TEL { get; set; }
        public string Email { get; set; }
        public string Remark { get; set; }
        public Nullable<System.DateTime> Create_Time { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public Nullable<int> Last_Updated_By { get; set; }


        public int AssetsCount { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
