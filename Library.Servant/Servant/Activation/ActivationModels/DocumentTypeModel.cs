﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Activation.ActivationModels
{
    public class DocumentTypeModel : AbstractEncryptionDTO
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> Create_Time { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public Nullable<int> Last_Updated_By { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }

    }
}
