﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using Newtonsoft.Json.Linq;
using System;

namespace Library.Servant.Servant.Activation.ActivationModels
{
    public class EJObject : AbstractEncryptionDTO {
        public JObject  Obj { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
    public class EInt : AbstractEncryptionDTO
    {
        public int type { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
    public class EString : AbstractEncryptionDTO
    {
        public string type { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
    public class SearchModel : AbstractEncryptionDTO
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }

        /// <summary>單據類型</summary>
        public string Transaction_Type { get; set; }

        /// <summary>來源類型</summary>
        public string Source { get; set; }

        /// <summary>表單流程代號(單據類型 + 來源類型)</summary>
        public string DocCode { get; set; }

        /// <summary>單據編號</summary>
        public string TRXHeaderID { get; set; }

        /// <summary>單據編號-起</summary>
        public string TRXHeaderIDBgn { get; set; }

        /// <summary>單據編號-迄</summary>
        public string TRXHeaderIDEnd { get; set; }

        /// <summary>帳本</summary>
        public string BookTypeCode { get; set; }

        /// <summary>單據處理日期-起</summary>
        public DateTime? BeginDate { get; set; }

        /// <summary>單據處理日期-迄</summary>
        public DateTime? EndDate { get; set; }

        /// <summary>保管單位</summary>
        public string AssignedBranch { get; set; }

        /// <summary>管轄單位</summary>
        public string OfficeBranch { get; set; }

        /// <summary>移出行(原管轄單位)/// </summary>
        public string OriOfficeBranch { get; set; }

        /// <summary>狀態</summary>
        public string FlowStatus { get; set; }

        /// <summary>備註(關鍵字)</summary>
        public string Remark { get; set; }

        //<summary>處分原因</summary>
        public string DisposeReason { get; set; }

        //<summary>資產編號</summary>
        public string AssetNumber { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
