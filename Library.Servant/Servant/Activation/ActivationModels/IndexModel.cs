﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Activation.ActivationModels
{
    public class IndexModel : AbstractEncryptionDTO
    {
        public IEnumerable<ActivationModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public SearchModel condition { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
    public class testIndexModel : AbstractEncryptionDTO
    {
        public JObject Items { get; set; }
        public int TotalAmount { get; set; }
        public SearchModel condition { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
