﻿using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servants.AbstractFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Activation.ActivationEventListener
{
    public class ActivationEventListener : ExampleListener, IHttpGetProvider
    {
        private IActivationServant ActivationServant = ServantAbstractFactory.Activation();

        public HttpGetViewModel GetUrl(string FormNo, string carryData)
        {
            return new HttpGetViewModel { ControllerName = "Activation", ActionName = "Edit", Parameter = "id=" + FormNo + "" };
        }

        public override bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            bool result = true;
           // MPServant.CreateToMP(formNo);
            return result;
        }
    }
}
