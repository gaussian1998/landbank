﻿using System;
using System.Collections.Generic;
using System.Data;
using Library.Servant.Servant.Activation.ActivationModels;
using Library.Servant.Common;
using Library.Entity.Edmx;
using Newtonsoft.Json.Linq;

namespace Library.Servant.Servant.Activation
{
    public class RemoteActivationServant : IActivationServant
    {
        public IndexModel Index(SearchModel condition)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, IndexModel>
            (
                condition,
                Config.ServantDomain + "/ActivationServant/Index"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetMaxNo(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/ActivationServant/GetMaxNo"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetDocID(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/ActivationServant/GetDocID"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject testIndex(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/ActivationServant/testIndex"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject J200IndexList(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/ActivationServant/J200IndexList"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject GetBuild(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/ActivationServant/GetBuild"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetLand(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/ActivationServant/GetLand"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetPB(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/ActivationServant/GetPB"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetAPB(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/ActivationServant/GetAPB"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetAB(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/ActivationServant/GetAB"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetAL(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/ActivationServant/GetAL"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetTarget(SearchModel condition)
        {
          
            var result = ProtocolService.EncryptionPost<SearchModel, EJObject>
            (
                condition,
                Config.ServantDomain + "/ActivationServant/GetTarget"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetTargetE(EJObject condition)
        {

            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/ActivationServant/GetTargetE"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject DocumentType(EInt type)
        {
            var result = ProtocolService.EncryptionPost<EInt, EJObject>
            (
                type,
                Config.ServantDomain + "/ActivationServant/DocumentType"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject DocumentTypeOne(EInt type)
        {
            var result = ProtocolService.EncryptionPost<EInt, EJObject>
            (
                type,
                Config.ServantDomain + "/ActivationServant/DocumentTypeOne"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject GetDPM(EInt type)
        {
            var result = ProtocolService.EncryptionPost<EInt, EJObject>
            (
                type,
                Config.ServantDomain + "/ActivationServant/GetDPM"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject GetOrder(EString type)
        {
            var result = ProtocolService.EncryptionPost<EString, EJObject>
            (
                type,
                Config.ServantDomain + "/ActivationServant/GetOrder"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject EDOCServiceTest(EString type)
        {
            var result = ProtocolService.EncryptionPost<EString, EJObject>
            (
                type,
                Config.ServantDomain + "/ActivationServant/EDOCServiceTest"
            );
            return Tools.ExceptionConvert(result);
        }
        
        public EJObject GetCode(EString type)
        {
            var result = ProtocolService.EncryptionPost<EString, EJObject>
            (
                type,
                Config.ServantDomain + "/ActivationServant/GetCode"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject UpdateOrder(EJObject model)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                model,
                Config.ServantDomain + "/ActivationServant/UpdateOrder"
            );

            return Tools.ExceptionConvert(result);
        }
        /*
        public string CreateTRXID(string Type)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                new SearchModel() { Transaction_Type = Type },
                Config.ServantDomain + "/MPServant/CreateTRXID"
            );

            return Tools.ExceptionConvert(result).StrResult;
        }

        public MPModel GetDetail(string TRXHeaderID)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, MPModel>
            (
                new SearchModel() { TRXHeaderID = TRXHeaderID  },
                Config.ServantDomain + "/MPServant/GetDetail"
            );

            return Tools.ExceptionConvert(result);
        }

        public AssetHandleResult HandleHeader(MPHeaderModel Header)
        {
            var result = ProtocolService.EncryptionPost<MPHeaderModel, AssetHandleResult>
            (
                Header,
                Config.ServantDomain + "/MPServant/HandleHeader"
            );
            return Tools.ExceptionConvert(result);
        }

        public AssetHandleResult Create_Asset(MPModel model)
        {
            var result = ProtocolService.EncryptionPost<MPModel, AssetHandleResult>
            (
                model,
                Config.ServantDomain + "/MPServant/Create_Asset"
            );
            return Tools.ExceptionConvert(result);
        }

        public AssetModel GetAssetDetail(int ID)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, AssetModel>
            (
                new AssetsSearchModel() { ID = ID },
                Config.ServantDomain + "/MPServant/GetAssetDetail"
            );
            return Tools.ExceptionConvert(result);
        }

        public AssetHandleResult Update_Asset(MPModel model)
        {
            var result = ProtocolService.EncryptionPost<MPModel, AssetHandleResult>
            (
                model,
                Config.ServantDomain + "/MPServant/Update_Asset"
            );
            return Tools.ExceptionConvert(result);
        }

        public bool GetDocID(string DocCode, out int DocID)
        {
            DocID = 0;
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                new SearchModel() { DocCode = DocCode },
                Config.ServantDomain + "/MPServant/GetDocID"
            );
            if(result != null)
            {
                DocID = Convert.ToInt32(Tools.ExceptionConvert(result).StrResult);
            }

            return Tools.ExceptionConvert(result).IsSuccess;
        }

        public bool UpdateFlowStatus(string TRX_Header_ID, string FlowStatus)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                 new SearchModel() { TRXHeaderID = TRX_Header_ID, FlowStatus = FlowStatus },
                 Config.ServantDomain + "/MPServant/UpdateFlowStatus"
            );
            return Tools.ExceptionConvert(result).IsSuccess;
        }

        public bool CopyAsset(string CopyAssetID, int CopyCount)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, StringResult>
            (
                 new AssetsSearchModel() { CopyAssetID = CopyAssetID, CopyCount = CopyCount },
                 Config.ServantDomain + "/MPServant/CopyAsset"
            );
            return Tools.ExceptionConvert(result).IsSuccess;
        }

        public string CreateAssetNumber(string type1, string type2, string type3)
        {
            var result = ProtocolService.EncryptionPost<AssetModel, StringResult>
            (
                 new AssetModel() {type1 = type1, type2 = type2, type3 = type3 },
                 Config.ServantDomain + "/MPServant/CreateAssetNumber"
            );
            return Tools.ExceptionConvert(result).StrResult;
        }

        public bool DeleteAsset(int AssetID)
        {
            var result = ProtocolService.EncryptionPost<AssetModel, StringResult>
            (
                 new AssetModel() { ID = AssetID },
                 Config.ServantDomain + "/MPServant/DeleteAsset"
            );
            return Tools.ExceptionConvert(result).IsSuccess;
        }

        public IEnumerable<CodeItem> GetAssetMain_Kind()
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, CodeItemModel>
            (
                 new AssetsSearchModel(),
                 Config.ServantDomain + "/MPServant/GetAssetMain_Kind"
            );
            return Tools.ExceptionConvert(result).Items;
        }

        public IEnumerable<CodeItem> GetAssetDetail_Kind(string MainKind)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, CodeItemModel>
             (
                  new AssetsSearchModel() { MainKind = MainKind },
                  Config.ServantDomain + "/MPServant/GetAssetDetail_Kind"
             );
            return Tools.ExceptionConvert(result).Items;
        }

        public IEnumerable<CodeItem> GetAssetUse_Types()
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, CodeItemModel>
             (
                  new AssetsSearchModel(),
                  Config.ServantDomain + "/MPServant/GetAssetUse_Types"
             );
            return Tools.ExceptionConvert(result).Items;
        }

        public IEnumerable<CodeItem> GetKeep_Position(string Target)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, CodeItemModel>
             (
                  new AssetsSearchModel() { KeepPosition = Target },
                  Config.ServantDomain + "/MPServant/GetKeep_Position"
             );
            return Tools.ExceptionConvert(result).Items;
        }

        public IEnumerable<CodeItem> GetUser(string Branch, string Dept)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, CodeItemModel>
             (
                  new AssetsSearchModel() { Branch = Branch , Dept = Dept},
                  Config.ServantDomain + "/MPServant/GetUser"
             );
            return Tools.ExceptionConvert(result).Items;
        }

        public string GetAccountItem(string AssetDetailKind)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, StringResult>
             (
                  new AssetsSearchModel() { AssetDetailKind = AssetDetailKind },
                  Config.ServantDomain + "/MPServant/GetAccountItem"
             );
            return Tools.ExceptionConvert(result).StrResult;
        }

        public IEnumerable<CodeItem> GetCodeItem(string CodeClass)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, CodeItemModel>
             (
                  new AssetsSearchModel() { CodeClass = CodeClass },
                  Config.ServantDomain + "/MPServant/GetCodeItem"
             );
            return Tools.ExceptionConvert(result).Items;
        }

        public IEnumerable<CodeItem> SearchAssetsByCategoryCode(string CategoryCode)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, CodeItemModel>
             (
                  new AssetsSearchModel() { CategoryCode = CategoryCode },
                  Config.ServantDomain + "/MPServant/SearchAssetsByCategoryCode"
             );
            return Tools.ExceptionConvert(result).Items;
        }

        public AssetModel GetAssetDetailByAssetNumber(string AssetNumber)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, AssetModel>
             (
                  new AssetsSearchModel() { Asset_Number = AssetNumber },
                  Config.ServantDomain + "/MPServant/GetAssetDetailByAssetNumber"
             );
            return Tools.ExceptionConvert(result);
        }

        public AssetsIndexModel SearchAssets(AssetsSearchModel condition)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, AssetsIndexModel>
             (
                  condition,
                  Config.ServantDomain + "/MPServant/SearchAssets"
             );
            return Tools.ExceptionConvert(result);
        }

        public AssetHandleResult CreateMP(MPModel MP)
        {
            var result = ProtocolService.EncryptionPost<MPModel, AssetHandleResult>
             (
                  MP,
                  Config.ServantDomain + "/MPServant/CreateMP"
             );
            return Tools.ExceptionConvert(result);
        }

        public string CreateImport_Num()
        {
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
             (
                  new SearchModel(),
                  Config.ServantDomain + "/MPServant/CreateImport_Num"
             );
            return Tools.ExceptionConvert(result).StrResult;
        }

        

        public AssetHandleResult CreateToMP(string TRXHeaderID)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, AssetHandleResult>
             (
                  new SearchModel() { TRXHeaderID = TRXHeaderID },
                  Config.ServantDomain + "/MPServant/CreateToMP"
             );
            return Tools.ExceptionConvert(result);
        }


        public AssetHandleResult CreateImport(MPImportModel import, IEnumerable<AssetModel> AssetList)
        {
            var result = ProtocolService.EncryptionPost<MPImportPostModel, AssetHandleResult>
             (
                  new MPImportPostModel() { import = import, AssetList = AssetList },
                  Config.ServantDomain + "/MPServant/CreateImport"
             );
            return Tools.ExceptionConvert(result);
        }
        
        public MPHeaderModel GetHeader(string AssetNumber, string Type)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, MPHeaderModel>
             (
                  new SearchModel() { AssetNumber = AssetNumber,Transaction_Type = Type },
                  Config.ServantDomain + "/MPServant/GetHeader"
             );
            return Tools.ExceptionConvert(result);
        }

       

        public AssetHandleResult UpdateToMP(string TRXHeaderID)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, AssetHandleResult>
             (
                  new SearchModel() { TRXHeaderID = TRXHeaderID },
                  Config.ServantDomain + "/MPServant/UpdateToMP"
             );
            return Tools.ExceptionConvert(result);
        }

        

        public ImportIndexModel ImportIndex(ImportSearchModel condition)
        {
            var result = ProtocolService.EncryptionPost<ImportSearchModel, ImportIndexModel>
             (
                  condition,
                  Config.ServantDomain + "/MPServant/ImportIndex"
             );
            return Tools.ExceptionConvert(result);
        }


        public IEnumerable<AS_VW_MP_ImportHis> GetImportData(int ImportID)
        {
            var result = ProtocolService.EncryptionPost<MPImportPostModel, MPImportPostModel>
             (
                  new MPImportPostModel() { ImportID = ImportID },
                  Config.ServantDomain + "/MPServant/GetImportData"
             );
            return Tools.ExceptionConvert(result).ImportHis;
        }

        public AssetHandleResult CreateImportReview(string IRID,string[] ImportList, MPHeaderModel Header)
        {
            var result = ProtocolService.EncryptionPost<MPImportPostModel, AssetHandleResult>
             (
                  new MPImportPostModel() { IRID = IRID , ImportList = ImportList , Header = Header },
                  Config.ServantDomain + "/MPServant/CreateImportReview"
             );
            return Tools.ExceptionConvert(result);
        }

        public MPImportReviewModel GetImportReviewDetail(string IRID)
        {
            var result = ProtocolService.EncryptionPost<MPImportPostModel, MPImportReviewModel>
             (
                  new MPImportPostModel() { IRID = IRID },
                  Config.ServantDomain + "/MPServant/GetImportReviewDetail"
             );
            return Tools.ExceptionConvert(result);
        }

        public ImportReviewIndexModel ImportReviewIndex()
        {
            var result = ProtocolService.EncryptionPost<MPImportPostModel, ImportReviewIndexModel>
             (
                  new MPImportPostModel(),
                  Config.ServantDomain + "/MPServant/ImportReviewIndex"
             );
            return Tools.ExceptionConvert(result);
        }

        public AssetsIndexModel GetImportAssets(int ImportID)
        {
            var result = ProtocolService.EncryptionPost<MPImportPostModel, AssetsIndexModel>
             (
                  new MPImportPostModel() { ImportID = ImportID },
                  Config.ServantDomain + "/MPServant/GetImportAssets"
             );
            return Tools.ExceptionConvert(result);
        }

        public AssetModel GetImportAssetDetail(int ID)
        {
            var result = ProtocolService.EncryptionPost<MPImportPostModel, AssetModel>
             (
                  new MPImportPostModel() { ID = ID },
                  Config.ServantDomain + "/MPServant/GetImportAssetDetail"
             );
            return Tools.ExceptionConvert(result);
        }

        public IEnumerable<DeprnRecordModel> GetDeprnRecord(string AssetNumber)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, MPModel>
             (
                  new AssetsSearchModel() { Asset_Number = AssetNumber },
                  Config.ServantDomain + "/MPServant/DeprnRecordModel"
             );
            return Tools.ExceptionConvert(result).DeprnRecord;
        }*/


    }
}
