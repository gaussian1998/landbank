﻿using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servants.AbstractFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.LeaseRights.LeaseRightsEventListener
{
    public class LeaseRightsAddedEventListener : ExampleListener, IHttpGetProvider
    {
        private ILeaseRightsServant LeaseRightsServant = ServantAbstractFactory.LeaseRights();

        public HttpGetViewModel GetUrl(string FormNo, string carryData)
        {
            return new HttpGetViewModel { ControllerName = "LeaseRightsAdded", ActionName = "Edit", Parameter = "id=" + FormNo + "&signmode=Y" };
        }

        public override bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            bool result = true;
            LeaseRightsServant.CreateToMP(formNo);
            return result;
        }
    }
}
