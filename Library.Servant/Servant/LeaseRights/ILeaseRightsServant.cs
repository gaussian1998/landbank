﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Entity.Edmx;
using Library.Servant.Servant.MP.MPModels;

namespace Library.Servant.Servant.LeaseRights
{
    public interface ILeaseRightsServant
    {
        IndexModel Index(SearchModel condition);

        MPModel GetDetail(string TRXHeaderID);

        AssetHandleResult Create_Asset(MPModel model);

        AssetHandleResult CreateToMP(string TRXHeaderID);

        AssetHandleResult Update_Asset(MPModel model);

        AssetHandleResult UpdateToMP(string TRXHeaderID);

        AssetModel GetAssetDetail(int ID);

        MPHeaderModel GetHeader(string AssetNumber, string Type);

        AssetsIndexModel SearchAssets(AssetsSearchModel condition);

        AssetHandleResult CreateMP(MPModel MP);

        string CreateTRXID(string Type);

        string CreateAssetNumber(string type1, string type2, string type3);

        AssetHandleResult HandleHeader(MPHeaderModel Header, IEnumerable<AssetModel> Assets = null);

        bool GetDocID(string DocCode, out int DocID);

        bool CopyAsset(string CopyAssetID, int CopyCount);

        bool UpdateFlowStatus(string TRX_Header_ID, string FlowStatus);

        bool DeleteAsset(int AssetID);

        IEnumerable<DeprnRecordModel> GetDeprnRecord(string AssetNumber);

        bool MPInventoryNotice(string TRX_Header_ID);

        AssetModel GetAssetMPDetail(int ID);

        bool AssetsDeprnUpdate(IEnumerable<AssetModel> assets);




        #region == 抓動產相關代碼20170830,暫時放這 ==
        IEnumerable<CodeItem> GetAssetMain_Kind(string Type);

        IEnumerable<CodeItem> GetAssetDetail_Kind(string MainKind);

        IEnumerable<CodeItem> GetAssetUse_Types();

        IEnumerable<CodeItem> GetKeep_Position(string Target);

        IEnumerable<CodeItem> GetUser(string Branch, string Dept);

        //取得會計科目
        string GetAccountItem(string AssetDetailKind);

        IEnumerable<CodeItem> GetCodeItem(string CodeClass);

        IEnumerable<CodeItem> SearchAssetsByCategoryCode(string CategoryCode);

        AssetModel GetAssetDetailByAssetNumber(string AssetNumber);
        #endregion

        #region == 抓目前登入者資訊 ==
        UserDetail GetUserInfo(int ID);
        #endregion
    }
}
