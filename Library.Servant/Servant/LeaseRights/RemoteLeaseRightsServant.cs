﻿using System;
using System.Collections.Generic;
using System.Data;
using Library.Servant.Servant.MP.MPModels;
using Library.Servant.Common;
using Library.Entity.Edmx;


namespace Library.Servant.Servant.LeaseRights
{
    public class RemoteLeaseRightsServant : ILeaseRightsServant
    {
        public bool AssetsDeprnUpdate(IEnumerable<AssetModel> assets)
        {
            throw new NotImplementedException();
        }

        public bool CopyAsset(string CopyAssetID, int CopyCount)
        {
            throw new NotImplementedException();
        }

        public string CreateAssetNumber(string type1, string type2, string type3)
        {
            throw new NotImplementedException();
        }

        public AssetHandleResult CreateMP(MPModel MP)
        {
            throw new NotImplementedException();
        }

        public AssetHandleResult CreateToMP(string TRXHeaderID)
        {
            throw new NotImplementedException();
        }

        public string CreateTRXID(string Type)
        {
            throw new NotImplementedException();
        }

        public AssetHandleResult Create_Asset(MPModel model)
        {
            throw new NotImplementedException();
        }

        public bool DeleteAsset(int AssetID)
        {
            throw new NotImplementedException();
        }

        public string GetAccountItem(string AssetDetailKind)
        {
            throw new NotImplementedException();
        }

        public AssetModel GetAssetDetail(int ID)
        {
            throw new NotImplementedException();
        }

        public AssetModel GetAssetDetailByAssetNumber(string AssetNumber)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CodeItem> GetAssetDetail_Kind(string MainKind)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CodeItem> GetAssetMain_Kind(string Type)
        {
            throw new NotImplementedException();
        }

        public AssetModel GetAssetMPDetail(int ID)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CodeItem> GetAssetUse_Types()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CodeItem> GetCodeItem(string CodeClass)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DeprnRecordModel> GetDeprnRecord(string AssetNumber)
        {
            throw new NotImplementedException();
        }

        public MPModel GetDetail(string TRXHeaderID)
        {
            throw new NotImplementedException();
        }

        public bool GetDocID(string DocCode, out int DocID)
        {
            throw new NotImplementedException();
        }

        public MPHeaderModel GetHeader(string AssetNumber, string Type)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CodeItem> GetKeep_Position(string Target)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CodeItem> GetUser(string Branch, string Dept)
        {
            throw new NotImplementedException();
        }

        public UserDetail GetUserInfo(int ID)
        {
            throw new NotImplementedException();
        }

        public AssetHandleResult HandleHeader(MPHeaderModel Header, IEnumerable<AssetModel> Assets = null)
        {
            throw new NotImplementedException();
        }

        public IndexModel Index(SearchModel condition)
        {
            throw new NotImplementedException();
        }

        public bool MPInventoryNotice(string TRX_Header_ID)
        {
            throw new NotImplementedException();
        }

        public AssetsIndexModel SearchAssets(AssetsSearchModel condition)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CodeItem> SearchAssetsByCategoryCode(string CategoryCode)
        {
            throw new NotImplementedException();
        }

        public bool UpdateFlowStatus(string TRX_Header_ID, string FlowStatus)
        {
            throw new NotImplementedException();
        }

        public AssetHandleResult UpdateToMP(string TRXHeaderID)
        {
            throw new NotImplementedException();
        }

        public AssetHandleResult Update_Asset(MPModel model)
        {
            throw new NotImplementedException();
        }
    }
}
