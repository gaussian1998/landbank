﻿using System.Linq;
using Library.Entity.Edmx;
using Library.Servant.Servant.UserHistory;
using Library.Servant.Servant.UserHistory.Models;

namespace Library.Servant.Servant.UserRole
{
    public class LocalUserHistoryServant : IUserHistoryServant
    {
        public IndexResult Index(SearchModel conditions)
        {
            // 為了把查詢區間調整為 xxxx/xx/xx 00:00:00 ~ yyyy/yy/yy 23:59:59
            var newEnd = conditions.End.AddDays(1).AddSeconds(-1.0);

            //todo_julius
            var pageList = AS_Opration_His_Records.Page(conditions.Page, conditions.PageSize, m => m.ID, 
                m => (string.IsNullOrEmpty(conditions.UserCode) || m.Branch_Code == conditions.BranchCode) && 
                     (string.IsNullOrEmpty(conditions.BranchCode) || m.Branch_Code == conditions.BranchCode) &&
                     (conditions.DepartmentID == null || m.Department_Code == conditions.DepartmentID) &&
                     (m.Login_Datetime >= conditions.Start && m.Login_Datetime <= newEnd), 
                entity => new DetailsResult
                {
                    ID = entity.ID,
                    UserName = entity.AS_Users.User_Name,
                    DepartmentName = entity.Department_Code, //todo_julius
                    LoginDateTime = entity.Login_Datetime,
                    LastDateTime = entity.Last_request,
                    LogoutDateTime = entity.Logout_Datetime,
                    IP = entity.User_IP
                });

            return new IndexResult
            {
                Items = pageList.Items,
                TotalAmount = pageList.TotalAmount
            };
        }
    }
}
