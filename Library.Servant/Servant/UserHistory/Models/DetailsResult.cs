﻿using System;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using Library.Servant.Enums;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.UserHistory.Models
{
    public class DetailsResult : InvasionEncryption
    {
        private string _department;

        public int ID { get; set; }
        public string UserName { get; set; }
        public string DepartmentName {
            get {
                var classCode = CodeTableClass.Department.GetClassID();
                
                return AS_Code_Table_Records.First(m => m.Class == classCode && m.Code_ID == _department, entity => entity.Text);
            }
            set {
                _department = value;
            }
        }
        public Nullable<DateTime> LoginDateTime { get; set; }
        public Nullable<DateTime> LastDateTime { get; set; }
        public Nullable<DateTime> LogoutDateTime { get; set; }
        public string IP { get; set; }
    }
}
