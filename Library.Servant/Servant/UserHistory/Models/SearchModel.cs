﻿using System;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.UserHistory.Models
{
    public class SearchModel : InvasionEncryption
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string UserCode { get; set; }
        public string BranchCode { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string DepartmentID { get; set; } 
    }
}
