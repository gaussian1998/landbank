﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.UserHistory.Models;

namespace Library.Servant.Servant.UserHistory
{
    public interface IUserHistoryServant
    {
        IndexResult Index(SearchModel conditions);
    }
}
