﻿using Library.Common.Utility;
using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalTodo.Constants;
using Library.Servant.Servant.CodeTable;
using Library.Servant.Servant.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Library.Servant.Servant.Common.ModelExtensions
{
    public static class FlowOpenExtension
    {
        public static FlowSearchOptionsModel Options(this List<FlowOpenIndexItem> cadidate, List<decimal> PermitState)
        {
            return LocalCodeTableServant.FlowSearchOptions(options =>
            {
                options.FlowStateOptions = PermitState.Select(state => new OptionModel<int, string> { Value = (int)state, Text = FlowStateServant.Text(state) }).ToList();
                options.ApplyUserOptions = cadidate.GroupBy(open => open.ApplyUserID).Select(open => new OptionModel<int, string> { Value = open.Key, Text = open.First().ApplyUserName }).ToList();
                options.CurrentApprovalUserOptions = cadidate.Where(open => open.CurrentApprovalUserID.HasValue).GroupBy(open => open.CurrentApprovalUserID).Select(open => new OptionModel<int, string> { Value = open.Key.Value, Text = open.First().CurrentApprovalUserName }).ToList();
            });
        }
    }
}
