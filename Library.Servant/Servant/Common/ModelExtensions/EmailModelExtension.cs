﻿using System;
using Library.Entity.Edmx;
using Library.Servant.Servant.Email.Models;

namespace Library.Servant.Servant.Common.ModelExtensions
{
    public static class EmailModelExtension
    {
        public static string ToXml(this EmailModel model, string domain)
        {
            string xml = string.Format( Pattern(), model.Parameter(domain));

            Console.WriteLine(xml);
            return xml;
        }

        private static object[] Parameter(this EmailModel model, string domain)
        {
            var sender = AS_Users_Records.First(user => user.ID == model.UserID);
            var recver = AS_Users_Records.First(user => user.ID == model.RecverID);
            return new object[] {

                sender.User_Code,
                sender.User_Name,
                recver.User_Name,
                recver.User_Code + "@" + domain,
                model.Title,
                model.Body
            };
        }

        private static string Pattern()
        {
            return 
@"<?xml version=""1.0"" encoding=""utf-8"" ?>
<BlueStar MsgName=""BSNS:EventFactory"" App=""XML"">
  <BSNS_TelegramSource>_J122</BSNS_TelegramSource>
  <BSNS_ProductID>J1</BSNS_ProductID>
  <BSNS_NotificationClassID>01</BSNS_NotificationClassID>
  <BSNS_CustomerID>{0}</BSNS_CustomerID>
  <BSNS_CustomerName>{1}</BSNS_CustomerName>
  <BSNS_IsBypassBSNS>Y</BSNS_IsBypassBSNS>
  <BSNS_ContactWindowSet>
    <BSNS_ContactWindow>
      <BSNS_DeliveryChannel>E</BSNS_DeliveryChannel>
      <BSNS_ContactName>{2}</BSNS_ContactName>
      <BSNS_ContactAddress>{3}</BSNS_ContactAddress>
      <BSNS_Language>zh-tw</BSNS_Language>
    </BSNS_ContactWindow>
  </BSNS_ContactWindowSet>
  <NotificationTitle>{4}</NotificationTitle>
  <ContentXML>
    <Head>{4}</Head>
    <Body>{5}</Body>
  </ContentXML>
</BlueStar>
";
        }
    }



}
