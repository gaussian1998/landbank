﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.Entity.Edmx;
using Library.Servant.Enums;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.Common
{
    public static class StaticDataServant
    {
        //private static string DocTypeClass = "DocType";

        public static List<IDSelectedModel> getIDOptions(string Class)
        {
            var Options = AS_Code_Table_Records.Find(m => m.Class == Class);

            return Options.Select(m => new IDSelectedModel { Value = m.Code_ID, Name = m.Text }).ToList();
        }


        public static List<SelectedModel> getDocNames()
        {
            var AllDocNames = AS_Doc_Name_Records.Find(m => m.Is_Active);

            return AllDocNames.Select(m => new SelectedModel { Value = m.Doc_No, Name = m.Doc_No + " " + m.Doc_Name }).ToList();
        }
        public static List<SelectedModel> getDocNames(string docType)
        {
            var AllDocNames = AS_Doc_Name_Records.Find(m => m.Is_Active );
            if (docType != null && docType!="") AllDocNames = AllDocNames.Where(x => x.Doc_Type == docType).ToList();
            return AllDocNames.Select(m => new SelectedModel { Value = m.Doc_No, Name = m.Doc_No + " " + m.Doc_Name }).ToList();
        }
        public static string GetStatusText(string code)
        {
            var id = CodeTableClass.TransactionStatus.GetClassID();
            var statusInfo = AS_Code_Table_Records.Find(m => m.Class == id && m.Code_ID == code, entity => entity);
            return statusInfo.FirstOrDefault()?.Text ?? "";
        }

        public static List<SelectedModel> GetBranchs() {
            var branch = AS_Code_Table_Records.Find(x => x.Class == "C01");
            return branch.Select(x => new SelectedModel { Value = x.Code_ID, Name = x.Code_ID+x.Text }).ToList();
        }

        public static List<SelectedModel> GetDepts()
        {
            var dept = AS_Code_Table_Records.Find(x => x.Class == "C02");
            return dept.Select(x => new SelectedModel { Value = x.Code_ID, Name = x.Code_ID+x.Text }).ToList();
        }

        public static List<SelectedModel> GetAccountingBooks()
        {
            var branch = AS_Code_Table_Records.Find(x => x.Class == "V01");
            return branch.Select(x => new SelectedModel { Value = x.Code_ID, Name = x.Code_ID }).ToList();
        }
        public static List<SelectedModel> GetCurrencys()
        {
            var currency = AS_Currency_Records.FindAll();
            return currency.Select(x => new SelectedModel { Value = x.Currency, Name = x.Currency_Name }).ToList();
        }

        public static List<SelectedModel> GetTransTypes()
        {
            var transType = AS_Code_Table_Records.Find(x => x.Class == "V03");
            return transType.Select(x => new SelectedModel { Value = x.Code_ID, Name = x.Text }).ToList();
        }

        public static List<SelectedModel> GetTradeTypes()
        {
            var transType = AS_Code_Table_Records.Find(x => x.Class == "V04");
            return transType.Select(x => new SelectedModel { Value = x.Code_ID, Name = x.Text }).ToList();
        }
        public static List<SelectedModel> GetAssetMainKinds()
        {
            var mainKinds = AS_Asset_Category_Main_Kinds_Records.Find(x => x.Is_Active == true);
            return mainKinds.Select(x => new SelectedModel { Value = x.No, Name = String.Format("{0}{1}", x.No, x.Name) }).ToList();
        }

        public static List<SelectedModel> GetAssetDetailKinds()
        {
            var mainKinds = AS_Asset_Category_Detail_Kinds_Records.Find(x => x.Is_Active == true);
            return mainKinds.Select(x => new SelectedModel { Value = x.No, Name = String.Format("{0}{1}", x.No, x.Name), Extend = x.Main_Kind_ID.ToString() }).ToList();
        }

        public static List<SelectedModel> GetAssetUseTypes()
        {
            var useTypes = AS_Asset_Category_Use_Types_Records.Find(x => x.Is_Active == true);
            return useTypes.Select(x => new SelectedModel { Value = x.No, Name = String.Format("{0}{1}", x.No, x.Name) }).ToList();
        }

        public static List<UserInfoOptionModel> GetAllUserInfo()
        {
            return AS_Users_Records.FindAll(m => new UserInfoOptionModel
            {
                UserID = m.ID,
                UserCode = m.User_Code,
                UserName = m.User_Name,
                BranchCode = m.Branch_Code,
                DepartmentCode = m.Department_Code
            });
        }

        public static List<SelectedModel> GetDocTypes()
        {
            return AS_Code_Table_Records.Find(m => m.Class == "DocType").
                Select(m => new SelectedModel { Value = m.Code_ID, Name = m.Text }).ToList();
        }

        public static List<SelectedModel> GetUserTransactionTypes()
        {
            return AS_Code_Table_Records.Find(m => m.Class == "T01").
                Select(m => new SelectedModel { Value = m.Code_ID, Name = m.Text }).ToList();
        }

        public static List<SelectedModel> GetApprovalTypes()
        {
            return AS_Code_Table_Records.Find(m => m.Class == "T02").
                Select(m => new SelectedModel { Value = m.Code_ID, Name = m.Text }).ToList();
        }

        public static string GetUserCode(int id)
        {
            return AS_Users_Records.First(m => m.ID == id).User_Code;
        }

        public static string GetUserName(int id)
        {
            return AS_Users_Records.First(m => m.ID == id).User_Name;
        }
        public static List<SelectedModel> GetTradeTypes(string transType)
        {
            List<AS_Code_Table> _transType = AS_Code_Table_Records.Find(x => x.Class == "V04").ToList();
            if (transType != null && transType!="") {
                List<string> _listTradeType = AS_GL_Trade_Define_Records.Find(x => x.Trans_Type == transType).Select(x => x.Trade_Type.Trim()).Distinct().ToList();
                _transType = _transType.Where( x=> _listTradeType.Contains(x.Code_ID)).ToList();
            }
            return _transType.Select(x => new SelectedModel { Value = x.Code_ID, Name = x.Text }).ToList();
        }
        public static List<SelectedModel> GetApproveRole(string operationType)
        {
            List<AS_Code_Table> _approveRole = AS_Code_Table_Records.Find(x => x.Class == "R01").ToList();
            if (operationType != null && operationType != "")
            {
                _approveRole = _approveRole.Where(x => x.Parameter1 == operationType).ToList();
            }
            return _approveRole.Select(x => new SelectedModel { Value = x.Code_ID, Name = x.Text }).ToList();
        }
        public static List<SelectedModel> GetApproveRoleR04()
        {
            List<AS_Code_Table> _approveRole = AS_Code_Table_Records.Find(x => x.Class == "R04").ToList();
            return _approveRole.Select(x => new SelectedModel { Value = x.Code_ID, Name = x.Text }).ToList();
        }
        public static List<SelectedModel> GetApproveRoleR05()
        {
            List<AS_Code_Table> _approveRole = AS_Code_Table_Records.Find(x => x.Class == "R05").ToList();
            return _approveRole.Select(x => new SelectedModel { Value = x.Code_ID, Name = x.Text }).ToList();
        }
        public static List<SelectedModel> GetApproveRoleR06()
        {
            List<AS_Code_Table> _approveRole = AS_Code_Table_Records.Find(x => x.Class == "R06").ToList();
            return _approveRole.Select(x => new SelectedModel { Value = x.Code_ID, Name = x.Text }).ToList();
        }
        public static List<SelectedModel> GetApproveRoleR07()
        {
            List<AS_Code_Table> _approveRole = AS_Code_Table_Records.Find(x => x.Class == "R07").ToList();
            return _approveRole.Select(x => new SelectedModel { Value = x.Code_ID, Name = x.Text }).ToList();
        }
        public static List<SelectedModel> GetApproveRoleR08()
        {
            List<AS_Code_Table> _approveRole = AS_Code_Table_Records.Find(x => x.Class == "R08").ToList();
            return _approveRole.Select(x => new SelectedModel { Value = x.Code_ID, Name = x.Text }).ToList();
        }
        public static List<SelectedModel> GetFlowCode(string operationType)
        {
            List<AS_Flow_Name> _flow = AS_Flow_Name_Records.FindAll().ToList();
            if (operationType != null && operationType != "")
            {
                _flow = _flow.Where(x => x.Flow_Type == operationType).ToList();
            }
            return _flow.Select(x => new SelectedModel { Value = x.ID.ToString(), Name = x.Flow_Code }).ToList();
        }
    }
}
