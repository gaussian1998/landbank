﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.Entity.Edmx;
using Library.Servant.Enums;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.Common
{
    public class LocalStaticDataServant : IStaticDataServant
    {
        public List<IDSelectedModel> getIDOptions(string Class)
        {
            ListModel _listModel = new ListModel();
            _listModel.ListIDSelectedModel = new List<IDSelectedModel>();
            _listModel.ListIDSelectedModel = StaticDataServant.getIDOptions(Class);
            return _listModel.ListIDSelectedModel;
        }


        public List<SelectedModel> getDocNames()
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.getDocNames();
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> getDocNames(string docType)
        {
            return StaticDataServant.getDocNames(docType);
        }
        public string GetStatusText(string code)
        {
            return StaticDataServant.GetStatusText(code);
        }

        public List<SelectedModel> GetBranchs() {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetBranchs();
            return _listModel.ListSelectedModel;
        }

        public List<SelectedModel> GetDepts()
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetDepts();
            return _listModel.ListSelectedModel;
        }

        public List<SelectedModel> GetAccountingBooks()
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetAccountingBooks();
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel>GetCurrencys()
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetCurrencys();
            return _listModel.ListSelectedModel;
        }

        public List<SelectedModel> GetTransTypes()
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetTransTypes();
            return _listModel.ListSelectedModel;
        }

        public List<SelectedModel> GetTradeTypes()
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetTradeTypes();
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetAssetMainKinds()
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetAssetMainKinds();
            return _listModel.ListSelectedModel;
        }

        public List<SelectedModel> GetAssetDetailKinds()
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetAssetDetailKinds();
            return _listModel.ListSelectedModel;
        }

        public List<SelectedModel> GetAssetUseTypes()
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetAssetUseTypes();
            return _listModel.ListSelectedModel;
        }

        public List<UserInfoOptionModel> GetAllUserInfo()
        {
            ListModel _listModel = new ListModel();
            _listModel.ListUserInfoOptionModel = new List<UserInfoOptionModel>();
            _listModel.ListUserInfoOptionModel = StaticDataServant.GetAllUserInfo();
            return _listModel.ListUserInfoOptionModel;
        }
        public List<SelectedModel> GetDocTypes()
        {
            return StaticDataServant.GetDocTypes();
        }
       
        public List<SelectedModel> GetUserTransactionTypes()
        {
            return StaticDataServant.GetUserTransactionTypes();
        }
        public List<SelectedModel> GetTradeTypes(string transType)
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetTradeTypes(transType);
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetApproveRole(string operationType)
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetApproveRole(operationType);
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetApproveRoleR04()
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetApproveRoleR04();
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetApproveRoleR05()
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetApproveRoleR05();
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetApproveRoleR06()
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetApproveRoleR06();
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetApproveRoleR07()
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetApproveRoleR07();
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetApproveRoleR08()
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetApproveRoleR08();
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetFlowCode(string operationType)
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = StaticDataServant.GetFlowCode(operationType);
            return _listModel.ListSelectedModel;
        }
    }
}
