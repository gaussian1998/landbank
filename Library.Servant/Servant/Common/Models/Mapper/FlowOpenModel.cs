﻿using Library.Servant.Communicate;
using System;

namespace Library.Servant.Servant.Common.Models.Mapper
{
    public class FlowOpenModel : InvasionEncryption
    {
        public int ID { get; set; }
        public string Doc_Type { protected get; set; }
        public string Doc_No { protected get; set; }
        public Nullable<int> Table_ID { protected get; set; }
        public Nullable<int> Last_ID { protected get; set; }
        public string Last_Reject_Memo { protected get; set; }
        public string Branch_Code { protected get; set; }
        public string Flow_Code { protected get; set; }
        public Nullable<decimal> Version { protected get; set; }
        public bool End_Point { protected get; set; }
        public decimal Flow_Status { protected get; set; }
        public Nullable<decimal> Gate_Others { protected get; set; }
        public Nullable<bool> Gate_Pass { protected get; set; }
        public Nullable<decimal> Result { protected get; set; }
        public string Audit_Opinion { protected get; set; }
        public string Current_Role_ID { protected get; set; }
        public Nullable<int> Current_User_ID { protected get; set; }
        public Nullable<decimal> Agent_User_ID { protected get; set; }
        public string Reject_Role_ID { protected get; set; }
        public string Reject_User_ID { protected get; set; }
        public Nullable<int> Step_Role_ID { protected get; set; }
        public Nullable<int> Step_User_ID { protected get; set; }
        public string Form_Name { protected get; set; }
        public Nullable<decimal> Form_Version { protected get; set; }
        public Nullable<decimal> Mgr_Branch_Code { protected get; set; }
        public string Mgr_Sub_Depart { protected get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { protected get; set; }
        public Nullable<decimal> Last_Updated_By { protected get; set; }
        public string Form_No { protected get; set; }
        public Nullable<decimal> Step_No { protected get; set; }
        public System.DateTime Issue_Time { protected get; set; }
        public string Event_Listener { protected get; set; }
        public string Carry { protected get; set; }
        public int Apply_User_ID { protected get; set; }
        public int Agent_Flow_ID { protected get; set; }
    }
}
