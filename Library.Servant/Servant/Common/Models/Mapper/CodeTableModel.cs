﻿using Library.Servant.Communicate;
using System;

namespace Library.Servant.Servant.Common.Models.Mapper
{
    public class CodeTableModel : InvasionEncryption
    {
        public int ID { get; set; }
        public string Class { get; set; }
        public string Class_Name { protected get; set; }
        public string Code_ID { protected get; set; }
        public string Text {  get; set; }
        public bool Is_Active { protected get; set; }
        public bool Cancel_Code {  get; set; }
        public string Parameter1 {  get; set; }
        public string Parameter2 {  get; set; }
        public string Remark {  get; set; }
        public Nullable<decimal> Value1 {  get; set; }
        public Nullable<decimal> Value2 {  get; set; }
        public Nullable<decimal> Value3 {  get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { protected get; set; }
        public Nullable<int> Last_Updated_By { protected get; set; }
    }
}
