﻿using Library.Servant.Communicate;
using System;

namespace Library.Servant.Servant.Common.Models.Mapper
{
    public class FlowNameModel : InvasionEncryption
    {
        public int ID { get; set; }
        public string Flow_Code { protected get; set; }
        public string Flow_Type { protected get; set; }
        public string Flow_Name { protected get; set; }
        public bool Is_Active { protected get; set; }
        public bool Cancel_Code { protected get; set; }
        public string Remark { get; set; }
        public DateTime? Last_Updated_Time { protected get; set; }
        public decimal? Last_Updated_By { protected get; set; }
    }
}
