﻿using Library.Servant.Communicate;
using System;

namespace Library.Servant.Servant.Common.Models.Mapper
{
    public class UserModel : InvasionEncryption
    {
        public int ID { protected get; set; }
        public string User_Code { protected get; set; }
        public string User_Name { protected get; set; }
        public string Branch_Code { protected get; set; }
        public string Branch_Name { protected get; set; }
        public string Department_Code { protected get; set; }
        public string Department_Name { protected get; set; }
        public Nullable<int> Department_ID { protected get; set; }
        public string Office_Branch { protected get; set; }
        public Nullable<bool> Is_Active { protected get; set; }
        public Nullable<System.DateTime> Quit_Date { protected get; set; }
        public string Email { protected get; set; }
        public string Notes { protected get; set; }
        public Nullable<int> Created_By { protected get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { protected get; set; }
        public Nullable<int> Last_Updated_By { protected get; set; }
        public Nullable<bool> Delete_Flag { protected get; set; }
    }
}
