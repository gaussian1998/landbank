﻿using Library.Servant.Communicate;
using System;

namespace Library.Servant.Servant.Common.Models.Mapper
{
    public class GLAccMonthsModel : InvasionEncryption
    {
        public int ID { protected get; set; }
        public string Book_Type { protected get; set; }
        public string Branch_Code { protected get; set; }
        public string Department { protected get; set; }
        public string Branch_Ind { protected get; set; }
        public string Account { protected get; set; }
        public string Account_Name { protected get; set; }
        public string Asset_Liablty { protected get; set; }
        public string Category { protected get; set; }
        public string Currency { protected get; set; }
        public string GL_Account { protected get; set; }
        public decimal Master_No { protected get; set; }
        public System.DateTime Open_Date { protected get; set; }
        public string Open_Type { protected get; set; }
        public string Trans_Type { protected get; set; }
        public string Trade_Type { protected get; set; }
        public string Account_Key { protected get; set; }
        public string Trans_Seq { protected get; set; }
        public int Post_Seq { protected get; set; }
        public string DB_CR { protected get; set; }
        public string Real_Memo { protected get; set; }
        public Nullable<System.DateTime> Stat_Balance_Date { protected get; set; }
        public string Status { protected get; set; }
        public string Asset_Number { protected get; set; }
        public string Lease_Number { protected get; set; }
        public string Virtual_Account { protected get; set; }
        public string Payment_User { protected get; set; }
        public string Payment_User_ID { protected get; set; }
        public string Batch_Seq { protected get; set; }
        public string Import_Number { protected get; set; }
        public bool Soft_Lock { protected get; set; }
        public Nullable<System.DateTime> Close_Date { protected get; set; }
        public decimal Stat_Frequency { protected get; set; }
        public Nullable<System.DateTime> Post_Date { protected get; set; }
        public decimal Amount { protected get; set; }
        public Nullable<System.DateTime> Value_Date { protected get; set; }
        public Nullable<System.DateTime> Settle_Date { protected get; set; }
        public string Notes { protected get; set; }
        public string Notes1 { protected get; set; }
        public System.DateTime Last_Updated_Time { protected get; set; }
        public decimal Last_Updated_By { protected get; set; }
        public bool Is_Updated { protected get; set; }
        public string Updated_Notes { protected get; set; }
        public Nullable<System.DateTime> EAI_Time { protected get; set; }
        public string EAI_Seq { protected get; set; }
        public string Deprn_Months { protected get; set; }
    }
}
