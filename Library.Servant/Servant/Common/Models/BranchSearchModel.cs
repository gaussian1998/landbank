﻿using Library.Common.Utility;
using Library.Entity.Edmx;
using Library.Servant.Communicate;
using System;
using System.Linq.Expressions;

namespace Library.Servant.Servant.Common.Models
{
    public class BranchSearchModel : InvasionEncryption
    {
        public int UserID { get; set; }
        public string BranchCode { get; set; }
        public string DepartmentCode { get; set; }
        public bool ThisMonth { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int ReportWidth { get; set; }


        public Expression<Func<AS_GL_Acc_Months, bool>> Expression()
        {
            bool IsSkipBranch = string.IsNullOrWhiteSpace(BranchCode);
            bool IsSkipDepartment = string.IsNullOrWhiteSpace(DepartmentCode);

            return ExpressionMaker.Make<AS_GL_Acc_Months, bool>(

                entity =>
                        (IsSkipBranch || entity.Branch_Code == BranchCode) &&
                        (IsSkipDepartment || entity.Department == DepartmentCode)
            );
        }
    }
}
