﻿using System.Collections.Generic;

namespace Library.Servant.Servant.Common.Models
{
    public class FlowSearchOptionsModel
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public List<OptionModel<int, string>> FlowStateOptions;
        public List<OptionModel<string, string>> BranchOptions;
        public List<OptionModel<string, string>> DepartmentOptions;
        public List<OptionModel<int, string>> ApplyUserOptions;
        public List<OptionModel<int, string>> CurrentApprovalUserOptions;
        public List<OptionModel<string, string>> DocTypeOptions;
    }
}
