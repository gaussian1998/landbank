﻿using System;
namespace Library.Servant.Servant.Common.Models
{
    public class RangeModel<T>
    {
        public T Start { get; set; }
        public T End { get; set; }
    }
}