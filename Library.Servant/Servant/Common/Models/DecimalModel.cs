﻿using Library.Servant.Communicate;

namespace Library.Servant.Servant.Common.Models
{
    public class DecimalModel : InvasionEncryption
    {
        public decimal Value { get; set; }
    }
}
