﻿
using Library.Servant.Communicate;

namespace Library.Servant.Servant.Common.Models
{
    public class OptionModel<Key,Name> : InvasionEncryption
    {
        public Key Value { get; set; }
        public Name Text { get; set; }
    }
}

