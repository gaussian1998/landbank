﻿using Library.Servant.Communicate;
using System.Collections.Generic;

namespace Library.Servant.Servant.Common.Models
{
    public class ListOptionModel<Key, Name> : InvasionEncryption
    {
       public List<OptionModel<Key, Name>> ListOptionModelItems { get; set; }
    }
}
