﻿using Library.Servant.Communicate;

namespace Library.Servant.Servant.Common.Models
{
    public class ByteModel : InvasionEncryption
    {
        public byte[] Value { get; set; }
    }
}
