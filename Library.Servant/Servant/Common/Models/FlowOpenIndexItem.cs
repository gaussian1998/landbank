﻿using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalTodo.Constants;
using Library.Servant.Servant.Common.Models.Mapper;
using System.Collections.Generic;
using System.Linq;

namespace Library.Servant.Servant.Common.Models
{
    public class FlowOpenIndexItem : FlowOpenModel
    {
        public AS_Agent_Flow AS_Agent_Flow
        {
            set
            {
                DocTypeCodeID = value.AS_Doc_Name.Doc_Type;
                DocTypeName = value.AS_Doc_Name.Doc_Type_Name;
                DocName = value.AS_Doc_Name.Doc_Name;
                FlowTemplateName = value.AS_Flow_Template.AS_Flow_Name.Flow_Name + "[版本" + value.AS_Flow_Template.Version + "]";
                FlowRoles = value.AS_Flow_Template.AS_Flow_Template_Flow_Role.Select(m => m.AS_Code_Table.Text).ToList();
            }
        }
        public string DocTypeCodeID { get; set; }
        public string DocTypeName { get; set; }
        public string DocName { get; set; }
        public string FlowTemplateName { get; set; }
        public List<string> FlowRoles { get; set; }

        public AS_Users AS_Users
        {
            set
            {
                if (value == null)
                    return;

                ApplyUserName = value.User_Name;
                BranchCode = value.Branch_Code;
                BranchName = value.Branch_Name;
                DepartmentCode = value.Department_Code;
                DepartmentName = value.Department_Name;
            }
        }
        public string ApplyUserName { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }

        public AS_Users AS_Users1
        {
            set
            {
                if (value == null)
                    return;
                CurrentApprovalUserName = value.User_Name;
            }
        }
        public string CurrentApprovalUserName { get; set; }

        public AS_Users AS_Users2
        {
            set
            {
                if (value == null)
                    return;
                NextApprovalUser = value.User_Name;
            }
        }
        public string NextApprovalUser { get; set; }

        public Dictionary<int, string> FlowRoleIdDictionary
        {
            set
            {
                if (NextApprovalFlowRoleID.HasValue && value.ContainsKey(NextApprovalFlowRoleID.Value)  )
                    NextApprovalFlowRole = value[NextApprovalFlowRoleID.Value];
            }
        }
        public string NextApprovalFlowRole { get; set; }

        public string FormNo
        {
            get
            {
                return Form_No;
            }
            set
            {
                Form_No = value;
            }
        }

        public System.DateTime IssueTime
        {
            get
            {
                return Issue_Time;
            }
            set
            {
                Issue_Time = value;
            }
        }

        public int ApplyUserID
        {
            get
            {
                return Apply_User_ID;
            }
            set
            {
                Apply_User_ID = value;
            }
        }

        public int? CurrentApprovalUserID
        {
            get
            {
                return Current_User_ID;
            }
            set
            {
                Current_User_ID = value;
            }
        }
        public int? NextApprovalFlowRoleID
        {
            get
            {
                return Step_Role_ID;
            }
            set
            {
                Step_Role_ID = value;
            }
        }

        public decimal FlowStatus
        {
            get
            {
                return Flow_Status;
            }
            set
            {
                Flow_Status = value;
            }
        }

        public string NextApproval
        {
            get
            {
                return string.IsNullOrEmpty(NextApprovalUser) ? NextApprovalFlowRole : NextApprovalUser + "(改派)";
            }
        }

        public string ApprovalStatus
        {
            get
            {
                return FlowStateServant.Text(FlowStatus);
            }
        }

        public string Remark { get; set; }
    }
}
