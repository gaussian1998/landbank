﻿using Library.Servant.Communicate;

namespace Library.Servant.Servant.Common.Models
{
    public class BoolResult : InvasionEncryption
    {
        public bool Value { get; set; }
        public static implicit operator bool(BoolResult result)
        {
            return result.Value;
        }
    }

    public static class BoolExtension
    {
        public static string Stringify(this bool result)
        {
            return new BoolResult { Value = result }.Stringify();
        }
    }
}
