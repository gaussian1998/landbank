﻿using Library.Common.Interface;

namespace Library.Servant.Servant.Common.Models
{
    public class AbstractPageModel<SubType> : IValidator
    {
        public string SerialNo { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public AbstractPageModel()
        {
            SerialNo = typeof(SubType).FullName;
            Page = 1;
            PageSize = 20;
        }

        public bool IsValid()
        {
            return SerialNo == typeof(SubType).FullName;
        }
    }
}
