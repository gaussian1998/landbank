﻿using Library.Servant.Communicate;

namespace Library.Servant.Servant.Common.Models
{
    public class IntModel : InvasionEncryption
    {
        public int Value { get; set; }
    }
}
