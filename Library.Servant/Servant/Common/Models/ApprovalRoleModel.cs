﻿namespace Library.Servant.Servant.Common.Models
{
    public class ApprovalRoleModel
    {
        public int ID { get; set; }
        public string CodeID { get; set; }
        public string Text { get; set; }
    }
}
