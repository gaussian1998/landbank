﻿using System;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;


namespace Library.Servant.Servant.Common
{
    public class GuidModel : AbstractEncryptionDTO
    {
        public Guid GUID { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
