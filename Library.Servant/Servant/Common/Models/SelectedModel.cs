﻿namespace Library.Servant.Servant.Common.Models
{
    public class SelectedModel
    {
        public string Value { get; set; }
        public string Name { get; set; }
        public string Extend { get; set; }
    }
}
