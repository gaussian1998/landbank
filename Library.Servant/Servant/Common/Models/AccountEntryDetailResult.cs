﻿using System;
using System.Collections.Generic;
using Library.Common.Extension;

namespace Library.Servant.Servant.Common.Models
{
    public partial class AccountEntryDetailResult
    {
        public int ID { get; set; }
        public string Book_Type { get; set; }
        public string Branch_Code { get; set; }
        public string Department { get; set; }
        public string Branch_Ind { get; set; }
        public string Account { get; set; }
        public string Account_Name { get; set; }
        public string Asset_Liablty { get; set; }
        public string Category { get; set; }
        public string Currency { get; set; }
        public string GL_Account { get; set; }
        public decimal Master_No { get; set; }
        public System.DateTime Open_Date { get; set; }
        public string Open_Type { get; set; }
        public string Trans_Type { get; set; }
        public string Trade_Type { get; set; }
        public string Account_Key { get; set; }
        public string Trans_Seq { get; set; }
        public int Post_Seq { get; set; }
        public string DB_CR { get; set; }
        public string Real_Memo { get; set; }
        public Nullable<System.DateTime> Stat_Balance_Date { get; set; }
        public string Status { get; set; }
        public string Asset_Number { get; set; }
        public string Lease_Number { get; set; }
        public string Virtual_Account { get; set; }
        public string Payment_User { get; set; }
        public string Payment_User_ID { get; set; }
        public string Batch_Seq { get; set; }
        public string Import_Number { get; set; }
        public bool Soft_Lock { get; set; }
        public Nullable<System.DateTime> Close_Date { get; set; }
        public decimal Stat_Frequency { get; set; }
        public Nullable<System.DateTime> Post_Date { get; set; }
        public decimal Amount { get; set; }
        public Nullable<System.DateTime> Value_Date { get; set; }
        public Nullable<System.DateTime> Settle_Date { get; set; }
        public string Notes { get; set; }
        public string Notes1 { get; set; }
        public System.DateTime Last_Updated_Time { get; set; }
        public decimal Last_Updated_By { get; set; }
        public bool Is_Updated { get; set; }
        public string Updated_Notes { get; set; }
    }

    public partial class AccountEntryDetailResult
    {
        public int TotalNumber { get; set; }

        public string CreditAmount
        {
            get
            {
                return IsCredit() ? Amount.Money() : string.Empty;
            }
        }
        public string DebitAmount
        {
            get
            {
                return IsCredit() ? string.Empty : Amount.Money();
            }
        }

        public Dictionary<string, string> Branchs
        {
            set
            {
                if (value.ContainsKey(Branch_Code))
                    BranchName = value[Branch_Code];
                else
                    BranchName = Branch_Code;
            }
        }
        public string BranchName { get; set; }

        public Dictionary<string, string> Departments
        {
            set
            {
                if (value.ContainsKey(Department))
                    DepartmentName = value[Department];
                else
                    DepartmentName = Department;
            }
        }
        public string DepartmentName { get; set; }

        private bool IsCredit()
        {
            return DB_CR == "C";
        }
    }
}
