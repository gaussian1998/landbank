﻿using Library.Servant.Communicate;
using System.Collections.Generic;

namespace Library.Servant.Servant.Common.Models
{
    public class ListModel : InvasionEncryption
    {
       public List<SelectedModel> ListSelectedModel { get; set; }
       public List<IDSelectedModel> ListIDSelectedModel { get; set; }
       public List<UserInfoOptionModel> ListUserInfoOptionModel { get; set; }
    }
}
