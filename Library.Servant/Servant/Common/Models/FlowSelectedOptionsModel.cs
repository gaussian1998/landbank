﻿

using Library.Common.Utility;
using System;
using System.Collections.Generic;

namespace Library.Servant.Servant.Common.Models
{
    public class FlowSelectedOptionsModel
    {
        public int? FlowStateCodeID { get; set; }
        public string BranchCodeID { get; set; }
        public string DepartmentCodeID { get; set; }
        public int? ApplyUserID { get; set; }
        public int? CurrentApprovalUserID { get; set; }
        public string DocTypeCodeID { get; set; }

        public  Func<FlowOpenIndexItem, bool> Expression()
        {
            bool isSkipFlowState = !FlowStateCodeID.HasValue;
            bool isSkipBranch = string.IsNullOrWhiteSpace(BranchCodeID);
            bool isSkipDepartment = string.IsNullOrWhiteSpace(DepartmentCodeID);
            bool isSkipApplyUser = !ApplyUserID.HasValue;
            bool isSkipCurrentApprovalUser = !CurrentApprovalUserID.HasValue;
            bool isSkipDocType = string.IsNullOrWhiteSpace(DocTypeCodeID);

            return ExpressionMaker.MakeF<FlowOpenIndexItem, bool>(open =>

                 (isSkipFlowState || FlowStateCodeID == open.FlowStatus) &&
                 (isSkipBranch || BranchCodeID == open.BranchCode) &&
                 (isSkipDepartment || DepartmentCodeID == open.DepartmentCode) &&
                 (isSkipApplyUser || ApplyUserID == open.ApplyUserID) &&
                 (isSkipCurrentApprovalUser || CurrentApprovalUserID == open.CurrentApprovalUserID) &&
                 (isSkipDocType || DocTypeCodeID == open.DocTypeCodeID)
            );
        }
    }
}
