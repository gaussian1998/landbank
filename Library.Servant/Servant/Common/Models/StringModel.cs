﻿using Library.Servant.Communicate;

namespace Library.Servant.Servant.Common.Models
{
    public class StringModel : InvasionEncryption
    {
        public string Value { get; set; }
        public override string ToString()
        {
            return Value;
        }
    }
}
