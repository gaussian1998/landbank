﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Servant.Common.Models
{
    public class DictionaryResult : AbstractEncryptionDTO
    {
        public Dictionary<string, List<string>> Value { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
