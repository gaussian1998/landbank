﻿namespace Library.Servant.Servant.Common.Models
{
    public class IDSelectedModel
    {
        public string Value { get; set; }
        public string Name { get; set; }
    }
}
