﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Common.Models
{
    public class UserInfoOptionModel
    {
        public int UserID { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string BranchCode { get; set; }
        public string DepartmentCode { get; set; }
    }
}
