﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.Common.Models
{
    public class ListRemoteModel<T> : InvasionEncryption
    {
        public List<T> Content;
        
        public ListRemoteModel()
        {
            Content = new List<T>();
        }
    }
}
