﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.Common.Models
{
    public class MessageModel : InvasionEncryption
    {
        public bool IsSuccess { get; set; }
        public string MessagCode { get; set; }
        public string Message { get; set; }
    }
}
