﻿using System.Collections.Generic;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.Common
{
    public interface IStaticDataServant
    {
        List<SelectedModel> GetAccountingBooks();
        List<UserInfoOptionModel> GetAllUserInfo();
        List<SelectedModel> GetAssetDetailKinds();
        List<SelectedModel> GetAssetMainKinds();
        List<SelectedModel> GetAssetUseTypes();
        List<SelectedModel> GetBranchs();
        List<SelectedModel> GetCurrencys();
        List<SelectedModel> GetDepts();
        List<SelectedModel> getDocNames();
        List<SelectedModel> getDocNames(string docType);
        List<IDSelectedModel> getIDOptions(string Class);
        string GetStatusText(string code);
        List<SelectedModel> GetTradeTypes();
        List<SelectedModel> GetTransTypes();
        List<SelectedModel> GetDocTypes();
       
        List<SelectedModel> GetUserTransactionTypes();
        List<SelectedModel> GetTradeTypes(string transType);
        List<SelectedModel> GetApproveRole(string operationType);
        List<SelectedModel> GetApproveRoleR04();
        List<SelectedModel> GetApproveRoleR05();
        List<SelectedModel> GetApproveRoleR06();
        List<SelectedModel> GetApproveRoleR07();
        List<SelectedModel> GetApproveRoleR08();
        List<SelectedModel> GetFlowCode(string operationType);
    }
}