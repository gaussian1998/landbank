﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Library.Servant.Communicate;
using Library.Utility;
using Newtonsoft.Json;


namespace Library.Servant.Servant
{
    public static class ProtocolService
    {
        static ProtocolService()
        {
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback( (p1,p2,p3,p4) => true );
        }

        public static Optional<R> EncryptionPost<T, R>(T model, string url, int timeOutSec = 90)
            where T : AbstractEncryptionDTO
            where R : AbstractEncryptionDTO, new()
        {
            try
            {
                HttpWebRequest request = HttpWebRequest.Create(url) as HttpWebRequest;
                request.Timeout = timeOutSec * 1000;
                return request.Post(model.UTF8()).EncryptionRead<R>();
            }
            catch (Exception ex)
            {
                return Optional<R>.Error(ex.Message);
            }
        }
        public static Optional<R> EncryptionPosttext<T, R>(T model, string url, int timeOutSec = 90)
           where T : AbstractEncryptionDTO
           where R : AbstractEncryptionDTO, new()
        {
            try
            {
                HttpWebRequest request = HttpWebRequest.Create(url) as HttpWebRequest;
                request.Timeout = timeOutSec * 1000;
                return request.Posttext(model.UTF8()).EncryptionRead<R>();
            }
            catch (Exception ex)
            {
                return Optional<R>.Error(ex.Message);
            }
        }

        public static Optional<R> PostBinary<R>(byte[] data, string url, Action<HttpWebRequest> modify)
           where R : AbstractEncryptionDTO, new()
        {
            try
            {
                HttpWebRequest request = HttpWebRequest.Create(url) as HttpWebRequest;
                modify(request);
                return request.Post( data, request.ContentType).EncryptionRead<R>();
            }
            catch (Exception ex)
            {
                return Optional<R>.Error(ex.Message);
            }
        }

        public static string TextGet(string url, int timeOutSec = 90)
        {
            WebResponse response = Get(url, timeOutSec);

            return Encoding.UTF8.GetString(response.ReadBody());
        }

        public static T JsonGet<T>(string url, int timeOutSec = 90)
        {
            string json = TextGet(url, timeOutSec);

            return JsonConvert.DeserializeObject<T>(json);
        }

        public static WebResponse Post(this HttpWebRequest request, byte[] body, string ContentType= "application/json ; charset=utf-8")
        {
            request.Method = "post";
            request.ContentType = ContentType;
            request.ContentLength = body.Length;
            using (var stream = request.GetRequestStream())
            {
                stream.Write(body, 0, body.Length);
            }

            return request.GetResponse();
        }

        public static WebResponse Posttext(this HttpWebRequest request, byte[] body, string ContentType = "application/text ; charset=utf-8")
        {
            request.Method = "post";
            request.ContentType = ContentType;
            request.ContentLength = body.Length;
            using (var stream = request.GetRequestStream())
            {
                stream.Write(body, 0, body.Length);
            }

            return request.GetResponse();
        }
        private static WebResponse Get(string url, int timeOutSec = 90)
        {
            HttpWebRequest request = HttpWebRequest.Create(url) as HttpWebRequest;
            request.Timeout = timeOutSec * 1000;
            request.Method = "get";

            return request.GetResponse();
        }

        private static Optional<T> EncryptionRead<T>(this WebResponse response)
            where T : AbstractEncryptionDTO, new()
        {
            return WebOptional.Deserialize<T>(response.ReadBody());
        }

        /// <summary>
        ///  新介面
        /// </summary>
        public static byte[] ReadBody(this WebResponse response)
        {
            using (var stream = response.GetResponseStream())
            {
                return stream.ReadToEnd();
            }
        }

        public static byte[] ReadToEnd(this Stream stream)
        {
            byte[] buffer = new byte[4096];
            using (MemoryStream ms = new MemoryStream())
            {
                int read = 0;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                    ms.Write(buffer, 0, read);

                return ms.ToArray();
            }
        }
    }
}
