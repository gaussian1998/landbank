﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Transactions;
using Library.Interface;

namespace Library.Servant.Servant.Common
{
    /// <summary>
    ///  上傳的檔案通常會關聯至資料庫,這裡展示一種和資料庫的互動方法
    ///   設計需考慮到交易問題
    ///   若且唯若資料庫和檔案都儲存成功,否則都失敗
    /// </summary>
    public static class FakeFileServant
    {
        public static long Create(Func<long, string> save)
        {
            using (TransactionScope trans = new TransactionScope())
            {
                var Id = FakeDB.Create();
                FakeDB.Update( Id, save(Id));

                trans.Complete();
                return Id;
            }
        }

        public static void Delete(this IStorageService stroage, long Id)
        {
            using (TransactionScope trans = new TransactionScope())
            {
                stroage.Remove( FakeDB.Delete(Id) );

                trans.Complete();
            }
        }

        public static List<FakeFile> FindAll()
        {
            return FakeDB.FindAll();
        }
    }

    public class FakeFile
    {
        public long Id { get; set; }
        public string Filename { get; set; }
    }


    /// <summary>
    ///  假的資料庫,暫時的
    ///  真實的資料庫有一定的機率會失敗
    /// </summary>
    static class FakeDB
    {
        public static List<FakeFile> FindAll()
        {
            var query = from pair in table
            select new FakeFile { Id = pair.Key, Filename = pair.Value };

            return query.ToList();
        }

        public static long Create()
        {
            table.Add(++nextId, "");
            return nextId;
        }

        public static void Update(long Id, string filename)
        {
            table[Id] = filename;
        }

        public static string Delete(long Id)
        {
            string filename = table[Id];
            table.Remove(Id);

            return filename;
        }

        private static readonly Dictionary<long, string> table = new Dictionary<long, string>();
        private static long nextId = 0;
    }
}
