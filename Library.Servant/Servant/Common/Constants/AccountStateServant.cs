﻿namespace Library.Servant.Servant.Common.Constants
{
    public static class AccountStateServant
    {
        public const string Delete = "-";

        /// <summary>
        ///  TODO: 試算鎖
        /// </summary>
        public const string New = "";
        public const string PrepareApproval = "0";

        /// <summary>
        ///  TODO: 簽核鎖
        /// </summary>
        public const string Approvaling = "S";
        public const string PrepareEAI = "1";

        /// <summary>
        ///  TODO: 拋轉鎖
        /// </summary>
        public const string EAIing = "A";
        public const string AfterEAI = "2";

        /// <summary>
        ///  End OF Transacation
        /// </summary>
        public const string End = "E";
    }
}
