﻿using Library.Servant.Common;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.Common
{
    public static class RemoteServant
    {
        public static R Post<T,R>(T model, string Url)
            where T : AbstractEncryptionDTO
            where R : AbstractEncryptionDTO, new()
        {
            var result = ProtocolService.EncryptionPost<T, R>
            (
                model,
                Config.ServantDomain + Url
            );

            return Tools.ExceptionConvert(result);
        }
        public static R Posttext<T, R>(T model, string Url)
            where T : AbstractEncryptionDTO
            where R : AbstractEncryptionDTO, new()
        {
            var result = ProtocolService.EncryptionPosttext<T, R>
            (
                model,
                Config.ServantDomain + Url
            );

            return Tools.ExceptionConvert(result);
        }
    }
}
