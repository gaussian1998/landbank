﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.Entity.Edmx;
using Library.Servant.Enums;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.Common
{
    public class RemoteStaticDataServant : IStaticDataServant
    {
        //private static string DocTypeClass = "DocType";

        public List<IDSelectedModel> getIDOptions(string Class)
        {
            ListModel _listModel = new ListModel();
            _listModel=RemoteServant.Post<StringModel, ListModel>(
                           new Models.StringModel { Value= Class },
                           "/StaticDataServant/getIDOptions"
          );
            return _listModel.ListIDSelectedModel;
        }


        public List<SelectedModel> getDocNames()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/getDocNames"
          );
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> getDocNames(string docType)
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<StringModel, ListModel>(
                           new StringModel {  Value= docType },
                           "/StaticDataServant/getDocNamesByDocType"
          );
            return _listModel.ListSelectedModel;
        }
        public string GetStatusText(string code)
        {
            StringModel _model = new StringModel();
            _model = (RemoteServant.Post<StringModel, StringModel>(
                           new StringModel { Value= code },
                           "/StaticDataServant/GetStatusText"
          ));
            return _model.Value;
        }

        public  List<SelectedModel> GetBranchs() {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetBranchs"
          );
            return _listModel.ListSelectedModel;
        }

        public  List<SelectedModel> GetDepts()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetDepts"
          );
            return _listModel.ListSelectedModel;
        }

        public  List<SelectedModel> GetAccountingBooks()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetAccountingBooks"
          );
            return _listModel.ListSelectedModel;
        }
        public  List<SelectedModel> GetCurrencys()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetCurrencys"
          );
            return _listModel.ListSelectedModel;
        }

        public  List<SelectedModel> GetTransTypes()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetTransTypes"
          );
            return _listModel.ListSelectedModel;
        }

        public  List<SelectedModel> GetTradeTypes()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetTradeTypes"
          );
            return _listModel.ListSelectedModel;
        }
        public  List<SelectedModel> GetAssetMainKinds()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetAssetMainKinds"
          );
            return _listModel.ListSelectedModel;
        }

        public  List<SelectedModel> GetAssetDetailKinds()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetAssetDetailKinds"
          );
            return _listModel.ListSelectedModel;
        }

        public  List<SelectedModel> GetAssetUseTypes()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetAssetUseTypes"
          );
            return _listModel.ListSelectedModel;
        }

        public  List<UserInfoOptionModel> GetAllUserInfo()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetAllUserInfo"
          );
            return _listModel.ListUserInfoOptionModel;
        }
        public List<SelectedModel> GetDocTypes()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetDocTypes"
          );
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetUserTransactionTypes()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetUserTransactionTypes"
          );
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetTradeTypes(string transType)
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<StringModel, ListModel>(
                           new Models.StringModel { Value = transType },
                           "/StaticDataServant/GetTradeTypesByTransType"
          );
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetApproveRole(string operationType)
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<StringModel, ListModel>(
                           new Models.StringModel { Value = operationType },
                           "/StaticDataServant/GetApproveRole"
          );
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetApproveRoleR04()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetApproveRoleR04"
          );
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetApproveRoleR05()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetApproveRoleR05"
          );
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetApproveRoleR06()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetApproveRoleR06"
          );
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetApproveRoleR07()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetApproveRoleR07"
          );
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetApproveRoleR08()
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel { },
                           "/StaticDataServant/GetApproveRoleR08"
          );
            return _listModel.ListSelectedModel;
        }
        public List<SelectedModel> GetFlowCode(string operationType)
        {
            ListModel _listModel = new ListModel();
            _listModel = RemoteServant.Post<StringModel, ListModel>(
                           new Models.StringModel { Value = operationType },
                           "/StaticDataServant/GetFlowCode"
          );
            return _listModel.ListSelectedModel;
        }
    }
}
