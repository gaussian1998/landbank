﻿using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Email.Models;

namespace Library.Servant.Servant.Email
{
    public interface IEmailServant
    {
        bool IsExist();
        StringModel Send(EmailModel model);
    }
}
