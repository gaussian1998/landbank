﻿using Library.Servant.Communicate;

namespace Library.Servant.Servant.Email.Models
{
    public class EmailModel : InvasionEncryption
    {
        public int UserID { get; set; }
        public int RecverID { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
