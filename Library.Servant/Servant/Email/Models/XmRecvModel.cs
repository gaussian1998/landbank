﻿public class EmailEcho
{
    public Bluestar BlueStar { get; set; }
}

public class Bluestar
{
    public string RqUid { get; set; }
    public string xmlns { get; set; }
    public string Status { get; set; }
    public string StatusCode { get; set; }
    public string StatusDesc { get; set; }
    public string BSNS_EventID { get; set; }
}


