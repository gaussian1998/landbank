﻿using System.Xml;
using Library.Servant.Servant.Email.Models;
using Newtonsoft.Json;
using Library.Servant.Servant.Common.ModelExtensions;
using Library.Servant.Servants.AbstractFactory;
using System;
using System.Net;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.Email
{
    public class LocalEmailServant : IEmailServant
    {
        public LocalEmailServant()
        {
            proxy = new MsgHandler();
            proxy.Url = ServantAbstractFactory.EmailServer();
        }

        public bool IsExist()
        {
            using (HttpWebResponse response = TrySend())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                    return true;
                else
                    return false;
            }
        }

        public StringModel Send(EmailModel model)
        {
            //Lance用本機去呼叫土銀EMail WebService加的
            //System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            try
            {
                string submit_xml = model.ToXml( ServantAbstractFactory.EmailDomain() );
                return new StringModel { Value = Deserialize(Submit(submit_xml)).BlueStar.StatusDesc };
            }
            catch(Exception ex)
            {
                return new StringModel { Value = "Exception" };
            }
        }

        private string Submit(string send_xml)
        {
            string result;
            proxy.SubmitXmlString( send_xml, out result);

            Console.WriteLine("result = " + result );
            return result;
        }

        private static EmailEcho Deserialize(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            string json = JsonConvert.SerializeXmlNode(doc);
            return JsonConvert.DeserializeObject<EmailEcho>(json);
        }

        private static HttpWebResponse TrySend()
        {
            HttpWebRequest request =
                WebRequest.Create(ServantAbstractFactory.EmailServer()) as HttpWebRequest;

            request.Timeout = 30000;
            return request.GetResponse() as HttpWebResponse;
        }

        private readonly MsgHandler proxy;
    }
}

