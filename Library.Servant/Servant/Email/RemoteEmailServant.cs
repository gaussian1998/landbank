﻿using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Email.Models;

namespace Library.Servant.Servant.Email
{
    public class RemoteEmailServant : IEmailServant
    {
        public bool IsExist()
        {
            return RemoteServant.Post<VoidModel, BoolResult>(

                new VoidModel { },
                "/EmailServant/IsExist"
           );
        }

        public StringModel Send(EmailModel model)
        {
            return RemoteServant.Post<EmailModel, StringModel>(

                model,
                "/EmailServant/Send"
            );
        }
    }
}
