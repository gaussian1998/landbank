﻿//Frontend和Servant溝通模式,二選一,只會影響 Web.Frontend
#define LocalServant
//#define RemoteServant
//#define Preview

//RemoteServant位置選擇,二選一,只會影響 Web.Frontend
#define Developer
//#define LandBank

//ADWS位置選擇,Remote模式時,只會影響 Web.Servant
#define LocalADWS
//#define LanceADWS
//#define LandBankTestADWS
//#define LandBankOnlineADWS

//Email Server位置選擇,Remote模式時,只會影響 Web.Servant
#define LocalEmail
//#define LandBankTestEmail
//#define LandBankOnlineEmail

//例外處理策略
#define DevelopExceptionPolicy
//#define OnlineExceptionPolicy

using Library.Servant.Servant.ApprovalMember;
using Library.Servant.Servant.ApprovalRole;
using Library.Servant.Servant.Authentication;
using Library.Servant.Servant.FunctionRole;
using Library.Servant.Servant.LoginRole;
using Library.Servant.Servant.UserConfig;
using Library.Servant.Servant.UserHistory;
using Library.Servant.Servant.UserRole;
using Library.Servant.Servant.WorkFlow;
using Library.Servant.Servant.Accounting;
using Library.Servant.Servant.PostsManagement;
using Library.Servant.Servant.Land;
using Library.Servant.Servant.MP;
using Library.Servant.Servant.Report;
using Library.Servant.Servant.TodoListManagement;
using Library.Servant.Servant.HistoryData;
using Library.Servant.Servant.CodeTableSetting;
using Library.Servant.Servant.Build;
using Library.Servant.Servant.AccountingBook;
using Library.Servant.Servant.AccountingCalendar;
using Library.Servant.Servant.ExchangeRate;
using Library.Servant.Servant.AccountingDeprnSetting;
using Library.Servant.Servant.Currency;
using Library.Servant.Servant.AccountingAssetParentRelation;
using Library.Servant.Servant.AssetCategory;
using Library.Servant.Servant.AccountingTradeAccount;
using Library.Servant.Servant.AccountingTradeDefine;
using Library.Servant.Servant.KeepPosition;
using Library.Servant.Servant.MU;
using Library.Servant.Servant.AssetLife;
using Library.Servant.Servant.Overdue;
using Library.Servant.Servant.AccountingImpairmGroup;
using Library.Servant.Servant.AccountingQueryDeprn;
using Library.Servant.Servant.Db;
using Library.Servant.Servant.Email;
using Library.Servant.Servant.AgentFlow;
using Library.Servant.Servant.ApprovalFlow;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.DocName;
using Library.Servant.Servant.AccountingEntryList;
using Library.Servant.Servant.AccountingEntryStatistics;
using Library.Servant.Servant.AccountingTradeTodayList;
using Library.Servant.Servant.AccountingTradeTodayStatistics;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Integration;
using Library.Servant.Servant.Activation;
using Library.Servant.Servant.Inspect;
using Library.Servant.Servant.Evaluation;
using Library.Servant.Servant.DossierRight;
using Library.Servant.Servant.LeaseRights;
using Library.Servant.Servant.QueryUserRole;


namespace Library.Servant.Servants.AbstractFactory
{
    public static class ServantAbstractFactory
    {

        public static string ServantDomain()
        {
#if Developer
            return "http://localhost:5292";
#endif

#if LandBank
            return "https://10.253.21.198";
#endif
        }

        public static IDocNameServant DocName()
        {
#if LocalServant
            return new LocalDocNameServant();
#endif

#if RemoteServant
            return new RemoteDocNameServant();
#endif

#if Preview
            return new RemoteDocNameServant();
#endif
        }

        public static IAccountingTradeTodayStatisticsServant AccountingTradeTodayStatistics()
        {
#if LocalServant
            return new LocalAccountingTradeTodayStatisticsServant();
#endif

#if RemoteServant
            return new RemoteAccountingTradeTodayStatisticsServant();
#endif

#if Preview
            return new RemoteAccountingTradeTodayStatisticsServant();
#endif
        }

        public static IAccountingTradeTodayListServant AccountingTradeTodayList()
        {
#if LocalServant
            return new LocalAccountingTradeTodayListServant();
#endif

#if RemoteServant
            return new RemoteAccountingTradeTodayListServant();
#endif

#if Preview
            return new RemoteAccountingTradeTodayListServant();
#endif
        }

        public static IAccountingEntryStatisticsServant AccountingEntryStatistics()
        {
#if LocalServant
            return new LocalAccountingEntryStatisticsServant();
#endif

#if RemoteServant
            return new RemoteAccountingEntryStatisticsServant();
#endif

#if Preview
            return new RemoteAccountingEntryStatisticsServant();
#endif
        }

        public static IAccountingEntryListServant AccountingEntryList()
        {
#if LocalServant
            return new LocalAccountingEntryListServant();
#endif

#if RemoteServant
            return new RemoteAccountingEntryListServant();
#endif

#if Preview
            return new LocalAccountingEntryListServant();
#endif
        }

        public static IApprovalTodoServant ApprovalTodo()
        {
#if LocalServant
            return new LocalApprovalTodoServant();
#endif

#if RemoteServant
            return new RemoteApprovalTodoServant();
#endif

#if Preview
            return new RemoteApprovalTodoServant();
#endif
        }

        public static IApprovalFlowServant ApprovalFlow()
        {
#if LocalServant
            return new LocalApprovalFlowServant();
#endif

#if RemoteServant
            return new RemoteApprovalFlowServant();
#endif

#if Preview
            return new RemoteApprovalFlowServant();
#endif
        }

        public static IAgentFlowServant AgentFlow()
        {
#if LocalServant
            return new LocalAgentFlowServant();
#endif

#if RemoteServant
            return new RemoteAgentFlowServant();
#endif

#if Preview
            return new RemoteAgentFlowServant();
#endif
    }

    public static IDbServant Db()
        {
#if LocalServant
            return new LocalDbServant();
#endif

#if RemoteServant
            return new RemoteDbServant();
#endif

#if Preview
            return new RemoteDbServant();
#endif
        }

        public static IEmailServant Email()
        {
#if LocalServant
            return new LocalEmailServant();
#endif

#if RemoteServant
            return new RemoteEmailServant();
#endif

#if Preview
            return new RemoteEmailServant();
#endif
        }

        public static IAccountingImpairmGroupServant AccountingImpairmGroupServant()
        {
#if LocalServant
            return new LocalAccountingImpairmGroupServant();
#endif

#if RemoteServant
            return new RemoteAccountingImpairmGroupServant();
#endif

#if Preview
            return new RemoteAccountingImpairmGroupServant();
#endif
        }

        public static IAssetLifeServant AssetLifeServant()
        {
#if LocalServant
            return new LocalAssetLifeServant();
#endif

#if RemoteServant
            return new RemoteAssetLifeServant();
#endif

#if Preview
            return new RemoteAssetLifeServant();
#endif
        }

        public static IKeepPositionServant KeepPosition()
        {
#if LocalServant
            return new LocalKeepPositionServant();
#endif

#if RemoteServant
            return new RemoteKeepPositionServant();
#endif

#if Preview
            return new RemoteKeepPositionServant();
#endif
        }


        public static IAccountingTradeDefineServant AccountingTradeDefine()
        {
#if LocalServant
            return new LocalAccountingTradeDefineServant();
#endif

#if RemoteServant
            return new RemoteAccountingTradeDefineServant();
#endif

#if Preview
            return new RemoteAccountingTradeDefineServant();
#endif
        }

        public static IAccountingTradeAccountSubServant AccountingTradeAccountSub()
        {
#if LocalServant
            return new LocalAccountingTradeAccountSubServant();
#endif

#if RemoteServant
            return new RemoteAccountingTradeAccountSubServant();
#endif

#if Preview
            return new RemoteAccountingTradeAccountSubServant();
#endif
        }

        public static IAccountingTradeAccountServant AccountingTradeAccount()
        {
#if LocalServant
            return new LocalAccountingTradeAccountServant();
#endif

#if RemoteServant
            return new RemoteAccountingTradeAccountServant();
#endif

#if Preview
            return new RemoteAccountingTradeAccountServant();
#endif
        }

        public static IAssetCategoryServant AssetCategory()
        {
#if LocalServant
            return new LocalAssetCategoryServant();
#endif

#if RemoteServant
            return new RemoteAssetCategoryServant();
#endif

#if Preview
            return new RemoteAssetCategoryServant();
#endif
        }

        public static IAccountingAssetParentRelationServant AccountingAssetParentRelation()
        {
#if LocalServant
            return new LocalAccountingAssetParentRelationServant();
#endif

#if RemoteServant
            return new RemoteAccountingAssetParentRelationServant();
#endif

#if Preview
            return new RemoteAccountingAssetParentRelationServant();
#endif
        }

        public static ICurrencyServant Currency()
        {
#if LocalServant
            return new LocalCurrencyServant();
#endif

#if RemoteServant
            return new RemoteCurrencyServant();
#endif

#if Preview
            return new RemoteCurrencyServant();
#endif
        }

        public static IAccountingDeprnSettingServant AccountingDeprnSetting()
        {
            return new LocalAccountingDeprnSettingServant();
        }

        public static IExchangeRateServant ExchangeRate()
        {
#if LocalServant
            return new LocalExchangeRateServant();
#endif

#if RemoteServant
            return new RemoteExchangeRateServant();
#endif

#if Preview
            return new RemoteExchangeRateServant();
#endif
        }

        public static IAccountingCalendarServant AccountingCalendar()
        {
#if LocalServant
            return new LocalAccountingCalendarServant();
#endif

#if RemoteServant
            return new RemoteAccountingCalendarServant();
#endif

#if Preview
            return new RemoteAccountingCalendarServant();
#endif
        }

        public static IAccountingQueryServant AccountingQuery()
        {
            return new LocalAccountingQueryServant();
        }
        public static IAccountingBookServant AccountingBook()
        {
#if LocalServant
            return new LocalAccountingBookServant();
#endif

#if RemoteServant
            return new RemoteAccountingBookServant();
#endif

#if Preview
            return new RemoteAccountingBookServant();
#endif
        }
        public static IStaticDataServant StaticData()
        {
#if LocalServant
            return new LocalStaticDataServant();
#endif

#if RemoteServant
            return new RemoteStaticDataServant();
#endif

#if Preview
            return new RemoteStaticDataServant();
#endif
        }


        public static ITradeDefineServant TradeDefine()
        {
            return new LocalTradeDefineServant();
        }

        public static IAccountingQueryDeprnServant AccountingQueryDeprn()
        {
            return new LocalAccountingQueryDeprnServant();
        }

        public static IAccountingDDLServant AccountingDDLServant()
        {
#if LocalServant
            return new LocalAccountingDDLServant();
#endif

#if RemoteServant
            return new RemoteAccountingDDLServant();
#endif

#if Preview
            return new LocalAccountingDDLServant();
#endif
        }

        public static IApprovalMemberServant ApprovalMember()
        {
#if LocalServant
            return new LocalApprovalMemberServant();
#endif

#if RemoteServant
            return new RemoteApprovalMemberServant();
#endif

#if Preview
            return new RemoteApprovalMemberServant();
#endif
        }

        public static IApprovalRoleServant ApprovalRole()
        {
#if LocalServant
            return new LocalApprovalRoleServant();
#endif

#if RemoteServant
            return new RemoteApprovalRoleServant();
#endif

#if Preview
            return new RemoteApprovalRoleServant();
#endif
        }

        public static IFucntionRoleServant FucntionRole()
        {
#if LocalServant
            return new LocalFunctionRoleServant();
#endif

#if RemoteServant
            return new RemoteFunctionRoleServant();
#endif

#if Preview
            return new LocalFunctionRoleServant();
#endif
        }

        public static ILoginRoleServant LoginRole()
        {
#if LocalServant
            return new LocalLoginRoleServant();
#endif

#if RemoteServant
            return new RemoteLoginRoleServant();
#endif

#if Preview
            return new LocalLoginRoleServant();
#endif
        }

        public static IUserRoleServant UserRole()
        {
#if LocalServant
            return new LocalUserRoleServant();
#endif

#if RemoteServant
            return new RemoteUserRoleServant();
#endif

#if Preview
            return new LocalUserRoleServant();
#endif
        }

        public static IQueryUserRole QueryUserRole()
        {
#if LocalServant
            return new LocalQueryUserRole();
#endif

#if RemoteServant
            return new LocalQueryUserRole();
#endif

#if Preview
            return new LocalQueryUserRole();
#endif
        }

        public static IPostsManagementServant PostsManagement()
        {
#if LocalServant
            return new LocalPostsManagementServant();
#endif

#if RemoteServant
            return new RemotePostsManagementServant();
#endif

#if Preview
            return new LocalPostsManagementServant();
#endif
        }
        
        public static IUserConfigServant UserConfig()
        {
#if LocalServant
            return new LocalUserConfigServant();
#endif

#if RemoteServant
            return new RemoteUserConfigServant();
#endif

#if Preview
            return new RemoteUserConfigServant();
#endif
        }

        public static IUserHistoryServant UserHistory()
        {
#if LocalServant
            return new LocalUserHistoryServant();
#endif

#if RemoteServant
            return new LocalUserHistoryServant();
#endif

#if Preview
            return new LocalUserHistoryServant();
#endif
        }

        public static IWorkFlowServant WorkFlow()
        {
#if LocalServant
            return new LocalWorkFlowServant();
#endif

#if RemoteServant
            return new RemoteWorkFlowServant();
#endif

#if Preview
            return new RemoteWorkFlowServant();
#endif
        }

        public static IAuthenticationServant Authentication()
        {
#if LocalServant
            return new LocalAuthenticationServant();
#endif

#if RemoteServant
            return new RemoteAuthenticationServant();
#endif

#if Preview
            return new RemoteAuthenticationServant();
#endif
        }
        
        public static string AdwsServer()
        {
#if LocalADWS
            return "http://localhost:12899/UserServiceEX.asmx";
#endif

#if LanceADWS
            return "http://landbankdemo.workway.com.tw/ADWS/UserServiceEX.asmx";
#endif

#if LandBankTestADWS
           return "https://adws.landbankt.com.tw/WebLdap/UserServiceEX.asmx";
#endif

#if LandBankOnlineADWS
            return "https://adws.landbank.com.tw/WebLdap/UserServiceEX.asmx";
#endif
        }


        public static IMPServant MP()
        {
#if LocalServant
            return new LocalMPServant();
#endif

#if RemoteServant
            return new RemoteMPervant();
#endif

#if Preview
            return new RemoteMPervant();
#endif
        }

        public static ILeaseRightsServant LeaseRights()
        {
#if LocalServant
            return new LocalLeaseRightsServant();
#endif

#if RemoteServant
            return new RemoteLeaseRightsServant();
#endif

#if Preview
            return new RemoteLeaseRightsServant();
#endif
        }

        public static ILandServant Land()
        {
#if LocalServant
            return new LocalLandServant();
#endif

#if RemoteServant
            return new RemoteLandServant();
#endif

#if Preview
            return new LocalLandServant();
#endif
        }

        public static IDossierRightServant DossierRight() //權狀--Added 2017-11-24 Shan
        {
#if LocalServant
            return new LocalDossierRightServant();
#endif

#if RemoteServant
            return new RemoteDossierRightServant();
#endif

#if Preview
            return new LocalDossierRightServant();
#endif
        }

        public static IBuildServant Build()
        {
#if LocalServant
            return new LocalBuildServant();
#endif

#if RemoteServant
            return new RemoteBuildServant();
#endif

#if Preview
            return new LocalBuildServant();
#endif
        }

        //Inspect
        public static IInspectServant Inspect()
        {
#if LocalServant
            return new LocalInspectServant();
#endif

#if RemoteServant
            return new RemoteInspectServant();
#endif

#if Preview
            return new LocalInspectServant();
#endif
        } //Evaluation
        public static IEvaluationServant Evaluation()
        {
#if LocalServant
            return new LocalEvaluationServant();
#endif

#if RemoteServant
            return new RemoteEvaluationServant();
#endif

#if Preview
            return new LocalEvaluationServant();
#endif
        }
        public static IMUServant MU()
        {
#if LocalServant
            return new LocalMUServant();
#endif

#if RemoteServant
            return new RemoteMUServant();
#endif

#if Preview
            return new LocalMUServant();
#endif
        }

        public static IReportServant Report()
        {
#if LocalServant
            return new LocalReportServant();
#endif

#if RemoteServant
            return new RemoteReportServant();
#endif

#if Preview
            return new LocalReportServant();
#endif
        }

        public static ITodoListManagement TodoList()
        {
#if LocalServant
            return new LocalTodoListManagement();
#endif

#if RemoteServant
            return new RemoteTodoListManagement();
#endif

#if Preview
            return new RemoteTodoListManagement();
#endif
        }
        
        public static IHistoryData HistoryData()
        {
#if LocalServant
            return new LocalHistoryData();
#endif

#if RemoteServant
            return new RemoteHistoryData();
#endif

#if Preview
            return new RemoteHistoryData();
#endif
        }

        public static ICodeTableSetting CodeTableSetting()
        {
#if LocalServant
            return new LocalCodeTableSetting();
#endif

#if RemoteServant
            return new RemoteCodeTableSetting();
#endif

#if Preview
            return new RemoteCodeTableSetting();
#endif
        }


        public static IOverdueServant Overdue()
        {
#if LocalServant
            return new LocalOverdueServant();
#endif

#if RemoteServant
            return new RemoteOverdueServant();
#endif

#if Preview
            return new LocalOverdueServant();
#endif
        }
        public static Library.Servant.Servant.AccountingSubject.IAccountingSubjectServant AccountingSubject()
        {
#if LocalServant
            return new Library.Servant.Servant.AccountingSubject.LocalAccountingSubjectServant();
#endif

#if RemoteServant
            return new Library.Servant.Servant.AccountingSubject.RemoteAccountingSubjectServant();
#endif

#if Preview
            return new Library.Servant.Servant.AccountingSubject.RemoteAccountingSubjectServant();
#endif
        }


        public static string EmailServer()
        {
#if LocalEmail
            return "http://localhost:2513/MsgHandler.asmx";
#endif

#if LandBankTestEmail
            return "https://cpportal.landbankt.com.tw/BlueStar/MsgHandler.asmx";
#endif

#if LandBankOnlineEmail
            return "https://cpportal.landbank.com.tw/BlueStar/MsgHandler.asmx";
#endif
        }

        public static string EmailDomain()
        {
#if LocalEmail
            return "landbankt.com.tw";
#endif

#if LandBankTestEmail
            return "landbankt.com.tw";
#endif

#if LandBankOnlineEmail
            return "landbank.com.tw";
#endif
        }

        public static bool ShowDetailsException()
        {
#if DevelopExceptionPolicy
            return true;
#endif

#if OnlineExceptionPolicy
            return false;
#endif
        }

        public static IIGServant Integration()
        {
#if LocalServant
            return new LocalIGServant();
#endif

#if RemoteServant
            return new RemoteIGServant();
#endif

#if Preview
            return new RemoteIGServant();
#endif
        }

        public static IActivationServant Activation()
        {
#if LocalServant
            return new LocalActivationServant();
#endif

#if RemoteServant
            return new RemoteActivationServant();
#endif

#if Preview
            return new RemoteActivationServant();
#endif
        }
    }
}