﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingBook.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.AccountingBook
{
    public interface IAccountingBookServant
    {
        IndexResult Index(SearchModel search);
        DetailResult Detail(int ID);
        MessageModel Create(CreateModel createModel);
        MessageModel Update(UpdateModel updateModel);
        MessageModel BatchUpdate(List<BatchUpdateModel> batchUpdate);
    }
}
