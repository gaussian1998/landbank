﻿using Library.Entity.Edmx;
using Library.Servant.Servant.AccountingBook.Models;
using Library.Servant.Servant.Common.Models;
using System;
using System.Collections.Generic;
using Library.Servant.Servant.Common;
using Library.Servant.Common;

namespace Library.Servant.Servant.AccountingBook
{
    public class RemoteAccountingBookServant: IAccountingBookServant
    {
        public MessageModel BatchUpdate(List<BatchUpdateModel> batchUpdate)
        {
            ListBatchUpdateModel _listBatchUpdateModel = new ListBatchUpdateModel();
            _listBatchUpdateModel.ListData = new List<BatchUpdateModel>();
            _listBatchUpdateModel.ListData = batchUpdate;
            return RemoteServant.Post<ListBatchUpdateModel, MessageModel>(
                           _listBatchUpdateModel,
                           "/AccountingBookServant/Create"
          );
        }
        public MessageModel Create(CreateModel createModel)
        {
            return RemoteServant.Post<CreateModel, MessageModel>(
                            createModel,
                            "/AccountingBookServant/Create"
           );
        }

        public DetailResult Detail(int ID)
        {
            return RemoteServant.Post<IntModel, DetailResult>(
                          new IntModel { Value = ID },
                          "/AccountingBookServant/Detail"
         );
        }

        public IndexResult Index(SearchModel search)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(
                            search,
                            "/AccountingBookServant/Index"
           );
        }

        public MessageModel Update(UpdateModel updateModel)
        {
            return RemoteServant.Post<UpdateModel, MessageModel>(
                           updateModel,
                           "/AccountingBookServant/Update"
          );
        }
    }
}
