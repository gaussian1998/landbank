﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Common.Models;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.AccountingBook.Models
{
    public class SearchModel : InvasionEncryption
    {
        /// <summary>
        /// Code or String.Empty (V01+V02)
        /// </summary>
        public string AccountBookType { get; set; }
        public string AccountBookCode { get; set; }
        /// <summary>
        /// 0 = false, 1 = true, 2 = all
        /// </summary>
        public int IsActive { get; set; }
        /// <summary>
        /// 0 = false, 1 = true, 2 = all
        /// </summary>
        public int IsCancel { get; set; }

        public int PageSize { get; set; }
        public int PageNumber { get; set; }
    }
}
