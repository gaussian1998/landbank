﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.AccountingBook.Models
{
    public class ListBatchUpdateModel : InvasionEncryption
    {
        public List<BatchUpdateModel> ListData { get; set; }
    }
}
