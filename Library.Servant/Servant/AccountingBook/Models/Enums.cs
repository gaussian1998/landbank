﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.AccountingBook.Models
{
    public enum MessageCode
    {
        AccountBookIsExisted = 1,
        AccountBookNotExisted = 2,
        CodeIdIsExisted = 3
    }
}
