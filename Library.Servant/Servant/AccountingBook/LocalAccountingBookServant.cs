﻿using Library.Entity.Edmx;
using Library.Servant.Servant.AccountingBook.Models;
using Library.Servant.Servant.Common.Models;
using System;
using System.Collections.Generic;

namespace Library.Servant.Servant.AccountingBook
{
    public class LocalAccountingBookServant : IAccountingBookServant
    {
        private string V01Name = "帳本";
        private string V02Name = "帳本-含稅務";

        public MessageModel Create(CreateModel createModel)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = String.Empty };

            #region checking the account book is existed
            if (createModel.ClassType.ToLower() == "v01&v02")
            {
                int codelist = AS_Code_Table_Records.Count(x => x.Code_ID == createModel.CodeID && (x.Class == "V01" || x.Class == "V02"));
                if (codelist > 0)
                {
                    message.IsSuccess = false;
                    message.MessagCode = MessageCode.AccountBookIsExisted.ToString();
                    return message;
                }
            }
            else
            {
                var code = AS_Code_Table_Records.Count(x => x.Code_ID == createModel.CodeID && x.Class == createModel.ClassType);
                if (code > 0)
                {
                    message.IsSuccess = false;
                    message.MessagCode = MessageCode.AccountBookIsExisted.ToString();
                    return message;
                }
            }
            #endregion

            #region create data
            DateTime saveTime = DateTime.Now;
            if (createModel.ClassType.ToLower() == "v01&v02")
            {
                AS_Code_Table_Records.Create(new AS_Code_Table
                {
                    Code_ID = createModel.CodeID,
                    Class = "V01",
                    Class_Name = V01Name,
                    Text = createModel.Text,
                    Is_Active = createModel.IsActive,
                    Cancel_Code = createModel.CancelCode,
                    Remark = createModel.Remark,
                    Last_Updated_By = createModel.LastUpdatedBy,
                    Last_Updated_Time = saveTime
                });

                AS_Code_Table_Records.Create(new AS_Code_Table
                {
                    Code_ID = createModel.CodeID,
                    Class = "V02",
                    Class_Name = V02Name,
                    Text = createModel.Text,
                    Is_Active = createModel.IsActive,
                    Cancel_Code = createModel.CancelCode,
                    Remark = createModel.Remark,
                    Last_Updated_By = createModel.LastUpdatedBy,
                    Last_Updated_Time = saveTime
                });

            }
            else
            {
                string className = createModel.ClassType.ToLower() == "v01" ? V01Name : V02Name;
                AS_Code_Table_Records.Create(new AS_Code_Table
                {
                    Code_ID = createModel.CodeID,
                    Class = createModel.ClassType,
                    Class_Name = className,
                    Text = createModel.Text,
                    Is_Active = createModel.IsActive,
                    Cancel_Code = createModel.CancelCode,
                    Remark = createModel.Remark,
                    Last_Updated_By = createModel.LastUpdatedBy,
                    Last_Updated_Time = saveTime
                });
            }
            #endregion

            return message;
        }

        public DetailResult Detail(int ID)
        {
            var found = AS_Code_Table_Records.First(x => x.ID == ID);
            if (found != null)
            {
                return new DetailResult {
                    ID = found.ID,
                    Class = found.Class,
                    ClassName = found.Class_Name,
                    CodeID = found.Code_ID,
                    Text = found.Text,
                    Remark = found.Remark,
                    IsActive = found.Is_Active,
                    CancelCode = found.Cancel_Code
                };
            }
            else
            {
                return new DetailResult { ID = -1 };
            }
        }

        public IndexResult Index(SearchModel search)
        {
            bool skipType = String.IsNullOrEmpty(search.AccountBookType);
            bool skipName = String.IsNullOrEmpty(search.AccountBookCode);
            bool skipActive = search.IsActive >= 2 ? true : false;
            bool skipCancel = search.IsCancel >= 2 ? true : false;

            bool isActive = Convert.ToBoolean(search.IsActive);
            bool isCancel = Convert.ToBoolean(search.IsCancel);
            IndexResult result = new IndexResult {
                Page = AS_Code_Table_Records.Page(
                    search.PageNumber,
                    search.PageSize,
                    x => x.Code_ID,
                    x =>
                        (x.Class == "V01" || x.Class == "V02") &&
                        (skipType || x.Class == search.AccountBookType) &&
                        (skipName || x.Text == search.AccountBookCode) &&
                        (skipActive || x.Is_Active == isActive) &&
                        (skipCancel || x.Cancel_Code == isCancel),

                    entity => new DetailResult {
                        ID = entity.ID,
                        Class = entity.Class,
                        ClassName = entity.Class_Name,
                        CodeID = entity.Code_ID,
                        Text = String.IsNullOrEmpty(entity.Text) ? String.Empty : entity.Text,
                        Remark = String.IsNullOrEmpty(entity.Remark) ? String.Empty : entity.Remark,
                        IsActive = entity.Is_Active,
                        CancelCode = entity.Cancel_Code
                    }

                )};

            return result;
        }

        public MessageModel Update(UpdateModel updateModel)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = String.Empty };
            var old = AS_Code_Table_Records.First(x => x.ID == updateModel.ID);
            if (old != null)
            {
                int checkCode = AS_Code_Table_Records.Count(x => x.Class == old.Class && x.Code_ID == updateModel.CodeID && x.ID != updateModel.ID);
                if (checkCode > 0)
                {
                    message.IsSuccess = false;
                    message.MessagCode = MessageCode.CodeIdIsExisted.ToString();
                    return message;
                }
                else
                {
                    AS_Code_Table_Records.Update(x => x.ID == updateModel.ID, entity => {
                        entity.Code_ID = updateModel.CodeID;
                        entity.Text = updateModel.Text;
                        entity.Is_Active = updateModel.IsActive;
                        entity.Cancel_Code = updateModel.CancelCode;
                        entity.Remark = updateModel.Remark;
                        entity.Last_Updated_By = updateModel.LastUpdatedBy;
                        entity.Last_Updated_Time = DateTime.Now;
                    });
                }
            }
            else
            {
                message.IsSuccess = false;
                message.MessagCode = MessageCode.AccountBookNotExisted.ToString();
            }
            return message;
        }

        public MessageModel BatchUpdate(List<BatchUpdateModel> batch)
        {
            DateTime saveTime = DateTime.Now;
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            foreach (BatchUpdateModel item in batch)
            {
                AS_Code_Table_Records.Update(x => x.ID == item.ID, entity => {
                    entity.Is_Active = item.IsActive;
                    entity.Cancel_Code = item.IsCancel;
                    entity.Last_Updated_By = item.LastUpdateBy;
                    entity.Last_Updated_Time = saveTime;
                });
            }
            return message;
        }
    }
}
