﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.HistoryData.Models;

namespace Library.Servant.Servant.HistoryData
{
    public interface IHistoryData
    {
        IndexResult<SettingItemsInfo> GetSettings(SettingCondition condition);
        IndexResult<HistoryItemsInfo> GetHistory(HistoryCondition condition);
        VoidResult SaveSettings(ListRemoteModel<SettingItemsInfo> settings);
        ListRemoteModel<OptionModel<string, string>> GetHistoryList();
        ByteModel GetCsvContent(string tableName);
    }
}
