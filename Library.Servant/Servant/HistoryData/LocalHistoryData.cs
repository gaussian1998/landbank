﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.HistoryData.Models;
using Library.Entity.Edmx;
using System.Transactions;
using Library.Servant.Servant.Common.Models;
using System.Data.SqlClient;
using System.Reflection;
using System.Collections;

namespace Library.Servant.Servant.HistoryData
{
    public class LocalHistoryData : IHistoryData
    {
        public IndexResult<HistoryItemsInfo> GetHistory(HistoryCondition condition)
        {
            var page = AS_History_Parameter_Records.Page(condition.Page, condition.PageSize, m => m.ID,
                m => /*(condition.ImportRange.Start >= m.Last_UpDatetimed_Time && condition.ImportRange.End <= m.Last_UpDatetimed_Time) &&*/
                     (string.IsNullOrEmpty(condition.TableName) || condition.TableName == m.Source_Table),
                entity => new HistoryItemsInfo
                {
                    HistoryTableName = entity.Purpose_Table,
                    LatestUpdateDateTime = entity.Last_UpDatetimed_Time,
                    TableName = entity.Source_Table_Name,
                    TotoalAmount = 10 //todo_julius 要去抓
                });

            return new IndexResult<HistoryItemsInfo>
            {
                Items = page.Items,
                TotalAmount = page.TotalAmount
            };
        }
        
        public IndexResult<SettingItemsInfo> GetSettings(SettingCondition condition)
        {
            var page = AS_History_Parameter_Records.Page(condition.Page, condition.PageSize, m => m.ID,
                m => string.IsNullOrEmpty(condition.TrasactionCode) || condition.TrasactionCode == m.Transaction_Type,
                entity => new SettingItemsInfo
                {
                    IsKeepFormal = entity.Formal_Area.Value,
                    IsKeepHistory = entity.History_Area.Value,
                    KeepMonthFormal = entity.Formal_Area_Months.Value,
                    KeepMonthHistory = entity.History_Area__Months.Value,
                    Table = entity.Source_Table,
                    TransactionType = entity.Transaction_Type,
                    TableName = entity.Source_Table_Name
                });

            page.Items.ForEach((m) => {
                m.TransactionTypeName = AS_Code_Table_Records.First(model => model.Class == "DocType" && model.Code_ID == m.TransactionType, rlt => rlt.Text);
            });

            return new IndexResult<SettingItemsInfo>
            {
                Items = page.Items,
                TotalAmount = page.TotalAmount
            };
        }
        
        public VoidResult SaveSettings(ListRemoteModel<SettingItemsInfo> settings)
        {
            List<SettingItemsInfo> realSettings = settings.Content;
            using (TransactionScope ts = new TransactionScope()) {
                foreach (var setting in realSettings) {
                    AS_History_Parameter_Records.Update(m => m.Source_Table == setting.Table, entity => {
                        entity.Formal_Area = setting.IsKeepFormal;
                        entity.Formal_Area_Months = setting.KeepMonthFormal;
                        entity.History_Area = setting.IsKeepHistory;
                        entity.History_Area__Months = setting.KeepMonthHistory;
                    });
                }

                ts.Complete();
            }           

            return new VoidResult();
        }

        public ListRemoteModel<OptionModel<string, string>> GetHistoryList()
        {
            var test = new ListRemoteModel<OptionModel<string, string>> {
                Content = AS_History_Parameter_Records.FindAll(entity => new OptionModel<string, string>
                {
                    Text = entity.Source_Table_Name,
                    Value = entity.Source_Table
                })
            };

            return test;
        }

        public ByteModel GetCsvContent(string tableName)
        {
            byte[] header = { 0xEF, 0xBB, 0xBF };
            var sb = new StringBuilder();
            tableName = "AS_Users"; // only for test
            foreach (Type t in Assembly.GetAssembly(typeof(AS_Users)).GetTypes()) {
                if (t.Name == tableName) {
                    sb.AppendLine(string.Join(",", t.GetProperties().Select(m => m.Name)));

                    using (var context = new AS_LandBankEntities()) {                        
                        var data = context.Database.SqlQuery(t, string.Format("SELECT * FROM dbo.{0}", tableName));

                        foreach (var item in data) {
                            sb.AppendLine(
                                string.Join(
                                    ",", 
                                    t.GetProperties().Where(m => m.PropertyType == typeof(string) || !typeof(IEnumerable).IsAssignableFrom(m.PropertyType)).
                                    Select(m => m.GetValue(item))
                                )
                            );
                        }
                    }

                    break;
                }
            }

            return new ByteModel { Value = header.Concat(Encoding.UTF8.GetBytes(sb.ToString())).ToArray() };
        }
    }
}
