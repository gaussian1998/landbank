﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.HistoryData.Models
{
    public class IndexResult<T> : InvasionEncryption
    {
        public int TotalAmount { get; set; }
        public List<T> Items { get; set; }
    }
}
