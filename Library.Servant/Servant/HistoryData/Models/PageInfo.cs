﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.HistoryData.Models
{
    public class PageInfo : InvasionEncryption
    {
        public PageInfo()
        {
            PageSize = 10;
            Page = 1;
        }

        public int PageSize { get; set; }
        public int Page { get; set; }
    }
}
