﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.HistoryData.Models
{
    public class SettingItemsInfo
    {
        public string Table { get; set; }
        public string TransactionType { get; set; }
        public string TransactionTypeName { get; set; }
        public string TableName { get; set; }
        public bool IsKeepFormal { get; set; }
        public int KeepMonthFormal { get; set; }
        public bool IsKeepHistory { get; set; }
        public int KeepMonthHistory { get; set; }
    }
}