﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.HistoryData.Models
{
    public class HistoryItemsInfo : InvasionEncryption
    {
        public string HistoryTableName { get; set; }
        public string TableName { get; set; }
        public Nullable<DateTime> LatestUpdateDateTime { get; set; }
        public int TotoalAmount { get; set; }
    }
}
