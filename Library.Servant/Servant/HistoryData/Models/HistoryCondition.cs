﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.HistoryData.Models
{
    public class HistoryCondition : PageInfo
    {
        public HistoryCondition()
        {
            ImportRange = new RangeModel<DateTime> {
                Start = DateTime.Now,
                End = DateTime.Now
            };
        }

        public string TransactionCode { get; set; }
        public string TableName { get; set; }
        public RangeModel<DateTime> ImportRange { get; set; }
    }
}
