﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Common;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.HistoryData.Models;

namespace Library.Servant.Servant.HistoryData
{
    public class RemoteHistoryData : IHistoryData
    {
        public IndexResult<HistoryItemsInfo> GetHistory(HistoryCondition condition)
        {
            var result = ProtocolService.EncryptionPost<HistoryCondition, IndexResult<HistoryItemsInfo>>
            (
                condition,
                Config.ServantDomain + "/HistoryServant/GetHistory"
            );

            return Tools.ExceptionConvert(result);
        }

        public ListRemoteModel<OptionModel<string, string>> GetHistoryList()
        {
            var result = ProtocolService.EncryptionPost<VoidModel, ListRemoteModel<OptionModel<string, string>>>
            (
                new VoidModel(),
                Config.ServantDomain + "/HistoryServant/GetHistoryList"
            );

            return Tools.ExceptionConvert(result);
        }

        public IndexResult<SettingItemsInfo> GetSettings(SettingCondition condition)
        {
            var result = ProtocolService.EncryptionPost<SettingCondition, IndexResult< SettingItemsInfo >>
            (
                condition,
                Config.ServantDomain + "/HistoryServant/GetSettings"
            );

            return Tools.ExceptionConvert(result);
        }

        public VoidResult SaveSettings(ListRemoteModel<SettingItemsInfo> settings)
        {
            var result = ProtocolService.EncryptionPost<ListRemoteModel<SettingItemsInfo>, VoidResult>
            (
                settings,
                Config.ServantDomain + "/HistoryServant/SaveSettings"
            );

            return Tools.ExceptionConvert(result);
        }

        public ByteModel GetCsvContent(string tableName)
        {
            var result = ProtocolService.EncryptionPost<StringModel, ByteModel>
            (
                new StringModel { Value = tableName },
                Config.ServantDomain + "/HistoryServant/GetCsvContent"
            );

            return Tools.ExceptionConvert(result);
        }
    }
}
