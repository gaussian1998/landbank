﻿using Library.Servant.Servant.ApprovalFlow.Models;

namespace Library.Servant.Servant.ApprovalFlow
{
    public interface IApprovalFlowServant
    {
        IndexResult Index(SearchModel model);
        LogsResult Logs(int ID);
    }
}
