﻿using Library.Servant.Servant.ApprovalFlow.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.ApprovalFlow
{
    public class RemoteApprovalFlowServant : IApprovalFlowServant
    {
        public IndexResult Index(SearchModel model)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(

                model,
                "/ApprovalFlowServant/Index"
            );
        }

        public LogsResult Logs(int ID)
        {
            return RemoteServant.Post<IntModel, LogsResult>(

                new IntModel { Value = ID },
                "/ApprovalFlowServant/Logs"
            );
        }
    }
}
