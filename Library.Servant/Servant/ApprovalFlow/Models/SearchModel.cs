﻿
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.ApprovalFlow.Models
{
    public class SearchModel : InvasionEncryption
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public FlowSelectedOptionsModel SelectedOptions { get; set; }
    }
}
