﻿using Library.Common.Models;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.ApprovalFlow.Models
{
    public class IndexResult : InvasionEncryption
    {
        public PageList<FlowOpenIndexItem> Page { get; set; }
        public FlowSearchOptionsModel Options { get; set; }
    }
}
