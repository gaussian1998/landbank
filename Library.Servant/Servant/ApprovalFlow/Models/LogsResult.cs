﻿using Library.Entity.Edmx;
using Library.Servant.Servant.Common.Models.Mapper;
using System.Collections.Generic;
using System.Linq;

namespace Library.Servant.Servant.ApprovalFlow.Models
{
    public class LogsResult :  FlowOpenModel
    {
        public  AS_Agent_Flow AS_Agent_Flow
        {
            set
            {
                if (value == null)
                    return;

                DocType = value.AS_Doc_Name.Doc_Type_Name;
                DocName = value.AS_Doc_Name.Doc_Name;
            }
        }
        public string DocType { get; set; }
        public string DocName { get; set; }

        public ICollection<AS_Flow_Open_Log> AS_Flow_Open_Log
        {
            set
            {
                Logs = value.AsQueryable().Select( log => new LogItemResult
                {
                    BranchName = log.AS_Users == null ? "無" : "[ " + log.AS_Users.Branch_Name + " ] " + log.AS_Users.Department_Name,
                    ApplyUserName = log.AS_Users == null ? "無" : log.AS_Users.User_Name,
                    UpdateTime = log.LastUpdatedTime.Value,
                    Remark = "",
                    Result = log.Result == 1 ? "審核中" :
                                    log.Result == 2 ? "通過" :
                                    log.Result == 3 ? "退件" :
                                    log.Result == 4 ? "退回修改" : "",

                    FlowStatus = log.FlowStatus

                }).ToList();
            }
        }
        public List<LogItemResult> Logs { get; set; }

        public string FormNo
        {
            get
            {
                return Form_No;
            }
            set
            {
                Form_No = value;
            }
        }
    }
}
