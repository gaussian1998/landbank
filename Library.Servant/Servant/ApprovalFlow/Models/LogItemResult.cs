﻿using Library.Servant.Servant.ApprovalTodo.Constants;
using System;

namespace Library.Servant.Servant.ApprovalFlow.Models
{
    public class LogItemResult
    {
        public string BranchName { get; set; }
        public string ApplyUserName { get; set; }
        public DateTime UpdateTime { get; set; }
        public string Result { get; set; }
        public decimal FlowStatus
        {
            set
            {
                Status = FlowStateServant.Text(value);
                FlowStatusImpl = value;
            }
            get
            {
                return FlowStatusImpl;
            }
        }
        private decimal FlowStatusImpl { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
    }
}
