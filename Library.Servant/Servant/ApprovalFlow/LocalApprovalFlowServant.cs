﻿using System.Linq;
using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalFlow.Models;
using Library.Common.Extension;
using Library.Servant.Servant.Common.ModelExtensions;
using Library.Servant.Servant.ApprovalTodo.Constants;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;
using Library.Servant.Servant.CodeTable;

namespace Library.Servant.Servant.ApprovalFlow
{
    public class LocalApprovalFlowServant : IApprovalFlowServant
    {
        public IndexResult Index(SearchModel model)
        {
            var cadidate = Cadidate();
            var selected = cadidate.Where(model.SelectedOptions.Expression());

            return new IndexResult
            {
                Options = cadidate.Options( FlowStateServant.All() ),
                Page = selected.Page( model.Page, model.PageSize, m => m.ID)
            };
        }

        public LogsResult Logs(int ID)
        {
            return AS_Flow_Open_Records.First<LogsResult>(m => m.ID == ID);
        }

        private List<FlowOpenIndexItem> Cadidate()
        {
            var flow_dictionary = LocalCodeTableServant.FlowRoleIdDictionary();

            return AS_Flow_Open_Records.FindModify<FlowOpenIndexItem>( m => true, result => {

                result.FlowRoleIdDictionary = flow_dictionary;
                return result;
            });
        }
    }
}

