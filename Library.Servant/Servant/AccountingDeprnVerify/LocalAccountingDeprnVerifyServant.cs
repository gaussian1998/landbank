﻿using System.Linq;
using System.Collections.Generic;
using Library.Entity.Edmx;
using Library.Servant.Servant.AccountingDeprnVerify.Models;
using Library.Servant.Servant.CodeTable;
using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Models;
using Library.Common.Models;
using Library.Servant.Servant.DocName;
using Library.Servant.Servant.ApprovalTodo;
using LandBankEntity.ActiveRecords;
using Library.Servant.Servant.Common.Constants;
using Library.Servant.Servant.Models;

namespace Library.Servant.Servant.AccountingDeprnVerify
{
    public class LocalAccountingDeprnVerifyServant : IAccountingDeprnVerifyServant
    {
        public TradeOptions TradeOptions()
        {
            return CodeTable.Models.TradeOptions.QueryDeprn();
        }

        public DocOptions DocOptions()
        {
            return LocalDocNameServant.Options();
        }

        public IndexResult Index(SearchModel search)
        {
            return new IndexResult
            {
                Page = Association( Search(search) )
            };
        }
        
        public void Verify(VerifyModel model)
        {
            string formNo = LocalApprovalTodoServant.Create(model.UserID, "A502", "M804", "DeprnVerify");
            Approval( model, formNo);
        }

        public void ReDeprn()
        {
            GLAccDeprnRecord.Update(

                    entity => entity.Status == AccountStateServant.PrepareApproval,
                    entity =>
                    {
                        entity.Status = AccountStateServant.New;
                    });
        }

        public DetailsResult Details(DetailsModel model)
        {
            var branchs = LocalCodeTableServant.BranchDictionary();
            var departments = LocalCodeTableServant.DepartmentDictionary();

            return new DetailsResult
            {
                Items = Details( model.Account, branchs, departments)
            }; 
        }

        private static List<AccountEntryDetailResult> Details(string Account, Dictionary<string, string> branchs, Dictionary<string, string> departments)
        {
            return AS_GL_Acc_Deprn_Records.FindModify<AccountEntryDetailResult>(m => m.Account == Account, item =>
            {
                item.Branchs = branchs;
                item.Departments = departments;
                return item;
            });
        }

        private static PageList<AccountEntryDetailResult> Search(SearchModel search)
        {
            return AS_GL_Acc_Deprn_Records.GroupList(

                    search.Page,
                    search.PageSize,
                    m => search.TradeKinds.Contains(m.Trade_Type) && m.Status == AccountStateServant.PrepareApproval,
                    m => new { m.Account, m.DB_CR },
                    group => new AccountEntryDetailResult
                    {
                        Department = group.FirstOrDefault().Department,
                        Branch_Code = group.FirstOrDefault().Branch_Code,
                        Account = group.FirstOrDefault().Account,
                        Account_Name = group.FirstOrDefault().Account_Name,
                        Amount = group.Sum(m => m.Amount),
                        Currency = group.FirstOrDefault().Currency,
                        DB_CR = group.FirstOrDefault().DB_CR,
                        Last_Updated_Time = group.FirstOrDefault().Last_Updated_Time,
                        TotalNumber = group.Count()
                    }
            );
        }

        private static PageList<AccountEntryDetailResult> Association(PageList<AccountEntryDetailResult> result)
        {
            var branchs = LocalCodeTableServant.BranchDictionary();
            var departments = LocalCodeTableServant.DepartmentDictionary();
            result.Items.ForEach( item => {

                item.Branchs = branchs;
                item.Departments = departments;
            } );

            return result;
        }

        private static void Approval(VerifyModel model, string formNo)
        {
            using (AS_LandBankEntities db = new AS_LandBankEntities())
            {
                if (StartFlow(db, model, formNo) && UpdateStatus(db, model, formNo))
                    db.SaveChanges();
            }
        }

        private static bool StartFlow(AS_LandBankEntities db, VerifyModel model, string formNo)
        {
            return LocalApprovalTodoServant.StartT(db, model.UserID, formNo, "TestData");
        }

        private static bool UpdateStatus(AS_LandBankEntities db, VerifyModel model, string formNo)
        {
            return GLAccDeprnRecord.UpdateT(
                    db,
                    entity => model.Accounts.Contains(entity.Account) && entity.Status == AccountStateServant.PrepareApproval,
                    entity =>
                    {
                        entity.Trans_Seq = formNo;
                        entity.Status = AccountStateServant.Approvaling;
                    }) != 0;
        }
    }
}
