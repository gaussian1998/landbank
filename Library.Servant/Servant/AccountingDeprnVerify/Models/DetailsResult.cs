﻿using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace Library.Servant.Servant.AccountingDeprnVerify.Models
{
    public class DetailsResult
    {
        public List<AccountEntryDetailResult> Items { get; set; }
    }
}
