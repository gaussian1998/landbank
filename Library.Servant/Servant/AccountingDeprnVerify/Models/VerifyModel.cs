﻿
using System.Collections.Generic;

namespace Library.Servant.Servant.AccountingDeprnVerify.Models
{
    public class VerifyModel
    {
        public int UserID { get; set; }
        public HashSet<string> Accounts { get; set; }
    }
}
