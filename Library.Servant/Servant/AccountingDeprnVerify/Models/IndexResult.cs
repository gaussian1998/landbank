﻿using Library.Common.Models;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.AccountingDeprnVerify.Models
{
    public class IndexResult
    {
        public PageList<AccountEntryDetailResult> Page { get; set; }
    }
}
