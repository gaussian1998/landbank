﻿
using System.Collections.Generic;

namespace Library.Servant.Servant.AccountingDeprnVerify.Models
{
    public class SearchModel
    {
        public int UserID { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public List<string> TradeKinds { get; set; }
    }
}
