﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.LandModels.ReportModel;
using Library.Servant.Servant.MPModels.ReportModel;

namespace Library.Servant.Servant.Report
{
    public class RemoteReportServant : IReportServant
    {
        public IEnumerable<MPReportModel> GetMPNewList(MPReportSearchModel condition)
        {
            throw new NotImplementedException();
        }
        public IEnumerable<MPReportModel> GetMPScrapList(MPReportSearchModel condition)
        {
            throw new NotImplementedException();
        }
        public IEnumerable<MPReportModel> GetMPScrapApprovedList(MPReportSearchModel condition)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MPReportModel> GetMPAssets(MPReportSearchModel condition)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MPReportModel> GetMPDisposeList(MPReportSearchModel condition)
        {
            throw new NotImplementedException();
        }
        #region==土地==
        public IEnumerable<LandReportModel> GetLandAllCityPlanPartitionList(LandReportSearchModel condition)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LandReportModel> GetLandAssetImpairmentList(LandReportSearchModel condition)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LandReportModel> GetLandAssetRevaluationList(LandReportSearchModel condition)
        {
            throw new NotImplementedException();
        }

     
        #endregion
    }
}
