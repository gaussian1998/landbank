﻿using Library.Servant.Servant.MPModels.ReportModel;
using Library.Servant.Servant.LandModels.ReportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Report
{
    public interface IReportServant
    {
        #region== 動產 ==
        IEnumerable<MPReportModel> GetMPAssets(MPReportSearchModel condition);
        IEnumerable<MPReportModel> GetMPNewList(MPReportSearchModel condition);
        IEnumerable<MPReportModel> GetMPScrapList(MPReportSearchModel condition);
        IEnumerable<MPReportModel> GetMPScrapApprovedList(MPReportSearchModel condition);
        IEnumerable<MPReportModel> GetMPDisposeList(MPReportSearchModel condition);
        #endregion

        #region==土地==
        IEnumerable<LandReportModel> GetLandAllCityPlanPartitionList(LandReportSearchModel condition);
        IEnumerable<LandReportModel> GetLandAssetImpairmentList(LandReportSearchModel condition);
        IEnumerable<LandReportModel> GetLandAssetRevaluationList(LandReportSearchModel condition);
      

        #endregion
    }
}
