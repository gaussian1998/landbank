﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.MPModels.ReportModel;
using System.Collections;
using Library.Entity.Edmx;
using Library.Servant.Servant.LandModels.ReportModel;

namespace Library.Servant.Servant.Report
{
    public class LocalReportServant : IReportServant
    {
        public IEnumerable<MPReportModel> GetMPNewList(MPReportSearchModel condition)
        {
            IEnumerable<MPReportModel> query = null;

            using (var db = new AS_LandBankEntities())
            {
                query = db.AS_VW_MP.Select(o => new MPReportModel()
                {
                     Assets_Category_ID = o.Assets_Category_ID,
                     Assets_Fixed_Cost = o.Assets_Fixed_Cost,
                     Assets_Name = o.Assets_Name,
                     Assets_Unit = o.Assets_Unit,
                     Asset_Category_Code = o.Asset_Category_Code,
                     Asset_Number = o.Asset_Number,
                     Assigned_Branch = o.Assigned_Branch,
                     Children = o.Children,
                     Life_Years = o.Life_Years,
                     Location_Disp = o.Location_Disp,
                     Model_Number = o.Model_Number,
                     Remark = o.Remark,
                     Transaction_Date = o.Transaction_Date,
                     PO_Number = o.PO_Number
                }).ToArray();
            }

            return query;
        }

        public IEnumerable<MPReportModel> GetMPScrapList(MPReportSearchModel condition)
        {
            IEnumerable<MPReportModel> query = null;

            using (var db = new AS_LandBankEntities())
            {
                query = db.AS_VW_MP.Select(o => new MPReportModel()
                {
                    Asset_Number = o.Asset_Number,//財產編號
                    Assets_Name = o.Assets_Name,//財物名稱
                    Model_Number = o.Model_Number,//廠牌規格
                    Date_Placed_In_Service = o.Date_Placed_In_Service,//啟用日期
                    Life_Years = o.Life_Years,//年限
                    Life_Month = o.Life_Month, //已折月數
                    Deprn_Date = o.Deprn_Date,//耐用止日
                    Assets_Unit = o.Assets_Unit,//數量
                    Assets_Fixed_Cost = o.Assets_Fixed_Cost,//資產金額
                    Deprn_Reserve = o.Deprn_Reserve,//累計折舊
                    Net_Value = o.Net_Value,//淨值
                    Assigned_Branch = o.Assigned_Branch//使用單位
                }).ToArray();
            }

            return query;
        }
        public IEnumerable<MPReportModel> GetMPScrapApprovedList(MPReportSearchModel condition)
        {
            IEnumerable<MPReportModel> query = null;

            using (var db = new AS_LandBankEntities())
            {
                query = db.AS_VW_MP.Select(o => new MPReportModel()
                {
                    Asset_Number = o.Asset_Number,//財產編號
                    Assets_Name = o.Assets_Name,//財物名稱
                    Model_Number = o.Model_Number,//廠牌規格
                    Date_Placed_In_Service = o.Date_Placed_In_Service,//啟用日期
                    Life_Years = o.Life_Years,//年限
                    Life_Month = o.Life_Month,//已折月數
                    Deprn_Date = o.Deprn_Date,//耐用止日
                    Assets_Unit = o.Assets_Unit,//數量
                    Assets_Fixed_Cost = o.Assets_Fixed_Cost,//資產金額
                    Deprn_Reserve = o.Deprn_Reserve,//累計折舊
                    Salvage_Value = o.Net_Value,//淨值
                    Assigned_Branch = o.Assigned_Branch//使用單位
                }).ToArray();
            }

            return query;
        }

        public IEnumerable<MPReportModel> GetMPAssets(MPReportSearchModel condition)
        {
            IEnumerable<MPReportModel> query = null;

            using (var db = new AS_LandBankEntities())
            {
                query = db.AS_VW_MP.Select(o => new MPReportModel()
                {
                    Asset_Number = o.Asset_Number,//財產編號
                    Assets_Name = o.Assets_Name,//財物名稱
                    Model_Number = o.Model_Number,//廠牌規格
                    Asset_Category_Code = o.Assets_Category_ID,//分類大類
                    Assets_Category_ID = o.Assets_Category_ID,//分類小類
                    Date_Placed_In_Service = o.Date_Placed_In_Service,//啟用日期
                    Life_Years = o.Life_Years,//年限
                    Life_Month = o.Life_Month,//已折月數
                    Assets_Unit = o.Assets_Unit,//數量
                    Assets_Fixed_Cost = o.Assets_Fixed_Cost,//資產金額
                    Deprn_Cost = o.Deprn_Cost,//本月折舊
                    Deprn_Reserve = o.Deprn_Reserve,//累計折舊
                    Salvage_Value = o.Salvage_Value,//殘值
                    Net_Value = o.Net_Value,//淨值
                    PO_Number = o.PO_Number,//採購案號
                    Assigned_Branch = o.Assigned_Branch,//使用單位
                    Remark = o.Remark//備註
                }).ToArray();
            }

            return query;
        }

        public IEnumerable<MPReportModel> GetMPDisposeList(MPReportSearchModel condition)
        {
            IEnumerable<MPReportModel> query = null;

            using (var db = new AS_LandBankEntities())
            {
                query = db.AS_VW_MP.Select(o => new MPReportModel()
                {
                    Asset_Number = o.Asset_Number,//財產編號
                    Assets_Name = o.Assets_Name,//財物名稱
                    Model_Number = o.Model_Number,//廠牌規格
                    Date_Placed_In_Service = o.Date_Placed_In_Service,//啟用日期
                    Life_Years = o.Life_Years,//年限
                    Life_Month = o.Life_Month,//已折月數
                    Deprn_Date = o.Deprn_Date,//耐用止日
                    Assets_Unit = o.Assets_Unit,//數量
                    Assets_Fixed_Cost = o.Assets_Fixed_Cost,//資產金額
                    Deprn_Reserve = o.Deprn_Reserve,//累計折舊
                    Salvage_Value = o.Salvage_Value,//淨值
                    Assigned_Branch = o.Assigned_Branch//使用單位
                }).ToArray();
            }

            return query;
        }

        public IEnumerable<LandReportModel> GetLandAllCityPlanPartitionList(LandReportSearchModel condition)
        {
            IEnumerable<LandReportModel> query = null;
            using (var db = new AS_LandBankEntities())
            {
                query = (from c1 in db.AS_Assets_Land
                        group c1 by c1.Used_Partition into g
                        select new LandReportModel()
                        {
                            Used_Partition = g.Key,//使用分區                             
                            Used_Partition_Amount = g.Count(),//數量
                            Area_Size = g.Sum(c1 => c1.Area_Size),//整筆面積
                            Current_Cost = g.Sum(c1=>c1.Current_Cost + c1.Reval_Reserve - c1.Deprn_Amount)//帳面金額
                        }).ToList();
            }
            return query;
        }

        public IEnumerable<LandReportModel> GetLandAssetImpairmentList(LandReportSearchModel condition)
        {
            IEnumerable<LandReportModel> query = null;
            using (var db = new AS_LandBankEntities())
            {
                query = db.AS_Assets_Land.Select(o => new LandReportModel()
                {
                    Asset_Number = o.Asset_Number,//財產編號
                    City_Name = o.City_Code,//縣市
                    District_Name = o.District_Code,//鄉鎮
                    Section_Name = o.Section_Code,//段
                    Sub_Section_Name = o.Sub_Section_Name,//小段
                    Parent_Land_Number = o.Parent_Land_Number,//母地號,地號=母地號-子弟號  -為連結
                    Filial_Land_Number = o.Filial_Land_Number,//子弟號
                    Authorized_Area = o.Authorized_Area,//權利面積
                    Announce_Amount = (o.Announce_Amount != null ? o.Announce_Amount : 1),//公告現值
                    All_Announce_Amount = (o.Authorized_Area * (o.Announce_Amount!=null?o.Announce_Amount:1)),//公告現值總額=Authorized_Area*Announce_Amount
                    //BonusNumber//加成數
                    Recoverable_amount = (o.Authorized_Area *( o.Announce_Amount != null ? o.Announce_Amount : 1)), //可回收金額All_Announce_Amount*加成數
                    Current_Cost = (o.Current_Cost != null ? o.Current_Cost : 0),
                    Reval_Reserve= (o.Reval_Reserve != null ? o.Reval_Reserve : 0),
                    Deprn_Amount= (o.Deprn_Amount != null ? o.Deprn_Amount : 0),
                    BookValueBalanceForAssetImpairment=((o.Current_Cost!=null?o.Current_Cost:0)+(o.Reval_Reserve!=null?o.Reval_Reserve:0)-(o.Deprn_Amount!=null?o.Deprn_Amount:0)),// 帳面價值餘額 - 資產減損明細表Current_Cost + Reval_Reserve - Deprn_Amount
                    RiseInNumber= ((o.Current_Cost != null ? o.Current_Cost : 0 )+ (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) -(o.Deprn_Amount != null ? o.Deprn_Amount : 0)),//補提減損數Current_Cost + Reval_Reserve - Deprn_Amount - Deprn_Amount
                    CountTheNumberofAccounts=o.Reval_Adjustment_Amount,//CountTheNumberofAccounts//帳列數
                    //NumberOfPreviousYears//以前年度減少數
                    NumberOfRevolutions= (((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) < 0?(((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) <(o.Reval_Adjustment_Amount!=null?o.Reval_Adjustment_Amount:0)?-((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) :-(o.Reval_Adjustment_Amount != null ? o.Reval_Adjustment_Amount : 0)) :(-(((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0))) <0?-(-(((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)))):0)),//NumberOfRevolutions//沖轉數
                    RevalAdjustmentAmountBalanceForAssetImpairment= ((o.Reval_Adjustment_Amount != null ? o.Reval_Adjustment_Amount : 0)+ (((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) < 0 ? (((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) < (o.Reval_Adjustment_Amount != null ? o.Reval_Adjustment_Amount : 0) ? -((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) : -(o.Reval_Adjustment_Amount != null ? o.Reval_Adjustment_Amount : 0)) : (-(((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0))) < 0 ? -(-(((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)))) : 0))),//RevalAdjustmentAmountBalanceForAssetImpairment//未實現重估增值餘額=沖轉數+帳列數
                    ReversalImpairmentLossAssets=(((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0))<0?-(((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) < 0?(((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) <(o.Reval_Adjustment_Amount!=null?o.Reval_Adjustment_Amount:0)?-((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) :-(o.Reval_Adjustment_Amount != null ? o.Reval_Adjustment_Amount : 0)) :(-(((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0))) <0?-(-(((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)))):0))- (((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) < 0 ? (((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) < (o.Reval_Adjustment_Amount != null ? o.Reval_Adjustment_Amount : 0) ? -((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) : -(o.Reval_Adjustment_Amount != null ? o.Reval_Adjustment_Amount : 0)) : (-(((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0))) < 0 ? -(-(((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)))) : 0)) : 0),//ReversalImpairmentLossAssets//資產減損迴轉利益
                    LossOfAssets= (((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0))>0 ? ((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) +(((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) < 0?(((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) <(o.Reval_Adjustment_Amount!=null?o.Reval_Adjustment_Amount:0)?-((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) :-(o.Reval_Adjustment_Amount != null ? o.Reval_Adjustment_Amount : 0)) :(-(((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0))) <0?-(-(((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)))):0)):0),//LossOfAssets//資產減損損失
                }
                ).ToList();

            }
                return query;
        }

        public IEnumerable<LandReportModel> GetLandAssetRevaluationList(LandReportSearchModel condition)
        {
            IEnumerable<LandReportModel> query = null;
            using (var db = new AS_LandBankEntities())
            {
                query = db.AS_Assets_Land.Select(o => new LandReportModel()
                {
                    Asset_Number = o.Asset_Number,//財產編號
                    City_Name = o.City_Code,//縣市
                    District_Name = o.District_Code,//鄉鎮
                    Section_Name = o.Section_Code,//段
                    Sub_Section_Name = o.Sub_Section_Name,//小段
                    Parent_Land_Number = o.Parent_Land_Number,//母地號,地號=母地號-子弟號  -為連結
                    Filial_Land_Number = o.Filial_Land_Number,//子弟號
                    Authorized_Area = o.Authorized_Area,//權利面積
                    Announce_Amount = (o.Announce_Amount != null ? o.Announce_Amount : 1),//公告現值
                    All_Announce_Amount = (o.Authorized_Area * (o.Announce_Amount!=null?o.Announce_Amount:1)),//公告現值總額=Authorized_Area*Announce_Amount
                    Current_Cost = (o.Current_Cost != null ? o.Current_Cost : 0),
                    Reval_Reserve = (o.Reval_Reserve != null ? o.Reval_Reserve : 0),
                    Deprn_Amount = (o.Deprn_Amount != null ? o.Deprn_Amount : 0),
                    TotalForAssetRevaluationList = (((o.Current_Cost != null ? o.Current_Cost : 0) != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)),// 合計 - 資產重估明細表Current_Cost + Reval_Reserve - Deprn_Amount
                    RevaluationAfterBookValue=((o.Authorized_Area * (o.Announce_Amount != null ? o.Announce_Amount : 1)) > ((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) ? (o.Authorized_Area * (o.Announce_Amount != null ? o.Announce_Amount : 1)) : ((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0))),//RevaluationAfterBookValue//重估后帳面價值
                    ValueAddedMoney=(((o.Authorized_Area * (o.Announce_Amount != null ? o.Announce_Amount : 1)) > ((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)) ? (o.Authorized_Area * (o.Announce_Amount != null ? o.Announce_Amount : 1)) : ((o.Current_Cost != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0)))- (((o.Current_Cost != null ? o.Current_Cost : 0) != null ? o.Current_Cost : 0) + (o.Reval_Reserve != null ? o.Reval_Reserve : 0) - (o.Deprn_Amount != null ? o.Deprn_Amount : 0))),//ValueAddedMoney//增值金額=重估后帳面價值-合計
                    Reval_Land_VATCountTheNumberofAccounts=(o.Reval_Land_VAT!=null?o.Reval_Land_VAT:0),//Reval_Land_VATCountTheNumberofAccounts////估計應付土地增值稅帳列數
                    //Reval_Land_VATRevaluationCountTheNumberofAccounts//估計應付土地增值稅重估后帳列數
                    //Reval_Land_VATNumberOfRevolutions //估計應付土地增值稅應補提（沖轉)數
                    Reval_Adjustment_AmountCountTheNumberofAccounts=(o.Reval_Adjustment_Amount!=null?o.Reval_Adjustment_Amount:0),//Reval_Adjustment_AmountCountTheNumberofAccounts //未實現重估增值帳列數
                    //Reval_Adjustment_AmountRevaluationCountTheNumberofAccounts //未實現重估增值重估后帳列數
                    //Reval_Adjustment_AmountNumberOfRevolutions//未實現重估增值應補提（沖轉)數
                    //SpecialLoss//特別損失
                   
                }
                ).ToList();

            }
            return query;
        }
       

    }
}
