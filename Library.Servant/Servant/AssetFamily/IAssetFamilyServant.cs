﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AssetFamily.Models;

namespace Library.Servant.Servant.AssetFamily
{
    public interface IAssetFamilyServant
    {
        IndexResult Index(SearchModel Search);
    }
}
