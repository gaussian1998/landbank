﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AssetFamily.Models;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.AssetFamily
{
    public class LocalAssetFamilyServant : IAssetFamilyServant
    {
        public IndexResult Index(SearchModel Search)
        {
            bool useParentCode, useChildCode;
            if (Search.AssetType == -1)
            {
                useParentCode = true;
                useChildCode = true;
            }
            else if (Search.AssetType == 0)
            {
                useParentCode = true;
                useChildCode = false;
            }
            else
            {
                useParentCode = false;
                useChildCode = true;
            }

            IndexResult result = new IndexResult
            {
                Conditions = Search,
                Page = AS_Asset_Family_Records.Page(
                    Search.CurrentPage,
                    Search.PageSize,
                    x => x.Asset_Number,
                    x => (useParentCode && x.Parent_Asset_Number.Contains(Search.AssetName)) || 
                         (useChildCode && x.Asset_Number.Contains(Search.AssetName)),
                    m => new DetailModel {
                        ID = m.ID,
                        Layer = m.Layer,
                        MainKind = m.Main_Kind,
                        ParentCategoryCode = m.Parent_Category_Code,
                        ParentID = (m.Parent_ID ?? 0),
                        ParentAssetNumber = m.Parent_Asset_Number,
                        AssetCategoryCode = m.Asset_Category_Code,
                        AssetNumber = m.Asset_Number,
                        DeprnMethod = m.Deprn_Method,
                        LifeYears = m.Life_Years,
                        LifeMonths = m.Life_Months,
                        SalvageValue =m.Salvage_Value,
                        NotDeprn = m.Not_Deprn_Flag,
                        BranchCode = (m.Branch_Code ?? 0),
                        Department = m.Department
                    }
                    )
            };

            return result;
        }
    }
}
