﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;
using Library.Common.Models;

namespace Library.Servant.Servant.AssetFamily.Models
{
    public class IndexResult : InvasionEncryption
    {
        public SearchModel Conditions { get; set; }
        public PageList<DetailModel> Page { get; set; }
    }
}
