﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AssetFamily.Models
{
    public class SearchModel : InvasionEncryption
    {
        /// <summary>
        /// 0:parent, 1:child, -1: all
        /// </summary>
        public int AssetType { get; set; }
        public string AssetName { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
    }
}
