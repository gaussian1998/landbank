﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AssetFamily.Models
{
    public class DetailModel : InvasionEncryption
    {
        public int ID { get; set; }
        public decimal Layer { get; set; }
        public decimal FamilyType { get; set; }
        public string MainKind { get; set; }
        public string ParentCategoryCode { get; set; }
        public int ParentID { get; set; }
        public string ParentAssetNumber { get; set; }
        public string AssetCategoryCode { get; set; }
        public string AssetNumber { get; set; }
        public string DeprnMethod { get; set; }
        public decimal LifeYears { get; set; }
        public decimal LifeMonths { get; set; }
        public decimal SalvageValue { get; set; }
        public bool NotDeprn { get; set; }
        public decimal BranchCode { get; set; }
        public string Department { get; set; }
        public decimal LastUpdatedBy { get; set; }
    }
}
