﻿using Library.Servant.Servant.AccountingEntryList.Models;
using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.AccountingEntryList
{
    public class RemoteAccountingEntryListServant : IAccountingEntryListServant
    {
        public BranchDepartmentOptions Options()
        {
            return RemoteServant.Post<VoidModel, BranchDepartmentOptions>(

                            new VoidModel { }, 
                            "/AccountingEntryListServant/Options"
           );
        }

        public IndexResult Index(BranchSearchModel model)
        {
            return RemoteServant.Post<BranchSearchModel, IndexResult>(

                            model,
                            "/AccountingEntryListServant/Index"
            );
        }
    }
}
