﻿using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.AccountingEntryList.Models;

namespace Library.Servant.Servant.AccountingEntryList
{
   public interface IAccountingEntryListServant
    {
        BranchDepartmentOptions Options();
        IndexResult Index(BranchSearchModel model);
    }
}
