﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.FunctionRole.Models
{
    public class IndexResult : InvasionEncryption
    {
        public List<DetailsResult> Items { get; set; }
        public int TotalAmount { get; set; }
        public List<OptionModel<string, string>> TotalFunctions { get; set; }
    }
}
