﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Servant.FunctionRole.Models
{
    public class EditResult : InvasionEncryption
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<OptionModel<int, string>> CadidateLoginRoles { get; set; }
        public List<OptionModel<int, string>> SelectedLoginRoles { get; set; }
    }
}
