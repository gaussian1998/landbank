﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Servant.FunctionRole.Models
{
    public class UpdateModel : InvasionEncryption
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public List<int> SelectedLoginRolesID { get; set; }
    }
}
