﻿using System.Collections.Generic;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.FunctionRole.Models
{
    public class CreateModel : InvasionEncryption
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<int> SelectedRolesID { get; set; }
    }
}
