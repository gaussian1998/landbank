﻿using System;

namespace Library.Servant.Servant.FunctionRole.Models
{
    public class FunctionModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}
