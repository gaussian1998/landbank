﻿using System.Linq;
using LandBankEntity.ActiveRecords;
using Library.Servant.Servant.FunctionRole.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common;
using System.Transactions;
using Library.Servant.Servant.LoginRole;
using System.Collections.Generic;
using Library.Entity.Edmx;
using System;

namespace Library.Servant.Servant.FunctionRole
{
    public class LocalFunctionRoleServant : IFucntionRoleServant
    {
        public IndexResult Index(SearchModel search)
        {
            var isSkipFunction = string.IsNullOrEmpty(search.FunctionID);
            var isSkipDocType = string.IsNullOrEmpty(search.DocType);

            var result = AS_Programs_Records.Page( 
                search.Page, 
                search.PageSize, 
                m => m.ID,
                m => (isSkipFunction || m.Programs_Code == search.FunctionID) &&
                     (isSkipDocType || m.Doc_Type == search.DocType),
                entity => new DetailsResult
                {
                    ID = entity.ID,
                    Code = entity.Programs_Number,
                    Name = entity.Programs_Name,
                    Description = entity.Remark,
                    LoginRoleNames = entity.AS_Programs_Roles.AsQueryable().Select(m => m.AS_Role.Role_Name).ToList()
                }
            );

            return new IndexResult
            {
                Items = result.Items,
                TotalAmount = result.TotalAmount,
                TotalFunctions = TotalFunctions()
            };
        }

        public EditResult Edit(int ID)
        {
            var result = AS_Programs_Records.First(m => m.ID == ID, entity => new EditResult
            {
                ID = entity.ID,
                Code = entity.Programs_Number,
                Name = entity.Programs_Name,
                Description = entity.Remark,
                SelectedLoginRoles = entity.AS_Programs_Roles.AsQueryable().Select( m => new OptionModel<int, string> { Value = m.AS_Role.ID, Text = m.AS_Role.Role_Name } ).ToList()
            } );

            result.CadidateLoginRoles = LocalLoginRoleServant.CadidateLoginRoles(result.SelectedLoginRoles);
            return result;
        }

        public VoidResult Update(UpdateModel model)
        {
            using (var trans = new TransactionScope())
            {
                AS_Programs_Roles_Records.Delete(m => m.AS_Programs.ID == model.ID);
                AS_Programs_Records.Update(m => m.ID == model.ID, entity => {

                    entity.Remark = model.Description;
                    foreach (var role_id in model.SelectedLoginRolesID)
                        entity.AS_Programs_Roles.Add(new Entity.Edmx.AS_Programs_Roles { Role_ID = role_id });
                });

                trans.Complete();
            }

            return new VoidResult();
        }

        private static List<OptionModel<string, string>> TotalFunctions()
        {
            return AS_Programs_Records.FindAll(m => new OptionModel<string, string> { Value = m.Programs_Code, Text = m.Programs_Name });
        }

        public NewResult New()
        {
            return new NewResult
            {
                CadidateRoles = AS_Role_Records.FindAll(entity => new OptionModel<int, string> { Value = entity.ID, Text = entity.Role_Name })
            };
        }

        public VoidResult Create(CreateModel model)
        {
            AS_Programs_Records.Create(model, entity => {

                foreach (var roleId in model.SelectedRolesID)
                    entity.AS_Programs_Roles.Add(new Entity.Edmx.AS_Programs_Roles { Role_ID = roleId });
            });

            return new VoidResult { };
        }
    }


}
