﻿using Library.Servant.Servant.Common;
using Library.Servant.Servant.FunctionRole.Models;

namespace Library.Servant.Servant.FunctionRole
{
    public interface IFucntionRoleServant
    {
        NewResult New();
        IndexResult Index(SearchModel condition);
        EditResult Edit(int ID);
        VoidResult Update(UpdateModel model);
        VoidResult Create(CreateModel model);
    }
}
