﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;
using Library.Servant.Servant.AccountingDeprnSetting.Models;
using Library.Common.Models;

namespace Library.Servant.Servant.AccountingDeprnSetting.Models
{
    public class IndexResult : InvasionEncryption
    {
        public SearchModel Conditions { get; set; }
        public PageList<DetailModel> Page { get;set; }
    }
}
