﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AccountingDeprnSetting.Models
{
    public class DetailModel : InvasionEncryption
    {
        public string AssetCategoryCode { get; set; }
        public string AssetCategoryName { get; set; }
        public int MainKindID { get; set; }
        public string MainKindDisplayName { get; set; }
        public int DetailKindID { get; set; }
        public string DetailKindDisplayName { get; set; }
        public bool IsActive { get; set; }
        public decimal LifeYears { get; set; }
        public decimal LifeMonths { get; set; }
        public decimal SalvageValue { get; set; }
        public bool NotDeprnFlag { get; set; }
    }
}
