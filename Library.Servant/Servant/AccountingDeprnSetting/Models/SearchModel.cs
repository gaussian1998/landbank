﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AccountingDeprnSetting.Models
{
    public class SearchModel : InvasionEncryption
    {
        public int MainKindID { get; set; }
        public int DetailKindID { get; set; }
        public string BeginCode { get; set; }
        public string EndCode { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
    }
}
