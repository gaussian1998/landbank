﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingDeprnSetting.Models;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.AccountingDeprnSetting
{
    public interface IAccountingDeprnSettingServant
    {
        IndexResult Index(SearchModel Search);
        MessageModel BatchUpdate(BatchUpdateModel BatchUpdate);
        byte[] ExportData(SearchModel Search);
    }
}
