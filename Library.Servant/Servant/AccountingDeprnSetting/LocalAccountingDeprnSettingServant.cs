﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingDeprnSetting.Models;
using Library.Servant.Servant.Common.Models;
using Library.Entity.Edmx;
using Library.Common.Models;
using System.Data.SqlClient;

namespace Library.Servant.Servant.AccountingDeprnSetting
{
    public class LocalAccountingDeprnSettingServant : IAccountingDeprnSettingServant
    {
        public MessageModel BatchUpdate(BatchUpdateModel BatchUpdate)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            DateTime lastUpdateTime = DateTime.Now;
            foreach (var item in BatchUpdate.List)
            {
                string[] ids = item.ID.Split('_');
                int mindKindId = Convert.ToInt32(ids[0]);
                int detailKindId = Convert.ToInt32(ids[1]);

                AS_Asset_Category_Records.Update(x => x.Main_Kind_ID == mindKindId && x.Detail_Kind_ID == detailKindId,
                    m => {
                    m.Life_Years = item.LifeYears;
                    m.Life_Months = item.LifeYears * 12;
                    m.Salvage_Value = item.SalvageValue;
                    m.Not_Deprn_Flag = item.NoDeprn;
                    m.Is_Active = item.IsActive;
                    m.Last_Updated_By = BatchUpdate.LastUpdateBy;
                    m.Last_Updated_Time = lastUpdateTime;
                });
            }
            return message;
        }
        public IndexResult Index(SearchModel Search)
        {
            string countSql = "select 1 from as_asset_category (nolock) where 0 = 0 {0}{1}{2}{3} group by Main_Kind_ID, Detail_Kind_ID";
            string querySql = "select CONCAT(a.Main_Kind, '.', a.Detail_Kind) as AssetCategoryCode, " +
" a.Main_Kind_ID as MainKindID, m.Name as MainKindDisplayName, a.Detail_Kind_ID as DetailKindID, d.Name as DetailKindDisplayName, " +
" (select top 1 Is_Active from as_asset_category(nolock) where Main_Kind_ID = a.Main_Kind_ID and Detail_Kind_ID = a.Detail_Kind_ID) as IsActive, " +
" (select top 1 Life_Years from as_asset_category(nolock) where Main_Kind_ID = a.Main_Kind_ID and Detail_Kind_ID = a.Detail_Kind_ID) as LifeYears," +
" (select top 1 Life_Months from as_asset_category(nolock) where Main_Kind_ID = a.Main_Kind_ID and Detail_Kind_ID = a.Detail_Kind_ID) as LifeMonths," +
" (select top 1 Salvage_Value from as_asset_category(nolock) where Main_Kind_ID = a.Main_Kind_ID and Detail_Kind_ID = a.Detail_Kind_ID) as SalvageValue," +
" (select top 1 Not_Deprn_Flag from as_asset_category(nolock) where Main_Kind_ID = a.Main_Kind_ID and Detail_Kind_ID = a.Detail_Kind_ID) as NotDeprnFlag" +
" from (select Main_Kind, Detail_Kind, Main_Kind_ID, Detail_Kind_ID from as_asset_category (nolock) " +
" where 0 = 0 {0}{1}{2}{3} " +
" group by Main_Kind, Detail_Kind, Main_Kind_ID, Detail_Kind_ID order by Main_Kind_ID, Detail_Kind_ID offset {4} rows fetch next {5} rows only) as a " +
" left join AS_Asset_Category_Main_Kinds as m on a.Main_Kind_ID = m.ID" +
" left join AS_Asset_Category_Detail_Kinds as d on a.Detail_Kind_ID = d.ID; ";

            string beginCodeCondition = "";
            string endCodeCondition = "";
            string mainKindCondition = "";
            string detailKindCondition = "";

            int offset = (Search.CurrentPage - 1) * Search.PageSize;

            if (Search.MainKindID != 0)
            {
                mainKindCondition = " and Main_Kind_ID = @mainKind";
            }
            if (Search.DetailKindID != 0)
            {
                detailKindCondition = " and Detail_Kind_ID = @detailKind";
            }

            if (!String.IsNullOrEmpty(Search.BeginCode))
            {
                beginCodeCondition = " and Asset_Category_Code >= @beginCode";
            }
            else
            {
                Search.BeginCode = "";
            }

            if (!String.IsNullOrEmpty(Search.EndCode))
            {
                endCodeCondition = " and Asset_Category_Code <= @endCode";
            }
            else
            {
                Search.EndCode = "";
            }

            IndexResult result = new IndexResult {
                Conditions = Search,
                Page = new PageList<DetailModel> {
                    Items = new List<DetailModel>(),
                    TotalAmount = 0
                }
            };

            countSql = String.Format(countSql, mainKindCondition, detailKindCondition, beginCodeCondition, endCodeCondition);
            querySql = String.Format(querySql, mainKindCondition, detailKindCondition, beginCodeCondition, endCodeCondition, offset, Search.PageSize);

            using (var context = new AS_LandBankEntities())
            {
                int totalAmount = context.Database.SqlQuery<int>(countSql, 
                    new SqlParameter("mainKind", Search.MainKindID),
                    new SqlParameter("detailKind", Search.DetailKindID),
                    new SqlParameter("beginCode", Search.BeginCode),
                    new SqlParameter("endCode", Search.EndCode)).Count();

                List<DetailModel> page = context.Database.SqlQuery<DetailModel>(querySql,
                    new SqlParameter("mainKind", Search.MainKindID),
                    new SqlParameter("detailKind", Search.DetailKindID),
                    new SqlParameter("beginCode", Search.BeginCode),
                    new SqlParameter("endCode", Search.EndCode)).ToList();

                result.Page.Items = page;
                result.Page.TotalAmount = totalAmount;
            }
            return result;
        }

        public byte[] ExportData(SearchModel Search)
        {

            string sqlCommand = "select a.Asset_Category_Code as AssetCategoryCode, a.Asset_Category_Name as AssetCategoryName, CONCAT(m.ID, m.Name) as MainKindDisplayName, CONCAT(d.ID, d.Name) as DetailKindDisplayName," +
                                " a.Life_Years as LifeYears, a.Life_Months as LifeMonths, a.Salvage_Value as SalvageValue, a.Not_Deprn_Flag as NotDeprnFlag, a.Is_Active as IsActive from AS_Asset_Category as a" +
                                " left join AS_Asset_Category_Main_Kinds as m on a.Main_Kind_ID = m.ID left join AS_Asset_Category_Detail_Kinds as d on a.Detail_Kind_ID = d.ID" +
                                " where 0=0 ";

            if (Search.MainKindID != 0)
            {
                sqlCommand += " a.Main_Kind_ID = @MainKindId";
            }
            if (Search.DetailKindID != 0)
            {
                sqlCommand += " and a.Detail_Kind_ID = @DetailKindId";
            }
            if (!String.IsNullOrEmpty(Search.BeginCode))
            {
                sqlCommand += " and a.Asset_Category_Code >= @BeginCode";
            }
            else
            {
                Search.BeginCode = "";
            }

            if (!String.IsNullOrEmpty(Search.EndCode))
            {
                sqlCommand += " and a.Asset_Category_Code <= @EndCode";
            }
            else
            {
                Search.EndCode = "";
            }
            
            byte[] header = { 0xEF, 0xBB, 0xBF };
            var sb = new StringBuilder();
            sb.Append("資產類別編號, 資產類別, 資產大類, 明細分類, 年限(年), 殘值, 不提列折舊, 啟用");

            List<DetailModel> data;
            using (var context = new AS_LandBankEntities())
            {
                data = context.Database.SqlQuery<DetailModel>(sqlCommand,
                        new SqlParameter("@MainKindId", Search.MainKindID),
                        new SqlParameter("@DetailKindId", Search.DetailKindID),
                        new SqlParameter("@BeginCode", Search.BeginCode),
                        new SqlParameter("@EndCode", Search.EndCode)).ToList();
            }

            foreach (var item in data)
            {
                sb.AppendFormat("{0}{1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}",
                    Environment.NewLine,
                    item.AssetCategoryCode,
                    item.AssetCategoryName,
                    item.MainKindDisplayName, 
                    item.DetailKindDisplayName, 
                    item.LifeYears, 
                    item.SalvageValue,
                    item.NotDeprnFlag ? "是" : "否",
                    item.IsActive ? "是" : "否");
            }

            return header.Concat(Encoding.UTF8.GetBytes(sb.ToString())).ToArray();
        }
    }
}
