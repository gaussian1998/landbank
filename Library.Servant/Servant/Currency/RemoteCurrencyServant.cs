﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Currency.Model;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.Currency
{
    public class RemoteCurrencyServant : ICurrencyServant
    {
        public MessageModel Create(CreateModel Create)
        {
            return RemoteServant.Post<CreateModel, MessageModel>(
                            Create,
                            "/CurrencyServant/Create"
           );
        }

        public UpdateModel Detail(int ID)
        {
            return RemoteServant.Post<IntModel, UpdateModel>(
                            new IntModel { Value=ID},
                            "/CurrencyServant/Detail"
           );
        }

        public IndexResult Index(SearchModel Search)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(
                            Search,
                            "/CurrencyServant/Index"
           );
        }

        public MessageModel Update(UpdateModel Update)
        {
            return RemoteServant.Post<UpdateModel, MessageModel>(
                            Update,
                            "/CurrencyServant/Update"
           );
        }

        public MessageModel BatchDelete(BatchDelete Delete)
        {
            return RemoteServant.Post<BatchDelete, MessageModel>(
                            Delete,
                            "/CurrencyServant/BatchDelete"
           );
        }
    }
}
