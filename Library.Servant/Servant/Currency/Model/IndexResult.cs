﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Common.Models;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.Currency.Model
{
    public class IndexResult : InvasionEncryption
    {
        public PageList<UpdateModel> Page { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public List<OptionModel<int, string>> Branchs { get; set; }
        public List<OptionModel<int, string>> SelectedBranchs { get; set; }
    }
}
