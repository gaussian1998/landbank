﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Common.Models;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.Currency.Model
{
    public class CreateModel : InvasionEncryption
    {
        public string Currency { get; set; }
        public string Country { get; set; }
        public string ExRateType { get; set; }
        public string CurrencyName { get; set; }
        public string SwiftCurrency { get; set; }
        public decimal Basis { get; set; }
        public decimal Decimal { get; set; }
        public decimal Truncation { get; set; }
        public List<int> SelectedBranchCode { get; set; }
        public int LastUpdateBy { get; set; }
    }
}
