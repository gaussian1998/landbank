﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Currency.Model
{
    public enum MessageCode
    {
        CurrencyIsExisted = 1,
        CurrencyNotExisted = 2
    }
}
