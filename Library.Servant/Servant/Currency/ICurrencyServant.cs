﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Currency.Model;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.Currency
{
    public interface ICurrencyServant
    {
        IndexResult Index(SearchModel Search);
        UpdateModel Detail(int ID);
        MessageModel Create(CreateModel Create);
        MessageModel Update(UpdateModel Update);
        MessageModel BatchDelete(BatchDelete Delete);
    }
}
