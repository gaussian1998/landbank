﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Currency.Model;
using Library.Entity.Edmx;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.Currency
{
    public class LocalCurrencyServant : ICurrencyServant
    {
        public MessageModel Create(CreateModel Create)
        {
            MessageModel _message = new MessageModel { IsSuccess = true, MessagCode = String.Empty };
            int existed = AS_Currency_Records.Count(x => x.Currency == Create.Currency);
            if (existed > 0)
            {
                _message.MessagCode = MessageCode.CurrencyIsExisted.ToString();
                _message.IsSuccess = false;
            }
            else
            {
                AS_Currency_Records.Create(new AS_Currency
                {
                    Currency = Create.Currency,
                    Country = Create.Country,
                    Currency_Name = Create.CurrencyName,
                    Ex_Rate_Type = Create.ExRateType,
                    Basis = Create.Basis,
                    Decimal = Create.Decimal,
                    Swift_Currency = Create.SwiftCurrency,
                    Truncation = Create.Truncation
                });
            }
            this.updateBranchCurrencyRate(Create);
            return _message;
        }

        public UpdateModel Detail(int ID)
        {
            UpdateModel _updateModel;
            _updateModel= AS_Currency_Records.First(x => x.ID == ID, entity => new UpdateModel {
                ID = entity.ID,
                Currency = entity.Currency,
                Country = entity.Country,
                CurrencyName = entity.Currency_Name,
                ExRateType = entity.Ex_Rate_Type,
                Basis = entity.Basis,
                Decimal = entity.Decimal,
                SwiftCurrency = entity.Swift_Currency,
                Truncation = entity.Truncation
            });
            //取得全部可設定分行
            List<SelectedModel> _allBranchs = StaticDataServant.GetBranchs();
            List<int> _listASBranchs = AS_Branch_Currency_Records.Find(x => x.Currency == _updateModel.Currency).Select(x=>(int)x.Branch_Code).ToList();
            //設定已選取的分行
            if (_listASBranchs != null && _listASBranchs.Count > 0)
            {
                _updateModel.SelectedBranchs = _allBranchs.Where(x => _listASBranchs.Contains(int.Parse(x.Value))).Select(m => new OptionModel<int, string>
                {
                    Value = int.Parse(m.Value),
                    Text = m.Name
                }).ToList();
            }
            else {
                _updateModel.SelectedBranchs = new List<OptionModel<int, string>>();
            }
            _updateModel.Branchs = this.GetBranchs(_updateModel.SelectedBranchs);
            return _updateModel;         
        }
        private  List<OptionModel<int, string>> GetBranchs(List<OptionModel<int, string>> selectedBranchs)
        {
            var selectedFunctionsID = selectedBranchs.Select(m => m.Value);
            return StaticDataServant.GetBranchs().Where(m =>(( int.Parse(m.Value) > 500 && int.Parse(m.Value) < 600) || int.Parse(m.Value)==92) && 
                !selectedFunctionsID.Contains(int.Parse(m.Value))).Select(entity=> new OptionModel<int, string>
            {
                Value = int.Parse(entity.Value),
                Text = entity.Name
            }).ToList();
        }
        public IndexResult Index(SearchModel Search)
        {
            IndexResult _result = new IndexResult
            {
                CurrentPage = Search.CurrentPage,
                PageSize = Search.PageSize,
                Page = AS_Currency_Records.Page(
                Search.CurrentPage,
                Search.PageSize,
                x => x.Currency,
                x => true,
                entity => new UpdateModel
                {
                    ID = entity.ID,
                    Currency = entity.Currency,
                    CurrencyName = entity.Currency_Name,
                    Country = entity.Country,
                    ExRateType = entity.Ex_Rate_Type,
                    Basis = entity.Basis,
                    Decimal = entity.Decimal,
                    SwiftCurrency = entity.Swift_Currency,
                    Truncation = entity.Truncation
                })
            };
            _result.Branchs = this.GetBranchs(new List<OptionModel<int, string>>());
            return _result;
        }

        public MessageModel Update(UpdateModel Update)
        {
            MessageModel _message = new MessageModel { IsSuccess = true, MessagCode = String.Empty };
            int checkExist = AS_Currency_Records.Count(x => x.ID == Update.ID);
            if (checkExist < 0)
            {
                _message.MessagCode = MessageCode.CurrencyNotExisted.ToString();
                _message.IsSuccess = false;
            }
            else
            {
                int existed = AS_Currency_Records.Count(x => x.Currency == Update.Currency && x.ID != Update.ID);
                if (existed > 0)
                {
                    _message.MessagCode = MessageCode.CurrencyIsExisted.ToString();
                    _message.IsSuccess = false;
                }
                else
                {
                    AS_Currency_Records.Update(x => x.ID == Update.ID, entity =>
                    {
                        entity.Currency = Update.Currency;
                        entity.Country = Update.Country;
                        entity.Currency_Name = Update.CurrencyName;
                        entity.Swift_Currency = Update.SwiftCurrency;
                        entity.Ex_Rate_Type = Update.ExRateType;
                        entity.Basis = Update.Basis;
                        entity.Decimal = Update.Decimal;
                        entity.Truncation = Update.Truncation;
                    });
                }
            }
            CreateModel _create = new CreateModel {
                Currency = Update.Currency,
                SelectedBranchCode = Update.SelectedBranchCode,
                LastUpdateBy = Update.LastUpdateBy
            };
            this.updateBranchCurrencyRate(_create);
            return _message;

        }

        public MessageModel BatchDelete(BatchDelete Delete)
        {
            MessageModel _message = new MessageModel { IsSuccess = true, MessagCode = "" };
            if (Delete.IDList.Count > 0)
            {
                foreach(int id in Delete.IDList)
                {
                    AS_Currency_Records.DeleteFirst(x => x.ID == id);
                }
            }
            return _message;
        }

        private void updateBranchCurrencyRate(CreateModel Create)
        {
            if (Create.SelectedBranchCode == null || Create.SelectedBranchCode.Count == 0) {
                return ;
            }
            //update branch code
            AS_RateRepository _rateRepo = RepositoryHelper.GetAS_RateRepository();
            //decimal branchCode = Convert.ToDecimal(Create.BranchCode);
            //var lastRateItem = _rateRepo.All().Where(x => x.Branch_Code == branchCode && x.Currency == Create.Currency).OrderByDescending(x => x.Input_Date).FirstOrDefault();
            var lastRateItem = _rateRepo.All().Where(x => x.Currency == Create.Currency).OrderByDescending(x => x.Input_Date).FirstOrDefault();

            //if (lastRateItem == null)
            //    return;
            decimal lastRate = 0;
            if(lastRateItem!=null)lastRate = Convert.ToDecimal(lastRateItem.Rate);
           
            if (Create != null && Create.SelectedBranchCode.Count > 0)
            {
                foreach (int _branchCode in Create.SelectedBranchCode)
                {
                    int foundCount = AS_Branch_Currency_Records.Count(x => x.Branch_Code == _branchCode );
                    if (foundCount > 0)
                    {
                        AS_Branch_Currency_Records.Update(x =>
                        x.Branch_Code == _branchCode ,
                        entity =>
                        {
                            entity.Rate = lastRate;
                            entity.Book_Type =(lastRateItem!=null ?lastRateItem.Book_Type:null);
                            entity.Currency = Create.Currency;
                            entity.Last_Updated_Time = DateTime.Now;
                            entity.Last_Updated_By = Create.LastUpdateBy;
                        });
                    }
                    else
                    {
                        //need to create branch currency
                        AS_Branch_Currency_Records.Create(new AS_Branch_Currency
                        {
                            Branch_Code = _branchCode,
                            Department = null,
                            Book_Type = (lastRateItem != null ? lastRateItem.Book_Type : null),
                            Currency = Create.Currency,
                            Rate = lastRate,
                            Last_Updated_By = Create.LastUpdateBy,
                            Last_Updated_Time = DateTime.Now
                        });
                    }
                }
            }
        }
    }
}
