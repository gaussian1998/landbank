﻿using System.Collections.Generic;
using Library.Entity.Edmx;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.PostsManagement.Models;

namespace Library.Servant.Servant.PostsManagement
{
    public interface IPostsManagementServant
    {
        IndexResult Index(SearchModel conditions);

        IndexResult FormIndex(string flowCode);

        SearchResult Search(string flowCode);

        VoidResult SaveForm(HeaderModel headerInfo);

        VoidResult SavePost(PostInfo postInfo);

        PostInfo Detail(PostInfo postInfo); //todo_julius

        VoidResult Submit(string flowCode);

        VoidResult Discard(string flowCode);

        VoidResult Delete(string postCode);

        List<AS_VW_Post_Report> GetReportContent(ReportSearchModel rsm);
    }
}
