﻿using System;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.PostsManagement.Models;
using Library.Servant.Servant.PostsManagement;
using Library.Entity.Edmx;
using System.Collections.Generic;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Common;

namespace Library.Servant.Servant.PostsManagement
{
    public class RemotePostsManagementServant : IPostsManagementServant
    {
        public VoidResult Delete(string postCode)
        {
            var result = ProtocolService.EncryptionPost<StringModel, VoidResult>
            (
                new StringModel { Value = postCode },
                Config.ServantDomain + "/PostsManagementServant/Delete"
            );

            return Tools.ExceptionConvert(result);
        }

        public PostInfo Detail(PostInfo postInfo)
        {
            var result = ProtocolService.EncryptionPost<PostInfo, PostInfo>
            (
                postInfo,
                Config.ServantDomain + "/PostsManagementServant/Detail"
            );

            return Tools.ExceptionConvert(result);
        }

        public VoidResult Discard(string flowCode)
        {
            var result = ProtocolService.EncryptionPost<StringModel, VoidResult>
            (
                new StringModel { Value = flowCode },
                Config.ServantDomain + "/PostsManagementServant/Discard"
            );

            return Tools.ExceptionConvert(result);
        }

        public IndexResult FormIndex(string flowCode)
        {
            throw new NotImplementedException();
        }

        public List<AS_VW_Post_Report> GetReportContent(ReportSearchModel rsm)
        {
            throw new NotImplementedException();
        }

        public IndexResult Index(SearchModel conditions)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, IndexResult>
            (
                conditions,
                Config.ServantDomain + "/PostsManagementServant/Index"
            );

            return Tools.ExceptionConvert(result);
        }

        public VoidResult SaveForm(HeaderModel headerInfo)
        {
            var result = ProtocolService.EncryptionPost<HeaderModel, VoidResult>
            (
                headerInfo,
                Config.ServantDomain + "/PostsManagementServant/SaveForm"
            );

            return Tools.ExceptionConvert(result);
        }

        public VoidResult SavePost(PostInfo postInfo)
        {
            var result = ProtocolService.EncryptionPost<PostInfo, VoidResult>
            (
                postInfo,
                Config.ServantDomain + "/PostsManagementServant/SavePost"
            );

            return Tools.ExceptionConvert(result);
        }

        public SearchResult Search(string flowCode)
        {
            var result = ProtocolService.EncryptionPost<StringModel, SearchResult>
            (
                new StringModel { Value = flowCode },
                Config.ServantDomain + "/PostsManagementServant/Search"
            );

            return Tools.ExceptionConvert(result);
        }

        public VoidResult Submit(string flowCode)
        {
            var result = ProtocolService.EncryptionPost<StringModel, VoidResult>
            (
                new StringModel { Value = flowCode },
                Config.ServantDomain + "/PostsManagementServant/Submit"
            );

            return Tools.ExceptionConvert(result);
        }
    }
}
