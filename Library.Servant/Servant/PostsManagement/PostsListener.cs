﻿using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Library.Entity.Edmx;
using Library.Servant.Enums;
using Library.Servant.Servant.ApprovalEventListener;

namespace Library.Servant.Servant.PostsManagement
{
    //todo_julius
    public class PostsListener : ExampleListener
    {
        public override bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            using (var trans = new TransactionScope()) {
                AS_Post_His_Records.Update(m => m.Flow_Code == formNo, entity => {
                    entity.Is_Active = true;
                });

                if (formNo.Substring(0, 4) == "P200") {
                    var list = AS_Post_His_Records.Find(m => m.Flow_Code == formNo, entity => entity);
                    foreach (var item in list) {
                        AS_Post_His_Records.Update(m => m.Flow_Code != formNo && item.Post_ID == m.Post_ID, entity => {
                            entity.Is_Active = false;
                        });
                    }                    
                }

                AS_Post_Header_Records.Update(m => m.Flow_Code == formNo, entity => {
                    entity.Transaction_Status = ((int)ApprovalStatus.Approved).ToString(); // 已核準
                });

                AS_Post_His_Records.Update(m => m.Flow_Code == formNo, entity => {
                    entity.Transaction_Status = (int)ApprovalStatus.Approved; // 已核準
                });

                trans.Complete();
            }

            return true;
        }

        public override bool OnReject(string formNo, string carry, AS_LandBankEntities context)
        {
            using (var trans = new TransactionScope()) {
                AS_Post_Header_Records.Update(m => m.Flow_Code == formNo, entity => {
                    entity.Transaction_Status = ((int)ApprovalStatus.RejectForEdit).ToString();
                });

                AS_Post_His_Records.Update(m => m.Flow_Code == formNo, entity => {
                    entity.Transaction_Status = (int)ApprovalStatus.RejectForEdit;
                });

                trans.Complete();
            }

            return true;
        }

        public override bool OnWithdraw(string formNo, string carry, AS_LandBankEntities context)
        {
            using (var trans = new TransactionScope()) {
                AS_Post_Header_Records.Update(m => m.Flow_Code == formNo, entity => {
                    entity.Transaction_Status = ((int)ApprovalStatus.CallbackForEdit).ToString();
                });

                AS_Post_His_Records.Update(m => m.Flow_Code == formNo, entity => {
                    entity.Transaction_Status = (int)ApprovalStatus.CallbackForEdit;
                });

                trans.Complete();
            }

            return true;
        }
    }
}
