﻿using System;
using System.Linq;
using Library.Entity.Edmx;
using System.Transactions;
using Library.Servant.Enums;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.PostsManagement.Models;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Library.Servant.Servant.PostsManagement
{
    public class LocalPostsManagementServant : IPostsManagementServant
    {
        public VoidResult Delete(string postCode)
        {
            AS_Post_His_Records.Delete(m => m.Post_ID == postCode);
            AS_Connection_Records.Delete(m => m.SubSystem_Code == postCode);

            return new VoidResult();
        }

        public PostInfo Detail(PostInfo postInfo)
        {
            var now = DateTime.Now;
            var prefix = "P" + now.ToString("yyyyMMdd");

            var count = AS_Post_His_Records.Max(m => m.Post_ID.IndexOf(prefix) == 0, m => m.Post_ID);
            var countNum = count == null ? 0 : int.Parse(count.Remove(0, 9));
            
            try {
                var links = AS_Connection_Records.Find(m => m.SubSystem_Code == postInfo.PostCode, link => new OptionModel<string, string>
                {
                    Text = link.Path,
                    Value = link.Supper_Connection
                });

                var fileName = AS_Upload_Detail_Records.Any(m => m.SubSystem_Code == postInfo.PostCode) ?
                               AS_Upload_Detail_Records.First(m => m.SubSystem_Code == postInfo.PostCode, file => file.File_Name) :
                               "";

                var post = AS_Post_His_Records.First(m => m.Post_ID == postInfo.PostCode && m.Flow_Code == postInfo.FlowCode, entity =>
                    new PostInfo
                    {
                        PostCode = entity.Post_ID,
                        FlowCode = postInfo.FlowCode,
                        NewFlowCode = postInfo.NewFlowCode,
                        AnnounceRange = new RangeModel<DateTime>
                        {
                            Start = entity.Post_Startdate,
                            End = entity.Post_Enddate
                        },
                        AnnounceDepartment = entity.Post_Department.ToString(),
                        Content = entity.Detail,
                        FileName = fileName,
                        DocumentType = entity.Main_Type.ToString(),
                        PrincipalID = entity.Created_By == null ? postInfo.PrincipalID : entity.Created_By.ToString(),
                        Title = entity.Subject
                    });

                post.Links = links;

                return post;
            }
            catch (Exception ex) {
                postInfo.PostCode = prefix + (++countNum).ToString("D3"); //todo_julius
                return postInfo;
            }
        }
        
        public IndexResult Index(SearchModel conditions)
        {
            // 為了把查詢區間調整為 xxxx/xx/xx 00:00:00 ~ yyyy/yy/yy 23:59:59
            var newStart = conditions.Start.Date;
            var newEnd = conditions.End.AddDays(1).AddSeconds(-1.0);
            var type = conditions.DocumentType == FormType.PostCreate || conditions.DocumentType == FormType.PostModified ?
                       conditions.DocumentType.GetPrefixCode() :
                       "";

            var flows = AS_Post_Header_Records.Find(m => string.IsNullOrEmpty(type) || m.Transaction_Type == type, entity => entity).Select(m => m.Flow_Code).ToList();
            
            var pageList = AS_Post_His_Records.Page(conditions.Page, conditions.PageSize, m => m.ID, //todo_julius
                m => (m.Created_By == conditions.currentUser || m.Is_Active == true) &&
                     (m.Post_Startdate >= newStart && m.Post_Startdate <= newEnd) &&
                     (flows.Contains(m.Flow_Code)),
                entity => new ItemResult
                {
                    AnnounceTitle = entity.Subject,
                    AnnounceDate = (DateTime)entity.Transaction_Datetime,
                    AnnounceDepartment = entity.Post_Department.ToString(),
                    PostCode = entity.Post_ID,
                    Principal = entity.Created_By.ToString(),
                    Type = entity.Main_Type.ToString(),
                    FlowCode = entity.Flow_Code,
                    Status = (ApprovalStatus)entity.Transaction_Status
                });

            return new IndexResult
            {
                Items = pageList.Items,
                TotalAmount = pageList.TotalAmount,
                SearchConditions = conditions
            };
        }

        public static IndexResult GetAnnouncements(int userId)
        {
            var today = DateTime.Now;
            //todo_julius
            var pageList = AS_Post_His_Records.Page(1, 10, m => m.ID, //todo_julius
                m => m.Is_Active == true,
                entity => new ItemResult {
                    AnnounceTitle = entity.Subject,
                    AnnounceDate = (DateTime)entity.Transaction_Datetime,
                    AnnounceDepartment = entity.Post_Department.ToString(),
                    PostCode = entity.Post_ID,
                    Principal = entity.Created_By.ToString(),
                    Type = entity.Main_Type.ToString(),
                    FlowCode = entity.Flow_Code,
                    Status = (ApprovalStatus)entity.Transaction_Status
                });

            return new IndexResult
            {
                Items = pageList.Items,
                TotalAmount = pageList.TotalAmount
            };
        }

        public VoidResult SaveForm(HeaderModel headerInfo)
        {
            var now = DateTime.Now;

            AS_Post_Header_Records.Create(new AS_Post_Header
            {
                Flow_Code = headerInfo.FlowCode,
                Transaction_Status = "0",//todo_julius
                Transaction_Type = headerInfo.FlowType.GetPrefixCode(),
                Transaction_Datetime = now,
                Created_By = headerInfo.CreateBy,
                Office_Branch = AS_Users_Records.First(m => m.ID == headerInfo.CreateBy).Branch_Code
            });

            return new VoidResult();
        }
        
        public VoidResult SavePost(PostInfo postInfo)
        {
            var now = DateTime.Now;
            var prefix = "P" + now.ToString("yyyyMMdd");

            var count = AS_Post_His_Records.Max(m => m.Post_ID.IndexOf(prefix) == 0, m => m.Post_ID);
            var countNum = count == null ? 0 : int.Parse(count.Remove(0, 9));

            using (var trans = new TransactionScope()) {
                var newPost = AS_Post_His_Records.Any(m => m.Post_ID == postInfo.PostCode && m.Flow_Code == postInfo.NewFlowCode) ?
                    AS_Post_His_Records.First(m => m.Post_ID == postInfo.PostCode && m.Flow_Code == postInfo.NewFlowCode) :
                    postInfo.Convert(postInfo.PostCode, now, ApprovalStatus.Create);

                AS_Connection_Records.Delete(m => m.SubSystem_Code == newPost.Post_ID);

                foreach (var link in postInfo.Links) {
                    AS_Connection_Records.Create(new AS_Connection
                    {
                        SubSystem_Code = newPost.Post_ID,
                        Path = link.Text,
                        Supper_Connection = link.Value
                    });
                }

                AS_Upload_Detail_Records.Delete(m => m.SubSystem_Code == newPost.Post_ID);
                AS_Upload_Detail_Records.Create(new AS_Upload_Detail
                    {
                        SubSystem_Code = newPost.Post_ID,
                        File_Name = postInfo.FileName
                    }
                );

                if (newPost.ID == 0) {
                    newPost.Post_ID = postInfo.PostCode; // 為了修改單
                    AS_Post_His_Records.Create(newPost);
                }
                else {
                    AS_Post_His_Records.Update(m => m.ID == newPost.ID, entity => {
                        entity.Main_Type = postInfo.DocumentType;
                        entity.Post_Department = postInfo.AnnounceDepartment;
                        entity.Subject = postInfo.Title;
                        entity.Detail = postInfo.Content;
                        entity.Post_Startdate = postInfo.AnnounceRange.Start;
                        entity.Post_Enddate = postInfo.AnnounceRange.End;
                    });
                }

                trans.Complete();
            }                

            return new VoidResult();
        }

        public SearchResult Search(string flowCode)
        {
            var pageList = AS_Post_His_Records.Page(1, 10, m => m.ID, //todo_julius
                m => true,
                entity => new SearchItemResult {
                    AnnounceTitle = entity.Subject,
                    AnnounceDate = (DateTime)entity.Transaction_Datetime,
                    AnnounceDepartment = entity.Post_Department.ToString(),
                    PostCode = entity.Post_ID,
                    Principal = entity.Created_By.ToString(),
                    Type = entity.Main_Type.ToString(),
                    FlowCode = entity.Flow_Code,
                });
            
            return new SearchResult
            {
                Items = pageList.Items,
                TotalAmount = pageList.TotalAmount
            };
        }

        public VoidResult Submit(string flowCode)
        {
            AS_Post_Header_Records.Update(m => m.Flow_Code == flowCode, entity => {
                entity.Transaction_Status = ((int)ApprovalStatus.Auditing).ToString();
            });

            AS_Post_His_Records.Update(m => m.Flow_Code == flowCode, entity => {
                entity.Transaction_Status = ((int)ApprovalStatus.Auditing);
            });

            return new VoidResult();
        }

        public List<AS_VW_Post_Report> GetReportContent(ReportSearchModel rsm)
        {
            return AS_VW_Post_Report_Records.
                Find(m => m.User_Name == rsm.Manager &&
                          (m.Start >= rsm.PostDateTime.Start && m.End <= rsm.PostDateTime.End)).
                ToList();
        }

        public VoidResult Discard(string flowCode)
        {
            var posts = AS_Post_His_Records.Find(m => m.Flow_Code == flowCode).Select(p => p.Post_ID);
            AS_Post_His_Records.Delete(m => m.Flow_Code == flowCode);
            AS_Connection_Records.Delete(m => posts.Any(p => p == m.SubSystem_Code));
            AS_Post_Header_Records.Delete(m => m.Flow_Code == flowCode);

            return new VoidResult();
        }

        public static string GetUserBranch(int id)
        {
            var code = AS_Users_Records.First(m => m.ID == id).Branch_Code;
            return AS_Code_Table_Records.First(m => m.Class == "C01" && m.Code_ID == code).Text;
        }
        
        public IndexResult FormIndex(string flowCode)
        {
            var userName = AS_Post_Header_Records.Any(m => m.Flow_Code == flowCode) ?
                AS_Post_Header_Records.First(m => m.Flow_Code == flowCode).AS_Users.User_Name :
                "";

            var pageList = AS_Post_His_Records.Page(1, 10, m => m.ID, //todo_julius
                m => m.Flow_Code == flowCode,
                entity => new ItemResult
                {
                    AnnounceTitle = entity.Subject,
                    AnnounceDate = (DateTime)entity.Transaction_Datetime,
                    AnnounceDepartment = entity.Post_Department.ToString(),
                    PostCode = entity.Post_ID,
                    Principal = userName, //todo_julius
                    Type = entity.Main_Type.ToString(),
                    FlowCode = flowCode
                });

            if (AS_Post_Header_Records.Any(m => m.Flow_Code == flowCode)) {
                var headerInfo = AS_Post_Header_Records.First(m => m.Flow_Code == flowCode);
                FormType type = FormType.Undefined;

                foreach (FormType item in Enum.GetValues(typeof(FormType))) {
                    if (item.GetPrefixCode() == headerInfo.Transaction_Type) {
                        type = item;
                    }
                }

                return new IndexResult
                {
                    Items = pageList.Items,
                    TotalAmount = pageList.TotalAmount,
                    HeaderInfo = new HeaderModel
                    {
                        FlowCode = headerInfo.Flow_Code,
                        FlowDate = headerInfo.Transaction_Datetime.Value,
                        ApprovalStatus = (ApprovalStatus)int.Parse(headerInfo.Transaction_Status),
                        FlowType = type
                    },
                    Conditions = new ConditionResult
                    {
                        IsSaved = headerInfo != null,
                        IsSubmited = headerInfo.Transaction_Status != "0"
                    }
                };
            }
            else {
                return new IndexResult();
            }
        }
    }
}
