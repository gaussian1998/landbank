﻿namespace Library.Servant.Servant.PostsManagement.Models
{
    public class ConditionResult
    {
        public bool IsSaved { get; set; }
        public bool IsSubmited { get; set; }
    }
}
