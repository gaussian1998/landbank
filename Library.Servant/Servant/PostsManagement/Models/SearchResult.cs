﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.PostsManagement.Models
{
    public class SearchResult : InvasionEncryption
    {
        public SearchResult()
        {
            Items = new List<SearchItemResult>();
            Condtiton = new SearchConditionModel();
        }

        public List<SearchItemResult> Items { get; set; }
        public int TotalAmount { get; set; }
        public SearchConditionModel Condtiton { get; set; }
    }
}
