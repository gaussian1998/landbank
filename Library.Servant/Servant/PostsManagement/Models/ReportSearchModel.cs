﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.PostsManagement.Models
{
    public class ReportSearchModel
    {
        public string Manager { get; set; }
        public RangeModel<string> CodeRange { get; set; }
        public RangeModel<DateTime> PostDateTime { get; set; }
    }
}
