﻿using System;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.PostsManagement.Models
{
    public class SearchConditionModel
    {
        public SearchConditionModel() {
            BranchRange = new RangeModel<string>();
            PostRange = new RangeModel<string>();
            AnnounceRange = new RangeModel<DateTime>();
        }

        public RangeModel<string> BranchRange { get; set; }
        public RangeModel<string> PostRange { get; set; }
        public RangeModel<DateTime> AnnounceRange { get; set; }
        public int DocumentType { get; set; }
    }
}
