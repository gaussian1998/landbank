﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.PostsManagement.Models
{
    public class IndexResult : InvasionEncryption
    {
        public IndexResult()
        {
            Items = new List<ItemResult>();
            HeaderInfo = new HeaderModel();
            Conditions = new ConditionResult();
            SearchConditions = new SearchModel();
        }

        public List<ItemResult> Items { get; set; }
        public int TotalAmount { get; set; }
        public HeaderModel HeaderInfo { get; set; }
        public ConditionResult Conditions { get; set; }
        public SearchModel SearchConditions { get; set; }
    }
}
