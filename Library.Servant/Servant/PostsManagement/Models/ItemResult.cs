﻿using System;
using Library.Interface;
using System.Collections.Generic;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using Library.Entity.Edmx;
using Library.Servant.Enums;

namespace Library.Servant.Servant.PostsManagement.Models
{
    public class ItemResult : InvasionEncryption
    {
        public string PostCode { get; set; }
        public string FlowCode { get; set; }
        public ApprovalStatus Status { get; set; }
        public DateTime AnnounceDate { get; set; }
        public string Type { get; set; }
        public string AnnounceTitle { get; set; }
        public string AnnounceDepartment { get; set; }
        public string Principal { get; set; }
        public string PrincipalName
        {
            get {
                var id = int.Parse(Principal);
                var userName = AS_Users_Records.Any(m => m.ID == id) ?
                    AS_Users_Records.First(m => m.ID == id).User_Name :
                    "";
                return userName;
            }
            set {
                Principal = value;
            }
        }

        public ItemResult()
        {

        }

        public ItemResult(AS_Post_His ef)
        {
            AnnounceTitle = ef.Subject;
            AnnounceDate = (DateTime)ef.Transaction_Datetime;
            AnnounceDepartment = ef.Post_Department.ToString();
            PostCode = ef.Post_ID;
            Principal = ef.Created_By.ToString();
            Type = ef.Main_Type.ToString();
            FlowCode = ef.Flow_Code;
            Status = (ApprovalStatus)ef.Transaction_Status;
        }
    }
}
