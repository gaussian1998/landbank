﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;
using Library.Servant.Enums;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.PostsManagement.Models
{
    public class HeaderModel : InvasionEncryption
    {
        private string _status;

        public int Page { get; set; }
        public int PageSize { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string FlowCode { get; set; }
        public DateTime FlowDate { get; set; }
        public FormType FlowType { get; set; }
        public string FlowName { get; set; }
        public int CreateBy { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }
    }
}
