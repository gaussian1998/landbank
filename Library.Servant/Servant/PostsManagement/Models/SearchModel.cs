﻿using System;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.Enums;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.PostsManagement.Models
{
    public class SearchModel : InvasionEncryption
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int UserID { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public FormType DocumentType { get; set; }
        public string Status { get; set; }
        public int currentUser { get; set; }
    }
}
