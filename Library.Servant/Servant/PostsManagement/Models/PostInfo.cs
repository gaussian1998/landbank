﻿using System;
using System.Collections.Generic;
using Library.Entity.Edmx;
using Library.Servant.Communicate;
using Library.Servant.Enums;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.PostsManagement.Models
{
    public class PostInfo : InvasionEncryption
    {
        public string FlowCode { get; set; }
        public string NewFlowCode { get; set; }
        public string PostCode { get; set; }
        public string DocumentType { get; set; }
        public string AnnounceDepartment { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string FileName { get; set; }
        public RangeModel<DateTime> AnnounceRange { get; set; }
        public string PrincipalID { get; set; }
        public string Principal
        {
            get {
                var id = int.Parse(PrincipalID);
                var userName = AS_Users_Records.Any(m => m.ID == id) ?
                    AS_Users_Records.First(m => m.ID == id).User_Name :
                    "";
                return userName;
            }
        }
        public List<OptionModel<string, string>> Links { get; set; }

        public PostInfo() { }

        public PostInfo(AS_Post_His ef, string originalCode, string newCode, string principleID)
        {
            PostCode = ef.Post_ID;
            FlowCode = originalCode;
            NewFlowCode = newCode;
            AnnounceRange = new RangeModel<DateTime>
            {
                Start = ef.Post_Startdate,
                End = ef.Post_Enddate
            };
            AnnounceDepartment = ef.Post_Department.ToString();
            Content = ef.Detail;
            DocumentType = ef.Main_Type.ToString();
            PrincipalID = ef.Created_By == null ? principleID : ef.Created_By.ToString();
            Title = ef.Subject;
        }

        public AS_Post_His Convert(string id, DateTime now, ApprovalStatus status)
        {
            return new AS_Post_His
            {
                Post_ID = id,
                Subject = Title,
                Flow_Code = NewFlowCode,
                Detail = Content,
                Transaction_Datetime = now,
                Transaction_Status = (int)status,
                Post_Department = AnnounceDepartment,
                Main_Type = DocumentType,
                Post_Startdate = AnnounceRange.Start,
                Post_Enddate = AnnounceRange.End,
                Created_By = int.Parse(PrincipalID),
                Is_Active = false
            };
        }
    }
}
