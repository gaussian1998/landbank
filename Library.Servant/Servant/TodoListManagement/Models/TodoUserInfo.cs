﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.TodoListManagement.Models
{
    public class TodoUserInfo
    {
        public string BranchCode { get; set; }
        public string DepartmentCode { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public bool IsNotifiedOnCreate { get; set; }
        public bool IsNotifiedForAlert { get; set; }
        public bool IsNotifiedOnFinish { get; set; }

        public AS_Mail_User Convert()
        {
            return new AS_Mail_User
            {
                ToDoList_Branch = BranchCode,
                ToDoList_Department = DepartmentCode,
                User_ID = UserID,
                Creat_Mail = IsNotifiedOnCreate,
                Remind_Mail = IsNotifiedForAlert,
                Finished_Mail = IsNotifiedOnFinish,
                Create_Time = DateTime.Now,
                Last_Updated_Time = DateTime.Now
            };
        }
    }
}
