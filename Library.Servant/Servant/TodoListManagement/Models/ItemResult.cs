﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Entity.Edmx;
using Library.Servant.Enums;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.TodoListManagement.Models
{
    public class ItemResult
    {
        private IEnumerable<OptionModel<string,string>> _statusList// 之後會由Servant取得
        {
            get {
                return new List<OptionModel<string, string>>
                {
                    new OptionModel<string,string> { Text="作業中", Value="0" },
                    new OptionModel<string,string> { Text="已完成", Value="1" },
                    new OptionModel<string,string> { Text="作廢", Value="2" },
                    new OptionModel<string,string> { Text="結案", Value="3" }
                };
            }
        }

        public string TodoListCode { get; set; }
        public string TodoTitile { get; set; }
        public DateTime CreateDateTime { get; set; }
        public int TodoUser { get; set; }
        public string TodoUserName
        {
            get {
                return AS_Users_Records.First(m => m.ID == TodoUser).User_Name;
            }
        }
        public string Status { get; set; }
        public string StatusName
        {
            get {
                return _statusList.First(m => m.Value == Status).Text;
            }
        }
    }
}
