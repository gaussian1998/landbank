﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Entity.Edmx;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.TodoListManagement.Models
{
    public class TodoListInfo : InvasionEncryption
    {
        public TodoListInfo()
        {
            TodoUsers = new List<TodoUserInfo>();
            Links = new List<OptionModel<string, string>>();
            TodoPeriod = new RangeModel<DateTime> {
                Start = DateTime.Now,
                End = DateTime.Now
            };
            AuditingFlowList = new List<AuditFlowModel>();
        }

        public TodoListInfo(AS_ToDoList ef)
        {
            TodoCode = ef.ToDoList_Code;
            Status = ef.ToDoList_Status;
            Type = ef.ToDoList_Type;
            AssignUserID = ef.User_ID;
            AssignUser = AS_Users_Records.First(m => m.ID == ef.User_ID).User_Name;
            Title = ef.Subject;
            Content = ef.Detail;
            TodoUsers = ef.AS_Mail_User.Select(u => new TodoUserInfo
            {
                BranchCode = u.ToDoList_Branch,
                DepartmentCode = u.ToDoList_Department,
                UserID = u.User_ID,
                UserName = AS_Users_Records.First(m => m.ID == u.User_ID).User_Name,
                IsNotifiedOnCreate = u.Creat_Mail,
                IsNotifiedForAlert = u.Remind_Mail,
                IsNotifiedOnFinish = u.Finished_Mail
            }).ToList();
            RemindDays = ef.RemindDays;
            TodoPeriod = new RangeModel<DateTime>
            {
                Start = ef.ToDoList_Startdate,
                End = ef.ToDoList_Enddate
            };
            AuditingFlowList = ef.AS_Checked_SubSystem.Select(m => new AuditFlowModel
            {
                CreateUser = m.Created_By,
                CurrentApproal = m.Now_User_ID.Value,
                FlowCode = m.Flow_Code,
                FlowStatus = m.Flow_status
            }).ToList();
        }

        public string TodoCode { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public int AssignUserID { get; set; }
        public string AssignUser { get; set; }
        public List<TodoUserInfo> TodoUsers { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public List<OptionModel<string, string>> Links { get; set; }
        public RangeModel<DateTime> TodoPeriod { get; set; }
        public int RemindDays { get; set; }
        public List<AuditFlowModel> AuditingFlowList { get; set; }
        public int UpdateUser { get; set; }
        public string UpdateUserName { get; set; }
        public string FileName { get; set; }

        public AS_ToDoList Convert()
        {
            return new AS_ToDoList
            {
                ToDoList_Code = this.TodoCode,
                User_ID = this.AssignUserID,
                ToDoList_Branch = "",
                ToDoList_Department = "",
                ToDoList_Startdate = this.TodoPeriod.Start,
                ToDoList_Enddate = this.TodoPeriod.End,
                Subject = this.Title,
                Detail = this.Content,
                Create_Time = DateTime.Now,
                Created_By = this.AssignUserID,
                Last_Updated_Time = DateTime.Now,
                Last_Updated_By = this.AssignUserID,
                ToDoList_Status = this.Status,
                ToDoList_Type = this.Type,
                RemindDays = this.RemindDays
            };
        }
    }
}
