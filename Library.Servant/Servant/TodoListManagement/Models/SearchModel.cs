﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.TodoListManagement.Models
{
    public class SearchModel : InvasionEncryption
    {
        public int UserId { get; set; }
        public int PageSize { get; set; }
        public int Page { get; set; }
        public RangeModel<string> BranchRange { get; set; }
        public RangeModel<string> TodoCodeRange { get; set; }
        public string Status { get; set; }
    }
}
