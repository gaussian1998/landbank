﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.TodoListManagement.Models
{
    public class IndexResult : InvasionEncryption
    {
        public List<ItemResult> Items { get; set; }
        public int TotalAmount { get; set; }
        public SearchModel Conditions { get; set; }
    }
}
