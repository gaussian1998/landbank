﻿using Library.Entity.Edmx;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.TodoListManagement.Models
{
    public class ReportResultModel : InvasionEncryption
    {
        public string Code { get; set; }
        public string UserName { get; set; }
        public System.DateTime Start { get; set; }
        public System.DateTime End { get; set; }
        public System.DateTime CreateTime { get; set; }
        public string Status { get; set; }
        public string Subject { get; set; }

        public ReportResultModel(AS_VW_TodoList_Report ef)
        {
            Code = ef.Code;
            UserName = ef.User_Name;
            Start = ef.Start;
            End = ef.End;
            CreateTime = ef.Create_Time;
            Status = ef.Status;
            Subject = ef.Subject;
        }
    }
}