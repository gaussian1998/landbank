﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Entity.Edmx;
using Library.Servant.Enums;

namespace Library.Servant.Servant.TodoListManagement.Models
{
    public class AuditFlowModel
    {
        public string FlowCode { get; set; }
        public string FlowStatus { get; set; }
        public int CurrentApproal { get; set; }
        public int CreateUser { get; set; }
        public string CurrentApproalName {
            get {
                return AS_Users_Records.First(m => m.ID == CurrentApproal).User_Name;
            }
        }

        public AS_Checked_SubSystem Convert(int userID)
        {
            return new AS_Checked_SubSystem
            {
                Flow_Code = FlowCode,
                Now_User_ID = userID,
                Flow_status = FlowStatus,
                Created_By = CreateUser,
                Create_Time = DateTime.Now,
                Last_Updated_By = userID,
                Last_Updated_Time = DateTime.Now
            };
        }
    }
}
