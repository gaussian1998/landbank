﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.TodoListManagement.Models
{
    public class ReportSearchModel : InvasionEncryption
    {
        public string AssignUser { get; set; }
        public RangeModel<string> AssignNumberRange { get; set; }
        public RangeModel<DateTime> AssignDateTime { get; set; }
        public bool IsNotFinished { get; set; }
        public string StatusCode { get; set; }
    }
}
