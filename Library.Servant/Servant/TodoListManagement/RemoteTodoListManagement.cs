﻿using System;
using System.Collections.Generic;
using Library.Servant.Servant.Common;
using Library.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.TodoListManagement.Models;

namespace Library.Servant.Servant.TodoListManagement
{
    public class RemoteTodoListManagement : ITodoListManagement
    {
        public VoidResult Delete(string code)
        {
            var result = ProtocolService.EncryptionPost<StringModel, VoidResult>
            (
                new StringModel { Value=code },
                Config.ServantDomain + "/TodoListManagementServant/Delete"
            );

            return Tools.ExceptionConvert(result);
        }

        public TodoListInfo Detail(string code)
        {
            var result = ProtocolService.EncryptionPost<StringModel, TodoListInfo>
            (
                new StringModel { Value = code },
                Config.ServantDomain + "/TodoListManagementServant/Detail"
            );

            return Tools.ExceptionConvert(result);
        }

        public List<ReportResultModel> GetReportContent(ReportSearchModel rsm)
        {
            //var result = ProtocolService.EncryptionPost<ReportSearchModel, ReportResultModel>
            //(
            //    new ReportSearchModel { Value = code },
            //    Config.ServantDomain + "/TodoListManagementServant/Detail"
            //);

            //return Tools.ExceptionConvert(result);
            throw new Exception();
        }


        public IndexResult Index(SearchModel condition)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, IndexResult>
               (
                   condition,
                   Config.ServantDomain + "/TodoListManagementServant/Index"
               );

            return Tools.ExceptionConvert(result);
        }

        public IndexResult QueryIndex(SearchModel condition, int userID)
        {
            condition.UserId = userID;
            var result = ProtocolService.EncryptionPost<SearchModel, IndexResult>
            (
                condition,
                Config.ServantDomain + "/TodoListManagementServant/QueryIndex"
            );

            return Tools.ExceptionConvert(result);
        }

        public VoidResult SaveTodoTask(TodoListInfo info)
        {
            var result = ProtocolService.EncryptionPost<TodoListInfo, VoidResult>
            (
                info,
                Config.ServantDomain + "/TodoListManagementServant/SaveTodoTask"
            );

            return Tools.ExceptionConvert(result);
        }


    }
}
