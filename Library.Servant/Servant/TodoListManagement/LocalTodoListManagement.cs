﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Library.Entity.Edmx;
using Library.Servant.Enums;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.TodoListManagement.Models;

namespace Library.Servant.Servant.TodoListManagement
{
    public class LocalTodoListManagement : ITodoListManagement
    {
        public VoidResult Delete(string code)
        {
            try {
                AS_ToDoList_Records.Delete(m => m.ToDoList_Code == code);   
            }
            catch (Exception ex) {
                throw new Exception("Delete Fail", ex);
            }

            return new VoidResult();
        }

        public TodoListInfo Detail(string code)
        {
            try {
                if (string.IsNullOrEmpty(code)) {
                    var result = new TodoListInfo();
                    var prefix = "S" + DateTime.Now.ToString("yyyyMMdd");
                    var count = AS_ToDoList_Records.Max(m => m.ToDoList_Code.IndexOf(prefix) == 0, m => m.ToDoList_Code);
                    var countNum = count == null ? 0 : int.Parse(count.Remove(0, 9));

                    result.TodoCode = prefix + (++countNum).ToString("D3");

                    return result;
                }

                var links = AS_Connection_Records.Find(m => m.SubSystem_Code == code, entity => new OptionModel<string, string> {
                    Text = entity.Path,
                    Value = entity.Supper_Connection
                });
                
                var todoTask = new TodoListInfo(AS_ToDoList_Records.First(m => m.ToDoList_Code == code));

                todoTask.Links = links;
                todoTask.FileName = AS_Upload_Detail_Records.Any(m => m.SubSystem_Code == todoTask.TodoCode) ?
                                    AS_Upload_Detail_Records.First(m => m.SubSystem_Code == todoTask.TodoCode, file => file.File_Name) :
                                    "";

                return todoTask;
            }
            catch (Exception ex) {
                return new TodoListInfo();
            }
        }

        public IndexResult Index(SearchModel condition)
        {
            try {
                var pageList = AS_ToDoList_Records.Page(condition.Page, condition.PageSize, m => m.ID, //todo_julius
                    m => /*(String.Compare(m.ToDoList_Branch, condition.BranchRange.Start) > 0 && String.Compare(m.ToDoList_Branch, condition.BranchRange.End) < 0) &&
                         (String.Compare(m.ToDoList_Branch, condition.BranchRange.Start) > 0 && String.Compare(m.ToDoList_Branch, condition.BranchRange.End) < 0) &&*/
                         (condition.Status == null || m.ToDoList_Status == condition.Status),
                    entity => new ItemResult
                    {
                        TodoListCode = entity.ToDoList_Code,
                        TodoTitile = entity.Subject,
                        CreateDateTime = entity.Create_Time,
                        Status =  entity.ToDoList_Status,
                        TodoUser = entity.User_ID
                    });

                return new IndexResult {
                    Items = pageList.Items,
                    TotalAmount = pageList.TotalAmount,
                    Conditions = condition
                };
            }
            catch (Exception) {
                return new IndexResult();
            }
        }

        public IndexResult QueryIndex(SearchModel condition, int userID)
        {
            try {
                var pageList = AS_ToDoList_Records.Page(condition.Page, condition.PageSize, m => m.ID, //todo_julius
                    m => /*(String.Compare(m.ToDoList_Branch, condition.BranchRange.Start) > 0 && String.Compare(m.ToDoList_Branch, condition.BranchRange.End) < 0) &&
                         (String.Compare(m.ToDoList_Branch, condition.BranchRange.Start) > 0 && String.Compare(m.ToDoList_Branch, condition.BranchRange.End) < 0) &&*/
                         ((m.AS_Mail_User.Any(u => u.User_ID == userID) || m.User_ID == userID) && m.ToDoList_Status == condition.Status) ||
                         (condition.Status == "3" || condition.Status == "2"),
                    entity => new ItemResult
                    {
                        TodoListCode = entity.ToDoList_Code,
                        TodoTitile = entity.Subject,
                        CreateDateTime = entity.Create_Time,
                        Status = entity.ToDoList_Status,
                        TodoUser = entity.User_ID
                    });

                return new IndexResult
                {
                    Items = pageList.Items,
                    TotalAmount = pageList.TotalAmount,
                    Conditions = condition
                };
            }
            catch (Exception) {
                return new IndexResult();
            }
        }
        
        public VoidResult SaveTodoTask(TodoListInfo info)
        {
            try {
                var todoInfo = info.Convert();
                var dic = AS_Users_Records.FindAll(m => new { m.ID, m.Department_Code, m.Branch_Code }).
                          ToDictionary(k => k.ID, v => new { v.Branch_Code, v.Department_Code });

                foreach (var item in info.TodoUsers) {
                    var user = item.Convert();
                    user.ToDoList_Branch = dic[item.UserID].Branch_Code;
                    user.ToDoList_Department = dic[item.UserID].Department_Code;
                    user.Created_By = todoInfo.Created_By;
                    user.Last_Updated_By = todoInfo.Last_Updated_By;
                    todoInfo.AS_Mail_User.Add(user);
                }

                using (var ts = new TransactionScope()) {
                    if (AS_ToDoList_Records.Any(m => m.ToDoList_Code == info.TodoCode)) {
                        AS_Mail_User_Records.Delete(m => m.AS_ToDoList.ToDoList_Code == info.TodoCode);
                        AS_Connection_Records.Delete(m => m.SubSystem_Code == info.TodoCode);
                        AS_Upload_Detail_Records.Delete(m => m.SubSystem_Code == info.TodoCode);

                        AS_ToDoList_Records.Update(m => m.ToDoList_Code == info.TodoCode, entity => {
                            entity.Subject = info.Title;
                            entity.Detail = info.Content;
                            entity.ToDoList_Status = info.Status;
                            entity.ToDoList_Type = info.Type;
                            entity.RemindDays = info.RemindDays;
                        });

                        AS_ToDoList_Records.Update(m => m.ToDoList_Code == info.TodoCode, entity => {
                            foreach (var item in todoInfo.AS_Mail_User) {
                                entity.AS_Mail_User.Add(item);
                            }
                        });                        
                    }
                    else {
                        AS_ToDoList_Records.Create(todoInfo);
                    }

                    // 連結
                    foreach (var link in info.Links) {
                        AS_Connection_Records.Create(new AS_Connection
                        {
                            SubSystem_Code = info.TodoCode,
                            Path = link.Text,
                            Supper_Connection = link.Value,
                            Last_Updated_By = info.AssignUserID,
                            Last_Updated_Time = DateTime.Now
                        });
                    }
                                 
                    // 檔案       
                    AS_Upload_Detail_Records.Create(new AS_Upload_Detail
                    {
                        SubSystem_Code = info.TodoCode,
                        File_Name = info.FileName
                    });

                    ts.Complete();
                }
            }
            catch (Exception ex) {
                throw new Exception("Save fail", ex);
            }

            return new VoidResult();
        }

        public static IndexResult GetTodoList(int userId)
        {
            var pageList = AS_ToDoList_Records.Page(1, 10, m => m.ID, 
                    m => (m.AS_Mail_User.Any(u => u.User_ID == userId) || m.User_ID == userId) &&
                    (m.ToDoList_Status != "3" && m.ToDoList_Status != "2"),
                    entity => new ItemResult
                    {
                        TodoListCode = entity.ToDoList_Code,
                        TodoTitile = entity.Subject,
                        CreateDateTime = entity.Create_Time,
                        Status = entity.ToDoList_Status,
                        TodoUser = entity.User_ID
                    });

            return new IndexResult
            {
                Items = pageList.Items,
                TotalAmount = pageList.TotalAmount
            };
        }

        public List<ReportResultModel> GetReportContent(ReportSearchModel rsm)
        {
            return AS_VW_TodoList_Report_Records.
                Find(m => m.User_Name == rsm.AssignUser &&
                          (m.Start >= rsm.AssignDateTime.Start && m.End <= rsm.AssignDateTime.End)).
                Select(m => new ReportResultModel(m)).
                ToList();
        }
    }
}
