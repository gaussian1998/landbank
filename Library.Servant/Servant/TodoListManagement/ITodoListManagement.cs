﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.TodoListManagement.Models;
using Library.Servant.Servant.Common;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.TodoListManagement
{
    public interface ITodoListManagement
    {
        IndexResult Index(SearchModel condition);

        IndexResult QueryIndex(SearchModel condition, int userID);

        TodoListInfo Detail(string code);
        
        VoidResult Delete(string code);

        VoidResult SaveTodoTask(TodoListInfo info);

        List<ReportResultModel> GetReportContent(ReportSearchModel rsm);
    }
}
