﻿using Library.Entity.Edmx;

namespace Library.Servant.Servant.ApprovalEventListener
{
    public class ExampleListener : IEventListener
    {
        public virtual bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            return true;
        }

        public virtual bool OnRevoke(string formNo, string carry, AS_LandBankEntities context)
        {
            return true;
        }

        public virtual bool OnApproval(string formNo, string carry, AS_LandBankEntities context)
        {
            return true;
        }

        public virtual bool OnReject(string formNo, string carry, AS_LandBankEntities context)
        {
            return true;
        }

        public virtual bool OnWithdraw(string formNo, string carry, AS_LandBankEntities context)
        {
            return true;
        }
    }

    public class ExamplePartialViewListener : ExampleListener, IPartialViewProvider
    {
        public PartialViewModel PartialView(string FormNo, string carryData)
        {
            return new PartialViewModel { ControllerName = "Example", ActionName = "FormPartialView", Data = carryData };
        }
    }

    public class ExampleHtmlGetViewListener : ExampleListener, IHttpGetProvider
    {
        public HttpGetViewModel GetUrl(string FormNo, string carryData)
        {
            return new HttpGetViewModel { ControllerName = "Example", ActionName = "Form", Parameter = "a=1&b=2" };
        }
    }

    public class ExampleFastReportViewListener : ExampleListener, IFastReportProvider
    {
        public FastReportViewModel FastReport(string FormNo, string carryData)
        {
            return new FastReportViewModel { ReportFileName = "A.frx" };
        }
    }
}
