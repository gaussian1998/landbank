﻿namespace Library.Servant.Servant.ApprovalEventListener
{
    public class PartialViewModel
    {
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string Data { get; set; }
    }
}
