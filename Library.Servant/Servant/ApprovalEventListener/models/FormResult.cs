﻿using Library.Servant.Communicate;
using Newtonsoft.Json;

namespace Library.Servant.Servant.ApprovalEventListener.models
{
    public class FormResult :  InvasionEncryption
    {
        public int Type { get; set; }
        public string Data { get; set; }

        public FormResult()
        {}
       
        private FormResult(PartialViewModel model)
        {
            Type = 1;
            Data = JsonConvert.SerializeObject(model);
        }

        private FormResult(HttpGetViewModel model)
        {
            Type = 2;
            Data = JsonConvert.SerializeObject(model);
        }

        private FormResult(FastReportViewModel model)
        {
            Type = 3;
            Data = JsonConvert.SerializeObject(model);
        }

        private FormResult(NullViewModel model)
        {
            Type = 100;
            Data = JsonConvert.SerializeObject(model);
        }

        public static FormResult Instance(IEventListener Listener, string FormNo, string CarryData)
        {
            if (Listener is IPartialViewProvider)
            {
                var provider = Listener as IPartialViewProvider;
                return new FormResult(provider.PartialView(FormNo, CarryData));
            }
            else if (Listener is IHttpGetProvider)
            {
                var provider = Listener as IHttpGetProvider;
                return new FormResult(provider.GetUrl(FormNo,CarryData));
            }
            else if (Listener is IFastReportProvider)
            {
                var provider = Listener as IFastReportProvider;
                return new FormResult(provider.FastReport(FormNo, CarryData));
            }
            else
            {
                return new FormResult(new NullViewModel { });
            }
        }
    }
}
