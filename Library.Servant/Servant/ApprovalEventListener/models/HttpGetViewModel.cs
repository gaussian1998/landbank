﻿
namespace Library.Servant.Servant.ApprovalEventListener
{
    public class HttpGetViewModel
    {
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string Parameter { get; set; }
    }
}
