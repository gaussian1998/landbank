﻿using System.Collections.Generic;
using Library.Servant.Servant.UserRole;
using Library.Servant.Servant.PostsManagement;
using Library.Servant.Servant.MP.MPEventListener;
using Library.Servant.Servant.Build.BuildEventListener;
using Library.Servant.Servant.AccountingDeprnVerify;
using Library.Servant.Servant.Land.LandEventListener;
using Library.Servant.Servant.AccountingVerify;
using Library.Servant.Servant.MU.MUEventListener;
using Library.Servant.Servant.AccountingSubject;
using Library.Servant.Servant.Overdue.OverdueEventListener;
using Library.Servant.Servant.Integration.IntegrationEventListener;
using Library.Servant.Servant.Activation.ActivationEventListener;
using Library.Servant.Servant.Inspect.InspectEventListener;
using Library.Servant.Servant.Evaluation.EvaluationEventListener;
using Library.Servant.Servant.Inspect.InspectAssignEventListener;
using Library.Servant.Servant.LeaseRights.LeaseRightsEventListener;

namespace Library.Servant.Servant.ApprovalEventListener
{
    public static class EventListener
    {
        public static Dictionary<string, IEventListener> Map { get; set; }

        static EventListener()
        {
            Map = new Dictionary<string, IEventListener>();
            Map.Add("example", new ExampleListener());
            Map.Add("example_partial_view", new ExamplePartialViewListener());
            Map.Add("example_fast_report", new ExampleFastReportViewListener());
            Map.Add("example_http_get", new ExampleHtmlGetViewListener());

            Map.Add("DeprnVerify", new DeprnVerifyListener());
            Map.Add("AccountVerify", new AccountVerifyListener());

            Map.Add("UserRole", new UserRoleListener() );
            Map.Add("Posts", new PostsListener());

            Map.Add("MPAdded", new MPAddedEventListener());
            Map.Add("MPMaintain",new MPMaintainEventListener());
            Map.Add("MPDisposal", new MPDisposalEventListener());
            Map.Add("MPTransfer",new MPTransferEventListener());
            Map.Add("MPDeprn", new MPDeprnEventListener());
            Map.Add("MPInventoryNotice", new MPInventoryNoticeEventListener());
            Map.Add("MPInventoryReciprocation", new MPInventoryReciprocationEventListener());
            Map.Add("MPInventorySummary", new MPInventorySummaryEventListener());
            Map.Add("MPScrappedNotice", new MPScrappedNoticeEventListener());
            Map.Add("MPScrappedReciprocation", new MPScrappedReciprocationEventListener());

            Map.Add("LeaseRightsAdded", new LeaseRightsAddedEventListener());
            Map.Add("LeaseRightsMaintain", new LeaseRightsMaintainEventListener());
            Map.Add("LeaseRightsDisposal", new LeaseRightsDisposalEventListener());
            Map.Add("LeaseRightsDeprn", new LeaseRightsDeprnEventListener());

            Map.Add("LandAdded", new LandAddedEventListener());
            Map.Add("LandAssets", new LandAssetsEventListener());
            Map.Add("LandCost", new MPDisposalEventListener());
            Map.Add("LandSplit", new MPTransferEventListener());
            Map.Add("LandConsolidation", new MPDeprnEventListener());


            Map.Add("BuildAdded", new BuildAddedEventListener());
            Map.Add("BuildMaintain", new BuildMaintainEventListener());
            Map.Add("BuildDisposal", new BuildDisposalEventListener());
            Map.Add("BuildReclassify", new BuildReclassifyEventListener());
            Map.Add("BuildLifeChange", new BuildLifeChangeEventListener());


            Map.Add("BuildMPAdded", new BuildMPAddedEventListener());
            Map.Add("BuildMPMaintain", new BuildMPMaintainEventListener());
            Map.Add("BuildMPDisposal", new BuildMPDisposalEventListener());
            Map.Add("BuildMPReclassify", new BuildMPReclassifyEventListener());
            Map.Add("BuildMPLifeChange", new BuildDisposalEventListener());

            //ActivationEventListener
            Map.Add("Activation", new ActivationEventListener());
            //Map.Add("ActivationAssign", new ActivationAssignEventListener());

            Map.Add("Inspect", new InspectEventListener());
            Map.Add("InspectAssign", new InspectAssignEventListener());

            //BuildEvaluation
            Map.Add("BuildEvaluation", new BuildEvaluationEventListener());

            Map.Add("MU", new MUEventListener());
            Map.Add("AccountingSubjectListener", new AccountingSubjectVerifyListener());
            Map.Add("Overdue", new OverdueEventListener());

            Map.Add("IntegrationAdded", new IntegrationAddEventListener());
            Map.Add("IntegrationMaintain", new IntegrationMaintainEventListener());
        }
    }
}
