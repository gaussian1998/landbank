﻿using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalEventListener.models;
using System;

namespace Library.Servant.Servant.ApprovalEventListener
{
    public class RegisterListener
    {
        private string ListenerName { get; set; }
        private string Carry { get; set; }
        private string FormNo { get; set; }
        private IEventListener Listener { get; set; }

        public static RegisterListener Instance(int ID)
        {
            var result = AS_Flow_Open_Records.First(m => m.ID == ID, entity => new RegisterListener
            {
                ListenerName = entity.Event_Listener,
                Carry = entity.Carry,
                FormNo = entity.Form_No
            });

            if (EventListener.Map.ContainsKey(result.ListenerName))
                result.Listener = EventListener.Map[result.ListenerName];
            else
                throw new Exception("編程錯誤,模組必須註冊專用的IEventListener實體,否則簽核流程無法做事件通知");

            return result;
        }

        public void OnApproval(AS_LandBankEntities db)
        {
            Listener.OnApproval(FormNo, Carry, db);
        }

        public void OnReject(AS_LandBankEntities db)
        {
            Listener.OnReject(FormNo, Carry, db);
        }

        public void OnRevoke(AS_LandBankEntities db)
        {
            Listener.OnRevoke(FormNo, Carry, db);
        }

        public void OnWithdraw(AS_LandBankEntities db)
        {
            Listener.OnWithdraw(FormNo, Carry, db);
        }

        public void OnFinish(AS_LandBankEntities db)
        {
            Listener.OnFinish(FormNo, Carry, db);
        }

        public FormResult GetForm()
        {
            return FormResult.Instance(Listener, FormNo, Carry);
        }
    }
}
