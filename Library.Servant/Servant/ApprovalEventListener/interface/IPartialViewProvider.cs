﻿
namespace Library.Servant.Servant.ApprovalEventListener
{
    interface IPartialViewProvider
    {
        PartialViewModel PartialView(string FormNo, string carryData);
    }
}
