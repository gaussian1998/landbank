﻿using Library.Entity.Edmx;

namespace Library.Servant.Servant.ApprovalEventListener
{
    public interface IEventListener
    {
        bool OnApproval(string formNo, string carry, AS_LandBankEntities context);
        bool OnRevoke(string formNo, string carry, AS_LandBankEntities context);
        bool OnReject(string formNo, string carry, AS_LandBankEntities context);
        bool OnWithdraw(string formNo, string carry, AS_LandBankEntities context);
        bool OnFinish(string formNo, string carry, AS_LandBankEntities context);
    }
}
