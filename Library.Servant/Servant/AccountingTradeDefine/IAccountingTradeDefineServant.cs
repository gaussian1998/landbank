﻿using Library.Servant.Servant.AccountingTradeDefine.Models;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.AccountingTradeDefine
{
    public  interface IAccountingTradeDefineServant
    {
        IndexResult Index(SearchModel Search);
        MessageModel Create(DetailModel Create);
        MessageModel Update(DetailModel Update);
        MessageModel BatchDelete(BatchDeleteModel BatchDelete);
        DetailModel Detail(int ID);
        DetailModel Detail(decimal TradeNo);
        TradeList QueryTradeDefine(string TransType, string TradeType);
    }
}
