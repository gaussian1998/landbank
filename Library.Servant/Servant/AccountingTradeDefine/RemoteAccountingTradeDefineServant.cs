﻿using Library.Servant.Servant.AccountingTradeDefine.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System;

namespace Library.Servant.Servant.AccountingTradeDefine
{
    public class RemoteAccountingTradeDefineServant : IAccountingTradeDefineServant
    {
        public MessageModel BatchDelete(BatchDeleteModel BatchDelete)
        {
            return RemoteServant.Post<BatchDeleteModel, MessageModel>(
                            BatchDelete,
                            "/AccountingTradeDefineServant/BatchDelete"
           );
        }

        public MessageModel Create(DetailModel Create)
        {
            return RemoteServant.Post<DetailModel, MessageModel>(
                            Create,
                            "/AccountingTradeDefineServant/Create"
           );
        }

        public DetailModel Detail(int ID)
        {
            return RemoteServant.Post<IntModel, DetailModel>(
                            new IntModel { Value=ID},
                            "/AccountingTradeDefineServant/Detail"
           );
        }

        public DetailModel Detail(decimal TradeNo)
        {
            return RemoteServant.Post<DecimalModel, DetailModel>(
                            new DecimalModel { Value= TradeNo },
                            "/AccountingTradeDefineServant/DetailDecimal"
           );
        }

        public IndexResult Index(SearchModel Search)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(
                            Search,
                            "/AccountingTradeDefineServant/Index"
           );
        }

        public TradeList QueryTradeDefine(string TransType, string TradeType)
        {
            return RemoteServant.Post<QueryTradeDefineModel, TradeList>(
                            new QueryTradeDefineModel { TransType = TransType , TradeType = TradeType },
                            "/AccountingTradeDefineServant/QueryTradeDefine"
           );
        }

        public MessageModel Update(DetailModel Update)
        {
            return RemoteServant.Post<DetailModel, MessageModel>(
                           Update,
                           "/AccountingTradeDefineServant/Update"
          );
        }
    }
}
