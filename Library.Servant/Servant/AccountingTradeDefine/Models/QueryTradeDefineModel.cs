﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AccountingTradeDefine.Models
{
    public class QueryTradeDefineModel : InvasionEncryption
    {
        public string TransType { get; set; }
        public string TradeType { get; set; }

    }
}
