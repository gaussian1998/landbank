﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AccountingTradeDefine.Models
{
    public class DetailModel : InvasionEncryption
    {
        public int ID { get; set; }
        public string AccountingBook { get; set; }
        public string TransType { get; set; }
        public string TransTypeName { get; set; }
        public string AccountKey { get; set; }
        public int MasterNo { get; set; }
        public string MasterText { get; set; }
        //public decimal BranchCode { get; set; }
        //public string BranchName { get; set; }
        //public string Department { get; set; }
        //public string DepartmentName { get; set; }
        public string TradeType { get; set; }
        public string TradeTypeName { get; set; }
        public decimal ClientType { get; set; }
        public bool SoftLock { get; set; }

    }
}
