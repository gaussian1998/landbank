﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingTradeDefine;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AccountingTradeDefine.Models
{
    public class SearchModel : InvasionEncryption
    {
        public string TradeType { get; set; }
        public string TransType { get; set; }
        public decimal FromMasterNo { get; set; }
        public decimal ToMasterNo { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
    }
}
