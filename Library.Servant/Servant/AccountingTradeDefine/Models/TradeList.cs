﻿using Library.Servant.Communicate;
using System.Collections.Generic;

namespace Library.Servant.Servant.AccountingTradeDefine.Models
{
    public class TradeList : InvasionEncryption
    {
        public List<DetailModel> List { get; set; }
    }
}
