﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AccountingTradeDefine.Models
{
    public class BatchDeleteModel : InvasionEncryption
    {
        public IList<int> IDList { get; set; }
        public int LastUpdateBy { get; set; }

    }
}
