﻿using Library.Entity.Edmx;
using Library.Servant.Servant.AccountingTradeDefine.Models;
using Library.Servant.Servant.Common.Models;
using System;
using System.Linq;

namespace Library.Servant.Servant.AccountingTradeDefine
{
    public class LocalAccountingTradeDefineServant : IAccountingTradeDefineServant
    {
        public MessageModel BatchDelete(BatchDeleteModel BatchDelete)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            if (BatchDelete.IDList != null && BatchDelete.IDList.Count > 0)
            {
                foreach (var id in BatchDelete.IDList)
                {
                    AS_GL_Trade_Define_Records.DeleteFirst(x => x.ID == id);
                }
            }
            else
            {
                message.IsSuccess = false;
                message.MessagCode = MessageCode.NoDataDeleted.ToString();
            }
            return message;
        }

        public MessageModel Create(DetailModel Create)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            var item = new AS_GL_Trade_Define
            {
                Book_Type = Create.AccountingBook,
                Trans_Type = Create.TransType,
                Trade_Type = Create.TradeType,
                Account_Key = Create.AccountKey,
                Master_No = Create.MasterNo,
                Master_Text = Create.MasterText,
                Soft_Lock = Create.SoftLock,
                //Department = String.IsNullOrEmpty(Create.Department) ? String.Empty : Create.Department
            };
            //if (Create.BranchCode != 0)
            //{
            //    item.Branch_Code = Create.BranchCode;
            //}

            AS_GL_Trade_Define_Records.Create(item);
            return message;
        }

        public MessageModel Update(DetailModel Update)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            AS_GL_Trade_Define_Records.Update(x => x.ID == Update.ID, m => {
                m.Book_Type = Update.AccountingBook;
                m.Trans_Type = Update.TransType;
                m.Trade_Type = Update.TradeType;
                m.Account_Key = Update.AccountKey;
                m.Master_No = Update.MasterNo;
                m.Master_Text = Update.MasterText;
                m.Soft_Lock = Update.SoftLock;
                //m.Branch_Code = (Update.BranchCode == 0 ? m.Branch_Code : Update.BranchCode);
                //m.Department = (Update.Department == null ? m.Department : Update.Department);
            });

            return message;
        }

        public DetailModel Detail(int ID)
        {
            DetailModel result = AS_GL_Trade_Define_Records.First(x => x.ID == ID, m => new DetailModel
            {
                ID = m.ID,
                AccountingBook = m.Book_Type,
                TransType = m.Trans_Type,
                AccountKey = m.Account_Key,
                MasterNo = (int)(m.Master_No ?? 0),
                MasterText = m.Master_Text,
                //BranchCode = m.Branch_Code ?? 0,
                //Department = m.Department,
                TradeType = m.Trade_Type.Trim(),
                ClientType = m.Client_Type ?? 0,
                SoftLock = m.Soft_Lock
            });

            result.TransTypeName = AS_Code_Table_Records.First(x => x.Class == "V03" && x.Code_ID == result.TransType).Text;
            result.TradeTypeName = AS_Code_Table_Records.First(x => x.Class == "V04" && x.Code_ID == result.TradeType).Text;
            //if(result.BranchCode != 0)
            //{
            //    string code = result.BranchCode.ToString("000");
            //    result.BranchName = AS_Code_Table_Records.First(x => x.Class == "C01" && x.Code_ID == code).Text;
            //}
            //else
            //{
            //    result.BranchName = "";
            //}

            //if (!String.IsNullOrEmpty(result.Department))
            //{
            //    result.DepartmentName = AS_Code_Table_Records.First(x => x.Class == "C02" && x.Code_ID == result.Department).Text;
            //}
            //else
            //{
            //    result.DepartmentName = "";
            //}
            return result;

        }

        public IndexResult Index(SearchModel Search)
        {
            bool skipTransType = String.IsNullOrEmpty(Search.TransType) ? true : false;
            bool skipTradeType = String.IsNullOrEmpty(Search.TradeType) ? true : false;
            bool skipFromNo = Search.FromMasterNo == 0 ? true : false;
            bool skipToNo = Search.ToMasterNo == 0 ? true : false;

            IndexResult result = new IndexResult
            {
                Conditions = Search,
                Page = AS_GL_Trade_Define_Records.Page(
                Search.CurrentPage,
                Search.PageSize,
                x => x.ID,
                x => (skipTransType || x.Trans_Type == Search.TransType) &&
                    (skipTradeType || x.Trade_Type == Search.TradeType) &&
                    (skipFromNo || x.Master_No >= Search.FromMasterNo) &&
                    (skipToNo || x.Master_No <= Search.ToMasterNo),
                m => new DetailModel
                {
                    ID = m.ID,
                    AccountingBook = m.Book_Type,
                    TransType = m.Trans_Type,
                    AccountKey = m.Account_Key,
                    MasterNo = (int)(m.Master_No ?? 0),
                    MasterText = m.Master_Text,
                    //BranchCode = m.Branch_Code ?? 0,
                    //Department = m.Department,
                    TradeType = m.Trade_Type.Trim(),
                    ClientType = m.Client_Type ?? 0,
                    SoftLock = m.Soft_Lock
                }
                )
            };
            //排序
            result.Page.Items = result.Page.Items.OrderBy(x=>x.MasterNo).ToList();
            //setup name
            foreach (var item in result.Page.Items)
            {
                item.TransTypeName = AS_Code_Table_Records.First(x => x.Class == "V03" && x.Code_ID == item.TransType).Text;
                item.TradeTypeName = AS_Code_Table_Records.First(x => x.Class == "V04" && x.Code_ID == item.TradeType).Text;
                //    if (item.BranchCode != 0)
                //    {
                //        string code = item.BranchCode.ToString("000");
                //        item.BranchName = AS_Code_Table_Records.First(x => x.Class == "C01" && x.Code_ID == code).Text;
                //    }
                //    else
                //    {
                //        item.BranchName = "";
                //    }

                //    if (!String.IsNullOrEmpty(item.Department))
                //    {
                //        item.DepartmentName = AS_Code_Table_Records.First(x => x.Class == "C02" && x.Code_ID == item.Department).Text;
                //    }
                //    else
                //    {
                //        item.DepartmentName = "";
                //    }
            }
            return result;
        }

        public TradeList QueryTradeDefine(string TransType, string TradeType)
        {
            TradeList result = new TradeList
            {
                List = AS_GL_Trade_Define_Records.Find(x => x.Trans_Type == TransType && x.Trade_Type == TradeType,
                m => new DetailModel
                {
                    ID = m.ID,
                    AccountingBook = m.Book_Type,
                    TransType = m.Trans_Type,
                    AccountKey = m.Account_Key,
                    MasterNo = (int)(m.Master_No ?? 0),
                    MasterText = m.Master_Text,
                    //BranchCode = m.Branch_Code ?? 0,
                    //Department = m.Department,
                    TradeType = m.Trade_Type.Trim(),
                    ClientType = m.Client_Type ?? 0,
                    SoftLock = m.Soft_Lock
                })
            };

            return result;
        }

        public DetailModel Detail(decimal TradeNo)
        {
            DetailModel result = AS_GL_Trade_Define_Records.First(x => x.Master_No == TradeNo, m => new DetailModel
            {
                ID = m.ID,
                AccountingBook = m.Book_Type,
                TransType = m.Trans_Type,
                AccountKey = m.Account_Key,
                MasterNo = (int)(m.Master_No ?? 0),
                MasterText = m.Master_Text,
                //BranchCode = m.Branch_Code ?? 0,
                //Department = m.Department,
                TradeType = m.Trade_Type.Trim(),
                ClientType = m.Client_Type ?? 0,
                SoftLock = m.Soft_Lock
            });

            result.TransTypeName = AS_Code_Table_Records.First(x => x.Class == "V03" && x.Code_ID == result.TransType).Text;
            result.TradeTypeName = AS_Code_Table_Records.First(x => x.Class == "V04" && x.Code_ID == result.TradeType).Text;
            //if (result.BranchCode != 0)
            //{
            //    string code = result.BranchCode.ToString();
            //    result.BranchName = AS_Code_Table_Records.First(x => x.Class == "C01" && x.Code_ID == code).Text;
            //}
            //else
            //{
            //    result.BranchName = "";
            //}

            //if (!String.IsNullOrEmpty(result.Department))
            //{
            //    result.DepartmentName = AS_Code_Table_Records.First(x => x.Class == "C02" && x.Code_ID == result.Department).Text;
            //}
            //else
            //{
            //    result.DepartmentName = "";
            //}
            return result;
        }
    }
}
