﻿using System;
using System.Linq;
using System.Transactions;
using Library.Entity.Edmx;
using Library.Servant.Enums;
using Library.Servant.Servant.ApprovalEventListener;

namespace Library.Servant.Servant.UserRole
{
    //todo_julius
    public class UserRoleListener : ExampleListener
    {
        public override bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            using (var trans = new TransactionScope()) {
                var ids = AS_Users_Role_Records.FindAll(m => m).Where(m => m.Is_Enable == false).Select(m => m.User_ID).ToList();
                AS_Users_Role_Records.Delete(m => ids.Contains(m.User_ID) && m.Is_Enable);
                AS_Users_Role_Records.Update(m => ids.Contains(m.User_ID), entity => {
                    entity.Is_Enable = true;
                });

                AS_Users_Records.Update(m => ids.Contains(m.ID), entity => {
                    entity.Is_Active = true;
                });

                AS_RoleFlow_Header_Records.Update(m => m.Flow_Code == formNo, entity => {
                    entity.Transaction_Status = ((int)ApprovalStatus.Approved).ToString();
                });

                trans.Complete();
            }

            return true;
        }

        public override bool OnReject(string formNo, string carry, AS_LandBankEntities context)
        {
            using (var trans = new TransactionScope()) {
                AS_RoleFlow_Header_Records.Update(m => m.Flow_Code == formNo, entity => {
                    entity.Transaction_Status = ((int)ApprovalStatus.Create).ToString();
                });

                trans.Complete();
            }

            return true;
        }
    }
}
