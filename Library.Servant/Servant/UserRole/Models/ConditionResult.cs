﻿namespace Library.Servant.Servant.UserRole.Models
{
    public class ConditionResult
    {
        public bool IsSaved { get; set; }
        public bool IsModified { get; set; }
        public bool IsSubmited { get; set; }
    }
}
