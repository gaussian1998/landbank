﻿using System;
using System.Collections.Generic;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.UserRole.Models
{
    public class DetailModel : InvasionEncryption
    {
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string BranchName { get; set; }
        public string Department { get; set; }
        public List<RoleDetailModel> TotalRoles { get; set; }
        public HeaderModel HeaderInfo { get; set; }
    }
}
