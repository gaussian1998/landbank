﻿using System;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;
using Library.Servant.Enums;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.UserRole.Models
{
    public class ListItemResult : InvasionEncryption
    {
        public string Branch { get; set; }
        public string Department { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public List<string> LoginRoleNames { get; set; }
    }
}
