﻿using Library.Interface;
using Library.Servant.Enums;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.UserRole.Models
{
    public class HeaderModel : InvasionEncryption
    {
        private string _status;

        public int AppliedUserId { get; set; }
        public string FlowCode { get; set; } 
        public string Status
        {
            get {
                return StaticDataServant.GetStatusText(_status);
            }
            set {
                _status = value;
            }
        }
        public FormType Type { get; set; }
        public string Reason { get; set; }
        public bool IsModified { get; set; }
    }
}
