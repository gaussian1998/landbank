﻿using System;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.UserRole.Models
{
    public class SearchModel : InvasionEncryption
    {
        public string Branch { get; set; }
        public string Department { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}
