﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Servant.UserRole.Models
{
    public class IndexResult : InvasionEncryption
    {
        public List<ListItemResult> Items { get; set; }
        public int TotalAmount { get; set; }
    }
}
