﻿using Library.Servant.Servant.Common;
using Library.Servant.Servant.UserRole.Models;

namespace Library.Servant.Servant.UserRole
{
    public interface IUserRoleServant
    {
        IndexResult Index(SearchModel search);
        DetailModel Detail(string code, string flowCode);
        VoidResult Submit(DetailModel model);
        VoidResult Delete(string userCode, string flowCode);
    }
}
