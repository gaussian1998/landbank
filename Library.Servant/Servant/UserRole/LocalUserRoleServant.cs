﻿using System;
using System.Linq;
using System.Transactions;
using Library.Entity.Edmx;
using Library.Servant.Enums;
using System.Collections.Generic;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.UserRole.Models;

namespace Library.Servant.Servant.UserRole
{
    public class LocalUserRoleServant : IUserRoleServant
    {
        public IndexResult Index(SearchModel search)
        {
            var isSkipBranch = string.IsNullOrEmpty(search.Branch);
            var isSkipDepartment = string.IsNullOrEmpty(search.Department);

            var pageList = AS_Users_Records.Page(search.Page, search.PageSize, m => m.ID,
                m => (isSkipBranch || m.Branch_Code == search.Branch) &&
                     (isSkipDepartment || m.Department_Code == search.Department),
                entity => new ListItemResult
                {
                    Branch = entity.Branch_Name,
                    Department = entity.Department_Name,
                    UserCode = entity.User_Code,
                    UserName = entity.User_Name,
                    LoginRoleNames = entity.AS_Users_Role.Select(m => m.AS_Role.Role_Name).ToList()
                });

            return new IndexResult
            {
                Items = pageList.Items,
                TotalAmount = pageList.TotalAmount
            };
        }
        
        public VoidResult Submit(DetailModel model)
        {
            var now = DateTime.Now;
            var userInfo = AS_Users_Records.First(m => m.User_Code == model.UserCode);

            using (var trans = new TransactionScope())
            {
                var header = new AS_RoleFlow_Header
                {
                    Flow_Code = model.HeaderInfo.FlowCode,
                    Created_By = model.HeaderInfo.AppliedUserId,
                    Create_Time = now,
                    Last_Updated_By = model.HeaderInfo.AppliedUserId,
                    Last_Updated_Time = now,
                    Version = "1", //todo_julius : need to confirm
                    Transaction_Datetime = now,
                    Transaction_Status = ((int)ApprovalStatus.Auditing).ToString(),
                    Transaction_Type = model.HeaderInfo.Type.GetPrefixCode(),
                    Application_Reson = model.HeaderInfo.Reason,
                };

                header.AS_Role_Flow.Add(new AS_Role_Flow
                {
                    User_ID = userInfo.ID,
                    User_ID_App = model.HeaderInfo.AppliedUserId,
                    Created_By = model.HeaderInfo.AppliedUserId,
                    Create_Time = now,
                    Last_Updated_By = model.HeaderInfo.AppliedUserId,
                    Last_Updated_Time = now
                });

                foreach (var role_id in model.TotalRoles.Where(m => m.IsChecked).Select(m => m.ID)) {
                    header.AS_Users_Role.Add(new AS_Users_Role { User_ID = userInfo.ID, Role_ID = role_id, Is_Enable = false });
                }

                AS_RoleFlow_Header_Records.Create(header);

                trans.Complete();
            }

            return new VoidResult();
        }

        public DetailModel Detail(string userCode, string flowCode)
        {
            try {
                var userInfo = AS_Users_Records.First(m => m.User_Code == userCode);
                var selected = userInfo.AS_Users_Role.Where(m => m.Is_Enable).
                    Select(m => m.Role_ID).ToList();

                return new DetailModel
                {
                    UserCode = userInfo.User_Code,
                    UserName = userInfo.User_Name,
                    BranchName = userInfo.Branch_Code,
                    Department = userInfo.Department_Name,
                    HeaderInfo = new HeaderModel(),
                    TotalRoles = AS_Role_Records.FindAll(m => m).AsEnumerable().Select(m => new RoleDetailModel
                    {
                        IsChecked = selected.Contains(m.ID),
                        ID = m.ID,
                        Name = m.Role_Name,
                        DocType = m.Doc_Type,
                        Programs = m.AS_Programs_Roles.Select(p => p.AS_Programs.Programs_Name).ToList()
                    }).ToList()
                };
            }
            catch (Exception ex) {
                return new DetailModel {
                    HeaderInfo = new HeaderModel(),
                    TotalRoles = AS_Role_Records.FindAll(m => m).AsEnumerable().Select(m => new RoleDetailModel
                    {
                        IsChecked = false,
                        ID = m.ID,
                        Name = m.Role_Name,
                        DocType = m.Doc_Type,
                        Programs = m.AS_Programs_Roles.Select(p => p.AS_Programs.Programs_Name).ToList()
                    }).ToList()
                };
            }
        }

        public VoidResult Delete(string userCode, string flowCode)
        {
            AS_Users_Role_Records.Delete(m => m.AS_Users.User_Code == userCode && m.Is_Enable == false);
            AS_Role_Flow_Records.Delete(m => m.AS_RoleFlow_Header.Flow_Code == flowCode && m.AS_Users.User_Code == userCode);
            AS_RoleFlow_Header_Records.Delete(m => m.Flow_Code == flowCode);

            return new VoidResult();
        }
    }
}
