﻿using System;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.UserRole.Models;

namespace Library.Servant.Servant.UserRole
{
    public class RemoteUserRoleServant : IUserRoleServant
    {
        public VoidResult Delete(string userCode, string flowCode)
        {
            throw new NotImplementedException();
        }

        public DetailModel Detail(string code, string flowCode)
        {
            throw new NotImplementedException();
        }

        public DetailModel Edit(string code)
        {
            throw new NotImplementedException();
        }

        public IndexResult Index(SearchModel search)
        {
            throw new NotImplementedException();
        }

        public VoidResult Submit(DetailModel model)
        {
            throw new NotImplementedException();
        }
    }
}
