﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Inspect.InspectModels;
using System.Transactions;
using Library.Entity.Edmx;
using Library.Servant.Repository.Land;
using Library.Servant.Repository.Inspect;

namespace Library.Servant.Servant.Inspect
{
    public class LocalInspectServant : IInspectServant
    {
        public EJObject Index(EJObject condition)
        {
            EJObject result = null;

            var query = InspectGeneral.GetIndex(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetReport(EJObject condition)
        {
            EJObject result = null;

            var query = InspectGeneral.GetReport(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject UpdateRemark(EJObject condition)
        {
            EJObject result = null;

            var query = InspectGeneral.UpdateRemark(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject GetUser(EJObject condition)
        {
            EJObject result = null;

            var query = InspectGeneral.GetUser(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject GetDocID(EJObject condition)
        {
            EJObject result = null;

            var query = InspectGeneral.GetDocID(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject OnFinish(EJObject condition)
        {
            EJObject result = null;

            var query = InspectGeneral.OnFinish(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject OnI200Finish(EJObject condition)
        {
            EJObject result = null;

            var query = InspectGeneral.OnI200Finish(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject GetPicList(EJObject condition)
        {
            EJObject result = null;

            var query = InspectGeneral.GetPicList(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject SavePic(EJObject condition)
        {
            EJObject result = null;

            var query = InspectGeneral.SavePic(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject UpdateDetail(EJObject condition)
        {
            EJObject result = null;

            var query = InspectGeneral.UpdateDetail(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject GetHisIns(EJObject condition)
        {
            EJObject result = null;

            var query = InspectGeneral.GetHisIns(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetHisInsA(EJObject condition)
        {
            EJObject result = null;

            var query = InspectGeneral.GetHisInsA(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject ContentDetail(EJObject condition)
        {
            EJObject result = null;

            var query = InspectGeneral.ContentDetail(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject GetMaxNo(EJObject condition)
        {
            EJObject result = null;

            var query = InspectGeneral.GetMaxNo(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetLand(EJObject condition)
        {

            EJObject result = null;
            var query = InspectGeneral.GetLand(condition);
            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject GetDetail(EJObject condition)
        {

            EJObject result = null;
            var query = InspectGeneral.GetDetail(condition);
            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetTarget(EJObject condition)
        {

            EJObject result = null;
            var query = InspectGeneral.GetTarget(condition);
            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetCategoryKind(EInt type)
        {
            EJObject result = new EJObject();

            var query = InspectGeneral.GetCategoryKind(type);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetCategoryType(EInt type)
        {
            EJObject result = new EJObject();

            var query = InspectGeneral.GetCategoryType(type);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject DocumentType(EInt type)
        {
            EJObject result = new EJObject();

            var query = InspectGeneral.DocumentType(type);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject DocumentTypeOne(EInt type)
        {
            EJObject result = new EJObject();

            var query = InspectGeneral.DocumentTypeOne(type);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetDPM(EInt type)
        {
            EJObject result = new EJObject();

            var query = InspectGeneral.GetDPM(type);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetCode(EString type)
        {
            EJObject result = new EJObject();

            var query = InspectGeneral.GetBookCode(type);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetOrder(EString ID)
        {
            EJObject result = new EJObject();

            var query = InspectGeneral.GetOrder(ID);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject UpdateOrder(EJObject model2)
        {
            EJObject result = new EJObject();

            var query = InspectGeneral.UpdateOrder(model2);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject IndexA(EJObject condition)
        {
            EJObject result = null;

            var query = InspectGeneral.GetIndexA(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject ContentDetailA(EJObject condition)
        {
            EJObject result = null;

            var query = InspectGeneral.ContentDetailA(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetDetailA(EJObject condition)
        {

            EJObject result = null;
            var query = InspectGeneral.GetDetailA(condition);
            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject UpdateOrderA(EJObject model2)
        {
            EJObject result = new EJObject();

            var query = InspectGeneral.UpdateOrderA(model2);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
    }
}
