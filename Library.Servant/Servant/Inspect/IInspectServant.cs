﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Inspect.InspectModels;
using Newtonsoft.Json;

namespace Library.Servant.Servant.Inspect
{
    public interface IInspectServant
    {
        EJObject Index(EJObject condition);
        EJObject IndexA(EJObject condition);
        EJObject UpdateOrder(EJObject condition);
        EJObject UpdateOrderA(EJObject condition);
        EJObject GetMaxNo(EJObject condition);
        EJObject GetTarget(EJObject condition);
        EJObject DocumentType(EInt type);
        EJObject DocumentTypeOne(EInt type);
        EJObject GetDPM(EInt type);
        EJObject GetCode(EString type);
        EJObject GetOrder(EString type);
        EJObject GetDetail(EJObject condition);
        EJObject GetDetailA(EJObject condition);
        EJObject GetLand(EJObject condition);
        EJObject ContentDetail(EJObject condition);
        EJObject ContentDetailA(EJObject condition);
        EJObject GetHisIns(EJObject condition);
        EJObject GetHisInsA(EJObject condition);
        EJObject UpdateDetail(EJObject condition);
        EJObject SavePic(EJObject condition);
        EJObject GetPicList(EJObject condition);
        EJObject OnFinish(EJObject condition);
        EJObject OnI200Finish(EJObject condition);
        EJObject GetDocID(EJObject condition);
        EJObject UpdateRemark(EJObject condition); 
        EJObject GetUser(EJObject condition);
        EJObject GetReport(EJObject condition);
        EJObject GetCategoryKind(EInt condition);
        EJObject GetCategoryType(EInt condition);
        
    }
}
