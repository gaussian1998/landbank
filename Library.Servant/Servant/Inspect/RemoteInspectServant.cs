﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Inspect.InspectModels;
using Library.Servant.Common;

namespace Library.Servant.Servant.Inspect
{
    public class RemoteInspectServant : IInspectServant
    {
        public EJObject GetMaxNo(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/GetMaxNo"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetUser(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/GetUser"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetReport(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/GetReport"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject UpdateRemark(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/UpdateRemark"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject GetDocID(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/GetDocID"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject OnFinish(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/OnFinish"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject OnI200Finish(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/OnI200Finish"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject GetPicList(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/GetPicList"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject SavePic(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/SavePic"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject UpdateDetail(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/UpdateDetail"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject GetHisIns(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/GetHisIns"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetHisInsA(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/GetHisInsA"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject ContentDetail(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/ContentDetail"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject ContentDetailA(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/ContentDetailA"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetDetail(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/GetDetail"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetDetailA(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/GetDetailA"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject Index(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/Index"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject IndexA(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/IndexA"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetTarget(EJObject condition)
        {

            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/InspectServant/GetTarget"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetCategoryKind(EInt type)
        {
            var result = ProtocolService.EncryptionPost<EInt, EJObject>
            (
                type,
                Config.ServantDomain + "/InspectServant/GetCategoryKind"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject GetCategoryType(EInt type)
        {
            var result = ProtocolService.EncryptionPost<EInt, EJObject>
            (
                type,
                Config.ServantDomain + "/InspectServant/GetCategoryType"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject DocumentType(EInt type)
        {
            var result = ProtocolService.EncryptionPost<EInt, EJObject>
            (
                type,
                Config.ServantDomain + "/InspectServant/DocumentType"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject DocumentTypeOne(EInt type)
        {
            var result = ProtocolService.EncryptionPost<EInt, EJObject>
            (
                type,
                Config.ServantDomain + "/InspectServant/DocumentTypeOne"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject GetDPM(EInt type)
        {
            var result = ProtocolService.EncryptionPost<EInt, EJObject>
            (
                type,
                Config.ServantDomain + "/InspectServant/GetDPM"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject GetOrder(EString type)
        {
            var result = ProtocolService.EncryptionPost<EString, EJObject>
            (
                type,
                Config.ServantDomain + "/InspectServant/GetOrder"
            );
            return Tools.ExceptionConvert(result);
        }


        public EJObject GetCode(EString type)
        {
            var result = ProtocolService.EncryptionPost<EString, EJObject>
            (
                type,
                Config.ServantDomain + "/InspectServant/GetCode"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject GetLand(EJObject model)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                model,
                Config.ServantDomain + "/InspectServant/GetLand"
            );

            return Tools.ExceptionConvert(result);
        }

        public EJObject UpdateOrder(EJObject model)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                model,
                Config.ServantDomain + "/InspectServant/UpdateOrder"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject UpdateOrderA(EJObject model)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                model,
                Config.ServantDomain + "/InspectServant/UpdateOrderA"
            );

            return Tools.ExceptionConvert(result);
        }

    }
}
