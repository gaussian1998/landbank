﻿using Library.Entity.Edmx;
using Library.Servant.Servant.Inspect.InspectModels;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servants.AbstractFactory;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Inspect.InspectEventListener
{
    public class InspectEventListener : ExampleListener, IHttpGetProvider
    {
        private IInspectServant InspectServant = ServantAbstractFactory.Inspect();

        public HttpGetViewModel GetUrl(string FormNo, string carryData)
        {
            return new HttpGetViewModel { ControllerName = "Inspect", ActionName = "Edit", Parameter = "id=" + FormNo + "" };
        }

        public override bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            bool result = true;
            EJObject s = new EJObject();
            s.Obj = new JObject();
            s.Obj["No"] = formNo;
              InspectServant.OnFinish(s);
            return result;
        }
      
    }
}
