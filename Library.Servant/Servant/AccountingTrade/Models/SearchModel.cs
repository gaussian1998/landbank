﻿namespace Library.Servant.Servant.AccountingTrade.Models
{
    public class SearchModel
    {
        public bool IsTrade { get; set; }
        public string TradeKind { get; set; }
        public string TradeDistinguish { get; set; }
        public string TransSeq { get; set; }


        public bool IsValidate()
        {
            return string.IsNullOrWhiteSpace(TransSeq) == false;
        }
    }
}
