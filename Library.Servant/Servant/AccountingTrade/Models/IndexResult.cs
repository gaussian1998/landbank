﻿using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace Library.Servant.Servant.AccountingTrade.Models
{
    public class IndexResult
    {
        public List<AccountEntryDetailResult> Items { get; set; }
    }
}
