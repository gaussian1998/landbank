﻿using Library.Entity.Edmx;
using Library.Servant.Servant.AccountingTrade.Models;
using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Models;
using System;

namespace Library.Servant.Servant.AccountingTrade
{
    public class LocalAccountingTradeServant
    {
        public TradeOptions Options()
        {
            return TradeOptions.Query();
        }

        public IndexResult Index(SearchModel model)
        {
            return new IndexResult
            {
                Items = AS_GL_Acc_Months_Records.FindAll<AccountEntryDetailResult>()
            };
        }
    }
}
