﻿using Library.Servant.Servant.Authentication.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.Authentication
{
    public class RemoteAuthenticationServant : IAuthenticationServant
    {
        public bool IsExist()
        {
            return RemoteServant.Post<VoidModel, BoolResult>(

                            new VoidModel { },
                            "/AuthenticationServant/IsExist"
           );
        }

        public LoginResult Login(string userID, string password)
        {
            return RemoteServant.Post<LoginModel, LoginResult>(

                            new LoginModel { UserID = userID, Password = password },
                            "/AuthenticationServant/Login"
           );
        }

        public VoidResult LoginRecord(int userID, string ip)
        {
            return RemoteServant.Post<IntModel, VoidResult>(

                            new IntModel { Value = userID },
                            "/AuthenticationServant/LoginRecord"
           );
        }

        public VoidResult Logout(int userID, string ip)
        {
            return RemoteServant.Post<IntModel, VoidResult>(

                            new IntModel { Value = userID },
                            "/AuthenticationServant/Logout"
           );
        }
    }
}
