﻿using Library.Servant.Communicate;
using Library.Interface;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.Authentication.Models
{
    public class LoginModel : AbstractEncryptionDTO
    {
        public string UserID { get; set; }
        public string Password { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
