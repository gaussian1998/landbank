﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Servant.Authentication.Models
{
    public class LoginResult : AbstractEncryptionDTO
    {
        public int ID { get; set; }
        public string UserID { get; set; }
        public string Name { get; set; }
        public List<string> Roles { get; set; }
        public List<string> Functions { get; set; }

        public bool IsAnonymous()
        {
            return Name == "Anonymous";
        }

        public static LoginResult Anonymous()
        {
            return new LoginResult
            {
                Name = "Anonymous",
                Roles = new List<string> { "Anonymous" },
                Functions = new List<string>()
            };
        }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
