﻿
using Library.Servant.Servant.Authentication.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.Authentication
{
    public interface IAuthenticationServant
    {
        bool IsExist();
        LoginResult Login(string userID, string password);
        VoidResult LoginRecord(int userID, string ip);
        VoidResult Logout(int userID, string ip);
    }
}
