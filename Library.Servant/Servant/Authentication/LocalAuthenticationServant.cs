﻿using System;
using System.Linq;
using Library.Servant.Servant.Authentication.Models;
using Library.Servant.Servant.ADWS;
using Library.Servant.Servant.ADWS.Models;
using Library.Entity.Edmx;
using Library.Servant.Servant.Common;
using System.Net;
using System.Net.Sockets;

namespace Library.Servant.Servant.Authentication
{
    public class LocalAuthenticationServant : IAuthenticationServant
    {
        public bool IsExist()
        {
            var result = Login("080383", "Lb1234567890");

            return result.IsAnonymous() == false;
        }

        public LoginResult Login(string userID, string password)
        {
                AdUserResult adUser = Adws.Verify( userID, password);
                return AS_Users_Records.First(

                            entity => entity.User_Code == userID, 
                            entity => new LoginResult
                            {
                                ID = entity.ID,
                                UserID = entity.User_Code,
                                Name = entity.User_Name,
                                Roles = entity.AS_Users_Role.AsQueryable().Select(m => m.AS_Role.Role_Name).ToList(),
                                Functions = entity.AS_Users_Role.SelectMany(m => m.AS_Role.AS_Programs_Roles).Select(m => m.AS_Programs).Select(m => m.Programs_Code).ToList()
                            }
                 );
        }

        public VoidResult LoginRecord(int userID, string ip)
        {
            var userInfo = AS_Users_Records.First(m => m.ID == userID);

            AS_Opration_His_Records.Create(new AS_Opration_His
            {
                User_ID = userInfo.ID,
                Branch_Code = userInfo.Branch_Code,
                Department_Code = userInfo.Department_Code,
                Login_Datetime = DateTime.Now,
                Last_request = DateTime.Now,
                User_IP = ip
            });

            return new VoidResult();
        }

        public VoidResult Logout(int userID, string ip)
        { 
            var userInfo = AS_Users_Records.First(m => m.ID == userID);

            if (AS_Opration_His_Records.Any(m => m.User_ID == userID && m.Logout_Datetime == null)) {
                AS_Opration_His_Records.Update(m => m.User_ID == userID, entity => {
                    entity.Logout_Datetime = DateTime.Now;
                });
            }
            else {
                AS_Opration_His_Records.Create(new AS_Opration_His
                {
                    User_ID = userInfo.ID,
                    Branch_Code = userInfo.Branch_Code,
                    Department_Code = userInfo.Department_Code,
                    Logout_Datetime = DateTime.Now,
                    Last_request = DateTime.Now,
                    User_IP = ip
                });
            }

            return new VoidResult();
        }



        public LocalAuthenticationServant()
        {
            this.Adws = new AdServant();
        }

        private LoginResult SystemUser(AdUserResult adUser)
        {
            /*if( AS_Users_Records.Any(entity => entity.User_Code == adUser.USER_ID)  == false)
            {
                AS_Users_Records.Create( entity => {

                } );
            }*/

            return AS_Users_Records.First(

                          entity => entity.User_Code == adUser.USER_ID,
                          entity => new LoginResult
                          {
                              ID = entity.ID,
                              UserID = entity.User_Code,
                              Name = entity.User_Name,
                              Roles = entity.AS_Users_Role.AsQueryable().Select(m => m.AS_Role.Role_Name).ToList(),
                              Functions = entity.AS_Users_Role.SelectMany(m => m.AS_Role.AS_Programs_Roles).Select(m => m.AS_Programs).Select(m => m.Programs_Code).ToList()
                          }
               );
        }

        private AdServant Adws;
    }
}
