﻿using Library.Servant.Servant.Build.BuildModels;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.MPBuild.BuildModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Build
{
    public interface IBuildServant
    {
        string CreateBuildFormNumber(string Type);
        string CreateTRXID (string Type);
        bool GetDocID(string DocCode, out int DocID);
        bool HeaderAndAsset_Del(string id, string Type = "");
        bool HeaderAndMPAsset_Del(string id, string Type = "");
        StringResult Asset_Del(int id, string Type = "");
        StringResult MPAsset_Del(int id, string Type = "");
        AssetHandleResult Create_Asset(BuildModel model);
        AssetHandleResult Create_AssetMP(BuildMPModel model);
        BuildModel GetDetail(string id);
        BuildMPModel GetMPDetail(string id);
        AssetModel GetAssetDetail(int id,out BuildTRXHeaderModel header);
        AssetModel GetAssetDetailByAssetNumber(string AssetNumber);
        AssetMPModel GetAssetMPDetail(int id, out BuildTRXHeaderModel header);
        IndexModel Index(SearchModel condition);
        AssetHandleResult CreateToBuild(string TRXHeaderID);
        AssetHandleResult CreateToBuildMP(string TRXHeaderID);
        AssetHandleResult UpdateToBuild(string TRXHeaderID);
        AssetHandleResult UpdateToBuildMP(string TRXHeaderID);
        AssetsIndexModel SearchAssets(AssetsSearchModel condition);
        AssetsMPIndexModel SearchAssetsMP(AssetsMPSearchModel condition);
        AssetHandleResult CreateBuild(BuildModel Build);
        AssetHandleResult CreateBuildMP(BuildMPModel BuildMP);
        AssetsMPIndexModel SearchUserDataByBranch(string BranchCode);

        bool UpdateFlowStatus(string TRX_Header_ID, string FlowStatus);

    }
}
