﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Build.BuildModels
{
    public class SearchModel : AbstractEncryptionDTO
    {
        //主要用於remote---
        public int id { get; set; }
        public string idStr { get; set; }
        public string TRXHeaderID { get; set; }
        public string Type { get; set; }
        public string AssetNumber { get; set; }
        public string FlowStatus { get; set; }
        public string DocCode { get; set; }
        public string BranchCode { get; set; }
        //---------------

        public int? Page { get; set; }
        public int PageSize { get; set; }
        public string Transaction_Type { get; set; }
        public string Form_Number { get; set; }
        public string TRX_Header_ID { get; set; }
        public string Transaction_Status { get; set; }
        public string Book_Type { get; set; }
        public string Book_Type_Code { get; set; }
        public DateTime? Transaction_Datetime_Entered { get; set; }
        public DateTime? Accounting_Datetime { get; set; }
        public string Office_Branch { get; set; }
        public string Description { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
