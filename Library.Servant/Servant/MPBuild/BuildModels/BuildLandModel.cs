﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Build.BuildModels
{
    public class BuildLandModel : AbstractEncryptionDTO
    {
        public int ID { get; set; }
        public string Asset_Number { get; set; }
        public string Parent_Land_Number { get; set; }
        public string Filial_Land_Number { get; set; }
        public decimal Area_Size { get; set; }
        public decimal Authorized_Scope_Molecule { get; set; }
        public decimal Authorized_Scope_Denomminx { get; set; }
        public decimal Authorized_Area { get; set; }
        public string Section { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
