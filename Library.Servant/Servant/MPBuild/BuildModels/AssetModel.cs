﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Build.BuildModels
{
    public class AssetModel : AbstractEncryptionDTO
    {
        public int ID { get; set; }
        public string Asset_Number { get; set; }
        public string Asset_Category_Code { get; set; }
        public string Deprn_Method_Code { get; set; }
        public decimal Life_Years { get; set; }
        public decimal Life_Months { get; set; }
        public string Location_Disp { get; set; }
        public string Parent_Asset_Number { get; set; }
        public string Old_Asset_Number { get; set; }
        public string Description { get; set; }
        public string City_Code { get; set; }
        public string District_Code { get; set; }
        public string Section_Code { get; set; }
        public string Sub_Sectioni_Name { get; set; }
        public string Build_Number { get; set; }
        public string Build_Address { get; set; }
        public string Building_STRU { get; set; }
        public decimal Building_Total_Floor { get; set; }
        public string Authorization_Number { get; set; }
        public string Authorization_Name { get; set; }
        public string Used_Type { get; set; }
        public string Used_Status { get; set; }
        public string Obtained_Method { get; set; }
        public decimal Original_Cost { get; set; }
        public decimal Salvage_Value { get; set; }
        public decimal Deprn_Amount { get; set; }
        public decimal Reval_Adjustment_Amount { get; set; }
        public decimal Business_Area_Size { get; set; }
        public decimal NONBusiness_Area_Size { get; set; }
        public decimal Current_Cost { get; set; }
        public decimal Reval_Reserve { get; set; }
        public decimal Business_Book_Amount { get; set; }
        public decimal NONBusiness_Book_Amount { get; set; }
        public decimal Deprn_Reserve { get; set; }
        public decimal Business_Deprn_Reserve { get; set; }
        public decimal NONBusiness_Deprn_Reserve { get; set; }
        public System.DateTime Date_Placed_In_Service { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public decimal Current_Units { get; set; }
        public string Delete_Reason { get; set; }
        public string Urban_Renewal { get; set; }
        public System.DateTime Create_Time { get; set; }
        public decimal? Created_By { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public decimal? Last_Updated_By { get; set; }
        public bool Not_Deprn_Flag { get; set; }
        public int? DEPRN_Counts { get; set; }
        public string Created_By_Name { get; set; }
        public string Last_UpDatetimed_By_Name { get; set; }

        public IEnumerable<AssetDetailModel> BuildDetails { get; set; }

        public IEnumerable<BuildLandModel> BuildLands { get; set; }
        public BuildCategoryModel BuildCategory { get; set; }
        public BuildRetireModel BuildRetire { get; set; }
        public BuildYearModel BuildYear { get; set; }
        public List<BuildChangeRecord> BuildChangeRecord { get; set; }
        public bool NeedRecord = false;
        public string Asset_Category_Code_Name { get; set; }  //用在房屋重大組成新增單元編輯模式時讀取母資產資訊

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
