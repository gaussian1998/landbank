﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Build.BuildModels
{
    public class BuildChangeRecord
    {
        public int TRX_Header_ID { get; set; }
        public int AS_Assets_Build_ID { get; set; }
        public int AS_Assets_Build_MP_ID { get; set; }
        public int AS_Assets_Build_MP_Record_ID { get; set; }
        public int Expense_Account { get; set; }
        public string UPD_File { get; set; }
        public string Field_Name { get; set; }
        public string Modify_Type { get; set; }
        public string Before_Data { get; set; }
        public string After_Data { get; set; }
        public decimal? Created_By { get; set; }
        public string Created_By_Name { get; set; }
        public DateTime? Create_Time { get; set; }
    }
}
