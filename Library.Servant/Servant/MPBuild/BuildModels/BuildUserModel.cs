﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Build.BuildModels
{
    public class BuildUserModel : AbstractEncryptionDTO
    {
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
        public int ID { get; set; }
        public string User_Code { get; set; }
        public string User_Name { get; set; }
        public string Office_Branch { get; set; }
        public bool? Is_Active { get; set; }
    }
}
