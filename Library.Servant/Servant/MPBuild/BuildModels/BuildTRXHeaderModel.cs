﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Build.BuildModels
{
    public class BuildTRXHeaderModel : AbstractEncryptionDTO
    {
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
        public int ID { get; set; }
        public string Form_Number { get; set; }
        public string Office_Branch { get; set; }
        public string Book_Type_Code { get; set; }
        public string Transaction_Type { get; set; }
        public string Transaction_Status { get; set; }
        public DateTime Transaction_Datetime_Entered { get; set; }
        public Nullable<DateTime> Accounting_Datetime { get; set; }
        public string Description { get; set; }
        public string Amotized_Adjustment_Flag { get; set; }
        public Nullable<DateTime> Create_Time { get; set; }
        public Nullable<decimal> Created_By { get; set; }
        public string Created_By_Name { get; set; }
        public Nullable<DateTime> Last_UpDatetimed_Time { get; set; }
        public Nullable<decimal> Last_UpDatetimed_By { get; set; }
        public string Last_UpDatetimed_By_Name { get; set; }
        public Nullable<int> Auto_Post { get; set; }
        public string OfficeBranchName { get; set; }
    }
}
