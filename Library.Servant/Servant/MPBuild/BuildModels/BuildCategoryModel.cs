﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Build.BuildModels
{
    public class BuildCategoryModel
    {
        public int ID { get; set; }
        public int TRX_Header_ID { get; set; }
        public int Asset_Build_ID { get; set; }
        public decimal Serial_Number { get; set; }
        public string Asset_Number { get; set; }
        public string Old_Asset_Category_Code { get; set; }
        public string NEW_Asset_Category_Code { get; set; }
        public string Description { get; set; }
        public string Expense_Account { get; set; }
        public Nullable<System.DateTime> Create_Time { get; set; }
        public Nullable<decimal> Created_By { get; set; }
        public string Created_By_Name { get; set; }
        public Nullable<System.DateTime> Last_UpDatetimed_Time { get; set; }
        public Nullable<decimal> Last_UpDatetimed_By { get; set; }
        public string Last_UpDatetimed_By_Name { get; set; }
        public BuildChangeRecord BuildChangeRecord { get; set; }
    }
}
