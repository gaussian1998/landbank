﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.Servant.Build.BuildModels;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Build.BuildModels
{
    public class BuildMPModel : AbstractEncryptionDTO
    {
        public BuildHeaderModel Header { get; set; }
        public BuildTRXHeaderModel TRXHeader { get; set; }

        public IEnumerable<AssetMPModel> Assets { get; set; }
        public IEnumerable<BuildMPRetireModel> Retire { get; set; }
        public IEnumerable<BuildMPCategoryModel> Category { get; set; }
        public IEnumerable<BuildMPYearModel> Year { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
