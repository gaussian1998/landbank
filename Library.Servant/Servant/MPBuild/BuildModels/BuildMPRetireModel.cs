﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Build.BuildModels
{
    public class BuildMPRetireModel
    {
        public int ID { get; set; }
        public int TRX_Header_ID { get; set; }
        public int Assets_Build_MP_ID { get; set; }
        public decimal Serial_Number { get; set; }
        public string Asset_Number { get; set; }
        public string Parent_Asset_Number { get; set; }
        public Nullable<decimal> Old_Cost { get; set; }
        public Nullable<decimal> Retire_Cost { get; set; }
        public Nullable<decimal> Sell_Amount { get; set; }
        public Nullable<decimal> Sell_Cost { get; set; }
        public Nullable<decimal> New_Cost { get; set; }
        public Nullable<decimal> Reval_Adjustment_Amount { get; set; }
        public Nullable<decimal> Reval_Reserve { get; set; }
        public Nullable<decimal> Reval_Land_VAT { get; set; }
        public string Reason_Code { get; set; }
        public string Description { get; set; }
        public Nullable<int> Account_Type { get; set; }
        public Nullable<decimal> Receipt_Type { get; set; }
        public Nullable<System.DateTime> Create_Time { get; set; }
        public Nullable<decimal> Created_By { get; set; }
        public string Created_By_Name { get; set; }
        public Nullable<System.DateTime> Last_UpDatetimed_Time { get; set; }
        public Nullable<decimal> Last_UpDatetimed_By { get; set; }
        public string Last_UpDatetimed_By_Name { get; set; }
    }
}
