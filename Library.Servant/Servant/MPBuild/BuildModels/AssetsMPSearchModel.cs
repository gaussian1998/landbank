﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Build.BuildModels
{
    public class AssetsMPSearchModel : AbstractEncryptionDTO
    {
        public string Search_Asset_Category_Code { get; set; }
        public string Search_Parent_Asset_Category_Code { get; set; }
        public int? Page { get; set; }
        public int PageSize { get; set; }
        public List<string> UseAssetNumber { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
