﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Build.BuildModels
{
    public class AssetsSearchModel : AbstractEncryptionDTO
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }
        public string City_Code { get; set; }
        public string District_Code { get; set; }
        public string Section_Code { get; set; }
        public string Build_Number { get; set; }
        public string AssetNumber { get; set; }
        public List<string> UseAssetNumber { get; set; }
        public bool NeedUserData = false;
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
