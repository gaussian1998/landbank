﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Build.BuildModels
{
    public class BuildHeaderModel : AbstractEncryptionDTO
    {
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }

        public int ID { get; set; }
        public string TRX_Header_ID { get; set; }
        public string Transaction_Type { get; set; }
        public string Transaction_Status { get; set; }
        public string Transaction_Status_Name { get; set; }
        public string Book_Type { get; set; }
        public System.DateTime Transaction_Datetime_Entered { get; set; }
        public Nullable<System.DateTime> Accounting_Datetime { get; set; }
        public string Office_Branch { get; set; }
        public string Office_Branch_Name { get; set; }
        public string Description { get; set; }
        public System.DateTime Create_Time { get; set; }
        public string Created_By { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public string Last_Updated_By { get; set; }
    }
}
