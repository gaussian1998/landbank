﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Build.BuildModels
{
    public class BuildModel : AbstractEncryptionDTO
    {
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
        public BuildTRXHeaderModel TRXHeader { get; set; } //From AS_TRX_Headers
        public IEnumerable<AssetModel> Assets { get; set; }
        public IEnumerable<BuildRetireModel> Retire { get; set; }
        public IEnumerable<BuildCategoryModel> Category { get; set; }
        public IEnumerable<BuildYearModel> Year { get; set; }
        public BuildHeaderModel Header { get; set; }
    }
}
