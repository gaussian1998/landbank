﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Build.BuildModels
{ 
    public class BuildMPChangeRecord
    {
        public int ID { get; set; }
        public Nullable<int> TRX_Header_ID { get; set; }
        public Nullable<int> AS_Assets_Build_MP_ID { get; set; }
        public Nullable<int> AS_Assets_Build_MP_Record_ID { get; set; }
        public string UPD_File { get; set; }
        public string Field_Name { get; set; }
        public string Modify_Type { get; set; }
        public string Before_Data { get; set; }
        public string After_Data { get; set; }
        public string Expense_Account { get; set; }
        public Nullable<System.DateTime> Create_Time { get; set; }
        public Nullable<decimal> Created_By { get; set; }
        public string Created_By_Name { get; set; }
        public Nullable<System.DateTime> Last_UpDatetimed_Time { get; set; }
        public Nullable<decimal> Last_UpDatetimed_By { get; set; }
        public string Last_UpDatetimed_By_Name { get; set; }
    }
}
