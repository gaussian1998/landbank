﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.Servant.Build.BuildModels;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MPBuild.BuildModels
{
    public class StringResult : AbstractEncryptionDTO
    {
        public string StrResult { get; set; }
        public bool IsSuccess { get; set; }

        public AssetModel AssetModel { get; set; }
        public BuildTRXHeaderModel BuildTRXHeaderModel { get; set; }
        public BuildHeaderModel BuildHeaderModel { get; set; }
        public AssetMPModel AssetMPModel { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
