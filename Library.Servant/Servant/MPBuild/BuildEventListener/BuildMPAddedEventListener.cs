﻿using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servants.AbstractFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Build.BuildEventListener
{
    public class BuildMPAddedEventListener : ExampleListener, IHttpGetProvider
    {
        private IBuildServant BuildServant = ServantAbstractFactory.Build();

        public HttpGetViewModel GetUrl(string FormNo, string carryData)
        {
            return new HttpGetViewModel { ControllerName = "BuildMPAdded", ActionName = "Edit", Parameter = "id=" + carryData + "&signmode=Y" };
        }

        public override bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            bool result = true;
            BuildServant.CreateToBuildMP(formNo);
            return result;
        }
    }
}
