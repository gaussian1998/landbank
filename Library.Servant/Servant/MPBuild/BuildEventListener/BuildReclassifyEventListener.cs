﻿using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servant.Build;
using Library.Servant.Servants.AbstractFactory;

namespace Library.Servant.Servant.Build.BuildEventListener
{
    public class BuildReclassifyEventListener : ExampleListener, IHttpGetProvider
    {
        private IBuildServant BuildServant = ServantAbstractFactory.Build();

        public HttpGetViewModel GetUrl(string FormNo, string carryData)
        {
            return new HttpGetViewModel { ControllerName = "BuildReclassify", ActionName = "Edit", Parameter = "id=" + FormNo + "&signmode=Y" };
        }

        public override bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            bool result = true;
            BuildServant.UpdateToBuild(formNo);
            return result;
        }
    }
}
