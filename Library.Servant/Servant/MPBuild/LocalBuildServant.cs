﻿using Library.Entity.Edmx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Build.BuildModels;
using System.Transactions;
using Library.Servant.Repository.Build;
using Library.Servant.Servant.MPBuild.BuildModels;
using Library.Servant.Servant.Common;
using System.Reflection;

namespace Library.Servant.Servant.Build
{
    public class LocalBuildServant : IBuildServant
    {
        public IndexModel Index(SearchModel condition)
        {
            IndexModel result = new IndexModel();

            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_TRX_Headers
                            join mc in
                            (
                               from mr in db.AS_Assets_Build_Record
                               group mr by new { mr.TRX_Header_ID } into mrg
                               select new
                               {
                                   TRX_Header_ID = mrg.Key.TRX_Header_ID,
                                   Count = mrg.Count()
                               }
                            ) on mh.Form_Number equals mc.TRX_Header_ID into mhtemp
                            from mc in mhtemp.DefaultIfEmpty()
                            join f in
                            (
                               from ff in db.AS_Code_Table
                               where ff.Class == "P02"
                               select ff
                            ) on mh.Transaction_Status equals f.Code_ID into tf
                            from f in tf.DefaultIfEmpty()
                            join o in db.AS_Keep_Position on ("001."+mh.Office_Branch) equals o.Keep_Position_Code into to
                            from o in to.DefaultIfEmpty()
                            select new { mh, mc , f , o};

                if (!string.IsNullOrEmpty(condition.Transaction_Type))
                {
                    query = query.Where(m => m.mh.Transaction_Type == condition.Transaction_Type);
                }

                if (!string.IsNullOrEmpty(condition.Form_Number))
                {
                    query = query.Where(m => m.mh.Form_Number == condition.Form_Number);
                }

                if(!string.IsNullOrEmpty(condition.Transaction_Status))
                {
                    query = query.Where(m => m.mh.Transaction_Status == condition.Transaction_Status);
                }

                DateTime CompareDate = new DateTime();

                if (condition.Transaction_Datetime_Entered != null)
                {
                    CompareDate = condition.Transaction_Datetime_Entered.Value.AddDays(1);
                    query = query.Where(m => m.mh.Create_Time >= condition.Transaction_Datetime_Entered && m.mh.Create_Time < CompareDate);
                }

                if (condition.Accounting_Datetime != null)
                {
                    CompareDate = condition.Accounting_Datetime.Value.AddDays(1);
                    query = query.Where(m => m.mh.Accounting_Datetime >= condition.Accounting_Datetime && m.mh.Accounting_Datetime < CompareDate);
                }

                if (!string.IsNullOrEmpty(condition.Office_Branch))
                {
                    query = query.Where(m => m.mh.Office_Branch == condition.Office_Branch);
                }

                if (!string.IsNullOrEmpty(condition.Description))
                {
                    query = query.Where(m => m.mh.Description == condition.Description);
                }

                result.TotalAmount = query.Count();
                //分頁
                if (condition.Page != null)
                {
                    query = query.OrderByDescending(m => m.mh.Create_Time).Skip((condition.Page.Value - 1) * Convert.ToInt32(condition.PageSize)).Take(Convert.ToInt32(condition.PageSize));
                }


                result.Items = query.ToList().Select(m => new BuildTRXHeaderModel()
                {
                    ID = m.mh.ID,
                    Form_Number = m.mh.Form_Number,
                    Office_Branch = m.mh.Office_Branch,
                    Book_Type_Code = m.mh.Book_Type_Code,
                    Transaction_Type = m.mh.Transaction_Type,
                    Transaction_Status = m.mh.Transaction_Status,
                    Transaction_Datetime_Entered = m.mh.Transaction_Datetime_Entered,
                    Accounting_Datetime = m.mh.Accounting_Datetime,
                    Description = m.mh.Description,
                    Amotized_Adjustment_Flag = m.mh.Amotized_Adjustment_Flag,
                    Create_Time = m.mh.Create_Time,
                    Created_By = m.mh.Created_By,
                    Created_By_Name = m.mh.Created_By_Name,
                    Last_UpDatetimed_Time = m.mh.Last_UpDatetimed_Time,
                    Last_UpDatetimed_By = m.mh.Last_UpDatetimed_By,
                    Last_UpDatetimed_By_Name = m.mh.Last_UpDatetimed_By_Name,
                    Auto_Post = m.mh.Auto_Post,
                    OfficeBranchName = (m.o != null)?m.o.Keep_Position_Name:"",
                });
                result.condition = condition;
                
            }

            return result;
        }

        public string CreateBuildFormNumber(string Type)
        {
            //Example: 1061103 + C010 + 000001 共17碼
            string CToDay = (DateTime.Today.Year - 1911).ToString().PadLeft(3, '0') + DateTime.Today.ToString("MMdd") + Type;
            string FormNumber = "";

            using (var db = new AS_LandBankEntities())
            {
                FormNumber = db.AS_TRX_Headers.Where(o => o.Transaction_Type == Type && o.Form_Number.StartsWith(CToDay)).Max(o => o.Form_Number);
            }
            if (FormNumber != null)
            {
                int Seq = Convert.ToInt32(FormNumber.Substring(12)) + 1;
                FormNumber = CToDay + Seq.ToString().PadLeft(6, '0');
            }
            else
            {
                FormNumber = CToDay + "000001";
            }

            return FormNumber;
        }

        public string CreateTRXID(string Type)
        {
            string CToDay = (DateTime.Today.Year - 1911).ToString().PadLeft(3, '0') + DateTime.Today.ToString("MMdd") + Type;
            string TRXID = "";

            using (var db = new AS_LandBankEntities())
            {
                TRXID = db.AS_Assets_Build_Header.Where(o => o.Transaction_Type == Type && o.TRX_Header_ID.StartsWith(CToDay)).Max(o => o.TRX_Header_ID);
            }
            if (TRXID != null)
            {
                int Seq = Convert.ToInt32(TRXID.Substring(11)) + 1;
                TRXID = CToDay + Seq.ToString().PadLeft(6, '0');
            }
            else
            {
                TRXID = CToDay + "000001";
            }

            return TRXID;
        }
        /// <summary>
        /// 刪除表頭單據以及資產
        /// </summary>
        /// <param name="id">AS_TRX_Headers.Form_Number</param>
        /// <param name="Type"></param>
        public bool HeaderAndAsset_Del(string id, string Type="")
        {
            bool Result = false;
            int HeaderID = 0;
            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        if (db.AS_TRX_Headers.Any(o => o.Form_Number == id))
                        {
                            var data = db.AS_TRX_Headers.Where(m => m.Form_Number == id).SingleOrDefault();
                            db.AS_TRX_Headers.Remove(data);
                            db.SaveChanges();
                            HeaderID = data.ID;
                        }

                        if (db.AS_Assets_Build_Record.Any(o => o.TRX_Header_ID == id))
                        {
                            var list = db.AS_Assets_Build_Record.Where(m => m.TRX_Header_ID == id).ToList();
                            if (list.Count > 0)
                            {
                                foreach (var item in list)
                                {
                                    if (db.AS_Assets_Build_Record.Any(o => o.ID == item.ID))
                                    {
                                        var data = db.AS_Assets_Build_Record.Find(item.ID);
                                        db.AS_Assets_Build_Record.Remove(data);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(Type))
                        {
                            switch (Type)
                            {
                                case "C210":
                                    if(db.AS_Assets_Build_Retire.Any(m=>m.TRX_Header_ID == HeaderID))
                                    {
                                        var list = db.AS_Assets_Build_Retire.Where(m => m.TRX_Header_ID == HeaderID).ToList();
                                        if (list.Count > 0)
                                        {
                                            foreach (var item in list)
                                            {
                                                if (db.AS_Assets_Build_Retire.Any(o => o.ID == item.ID))
                                                {
                                                    var data = db.AS_Assets_Build_Retire.Find(item.ID);
                                                    db.AS_Assets_Build_Retire.Remove(data);
                                                }
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                    break;
                                case "C220":
                                    if (db.AS_Assets_Build_Category.Any(m => m.TRX_Header_ID == HeaderID))
                                    {
                                        var list = db.AS_Assets_Build_Category.Where(m => m.TRX_Header_ID == HeaderID).ToList();
                                        if (list.Count > 0)
                                        {
                                            foreach (var item in list)
                                            {
                                                if (db.AS_Assets_Build_Category.Any(o => o.ID == item.ID))
                                                {
                                                    var data = db.AS_Assets_Build_Category.Find(item.ID);
                                                    db.AS_Assets_Build_Category.Remove(data);
                                                }
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                    break;
                                case "C230":
                                    if (db.AS_Assets_Build_Years.Any(m => m.TRX_Header_ID == HeaderID))
                                    {
                                        var list = db.AS_Assets_Build_Years.Where(m => m.TRX_Header_ID == HeaderID).ToList();
                                        if (list.Count > 0)
                                        {
                                            foreach (var item in list)
                                            {
                                                if (db.AS_Assets_Build_Years.Any(o => o.ID == item.ID))
                                                {
                                                    var data = db.AS_Assets_Build_Years.Find(item.ID);
                                                    db.AS_Assets_Build_Years.Remove(data);
                                                }
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }

                    }
                    trans.Complete();
                }
            }
            catch (Exception ex)
            {

            }
            Result = true;
            return Result;
        }
        /// <summary>
        /// 刪除表頭單據以及資產
        /// </summary>
        /// <param name="id">AS_TRX_Headers.Form_Number</param>
        /// <param name="Type"></param>
        public bool HeaderAndMPAsset_Del(string id, string Type = "")
        {
            bool Result = false;
            int HeaderID = 0;
            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        if (db.AS_TRX_Headers.Any(o => o.Form_Number == id))
                        {
                            var data = db.AS_TRX_Headers.Where(m => m.Form_Number == id).SingleOrDefault();
                            db.AS_TRX_Headers.Remove(data);
                            db.SaveChanges();
                            HeaderID = data.ID;
                        }

                        if (db.AS_Assets_Build_MP_Record.Any(o => o.TRX_Header_ID == HeaderID))
                        {
                            var list = db.AS_Assets_Build_MP_Record.Where(m => m.TRX_Header_ID == HeaderID).ToList();
                            if (list.Count > 0)
                            {
                                foreach (var item in list)
                                {
                                    if (db.AS_Assets_Build_MP_Record.Any(o => o.ID == item.ID))
                                    {
                                        var data = db.AS_Assets_Build_MP_Record.Find(item.ID);
                                        db.AS_Assets_Build_MP_Record.Remove(data);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(Type))
                        {
                            switch (Type)
                            {
                                case "C330":
                                    if (db.AS_Assets_Build_MP_Retire.Any(m => m.TRX_Header_ID == HeaderID))
                                    {
                                        var list = db.AS_Assets_Build_MP_Retire.Where(m => m.TRX_Header_ID == HeaderID).ToList();
                                        if (list.Count > 0)
                                        {
                                            foreach (var item in list)
                                            {
                                                if (db.AS_Assets_Build_MP_Retire.Any(o => o.ID == item.ID))
                                                {
                                                    var data = db.AS_Assets_Build_MP_Retire.Find(item.ID);
                                                    db.AS_Assets_Build_MP_Retire.Remove(data);
                                                }
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                    break;
                                case "C340":
                                    if (db.AS_Assets_Build_MP_Category.Any(m => m.TRX_Header_ID == HeaderID))
                                    {
                                        var list = db.AS_Assets_Build_MP_Category.Where(m => m.TRX_Header_ID == HeaderID).ToList();
                                        if (list.Count > 0)
                                        {
                                            foreach (var item in list)
                                            {
                                                if (db.AS_Assets_Build_MP_Category.Any(o => o.ID == item.ID))
                                                {
                                                    var data = db.AS_Assets_Build_MP_Category.Find(item.ID);
                                                    db.AS_Assets_Build_MP_Category.Remove(data);
                                                }
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                    break;
                                case "C350":
                                    if (db.AS_Assets_Build_MP_Years.Any(m => m.TRX_Header_ID == HeaderID))
                                    {
                                        var list = db.AS_Assets_Build_MP_Years.Where(m => m.TRX_Header_ID == HeaderID).ToList();
                                        if (list.Count > 0)
                                        {
                                            foreach (var item in list)
                                            {
                                                if (db.AS_Assets_Build_MP_Years.Any(o => o.ID == item.ID))
                                                {
                                                    var data = db.AS_Assets_Build_MP_Years.Find(item.ID);
                                                    db.AS_Assets_Build_MP_Years.Remove(data);
                                                }
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }

                    }
                    trans.Complete();
                }
            }
            catch (Exception ex)
            {

            }
            Result = true;
            return Result;
        }
        /// <summary>
        /// 刪除資產
        /// </summary>
        /// <param name="id">AS_Assets_Build_Record.ID</param>
        /// <param name="Type"></param>
        /// <returns></returns>
        public StringResult Asset_Del(int id, string Type = "")
        {
            string Form_Number = string.Empty;
            string Asset_Number = string.Empty;
            bool Result = false;
            int HeaderID = 0;
            try
            {
                using (var db = new AS_LandBankEntities())
                {
                    if (db.AS_Assets_Build_Record.Any(o => o.ID == id))
                    {
                        var data = db.AS_Assets_Build_Record.Find(id);
                        Form_Number = data.TRX_Header_ID;
                        Asset_Number = data.Asset_Number;
                        HeaderID = db.AS_TRX_Headers.Where(m => m.Form_Number == Form_Number).SingleOrDefault().ID;

                        db.AS_Assets_Build_Record.Remove(data);
                        db.SaveChanges();
                    }
                    if (!string.IsNullOrEmpty(Type))
                    {
                        switch (Type)
                        {
                            case "C210":
                                if (db.AS_Assets_Build_Retire.Any(m => m.TRX_Header_ID == HeaderID && m.Asset_Number == Asset_Number))
                                {
                                    var data = db.AS_Assets_Build_Retire.Where(m => m.TRX_Header_ID == HeaderID && m.Asset_Number == Asset_Number).SingleOrDefault();
                                    db.AS_Assets_Build_Retire.Remove(data);
                                    db.SaveChanges();
                                }
                                break;
                            case "C220":
                                if (db.AS_Assets_Build_Category.Any(m => m.TRX_Header_ID == HeaderID && m.Asset_Number == Asset_Number))
                                {
                                    var data = db.AS_Assets_Build_Category.Where(m => m.TRX_Header_ID == HeaderID && m.Asset_Number == Asset_Number).SingleOrDefault();
                                    db.AS_Assets_Build_Category.Remove(data);
                                    db.SaveChanges();
                                }
                                break;
                            case "C230":
                                if (db.AS_Assets_Build_Years.Any(m => m.TRX_Header_ID == HeaderID && m.Asset_Number == Asset_Number))
                                {
                                    var data = db.AS_Assets_Build_Years.Where(m => m.TRX_Header_ID == HeaderID && m.Asset_Number == Asset_Number).SingleOrDefault();
                                    db.AS_Assets_Build_Years.Remove(data);
                                    db.SaveChanges();
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            Result = true;
            return new StringResult() { StrResult = Form_Number,IsSuccess= Result };

        }
        public StringResult MPAsset_Del(int id, string Type = "")
        {
            string Form_Number = string.Empty;
            string Asset_Number = string.Empty;
            bool Result = false;
            int HeaderID = 0;
            try
            {
                using (var db = new AS_LandBankEntities())
                {
                    if (db.AS_Assets_Build_MP_Record.Any(o => o.ID == id))
                    {
                        var data = db.AS_Assets_Build_MP_Record.Find(id);
                        HeaderID = data.TRX_Header_ID;
                        Form_Number = db.AS_TRX_Headers.Where(m => m.ID == HeaderID).SingleOrDefault().Form_Number;
                        Asset_Number = data.Asset_Number;

                        db.AS_Assets_Build_MP_Record.Remove(data);
                        db.SaveChanges();
                    }
                    if (!string.IsNullOrEmpty(Type))
                    {
                        switch (Type)
                        {
                            case "C330":
                                if (db.AS_Assets_Build_MP_Retire.Any(m => m.TRX_Header_ID == HeaderID && m.Asset_Number == Asset_Number))
                                {
                                    var data = db.AS_Assets_Build_MP_Retire.Where(m => m.TRX_Header_ID == HeaderID && m.Asset_Number == Asset_Number).SingleOrDefault();
                                    db.AS_Assets_Build_MP_Retire.Remove(data);
                                    db.SaveChanges();
                                }
                                break;
                            case "C340":
                                if (db.AS_Assets_Build_MP_Category.Any(m => m.TRX_Header_ID == HeaderID && m.Asset_Number == Asset_Number))
                                {
                                    var data = db.AS_Assets_Build_MP_Category.Where(m => m.TRX_Header_ID == HeaderID && m.Asset_Number == Asset_Number).SingleOrDefault();
                                    db.AS_Assets_Build_MP_Category.Remove(data);
                                    db.SaveChanges();
                                }
                                break;
                            case "C350":
                                if (db.AS_Assets_Build_MP_Years.Any(m => m.TRX_Header_ID == HeaderID && m.Asset_Number == Asset_Number))
                                {
                                    var data = db.AS_Assets_Build_MP_Years.Where(m => m.TRX_Header_ID == HeaderID && m.Asset_Number == Asset_Number).SingleOrDefault();
                                    db.AS_Assets_Build_MP_Years.Remove(data);
                                    db.SaveChanges();
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            Result = true;
            return new StringResult() { StrResult = Form_Number, IsSuccess = Result };

        }

        public AssetHandleResult Create_Asset(BuildModel model)
        {
            int AssetID = 0;
            int TRXHeader_ID = 0;
            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        #region AS_TRX_Headers
                        //Header
                        AS_TRX_Headers Header = null;
                        if (!db.AS_TRX_Headers.Any(h => h.Form_Number == model.TRXHeader.Form_Number))
                        {
                            Header = BuildGeneral.ToAS_TRX_Headers(model.TRXHeader);
                            db.Entry(Header).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                            TRXHeader_ID = Header.ID;
                        }
                        else
                        {
                            Header = db.AS_TRX_Headers.First(h => h.Form_Number == model.TRXHeader.Form_Number);
                            Header.Last_UpDatetimed_By = null;
                            Header.Last_UpDatetimed_By_Name = string.Empty;
                            Header.Last_UpDatetimed_Time = DateTime.Now;
                            db.Entry(Header).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            TRXHeader_ID = Header.ID;
                        }
                        #endregion                        


                        foreach (var asset in model.Assets)
                        {
                            #region AS_Assets_Build_Record
                            

                            //Record
                            AS_Assets_Build_Record Record = null;
                            if (db.AS_Assets_Build_Record.Any(o => o.TRX_Header_ID == Header.Form_Number && o.ID == asset.ID))
                            {
                                //Build Record
                                Record = db.AS_Assets_Build_Record.FirstOrDefault(m => m.ID == asset.ID);
                                
                                //異動紀錄
                                List<BuildChangeRecord> change = new List<BuildChangeRecord>();
                                if (asset.NeedRecord)
                                {
                                    change = GenBuildAssetChangeRocord(asset, Record);
                                }

                                Record.Asset_Number = asset.Asset_Number;
                                Record.Asset_Category_Code = asset.Asset_Category_Code;
                                Record.Deprn_Method_Code = asset.Deprn_Method_Code;
                                Record.Life_Years = asset.Life_Years;
                                Record.Life_Months = asset.Life_Months;
                                Record.Location_Disp = asset.Location_Disp;
                                Record.Parent_Asset_Number = asset.Parent_Asset_Number;
                                Record.Old_Asset_Number = asset.Old_Asset_Number;
                                Record.Description = asset.Description;
                                Record.Authorization_Name = asset.Authorization_Name;
                                Record.Authorization_Number = asset.Authorization_Number;
                                Record.Building_STRU = asset.Building_STRU;
                                Record.Building_Total_Floor = asset.Building_Total_Floor;
                                Record.Build_Address = asset.Build_Address;
                                Record.Build_Number = asset.Build_Number;
                                Record.City_Code = asset.City_Code;
                                Record.District_Code = asset.District_Code;
                                Record.Section_Code = asset.Section_Code;
                                Record.Sub_Sectioni_Name = asset.Sub_Sectioni_Name;
                                Record.Used_Type = asset.Used_Type;
                                Record.Used_Status = asset.Used_Status;
                                Record.Obtained_Method = asset.Obtained_Method;
                                Record.Original_Cost = asset.Original_Cost;
                                Record.Salvage_Value = asset.Salvage_Value;
                                Record.Deprn_Amount = asset.Deprn_Amount;
                                Record.Reval_Adjustment_Amount = asset.Reval_Adjustment_Amount;
                                Record.Business_Area_Size = asset.Business_Area_Size;
                                Record.NONBusiness_Area_Size = asset.NONBusiness_Area_Size;
                                Record.Current_Cost = asset.Current_Cost;
                                Record.Reval_Reserve = asset.Reval_Reserve;
                                Record.Business_Book_Amount = asset.Business_Book_Amount;
                                Record.NONBusiness_Book_Amount = asset.NONBusiness_Book_Amount;
                                Record.Deprn_Reserve = asset.Deprn_Reserve;
                                Record.Business_Deprn_Reserve = asset.Business_Deprn_Reserve;
                                Record.NONBusiness_Deprn_Reserve = asset.NONBusiness_Deprn_Reserve;
                                Record.Date_Placed_In_Service = asset.Date_Placed_In_Service;
                                Record.Remark1 = asset.Remark1;
                                Record.Remark2 = asset.Remark2;
                                Record.Current_Units = asset.Current_Units;
                                Record.Urban_Renewal = asset.Urban_Renewal;
                                Record.Last_Updated_Time = DateTime.Now;
                                Record.Last_Updated_By = Convert.ToDecimal(asset.Last_Updated_By);

                                db.Entry(Record).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();

                                //寫入異動紀錄
                                foreach (var record in change)
                                {
                                    record.TRX_Header_ID = TRXHeader_ID;
                                    record.AS_Assets_Build_ID = db.AS_Assets_Build.Where(m=>m.Asset_Number == asset.Asset_Number).SingleOrDefault().ID;
                                    BuildChangeRecord(db, record);
                                }
                            }
                            else
                            {
                                Record = BuildGeneral.ToAS_Assets_Build_Record(Header.Form_Number, asset);
                                if(string.IsNullOrEmpty(Record.Asset_Number))
                                {
                                    var CodeArray = DetachCategoryCode(Record.Asset_Category_Code); //拆資產分類代碼
                                    if (CodeArray != null)
                                    {
                                        Record.Asset_Number = CreateAssetNumber(CodeArray[0], CodeArray[1]); //產生資產編號
                                    }
                                }
                                Record.Book_Type = Record.Book_Type ?? string.Empty;
                                Record.Officer_Branch = Record.Officer_Branch ?? string.Empty;

                                db.Entry(Record).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                            #endregion

                            AssetID = Record.ID;

                            #region AS_Assets_Build_Detail_Record

                            //BuildDetails
                            //BuildDetails先刪除全部
                            db.AS_Assets_Build_Detail_Record.RemoveRange(db.AS_Assets_Build_Detail_Record.Where(o => o.TRX_Header_ID == Record.TRX_Header_ID && o.Assets_Land_Build_ID == Record.ID));
                            db.SaveChanges();
                            //BuildDetails Insert
                            foreach (var item in asset.BuildDetails)
                            {
                                AS_Assets_Build_Detail_Record obj = new AS_Assets_Build_Detail_Record()
                                {
                                    TRX_Header_ID = Header.Form_Number,
                                    Asset_Type = item.Asset_Type,
                                    Area_Size = item.Area_Size,
                                    Assets_Land_Build_ID = Record.ID,
                                    Authorized_Area = item.Authorized_Area,
                                    Authorized_Scope_Denominx = item.Authorized_Scope_Denominx,
                                    Authorized_Scope_Moldcule = item.Authorized_Scope_Moldcule,
                                    Floor_Uses = item.Floor_Uses,
                                    Created_By = "",
                                    Create_Time = DateTime.Now,
                                    Last_Updated_By = ""
                                };
                                db.Entry(obj).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }

                            #endregion

                            #region AS_Assets_Build_Land_Record
                            //BuildLand
                            //BuildLand先刪除全部
                            db.AS_Assets_Build_Land_Record.RemoveRange(db.AS_Assets_Build_Land_Record.Where(o => o.TRX_Header_ID == Record.TRX_Header_ID && o.Asset_Build_ID == Record.ID));
                            db.SaveChanges();
                            //BuildLand Insert
                            foreach (var item in asset.BuildLands)
                            {
                                AS_Assets_Build_Land_Record obj = new AS_Assets_Build_Land_Record()
                                {
                                    TRX_Header_ID = Header.Form_Number,
                                    Asset_Build_ID = Record.ID,
                                    Asset_Land_ID = item.ID,
                                    Created_By = "",
                                    Create_Time = DateTime.Now,
                                    Last_Updated_By = ""                                   
                                };
                                db.Entry(obj).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                            #endregion

                            #region AS_Assets_Build_Retire

                            if (model.Retire != null)
                            {
                                decimal serial = 0;
                                if(db.AS_Assets_Build_Retire.Any(m => m.TRX_Header_ID == TRXHeader_ID))
                                {
                                    serial = db.AS_Assets_Build_Retire.Where(m => m.TRX_Header_ID == TRXHeader_ID)
                                             .OrderByDescending(a => a.Serial_Number).FirstOrDefault().Serial_Number;
                                }
                                foreach (var item in model.Retire)
                                {
                                    AS_Assets_Build_Retire RetireObj = new AS_Assets_Build_Retire();
                                    if (db.AS_Assets_Build_Retire.Any(m => m.ID == item.ID))
                                    {
                                        //edit
                                        RetireObj = db.AS_Assets_Build_Retire.FirstOrDefault(m => m.ID == item.ID);
                                        RetireObj.Retire_Cost = item.Retire_Cost;
                                        RetireObj.Sell_Amount = item.Sell_Amount;
                                        RetireObj.Sell_Cost = item.Sell_Cost;
                                        //RetireObj.New_Cost = item.New_Cost;
                                        RetireObj.Reval_Adjustment_Amount = item.Reval_Adjustment_Amount;
                                        RetireObj.Reval_Reserve = item.Reval_Reserve;
                                        RetireObj.Reval_Land_VAT = item.Reval_Land_VAT;
                                        RetireObj.Reason_Code = item.Reason_Code;
                                        //RetireObj.Description = item.Description;
                                        //RetireObj.Account_Type = item.Account_Type;
                                        //RetireObj.Receipt_Type = item.Receipt_Type;
                                        //RetireObj.Retire_Cost_Account = item.Retire_Cost_Account;
                                        //RetireObj.Sell_Amount_Account = item.Sell_Amount_Account;
                                        //RetireObj.Sell_Cost_Account = item.Sell_Cost_Account;
                                        //RetireObj.Reval_Adjustment_Amount_Account = item.Reval_Adjustment_Amount_Account;
                                        //RetireObj.Reval_Reserve_Account = item.Reval_Reserve_Account;
                                        //RetireObj.Reval_Land_VAT_Account = item.Reval_Land_VAT_Account;
                                        RetireObj.Last_UpDatetimed_Time = item.Last_UpDatetimed_Time;
                                        RetireObj.Last_UpDatetimed_By = item.Last_UpDatetimed_By;
                                        RetireObj.Last_UpDatetimed_By_Name = item.Last_UpDatetimed_By_Name;

                                        db.Entry(RetireObj).State = System.Data.Entity.EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        //create
                                        RetireObj.TRX_Header_ID = TRXHeader_ID;
                                        RetireObj.Asset_Build_ID = item.Asset_Build_ID;
                                        RetireObj.Serial_Number = serial == 0 ? item.Serial_Number : serial + item.Serial_Number;
                                        RetireObj.Asset_Number = item.Asset_Number;
                                        RetireObj.Parent_Asset_Number = item.Parent_Asset_Number;
                                        RetireObj.Old_Cost = item.Old_Cost;
                                        RetireObj.Retire_Cost = item.Retire_Cost;
                                        RetireObj.Sell_Amount = item.Sell_Amount;
                                        RetireObj.Sell_Cost = item.Sell_Cost;
                                        RetireObj.New_Cost = item.New_Cost;
                                        RetireObj.Reval_Adjustment_Amount = item.Reval_Adjustment_Amount;
                                        RetireObj.Reval_Reserve = item.Reval_Reserve;
                                        RetireObj.Reval_Land_VAT = item.Reval_Land_VAT;
                                        RetireObj.Reason_Code = item.Reason_Code;
                                        RetireObj.Description = item.Description;
                                        RetireObj.Account_Type = item.Account_Type;
                                        RetireObj.Receipt_Type = item.Receipt_Type;
                                        RetireObj.Retire_Cost_Account = item.Retire_Cost_Account;
                                        RetireObj.Sell_Amount_Account = item.Sell_Amount_Account;
                                        RetireObj.Sell_Cost_Account = item.Sell_Cost_Account;
                                        RetireObj.Reval_Adjustment_Amount_Account = item.Reval_Adjustment_Amount_Account;
                                        RetireObj.Reval_Reserve_Account = item.Reval_Reserve_Account;
                                        RetireObj.Reval_Land_VAT_Account = item.Reval_Land_VAT_Account;
                                        RetireObj.Create_Time = item.Create_Time;
                                        RetireObj.Created_By = item.Created_By;
                                        RetireObj.Created_By_Name = item.Created_By_Name;
                                        RetireObj.Last_UpDatetimed_Time = item.Last_UpDatetimed_Time;
                                        RetireObj.Last_UpDatetimed_By = item.Last_UpDatetimed_By;
                                        RetireObj.Last_UpDatetimed_By_Name = item.Last_UpDatetimed_By_Name;

                                        db.Entry(RetireObj).State = System.Data.Entity.EntityState.Added;
                                        db.SaveChanges();
                                    }                                    
                                }
                            }
                            #endregion

                            #region AS_Assets_Build_Category
                            if (model.Category != null && model.Category.Count()>0)
                            {

                                decimal serial = 0;

                                if (db.AS_Assets_Build_Category.Any(m => m.TRX_Header_ID == TRXHeader_ID))
                                {
                                    serial = db.AS_Assets_Build_Category.Where(m => m.TRX_Header_ID == TRXHeader_ID)
                                             .OrderByDescending(a => a.Serial_Number).FirstOrDefault().Serial_Number;
                                }

                                foreach (var item in model.Category)
                                {
                                     AS_Assets_Build_Category Obj = new AS_Assets_Build_Category();
                                    if (db.AS_Assets_Build_Category.Any(m=>m.ID == item.ID))
                                    {
                                        //edit
                                        Obj = db.AS_Assets_Build_Category.FirstOrDefault(m => m.ID == item.ID);
                                        Obj.NEW_Asset_Category_Code = item.NEW_Asset_Category_Code;
                                        Obj.Description = item.Description;
                                        Obj.Expense_Account = item.Expense_Account; //會計帳目
                                        Obj.Last_UpDatetimed_Time = item.Last_UpDatetimed_Time;
                                        Obj.Last_UpDatetimed_By = item.Last_UpDatetimed_By;
                                        Obj.Last_UpDatetimed_By_Name = item.Last_UpDatetimed_By_Name;

                                        db.Entry(Obj).State = System.Data.Entity.EntityState.Modified;
                                        db.SaveChanges();

                                        //寫入異動紀錄
                                        item.BuildChangeRecord.TRX_Header_ID = Obj.TRX_Header_ID;
                                        item.BuildChangeRecord.AS_Assets_Build_ID = Obj.Asset_Build_ID;
                                        BuildChangeRecord(db, item.BuildChangeRecord);
                                    }
                                    else
                                    {
                                        //create
                                        Obj.TRX_Header_ID = TRXHeader_ID;
                                        Obj.Asset_Build_ID = item.Asset_Build_ID;
                                        Obj.Serial_Number = serial == 0 ? item.Serial_Number : serial + item.Serial_Number;
                                        Obj.Asset_Number = item.Asset_Number;
                                        Obj.Old_Asset_Category_Code = item.Old_Asset_Category_Code;
                                        Obj.NEW_Asset_Category_Code = item.NEW_Asset_Category_Code;
                                        Obj.Description = item.Description;
                                        Obj.Expense_Account = item.Expense_Account;
                                        Obj.Create_Time = item.Create_Time;
                                        Obj.Created_By = item.Created_By;
                                        Obj.Created_By_Name = item.Created_By_Name;
                                        Obj.Last_UpDatetimed_Time = item.Last_UpDatetimed_Time;
                                        Obj.Last_UpDatetimed_By = item.Last_UpDatetimed_By;
                                        Obj.Last_UpDatetimed_By_Name = item.Last_UpDatetimed_By_Name;

                                        db.Entry(Obj).State = System.Data.Entity.EntityState.Added;
                                        db.SaveChanges();

                                        //寫入異動紀錄
                                        item.BuildChangeRecord.TRX_Header_ID = Obj.TRX_Header_ID;
                                        item.BuildChangeRecord.AS_Assets_Build_ID = Obj.Asset_Build_ID;
                                        BuildChangeRecord(db, item.BuildChangeRecord);
                                    }

                                }
                            }
                            #endregion

                            #region AS_Assets_Build_Years
                            if (model.Year != null && model.Year.Count() > 0)
                            {

                                decimal serial = 0;

                                if (db.AS_Assets_Build_Years.Any(m => m.TRX_Header_ID == TRXHeader_ID))
                                {
                                    serial = db.AS_Assets_Build_Years.Where(m => m.TRX_Header_ID == TRXHeader_ID)
                                             .OrderByDescending(a => a.Serial_Number).FirstOrDefault().Serial_Number;
                                }

                                foreach (var item in model.Year)
                                {
                                    AS_Assets_Build_Years Obj = new AS_Assets_Build_Years();
                                    if (db.AS_Assets_Build_Years.Any(m => m.ID == item.ID))
                                    {
                                        //edit
                                        Obj = db.AS_Assets_Build_Years.FirstOrDefault(m => m.ID == item.ID);
                                        Obj.New_Life_Years = item.New_Life_Years;
                                        Obj.New_Life_Months = item.New_Life_Months;
                                        Obj.Description = item.Description;
                                        Obj.Last_UpDatetimed_Time = item.Last_UpDatetimed_Time;
                                        Obj.Last_UpDatetimed_By = item.Last_UpDatetimed_By;
                                        Obj.Last_UpDatetimed_By_Name = item.Last_UpDatetimed_By_Name;

                                        db.Entry(Obj).State = System.Data.Entity.EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        //create
                                        Obj.TRX_Header_ID = TRXHeader_ID;
                                        Obj.Asset_Build_ID = item.Asset_Build_ID;
                                        Obj.Serial_Number = serial == 0 ? item.Serial_Number : serial + item.Serial_Number;
                                        Obj.Asset_Number = item.Asset_Number;
                                        Obj.Old_Life_Years = item.Old_Life_Years;
                                        Obj.Old_Life_Months = item.Old_Life_Months;
                                        Obj.New_Life_Years = item.New_Life_Years;
                                        Obj.New_Life_Months = item.New_Life_Months;
                                        Obj.Description = item.Description;
                                        Obj.Create_Time = item.Create_Time;
                                        Obj.Created_By = item.Created_By;
                                        Obj.Created_By_Name = item.Created_By_Name;
                                        Obj.Last_UpDatetimed_Time = item.Last_UpDatetimed_Time;
                                        Obj.Last_UpDatetimed_By = item.Last_UpDatetimed_By;
                                        Obj.Last_UpDatetimed_By_Name = item.Last_UpDatetimed_By_Name;

                                        db.Entry(Obj).State = System.Data.Entity.EntityState.Added;
                                        db.SaveChanges();

                                   
                                    }
                                    //寫入異動紀錄
                                    foreach (var record in item.BuildChangeRecord)
                                    {
                                        record.TRX_Header_ID = Obj.TRX_Header_ID;
                                        record.AS_Assets_Build_ID = Obj.Asset_Build_ID;
                                        BuildChangeRecord(db, record);
                                    }

                                }
                            }
                            #endregion
                        }
                    }

                    trans.Complete();
                }
            }
            catch (Exception ex)
            {

            }
            return new AssetHandleResult() { Form_Number = model.TRXHeader.Form_Number, AssetID = AssetID };
        }
        public List<BuildMPChangeRecord> GenBuildMPAssetChangeRocord(AssetMPModel modify, AS_Assets_Build_MP_Record original)
        {
            PropertyInfo[] props = typeof(AS_Assets_Build_MP_Record).GetProperties();
            PropertyInfo[] AssetMPProps = typeof(AssetMPModel).GetProperties();
            string[] NotInCondition = new string[] { "ID","Assets_Build_MP_ID", "TRX_Header_ID", "Created_By", "Create_Time", "Created_By_Name", "Last_UpDatetimed_By", "Last_UpDatetimed_By_Name", "Last_UpDatetimed_Time", "Last_Updated_Time", "Last_Updated_By", "Delete_Reason" };
            var list = props.Where(m => !NotInCondition.Contains(m.Name)).Select(m => m.Name).ToList();
            List<BuildMPChangeRecord> change = new List<BuildMPChangeRecord>();
            for (int i = 0; i < list.Count(); i++)
            {
                if (AssetMPProps.Any(m => m.Name == list[i]))
                {
                    object o = typeof(AS_Assets_Build_MP_Record).GetProperty(list[i]).GetValue(original);
                    object m = typeof(AssetMPModel).GetProperty(list[i]).GetValue(modify);
                    o = o ?? string.Empty;
                    m = m ?? string.Empty;
                    if(!o.Equals(m))
                    {
                        change.Add(new BuildMPChangeRecord()
                        {
                            Field_Name = list[i],
                            Before_Data = o.ToString(),
                            After_Data = m.ToString()
                        });
                    }
                }
            }
            return change;
        }
        public List<BuildChangeRecord> GenBuildAssetChangeRocord(AssetModel modify, AS_Assets_Build_Record original)
        {
            PropertyInfo[] props = typeof(AS_Assets_Build_Record).GetProperties();
            PropertyInfo[] AssetProps = typeof(AssetModel).GetProperties();
            string[] NotInCondition = new string[] { "ID","Assets_Build_ID", "TRX_Header_ID", "Created_By", "Create_Time", "Created_By_Name", "Last_UpDatetimed_By", "Last_UpDatetimed_By_Name", "Last_UpDatetimed_Time", "Last_Updated_Time", "Last_Updated_By", "Delete_Reason" };
            var list = props.Where(m=>!NotInCondition.Contains(m.Name)).Select(m => m.Name).ToList();
            List<BuildChangeRecord> change = new List<BuildChangeRecord>();
            for (int i = 0; i < list.Count(); i++)
            {
                if(AssetProps.Any(m=>m.Name == list[i]))
                {
                    object o = typeof(AS_Assets_Build_Record).GetProperty(list[i]).GetValue(original);
                    object m = typeof(AssetModel).GetProperty(list[i]).GetValue(modify);
                    o = o ?? string.Empty;
                    m = m ?? string.Empty;
                    if (!o.Equals(m))
                    {
                            change.Add(new BuildChangeRecord() {
                            Field_Name = list[i],
                            Before_Data = o.ToString(),
                            After_Data = m.ToString()
                        });
                    }
                }
            }
            return change;
        }
        public void BuildMPChangeRecord(AS_LandBankEntities db, BuildMPChangeRecord data)
        {
            if (data == null)
                return;
            if (data.TRX_Header_ID == 0 || data.AS_Assets_Build_MP_ID == 0)
                return;
            if (db.AS_Assets_Build_MP_Change.Any(m => m.TRX_Header_ID == data.TRX_Header_ID && m.AS_Assets_Build_MP_ID == data.AS_Assets_Build_MP_ID && m.Field_Name == data.Field_Name))
            {
                //update master and create detail
                AS_Assets_Build_MP_Change master = new AS_Assets_Build_MP_Change();
                master = db.AS_Assets_Build_MP_Change.FirstOrDefault(m => m.TRX_Header_ID == data.TRX_Header_ID && m.AS_Assets_Build_MP_ID == data.AS_Assets_Build_MP_ID && m.Field_Name == data.Field_Name);
                //master.UPD_File = data.UPD_File;
                //master.Modify_Type = data.Modify_Type;
                master.After_Data = data.After_Data;
                master.Last_UpDatetimed_By = data.Created_By;
                master.Last_UpDatetimed_By_Name = data.Created_By_Name;
                master.Last_UpDatetimed_Time = data.Create_Time;
                db.Entry(master).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                AS_Assets_Build_MP_Change_Record detail = new AS_Assets_Build_MP_Change_Record();
                detail.TRX_Header_ID = data.TRX_Header_ID;
                detail.AS_Assets_Build_MP_Record_ID = master.ID;
                detail.UPD_File = data.UPD_File;
                detail.Field_Name = data.Field_Name;
                detail.Modify_Type = data.Modify_Type;
                detail.Before_Data = data.Before_Data;
                detail.After_Data = data.After_Data;
                detail.Created_By = data.Created_By;
                detail.Created_By_Name = data.Created_By_Name;
                detail.Create_Time = data.Create_Time;
                detail.Last_UpDatetimed_By = null;
                detail.Last_UpDatetimed_By_Name = string.Empty;
                detail.Last_UpDatetimed_Time = null;
                db.Entry(detail).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();
            }
            else
            {
                //create master and detail
                AS_Assets_Build_MP_Change master = new AS_Assets_Build_MP_Change();
                master.TRX_Header_ID = data.TRX_Header_ID;
                master.AS_Assets_Build_MP_ID = data.AS_Assets_Build_MP_ID;
                master.UPD_File = data.UPD_File;
                master.Field_Name = data.Field_Name;
                master.Modify_Type = data.Modify_Type;
                master.Before_Data = data.Before_Data;
                master.After_Data = data.After_Data;
                master.Created_By = data.Created_By;
                master.Created_By_Name = data.Created_By_Name;
                master.Create_Time = data.Create_Time;
                master.Last_UpDatetimed_By = null;
                master.Last_UpDatetimed_By_Name = string.Empty;
                master.Last_UpDatetimed_Time = null;
                db.Entry(master).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();

                AS_Assets_Build_MP_Change_Record detail = new AS_Assets_Build_MP_Change_Record();
                detail.TRX_Header_ID = data.TRX_Header_ID;
                detail.AS_Assets_Build_MP_Record_ID = master.ID;
                detail.UPD_File = data.UPD_File;
                detail.Field_Name = data.Field_Name;
                detail.Modify_Type = data.Modify_Type;
                detail.Before_Data = data.Before_Data;
                detail.After_Data = data.After_Data;
                detail.Created_By = data.Created_By;
                detail.Created_By_Name = data.Created_By_Name;
                detail.Create_Time = data.Create_Time;
                detail.Last_UpDatetimed_By = null;
                detail.Last_UpDatetimed_By_Name = string.Empty;
                detail.Last_UpDatetimed_Time = null;
                db.Entry(detail).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();
            }
        }
        public void BuildChangeRecord(AS_LandBankEntities db,BuildChangeRecord data)
        {
            if (data == null)
                return;
            if (data.TRX_Header_ID == 0 || data.AS_Assets_Build_ID == 0)
                return;
            if(db.AS_Assets_Build_Change.Any(m=>m.TRX_Header_ID== data.TRX_Header_ID && m.AS_Assets_Build_ID==data.AS_Assets_Build_ID && m.Field_Name == data.Field_Name))
            {
                //update master and create detail
                AS_Assets_Build_Change master = new AS_Assets_Build_Change();
                master = db.AS_Assets_Build_Change.FirstOrDefault(m => m.TRX_Header_ID == data.TRX_Header_ID && m.AS_Assets_Build_ID == data.AS_Assets_Build_ID && m.Field_Name == data.Field_Name) ;
                //master.UPD_File = data.UPD_File;
                //master.Modify_Type = data.Modify_Type;
                master.After_Data = data.After_Data;
                master.Last_UpDatetimed_By = data.Created_By;
                master.Last_UpDatetimed_By_Name = data.Created_By_Name;
                master.Last_UpDatetimed_Time = data.Create_Time;
                db.Entry(master).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                AS_Assets_Build_Change_Record detail = new AS_Assets_Build_Change_Record();
                detail.TRX_Header_ID = data.TRX_Header_ID;
                detail.AS_Assets_Build_Record_ID = master.ID;
                detail.UPD_File = data.UPD_File;
                detail.Field_Name = data.Field_Name;
                detail.Modify_Type = data.Modify_Type;
                detail.Before_Data = data.Before_Data;
                detail.After_Data = data.After_Data;
                detail.Created_By = data.Created_By;
                detail.Created_By_Name = data.Created_By_Name;
                detail.Create_Time = data.Create_Time;
                detail.Last_UpDatetimed_By = null;
                detail.Last_UpDatetimed_By_Name = string.Empty;
                detail.Last_UpDatetimed_Time = null;
                db.Entry(detail).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();
            }
            else
            {
                //create master and detail
                AS_Assets_Build_Change master = new AS_Assets_Build_Change();
                master.TRX_Header_ID = data.TRX_Header_ID;
                master.AS_Assets_Build_ID = data.AS_Assets_Build_ID;
                master.UPD_File = data.UPD_File;
                master.Field_Name = data.Field_Name;
                master.Modify_Type = data.Modify_Type;
                master.Before_Data = data.Before_Data;
                master.After_Data = data.After_Data;
                master.Created_By = data.Created_By;
                master.Created_By_Name = data.Created_By_Name;
                master.Create_Time = data.Create_Time;
                master.Last_UpDatetimed_By = null;
                master.Last_UpDatetimed_By_Name = string.Empty;
                master.Last_UpDatetimed_Time = null;
                db.Entry(master).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();

                AS_Assets_Build_Change_Record detail = new AS_Assets_Build_Change_Record();
                detail.TRX_Header_ID = data.TRX_Header_ID;
                detail.AS_Assets_Build_Record_ID = master.ID;
                detail.UPD_File = data.UPD_File;
                detail.Field_Name = data.Field_Name;
                detail.Modify_Type = data.Modify_Type;
                detail.Before_Data = data.Before_Data;
                detail.After_Data = data.After_Data;
                detail.Created_By = data.Created_By;
                detail.Created_By_Name = data.Created_By_Name;
                detail.Create_Time = data.Create_Time;
                detail.Last_UpDatetimed_By = null;
                detail.Last_UpDatetimed_By_Name = string.Empty;
                detail.Last_UpDatetimed_Time = null;
                db.Entry(detail).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();
            }
        }
        public AssetHandleResult Create_AssetMP(BuildMPModel model)
        {
            try
            {
                int _TRX_Header_ID = 0;
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        #region AS_TRX_Headers
                        AS_TRX_Headers Header = null;
                        if (!db.AS_TRX_Headers.Any(h => h.Form_Number == model.TRXHeader.Form_Number))
                        {
                            Header = BuildGeneral.ToAS_TRX_Headers(model.TRXHeader);
                            db.Entry(Header).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                            _TRX_Header_ID = Header.ID;
                        }
                        else
                        {
                            Header = db.AS_TRX_Headers.First(h => h.Form_Number == model.TRXHeader.Form_Number);
                            Header.Last_UpDatetimed_By = model.TRXHeader.Last_UpDatetimed_By;
                            Header.Last_UpDatetimed_By_Name = model.TRXHeader.Last_UpDatetimed_By_Name;
                            Header.Last_UpDatetimed_Time = model.TRXHeader.Last_UpDatetimed_Time;
                            db.Entry(Header).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            _TRX_Header_ID = Header.ID;
                        }
                        #endregion

                        foreach (var asset in model.Assets)
                        {
                            #region AS_Assets_Build_MP_Record
                            //Record
                            AS_Assets_Build_MP_Record Record = null;
                            if (db.AS_Assets_Build_MP_Record.Any(o => o.TRX_Header_ID == _TRX_Header_ID && o.ID == asset.ID))
                            {
                                //Build MP Record :edit
                                Record = db.AS_Assets_Build_MP_Record.FirstOrDefault(m => m.ID == asset.ID);

                                //異動紀錄
                                List<BuildMPChangeRecord> change = new List<BuildMPChangeRecord>();
                                if (asset.NeedRecord)
                                {
                                    change = GenBuildMPAssetChangeRocord(asset, Record);
                                }

                                //Record.Book_Type = asset.Book_Type;
                                Record.Asset_Number = asset.Asset_Number;
                                Record.Asset_Category_Code = asset.Asset_Category_Code;
                                Record.Deprn_Method_Code = asset.Deprn_Method_Code;
                                Record.Life_Years = asset.Life_Years;
                                Record.Life_Months = asset.Life_Months;
                                Record.Location_Disp = asset.Location_Disp;
                                Record.Parent_Asset_Number = asset.Parent_Asset_Number;
                                Record.Old_Asset_Number = asset.Old_Asset_Number ?? string.Empty;
                                Record.Description = asset.Description;
                                Record.PO_Number = asset.PO_Number;
                                Record.PO_Destination = asset.PO_Destination;
                                Record.Model_Number = asset.Model_Number;
                                Record.Transaction_Date = asset.Transaction_Date;
                                Record.Assets_Unit = asset.Assets_Unit;
                                Record.Accessory_Equipment = asset.Accessory_Equipment;
                                Record.Assigned_NUM = asset.Assigned_NUM;
                                Record.Asset_Category_NUM = asset.Asset_Category_NUM;
                                Record.Asset_Structure = asset.Asset_Structure ?? string.Empty;
                                Record.Current_Cost = asset.Current_Cost;
                                Record.Deprn_Reserve = asset.Deprn_Reserve;
                                Record.Salvage_Value = asset.Salvage_Value;
                                Record.Date_Placed_In_Service = asset.Date_Placed_In_Service;
                                Record.Remark = asset.Remark;
                                Record.Current_Units = asset.Current_Units;
                                Record.Last_UpDatetimed_Time = DateTime.Now;
                                Record.Last_UpDatetimed_By = asset.Last_UpDatetimed_By;
                                Record.Last_UpDatetimed_By_Name = asset.Last_UpDatetimed_By_Name;

                                db.Entry(Record).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();

                                //寫入異動紀錄
                                foreach (var record in change)
                                {
                                    record.TRX_Header_ID = _TRX_Header_ID;
                                    record.AS_Assets_Build_MP_ID = db.AS_Assets_Build_MP.Where(m => m.Asset_Number == asset.Asset_Number).SingleOrDefault().ID;
                                    BuildMPChangeRecord(db, record);
                                }
                            }
                            else
                            {
                                //create
                                Record = BuildGeneral.ToAS_Assets_Build_MP_Record(_TRX_Header_ID, asset);
                                if (string.IsNullOrEmpty(Record.Asset_Number))
                                {
                                    var CodeArray = DetachCategoryCode(Record.Asset_Category_Code); //拆資產分類代碼
                                    if (CodeArray != null)
                                    {
                                        Record.Asset_Number = CreateAssetNumber(CodeArray[0], CodeArray[1]); //產生資產編號
                                    }
                                }
                                db.Entry(Record).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                            #endregion

                            #region AS_Assets_Build_MP_Retire

                            if (model.Retire != null)
                            {
                                if (model.Retire.Count() == 0)
                                    continue;

                                decimal serial = 0;
                                if (db.AS_Assets_Build_MP_Retire.Any(m => m.TRX_Header_ID == _TRX_Header_ID))
                                {
                                    serial = db.AS_Assets_Build_MP_Retire.Where(m => m.TRX_Header_ID == _TRX_Header_ID)
                                             .OrderByDescending(a => a.Serial_Number).FirstOrDefault().Serial_Number;
                                }
                                foreach (var item in model.Retire)
                                {
                                    AS_Assets_Build_MP_Retire RetireObj = new AS_Assets_Build_MP_Retire();
                                    if (db.AS_Assets_Build_MP_Retire.Any(m => m.ID == item.ID))
                                    {
                                        //edit
                                        RetireObj = db.AS_Assets_Build_MP_Retire.FirstOrDefault(m => m.ID == item.ID);
                                        RetireObj.Retire_Cost = item.Retire_Cost;
                                        RetireObj.Sell_Amount = item.Sell_Amount;
                                        RetireObj.Sell_Cost = item.Sell_Cost;
                                        //RetireObj.New_Cost = item.New_Cost;
                                        RetireObj.Reval_Adjustment_Amount = item.Reval_Adjustment_Amount;
                                        RetireObj.Reval_Reserve = item.Reval_Reserve;
                                        RetireObj.Reval_Land_VAT = item.Reval_Land_VAT;
                                        RetireObj.Reason_Code = item.Reason_Code;
                                        //RetireObj.Description = item.Description;
                                        //RetireObj.Account_Type = item.Account_Type;
                                        //RetireObj.Receipt_Type = item.Receipt_Type;
                                        RetireObj.Last_UpDatetimed_Time = item.Last_UpDatetimed_Time;
                                        RetireObj.Last_UpDatetimed_By = item.Last_UpDatetimed_By;
                                        RetireObj.Last_UpDatetimed_By_Name = item.Last_UpDatetimed_By_Name;

                                        db.Entry(RetireObj).State = System.Data.Entity.EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        //create
                                        RetireObj.TRX_Header_ID = _TRX_Header_ID;
                                        RetireObj.Assets_Build_MP_ID = item.Assets_Build_MP_ID;
                                        RetireObj.Serial_Number = serial == 0 ? item.Serial_Number : serial + item.Serial_Number;
                                        RetireObj.Asset_Number = item.Asset_Number;
                                        RetireObj.Parent_Asset_Number = item.Parent_Asset_Number;
                                        RetireObj.Old_Cost = item.Old_Cost;
                                        RetireObj.Retire_Cost = item.Retire_Cost;
                                        RetireObj.Sell_Amount = item.Sell_Amount;
                                        RetireObj.Sell_Cost = item.Sell_Cost;
                                        RetireObj.New_Cost = item.New_Cost;
                                        RetireObj.Reval_Adjustment_Amount = item.Reval_Adjustment_Amount;
                                        RetireObj.Reval_Reserve = item.Reval_Reserve;
                                        RetireObj.Reval_Land_VAT = item.Reval_Land_VAT;
                                        RetireObj.Reason_Code = item.Reason_Code;
                                        RetireObj.Description = item.Description;
                                        RetireObj.Account_Type = item.Account_Type;
                                        RetireObj.Receipt_Type = item.Receipt_Type;
                                        RetireObj.Create_Time = item.Create_Time;
                                        RetireObj.Created_By = item.Created_By;
                                        RetireObj.Created_By_Name = item.Created_By_Name;
                                        RetireObj.Last_UpDatetimed_Time = item.Last_UpDatetimed_Time;
                                        RetireObj.Last_UpDatetimed_By = item.Last_UpDatetimed_By;
                                        RetireObj.Last_UpDatetimed_By_Name = item.Last_UpDatetimed_By_Name;

                                        db.Entry(RetireObj).State = System.Data.Entity.EntityState.Added;
                                        db.SaveChanges();
                                    }
                                }
                            }
                            #endregion

                            #region AS_Assets_Build_MP_Category
                            if (model.Category != null)
                            {
                                if (model.Category.Count() == 0)
                                    continue;

                                decimal serial = 0;

                                if (db.AS_Assets_Build_MP_Category.Any(m => m.TRX_Header_ID == _TRX_Header_ID))
                                {
                                    serial = db.AS_Assets_Build_MP_Category.Where(m => m.TRX_Header_ID == _TRX_Header_ID)
                                             .OrderByDescending(a => a.Serial_Number).FirstOrDefault().Serial_Number;
                                }

                                foreach (var item in model.Category)
                                {
                                    AS_Assets_Build_MP_Category Obj = new AS_Assets_Build_MP_Category();
                                    if (db.AS_Assets_Build_MP_Category.Any(m => m.ID == item.ID))
                                    {
                                        //edit
                                        Obj = db.AS_Assets_Build_MP_Category.FirstOrDefault(m => m.ID == item.ID);
                                        Obj.NEW_Asset_Category_Code = item.NEW_Asset_Category_Code;
                                        Obj.Description = item.Description;
                                        Obj.Expense_Account = item.Expense_Account; //會計帳目
                                        Obj.Last_UpDatetimed_Time = item.Last_UpDatetimed_Time;
                                        Obj.Last_UpDatetimed_By = item.Last_UpDatetimed_By;
                                        Obj.Last_UpDatetimed_By_Name = item.Last_UpDatetimed_By_Name;

                                        db.Entry(Obj).State = System.Data.Entity.EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        //create
                                        Obj.TRX_Header_ID = _TRX_Header_ID;
                                        Obj.Asset_Build_MP_ID = item.Asset_Build_MP_ID;
                                        Obj.Serial_Number = serial == 0 ? item.Serial_Number : serial + item.Serial_Number;
                                        Obj.Asset_Number = item.Asset_Number;
                                        Obj.Old_Asset_Category_Code = item.Old_Asset_Category_Code;
                                        Obj.NEW_Asset_Category_Code = item.NEW_Asset_Category_Code;
                                        Obj.Description = item.Description;
                                        Obj.Expense_Account = item.Expense_Account;
                                        Obj.Create_Time = item.Create_Time;
                                        Obj.Created_By = item.Created_By;
                                        Obj.Created_By_Name = item.Created_By_Name;
                                        Obj.Last_UpDatetimed_Time = item.Last_UpDatetimed_Time;
                                        Obj.Last_UpDatetimed_By = item.Last_UpDatetimed_By;
                                        Obj.Last_UpDatetimed_By_Name = item.Last_UpDatetimed_By_Name;

                                        db.Entry(Obj).State = System.Data.Entity.EntityState.Added;
                                        db.SaveChanges();
                                    }
                                    //寫入異動紀錄
                                    item.BuildMPChangeRecord.TRX_Header_ID = Obj.TRX_Header_ID;
                                    item.BuildMPChangeRecord.AS_Assets_Build_MP_ID = Obj.Asset_Build_MP_ID;
                                    BuildMPChangeRecord(db, item.BuildMPChangeRecord);
                                }
                            }
                            #endregion

                            #region AS_Assets_Build_MP_Years
                            if (model.Year != null)
                            {
                                if(model.Year.Count() == 0)
                                    continue;

                                decimal serial = 0;

                                if (db.AS_Assets_Build_MP_Years.Any(m => m.TRX_Header_ID == _TRX_Header_ID))
                                {
                                    serial = db.AS_Assets_Build_MP_Years.Where(m => m.TRX_Header_ID == _TRX_Header_ID)
                                             .OrderByDescending(a => a.Serial_Number).FirstOrDefault().Serial_Number;
                                }

                                foreach (var item in model.Year)
                                {
                                    AS_Assets_Build_MP_Years Obj = new AS_Assets_Build_MP_Years();
                                    if (db.AS_Assets_Build_MP_Years.Any(m => m.ID == item.ID))
                                    {
                                        //edit
                                        Obj = db.AS_Assets_Build_MP_Years.FirstOrDefault(m => m.ID == item.ID);
                                        Obj.New_Life_Years = item.New_Life_Years;
                                        Obj.New_Life_Months = item.New_Life_Months;
                                        Obj.Description = item.Description;
                                        Obj.Last_UpDatetimed_Time = item.Last_UpDatetimed_Time;
                                        Obj.Last_UpDatetimed_By = item.Last_UpDatetimed_By;
                                        Obj.Last_UpDatetimed_By_Name = item.Last_UpDatetimed_By_Name;

                                        db.Entry(Obj).State = System.Data.Entity.EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        //create
                                        Obj.TRX_Header_ID = _TRX_Header_ID;
                                        Obj.Asset_Build_MP_ID = item.Asset_Build_MP_ID;
                                        Obj.Serial_Number = serial == 0 ? item.Serial_Number : serial + item.Serial_Number;
                                        Obj.Asset_Number = item.Asset_Number;
                                        Obj.Old_Life_Years = item.Old_Life_Years;
                                        Obj.Old_Life_Months = item.Old_Life_Months;
                                        Obj.New_Life_Years = item.New_Life_Years;
                                        Obj.New_Life_Months = item.New_Life_Months;
                                        Obj.Description = item.Description;
                                        Obj.Create_Time = item.Create_Time;
                                        Obj.Created_By = item.Created_By;
                                        Obj.Created_By_Name = item.Created_By_Name;
                                        Obj.Last_UpDatetimed_Time = item.Last_UpDatetimed_Time;
                                        Obj.Last_UpDatetimed_By = item.Last_UpDatetimed_By;
                                        Obj.Last_UpDatetimed_By_Name = item.Last_UpDatetimed_By_Name;

                                        db.Entry(Obj).State = System.Data.Entity.EntityState.Added;
                                        db.SaveChanges();


                                    }
                                    //寫入異動紀錄
                                    foreach (var record in item.BuildMPChangeRecord)
                                    {
                                        record.TRX_Header_ID = Obj.TRX_Header_ID;
                                        record.AS_Assets_Build_MP_ID = Obj.Asset_Build_MP_ID;
                                        BuildMPChangeRecord(db, record);
                                    }
                                }
                            }
                            #endregion
                        }
                    }

                    trans.Complete();
                }
            }
            catch (Exception ex)
            {

            }
            return new AssetHandleResult() { Form_Number = model.TRXHeader.Form_Number };
        }

        public BuildModel GetDetail(string id)
        {
            BuildModel result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in
                            (
                                from h in db.AS_TRX_Headers
                                where h.Form_Number == id
                                select h
                            )
                            join mas in db.AS_Assets_Build_Record on mh.Form_Number equals mas.TRX_Header_ID into temp
                            from mas in temp.DefaultIfEmpty()
                            join disposal in db.AS_Assets_Build_Retire on new { A = mh.ID, B = mas.Asset_Number } equals new { A = disposal.TRX_Header_ID, B = disposal.Asset_Number } into temp2
                            from disposal in temp2.DefaultIfEmpty()
                            join category in db.AS_Assets_Build_Category on new { A = mh.ID, B = mas.Asset_Number } equals new { A = category.TRX_Header_ID, B = category.Asset_Number } into temp3
                            from category in temp3.DefaultIfEmpty()
                            join year in db.AS_Assets_Build_Years on new { A= mh.ID,B=mas.Asset_Number } equals new { A = year.TRX_Header_ID,B = year.Asset_Number } into temp4
                            from year in temp4.DefaultIfEmpty()
                            select new { mh, mas, disposal, category, year };

                var QueryToList = query.ToList();

                if (query != null && query.Count() > 0)
                {
                    result = new BuildModel()
                    {
                        Header = new BuildHeaderModel(),
                        TRXHeader = query.ToList().Select(o => BuildGeneral.ToBuildTRXHeaderModel(o.mh)).First(),
                        Assets = QueryToList.FirstOrDefault().mas==null? null : QueryToList.Select(o => BuildGeneral.ToAssetModel(o.mas)),
                        Retire = QueryToList.FirstOrDefault().disposal == null ? null: QueryToList.Select(o => BuildGeneral.ToBuildRetireModel(o.disposal)),
                        Category = QueryToList.FirstOrDefault().category == null ? null: QueryToList.Select(o => BuildGeneral.ToBuildCategoryModel(o.category)),
                        Year = QueryToList.FirstOrDefault().year == null ? null: QueryToList.Select(o => BuildGeneral.ToBuildYearModel(o.year)),
                    };
                }
            }

            return result;
        }

        public BuildMPModel GetMPDetail(string id)
        {
            BuildMPModel result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in
                            (
                                from h in db.AS_TRX_Headers
                                where h.Form_Number == id
                                select h
                            )
                            join mas in db.AS_Assets_Build_MP_Record on mh.ID equals mas.TRX_Header_ID into temp
                            from mas in temp.DefaultIfEmpty()
                            join disposal in db.AS_Assets_Build_MP_Retire on new { A = mh.ID, B = mas.Asset_Number } equals new { A = disposal.TRX_Header_ID, B = disposal.Asset_Number } into temp2
                            from disposal in temp2.DefaultIfEmpty()
                            join category in db.AS_Assets_Build_MP_Category on new { A = mh.ID, B = mas.Asset_Number } equals new { A = category.TRX_Header_ID, B = category.Asset_Number } into temp3
                            from category in temp3.DefaultIfEmpty()
                            join year in db.AS_Assets_Build_MP_Years on new { A = mh.ID, B = mas.Asset_Number } equals new { A = year.TRX_Header_ID, B = year.Asset_Number } into temp4
                            from year in temp4.DefaultIfEmpty()
                            select new { mh, mas, disposal, category, year };

                var QueryToList = query.ToList();

                if (query != null && query.Count() > 0)
                {
                    result = new BuildMPModel()
                    {
                        Header = new BuildHeaderModel(),
                        TRXHeader = query.ToList().Select(o => BuildGeneral.ToBuildTRXHeaderModel(o.mh)).First(),
                        Assets = QueryToList.FirstOrDefault().mas == null ? null : QueryToList.Select(o => BuildGeneral.ToAssetMPModel(o.mas)),
                        Retire = QueryToList.FirstOrDefault().disposal == null ? null : QueryToList.Select(o => BuildGeneral.ToBuildMPRetireModel(o.disposal)),
                        Category = QueryToList.FirstOrDefault().category == null ? null : QueryToList.Select(o => BuildGeneral.ToBuildMPCategoryModel(o.category)),
                        Year = QueryToList.FirstOrDefault().year == null ? null : QueryToList.Select(o => BuildGeneral.ToBuildMPYearModel(o.year)),
                    };
                }
            }

            return result;
        }

        public AssetModel GetAssetDetail(int id,out BuildTRXHeaderModel header)
        {
            AssetModel asset = null;
            header = null;
            int headerID = 0;
            int Assets_Build_ID = 0;
            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_Build_Record Asset = (from a in db.AS_Assets_Build_Record
                                                     where a.ID == id
                                                     select a).FirstOrDefault();

                //Assets_Build_ID = db.AS_Assets_Build.Where(m => m.Asset_Number == Asset.Asset_Number).SingleOrDefault().ID;
                if (db.AS_Assets_Build.Any(m => m.Asset_Number == Asset.Asset_Number))
                {
                    Assets_Build_ID = db.AS_Assets_Build.Where(m => m.Asset_Number == Asset.Asset_Number).SingleOrDefault().ID;
                }

                if (Asset != null)
                {
                    asset = BuildGeneral.ToAssetModel(Asset);
                    if(db.AS_TRX_Headers.Any(o => o.Form_Number == Asset.TRX_Header_ID))
                    {
                        var HeaderData = db.AS_TRX_Headers.FirstOrDefault(o => o.Form_Number == Asset.TRX_Header_ID);
                        headerID = HeaderData.ID;
                        header = BuildGeneral.ToBuildTRXHeaderModel(HeaderData);

                    }

                    var buildland = from a in db.AS_Assets_Build_Land_Record
                                    join b in db.AS_Assets_Land on a.Asset_Land_ID equals b.ID
                                    join sectioncode in
                                    (
                                       from dd in db.AS_Land_Code
                                       select new { dd.District_Code, dd.District_Name, dd.City_Code, dd.Section_Code, dd.Sectioni_Name }
                                    ).Distinct() on new { b.City_Code, b.District_Code, b.Section_Code } equals new { sectioncode.City_Code, sectioncode.District_Code, sectioncode.Section_Code } into tsectioncode
                                    from sectioncode in tsectioncode.DefaultIfEmpty()
                                    where a.Asset_Build_ID == Asset.ID && a.TRX_Header_ID == Asset.TRX_Header_ID
                                    select new BuildLandModel()
                                    {
                                        ID = a.Asset_Land_ID,
                                        Parent_Land_Number = b.Parent_Land_Number,
                                        Area_Size = b.Area_Size,
                                        Asset_Number = b.Asset_Number,
                                        Authorized_Area = b.Authorized_Area,
                                        Authorized_Scope_Denomminx = b.Authorized_Scope_Denomminx,
                                        Authorized_Scope_Molecule = b.Authorized_Scope_Molecule,
                                        Filial_Land_Number = b.Filial_Land_Number,
                                        Section = ((sectioncode != null)? sectioncode.Sectioni_Name : "") + b.Sub_Section_Name
                                    };
                     asset.BuildLands = buildland.ToList();

                    var builddetails = from o in db.AS_Assets_Build_Detail_Record
                                       where o.Assets_Land_Build_ID == Asset.ID
                                       select new AssetDetailModel()
                                       {
                                           Asset_Type = o.Asset_Type,
                                           Area_Size = o.Area_Size,
                                           Assets_Land_Build_ID = o.ID,
                                           Authorized_Area = o.Authorized_Area,
                                           Authorized_Scope_Denominx = o.Authorized_Scope_Denominx,
                                           Authorized_Scope_Moldcule = o.Authorized_Scope_Moldcule,
                                           Floor_Uses = o.Floor_Uses
                                       };
                    asset.BuildDetails = builddetails.ToList();

                    var buildcategory = from c in db.AS_Assets_Build_Category
                                        where c.TRX_Header_ID == headerID && c.Asset_Number==asset.Asset_Number
                                        select new BuildCategoryModel()
                                        {
                                            ID = c.ID,
                                            TRX_Header_ID = c.TRX_Header_ID,
                                            Asset_Build_ID = c.Asset_Build_ID,
                                            Serial_Number = c.Serial_Number,
                                            Asset_Number = c.Asset_Number,
                                            Old_Asset_Category_Code = c.Old_Asset_Category_Code,
                                            NEW_Asset_Category_Code = c.NEW_Asset_Category_Code,
                                            Description = c.Description,
                                            Expense_Account = c.Expense_Account,
                                            Create_Time = c.Create_Time,
                                            Created_By = c.Created_By,
                                            Created_By_Name = c.Created_By_Name,
                                            Last_UpDatetimed_Time = c.Last_UpDatetimed_Time,
                                            Last_UpDatetimed_By = c.Last_UpDatetimed_By,
                                            Last_UpDatetimed_By_Name = c.Last_UpDatetimed_By_Name
                                        };
                    asset.BuildCategory = buildcategory.SingleOrDefault();

                    var buildretire = from c in db.AS_Assets_Build_Retire
                                       where c.TRX_Header_ID == headerID && c.Asset_Number == asset.Asset_Number
                                        select new BuildRetireModel()
                                        {
                                            ID = c.ID,
                                            TRX_Header_ID = c.TRX_Header_ID,
                                            Asset_Build_ID = c.Asset_Build_ID,
                                            Serial_Number = c.Serial_Number,
                                            Asset_Number = c.Asset_Number,
                                            Parent_Asset_Number = c.Parent_Asset_Number,
                                            Old_Cost = c.Old_Cost,
                                            Retire_Cost = c.Retire_Cost,
                                            Sell_Amount = c.Sell_Amount,
                                            Sell_Cost = c.Sell_Cost,
                                            New_Cost = c.New_Cost,
                                            Reval_Adjustment_Amount = c.Reval_Adjustment_Amount,
                                            Reval_Reserve = c.Reval_Reserve,
                                            Reval_Land_VAT = c.Reval_Land_VAT,
                                            Reason_Code = c.Reason_Code,
                                            Description = c.Description,
                                            Account_Type = c.Account_Type,
                                            Receipt_Type = c.Receipt_Type,
                                            Retire_Cost_Account = c.Retire_Cost_Account,
                                            Sell_Amount_Account = c.Sell_Amount_Account,
                                            Sell_Cost_Account = c.Sell_Cost_Account,
                                            Reval_Adjustment_Amount_Account = c.Reval_Adjustment_Amount_Account,
                                            Reval_Reserve_Account = c.Reval_Reserve_Account,
                                            Reval_Land_VAT_Account = c.Reval_Land_VAT_Account,
                                            Create_Time = c.Create_Time,
                                            Created_By = c.Created_By,
                                            Created_By_Name = c.Created_By_Name,
                                            Last_UpDatetimed_Time = c.Last_UpDatetimed_Time,
                                            Last_UpDatetimed_By = c.Last_UpDatetimed_By,
                                            Last_UpDatetimed_By_Name = c.Last_UpDatetimed_By_Name
                                        };
                    asset.BuildRetire = buildretire.SingleOrDefault();

                    var buildyear = from c in db.AS_Assets_Build_Years
                                     where c.TRX_Header_ID == headerID && c.Asset_Number == asset.Asset_Number
                                      select new BuildYearModel()
                                      {
                                          ID = c.ID,
                                          TRX_Header_ID = c.TRX_Header_ID,
                                          Asset_Build_ID = c.Asset_Build_ID,
                                          Serial_Number = c.Serial_Number,
                                          Asset_Number = c.Asset_Number,
                                          Old_Life_Years = c.Old_Life_Years,
                                          Old_Life_Months = c.Old_Life_Months,
                                          New_Life_Years = c.New_Life_Years,
                                          New_Life_Months = c.New_Life_Months,
                                          Description = c.Description,
                                          Create_Time = c.Create_Time,
                                          Created_By = c.Created_By,
                                          Created_By_Name = c.Created_By_Name,
                                          Last_UpDatetimed_Time = c.Last_UpDatetimed_Time,
                                          Last_UpDatetimed_By = c.Last_UpDatetimed_By,
                                          Last_UpDatetimed_By_Name = c.Last_UpDatetimed_By_Name
                                      };
                    asset.BuildYear = buildyear.SingleOrDefault();

                    if (Assets_Build_ID != 0)
                    {
                        var buildchange = from c in db.AS_Assets_Build_Change
                                          where c.TRX_Header_ID == headerID && c.AS_Assets_Build_ID == Assets_Build_ID
                                          select new BuildChangeRecord()
                                          {
                                              TRX_Header_ID = c.TRX_Header_ID ?? 0,
                                              AS_Assets_Build_ID = c.AS_Assets_Build_ID ?? 0,
                                              UPD_File = c.UPD_File,
                                              Field_Name = c.Field_Name,
                                              Modify_Type = c.Modify_Type,
                                              Before_Data = c.Before_Data,
                                              After_Data = c.After_Data,
                                              Created_By = c.Created_By,
                                              Created_By_Name = c.Created_By_Name,
                                              Create_Time = c.Create_Time
                                          };
                        asset.BuildChangeRecord = buildchange.ToList();
                    }
                    else
                    {
                        asset.BuildChangeRecord = new List<BuildChangeRecord>();
                    }

                    //var buildchange = from c in db.AS_Assets_Build_Change
                    //                  where c.TRX_Header_ID == headerID && c.AS_Assets_Build_ID == Assets_Build_ID
                    //                  select new BuildChangeRecord()
                    //                  {
                    //                      TRX_Header_ID = c.TRX_Header_ID ?? 0,
                    //                      AS_Assets_Build_ID = c.AS_Assets_Build_ID ?? 0,
                    //                      UPD_File = c.UPD_File,
                    //                      Field_Name = c.Field_Name,
                    //                      Modify_Type = c.Modify_Type,
                    //                      Before_Data = c.Before_Data,
                    //                      After_Data = c.After_Data,
                    //                      Created_By = c.Created_By,
                    //                      Created_By_Name = c.Created_By_Name,
                    //                      Create_Time = c.Create_Time
                    //                  };
                    //asset.BuildChangeRecord = buildchange.ToList();
                }
            }

            return asset;
        }

        public AssetMPModel GetAssetMPDetail(int id, out BuildTRXHeaderModel header)
        {
            AssetMPModel asset = null;
            header = null;
            int headerID = 0;
            int Assets_Build_MP_ID = 0;
            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_Build_MP_Record Asset = (from a in db.AS_Assets_Build_MP_Record
                                                   where a.ID == id
                                                   select a).FirstOrDefault();
                //Assets_Build_MP_ID = db.AS_Assets_Build_MP.Where(m => m.Asset_Number == Asset.Asset_Number).SingleOrDefault().ID;

                if(db.AS_Assets_Build_MP.Any(m => m.Asset_Number == Asset.Asset_Number))
                {
                    Assets_Build_MP_ID = db.AS_Assets_Build_MP.Where(m => m.Asset_Number == Asset.Asset_Number).SingleOrDefault().ID;
                }

                if (Asset != null)
                {
                    asset = BuildGeneral.ToAssetMPModel(Asset);
                    if (db.AS_TRX_Headers.Any(o => o.ID == Asset.TRX_Header_ID))
                    {
                        header = BuildGeneral.ToBuildTRXHeaderModel(db.AS_TRX_Headers.FirstOrDefault(o => o.ID == Asset.TRX_Header_ID));
                        headerID = header.ID;
                    }
                    var buildretire = from c in db.AS_Assets_Build_MP_Retire
                                      where c.TRX_Header_ID == headerID && c.Asset_Number == asset.Asset_Number
                                      select new BuildMPRetireModel()
                                      {
                                          ID = c.ID,
                                          TRX_Header_ID = c.TRX_Header_ID,
                                          Assets_Build_MP_ID = c.Assets_Build_MP_ID,
                                          Serial_Number = c.Serial_Number,
                                          Asset_Number = c.Asset_Number,
                                          Parent_Asset_Number = c.Parent_Asset_Number,
                                          Old_Cost = c.Old_Cost,
                                          Retire_Cost = c.Retire_Cost,
                                          Sell_Amount = c.Sell_Amount,
                                          Sell_Cost = c.Sell_Cost,
                                          New_Cost = c.New_Cost,
                                          Reval_Adjustment_Amount = c.Reval_Adjustment_Amount,
                                          Reval_Reserve = c.Reval_Reserve,
                                          Reval_Land_VAT = c.Reval_Land_VAT,
                                          Reason_Code = c.Reason_Code,
                                          Description = c.Description,
                                          Account_Type = c.Account_Type,
                                          Receipt_Type = c.Receipt_Type,
                                          Create_Time = c.Create_Time,
                                          Created_By = c.Created_By,
                                          Created_By_Name = c.Created_By_Name,
                                          Last_UpDatetimed_Time = c.Last_UpDatetimed_Time,
                                          Last_UpDatetimed_By = c.Last_UpDatetimed_By,
                                          Last_UpDatetimed_By_Name = c.Last_UpDatetimed_By_Name
                                      };
                    asset.BuildRetire = buildretire.SingleOrDefault();
                    var buildcategory = from c in db.AS_Assets_Build_MP_Category
                                        where c.TRX_Header_ID == headerID && c.Asset_Number == asset.Asset_Number
                                        select new BuildMPCategoryModel()
                                        {
                                            ID = c.ID,
                                            TRX_Header_ID = c.TRX_Header_ID,
                                            Asset_Build_MP_ID = c.Asset_Build_MP_ID,
                                            Serial_Number = c.Serial_Number,
                                            Asset_Number = c.Asset_Number,
                                            Old_Asset_Category_Code = c.Old_Asset_Category_Code,
                                            NEW_Asset_Category_Code = c.NEW_Asset_Category_Code,
                                            Description = c.Description,
                                            Expense_Account = c.Expense_Account,
                                            Create_Time = c.Create_Time,
                                            Created_By = c.Created_By,
                                            Created_By_Name = c.Created_By_Name,
                                            Last_UpDatetimed_Time = c.Last_UpDatetimed_Time,
                                            Last_UpDatetimed_By = c.Last_UpDatetimed_By,
                                            Last_UpDatetimed_By_Name = c.Last_UpDatetimed_By_Name
                                        };
                    asset.BuildCategory = buildcategory.SingleOrDefault();
                    var buildyear = from c in db.AS_Assets_Build_MP_Years
                                    where c.TRX_Header_ID == headerID && c.Asset_Number == asset.Asset_Number
                                    select new BuildMPYearModel()
                                    {
                                        ID = c.ID,
                                        TRX_Header_ID = c.TRX_Header_ID,
                                        Asset_Build_MP_ID = c.Asset_Build_MP_ID,
                                        Serial_Number = c.Serial_Number,
                                        Asset_Number = c.Asset_Number,
                                        Old_Life_Years = c.Old_Life_Years,
                                        Old_Life_Months = c.Old_Life_Months,
                                        New_Life_Years = c.New_Life_Years,
                                        New_Life_Months = c.New_Life_Months,
                                        Description = c.Description,
                                        Create_Time = c.Create_Time,
                                        Created_By = c.Created_By,
                                        Created_By_Name = c.Created_By_Name,
                                        Last_UpDatetimed_Time = c.Last_UpDatetimed_Time,
                                        Last_UpDatetimed_By = c.Last_UpDatetimed_By,
                                        Last_UpDatetimed_By_Name = c.Last_UpDatetimed_By_Name
                                    };
                    asset.BuildYear = buildyear.SingleOrDefault();

                    if(Assets_Build_MP_ID != 0)
                    {
                        var buildchange = from c in db.AS_Assets_Build_MP_Change
                                          where c.TRX_Header_ID == headerID && c.AS_Assets_Build_MP_ID == Assets_Build_MP_ID
                                          select new BuildMPChangeRecord()
                                          {
                                              TRX_Header_ID = c.TRX_Header_ID ?? 0,
                                              AS_Assets_Build_MP_ID = c.AS_Assets_Build_MP_ID ?? 0,
                                              UPD_File = c.UPD_File,
                                              Field_Name = c.Field_Name,
                                              Modify_Type = c.Modify_Type,
                                              Before_Data = c.Before_Data,
                                              After_Data = c.After_Data,
                                              Created_By = c.Created_By,
                                              Created_By_Name = c.Created_By_Name,
                                              Create_Time = c.Create_Time
                                          };
                        asset.BuildMPChangeRecord = buildchange.ToList();
                    }
                    else
                    {
                        asset.BuildMPChangeRecord = new List<BuildMPChangeRecord>();
                    }
                    //var buildchange = from c in db.AS_Assets_Build_MP_Change
                    //                  where c.TRX_Header_ID == headerID && c.AS_Assets_Build_MP_ID == Assets_Build_MP_ID
                    //                  select new BuildMPChangeRecord()
                    //                  {
                    //                      TRX_Header_ID = c.TRX_Header_ID ?? 0,
                    //                      AS_Assets_Build_MP_ID = c.AS_Assets_Build_MP_ID ?? 0,
                    //                      UPD_File = c.UPD_File,
                    //                      Field_Name = c.Field_Name,
                    //                      Modify_Type = c.Modify_Type,
                    //                      Before_Data = c.Before_Data,
                    //                      After_Data = c.After_Data,
                    //                      Created_By = c.Created_By,
                    //                      Created_By_Name = c.Created_By_Name,
                    //                      Create_Time = c.Create_Time
                    //                  };
                    //asset.BuildMPChangeRecord = buildchange.ToList();
                }
            }

            return asset;
        }
        

        public bool GetDocID(string DocCode, out int DocID)
        {
            bool result = true;
            using (var db = new AS_LandBankEntities())
            {
                result = db.AS_Doc_Name.Any(m => m.Doc_No == DocCode);
                DocID = (result) ? db.AS_Doc_Name.First(m => m.Doc_No == DocCode).ID : 0;
            }
            return result;
        }

        public AssetHandleResult CreateToBuild(string TRXHeaderID)
        {
            AssetHandleResult result = new AssetHandleResult() { };

            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        AS_Assets_Build_Header Header = db.AS_Assets_Build_Header.First(o => o.TRX_Header_ID == TRXHeaderID);
                        Header.Last_Updated_Time = DateTime.Now;
                        Header.Transaction_Status = "2";
                        db.Entry(Header).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        //AS_Assets_Build_Record To AS_Assets_Build
                        var query = from m in db.AS_Assets_Build_Record
                                    where m.TRX_Header_ID == TRXHeaderID
                                    select m;

                        foreach (var item in query)
                        {
                            int ID = 1;
                            if (db.AS_Assets_Build.Count() > 0)
                            {
                                ID = db.AS_Assets_Build.Max(o => o.ID) + 1;
                            }

                            AS_Assets_Build obj = new AS_Assets_Build()
                            {
                                ID = ID,
                                Asset_Number = item.Asset_Number,
                                Asset_Category_Code = item.Asset_Category_Code,
                                Deprn_Method_Code = item.Deprn_Method_Code,
                                Life_Years = item.Life_Years,
                                Life_Months = item.Life_Months,
                                Location_Disp = item.Location_Disp,
                                Parent_Asset_Number = item.Parent_Asset_Number,
                                Old_Asset_Number = item.Old_Asset_Number,
                                Description = item.Description,
                                Authorization_Name = item.Authorization_Name,
                                Authorization_Number = item.Authorization_Number,
                                Building_STRU = item.Building_STRU,
                                Building_Total_Floor = item.Building_Total_Floor,
                                Build_Address = item.Build_Address,
                                Build_Number = item.Build_Number,
                                City_Code = item.City_Code,
                                District_Code = item.District_Code,
                                Section_Code = item.Section_Code,
                                Sub_Sectioni_Name = item.Section_Code,
                                Used_Type = item.Used_Type,
                                Used_Status = item.Used_Status,
                                Obtained_Method = item.Obtained_Method,
                                Original_Cost = item.Original_Cost,
                                Salvage_Value = item.Salvage_Value,
                                Deprn_Amount = item.Deprn_Amount,
                                Reval_Adjustment_Amount = item.Reval_Adjustment_Amount,
                                Business_Area_Size = item.Business_Area_Size,
                                NONBusiness_Area_Size = item.NONBusiness_Area_Size,
                                Current_Cost = item.Current_Cost,
                                Reval_Reserve = item.Reval_Reserve,
                                Business_Book_Amount = item.Business_Book_Amount,
                                NONBusiness_Book_Amount = item.NONBusiness_Book_Amount,
                                Deprn_Reserve = item.Deprn_Reserve,
                                Business_Deprn_Reserve = item.Business_Deprn_Reserve,
                                NONBusiness_Deprn_Reserve = item.NONBusiness_Deprn_Reserve,
                                Date_Placed_In_Service = item.Date_Placed_In_Service,
                                Remark1 = item.Remark1,
                                Remark2 = item.Remark2,
                                Current_Units = item.Current_Units,
                                Delete_Reason = item.Delete_Reason,
                                Urban_Renewal = item.Urban_Renewal,
                                Create_Time = DateTime.Now,
                                Created_By = item.Created_By.ToString(),
                                Last_Updated_By = item.Last_Updated_By.ToString(),
                                Book_Type = Header.Book_Type,
                                City_Name = "",
                                District_Name = "",
                                Sectioni_Name = "",
                                Officer_Branch = Header.Office_Branch,
                                Old_Asset_Category_Code = ""
                            };
                            db.Entry(obj).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();


                            //AS_Assets_Build_Detail_Record To AS_Assets_Build_Detail
                            var builddetail = from m in db.AS_Assets_Build_Detail_Record
                                              where m.TRX_Header_ID == TRXHeaderID && m.Assets_Land_Build_ID == item.ID
                                              select m;

                            foreach (var builddetailitem in builddetail)
                            {
                                int BDID = 1;
                                if (db.AS_Assets_Build_Detail.Count() > 0)
                                {
                                    BDID = db.AS_Assets_Build_Detail.Max(o => o.ID) + 1;
                                }

                                AS_Assets_Build_Detail BD = new AS_Assets_Build_Detail()
                                {
                                    ID = BDID,
                                    Area_Size = builddetailitem.Area_Size,
                                    Assets_Build_ID = obj.ID,
                                    Asset_Type = builddetailitem.Asset_Type,
                                    Authorized_Area = builddetailitem.Authorized_Area,
                                    Authorized_Scope_Denominx = builddetailitem.Authorized_Scope_Denominx,
                                    Authorized_Scope_Moldcule = builddetailitem.Authorized_Scope_Moldcule,
                                    Floor_Uses = builddetailitem.Floor_Uses,
                                    Create_Time = DateTime.Now,
                                    Created_By = builddetailitem.Created_By,
                                    Last_Updated_By = builddetailitem.Last_Updated_By,
                                    Own_Area = 0,
                                    NONOwn_Area = 0
                                };
                                db.Entry(BD).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }

                            //AS_Assets_Build_Land_Record To AS_Assets_Build_Land
                            var buildlands = from m in db.AS_Assets_Build_Land_Record
                                             where m.TRX_Header_ID == TRXHeaderID && m.Asset_Build_ID == item.ID
                                             select m;

                            foreach (var buildlandsitem in buildlands)
                            {
                                int BLID = 1;
                                if (db.AS_Assets_Build_Land.Count() > 0)
                                {
                                    BLID = db.AS_Assets_Build_Land.Max(o => o.ID) + 1;
                                }

                                AS_Assets_Build_Land BL = new AS_Assets_Build_Land()
                                {
                                    ID = BLID,
                                    Asset_Build_ID = obj.ID,
                                    Asset_Land_ID = buildlandsitem.Asset_Land_ID,
                                    Create_Time = DateTime.Now,
                                    Created_By = item.Created_By.ToString(),
                                    Last_Updated_By = item.Last_Updated_By.ToString()
                                };
                                db.Entry(BL).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }

                        }
                    }

                    trans.Complete();
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public AssetHandleResult CreateToBuildMP(string TRXHeaderID)
        {
            AssetHandleResult result = new AssetHandleResult() { };
            AS_TRX_Headers _Header = null;
            AS_Assets_Build _Build = null;
            AS_Assets_Build_MP_Record _Record = null;
            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        //AS_Assets_Build_MP_Record To AS_Assets_Build_MP-----------------

                        //TRXHeaderID是單據編號?
                        if(db.AS_TRX_Headers.Any(m => m.Form_Number == TRXHeaderID))
                        {
                            _Header = db.AS_TRX_Headers.Where(m => m.Form_Number == TRXHeaderID).SingleOrDefault();
                        }

                        //取得Record
                        var query = from m in db.AS_Assets_Build_MP_Record
                                    where m.TRX_Header_ID == _Header.ID
                                    select m;
                        _Record = query.SingleOrDefault();

                        //取得Build
                        if (db.AS_Assets_Build.Any(m => m.Asset_Number == _Record.Asset_Number))
                        {
                            _Build = db.AS_Assets_Build.Where(m => m.Asset_Number == _Record.Asset_Number).SingleOrDefault();
                        }

                        foreach (var item in query)
                        {
                            AS_Assets_Build_MP obj = new AS_Assets_Build_MP()
                            {
                                ID = item.ID,
                                Asset_Build_ID = _Build.ID,
                                Asset_Number = item.Asset_Number, //分類號 + 流水號 
                                Parent_Asset_Number = item.Parent_Asset_Number, 
                                Asset_Category_Code = item.Asset_Category_Code, // 分類號
                                Old_Asset_Number = item.Old_Asset_Number, //原資產編號
                                Book_Type = item.Book_Type,
                                Date_Placed_In_Service = item.Date_Placed_In_Service ?? default(DateTime),
                                Current_Units = item.Current_Units,
                                Deprn_Method_Code = item.Deprn_Method_Code,
                                Life_Years = item.Life_Years,
                                Life_Months = item.Life_Months,
                                Location_Disp = item.Location_Disp,
                                PO_Number = item.PO_Number,
                                PO_Destination = item.PO_Destination,
                                Model_Number = item.Model_Number,
                                Transaction_Date = item.Transaction_Date,
                                Assets_Unit = item.Assets_Unit,
                                Accessory_Equipment = item.Accessory_Equipment,
                                Assigned_NUM = item.Assigned_NUM,
                                Asset_Category_NUM = item.Asset_Category_NUM, //財務分類號?
                                Asset_Category_Name = string.Empty, //財物名稱 //新分類名稱? //AS_Assets_MP_HIS?
                                Asset_Structure = item.Asset_Structure,
                                Current_Cost = item.Current_Cost,
                                Deprn_Reserve = item.Deprn_Reserve,
                                Salvage_Value = item.Salvage_Value,
                                Remark = item.Remark,
                                Deprn_Type = 0, //折舊註記
                                Create_Time = item.Create_Time,
                                Created_By = item.Created_By,
                                Created_By_Name = item.Created_By_Name,
                                Last_UpDatetimed_Time = item.Last_UpDatetimed_Time,
                                Last_UpDatetimed_By = item.Last_UpDatetimed_By,
                                Last_UpDatetimed_By_Name = item.Last_UpDatetimed_By_Name
                            };
                            db.Entry(obj).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }
                    }

                    trans.Complete();
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }
        public AssetHandleResult UpdateToBuild(string TRXHeaderID)
        {
            AssetHandleResult result = new AssetHandleResult() { };

            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        AS_Assets_Build_Header Header = db.AS_Assets_Build_Header.First(o => o.TRX_Header_ID == TRXHeaderID);
                        Header.Last_Updated_Time = DateTime.Now;
                        Header.Transaction_Status = "2";
                        db.Entry(Header).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        //AS_Assets_Build_Record To AS_Assets_Build
                        var query = from m in db.AS_Assets_Build_Record
                                    where m.TRX_Header_ID == TRXHeaderID
                                    select m;

                        foreach (var item in query)
                        {
                            if(db.AS_Assets_Build.Any(o=>o.Asset_Number == item.Asset_Number))
                            {
                                AS_Assets_Build obj = db.AS_Assets_Build.First(o => o.Asset_Number == item.Asset_Number);

                                obj.Asset_Category_Code = item.Asset_Category_Code;
                                obj.Deprn_Method_Code = item.Deprn_Method_Code;
                                obj.Life_Years = item.Life_Years;
                                obj.Life_Months = item.Life_Months;
                                obj.Location_Disp = item.Location_Disp;
                                obj.Parent_Asset_Number = item.Parent_Asset_Number;
                                obj.Old_Asset_Number = item.Old_Asset_Number;
                                obj.Description = item.Description;
                                obj.Authorization_Name = item.Authorization_Name;
                                obj.Authorization_Number = item.Authorization_Number;
                                obj.Building_STRU = item.Building_STRU;
                                obj.Building_Total_Floor = item.Building_Total_Floor;
                                obj.Build_Address = item.Build_Address;
                                obj.Build_Number = item.Build_Number;
                                obj.City_Code = item.City_Code;
                                obj.District_Code = item.District_Code;
                                obj.Section_Code = item.Section_Code;
                                obj.Sub_Sectioni_Name = item.Section_Code;
                                obj.Used_Type = item.Used_Type;
                                obj.Used_Status = item.Used_Status;
                                obj.Obtained_Method = item.Obtained_Method;
                                obj.Original_Cost = item.Original_Cost;
                                obj.Salvage_Value = item.Salvage_Value;
                                obj.Deprn_Amount = item.Deprn_Amount;
                                obj.Reval_Adjustment_Amount = item.Reval_Adjustment_Amount;
                                obj.Business_Area_Size = item.Business_Area_Size;
                                obj.NONBusiness_Area_Size = item.NONBusiness_Area_Size;
                                obj.Current_Cost = item.Current_Cost;
                                obj.Reval_Reserve = item.Reval_Reserve;
                                obj.Business_Book_Amount = item.Business_Book_Amount;
                                obj.NONBusiness_Book_Amount = item.NONBusiness_Book_Amount;
                                obj.Deprn_Reserve = item.Deprn_Reserve;
                                obj.Business_Deprn_Reserve = item.Business_Deprn_Reserve;
                                obj.NONBusiness_Deprn_Reserve = item.NONBusiness_Deprn_Reserve;
                                obj.Date_Placed_In_Service = item.Date_Placed_In_Service;
                                obj.Remark1 = item.Remark1;
                                obj.Remark2 = item.Remark2;
                                obj.Current_Units = item.Current_Units;
                                obj.Delete_Reason = item.Delete_Reason;
                                obj.Urban_Renewal = item.Urban_Renewal;
                                obj.Last_Updated_By = item.Last_Updated_By.ToString();
                                obj.Last_Updated_Time = DateTime.Now;
                                obj.Book_Type = Header.Book_Type;
                                obj.Officer_Branch = Header.Office_Branch;



                                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();


                                //AS_Assets_Build_Detail_Record To AS_Assets_Build_Detail
                                //BuildDetails
                                //BuildLand先刪除全部
                                db.AS_Assets_Build_Detail.RemoveRange(db.AS_Assets_Build_Detail.Where(o=>o.Assets_Build_ID == obj.ID));
                                db.SaveChanges();
                                var builddetail = from m in db.AS_Assets_Build_Detail_Record
                                                  where m.TRX_Header_ID == TRXHeaderID && m.Assets_Land_Build_ID == item.ID
                                                  select m;

                                foreach (var builddetailitem in builddetail)
                                {
                                    int BDID = 1;
                                    if (db.AS_Assets_Build_Detail.Count() > 0)
                                    {
                                        BDID = db.AS_Assets_Build_Detail.Max(o => o.ID) + 1;
                                    }

                                    AS_Assets_Build_Detail BD = new AS_Assets_Build_Detail()
                                    {
                                        ID = BDID,
                                        Area_Size = builddetailitem.Area_Size,
                                        Assets_Build_ID = obj.ID,
                                        Asset_Type = builddetailitem.Asset_Type,
                                        Authorized_Area = builddetailitem.Authorized_Area,
                                        Authorized_Scope_Denominx = builddetailitem.Authorized_Scope_Denominx,
                                        Authorized_Scope_Moldcule = builddetailitem.Authorized_Scope_Moldcule,
                                        Floor_Uses = builddetailitem.Floor_Uses,
                                        Create_Time = DateTime.Now,
                                        Created_By = builddetailitem.Created_By,
                                        Last_Updated_By = builddetailitem.Last_Updated_By,
                                        Own_Area = 0,
                                        NONOwn_Area = 0
                                    };
                                    db.Entry(BD).State = System.Data.Entity.EntityState.Added;
                                    db.SaveChanges();
                                }

                                //AS_Assets_Build_Land_Record To AS_Assets_Build_Land
                                db.AS_Assets_Build_Land.RemoveRange(db.AS_Assets_Build_Land.Where(o => o.Asset_Build_ID == obj.ID));
                                db.SaveChanges();
                                var buildlands = from m in db.AS_Assets_Build_Land_Record
                                                 where m.TRX_Header_ID == TRXHeaderID && m.Asset_Build_ID == item.ID
                                                 select m;

                                foreach (var buildlandsitem in buildlands)
                                {
                                    int BLID = 1;
                                    if (db.AS_Assets_Build_Land.Count() > 0)
                                    {
                                        BLID = db.AS_Assets_Build_Land.Max(o => o.ID) + 1;
                                    }

                                    AS_Assets_Build_Land BL = new AS_Assets_Build_Land()
                                    {
                                        ID = BLID,
                                        Asset_Build_ID = obj.ID,
                                        Asset_Land_ID = buildlandsitem.Asset_Land_ID,
                                        Create_Time = DateTime.Now,
                                        Created_By = item.Created_By.ToString(),
                                        Last_Updated_By = item.Last_Updated_By.ToString()
                                    };
                                    db.Entry(BL).State = System.Data.Entity.EntityState.Added;
                                    db.SaveChanges();
                                }


                            }
                        }
                    }

                    trans.Complete();
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public AssetHandleResult UpdateToBuildMP(string TRXHeaderID)
        {
            AssetHandleResult result = new AssetHandleResult() { };

            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        var query = from m in db.AS_Assets_Build_MP_Record
                                    where m.TRX_Header_ID == Convert.ToInt32(TRXHeaderID) //TODO: 將會出錯 傳入資料必須為id而非編碼
                                    select m;

                        foreach (var item in query)
                        {
                            if (db.AS_Assets_Build_MP.Any(o => o.Asset_Number == item.Asset_Number))
                            {
                                AS_Assets_Build_MP obj = db.AS_Assets_Build_MP.First(o => o.Asset_Number == item.Asset_Number);

                                obj.Asset_Number = item.Asset_Number;
                                obj.Asset_Category_Code = item.Asset_Category_Code;
                                obj.Deprn_Method_Code = item.Deprn_Method_Code;
                                obj.Life_Years = item.Life_Years;
                                obj.Life_Months = item.Life_Months;
                                obj.Location_Disp = item.Location_Disp;
                                obj.Parent_Asset_Number = item.Parent_Asset_Number;
                                obj.Old_Asset_Number = item.Old_Asset_Number;
                                //obj.Description = item.Description;
                                obj.PO_Number = item.PO_Number;
                                obj.PO_Destination = item.PO_Destination;
                                obj.Model_Number = item.Model_Number;
                                obj.Transaction_Date = item.Transaction_Date;
                                obj.Assets_Unit = item.Assets_Unit;
                                obj.Accessory_Equipment = item.Accessory_Equipment;
                                obj.Assigned_NUM = item.Assigned_NUM;
                                //obj.Assigned_ID = item.Assigned_ID;
                                obj.Asset_Structure = item.Asset_Structure;
                                obj.Current_Cost = item.Current_Cost;
                                obj.Deprn_Reserve = item.Deprn_Reserve;
                                obj.Salvage_Value = item.Salvage_Value;
                                obj.Date_Placed_In_Service = item.Date_Placed_In_Service??default(DateTime);
                                obj.Remark = item.Remark;
                                obj.Current_Units = item.Current_Units;
                                obj.Create_Time = DateTime.Now;
                                obj.Created_By = item.Created_By;
                                obj.Created_By_Name = item.Created_By_Name;
                                obj.Last_UpDatetimed_By_Name = item.Last_UpDatetimed_By_Name;
                                obj.Last_UpDatetimed_By = item.Last_UpDatetimed_By;
                                obj.Last_UpDatetimed_Time = DateTime.Now;

                                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }

                    trans.Complete();
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public AssetsIndexModel SearchAssets(AssetsSearchModel condition)
        {
            AssetsIndexModel result = new AssetsIndexModel()
            {
                 condition = condition,
                 Items = new List<AssetModel>()
            };
            using (var db = new AS_LandBankEntities())
            {
                var query = from m in db.AS_Assets_Build
                            select m;

                if(!string.IsNullOrEmpty(condition.City_Code))
                {
                    //query = query.Where(o => o.City_Code == "0");
                    query = query.Where(o => o.City_Code == condition.City_Code);
                }

                if (!string.IsNullOrEmpty(condition.District_Code))
                {
                    //query = query.Where(o => o.District_Code == "0");
                    query = query.Where(o => o.District_Code == condition.District_Code);
                }

                if (!string.IsNullOrEmpty(condition.Section_Code))
                {
                    //query = query.Where(o => o.Section_Code == "0");
                    query = query.Where(o => o.Section_Code == condition.Section_Code);
                }

                if (!string.IsNullOrEmpty(condition.Build_Number))
                {
                    query = query.Where(o => o.Build_Number == condition.Build_Number);
                }

                if (!string.IsNullOrEmpty(condition.AssetNumber))
                {
                    query = query.Where(o => o.Asset_Number == condition.AssetNumber);
                }

                if(condition.UseAssetNumber != null)
                {
                    if(condition.UseAssetNumber.Count()>0)
                    {
                        query = from p in query
                                where !(condition.UseAssetNumber).Contains(p.Asset_Number)
                                select p;
                    }
                }

                result.TotalAmount = query.Count();

                //分頁
                if (condition.Page != null)
                {
                    query = query.OrderByDescending(m => m.Create_Time).Skip((condition.Page.Value - 1) * Convert.ToInt32(condition.PageSize)).Take(Convert.ToInt32(condition.PageSize));
                }
                result.Items = query.ToList().Select(o => BuildGeneral.ToAssetModel(o));

                //if (condition.NeedUserData)
                //{
                //    var user = db.AS_Users.ToList().Select(m=>BuildGeneral.ToBuildUserModel(m)).ToList();
                //    result.BuildUserModel = user;
                //}else
                //{
                //    result.BuildUserModel = new List<BuildUserModel>();
                //}

            }
            return result;
        }

        public AssetsMPIndexModel SearchAssetsMP(AssetsMPSearchModel condition)
        {
            AssetsMPIndexModel result = new AssetsMPIndexModel()
            {
                condition = condition,
                Items = new List<AssetMPModel>()
            };
            using (var db = new AS_LandBankEntities())
            {
                var query = from m in db.AS_Assets_Build_MP
                            select m;

                if (!string.IsNullOrEmpty(condition.Search_Asset_Category_Code))
                {
                    query = query.Where(m=>m.Asset_Category_Code.Contains(condition.Search_Asset_Category_Code));

                }
                if (!string.IsNullOrEmpty(condition.Search_Parent_Asset_Category_Code))
                {
                    var array = condition.Search_Parent_Asset_Category_Code.Split('.');
                    if (array != null)
                    {
                        string str = string.Empty;
                        for (int i = 0; i < ((array.Length > 2) ? 2 : array.Length); i++)
                        {
                            str += array[i];
                        }
                        query = query.Where(m => m.Parent_Asset_Number.StartsWith(str));
                    }
                }
                if (condition.UseAssetNumber != null)
                {
                    if (condition.UseAssetNumber.Count() > 0)
                    {
                        query = from p in query
                                where !(condition.UseAssetNumber).Contains(p.Asset_Number)
                                select p;
                    }
                }
                if (query != null && query.Count() > 0)
                {
                    //分頁
                    //if (condition.Page != null)
                    //{
                    //    query = query.OrderByDescending(m => m.Create_Time).Skip((condition.Page.Value - 1) * Convert.ToInt32(condition.PageSize)).Take(Convert.ToInt32(condition.PageSize));
                    //}
                    result.Items = query.ToList().Select(o => BuildGeneral.ToAssetMPModel(o));
                    result.TotalAmount = query.Count();
                }
            }
            return result;
        }

        public AssetHandleResult CreateBuild(BuildModel Build)
        {
            BuildModel CreateBuild = new BuildModel()
            {
                TRXHeader = Build.TRXHeader,
                Retire = Build.Retire,
                Category = Build.Category,
                Year = Build.Year,
                Header = new BuildHeaderModel() //之後要移除
            };
            CreateBuild.Assets = Build.Assets.Select(o => GetAssetDetailByAssetNumber(o.Asset_Number));

            return Create_Asset(CreateBuild);
        }

        public AssetModel GetAssetDetailByAssetNumber(string AssetNumber)
        {
            AssetModel asset = null;

            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_Build AS_Build = (from a in db.AS_Assets_Build
                                            where a.Asset_Number == AssetNumber
                                            select a).FirstOrDefault();
                if (AS_Build != null)
                {
                    asset = BuildGeneral.ToAssetModel(AS_Build);

                    var buildlands = from l in db.AS_Assets_Build_Land
                                     where l.Asset_Build_ID == AS_Build.ID
                                     select new BuildLandModel()
                                     {
                                         ID = l.Asset_Land_ID
                                     };
                    asset.BuildLands = buildlands.ToList();

                    var builddetails = from bd in db.AS_Assets_Build_Detail
                                       where bd.Assets_Build_ID == AS_Build.ID
                                       select new AssetDetailModel()
                                       {
                                           Asset_Type = bd.Asset_Type,
                                           Area_Size = bd.Area_Size,
                                           Authorized_Area = bd.Authorized_Area,
                                           Authorized_Scope_Denominx = bd.Authorized_Scope_Denominx,
                                           Authorized_Scope_Moldcule = bd.Authorized_Scope_Moldcule,
                                           Floor_Uses = bd.Floor_Uses
                                       };
                    asset.BuildDetails = builddetails.ToList();

                    //取得資產分類名稱
                    if (!string.IsNullOrEmpty(asset.Asset_Category_Code))
                    {
                        if (db.AS_Asset_Category.Any(m => m.Asset_Category_Code == asset.Asset_Category_Code))
                        {
                            asset.Asset_Category_Code_Name =
                                db.AS_Asset_Category.Where(m => m.Asset_Category_Code == asset.Asset_Category_Code).SingleOrDefault().Asset_Category_Name;
                        }
                    }
                }
            }

            return asset;
        }

        public AssetHandleResult CreateBuildMP(BuildMPModel BuildMP)
        {
            BuildMPModel CreateBuildMP = new BuildMPModel()
            {
                TRXHeader = BuildMP.TRXHeader,
                Header = new BuildHeaderModel(), //之後要移除
                Retire = BuildMP.Retire,
                Category = BuildMP.Category,
                Year = BuildMP.Year
                //Header = BuildMP.Header
            };
            CreateBuildMP.Assets = BuildMP.Assets.Select(o => GetAssetMPDetailByAssetNumber(o.Asset_Number));

            return Create_AssetMP(CreateBuildMP);
        }

        private AssetMPModel GetAssetMPDetailByAssetNumber(string AssetNumber)
        {
            AssetMPModel asset = null;

            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_Build_MP AS_Build_MP = (from a in db.AS_Assets_Build_MP
                                                  where a.Asset_Number == AssetNumber
                                                  select a).FirstOrDefault();
                if (AS_Build_MP != null)
                {
                    asset = BuildGeneral.ToAssetMPModel(AS_Build_MP);
                }
            }

            return asset;
        }

        public bool UpdateFlowStatus(string TRX_Header_ID, string FlowStatus)
        {
            bool Result = true;

            using (var db = new AS_LandBankEntities())
            {
                AS_TRX_Headers Header = db.AS_TRX_Headers.First(o => o.Form_Number == TRX_Header_ID);
                Header.Transaction_Status = FlowStatus;
                Header.Last_UpDatetimed_Time = DateTime.Now;
                db.Entry(Header).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }

            //using (var db = new AS_LandBankEntities())
            //{
            //    AS_Assets_Build_Header Header = db.AS_Assets_Build_Header.First(o => o.TRX_Header_ID == TRX_Header_ID);
            //    Header.Transaction_Status = FlowStatus;
            //    Header.Last_Updated_Time = DateTime.Now;
            //    db.Entry(Header).State = System.Data.Entity.EntityState.Modified;
            //    db.SaveChanges();
            //}

            return Result;
        }

        public AssetsMPIndexModel SearchUserDataByBranch(string BranchCode)
        {
            AssetsMPIndexModel result = new AssetsMPIndexModel()
            {
                BuildUserModel = new List<BuildUserModel>()
            };
            
            string Branch = string.Empty;
            string Dept = string.Empty;
            
            if(string.IsNullOrEmpty(BranchCode))
            {
                return result;
            }

            var Array = BranchCode.Split('.');
            if (Array.Length > 0)
                Branch = Array[0];
            if (Array.Length > 1)
                Dept = Array[1];

            using (var db = new AS_LandBankEntities())
            {
                if(db.AS_Users.Any(m => m.Branch_Code == Dept))
                {
                    var source = db.AS_Users.Where(m => m.Branch_Code == Dept).ToList();
                    result.BuildUserModel = source.Select(m => BuildGeneral.ToBuildUserModel(m)).ToList();
                }
               
            }

            return result;
        }

        public string CreateAssetNumber(string type1, string type2)
        {
            string AssetNumber = "";
            using (var db = new AS_LandBankEntities())
            {
                AssetNumber = (from build in db.AS_Assets_Build select new { Asset_Number = build.Asset_Number }).Concat(from buildr in db.AS_Assets_Build_Record select new { Asset_Number = buildr.Asset_Number })
                              .Where(i => i.Asset_Number.StartsWith(type1 + type2)).Max(i => i.Asset_Number);
            }
            if (AssetNumber != null)
            {
                //30000310000001
                int Seq = Convert.ToInt32(AssetNumber.Substring(8)) + 1;
                AssetNumber = type1 + type2 + Seq.ToString().PadLeft(7, '0');
            }
            else
            {
                AssetNumber = type1 + type2 + "0000001";
            }
            return AssetNumber;
        }

        private string[] DetachCategoryCode(string code)
        {
            if(string.IsNullOrEmpty(code))
            {
                return null;
            }
            var source = code.Split('.');
            if (source.Length < 2)
            {
                return null;
            }
            else
            {
                string[] result = new string[2];
                for (int i = 0; i < 2; i++)
                {
                    result[i] = source[i];
                }
                return result;
            }
        }

    }
}
