﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Build.BuildModels;
using Library.Servant.Common;
using Library.Servant.Servant.MPBuild.BuildModels;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.Build
{
    public class RemoteBuildServant : IBuildServant
    {
        public bool HeaderAndAsset_Del(string id, string Type="")
        {
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                new SearchModel() { Form_Number = id ,Type = Type},
                Config.ServantDomain + "/BuildServant/HeaderAndAsset_Del"
            );
            return Tools.ExceptionConvert(result);

            //voidresult
            //var result = ProtocolService.EncryptionPost<SearchModel, VoidResult>
            //(
            //    new SearchModel() { Form_Number = id },
            //    Config.ServantDomain + "/BuildServant/HeaderAndAsset_Del"
            //);
            //return Tools.ExceptionConvert(result);
        }
        public bool HeaderAndMPAsset_Del(string id, string Type = "")
        {
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                new SearchModel() { Form_Number = id, Type = Type },
                Config.ServantDomain + "/BuildServant/HeaderAndMPAsset_Del"
            );
            return Tools.ExceptionConvert(result);
        }
        public StringResult Asset_Del(int id, string Type = "")
        {
           var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
           (
               new SearchModel() { id = id, Type = Type },
               Config.ServantDomain + "/BuildServant/Asset_Del"
           );

            return Tools.ExceptionConvert(result);
        }
        public StringResult MPAsset_Del(int id, string Type = "")
        {
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                new SearchModel() { id = id, Type = Type },
                Config.ServantDomain + "/BuildServant/MPAsset_Del"
            );

            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult CreateBuild(BuildModel Build)
        {
            var result = ProtocolService.EncryptionPost<BuildModel, AssetHandleResult>
            (
                Build,
                Config.ServantDomain + "/BuildServant/CreateBuild"
            );
            return Tools.ExceptionConvert(result);
        }

        public AssetHandleResult CreateBuildMP(BuildMPModel BuildMP)
        {
            var result = ProtocolService.EncryptionPost<BuildMPModel, AssetHandleResult>
            (
                BuildMP,
                Config.ServantDomain + "/BuildServant/CreateBuildMP"
            );
            return Tools.ExceptionConvert(result);
        }

        public AssetHandleResult CreateToBuild(string TRXHeaderID)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, AssetHandleResult>
            (
                new SearchModel() { TRXHeaderID = TRXHeaderID },
                Config.ServantDomain + "/BuildServant/CreateToBuild"
            );
            return Tools.ExceptionConvert(result);
        }

        public AssetHandleResult CreateToBuildMP(string TRXHeaderID)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, AssetHandleResult>
            (
                new SearchModel() { TRXHeaderID = TRXHeaderID },
                Config.ServantDomain + "/BuildServant/CreateToBuildMP"
            );
            return Tools.ExceptionConvert(result);
        }

        public string CreateBuildFormNumber(string Type)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                new SearchModel() { Type = Type },
                Config.ServantDomain + "/BuildServant/CreateBuildFormNumber"
            );
            return Tools.ExceptionConvert(result).StrResult;
        }
        public string CreateTRXID(string Type)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                new SearchModel() { Type = Type },
                Config.ServantDomain + "/BuildServant/CreateTRXID"
            );
            return Tools.ExceptionConvert(result).StrResult;
        }

        public AssetHandleResult Create_Asset(BuildModel model)
        {
            var result = ProtocolService.EncryptionPost<BuildModel, AssetHandleResult>
            (
                model,
                Config.ServantDomain + "/BuildServant/Create_Asset"
            );
            return Tools.ExceptionConvert(result);
        }

        public AssetHandleResult Create_AssetMP(BuildMPModel model)
        {
            var result = ProtocolService.EncryptionPost<BuildMPModel, AssetHandleResult>
            (
                model,
                Config.ServantDomain + "/BuildServant/Create_AssetMP"
            );
            return Tools.ExceptionConvert(result);
        }

        public AssetModel GetAssetDetail(int id, out BuildTRXHeaderModel header) //---
        {
            header = new BuildTRXHeaderModel();
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                new SearchModel() { id = id },
                Config.ServantDomain + "/BuildServant/GetAssetDetail"
            );
            if (result != null)
            {
                header = Tools.ExceptionConvert(result).BuildTRXHeaderModel;
            }
            return Tools.ExceptionConvert(result).AssetModel;
        }

        public AssetModel GetAssetDetailByAssetNumber(string AssetNumber)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, AssetModel>
            (
                new SearchModel() { AssetNumber = AssetNumber },
                Config.ServantDomain + "/BuildServant/GetAssetDetailByAssetNumber"
            );
            return Tools.ExceptionConvert(result);
        }

        public AssetMPModel GetAssetMPDetail(int id, out BuildTRXHeaderModel header) //---
        {
            header = new BuildTRXHeaderModel();
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                new SearchModel() { id = id },
                Config.ServantDomain + "/BuildServant/GetAssetMPDetail"
            );
            if (result != null)
            {
                header = Tools.ExceptionConvert(result).BuildTRXHeaderModel;
            }
            return Tools.ExceptionConvert(result).AssetMPModel;
        }

        public BuildModel GetDetail(string id)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, BuildModel>
            (
                new SearchModel() { idStr = id },
                Config.ServantDomain + "/BuildServant/GetDetail"
            );
            return Tools.ExceptionConvert(result);
        }

        public bool GetDocID(string DocCode, out int DocID) //---
        {
            DocID = 0;
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                new SearchModel() { DocCode = DocCode },
                Config.ServantDomain + "/BuildServant/GetDocID"
            );
            if (result != null)
            {
                DocID = Convert.ToInt32(Tools.ExceptionConvert(result).StrResult);
            }
            return Tools.ExceptionConvert(result).IsSuccess;
        }

        public BuildMPModel GetMPDetail(string id)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, BuildMPModel>
            (
                new SearchModel() { idStr = id },
                Config.ServantDomain + "/BuildServant/GetMPDetail"
            );
            return Tools.ExceptionConvert(result);
        }

        public IndexModel Index(SearchModel condition)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, IndexModel>
            (
                condition,
                Config.ServantDomain + "/BuildServant/Index"
            );
            return Tools.ExceptionConvert(result);
        }

        public AssetsIndexModel SearchAssets(AssetsSearchModel condition)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, AssetsIndexModel>
            (
                condition,
                Config.ServantDomain + "/BuildServant/SearchAssets"
            );
            return Tools.ExceptionConvert(result);
        }

        public AssetsMPIndexModel SearchAssetsMP(AssetsMPSearchModel condition)
        {
            var result = ProtocolService.EncryptionPost<AssetsMPSearchModel, AssetsMPIndexModel>
            (
                condition,
                Config.ServantDomain + "/BuildServant/SearchAssetsMP"
            );
            return Tools.ExceptionConvert(result);
        }

        public bool UpdateFlowStatus(string TRX_Header_ID, string FlowStatus)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                new SearchModel() { TRX_Header_ID = TRX_Header_ID, FlowStatus = FlowStatus },
                Config.ServantDomain + "/BuildServant/UpdateFlowStatus"
            );
            return Tools.ExceptionConvert(result).IsSuccess;
        }

        public AssetHandleResult UpdateToBuild(string TRXHeaderID)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, AssetHandleResult>
            (
                new SearchModel() { TRXHeaderID = TRXHeaderID },
                Config.ServantDomain + "/BuildServant/UpdateToBuild"
            );
            return Tools.ExceptionConvert(result);
        }

        public AssetHandleResult UpdateToBuildMP(string TRXHeaderID)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, AssetHandleResult>
            (
                new SearchModel() { TRXHeaderID = TRXHeaderID },
                Config.ServantDomain + "/BuildServant/UpdateToBuildMP"
            );
            return Tools.ExceptionConvert(result);
        }

        public AssetsMPIndexModel SearchUserDataByBranch(string BranchCode)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, AssetsMPIndexModel>
            (
                new SearchModel() { BranchCode = BranchCode },
                Config.ServantDomain + "/BuildServant/SearchUserDataByBranch"
            );
            return Tools.ExceptionConvert(result);
        }
    }
}
