﻿using System.Linq;
using System.Collections.Generic;
using LandBankEntity.ActiveRecords;
using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servant.ApprovalMember;
using Library.Servant.Servant.ApprovalTodo.Constants;
using Library.Servant.Servant.ApprovalMember.Models;

namespace Library.Servant.Servant.ApprovalTodo.Methods
{

    public class RejectMethod
    {
        /// <summary>
        /// 執行條件
        /// </summary>
        private readonly int FlowID;
        private readonly List<decimal> PermitState;

        private readonly DetailsResult User;
        private readonly List<int> UserRoles;
                        
        public RejectMethod(int userID, int flowID)
        {
            FlowID = flowID;
            PermitState = new List<decimal> { FlowStateServant.New, FlowStateServant.Approval, FlowStateServant.Reassign  };

            User = LocalApprovalMemberServant.Details(userID);
            UserRoles = User.ApprovalRoles.Select(role => role.ID).ToList();
        }

        public void Call()
        {
            using (AS_LandBankEntities db = new AS_LandBankEntities())
            {
                if ( Reject(db) )
                    RegisterListener.Instance(FlowID).OnReject(db);

                db.SaveChanges();
            }
        }

        private bool Reject(AS_LandBankEntities db)
        {
            return FlowOpenRecord.UpdateT(

                db,
                entity =>
                    FlowID == entity.ID &&
                    entity.Branch_Code == User.BranchCode &&
                    PermitState.Contains(entity.Flow_Status) &&
                    UserRoles.Contains(entity.Step_Role_ID.Value),

                entity => entity.AddRejectLog(User.UserID)

            ) == 1;
        }
    }
}
