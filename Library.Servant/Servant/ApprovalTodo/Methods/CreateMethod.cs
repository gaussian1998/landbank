﻿using Library.Entity.Edmx;
using System.Linq;

namespace Library.Servant.Servant.ApprovalTodo.Methods
{
    public class CreateMethod
    {
        private readonly int UserID;
        private readonly string UserBranchCode;
        private readonly int Newest_Agent_Flow_ID;

        public CreateMethod(int userID, int docID)
        {
            UserID = userID;
            UserBranchCode = AS_Users_Records.First(user => user.ID == userID).Branch_Code;
            Newest_Agent_Flow_ID = AS_Agent_Flow_Records.First(m => m.Doc_Name_ID == docID && m.Version == m.AS_Doc_Name.AS_Agent_Flow.Max(e => e.Version)).ID;
        }

        public int Call(string FormNo, string EventListener)
        {
            return AS_Flow_Open_Records.Create(entity =>
            {
                entity.Form_No = FormNo;
                entity.Agent_Flow_ID = Newest_Agent_Flow_ID;
                entity.Apply_User_ID = UserID;
                entity.Event_Listener = EventListener;
                entity.AddCreatedLog();
                entity.Branch_Code = UserBranchCode;
            }).ID;
        }
    }
}
