﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalMember;
using Library.Servant.Servant.ApprovalTodo.Constants;

namespace Library.Servant.Servant.ApprovalTodo.Methods
{
    public static class TodoUtility
    {
        public static List<int> UserRoles(int userID)
        {
            return AS_Users_Records.First(
                m => m.ID == userID,
                entity => entity.AS_Users_FlowRole.Select(e => e.FlowRoleID).ToList()
            );
        }

        public static void AddCreatedLog(this AS_Flow_Open entity)
        {
            entity.Step_No = 0;
            entity.Flow_Status = FlowStateServant.Draft;
            entity.Issue_Time = DateTime.Now;
            entity.Carry = string.Empty;
            entity.Result = 5;

            entity.LogSnapshop();
        }

        public static void AddWithdrawLog(this AS_Flow_Open entity, int userID)
        {
            entity.Step_No = 0;
            entity.Flow_Status = FlowStateServant.Withdraw;
            entity.Current_User_ID = userID;
            entity.Step_Role_ID = null;
            entity.Step_User_ID = null;
            entity.Result = 5;

            entity.LogSnapshop();
        }

        public static void AddRejectLog(this AS_Flow_Open entity, int userID)
        {
            entity.Step_No = 0;
            entity.Flow_Status = FlowStateServant.Reject;
            entity.Current_User_ID = userID;
            entity.Step_Role_ID = null;
            entity.Step_User_ID = null;
            entity.Result = 4;

            entity.LogSnapshop();
        }

        public static void AddReassignLog(this AS_Flow_Open entity, int userID, int ReassignUserID)
        {
            entity.Flow_Status = FlowStateServant.Reassign;
            entity.Current_User_ID = userID;
            entity.Step_Role_ID = null;
            entity.Step_User_ID = ReassignUserID;
            entity.Result = 1;

            entity.LogSnapshop();
        }

        public static void AddRevokeLog(this AS_Flow_Open entity, int userID)
        {
            entity.Flow_Status = FlowStateServant.Revoke;
            entity.Current_User_ID = userID;
            entity.Step_Role_ID = null;
            entity.Step_User_ID = null;
            entity.Result = 3;

            entity.LogSnapshop();
        }

        public static void AddApprovalLog(this AS_Flow_Open entity, AS_Flow_Template_Flow_Role next_flow_role, int userID)
        {
            entity.Flow_Status = next_flow_role == null ? FlowStateServant.Pass : FlowStateServant.Approval;
            entity.Current_User_ID = userID;
            entity.Step_Role_ID = next_flow_role == null ? new Nullable<int>() : next_flow_role.Flow_Role_ID;
            entity.Step_User_ID = null;
            entity.Result = 2;

            entity.LogSnapshop();
        }

        public static void AddStartLog(this AS_Flow_Open entity, string Carry)
        {
            entity.Flow_Status = FlowStateServant.New;
            entity.Current_User_ID = null;
            entity.Step_Role_ID = AS_Agent_Flow_Records.First(m => m.ID == entity.Agent_Flow_ID, m => m.AS_Flow_Template.AS_Flow_Template_Flow_Role.FirstOrDefault().Flow_Role_ID);
            entity.Step_User_ID = null;
            entity.Carry = Carry;
            entity.Result = 1;

            entity.LogSnapshop();
        }

        private static void LogSnapshop(this AS_Flow_Open entity)
        {
            entity.Last_Updated_Time = DateTime.Now;
            entity.AS_Flow_Open_Log.Add(new AS_Flow_Open_Log
            {
                FlowStatus = entity.Flow_Status,
                CurrentUserID = entity.Current_User_ID,
                StepRoleID = entity.Step_Role_ID,
                StepUserID = entity.Step_User_ID,
                IssueTime = entity.Issue_Time,
                LastUpdatedTime = DateTime.Now,
                HistoryCarry = entity.Carry,
                Result = entity.Result
            });
        }
    }
}
