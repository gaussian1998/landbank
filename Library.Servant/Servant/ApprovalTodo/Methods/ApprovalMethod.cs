﻿using System.Linq;
using System.Collections.Generic;
using Library.Entity.Edmx;
using LandBankEntity.ActiveRecords;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servant.ApprovalMember;
using Library.Servant.Servant.ApprovalTodo.Constants;
using Library.Servant.Servant.ApprovalMember.Models;
using System.Linq.Expressions;
using System;
using Library.Common.Utility;

namespace Library.Servant.Servant.ApprovalTodo.Methods
{
    public class ApprovalMethod
    {
        private bool IsFinal;
        /// <summary>
        /// 執行條件
        /// </summary>
        private readonly int FlowID;
        private readonly List<decimal> PermitState;

        private readonly DetailsResult User;
        private readonly List<int> UserRoles;
        
        public ApprovalMethod(int userID, int flowID=-1)
        {
            IsFinal = false;

            FlowID = flowID;
            PermitState = new List<decimal> { FlowStateServant.New, FlowStateServant.Approval, FlowStateServant.Reassign };

            User = LocalApprovalMemberServant.Details(userID);
            UserRoles = User.ApprovalRoles.Select(role => role.ID).ToList();
        }

        public void Call()
        {
            using (AS_LandBankEntities db = new AS_LandBankEntities())
            {
                if (Approval(db))
                    RegisterListener.Instance(FlowID).OnApproval(db);

                if (IsFinal)
                    RegisterListener.Instance(FlowID).OnFinish(db);

                db.SaveChanges();
            }
        }

        public static Expression<Func<AS_Flow_Open, bool>> IndexExpression(int userID)
        {
            var approval = new ApprovalMethod(userID);

            return approval.IndexExpression();
        }

        private Expression<Func<AS_Flow_Open, bool>> IndexExpression()
        {
            return ExpressionMaker.Make<AS_Flow_Open, bool>( 
                
                entity => 
                    User.BranchCode == entity.Branch_Code &&
                    PermitState.Contains(entity.Flow_Status) &&
                    (UserRoles.Contains(entity.Step_Role_ID.Value) || entity.Step_User_ID == User.UserID)
            );
        }
        
        private bool Approval(AS_LandBankEntities db)
        {
            return FlowOpenRecord.UpdateT(

                db,

                entity =>
                    FlowID == entity.ID &&
                    User.BranchCode == entity.Branch_Code &&
                    PermitState.Contains( entity.Flow_Status ) &&
                    (UserRoles.Contains(entity.Step_Role_ID.Value) || entity.Step_User_ID == User.UserID),

                entity => PrepareLog(entity)

            ) == 1;
        }

        private static AS_Flow_Template_Flow_Role NextFlowRole(AS_Flow_Open entity)
        {
            entity.Step_No += 1;
            return entity.AS_Agent_Flow.AS_Flow_Template.AS_Flow_Template_Flow_Role.FirstOrDefault(m => m.Step_No == (entity.Step_No + 1));
        }

        private void PrepareLog(AS_Flow_Open entity)
        {
            AS_Flow_Template_Flow_Role next_flow_role = NextFlowRole(entity);
            IsFinal = (next_flow_role == null);

            entity.AddApprovalLog(next_flow_role, User.UserID);
        }
    }
}
