﻿using LandBankEntity.ActiveRecords;
using Library.Common.Utility;
using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servant.ApprovalTodo.Constants;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Library.Servant.Servant.ApprovalTodo.Methods
{
    public class WithdrawMethod
    {
        /// <summary>
        /// 執行條件
        /// </summary>
        private readonly int FlowID;
        private readonly int UserID;
        private readonly List<decimal> PermitState;

        public WithdrawMethod(int userID, int flowID=-1)
        {
            this.FlowID = flowID;
            this.UserID = userID;
            this.PermitState = new List<decimal> { FlowStateServant.New, FlowStateServant.Approval, FlowStateServant.Reassign };
        }

        public void Call()
        {
            using (AS_LandBankEntities db = new AS_LandBankEntities())
            {
                if ( Withdraw(db) )
                    RegisterListener.Instance(FlowID).OnWithdraw(db);

                db.SaveChanges();
            }
        }

        public static Expression<Func<AS_Flow_Open, bool>> IndexExpression(int userID)
        {
            var withdraw = new WithdrawMethod(userID);

            return withdraw.IndexExpression();
        }

        private Expression<Func<AS_Flow_Open, bool>> IndexExpression()
        {
            return ExpressionMaker.Make<AS_Flow_Open, bool>(

                entity =>

                  UserID == entity.Apply_User_ID &&
                    PermitState.Contains(entity.Flow_Status)
            );
        }

        private bool Withdraw(AS_LandBankEntities db)
        {
            return FlowOpenRecord.UpdateT(

                db,
                entity =>
                    FlowID == entity.ID &&
                    UserID == entity.Apply_User_ID &&
                    PermitState.Contains(entity.Flow_Status),

                entity => entity.AddWithdrawLog(UserID)

            ) == 1;
        }
    }
}
