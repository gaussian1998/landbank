﻿using System;
using System.Linq.Expressions;
using System.Linq;
using System.Collections.Generic;
using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalMember;
using Library.Servant.Servant.ApprovalTodo.Constants;
using Library.Servant.Servant.ApprovalMember.Models;
using Library.Common.Utility;

namespace Library.Servant.Servant.ApprovalTodo.Methods
{
    public class ReassignMethod
    {
        /// <summary>
        /// 執行條件
        /// </summary>
        private readonly int FlowID;
        private readonly List<decimal> PermitState;

        private readonly DetailsResult User;
        private readonly List<int> UserRoles;
        
        public ReassignMethod(int userID, int flowID=-1)
        {
            FlowID = flowID;
            PermitState = new List<decimal> { FlowStateServant.New, FlowStateServant.Approval  };

            User = LocalApprovalMemberServant.Details(userID);
            UserRoles = User.ApprovalRoles.Select(role => role.ID).ToList();
        }

        public void Call(int ReassignUserID)
        {
            AS_Flow_Open_Records.Update(

                entity =>
                   FlowID == entity.ID &&
                   entity.Branch_Code == User.BranchCode &&
                   (UserRoles.Contains(entity.Step_Role_ID.Value) || entity.Step_User_ID == User.UserID),

                entity => entity.AddReassignLog( User.UserID, ReassignUserID)
            );
        }

        public static Expression<Func<AS_Flow_Open, bool>> IndexExpression(int userID)
        {
            var reassign = new ReassignMethod(userID);

            return reassign.IndexExpression();
        }

        private Expression<Func<AS_Flow_Open, bool>> IndexExpression()
        {
            return ExpressionMaker.Make<AS_Flow_Open, bool>(

                entity =>

                   entity.Branch_Code == User.BranchCode &&
                   (UserRoles.Contains(entity.Step_Role_ID.Value) || entity.Step_User_ID == User.UserID)
            );
        }
    }
}
