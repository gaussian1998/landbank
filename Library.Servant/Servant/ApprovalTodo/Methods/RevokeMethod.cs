﻿using System.Linq;
using System.Collections.Generic;
using LandBankEntity.ActiveRecords;
using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servant.ApprovalMember;
using Library.Servant.Servant.ApprovalTodo.Constants;
using Library.Servant.Servant.ApprovalMember.Models;
using System.Linq.Expressions;
using System;
using Library.Common.Utility;

namespace Library.Servant.Servant.ApprovalTodo.Methods
{
    public class RevokeMethod
    {
        /// <summary>
        /// 執行條件
        /// </summary>
        private readonly int FlowID;
        private readonly List<decimal> ApprovalState;
        private readonly List<decimal> DraftState;

        private readonly DetailsResult User;
        private readonly List<int> UserRoles;
        
        public RevokeMethod(int userID, int flowID=-1)
        {
            FlowID = flowID;
            ApprovalState = new List<decimal> { FlowStateServant.New, FlowStateServant.Approval, FlowStateServant.Reassign  };
            DraftState = new List<decimal> { FlowStateServant.Draft, FlowStateServant.Reject, FlowStateServant.Withdraw };

            User = LocalApprovalMemberServant.Details(userID);
            UserRoles = User.ApprovalRoles.Select(role => role.ID).ToList();
        }

        public void Call()
        {
            using (AS_LandBankEntities db = new AS_LandBankEntities())
            {
                if (Revoke(db))
                    RegisterListener.Instance(FlowID).OnRevoke(db);

                db.SaveChanges();
            }
        }

        public static Expression<Func<AS_Flow_Open, bool>> IndexExpression(int userID)
        {
            var revoke = new RevokeMethod(userID);

            return revoke.IndexExpression();
        }

        private Expression<Func<AS_Flow_Open, bool>> IndexExpression()
        {
            return ExpressionMaker.Make<AS_Flow_Open, bool>(

                entity =>

                    entity.Branch_Code == User.BranchCode &&
                    
                    ( (entity.Apply_User_ID == User.UserID && DraftState.Contains(entity.Flow_Status)) ||  
                    ( (UserRoles.Contains(entity.Step_Role_ID.Value) || entity.Step_User_ID == User.UserID) && ApprovalState.Contains(entity.Flow_Status) ) )
            );
        }

        private bool Revoke(AS_LandBankEntities db)
        {
            return FlowOpenRecord.UpdateT(

                db,
                entity =>
                    entity.ID == FlowID &&
                    entity.Branch_Code == User.BranchCode &&
                    ((entity.Apply_User_ID == User.UserID && DraftState.Contains(entity.Flow_Status)) ||
                    ((UserRoles.Contains(entity.Step_Role_ID.Value) || entity.Step_User_ID == User.UserID) && ApprovalState.Contains(entity.Flow_Status))),

                entity => entity.AddRevokeLog(User.UserID)

            ) == 1;
        }
    }
}
