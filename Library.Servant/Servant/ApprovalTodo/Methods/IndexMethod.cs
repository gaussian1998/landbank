﻿using Library.Servant.Servant.ApprovalTodo.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Servant.Servant.ApprovalTodo.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.CodeTable;
using Library.Entity.Edmx;
using Library.Servant.Servant.Common.ModelExtensions;
using Library.Common.Extension;


namespace Library.Servant.Servant.ApprovalTodo.Methods
{
    public class IndexMethod
    {
        private readonly SearchModel searchCondition;
        private readonly List<decimal> PermitState;

        public IndexMethod(SearchModel searchCondition)
        {
            this.searchCondition = searchCondition;

            this.PermitState = new List<decimal> { FlowStateServant.Approval, FlowStateServant.Reassign, FlowStateServant.New };
        }

        public IndexResult Call(Expression<Func<AS_Flow_Open, bool>> cadidateExpress)
        {
            var cadidate = Cadidate(cadidateExpress);
            var selected = cadidate.Where(searchCondition.SelectedOptions.Expression());

            return new IndexResult
            {
                Options = cadidate.Options(PermitState),
                Page = selected.Page(searchCondition.Page, searchCondition.PageSize, m => m.ID)
            };
        }

        private List<FlowOpenIndexItem> Cadidate(Expression<Func<AS_Flow_Open, bool>> cadidateExpress)
        {
            var flow_dictionary = LocalCodeTableServant.FlowRoleIdDictionary();
            return AS_Flow_Open_Records.FindModify<FlowOpenIndexItem>(

               cadidateExpress,
               result => {

                    result.FlowRoleIdDictionary = flow_dictionary;
                    return result;
               });
        }
    }
}
