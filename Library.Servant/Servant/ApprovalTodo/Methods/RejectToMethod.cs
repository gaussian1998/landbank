﻿using System.Linq;
using System.Collections.Generic;
using Library.Entity.Edmx;
using LandBankEntity.ActiveRecords;
using Library.Servant.Servant.ApprovalMember;
using Library.Servant.Servant.ApprovalTodo.Constants;
using Library.Servant.Servant.ApprovalMember.Models;

namespace Library.Servant.Servant.ApprovalTodo.Methods
{
    public class RejectToMethod
    {
        /// <summary>
        /// 執行條件
        /// </summary>
        private readonly int FlowID;
        private readonly List<decimal> PermitState;

        private readonly DetailsResult User;
        private readonly List<int> UserRoles;

        public RejectToMethod(int userID, int flowID)
        {
            FlowID = flowID;
            PermitState = new List<decimal> { FlowStateServant.Approval, FlowStateServant.Reassign };

            User = LocalApprovalMemberServant.Details(userID);
            UserRoles = User.ApprovalRoles.Select(role => role.ID).ToList();
        }

        public void Call(int RejectStepNo)
        {
            FlowOpenRecord.Update(

                entity =>
                    FlowID == entity.ID &&
                    User.BranchCode == entity.Branch_Code &&
                    PermitState.Contains(entity.Flow_Status) &&
                    RejectStepNo <= entity.Step_No &&
                    (UserRoles.Contains(entity.Step_Role_ID.Value) || entity.Step_User_ID == User.UserID),

                entity => PrepareLog(entity, RejectStepNo)
            );
        }

        private static AS_Flow_Template_Flow_Role NextFlowRole(AS_Flow_Open entity, int RejectStepNo)
        {
            entity.Step_No = RejectStepNo-1;
            return entity.AS_Agent_Flow.AS_Flow_Template.AS_Flow_Template_Flow_Role.FirstOrDefault(m => m.Step_No == RejectStepNo);
        }

        private void PrepareLog(AS_Flow_Open entity, int RejectStepNo)
        {
            AS_Flow_Template_Flow_Role next_flow_role = NextFlowRole(entity, RejectStepNo);

            entity.AddApprovalLog(next_flow_role, User.UserID);
        }
    }
}
