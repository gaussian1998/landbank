﻿using LandBankEntity.ActiveRecords;
using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalTodo.Constants;
using System.Collections.Generic;
using System.Linq;

namespace Library.Servant.Servant.ApprovalTodo.Methods
{
    public class StartMethod
    {
        private readonly string Carry;
        /// <summary>
        /// 執行條件
        /// </summary>
        private readonly string FormNo;
        private readonly int UserID;
        private readonly List<decimal> PermitState;

        public StartMethod(int UserID, string FormNo, string Carry)
        {
            this.FormNo = FormNo;
            this.Carry = Carry;
            this.UserID = UserID;
            this.PermitState = new List<decimal> { FlowStateServant.Draft, FlowStateServant.Reject, FlowStateServant.Withdraw };
        }

        public bool Call()
        {
            using (AS_LandBankEntities db = new AS_LandBankEntities())
            {
                bool result = Call(db);
                db.SaveChanges();
                return result;
            }
        }

        public bool Call(AS_LandBankEntities db)
        {
            return FlowOpenRecord.UpdateT(

                db,
                entity =>
                    entity.Form_No == FormNo &&
                    entity.Apply_User_ID == UserID &&
                    PermitState.Contains(entity.Flow_Status),

                entity => entity.AddStartLog(Carry)

             ) == 1;
        }
    }
}
