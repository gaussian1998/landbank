﻿using System.Collections.Generic;

namespace Library.Servant.Servant.ApprovalTodo.Constants
{
    public static class FlowStateServant
    {
        public const decimal Draft = 0;               // 各模組產生
        public const decimal New = 1;               // 呼叫Start,進入簽核,尚未有人簽過
        public const decimal Approval = 2;       // 簽核態,至少有一個人以上簽過
        public const decimal Pass = 3;               // 放行,沒辦法再改變狀態(進去出不來)
        public const decimal Revoke = 4;         // 廢棄,沒辦法再改變狀態(進去出不來)
        public const decimal Reject = 5;           // 退回原經辦
        public const decimal Withdraw = 6;    // 原經辦抽回
        public const decimal Reassign = 7;     // 改派
        
        public static string Text(decimal StateCode)
        {
            return
                StateCode == Draft ? "草稿" :
                StateCode == Approval ? "簽核中" :
                StateCode == Pass ? "已核准" :
                StateCode == Revoke ? "撤件" :
                StateCode == Reject ? "退回修改" :
                StateCode == Withdraw ? "抽回" :
                StateCode == Reassign ? "改派" :
                StateCode == New ? "新增" : "不明";
        }
        public static List<decimal> All()
        {
            return new List<decimal> { Draft, Approval, Pass, Revoke, Reject, Withdraw, Reassign, New };
        }
    }
}
