﻿using Library.Servant.Communicate;

namespace Library.Servant.Servant.ApprovalTodo.Models
{
    public class RejectModel : InvasionEncryption
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public string Remark { get; set; }
    }
}
