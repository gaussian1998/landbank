﻿using Library.Servant.Communicate;

namespace Library.Servant.Servant.ApprovalTodo.Models
{
    public class RevokeModel : InvasionEncryption
    {
        public int ID { get; set; }
        public int UserID { get; set; }
    }
}
