﻿using Library.Servant.Communicate;

namespace Library.Servant.Servant.ApprovalTodo.Models
{
    public class ReassignModel : InvasionEncryption
    {
        public int UserID { get; set; }
        public int ReassignUserID { get; set; }
        public int FlowID { get; set; }
    }
}
