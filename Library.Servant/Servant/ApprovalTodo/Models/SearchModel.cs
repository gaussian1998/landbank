﻿using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.ApprovalTodo.Models
{
    public class SearchModel :  InvasionEncryption
    {
        public SearchModel()
        {
            this.UserID = UserID;
            Page = 1;
            PageSize = 1000;
            IncludeApplyUser = false;
            SelectedOptions = new FlowSelectedOptionsModel();
        }

        public int UserID { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public bool IncludeApplyUser { get; set; }
        public FlowSelectedOptionsModel SelectedOptions { get; set; }
    }
}
