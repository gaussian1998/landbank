﻿using System;
using System.Linq;
using Library.Servant.Servant.ApprovalTodo.Constants;
using Library.Servant.Servant.Common.Models.Mapper;
using Library.Entity.Edmx;
using System.Collections.Generic;
using Library.Common.Extension;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.ApprovalTodo.Models
{
    public class EditResult : FlowOpenModel
    {
        /// <summary>
        ///  自動對映
        /// </summary>
        public AS_Agent_Flow AS_Agent_Flow
        {
            set
            {
                if (value != null)
                {
                    DocName = value.AS_Doc_Name.Doc_Name;
                    AlreadyApprovalRoles =
                        value.AS_Flow_Template.AS_Flow_Template_Flow_Role.
                        Where(role => role.Step_No <= Step_No).
                        Select(role => new OptionModel<int, string> { Value = role.Step_No, Text = role.AS_Code_Table.Text }).ToList();
                }
            }
        }
        public string DocName { get; set; }
        public List<OptionModel<int,string>> AlreadyApprovalRoles { get; set; }

        /// <summary>
        ///  自動對映
        /// </summary>
        public AS_Users AS_Users
        {
            set
            {
                if (value == null)
                    return;

                ApplyUserName = value.User_Name;
                BranchName = value.Department_Name;
            }
        }
        public string BranchName { get; set; }
        public string ApplyUserName { get; set; }

        /// <summary>
        ///  自動對映
        /// </summary>
        public AS_Users AS_Users1
        {
            set
            {
                if (value == null)
                    return;

                CurrentApprovalUserName = value.User_Name;
            }
        }
        public string CurrentApprovalUserName { get; set; }


        /// <summary>
        ///  Entity 表象子
        /// </summary>
        public string FormNo
        {
            get
            {
                return Form_No;
            }
            set
            {
                Form_No = value;
            }
        }

        public DateTime DocIssueTime
        {
            get
            {
                return Issue_Time;
            }
            set
            {
                Issue_Time = value;
            }
        }
        public string DocIssueTimeDescription
        {
            get
            {
                return Issue_Time.Description();
            }
            set { }
        }

        public decimal FlowStatus
        {
            set
            {
                Flow_Status = value;
            }
            get
            {
                return Flow_Status;
            }
        }

        public DateTime? LogIssueTime
        {
            set
            {
                Last_Updated_Time = value;
            }
            get
            {
                return Last_Updated_Time;
            }
        }
        public string LogIssueDescription
        {
            get
            {
                return Last_Updated_Time.Description();
            }
            set { }
        }

        public string ApprovalStatus
        {
            get
            {
                return FlowStateServant.Text(Flow_Status);
            }
        }
    }
}
