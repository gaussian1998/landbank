﻿using Library.Common.Models;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.ApprovalTodo.Models
{
    public class IndexResult : InvasionEncryption
    {
        public FlowSearchOptionsModel Options { get; set; }
        public PageList<FlowOpenIndexItem> Page { get; set; }
    }
}
