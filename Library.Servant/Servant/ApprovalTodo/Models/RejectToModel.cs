﻿
using Library.Servant.Communicate;

namespace Library.Servant.Servant.ApprovalTodo.Models
{
    public class RejectToModel : InvasionEncryption
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int RejectToStep { get; set; }
        public string Remark { get; set; }
    }
}
