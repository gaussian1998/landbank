﻿using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalTodo.Constants;
using Library.Servant.Servant.Common.Models.Mapper;

namespace Library.Servant.Servant.ApprovalTodo.Models
{
    public class DetailsResult : FlowOpenModel
    {
        /// <summary>
        ///  自動對映
        /// </summary>
        public AS_Users AS_Users1
        {
            set
            {
                if (value == null)
                    return;

                CurrentApprovalUserName = value.User_Name;
            }
        }
        public string CurrentApprovalUserName { get; set; }

        /// <summary>
        ///  Entity 表象子
        /// </summary>
        public decimal FlowStatus
        {
            set
            {
                Flow_Status = value;
            } 
            get
            {
                return Flow_Status;
            }
        }
        public string ApprovalStatus
        {
            get
            {
                return FlowStateServant.Text(Flow_Status);
            }
        }
    }
}
