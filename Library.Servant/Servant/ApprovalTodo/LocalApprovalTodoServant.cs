﻿using Library.Entity.Edmx;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servant.ApprovalTodo.Models;
using Library.Servant.Servant.ApprovalEventListener.models;
using Library.Servant.Servant.FormNo;
using Library.Servant.Servant.ApprovalTodo.Methods;

namespace Library.Servant.Servant.ApprovalTodo
{
    public class LocalApprovalTodoServant : IApprovalTodoServant
    {
        public IndexResult IndexApproval(SearchModel model)
        {
            var method = new IndexMethod(model);

            return method.Call( ApprovalMethod.IndexExpression(model.UserID) );
        }

        public IndexResult IndexRevoke(SearchModel model)
        {
            var method = new IndexMethod(model);

            return method.Call( RevokeMethod.IndexExpression(model.UserID) );
        }

        public IndexResult IndexReassign(SearchModel model)
        {
            var method = new IndexMethod(model);

            return method.Call( ReassignMethod.IndexExpression(model.UserID) );
        }

        public IndexResult IndexWithdraw(SearchModel model)
        {
            var method = new IndexMethod(model);

            return method.Call( WithdrawMethod.IndexExpression(model.UserID) );
        }

        public EditResult Edit(int ID)
        {
            return AS_Flow_Open_Records.First<EditResult>(m => m.ID == ID);
        }

        public FormResult Form(int ID)
        {
            return RegisterListener.Instance(ID).GetForm();
        }


        public VoidResult Withdraw(WithdrawModel model)
        {
            var method = new WithdrawMethod(model.UserID, model.ID);

            method.Call();
            return new VoidResult { };
        }


        public VoidResult Revoke(RevokeModel model)
        {
            var method = new RevokeMethod(model.UserID, model.ID);

            method.Call();
            return new VoidResult { };
        }

        public VoidResult Approval(ApprovalModel model)
        {
            var method = new ApprovalMethod(model.UserID, model.ID);

            method.Call();
            return new VoidResult { };
        }

        public VoidResult Reject(RejectModel model)
        {
            var method = new RejectMethod(model.UserID, model.ID);

            method.Call();
            return new VoidResult { };
        }

        public VoidResult RejectTo(RejectToModel model)
        {
            var method = new RejectToMethod(model.UserID, model.ID);

            method.Call(model.RejectToStep);
            return new VoidResult { };
        }
        
        public VoidResult Reassign(ReassignModel model)
        {
            var method = new ReassignMethod(model.UserID, model.FlowID);

            method.Call(model.ReassignUserID);
            return new VoidResult { };
        }

        public static DetailsResult Details(string FormNo)
        {
            return AS_Flow_Open_Records.First<DetailsResult>(m => m.Form_No == FormNo);
        }

        /// <summary>
        ///  Todo : 所有Create類函式需統一成一個,
        ///  不要再呼叫這個函式了,改用下面這一個.....拜託
        /// </summary>
        public static int Create(int userID, int docID, string FormNo, string EventListener)
        {
            var method = new CreateMethod(userID, docID);

            return method.Call( FormNo, EventListener);
        }

        public static string Create(int userID, string docNo, string FormBaseName, string EventListener)
        {
            int docID = AS_Doc_Name_Records.First(m => m.Doc_No == docNo).ID;
            string formNo = FormNoServant.Generate(FormBaseName);

            Create(userID, docID, formNo, EventListener);
            return formNo;
        }

        public static bool Start(int UserID, string FormNo, string Carry = "")
        {
            var method = new StartMethod( UserID, FormNo, Carry);

            return method.Call();
        }

        public static bool StartT(AS_LandBankEntities db, int UserID, string FormNo, string Carry = "")
        {
            var method = new StartMethod(UserID, FormNo, Carry);

            return method.Call(db);
        }
    }
}
