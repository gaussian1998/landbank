﻿using Library.Servant.Servant.ApprovalEventListener.models;
using Library.Servant.Servant.ApprovalTodo.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.ApprovalTodo
{
    public class RemoteApprovalTodoServant : IApprovalTodoServant
    {
        public IndexResult IndexApproval(SearchModel model)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(

                model,
                "/ApprovalTodoServant/IndexApproval"
            );
        }

        public IndexResult IndexReassign(SearchModel model)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(

                model,
                "/ApprovalTodoServant/IndexReassign"
            );
        }

        public IndexResult IndexRevoke(SearchModel model)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(

               model,
               "/ApprovalTodoServant/IndexRevoke"
           );
        }

        public IndexResult IndexWithdraw(SearchModel model)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(

               model,
               "/ApprovalTodoServant/IndexWithdraw"
           );
        }

        public EditResult Edit(int ID)
        {
            return RemoteServant.Post<IntModel, EditResult>(

               new IntModel { Value = ID },
               "/ApprovalTodoServant/Edit"
           );
        }

        public FormResult Form(int ID)
        {
            return RemoteServant.Post<IntModel, FormResult>(

               new IntModel { Value = ID },
               "/ApprovalTodoServant/Form"
           );
        }
        
        public VoidResult Approval(ApprovalModel model)
        {
            return RemoteServant.Post<ApprovalModel, VoidResult>(

               model,
               "/ApprovalTodoServant/Approval"
           );
        }

        public VoidResult Withdraw(WithdrawModel model)
        {
            return RemoteServant.Post<WithdrawModel, VoidResult>(

               model,
               "/ApprovalTodoServant/Withdraw"
           );
        }
        
        public VoidResult Reassign(ReassignModel model)
        {
            return RemoteServant.Post<ReassignModel, VoidResult>(

               model,
               "/ApprovalTodoServant/Reassign"
           );
        }
        
        public VoidResult Revoke(RevokeModel model)
        {
            return RemoteServant.Post<RevokeModel, VoidResult>(

               model,
               "/ApprovalTodoServant/Revoke"
           );
        }

        public VoidResult Reject(RejectModel model)
        {
            return RemoteServant.Post<RejectModel, VoidResult>(

               model,
               "/ApprovalTodoServant/Reject"
           );
        }

        public VoidResult RejectTo(RejectToModel model)
        {
            return RemoteServant.Post<RejectToModel, VoidResult>(

               model,
               "/ApprovalTodoServant/RejectTo"
           );
        }
    }
}
