﻿
using Library.Servant.Servant.ApprovalEventListener.models;
using Library.Servant.Servant.ApprovalTodo.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.ApprovalTodo
{
    public interface IApprovalTodoServant
    {
        IndexResult IndexApproval(SearchModel model);
        IndexResult IndexReassign(SearchModel model);
        IndexResult IndexRevoke(SearchModel model);
        IndexResult IndexWithdraw(SearchModel model);
        EditResult Edit(int ID);
        FormResult Form(int ID);
        VoidResult Withdraw(WithdrawModel model);
        VoidResult Approval(ApprovalModel model);
        VoidResult Reassign(ReassignModel model);
        VoidResult Revoke(RevokeModel model);
        VoidResult Reject(RejectModel model);
        VoidResult RejectTo(RejectToModel model);
    }
}
