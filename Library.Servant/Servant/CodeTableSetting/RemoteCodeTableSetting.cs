﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Common;
using Library.Servant.Servant.CodeTableSetting.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.CodeTableSetting
{
    public class RemoteCodeTableSetting : ICodeTableSetting
    {
        public CodeTableInfo Detail(string code)
        {
            var result = ProtocolService.EncryptionPost<StringModel, CodeTableInfo>
            (
                new StringModel { Value = code },
                Config.ServantDomain + "/CodeTableServant/Detail"
            );

            return Tools.ExceptionConvert(result);
        }

        public List<SelectedModel> GetCodeClasses()
        {
            throw new NotImplementedException();
        }

        public IndexResult Index(SearchModel condition)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, IndexResult>
            (
                condition,
                Config.ServantDomain + "/CodeTableServant/Index"
            );

            return Tools.ExceptionConvert(result);
        }

        public VoidResult SaveSetting(CodeTableInfo setting)
        {
            var result = ProtocolService.EncryptionPost<CodeTableInfo, VoidResult>
            (
                setting,
                Config.ServantDomain + "/CodeTableServant/SaveSetting"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public VoidResult SaveSettings(ListRemoteModel<CodeTableInfo> settings)
        {
            var result = ProtocolService.EncryptionPost<ListRemoteModel<CodeTableInfo>, VoidResult>
            (
                settings,
                Config.ServantDomain + "/CodeTableServant/SaveSettings"
            );

            return Tools.ExceptionConvert(result);
        }
    }
}
