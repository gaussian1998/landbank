﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.CodeTableSetting.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.CodeTableSetting
{
    public interface ICodeTableSetting
    {
        IndexResult Index(SearchModel condition);
        CodeTableInfo Detail(string code);
        VoidResult SaveSettings(ListRemoteModel<CodeTableInfo> settings);
        VoidResult SaveSetting(CodeTableInfo setting);
        List<SelectedModel> GetCodeClasses();
    }
}
