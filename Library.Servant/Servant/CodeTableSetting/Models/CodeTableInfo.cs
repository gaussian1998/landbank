﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.CodeTableSetting.Models
{
    public class CodeTableInfo : InvasionEncryption
    {
        public string CodeClassName { get; set; }
        public string CodeClass { get; set; }
        public string Code { get; set; }
        public string CodeName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string Parameter1 { get; set; }
        public string Parameter2 { get; set; }
        public decimal? Setting1 { get; set; }
        public decimal? Setting2 { get; set; }
        public decimal? Setting3 { get; set; }
        public string Remark { get; set; }
    }
}
