﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.HistoryData.Models;

namespace Library.Servant.Servant.CodeTableSetting.Models
{
    public class SearchModel : PageInfo
    {
        public string CodeClass { get; set; }
        public string Code { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
