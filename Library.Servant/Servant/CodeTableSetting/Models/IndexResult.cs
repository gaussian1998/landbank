﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.CodeTableSetting.Models
{
    public class IndexResult : InvasionEncryption
    {
        public List<CodeTableInfo> Items { get; set; }
        public int TotalAmount { get; set; }
    }
}
