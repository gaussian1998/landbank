﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Library.Entity.Edmx;
using Library.Servant.Servant.CodeTableSetting.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.CodeTableSetting
{
    public class LocalCodeTableSetting : ICodeTableSetting
    {
        public CodeTableInfo Detail(string code)
        {
            return AS_Code_Table_Records.First(m => m.Code_ID == code, entity => new CodeTableInfo
            {
                Code = entity.Code_ID,
                CodeName = entity.Text,
                CodeClass = entity.Class,
                CodeClassName = entity.Class_Name,
                IsActive = entity.Is_Active,
                IsDeleted = entity.Cancel_Code,
                Parameter1 = entity.Parameter1,
                Parameter2 = entity.Parameter2,
                Setting1 = entity.Value1.Value,
                Setting2 = entity.Value2.Value,
                Setting3 = entity.Value3.Value,
                Remark = entity.Remark
            });
        }

        public IndexResult Index(SearchModel condition)
        {
            var page = AS_Code_Table_Records.Page(condition.Page, condition.PageSize, m => m.ID,
                m => (m.Class == condition.CodeClass) &&
                     (string.IsNullOrEmpty(condition.Code) || m.Code_ID == condition.Code) &&
                     (condition.IsActive == null || condition.IsActive == m.Is_Active) &&
                     (condition.IsDeleted == null || condition.IsDeleted == m.Cancel_Code),
                entity => new CodeTableInfo {
                    Code = entity.Code_ID,
                    CodeName = entity.Text,
                    CodeClass = entity.Class,
                    CodeClassName = entity.Class_Name,
                    IsActive = entity.Is_Active,
                    IsDeleted = entity.Cancel_Code,
                    Parameter1 = entity.Parameter1,
                    Parameter2 = entity.Parameter2,
                    Setting1 = entity.Value1.Value,
                    Setting2 = entity.Value2.Value,
                    Setting3 = entity.Value3.Value,
                    Remark = entity.Remark
                });

            return new IndexResult
            {
                Items = page.Items,
                TotalAmount = page.TotalAmount
            };
        }

        public VoidResult SaveSetting(CodeTableInfo setting)
        {
            if (AS_Code_Table_Records.Any(m => m.Class == setting.CodeClass && m.Code_ID == setting.Code)) {
                AS_Code_Table_Records.Update(m => m.Class == setting.CodeClass && m.Code_ID == setting.Code, entity => {
                    entity.Code_ID = setting.Code;
                    entity.Class = setting.CodeClass;
                    entity.Text = setting.CodeName;
                    entity.Class_Name = setting.CodeClassName;
                    entity.Parameter1 = setting.Parameter1;
                    entity.Parameter2 = setting.Parameter2;
                    entity.Value1 = setting.Setting1;
                    entity.Value2 = setting.Setting2;
                    entity.Value3 = setting.Setting3;
                    entity.Is_Active = setting.IsActive;
                    entity.Cancel_Code = setting.IsDeleted;
                    entity.Remark = setting.Remark;
                });
            }
            else {
                AS_Code_Table_Records.Create(entity => {
                    entity.Code_ID = setting.Code;
                    entity.Class = setting.CodeClass;
                    entity.Text = setting.CodeName;
                    entity.Class_Name = setting.CodeClassName;
                    entity.Parameter1 = setting.Parameter1;
                    entity.Parameter2 = setting.Parameter2;
                    entity.Value1 = setting.Setting1;
                    entity.Value2 = setting.Setting2;
                    entity.Value3 = setting.Setting3;
                    entity.Is_Active = setting.IsActive;
                    entity.Cancel_Code = setting.IsDeleted;
                    entity.Remark = setting.Remark;
                });
            }

            return new VoidResult();
        }

        public VoidResult SaveSettings(ListRemoteModel<CodeTableInfo> settings)
        {
            using (TransactionScope ts = new TransactionScope()) {
                foreach (var setting in settings.Content) {
                    AS_Code_Table_Records.Update(m => m.Class == setting.CodeClass && m.Code_ID == setting.Code, entity => {
                        entity.Is_Active = setting.IsActive;
                        entity.Cancel_Code = setting.IsDeleted;
                    });
                }

                ts.Complete();
            }

            return new VoidResult();
        }

        public List<SelectedModel> GetCodeClasses()
        {
            return AS_Code_Table_Records.Find(m => m.Class == "00").
                Select(m => new SelectedModel { Value = m.Code_ID, Name = m.Text }).ToList();
        }
    }
}
