﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingSubject.Models;
using Library.Servant.Servant.Common.Models;
using Library.Entity.Edmx;
using Library.Utility;

namespace Library.Servant.Servant.AccountingSubject
{
    public class LocalAccountingSubjectServant : IAccountingSubjectServant
    {
        private readonly IAS_GL_AccountRepository _repoAccounting = RepositoryHelper.GetAS_GL_AccountRepository();

        public MessageModel BatchDelete(BatchDeleteModel BatchDelete)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            foreach (int id in BatchDelete.IDList)
            {
                AS_GL_Account_Records.Delete(x => x.ID == id);
            }
            return message;
        }

        public MessageModel Create(DetailModel Create)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            AS_GL_Account acc = new AS_GL_Account
            {
                Book_Type = Create.AccountingBook,
                Account_Type = Create.AccountType,
                Account = Create.Account,
                Account_Name = Create.AccountName,
                Asset_Liablty = Create.AssetLiablty,
                DB_CR = Create.DBCR,
                IFRS_Account = Create.IFRSAccount,
                Open_Date = Create.OpenDate,
                Cancel_Date = Create.CancelDate,
                Ledger_Type = Create.LedgerType,
                Head_Office_Only = Create.HeadOfficeOnly,
                Is_Banking_Dept = Create.IsBankingDept,
                Is_Trust_Dept = Create.IsTrustDept,
                Is_Bond_Dept = Create.IsBondDept,
                Is_CreditCard_Dept = Create.IsCreditCardDept,
                Is_Budget_Limit = Create.IsBudgetLimit,
                Is_Auto_Link = Create.IsAutoLink,
                Income_Type = Create.IncomeType,
                Opposite_Account = Create.OppositeAccount,
                Single_Declare_Acc = Create.SingleDeclareAcc,
                Notes = Create.Notes,
                Last_Updated_Date = DateTime.Now
            };

            AS_GL_Account_Records.Create(acc);
            return message;
        }

        public IndexResult Index(SearchModel Search)
        {
            string AL = "";
            bool skipAL = false;
            bool skipFromAccount = false;
            bool skipToAccount = false;
            bool skipFromDate = false;
            bool skipToDate = false;

            if (Search.AssetLiablty == 0)
            {
                skipAL = true;
            }
            else if (Search.AssetLiablty == 1)
            {
                AL = "A";
            }
            else
            {
                AL = "L";
            }

            if (String.IsNullOrEmpty(Search.FromAccountSubject))
            {
                skipFromAccount = true;
            }
            if (String.IsNullOrEmpty(Search.ToAccountSubject))
            {
                skipToAccount = true;
            }

            if (Search.FromDate == null)
            {
                skipFromDate = true;
            }
            if (Search.ToDate == null)
            {
                skipToDate = true;
            }

            IndexResult result = new IndexResult {
                Conditions = Search,
                Page = AS_GL_Account_Records.Page(
                    Search.CurrentPage,
                    Search.PageSize,
                    x => x.Account,
                    x => (skipAL || x.Asset_Liablty == AL) &&
                         (skipFromAccount || x.Account.CompareTo(Search.FromAccountSubject) >= 0) &&
                         (skipToAccount || x.Account.CompareTo(Search.ToAccountSubject) <= 0) &&
                         (skipFromDate || x.Open_Date >= Search.FromDate) &&
                         (skipToDate || x.Open_Date <= Search.ToDate),
                    m => new DetailModel
                    {
                        ID = m.ID,
                        AccountingBook = m.Book_Type,
                        AccountType = m.Account_Type,
                        Account = m.Account,
                        AccountName = m.Account_Name,
                        AssetLiablty = m.Asset_Liablty,
                        DBCR = m.DB_CR,
                        IFRSAccount = m.IFRS_Account,
                        OpenDate = (m.Open_Date ?? DateTime.MinValue),
                        CancelDate = (m.Cancel_Date ?? DateTime.MaxValue),
                        LedgerType = m.Ledger_Type,
                        HeadOfficeOnly = (m.Head_Office_Only ?? false),
                        IsBankingDept = (m.Is_Banking_Dept ?? false),
                        IsTrustDept = (m.Is_Trust_Dept ?? false),
                        IsBondDept = (m.Is_Bond_Dept ?? false),
                        IsCreditCardDept = (m.Is_CreditCard_Dept ?? false),
                        IsBudgetLimit = (m.Is_Budget_Limit ?? false),
                        IsAutoLink = (m.Is_Auto_Link ?? false),
                        IncomeType = m.Income_Type,
                        OppositeAccount = m.Opposite_Account,
                        SingleDeclareAcc = m.Single_Declare_Acc,
                        IsParent = m.Is_Parent,
                        ParentAccount = m.Parent_Account,
                        Notes = m.Notes,
                        LastUpdateDate = (m.Last_Updated_Date ?? DateTime.MinValue)
                    })
            };

            return result;
        }

        public MessageModel Update(DetailModel Update)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            AS_GL_Account_Records.Update(x => x.ID == Update.ID, m => {
                m.Book_Type = Update.AccountingBook;
                m.Account_Type = Update.AccountType;
                m.Account = Update.Account;
                m.Account_Name = Update.AccountName;
                m.Asset_Liablty = Update.AssetLiablty;
                m.DB_CR = Update.DBCR;
                m.IFRS_Account = Update.IFRSAccount;
                m.Open_Date = Update.OpenDate;
                m.Cancel_Date = Update.CancelDate;
                m.Ledger_Type = Update.LedgerType;
                m.Head_Office_Only = Update.HeadOfficeOnly;
                m.Is_Banking_Dept = Update.IsBankingDept;
                m.Is_Trust_Dept = Update.IsTrustDept;
                m.Is_Bond_Dept = Update.IsBondDept;
                m.Is_CreditCard_Dept = Update.IsCreditCardDept;
                m.Is_Budget_Limit = Update.IsBudgetLimit;
                m.Is_Auto_Link = Update.IsAutoLink;
                m.Income_Type = Update.IncomeType;
                m.Opposite_Account = Update.OppositeAccount;
                m.Single_Declare_Acc = Update.SingleDeclareAcc;
                m.Notes = Update.Notes;
                m.Last_Updated_Date = DateTime.Now;
            });

            return message;
        }

        public DetailModel Detail(int ID)
        {
            DetailModel result = AS_GL_Account_Records.First(x => x.ID == ID, m => new DetailModel {
                ID = m.ID,
                AccountingBook = m.Book_Type,
                AccountType = m.Account_Type,
                Account = m.Account,
                AccountName = m.Account_Name,
                AssetLiablty = m.Asset_Liablty,
                DBCR = m.DB_CR,
                IFRSAccount = m.IFRS_Account,
                OpenDate = (m.Open_Date ?? DateTime.MinValue),
                CancelDate = (m.Cancel_Date ?? DateTime.MaxValue),
                LedgerType = m.Ledger_Type,
                HeadOfficeOnly = (m.Head_Office_Only ?? false),
                IsBankingDept = (m.Is_Banking_Dept ?? false),
                IsTrustDept = (m.Is_Trust_Dept ?? false),
                IsBondDept = (m.Is_Bond_Dept ?? false),
                IsCreditCardDept = (m.Is_CreditCard_Dept ?? false),
                IsBudgetLimit = (m.Is_Budget_Limit ?? false),
                IsAutoLink = (m.Is_Auto_Link ?? false),
                IncomeType = m.Income_Type,
                OppositeAccount = m.Opposite_Account,
                SingleDeclareAcc = m.Single_Declare_Acc,
                IsParent = m.Is_Parent,
                ParentAccount = m.Parent_Account,
                Notes = m.Notes,
                LastUpdateDate = (m.Last_Updated_Date ?? DateTime.MinValue)
            });

            return result;
        }

        public byte[] ExportData(SearchModel Search)
        {
            string AL = "";
            bool skipAL = false;
            bool skipFromAccount = false;
            bool skipToAccount = false;
            bool skipFromDate = false;
            bool skipToDate = false;

            if (Search.AssetLiablty == 0)
            {
                skipAL = true;
            }
            else if (Search.AssetLiablty == 1)
            {
                AL = "A";
            }
            else
            {
                AL = "L";
            }

            if (String.IsNullOrEmpty(Search.FromAccountSubject))
            {
                skipFromAccount = true;
            }
            if (String.IsNullOrEmpty(Search.ToAccountSubject))
            {
                skipToAccount = true;
            }

            if (Search.FromDate == null)
            {
                skipFromDate = true;
            }
            if (Search.ToDate == null)
            {
                skipToDate = true;
            }
            List<AS_GL_Account> data = _repoAccounting.Where(x => (skipAL || x.Asset_Liablty == AL) &&
                         (skipFromAccount || x.Account.CompareTo(Search.FromAccountSubject) > 0) &&
                         (skipToAccount || x.Account.CompareTo(Search.ToAccountSubject) < 0) &&
                         (skipFromDate || x.Open_Date > Search.FromDate) &&
                         (skipToDate || x.Open_Date < Search.ToDate)).ToList();

            byte[] header = { 0xEF, 0xBB, 0xBF };
            var sb = new StringBuilder();
            sb.Append("帳簿類別, 科子細目編號, IFRS會計科目, 科子細目名稱, 資產負債別, 啟用日期, 刪除日期, 借貸別, 分列帳式, 限總行, 銀行部, 信託部, 證券部, 信用卡部, 預算控制, 連動記號, 收支歸屬, 對方科目, 申報科目, 類別, 備註");

            foreach (var item in data)
            {
                sb.AppendFormat("{0}{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21}",
                    Environment.NewLine,
                    item.Book_Type, item.Account, item.IFRS_Account, item.Account_Name,
                    item.Asset_Liablty == "A" ? "資產" : "負債", 
                    item.Open_Date != null ? Convert.ToDateTime(item.Open_Date).ToString("yyyy-MM-dd") : "",
                    item.Cancel_Date != null ? Convert.ToDateTime(item.Cancel_Date).ToString("yyyy-MM-dd") : "",
                    item.DB_CR == "D" ? "借" : "貸", item.Ledger_Type,
                    Convert.ToBoolean(item.Head_Office_Only) ? "是" : "否",
                    Convert.ToBoolean(item.Is_Banking_Dept) ? "是" : "否",
                    Convert.ToBoolean(item.Is_Trust_Dept) ? "是" : "否",
                    Convert.ToBoolean(item.Is_Bond_Dept) ? "是" : "否",
                    Convert.ToBoolean(item.Is_CreditCard_Dept) ? "是" : "否",
                    Convert.ToBoolean(item.Is_Budget_Limit) ? "是" : "否",
                    Convert.ToBoolean(item.Is_Auto_Link) ? "是" : "否",
                    item.Income_Type, item.Opposite_Account, item.Single_Declare_Acc,
                    item.Account_Type == "R" ? "Real" : (item.Account_Type == "M" ? "Memo" : "Dummy"),
                    item.Notes.Replace(',', ' ')
                    );
            }
            return header.Concat(Encoding.UTF8.GetBytes(sb.ToString())).ToArray();

        }

        public AsGlAccountModel GetAccountInfo(string account)
        {
            AsGlAccountModel _model = new AsGlAccountModel();
            _model=MapProperty.OverrideBy<AsGlAccountModel, AS_GL_Account>(_model, _repoAccounting.All().FirstOrDefault(p => p.Account == account));
            return _model;
        }

        public int GetFlowFormId()
        {
            var result = AS_Doc_Name_Records.First(x => x.Doc_No == "Y100").ID;
            return result;
        }

        public string GetFlowFormNo()
        {
            return "Y100";
        }

        public MessageModel ImportData(List<DetailModel> Import)
        {
            MessageModel message = new MessageModel { IsSuccess = true };
            if(Import.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("delete from AS_GL_Account;");
                string insertCommnd = "INSERT [AS_GL_Account] ([Book_Type], [Account_Type], [Account], [Account_Name], [Asset_Liablty], [DB_CR], [IFRS_Account], [Open_Date], [Cancel_Date], [Ledger_Type], [Head_Office_Only], [Is_Banking_Dept], [Is_Trust_Dept], [Is_Bond_Dept], [Is_CreditCard_Dept], [Is_Budget_Limit], [Is_Auto_Link], [Income_Type], [Opposite_Account], [Last_Updated_Date], [Single_Declare_Acc], [Is_Parent], [Parent_Account], [Notes]) VALUES (N'{0}', N'{1}', N'{2}', N'{3}', N'{4}', N'{5}', N'{6}', {7}, {8}, N'{9}', {10}, {11}, {12}, {13}, {14}, {15}, {16}, N'{17}', N'{18}', {19}, N'{20}', N'{21}', N'{22}', N'{23}');";
                string updateDate = "getdate()";

                foreach (var item in Import)
                {
                    string openDate = item.OpenDate == DateTime.MinValue ? "NULL" : String.Format("CAST(N'{0}' AS Date)", item.OpenDate.ToString("yyyy-MM-dd"));
                    string cancelDate = item.CancelDate == DateTime.MinValue ? "NULL" : String.Format("CAST(N'{0}' AS Date)", item.CancelDate.ToString("yyyy-MM-dd"));
                    

                    sb.AppendFormat(insertCommnd, 
                        item.AccountingBook, item.AccountType, item.Account, 
                        item.AccountName, item.AssetLiablty, item.DBCR,
                        item.IFRSAccount, openDate, cancelDate, item.LedgerType, 
                        item.HeadOfficeOnly ? 1 : 0, item.IsBankingDept ? 1: 0, 
                        item.IsTrustDept ? 1: 0, item.IsBondDept ? 1:0, item.IsCreditCardDept ? 1: 0,
                        item.IsBudgetLimit ? 1:0, item.IsAutoLink ? 1: 0, item.IncomeType,
                        item.OppositeAccount, updateDate, item.SingleDeclareAcc, 0, item.ParentAccount, item.Notes);
                }

                using (var context = new AS_LandBankEntities())
                {
                    context.Database.ExecuteSqlCommand(sb.ToString());
                }
            }
            return message;
        }


    }
}
