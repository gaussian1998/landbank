﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Entity.Edmx;
using Library.Servant.Servant.AccountingSubject.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.AccountingSubject
{
    public class RemoteAccountingSubjectServant : IAccountingSubjectServant
    {
        public MessageModel BatchDelete(BatchDeleteModel BatchDelete)
        {
            return RemoteServant.Post<BatchDeleteModel, MessageModel>(
                            BatchDelete,
                            "/AccountingSubjectServant/BatchDelete"
           );
        }

        public MessageModel Create(DetailModel Create)
        {
            return RemoteServant.Post<DetailModel, MessageModel>(
                            Create,
                            "/AccountingSubjectServant/Create"
           );
        }

        public IndexResult Index(SearchModel Search)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(
                            Search,
                            "/AccountingSubjectServant/Index"
           );
        }

        public MessageModel Update(DetailModel Update)
        {
            return RemoteServant.Post<DetailModel, MessageModel>(
                            Update,
                            "/AccountingSubjectServant/Update"
           );
        }

        public DetailModel Detail(int ID)
        {
            return RemoteServant.Post<IntModel, DetailModel>(
                            new IntModel {Value=ID },
                            "/AccountingSubjectServant/Detail"
           );
        }

        public AsGlAccountModel GetAccountInfo(string account)
        {
            return RemoteServant.Post<StringModel, AsGlAccountModel>(
                            new StringModel { Value = account },
                            "/AccountingSubjectServant/GetAccountInfo"
           );
        }

        public byte[] ExportData(SearchModel Search)
        {
            return (RemoteServant.Post<SearchModel, ByteModel>(
                            Search,
                            "/AccountingSubjectServant/ExportData"
           )).Value;
        }

        public int GetFlowFormId()
        {
            return (RemoteServant.Post<VoidModel, IntModel>(
                            new VoidModel {  },
                            "/AccountingSubjectServant/GetFlowFormId"
           )).Value;
        }

        public MessageModel ImportData(List<DetailModel> Import)
        {
            ListDetailModel _listModel = new Models.ListDetailModel();
            _listModel.ListData = Import;
            return RemoteServant.Posttext<ListDetailModel, MessageModel>(
                            _listModel,
                            "/AccountingSubjectServant/ImportData"
           );
        }

        public string GetFlowFormNo()
        {
            return (RemoteServant.Post<VoidModel, StringModel>(
                            new VoidModel { },
                            "/AccountingSubjectServant/GetFlowFormNo"
           )).Value;
        }
    }
}
