﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.AccountingSubject.Models;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.AccountingSubject
{
    public interface IAccountingSubjectServant
    {
        MessageModel Create(DetailModel Create);
        MessageModel Update(DetailModel Update);
        MessageModel BatchDelete(BatchDeleteModel BatchDelete);
        IndexResult Index(SearchModel Search);
        DetailModel Detail(int ID);
        AsGlAccountModel GetAccountInfo(string account);
        byte[] ExportData(SearchModel Search);
        MessageModel ImportData(List<DetailModel> Import);
        int GetFlowFormId();
        string GetFlowFormNo();
    }
}
