﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servant.AccountingSubject.Models;
using Newtonsoft.Json;
using Library.Servant.Servants.AbstractFactory;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.AccountingSubject
{
    public class AccountingSubjectVerifyListener : ExampleListener, IPartialViewProvider
    {
        IAccountingSubjectServant _accountingServant = new LocalAccountingSubjectServant();
        public override bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            var Model = JsonConvert.DeserializeObject<DetailModel>(carry);
            MessageModel message = new MessageModel { IsSuccess = false };
            if (Model.ID > 0)
            {
                message = _accountingServant.Update(Model);
            }
            else
            {
                message = _accountingServant.Create(Model);
            }
            return message.IsSuccess;
        }

        public PartialViewModel PartialView(string FormNo, string carryData)
        {
            return new PartialViewModel { ControllerName = "AccountingSubject", ActionName = "VerifyForm", Data = carryData };
        }
    }
}
