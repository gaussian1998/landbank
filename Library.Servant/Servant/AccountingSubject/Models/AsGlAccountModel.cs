﻿using Library.Servant.Communicate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.AccountingSubject.Models
{
    public class AsGlAccountModel : InvasionEncryption
    {
        public int ID { get; set; }
        public string Book_Type { get; set; }
        public string Account_Type { get; set; }
        public string Account { get; set; }
        public string Account_Name { get; set; }
        public string Asset_Liablty { get; set; }
        public string DB_CR { get; set; }
        public string IFRS_Account { get; set; }
        public Nullable<System.DateTime> Open_Date { get; set; }
        public Nullable<System.DateTime> Cancel_Date { get; set; }
        public string Ledger_Type { get; set; }
        public Nullable<bool> Head_Office_Only { get; set; }
        public Nullable<bool> Is_Banking_Dept { get; set; }
        public Nullable<bool> Is_Trust_Dept { get; set; }
        public Nullable<bool> Is_Bond_Dept { get; set; }
        public Nullable<bool> Is_CreditCard_Dept { get; set; }
        public Nullable<bool> Is_Budget_Limit { get; set; }
        public Nullable<bool> Is_Auto_Link { get; set; }
        public string Income_Type { get; set; }
        public string Opposite_Account { get; set; }
        public Nullable<System.DateTime> Last_Updated_Date { get; set; }
        public string Single_Declare_Acc { get; set; }
        public string Is_Parent { get; set; }
        public string Parent_Account { get; set; }
        public string Notes { get; set; }
    }
}
