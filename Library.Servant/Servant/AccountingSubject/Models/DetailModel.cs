﻿using Library.Servant.Communicate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.AccountingSubject.Models
{
    public class DetailModel : InvasionEncryption
    {
        public int ID { get; set; }
        public string AccountingBook { get; set; }
        public string AccountType { get; set; }
        public string Account { get; set; }
        public string AccountName { get; set; }
        public string AssetLiablty { get; set; }
        public string DBCR{ get; set; }
        public string IFRSAccount { get; set; }
        public DateTime OpenDate { get; set; }
        public DateTime CancelDate { get; set; }
        public string LedgerType { get; set; }
        public bool HeadOfficeOnly { get; set; }
        public bool IsBankingDept { get; set; }
        public bool IsTrustDept { get; set; }
        public bool IsBondDept { get; set; }
        public bool IsCreditCardDept { get; set; }
        public bool IsBudgetLimit { get; set; }
        public bool IsAutoLink { get; set; }
        public string IncomeType { get; set; }
        public string OppositeAccount { get; set; }
        public string SingleDeclareAcc { get; set; }
        public string IsParent { get; set; }
        public string ParentAccount { get; set; }
        public string Notes { get; set; }
        public DateTime LastUpdateDate { get; set; }
    }
}
