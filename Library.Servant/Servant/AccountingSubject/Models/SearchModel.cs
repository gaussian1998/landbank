﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;
namespace Library.Servant.Servant.AccountingSubject.Models
{
    public class SearchModel : InvasionEncryption
    {
        /// <summary>
        /// 1 : asset, 2:liablty, 0: all
        /// </summary>
        public int AssetLiablty { get; set; }
        public string FromAccountSubject { get; set; }
        public string ToAccountSubject { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
    }
}
