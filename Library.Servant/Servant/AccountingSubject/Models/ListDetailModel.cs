﻿using Library.Servant.Communicate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.AccountingSubject.Models
{
    public class ListDetailModel : InvasionEncryption
    {
        public List<DetailModel> ListData { get; set; }
    }
}
