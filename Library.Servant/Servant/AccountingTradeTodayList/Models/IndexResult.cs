﻿using Library.Entity.Edmx;
using Library.Servant.Communicate;
using System.Collections.Generic;

namespace Library.Servant.Servant.AccountingTradeTodayList.Models
{
    public class IndexResult : InvasionEncryption
    {
        public List<FR_Accounting_Report> Items { get; set; }
    }
}
