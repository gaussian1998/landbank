﻿using Library.Servant.Servant.AccountingTradeTodayList.Models;
using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.AccountingTradeTodayList
{
    public class RemoteAccountingTradeTodayListServant : IAccountingTradeTodayListServant
    {
        public BranchDepartmentOptions Options()
        {
            return RemoteServant.Post<VoidModel, BranchDepartmentOptions>(

                            new VoidModel { },
                            "/AccountingTradeTodayListServant/Options"
           );
        }

        public IndexResult Index(BranchSearchModel model)
        {
            return RemoteServant.Post<BranchSearchModel, IndexResult>(

                            model,
                            "/AccountingTradeTodayListServant/Index"
            );
        }
    }
}
