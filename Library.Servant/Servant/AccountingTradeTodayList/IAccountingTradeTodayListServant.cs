﻿using Library.Servant.Servant.AccountingTradeTodayList.Models;
using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.AccountingTradeTodayList
{
    public interface IAccountingTradeTodayListServant
    {
        BranchDepartmentOptions Options();
        IndexResult Index(BranchSearchModel model);
    }
}
