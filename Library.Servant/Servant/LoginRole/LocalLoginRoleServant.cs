﻿using System.Linq;
using System.Collections.Generic;
using System.Transactions;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.LoginRole.Models;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.LoginRole
{
    public class LocalLoginRoleServant : ILoginRoleServant
    {
        public IndexResult Index()
        {
            return new IndexResult { Items = AS_Role_Records.FindAll( entity => new DetailsResult {

                ID = entity.ID,
                FunctionNames = entity.AS_Programs_Roles.AsQueryable().Select( m => m.AS_Programs.Programs_Name ).ToList()
            } ) };
        }

        public IndexResult Index(SearchModel search)
        {
            var isSkipRole = string.IsNullOrEmpty(search.RoleID);
            var isSkipDocType = string.IsNullOrEmpty(search.DocType);

            var result = AS_Role_Records.Page( 
                search.Page, 
                search.PageSize, 
                m => m.ID, 
                m => (isSkipRole || m.Role_Code == search.RoleID) &&
                     (isSkipDocType || m.Doc_Type == search.DocType), 
                entity => new DetailsResult
                {
                    ID = entity.ID,
                    Name = entity.Role_Name,
                    Description = entity.Remark,
                    FunctionNames = entity.AS_Programs_Roles.AsQueryable().Select(m => m.AS_Programs.Programs_Name).ToList()
                });

            return new IndexResult
            {
                Items = result.Items,
                TotalAmount = result.TotalAmount,
                TotalRoles = TotalRoles()
            };
        }

        public NewResult New()
        {
            return new NewResult {

                CadidateFunctions = AS_Programs_Records.FindAll( entity => new OptionModel<int,string> { Value = entity.ID, Text = entity.Programs_Name } )
            };
        }

        public VoidResult Create(CreateModel model)
        {
            AS_Role_Records.Create( model, entity => {

                entity.Role_Code = model.Code;
                entity.Role_Name = model.Name;
                entity.Remark = model.Description;

                foreach(var function_id in model.SelectedProgramID)
                    entity.AS_Programs_Roles.Add( new Entity.Edmx.AS_Programs_Roles { Program_ID = function_id } );
            } );

            return new VoidResult { };
        }



        public EditResult Edit(int ID)
        {
            var result = AS_Role_Records.First(m => m.ID == ID, entity => new EditResult
            {
                ID = entity.ID,
                Code = entity.Role_Code,
                Name = entity.Role_Name,
                Description = entity.Remark,
                SelectedFunctions = entity.AS_Programs_Roles.AsQueryable().Select( m => new OptionModel<int,string>
                {
                    Value = m.AS_Programs.ID,
                    Text = m.AS_Programs.Programs_Name
                } ).ToList()
            } );

            result.CadidateFunctions = CadidateFunctions(result.SelectedFunctions);
            return result;
        }

        public VoidResult Update(UpdateModel model)
        {
            using (var trans = new TransactionScope())
            {
                AS_Programs_Roles_Records.Delete(m => m.Role_ID == model.ID);
                AS_Role_Records.Update(m => m.ID == model.ID, entity => {

                    entity.Role_Code = model.Code;
                    entity.Role_Name = model.Name;
                    entity.Remark = model.Description;

                    foreach (var function in model.SelectedFunctionsID)
                        entity.AS_Programs_Roles.Add(new AS_Programs_Roles { Program_ID = function });
                });

                trans.Complete();
            }

            return new VoidResult { };
        }

        public VoidResult Delete(int ID)
        {
            AS_Role_Records.Delete( m => m.ID == ID);
            return new VoidResult { };
        }

        public static List<OptionModel<int, string>> CadidateLoginRoles(List<OptionModel<int, string>> SelectedLoginRoles)
        {
            var selectedRolesID = SelectedLoginRoles.Select(m => m.Value);
            return AS_Role_Records.Find(m => !selectedRolesID.Contains(m.ID), entity => new OptionModel<int, string>
            {
                Value = entity.ID,
                Text = entity.Role_Name
            });
        }

        private static List<OptionModel<int, string>> CadidateFunctions(List<OptionModel<int, string>> SelectedFunctions)
        {
            var selectedFunctionsID = SelectedFunctions.Select(m => m.Value);
            return AS_Programs_Records.Find(m => !selectedFunctionsID.Contains(m.ID), entity => new OptionModel<int, string>
            {
                Value = entity.ID,
                Text = entity.Programs_Name
            });
        }

        private static List<OptionModel<string, string>> TotalFunctions()
        {
            return AS_Programs_Records.FindAll(m => new OptionModel<string, string> { Value = m.Programs_Code, Text = m.Programs_Name });
        }

        private static List<OptionModel<string, string>> TotalRoles()
        {
            return AS_Role_Records.FindAll(m => new OptionModel<string, string> { Value = m.Role_Code, Text = m.Role_Name });
        }

        public List<OptionModel<string, string>> GetOperatingTypes()
        {
            return AS_Code_Table_Records.Find(m => m.Class == "R09", m => new OptionModel<string, string> { Value = m.Code_ID, Text = m.Text });
        }

        public List<OptionModel<string, string>> GetBranchTypes()
        {
            return AS_Code_Table_Records.Find(m => m.Class == "R04", m => new OptionModel<string, string> { Value = m.Code_ID, Text = m.Text });
        }
    }
}
