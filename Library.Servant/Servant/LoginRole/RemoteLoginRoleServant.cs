﻿using System;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.LoginRole.Models;
using Library.Servant.Servant.FunctionRole;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace Library.Servant.Servant.LoginRole
{
    public class RemoteLoginRoleServant : ILoginRoleServant
    {
        public IndexResult Index()
        {
            throw new NotImplementedException();
        }

        public IndexResult Index(SearchModel search)
        {
            throw new NotImplementedException();
        }

        public NewResult New()
        {
            throw new NotImplementedException();
        }

        public VoidResult Create(CreateModel model)
        {
            throw new NotImplementedException();
        }

        public EditResult Edit(int ID)
        {
            throw new NotImplementedException();
        }

        public VoidResult Update(UpdateModel model)
        {
            throw new NotImplementedException();
        }


        public VoidResult Delete(int ID)
        {
            throw new NotImplementedException();
        }

        public List<OptionModel<string, string>> GetOperatingTypes()
        {
            throw new NotImplementedException();
        }

        public List<OptionModel<string, string>> GetBranchTypes()
        {
            throw new NotImplementedException();
        }
    }
}
