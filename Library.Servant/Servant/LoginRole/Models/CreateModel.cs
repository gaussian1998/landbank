﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Servant.LoginRole.Models
{
    public class CreateModel : InvasionEncryption
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<int> SelectedProgramID { get; set; }
    }
}
