﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.LoginRole.Models
{
    public class SearchModel : InvasionEncryption
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string RoleID { get; set; }
        public string DocType { get; set; }
    }
}
