﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;
using System;

namespace Library.Servant.Servant.LoginRole.Models
{
    public class UpdateModel : InvasionEncryption
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public DateTime ExpireDate { get; set; }
        public string DocType { get; set; }
        public string OperatingType { get; set; }
        public string BranchType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public List<int> SelectedFunctionsID { get; set; }
    }
}
