﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Servant.LoginRole.Models
{
    public class IndexResult : InvasionEncryption
    {
        public List<DetailsResult> Items { get; set; }
        public int TotalAmount { get; set; }
        public List<OptionModel<string, string>> TotalRoles { get; set; }
    }
}
