﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Servant.LoginRole.Models
{
    public class NewResult : InvasionEncryption
    {
        public List<OptionModel<int,string>> CadidateFunctions { get; set; }
    }
}
