﻿using System.Collections.Generic;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.LoginRole.Models;

namespace Library.Servant.Servant.LoginRole
{
    public interface ILoginRoleServant
    {
        NewResult New();
        IndexResult Index();
        IndexResult Index(SearchModel search);
        VoidResult Create(CreateModel model);
        EditResult Edit(int ID);
        VoidResult Update(UpdateModel model);
        VoidResult Delete(int ID);
        List<OptionModel<string, string>> GetOperatingTypes();
        List<OptionModel<string, string>> GetBranchTypes();
    }
}
