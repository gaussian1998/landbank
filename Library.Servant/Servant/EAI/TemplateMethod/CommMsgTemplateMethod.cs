﻿using Library.Servant.Servant.EAI.RequestModels;
using Library.Servant.Servant.EAI.ResponseModels;
using System;

namespace Library.Servant.Servant.EAI.TemplateMethod
{
    public abstract class CommMsgTemplateMethod
    {
        public EAIResponseResult Call()
        {
            string response = EAIUtility.PostToEAI(Url(), RequestXml);
            Console.WriteLine(response);
            return new EAIResponseResult { XML = response };
        }

        private string RequestXml
        {
            get
            {
                DateTime dt = DateTime.Now;
                CommMsgRequestModel model = new CommMsgRequestModel
                {
                    SPName = SPName(),
                    CustLoginId = CustLoginId(),
                    Pswd = EAIUtility.DesEncrypt( MacKey(), EAIUtility.SeedDt(dt)),
                    ClientDt = EAIUtility.ClientDt(dt),
                    MsgId = MsgId(),
                    KINBR = KINBR(),
                    TRMSEQ = TRMSEQ(),
                    RqUID = RqUID(),
                    TXTNO = TXTNO(),
                    TLRNO = TLRNO(),
                    APTYPE = APTYPE(),
                    TXNO = TXNO(),
                    STXNO = STXNO(),
                    PTYPE = PTYPE(),
                    TXTYPE = TXTYPE(),
                    CRDB = CRDB(),
                    HCODE = HCODE(),
                    YCODE = YCODE(),
                    CURCD = CURCD(),
                    TXAMT = TXAMT(),
                    MRKEY = MRKEY(),
                    TITA_TXDAY = TITA_TXDAY(),
                    TITA_TXTIME = TITA_TXTIME(),
                    ISDAY = ISDAY(),
                    CNT = CNT(),
                    IBSRNUM1 = IBSRNUM1(),
                    IDSCPT1 = IDSCPT1(),
                    ITXAMT1 = ITXAMT1(),
                    DEPNO = DEPNO(),
                    ACCNO = ACCNO(),
                    TRMTYP = TRMTYP()
                };

                Console.WriteLine(model.XML);
                return model.XML;
            }
        }
        
        public abstract string MacKey();
        public abstract string MsgId();
        public abstract string KINBR();
        public abstract string TRMSEQ();
        public abstract string RqUID();
        public abstract string TXTNO();
        public abstract string TLRNO();

        public virtual string SPName()
        {
            return "AS";
        }

        public virtual string CustLoginId()
        {
            return "SAS00ETA01";
        }

        public virtual string Url()
        {
            return "https://md.landbankt.com.tw/EAI/httppost.ashx";
        }

        public virtual string TRMTYP()
        {
            return "9";
        }

        public virtual string APTYPE()
        {
            return "";
        }

        public virtual string TXNO()
        {
            return "";
        }

        public virtual string STXNO()
        {
            return "";
        }

        public virtual string PTYPE()
        {
            return "";
        }

        public virtual string TXTYPE()
        {
            return "";
        }

        public virtual string CRDB()
        {
            return "";
        }

        public virtual string HCODE()
        {
            return "";
        }

        public virtual string YCODE()
        {
            return "";
        }

        public virtual string CURCD()
        {
            return "";
        }

        public virtual string TXAMT()
        {
            return "";
        }

        public virtual string MRKEY()
        {
            return "";
        }

        public virtual string TITA_TXDAY()
        {
            return "";
        }

        public virtual string TITA_TXTIME()
        {
            return "";
        }

        public virtual string ISDAY()
        {
            return "";
        }

        public virtual string CNT()
        {
            return "";
        }

        public virtual string IBSRNUM1()
        {
            return "";
        }

        public virtual string IDSCPT1()
        {
            return "";
        }

        public virtual string ITXAMT1()
        {
            return "";
        }

        public virtual string DEPNO()
        {
            return "";
        }

        public virtual string ACCNO()
        {
            return "";
        }
    }
}
