﻿using Library.Servant.Servant.EAI.ResponseModels;
using Library.Utility;

namespace Library.Servant.Servant.EAI.TemplateMethod
{
    public abstract class ExchangeKeyTemplateMethod
    {
        public Optional<string> Call()
        {
            EAIResponseResult result = new EAIResponseResult { };
            result.XML = EAIUtility.PostToEAI( Url(), RequestXml);

            if (result.StatusCode == "0")
                return Optional<string>.Ok( EAIUtility.GenerateMacKey( SignKey(), result.NewPswd) );
            else
                return Optional<string>.Error( result.StatusDesc );
        }
        

        private string RequestXml
        {
            get
            {
                return EAIUtility.ExchangeKeyRequestXml( SignKey(), SPName(), CustLoginId());
            }
        }

        public virtual string SPName()
        {
            return "AS";
        }

        public virtual string CustLoginId()
        {
            return "SAS00ETA01";
        }

        public virtual string SignKey()
        {
            return "000A27D229DCFFF6";
        }
        public virtual string Url()
        {
            return "https://md.landbankt.com.tw/EAI/httppost.ashx";
        }
    }
}
