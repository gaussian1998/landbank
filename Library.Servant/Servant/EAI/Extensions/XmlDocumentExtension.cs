﻿using System.Xml;

namespace Library.Servant.Servant.EAI.Extensions
{
    public static class XmlDocumentExtension
    {
        public static string ChannelText(this XmlDocument doc, string nodeName)
        {
            return doc.SigonRsText( "Channel", nodeName);
        }

        public static string PSWDText(this XmlDocument doc, string nodeName)
        {
            return doc.SigonRsText("PSWD", nodeName);
        }

        public static string ClientDtText(this XmlDocument doc, string nodeName)
        {
            return doc.SigonRsText("ClientDt", nodeName);
        }

        public static string SPNameText(this XmlDocument doc, string nodeName)
        {
            return doc.SigonRsText("SPName", nodeName);
        }

        public static string MsgIdText(this XmlDocument doc, string nodeName)
        {
            return doc.SigonRsText("MsgId", nodeName);
        }

        public static string CustIdText(this XmlDocument doc, string nodeName)
        {
            return doc.SigonRsText("CustId", nodeName);
        }

        public static string StatusText(this XmlDocument doc, string nodeName)
        {
            return doc.SigonRsText("Status", nodeName);
        }

        public static string SignonRsText(this XmlDocument doc, string nodeName)
        {
            return doc.SigonRsText("", nodeName);
        }

        public static string CommMsgStatusText(this XmlDocument doc, string nodeName)
        {
            return doc.CommMsgText( "Status", nodeName);
        }

        private static string SigonRsText(this XmlDocument doc, string parentNodeName, string nodeName)
        {
            var node = doc.SelectSingleNode(string.Format("LandBankML/SignonRs/{0}/{1}", parentNodeName, nodeName));
            return node == null ? string.Empty : node.InnerText;
        }

        private static string CommMsgText(this XmlDocument doc, string parentNodeName, string nodeName)
        {
            var node = doc.SelectSingleNode(string.Format("LandBankML/CommMsg/{0}/{1}", parentNodeName, nodeName));
            return node == null ? string.Empty : node.InnerText;
        }
    }
}
