﻿using Library.Servant.Servant.EAI.RequestModels;
using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace Library.Servant.Servant.EAI
{
    public static class EAIUtility
    {
        public static string ExchangeKeyRequestXml(string SignKey, string SPName, string CustLoginId)
        {
            DateTime dt = DateTime.Now;
            ExchangeKeyRequestModel model = new ExchangeKeyRequestModel
            {

                SPName = SPName,
                CustLoginId = CustLoginId,
                OldPswd = GenerateOldPassword(SignKey, dt),
                ClientDt = ClientDt(dt)
            };

            return model.XML;
        }

        public static string PostToEAI(string url, string requestXml)
        {
            WebResponse response = HttpRequest(url).Post( Encoding.UTF8.GetBytes(requestXml), "application/xml ; charset=utf-8");

            return Encoding.UTF8.GetString(response.ReadBody());
        }

        private static HttpWebRequest HttpRequest(string url)
        {
            HttpWebRequest result = HttpWebRequest.Create(url) as HttpWebRequest;
            result.Timeout = 10 * 1000;

            return result;
        }

        public static string ClientDt(DateTime dt)
        {
            return dt.ToString("yyyy/MM/dd HH:mm:ss");
        }

        public static string SeedDt(DateTime dt)
        {
            return "F" + dt.ToString("yyyyMMddHHmmss") + "F";
        }

        public static string GenerateOldPassword(string Key, DateTime dt)
        {
            return DesEncrypt(Key, SeedDt(dt));
        }

        public static string GenerateMacKey(string Key, string EAIGeneratePassword)
        {
            return DesDecrypt(Key, EAIGeneratePassword);
        }

        public static string DesEncrypt(string Key, string hex)
        {
            return DesMethod(hex, Encryptor(Key));
        }

        public static string DesDecrypt(string Key, string hex)
        {
            return DesMethod(hex, Decryptor(Key));
        }

        private static string DesMethod(string hex, ICryptoTransform transform)
        {
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, transform, CryptoStreamMode.Write))
                {
                    byte[] bytes = hex.ToBytes();
                    cs.Write(bytes, 0, bytes.Length);
                }
                return ms.ToArray().ToHexString();
            }
        }

        private static byte[] ToBytes(this string hex)
        {
            byte[] result = BitConverter.GetBytes(UInt64.Parse(hex, System.Globalization.NumberStyles.HexNumber));

            Array.Reverse(result);
            return result;
        }

        private static string ToHexString(this byte[] bytes)
        {
            var hex = BitConverter.ToString(bytes);
            return hex.Replace("-", "");
        }

        private static ICryptoTransform Encryptor(string Key)
        {
            byte[] DesKey = Key.ToBytes();

            return Provider().CreateEncryptor(DesKey, DesIV);
        }

        private static ICryptoTransform Decryptor(string Key)
        {
            byte[] DesKey = Key.ToBytes();

            return Provider().CreateDecryptor(DesKey, DesIV);
        }

        private static DESCryptoServiceProvider Provider()
        {
            var provider = new DESCryptoServiceProvider();
            provider.Mode = CipherMode.ECB;
            provider.Padding = PaddingMode.None;

            return provider;
        }

        private static readonly string IV = "0000000000000000";
        private static readonly byte[] DesIV = IV.ToBytes();
    }
}
