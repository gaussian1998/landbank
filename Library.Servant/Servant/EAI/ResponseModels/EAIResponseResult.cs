﻿using Library.Servant.Servant.EAI.Extensions;
using System.Text;
using System.Xml;

namespace Library.Servant.Servant.EAI.ResponseModels
{
    public class EAIResponseResult
    {
        public string StatusCode { get; set; }
        public string StatusDesc { get; set; }
        public string StatusDetail { get; set; }
        public string SPName { get; set; }
        public string CustLoginId { get; set; }
        public string ClientDt { get; set; }
        public string ServerDt { get; set; }
        public string NewPswd { get; set; }
        public string ExpireDt { get; set; }

        public string CommMsg_StatusCode { get; set; }
        public string Severity { get; set; }
        public string CommMsg_StatusDesc { get; set; }
        public string CommMsg_StatusDetail { get; set; }

        public string XML
        {
            get
            {
                return string.Format( Template, StatusCode, StatusDesc, StatusDetail,
                    SPName, CustLoginId, ClientDt, ServerDt, NewPswd, ExpireDt,
                    CommMsg_StatusCode, Severity, CommMsg_StatusDesc, CommMsg_StatusDetail);
            }
            set
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(value.Replace("xmlns", "skip"));

                StatusCode = doc.StatusText("StatusCode");
                StatusDesc = doc.StatusText("StatusDesc");
                StatusDetail = doc.StatusText("StatusDetail");

                SPName = doc.CustIdText("SPName");
                CustLoginId = doc.CustIdText("CustLoginId");

                ClientDt = doc.SignonRsText("ClientDt");
                ServerDt = doc.SignonRsText("ServerDt");
                NewPswd = doc.SignonRsText("NewPswd");
                ExpireDt = doc.SignonRsText("ExpireDt");

                CommMsg_StatusCode = doc.CommMsgStatusText("StatusCode");
                CommMsg_StatusDesc = doc.CommMsgStatusText("StatusDesc");
                CommMsg_StatusDetail = doc.CommMsgStatusText("StatusDetail");
                Severity = doc.CommMsgStatusText("Severity");
            }
        }

        private static readonly string Template = GenerateTemplate();
        private static string GenerateTemplate()
        {
            StringBuilder response = new StringBuilder("<LandBankML xmlns=\"urn:schema-bluestar-com:multichannel\" version=\"1.0\">").AppendLine();
            {
                response.Append("<SignonRs>").AppendLine();
                {
                    response.Append("<Status>").AppendLine();
                    {
                        response.Append("<StatusCode>{0}</StatusCode>").AppendLine();
                        response.Append("<Severity>Info</Severity>").AppendLine();
                        response.Append("<StatusDesc>{1}</StatusDesc>").AppendLine();
                        response.Append("<StatusDetail>{2}</StatusDetail>").AppendLine();
                    }
                    response.Append("</Status>").AppendLine();

                    response.Append("<CustId>").AppendLine();
                    {
                        response.Append("<SPName>{3}</SPName>").AppendLine();
                        response.Append("<CustLoginId>{4}</CustLoginId>").AppendLine();
                    }
                    response.Append("</CustId>").AppendLine();

                    response.Append("<ClientDt>{5}</ClientDt>").AppendLine();
                    response.Append("<ServerDt>{6}</ServerDt>").AppendLine();
                    response.Append("<NewPswd>{7}</NewPswd>").AppendLine();
                    response.Append("<ExpireDt>{8}</ExpireDt>").AppendLine();
                }
                response.Append("</SignonRs>").AppendLine();


                response.Append("<CommMsg>").AppendLine();
                {
                    response.Append("<Status>").AppendLine();
                    {
                        response.Append("<StatusCode>{9}</StatusCode>").AppendLine();
                        response.Append("<Severity>{10}</Severity>").AppendLine();
                        response.Append("<StatusDesc>{11}</StatusDesc>").AppendLine();
                        response.Append("<StatusDetail>{12}</StatusDetail>").AppendLine();
                    }
                    response.Append("</Status>").AppendLine();
                }
                response.Append("</CommMsg>").AppendLine();
            }
            response.Append("</LandBankML>").AppendLine();

            return response.ToString();
        }
    }
}
