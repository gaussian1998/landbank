﻿using Library.Servant.Servant.EAI.Extensions;
using System.Text;
using System.Xml;

namespace Library.Servant.Servant.EAI.ResponseModels
{
    public class CommonResponseResult
    {
        public string CH_SPName { get; set; }
        public string CH_CustLoginId { get; set; }
        public string Channel_Exist { get; set; }
        public string Channel_ACTIVES { get; set; }
        public string Channel_IP { get; set; }

        public string Pswd_EAI { get; set; }
        public string Pswd_Original { get; set; }
        public string Pswd_Check { get; set; }

        public string EAIDt { get; set; }
        public string ClientDt { get; set; }
        public string STATUS { get; set; }

        public string SPName_CustId { get; set; }
        public string SPName { get; set; }
        public string SPName_IsNull { get; set; }
        public string SPName_IsSame { get; set; }
        public string SPName_IsLogin { get; set; }

        public string MsgId { get; set; }
        public string MsgId_Check { get; set; }
        public string HSM_Check { get; set; }
        public string Chinese_Check { get; set; }

        public string XML
        {
            get
            {
                return string.Format( 
                    Template, 
                    CH_SPName, CH_CustLoginId, Channel_Exist, Channel_ACTIVES, Channel_IP,
                    Pswd_EAI, Pswd_Original, Pswd_Check,
                    EAIDt, ClientDt, STATUS,
                    SPName_CustId, SPName, SPName_IsNull, SPName_IsSame, SPName_IsLogin,
                    MsgId, MsgId_Check, HSM_Check, Chinese_Check);
            }
            set
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(value.Replace("xmlns", "skip"));

                CH_SPName = doc.ChannelText("CH_SPName");
                CH_CustLoginId = doc.ChannelText("CH_CustLoginId");
                Channel_Exist = doc.ChannelText("Channel_Exist");
                Channel_ACTIVES = doc.ChannelText("Channel_ACTIVES");
                Channel_IP = doc.ChannelText("Channel_IP");

                Pswd_EAI = doc.PSWDText("Pswd_EAI");
                Pswd_Original = doc.PSWDText("Pswd_Original");
                Pswd_Check = doc.PSWDText("Pswd_Check");

                EAIDt = doc.ClientDtText("EAIDt");
                ClientDt = doc.ClientDtText("ClientDt");
                STATUS = doc.ClientDtText("STATUS");

                SPName_CustId = doc.SPNameText("SPName_CustId");
                SPName = doc.SPNameText("SPName");
                SPName_IsNull = doc.SPNameText("SPName_IsNull");
                SPName_IsSame = doc.SPNameText("SPName_IsSame");
                SPName_IsLogin = doc.SPNameText("SPName_IsLogin");

                MsgId = doc.MsgIdText("MsgId");
                MsgId_Check = doc.MsgIdText("MsgId_Check");
                HSM_Check = doc.MsgIdText("HSM_Check");
                Chinese_Check = doc.MsgIdText("Chinese_Check");
            }
        }

        private static readonly string Template = GenerateTemplate();
        private static string GenerateTemplate()
        {
            StringBuilder response = new StringBuilder("<?xml version=\"1.0\" encoding=\"utf-16\"?>\n");
            response.Append("<LandBankML>").AppendLine();
            {
                response.Append("<SignonRs>").AppendLine();
                {
                    response.Append("<Channel>").AppendLine();
                    {
                        response.Append("<CH_SPName>{0}</CH_SPName>").AppendLine();
                        response.Append("<CH_CustLoginId>{1}</CH_CustLoginId>").AppendLine();
                        response.Append("<Channel_Exist>{2}</Channel_Exist>").AppendLine();
                        response.Append("<Channel_ACTIVES>{3}</Channel_ACTIVES>").AppendLine();
                        response.Append("<Channel_IP>{4}</Channel_IP>").AppendLine();
                    }
                    response.Append("</Channel>").AppendLine();

                    response.Append("<PSWD>").AppendLine();
                    {
                        response.Append("<Pswd_EAI>{5}</Pswd_EAI>").AppendLine();
                        response.Append("<Pswd_Original>{6}</Pswd_Original>").AppendLine();
                        response.Append("<Pswd_Check>{7}</Pswd_Check>").AppendLine();
                    }
                    response.Append("</PSWD>").AppendLine();

                    response.Append("<ClientDt>").AppendLine();
                    {
                        response.Append("<EAIDt>{8}</EAIDt>").AppendLine();
                        response.Append("<ClientDt>{9}</ClientDt>").AppendLine();
                        response.Append("<STATUS>{10}</STATUS>").AppendLine();
                    }
                    response.Append("</ClientDt>").AppendLine();

                    response.Append("<SPName>").AppendLine();
                    {
                        response.Append("<SPName_CustId>{11}</SPName_CustId>").AppendLine();
                        response.Append("<SPName>{12}</SPName>").AppendLine();
                        response.Append("<SPName_IsNull>{13}</SPName_IsNull>").AppendLine();
                        response.Append("<SPName_IsSame>{14}</SPName_IsSame>").AppendLine();
                        response.Append("<SPName_IsLogin>{15}</SPName_IsLogin>").AppendLine();
                    }
                    response.Append("</SPName>").AppendLine();

                    response.Append("<MsgId>").AppendLine();
                    {
                        response.Append("<SPName />").AppendLine();
                        response.Append("<MsgId>{16}</MsgId>").AppendLine();
                        response.Append("<MsgId_Check>{17}</MsgId_Check>").AppendLine();
                        response.Append("<HSM_Check>{18}</HSM_Check>").AppendLine();
                        response.Append("<Chinese_Check >{19}</Chinese_Check>").AppendLine();
                    }
                    response.Append("</MsgId>").AppendLine();
                }
                response.Append("</SignonRs>").AppendLine();
            }
            response.Append("</LandBankML>").AppendLine();

            return response.ToString();
        }
    }
}
