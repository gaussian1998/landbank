﻿using System.Text;

namespace Library.Servant.Servant.EAI.RequestModels
{
    public partial class CommMsgRequestModel
    {
        public string SPName { get; set; }
        public string CustLoginId { get; set; }
        public string Pswd { get; set; }
        public string ClientDt { get; set; }
        public string MsgId { get; set; }
        public string KINBR { get; set; }
        public string TRMSEQ { get; set; }
        public string RqUID { get; set; }
        public string TXTNO { get; set; }
        public string TLRNO { get; set; }
        public string APTYPE { get; set; }
        public string TXNO { get; set; }
        public string STXNO { get; set; }
        public string PTYPE { get; set; }
        public string TXTYPE { get; set; }
        public string CRDB { get; set; }
        public string HCODE { get; set; }
        public string YCODE { get; set; }
        public string CURCD { get; set; }
        public string TXAMT { get; set; }
        public string MRKEY { get; set; }
        public string TITA_TXDAY { get; set; }
        public string TITA_TXTIME { get; set; }
        public string ISDAY { get; set; }
        public string CNT { get; set; }
        public string IBSRNUM1 { get; set; }
        public string IDSCPT1 { get; set; }
        public string ITXAMT1 { get; set; }
        public string DEPNO { get; set; }
        public string ACCNO { get; set; }
        public string TRMTYP { get; set; }

        public string XML
        {
            get
            {
                return  string.Format( Template, 
                    SPName, CustLoginId, Pswd,  ClientDt, 
                    MsgId, KINBR, TRMSEQ, RqUID, 
                    TXTNO, TLRNO, APTYPE, TXNO,
                    STXNO, PTYPE, TXTYPE, CRDB,
                    HCODE, YCODE, CURCD, TXAMT,
                    MRKEY, TITA_TXDAY, TITA_TXTIME, ISDAY,
                    CNT, IBSRNUM1, IDSCPT1, ITXAMT1, 
                    DEPNO, ACCNO, TRMTYP);
            }
        }
    }



    /// <summary>
    ///  Private
    /// </summary>
    public partial class CommMsgRequestModel
    {
        private static readonly string Template = GenerateTemplate();
        private static string GenerateTemplate()
        {
            StringBuilder response = new StringBuilder();
            response.Append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            response.Append("<LandBankML xmlns =\"urn:schema-bluestar-com:multichannel\" version=\"1.0\">").AppendLine();
            {
                response.Append("<SignonRq>").AppendLine();
                {
                    response.Append("<SignonPswd>").AppendLine();
                    {
                        response.Append("<CustId>").AppendLine();
                        {
                            response.Append("<SPName>{0}</SPName>").AppendLine();
                            response.Append("<CustLoginId>{1}</CustLoginId>").AppendLine();
                        }
                        response.Append("</CustId>").AppendLine();

                        response.Append("<CustPswd>").AppendLine();
                        {
                            response.Append("<CryptType>DES1</CryptType>").AppendLine();
                            response.Append("<Pswd>{2}</Pswd>").AppendLine();
                        }
                        response.Append("</CustPswd>").AppendLine();
                    }
                    response.Append("</SignonPswd>").AppendLine();

                    response.Append("<ClientDt>{3}</ClientDt>").AppendLine();
                }
                response.Append("</SignonRq>").AppendLine();


                response.Append("<CommMsg>").AppendLine();
                {
                    response.Append("<SPName>{0}</SPName>").AppendLine();//服務提供者名稱,照申請書填AS
                    response.Append("<MsgId>{4}</MsgId>").AppendLine();//交易識別碼,目前有C1100,J1400,A1012_2
                    response.Append("<KINBR>{5}</KINBR>").AppendLine();//輸入行,什麼鳥?KINBR x TXTNO不可重複送
                    response.Append("<TRMSEQ>{6}</TRMSEQ>").AppendLine();//櫃台機序號,照申請書填1543不通
                    response.Append("<TRMTYP>{30}</TRMTYP>").AppendLine();//櫃台機種類,什麼鳥?照申請書填
                    response.Append("<TTSKID>{0}</TTSKID>").AppendLine();//端末 TASK ID,李建民說填AS
                    response.Append("<AllRecCtrlIn>N</AllRecCtrlIn>").AppendLine();//交易折返記號,什麼鳥?
                    response.Append("<RqUID>{7}</RqUID>").AppendLine();//交易訊息序號,什麼鳥?先填UUID
                    response.Append("<TXTNO>{8}</TXTNO>").AppendLine();//交易傳輸編號,KINBR x TXTNO不可重複送,自行編碼,所有交易共用
                    response.Append("<XFG>~</XFG>").AppendLine();//轉換標記,什麼鳥?
                    response.Append("<TLRNO>{9}</TLRNO>").AppendLine();//櫃員代號
                    response.Append("<APTYPE>{10}</APTYPE>").AppendLine();//業務大類
                    response.Append("<TXNO>{11}</TXNO>").AppendLine();//交易代號
                    response.Append("<STXNO>{12}</STXNO>").AppendLine();//次交易代號
                    response.Append("<PTYPE>{13}</PTYPE> ").AppendLine();//處理型態
                    response.Append("<TXTYPE>{14}</TXTYPE>").AppendLine();//帳務別
                    response.Append("<HCODE>{16}</HCODE>").AppendLine();//更正記號
                    response.Append("<YCODE>{17}</YCODE>").AppendLine();//補送記號
                    response.Append("<CURCD>{18}</CURCD>").AppendLine();//幣別
                    response.Append("<TXAMT>{19}</TXAMT>").AppendLine();//交易金額
                    response.Append("<DSCPT>AS</DSCPT>").AppendLine();//交易摘要
                    response.Append("<MRKEY>{20}</MRKEY>").AppendLine();//帳號
                    response.Append("<TITA_UNINO/>").AppendLine();//歸戶編號
                    response.Append("<TITA_UIDERR/>").AppendLine();//歸戶編號錯誤記號
                    response.Append("<ECKIN/>").AppendLine();//原輸入行別
                    response.Append("<ECTRMS/>").AppendLine();//原櫃台機序號
                    response.Append("<ECTNO/>").AppendLine();//原交易傳輸編號
                     response.Append("<SPCD/>").AppendLine();//主管許可記號
                    response.Append("<SPID/>").AppendLine();//主管代號
                    response.Append("<NBCD/>").AppendLine();//無摺記號
                    response.Append("<TITA_ENTDD/>").AppendLine();//作帳日
                    response.Append("<TITA_TXDAY>{21}</TITA_TXDAY>").AppendLine();//實際交易日
                    response.Append("<TITA_TXTIME>{22}</TITA_TXTIME>").AppendLine();//實際交易時間
                    response.Append("<ISDAY>{23}</ISDAY>").AppendLine();//??????????啥鳥?
                    response.Append("<CNT>{24}</CNT>").AppendLine();//??????????啥鳥?
                    response.Append("<IBSRNUM1>{25}</IBSRNUM1>").AppendLine();//不能重複,重復會回返已有該筆交易紀錄
                    response.Append("<IDSCPT1>{26}</IDSCPT1>").AppendLine();//??????????啥鳥?
                    response.Append("<ITXAMT1>{27}</ITXAMT1>").AppendLine();//??????????啥鳥?
                    response.Append("<MTTPSEQ/>").AppendLine();//連動次數
                    response.Append("<COKEY/>").AppendLine();//對方帳號
                    response.Append("<TITA_SECNO/>").AppendLine();//業務別
                    response.Append("<TITA_SEQFG/>").AppendLine();//編列序號記號
                    response.Append("<IBFFG/>").AppendLine();//即時扣帳記號
                    response.Append("<SBCASH/>").AppendLine();//出納代收付記號
                    response.Append("<BATCHNO/>").AppendLine();//批號
                    response.Append("<TITA_OBUFG/>").AppendLine();//OBU記號
                    response.Append("<INBKTXNO/>").AppendLine();//INBK交易代號
                    response.Append("<TITA_SYNC/>").AppendLine();//設備驗證碼
                    response.Append("<TITA_SECTYPE/>").AppendLine();//憑證代號
                    response.Append("<SRFLG/>").AppendLine();//登銷代號
                    response.Append("<WSCNT/>").AppendLine();
                    response.Append("<Detail><DEPNO>{28}</DEPNO><ACCNO>{29}</ACCNO><CRDB>{15}</CRDB><AMT>10500</AMT><MEMO>2017-11-29 測試 </MEMO><FSFLG>2</FSFLG></Detail>");
                }
                response.Append("</CommMsg>").AppendLine();
            }
            response.Append("</LandBankML>").AppendLine();

            return response.ToString();
        }
    }
}
