﻿using System.Text;

namespace Library.Servant.Servant.EAI.RequestModels
{
    public partial class ExchangeKeyRequestModel
    {
        public string SPName { get; set; }
        public string CustLoginId { get; set; }
        public string OldPswd { get; set; }
        public string ClientDt { get; set; }

        public string XML
        {
            get
            {
                return string.Format( Template, SPName, CustLoginId, OldPswd, ClientDt);
            }
        }
    }




    /// <summary>
    ///  Private
    /// </summary>
    public partial class ExchangeKeyRequestModel
    {
        private static readonly string Template = GenerateTemplate();
        private static string GenerateTemplate()
        {
            StringBuilder response = new StringBuilder();
            response.Append("<LandBankML xmlns=\"urn:schema-bluestar-com:multichannel\" version=\"1.0\" >").AppendLine();
            {
                response.Append("<SignonRq>").AppendLine();
                {
                    response.Append("<SignonChgPswd>").AppendLine();
                    {
                        response.Append("<CustId>").AppendLine();
                        {
                            response.Append("<SPName>{0}</SPName>").AppendLine();
                            response.Append("<CustLoginId>{1}</CustLoginId>").AppendLine();
                        }
                        response.Append("</CustId>").AppendLine();

                        response.Append("<CustChgPswd>").AppendLine();
                        {
                            response.Append("<CryptType>DES1</CryptType>").AppendLine();
                            response.Append("<OldPswd>{2}</OldPswd>").AppendLine();
                            response.Append("<NewPswd />").AppendLine();
                        }
                        response.Append("</CustChgPswd>").AppendLine();
                    }
                    response.Append("</SignonChgPswd>").AppendLine();
                    response.Append("<ClientDt>{3}</ClientDt>").AppendLine();
                }
                response.Append("</SignonRq>").AppendLine();
            }
            response.Append("</LandBankML>").AppendLine();

            return response.ToString();
        }
    }
}
