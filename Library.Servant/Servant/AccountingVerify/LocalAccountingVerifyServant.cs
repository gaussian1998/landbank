﻿using System.Linq;
using System.Collections.Generic;
using LandBankEntity.ActiveRecords;
using Library.Common.Models;
using Library.Entity.Edmx;
using Library.Servant.Servant.AccountingVerify.Models;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.CodeTable;
using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Constants;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.AccountingVerify
{
    public class LocalAccountingVerifyServant : IAccountingVerifyServant
    {
        public TradeOptions TradeOptions()
        {
            return CodeTable.Models.TradeOptions.Query();
        }


        public IndexResult Index(SearchModel search)
        {
            return new IndexResult {

                Items = Associate( Items(search) )
            };
        }

        public DetailsResult Details(DetailsModel model)
        {
            var branchs = LocalCodeTableServant.BranchDictionary();
            var departments = LocalCodeTableServant.DepartmentDictionary();

            return new DetailsResult
            {
                Items = Details( model.Account, branchs, departments)
            }; 
        }

        public void Verify(VerifyModel model)
        {
            string formNo = LocalApprovalTodoServant.Create(model.UserID, "A502", "M803", "AccountVerify");
            Approval( model, formNo);
        }

        private static List<AccountEntryDetailResult> Details(string Account, Dictionary<string, string> branchs, Dictionary<string, string> departments)
        {
            return AS_GL_Acc_Months_Records.FindModify<AccountEntryDetailResult>(m => m.Account == Account, item =>
            {
                item.Branchs = branchs;
                item.Departments = departments;
                return item;
            });
        }

        private static PageList<AccountEntryDetailResult> Items(SearchModel search)
        {
            bool isSkipTradeKind = string.IsNullOrWhiteSpace(search.TradeKind);

            return AS_GL_Acc_Months_Records.GroupList(
                search.Page,
                search.PageSize,
                m => (isSkipTradeKind || m.Trade_Type == search.TradeKind) && m.Status != AccountStateServant.Approvaling,
                m => new { m.Account, m.DB_CR },
                group => new AccountEntryDetailResult
                {
                    Department = group.FirstOrDefault().Department,
                    Branch_Code = group.FirstOrDefault().Branch_Code,
                    Account = group.FirstOrDefault().Account,
                    Account_Name = group.FirstOrDefault().Account_Name,
                    Amount = group.Sum(m => m.Amount),
                    Currency = group.FirstOrDefault().Currency,
                    DB_CR = group.FirstOrDefault().DB_CR,
                    Last_Updated_Time = group.FirstOrDefault().Last_Updated_Time,
                    TotalNumber = group.Count()
                }
            );
        }

        private static PageList<AccountEntryDetailResult> Associate(PageList<AccountEntryDetailResult> result)
        {
            var branchs = LocalCodeTableServant.BranchDictionary();
            var departments = LocalCodeTableServant.DepartmentDictionary();

            result.Items.ForEach( item => {

                item.Branchs = branchs;
                item.Departments = departments;
            });
            return result;
        }

        private static void Approval(VerifyModel model, string formNo)
        {
            using (AS_LandBankEntities db = new AS_LandBankEntities())
            {
                if ( StartFlow(db, model, formNo) && UpdateStatus(db, formNo) )
                        db.SaveChanges();
            }
        }

        private static bool StartFlow(AS_LandBankEntities db, VerifyModel model, string formNo)
        {
            return LocalApprovalTodoServant.StartT( db, model.UserID, formNo, "TestData2");
        }

        private static bool UpdateStatus(AS_LandBankEntities db, string formNo)
        {
            return GLAccMonthsRecord.UpdateT(
                         db,
                         entity => entity.Status == AccountStateServant.New,
                         entity =>
                         {
                             entity.Trans_Seq = formNo;
                             entity.Status = AccountStateServant.Approvaling;
                         }) != 0;
        }
    }
}
