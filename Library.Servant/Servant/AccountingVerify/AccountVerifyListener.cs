﻿using LandBankEntity.ActiveRecords;
using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servant.Common.Constants;

namespace Library.Servant.Servant.AccountingVerify
{
    public class AccountVerifyListener : ExampleListener
    {
        public override bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            GLAccDeprnRecord.UpdateT(context, m => m.Trans_Seq == formNo, entity =>
            {
                entity.Trans_Seq = formNo;
                entity.Status = AccountStateServant.PrepareEAI;
            });
            return true;
        }
    }
}
