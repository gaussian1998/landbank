﻿using Library.Common.Models;
using Library.Servant.Servant.Common.Models;


namespace Library.Servant.Servant.AccountingVerify.Models
{
    public class IndexResult
    {
        public PageList<AccountEntryDetailResult> Items { get; set; }
    }
}
