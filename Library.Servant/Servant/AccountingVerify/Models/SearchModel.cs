﻿namespace Library.Servant.Servant.AccountingVerify.Models
{
    public class SearchModel
    {
        public int UserID { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string TradeKind { get; set; }
        public string TradeDistinguish { get; set; }
        public string TransSeq { get; set; }
        public string MasterNo { get; set; }
        public string AssetNumber { get; set; }
    }
}
