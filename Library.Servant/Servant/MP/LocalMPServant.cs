﻿using System;
using System.Linq;
using Library.Servant.Servant.MP.MPModels;
using System.Transactions;
using Library.Entity.Edmx;
using Library.Servant.Repository.MP;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace Library.Servant.Servant.MP
{
    public class LocalMPServant : IMPServant
    {
        public IndexModel Index(SearchModel condition)
        {
            IndexModel result = null;

            var query = MPGeneral.GetIndex(condition);

            if(query != null)
            {
                result = new IndexModel();
                result.TotalAmount = query.Count();
                result.Items = query;
                result.condition = condition;

                //分頁
                if (condition.Page != null)
                {
                    //選取該頁之資訊
                    result.Items = query.OrderByDescending(m => m.Create_Time).Skip((condition.Page.Value - 1) * Convert.ToInt32(condition.PageSize)).Take(Convert.ToInt32(condition.PageSize));
                }
            }
            
            return result;
        }

        public ImportIndexModel ImportIndex(ImportSearchModel condition)
        {
            ImportIndexModel result = new ImportIndexModel();
            using (var db = new AS_LandBankEntities())
            {
                var query = from m in db.AS_Assets_MP_Import
                            join r in
                            (
                               from mr in db.AS_Assets_MP_ImportHIS
                               group mr by new { mr.Import_Num } into mrg
                               select new
                               {
                                   Import_Num = mrg.Key.Import_Num,
                                   Count = mrg.Count()
                               }
                            ) on m.Import_Num equals r.Import_Num into mhtemp
                            from r in mhtemp.DefaultIfEmpty()
                            join u in db.AS_Users on m.Created_By equals u.ID.ToString() into tu
                            from u in tu.DefaultIfEmpty()
                            join o in db.AS_Keep_Position on m.Office_Branch equals o.Keep_Position_Code into to
                            from o in to.DefaultIfEmpty()
                            join f in
                            (
                               from ff in db.AS_VW_Code_Table
                               where ff.Class == "MPImportStatus"
                               select ff
                            ) on m.Status equals f.Code_ID into tf
                            from f in tf.DefaultIfEmpty()
                            select new { m, r , o , u , f };

                if (!string.IsNullOrEmpty(condition.ImportNum))
                {
                    query = query.Where(m => m.m.Import_Num == condition.ImportNum);
                }

                if(!string.IsNullOrEmpty(condition.ImportNumBegin))
                {
                    query = query.Where(m => string.Compare(m.m.Import_Num,condition.ImportNum) >= 0);
                }

                if (!string.IsNullOrEmpty(condition.ImportNumEnd))
                {
                    query = query.Where(m => string.Compare(m.m.Import_Num, condition.ImportNumEnd) <= 0);
                }

                if (condition.ImportBeginDate != null)
                {
                    query = query.Where(m => m.m.Create_Time>=condition.ImportBeginDate);
                }

                if (condition.ImportEndDate != null)
                {
                    query = query.Where(m => m.m.Create_Time <= condition.ImportEndDate);
                }

                if (!string.IsNullOrEmpty(condition.PONumberBegin))
                {
                    query = query.Where(m => string.Compare(m.m.PO_Number, condition.PONumberBegin) <= 0);
                }

                if (!string.IsNullOrEmpty(condition.PONumberEnd))
                {
                    query = query.Where(m => string.Compare(m.m.PO_Number, condition.PONumberEnd) >= 0);
                }

                if(condition.ImportYear != null)
                {
                    query = query.Where(m => m.m.Import_Year == condition.ImportYear);
                }

                if (condition.ImportMonth != null)
                {
                    query = query.Where(m => m.m.Import_Month == condition.ImportMonth);
                }

                if (!string.IsNullOrEmpty(condition.Status))
                {
                    string[] Status = condition.Status.Split(',');
                    query = query.Where(m => Status.Contains(m.m.Status));
                }

                if (condition.LastRecord)
                {
                    query = from q in query
                            join mx in
                            (
                                from a in db.AS_Assets_MP_Import
                                where a.Status == "1"
                                group a by a.Import_Num into ag
                                select new
                                {
                                    Import_Num = ag.Key,
                                    ID = ag.Max(o => o.ID)
                                }
                            ) on new { q.m.Import_Num, q.m.ID } equals new { mx.Import_Num, mx.ID }
                            select q; 
                }

                result.Items = query.ToList().OrderByDescending(m => m.m.Create_Time).Select(m => MPGeneral.ToMPImportModel(m.m,((m.r != null) ? m.r.Count : 0),m.u,m.o,m.f));
                result.TotalAmount = query.Count();
            }

            return result;
        }

        public ImportReviewIndexModel ImportReviewIndex()
        {
            ImportReviewIndexModel result = new ImportReviewIndexModel();

            using (var db = new AS_LandBankEntities())
            {
                var query = (from r in db.AS_Assets_MP_ImportReview
                             group r by r.ImportReviewID into rg
                             select new
                             {
                                 IRID = rg.Key
                             }).ToList();

                result.TotalAmount = query.Count();
                result.Items = query.Select(o => new MPImportReviewModel()
                {
                     IRID = o.IRID
                });
            }

            return result;
        }


        public IEnumerable<AS_VW_MP_ImportHis> GetImportData(int ImportID)
        {
            IEnumerable<AS_VW_MP_ImportHis> result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = (from m in db.AS_VW_MP_ImportHis
                            where m.ImportID == ImportID
                            select m).ToList();
                            
                if (query.Count() > 0)
                {
                    result = query;
                }
            }
            return result;
        }

        public AssetsIndexModel GetImportAssets(int ImportID)
        {
            AssetsIndexModel result = new AssetsIndexModel();
            using (var db = new AS_LandBankEntities())
            {
                var query = (from m in db.AS_Assets_MP_ImportHIS
                            where m.ImportID == ImportID
                            select m).ToList();

                if (query.Count() > 0)
                {
                    result.TotalAmount = query.Count();
                    result.Items = query.Select(o => MPGeneral.ToAssetModel(o));
                }
            }

            return result;
        }

        public MPModel GetDetail(string TRXHeaderID)
        {
            MPModel result = MPGeneral.GetDetail(TRXHeaderID);
            return result;
        }

        public MPImportReviewModel GetImportReviewDetail(string IRID)
        {
            MPImportReviewModel result = new MPImportReviewModel();
            using (var db = new AS_LandBankEntities())
            {
                var query = (from m in db.AS_Assets_MP_ImportReview
                            join i in db.AS_Assets_MP_Import on m.ImportID equals i.ID
                            join t in db.AS_Assets_MP_HIS on i.Import_Num equals t.Import_Num
                            join h in db.AS_Assets_MP_Header on t.TRX_Header_ID equals h.TRX_Header_ID
                            where m.ImportReviewID == IRID
                            select new { m ,i ,t , h }).ToList();

                result.Imports = query.Select(o => o.i).Distinct().Select(o=> MPGeneral.ToMPImportModel(o));
                result.MPs = query.Select(o=>o.h).Distinct().Select(o=>new MPModel()
                {
                     Header = MPGeneral.ToMPHeaderModel(o,null,null,null,null,(query.Select(t => t.t).Where(t => t.TRX_Header_ID == o.TRX_Header_ID).Count())),
                     Assets = query.Select(t=>t.t).Where(t=>t.TRX_Header_ID == o.TRX_Header_ID).Select(t=>MPGeneral.ToAssetModel(t)),
                     Assigned_Branch = query.Select(t => t.t).Where(t => t.TRX_Header_ID == o.TRX_Header_ID).First().Assigned_Branch,
                     HandledBy = "",
                     Email = "",
                     Phone = ""
                });
            }

            return result;
        }

        public AssetHandleResult Create_Asset(MPModel model)
        {       
            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        //Header
                        AS_Assets_MP_Header Header = MPGeneral.ToAS_Assets_MP_Header(model.Header);
                        if (!db.AS_Assets_MP_Header.Any(h => h.TRX_Header_ID == Header.TRX_Header_ID))
                        {
                            db.Entry(Header).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }

                        foreach (var asset in model.Assets)
                        {
                            //MP_His
                            AS_Assets_MP_HIS His = MPGeneral.ToAS_Assets_MP_HIS(Header.TRX_Header_ID, asset);
                            if(string.IsNullOrEmpty(His.Asset_Number))
                            {
                                His.Asset_Number = CreateAssetNumber(asset.type1,asset.type2,asset.type3);
                            }
                            //Deprn
                            His.Deprn_Reserve = CreateDeprn(db, asset, model.Header);
                            db.Entry(His).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }
                    }

                    trans.Complete();
                }
            }   
            catch(Exception ex)
            {

            } 
            return new AssetHandleResult() { TRX_Header_ID = model.Header.TRX_Header_ID };
        }

        public AssetHandleResult CreateToMP(string TRXHeaderID)
        {
            AssetHandleResult result = new AssetHandleResult(){};

            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {

                        AS_Assets_MP_Header header = db.AS_Assets_MP_Header.First(o=>o.TRX_Header_ID == TRXHeaderID);
                        header.Last_Updated_Time = DateTime.Now;
                        header.Flow_Status = "2";
                        db.Entry(header).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();


                        var query = from m in db.AS_Assets_MP_HIS
                                    where m.TRX_Header_ID == TRXHeaderID
                                    select m;

                        foreach (var item in query)
                        {
                            int ID = 1;
                            //抓AS_Assets_MP ID的(Max + 1)
                            if (db.AS_Assets_MP.Count() > 0)
                            {
                               ID = db.AS_Assets_MP.Max(o => o.ID) + 1;
                            }

                            AS_Assets_MP mp = MPGeneral.ToAS_Assets_MPFromAS_Assets_MP_HIS(item);
                            mp.ID = ID;
                            mp.Source = header.Source;
                            mp.Department = header.OriOffice_Branch;
                            mp.Office_Branch = header.Office_Branch;
                            mp.Date_Placed_In_Service = DateTime.Now;
                            db.Entry(mp).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }
                    }

                    trans.Complete();
                }
            }
            catch(Exception ex)
            {

            }

            return result;
        }

        public AssetHandleResult CreateImport(MPImportModel import, IEnumerable<AssetModel> AssetList)
        {
            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        AS_Assets_MP_Import Import = MPGeneral.ToAS_Asset_MP_Import(import);
                        //Import
                        db.Entry(Import).State = System.Data.Entity.EntityState.Added;
                        db.SaveChanges();
                        int ImportID = Import.ID;

                        foreach (var asset in AssetList)
                        {
                            asset.Asset_Number = CreateAssetNumber(asset.type1, asset.type2, asset.type3);
                            asset.ImportNum = import.Import_Num;
                            //MPImport_His
                            AS_Assets_MP_ImportHIS ImportHis = MPGeneral.ToAS_Assets_MP_ImportHIS(ImportID,asset);
                            db.Entry(ImportHis).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }
                    }

                    trans.Complete();
                }
            }
            catch (Exception ex)
            {

            }
            return new AssetHandleResult() { ImportNum = import.Import_Num };
        }

        public AssetHandleResult CreateImportReview(string IRID, string[] ImportList, MPHeaderModel Header)
        {
            if(string.IsNullOrEmpty(IRID))
            {
                IRID = CreateImportReviewID();
            }

            try
            {
                List<AssetModel> Assets = new List<AssetModel>();

                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        var imports = db.AS_Assets_MP_ImportReview.Where(o => o.ImportReviewID == IRID).Select(o=>o.ImportID);

                        foreach(string importID in ImportList)
                        {
                            int id = Convert.ToInt32(importID);

                            if(!imports.ToList().Contains(id))
                            {
                                //ImportReview
                                AS_Assets_MP_ImportReview ImportReview = new AS_Assets_MP_ImportReview()
                                {
                                    ImportReviewID = IRID,
                                    ImportID = Convert.ToInt32(importID),
                                    Created_By = "",
                                    Create_Time = DateTime.Now,
                                    Last_Updated_By = ""
                                };
                                db.Entry(ImportReview).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();

                                //撈批次下的資產
                                var query = from i in db.AS_Assets_MP_ImportHIS
                                            where i.ImportID == id
                                            select i;

                                //撈批號
                                string ImportNum = db.AS_Assets_MP_Import.Where(o => o.ID == id).First().Import_Num;

                                foreach (var item in query)
                                {
                                    item.Import_Num = ImportNum;
                                    Assets.Add(MPGeneral.ToAssetModel(item));
                                }

                            }
                        }

                        //Group by Assets
                        List<AssetModel> GroupAsset = (from a in Assets.ToList()
                                                      group a by new { a.Assigned_Branch } into ag
                                                      select new AssetModel()
                                                      {
                                                          Assigned_Branch = ag.Key.Assigned_Branch
                                                      }).ToList();

                        
                        for (int g = 0; g < GroupAsset.Count; g++)
                        {
                            string TrxID = CreateTRXID("A210");
                            //Insert MP_Header
                            AS_Assets_MP_Header header = MPGeneral.ToAS_Assets_MP_Header(Header);
                            header.TRX_Header_ID = TrxID;
                            db.Entry(header).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();

                            //Insert MP_His
                            var ats = Assets.Where(a => a.Assigned_Branch == GroupAsset[g].Assigned_Branch);

                            foreach(var item in ats)
                            {
                                AS_Assets_MP_HIS His = MPGeneral.ToAS_Assets_MP_HIS(TrxID, item);
                                db.Entry(His).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                        }
                    }

                    trans.Complete();
                }
            }
            catch (Exception ex)
            {

            }
            return new AssetHandleResult() { ImportReviewID = IRID };
        }

        private decimal CreateDeprn(AS_LandBankEntities db,AssetModel asset,MPHeaderModel header)
        {
            decimal Deprn_Cost = 0;
            if(asset.Life_Years != 0)
            {
                db.AS_Assets_MP_Deprn.RemoveRange(db.AS_Assets_MP_Deprn.Where(m => m.TRX_Header_ID == header.TRX_Header_ID && m.Asset_Number == asset.Asset_Number));
                db.SaveChanges();

                //折舊
                int month = asset.Life_Years * 12;

                Deprn_Cost = Math.Floor(asset.Assets_Fixed_Cost / month);
                decimal LastDeprn_Cost = asset.Assets_Fixed_Cost - (month - 1) * Deprn_Cost;

                for (int i = 1; i <= month; i++)
                {
                    //MP_Deprn
                    AS_Assets_MP_Deprn Deprn = new AS_Assets_MP_Deprn()
                    {
                        TRX_Header_ID = header.TRX_Header_ID,
                        Asset_Number = asset.Asset_Number,
                        Life_Month = i,
                        Deprn_Source = header.Source,
                        Deprn_Cost = (i != month) ? Deprn_Cost : LastDeprn_Cost,
                        Adjusted_Cost = (i != month) ? asset.Assets_Fixed_Cost - (i * Deprn_Cost) : asset.Assets_Fixed_Cost - (((i - 1) * Deprn_Cost) + LastDeprn_Cost),
                        Created_By = "",
                        Create_Time = DateTime.Now,
                        Last_Updated_By = ""
                    };
                    db.Entry(Deprn).State = System.Data.Entity.EntityState.Added;
                    db.SaveChanges();
                }
            }
            return Deprn_Cost;
        }

        public AssetHandleResult Update_Asset(MPModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (var asset in model.Assets)
                    {
                        //MP_His Update 
                        AS_Assets_MP_HIS His = db.AS_Assets_MP_HIS.FirstOrDefault(m=>m.ID == asset.ID);
                        
                        His.Assets_Parent_Number = asset.Assets_Parent_Number;
                        His.IsOversea = asset.IsOversea;
                        His.Asset_Category_Code = asset.Asset_Category_Code;
                        His.Assets_Name = asset.Assets_Name;
                        His.Assets_Alias = asset.Assets_Alias;
                        His.Asset_Number = asset.Asset_Number;
                        His.Assets_Original_ID = asset.Assets_Original_ID;
                        His.Life_Years = asset.Life_Years;
                        His.Assets_Category_ID = asset.Assets_Category_ID;
                        His.Deprn_Method_Code = asset.Deprn_Method_Code;
                        His.Date_Placed_In_Service = asset.Date_Placed_In_Service;
                        His.Assets_Unit = asset.Assets_Unit;
                        His.Location_Disp = asset.Location_Disp;
                        His.Assigned_Branch = asset.Assigned_Branch;
                        His.Assigned_ID = asset.Assigned_ID;
                        His.PO_Number = asset.PO_Number;
                        His.PO_Description = asset.PO_Description;
                        His.Model_Number = asset.Model_Number;
                        His.Transaction_Date = asset.Transaction_Date;
                        His.Assets_Fixed_Cost = asset.Assets_Fixed_Cost;
                        //Deprn
                        His.Deprn_Reserve = CreateDeprn(db, asset, model.Header);
                        His.Salvage_Value = asset.Salvage_Value;
                        His.Description = asset.Description;
                        His.Post_Date = asset.Post_Date;
                        His.Last_Updated_By = asset.Last_Updated_By;
                        His.Last_Updated_Time = asset.Last_Updated_Time;

                        db.Entry(His).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                   
                trans.Complete();
            }
            return new AssetHandleResult() { TRX_Header_ID = model.Header.TRX_Header_ID };
        }

        public AssetHandleResult UpdateToMP(string TRXHeaderID)
        {
            AssetHandleResult result = new AssetHandleResult() { };

            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        AS_Assets_MP_Header header = db.AS_Assets_MP_Header.First(o => o.TRX_Header_ID == TRXHeaderID);
                        header.Last_Updated_Time = DateTime.Now;
                        header.Flow_Status = "2";
                        db.Entry(header).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        var query = from m in db.AS_Assets_MP_HIS
                                    where m.TRX_Header_ID == TRXHeaderID
                                    select m;

                        foreach (var his in query)
                        {
                            var asset = db.AS_Assets_MP.FirstOrDefault(o => o.Asset_Number == his.Asset_Number);

                            if(asset != null)
                            {
                                asset.Assets_Parent_Number = his.Assets_Parent_Number;
                                asset.IsOversea = his.IsOversea;
                                asset.Asset_Category_Code = his.Asset_Category_Code;
                                asset.Assets_Name = his.Assets_Name;
                                asset.Assets_Alias = his.Assets_Alias;
                                asset.Assets_Original_ID = his.Assets_Original_ID;
                                asset.Life_Years = his.Life_Years;
                                asset.Assets_Category_ID = his.Assets_Category_ID;
                                asset.Deprn_Method_Code = his.Deprn_Method_Code;
                                asset.Date_Placed_In_Service = his.Date_Placed_In_Service.Value;
                                asset.Assets_Unit = his.Assets_Unit;
                                asset.Location_Disp = his.Location_Disp;
                                asset.Assigned_Branch = his.Assigned_Branch;
                                asset.Assigned_ID = his.Assigned_ID;
                                asset.PO_Number = his.PO_Number;
                                asset.PO_Description = his.PO_Description;
                                asset.Model_Number = his.Model_Number;
                                asset.Transaction_Date = his.Transaction_Date.Value;
                                asset.Assets_Fixed_Cost = his.Assets_Fixed_Cost;
                                asset.Deprn_Reserve = his.Deprn_Reserve;
                                asset.Salvage_Value = his.Salvage_Value;
                                asset.Description = his.Description;
                                asset.Post_Date = his.Post_Date;
                                asset.Last_Updated_By = his.Last_Updated_By;
                                asset.Last_Updated_Time = his.Last_Updated_Time;

                                db.Entry(asset).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }

                    trans.Complete();
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public AssetModel GetAssetDetail(int ID)
        {
            AssetModel asset = null;

            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_MP_HIS AS_MP = (from a in db.AS_Assets_MP_HIS
                                          where a.ID == ID
                                     select a).FirstOrDefault();
                if(AS_MP != null)
                {
                    asset = MPGeneral.ToAssetModel(AS_MP);
                }
            }

            return asset;
        }

        public AssetModel GetAssetMPDetail(int ID)
        {
            AssetModel asset = null;

            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_MP AS_MP = (from a in db.AS_Assets_MP
                                      where a.ID == ID
                                      select a).FirstOrDefault();
                if (AS_MP != null)
                {
                    asset = MPGeneral.ToAssetModel(AS_MP);
                }
            }

            return asset;
        }

        public bool AssetsDeprnUpdate(IEnumerable<AssetModel> assets)
        {
            bool result = true;

            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        foreach (var item in assets)
                        {
                            if (db.AS_Assets_MP.Any(o => o.ID == item.ID))
                            {
                                AS_Assets_MP mp = db.AS_Assets_MP.First(o => o.ID == item.ID);
                                mp.Not_Deprn_Flag = item.Not_Deprn_Flag;
                                mp.Last_Updated_Time = DateTime.Now;
                                db.Entry(mp).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }

                    }

                    trans.Complete();
                }
            }
            catch(Exception ex)
            {
                result = false;
            }
            return result;
        }

        public AssetModel GetImportAssetDetail(int ID)
        {
            AssetModel result = new AssetModel();
            using (var db = new AS_LandBankEntities())
            {
                var query = (from m in db.AS_Assets_MP_ImportHIS
                             where m.ID == ID
                             select m).FirstOrDefault();

                if (query != null)
                {
                    result = MPGeneral.ToAssetModel(query);
                }
            }

            return result;
        }

        public AssetModel GetAssetDetailByAssetNumber(string AssetNumber)
        {
            AssetModel asset = null;

            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_MP AS_MP = (from a in db.AS_Assets_MP
                                      where a.Asset_Number == AssetNumber
                                      select a).FirstOrDefault();
                if (AS_MP != null)
                {
                    asset = MPGeneral.ToAssetModel(AS_MP);
                }
            }

            return asset;
        }

        public MPHeaderModel GetHeader(string AssetNumber, string Type)
        {
            MPHeaderModel result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mr in
                            (
                                from r in db.AS_Assets_MP_HIS
                                where r.Asset_Number == AssetNumber
                                select r
                            )
                            join mh in
                            (
                                from h in db.AS_Assets_MP_Header
                                where h.Transaction_Type == Type
                                select h
                            ) on mr.TRX_Header_ID equals mh.TRX_Header_ID
                            select mh;
                if(query != null)
                {
                    result = query.ToList().Select(o => MPGeneral.ToMPHeaderModel(o,null,null,null,null)).FirstOrDefault();
                }
            }
            return result;
        }

        public AssetsIndexModel SearchAssets(AssetsSearchModel condition)
        {
            AssetsIndexModel result = new AssetsIndexModel();
            using (var db = new AS_LandBankEntities())
            {
                var query = from m in db.AS_Assets_MP
                            join bu in db.AS_Users on m.Created_By equals bu.ID.ToString() into tbu
                            from bu in tbu.DefaultIfEmpty()
                            join bo in db.AS_Keep_Position on m.Assigned_Branch equals bo.Keep_Position_Code into tbo
                            from bo in tbo.DefaultIfEmpty()
                            select new {m , bu , bo };

                if(query != null && query.Count() > 0)
                {
                    if (!string.IsNullOrEmpty(condition.Asset_Category_Code))
                    {
                        query = query.Where(o => o.m.Asset_Category_Code == condition.Asset_Category_Code);
                    }
                    if(!string.IsNullOrEmpty(condition.Assets_Name))
                    {
                        query = query.Where(o => o.m.Assets_Name.Contains(condition.Assets_Name));
                    }
                    if(!string.IsNullOrEmpty(condition.Assets_Alias))
                    {
                        query = query.Where(o => o.m.Assets_Alias.Contains(condition.Assets_Alias));
                    }
                    if (!string.IsNullOrEmpty(condition.Asset_Number))
                    {
                        query = query.Where(o => o.m.Asset_Number == condition.Asset_Number);
                    }
                    if (!string.IsNullOrEmpty(condition.Asset_NumberBegin))
                    {
                        query = query.Where(m => String.Compare(m.m.Asset_Number, condition.Asset_NumberBegin) >= 0);
                    }
                    if (!string.IsNullOrEmpty(condition.Asset_NumberEnd))
                    {
                        query = query.Where(m => String.Compare(m.m.Asset_Number, condition.Asset_NumberEnd) <= 0);
                    }
                    if (!string.IsNullOrEmpty(condition.Assets_Category_ID))
                    {
                        query = query.Where(o => o.m.Assets_Category_ID == condition.Assets_Category_ID);
                    }
                    if (!string.IsNullOrEmpty(condition.Assets_Category_IDBegin))
                    {
                        query = query.Where(m => String.Compare(m.m.Assets_Category_ID, condition.Assets_Category_IDBegin) >= 0);
                    }
                    if (!string.IsNullOrEmpty(condition.Assets_Category_IDEnd))
                    {
                        query = query.Where(m => String.Compare(m.m.Assets_Category_ID, condition.Assets_Category_IDEnd) <= 0);
                    }
                    if(!string.IsNullOrEmpty(condition.IsOversea))
                    {
                        query = query.Where(m => m.m.IsOversea == condition.IsOversea);
                    }
                    if (condition.Date_Placed_In_Service_Start != null)
                    {
                        query = query.Where(o => o.m.Date_Placed_In_Service >= condition.Date_Placed_In_Service_Start);
                    }
                    if (condition.Date_Placed_In_Service_End != null)
                    {
                        query = query.Where(o => o.m.Date_Placed_In_Service <= condition.Date_Placed_In_Service_End);
                    }
                    if (condition.Deprn_Date != null)
                    {
                        query = query.Where(o => o.m.Date_Placed_In_Service <= condition.Deprn_Date);
                    }
                    if (condition.Salvage_Value_Start != null)
                    {
                        query = query.Where(o => o.m.Assets_Fixed_Cost >= condition.Salvage_Value_Start);
                    }
                    if (condition.Salvage_Value_End != null)
                    {
                        query = query.Where(o => o.m.Assets_Fixed_Cost <= condition.Salvage_Value_End);
                    }
                    if (!string.IsNullOrEmpty(condition.Location_Disp))
                    {
                        query = query.Where(o => o.m.Location_Disp == condition.Location_Disp);
                    }
                    if (!string.IsNullOrEmpty(condition.Assigned_Branch))
                    {
                        query = query.Where(o => o.m.Assigned_Branch == condition.Assigned_Branch);
                    }
                    if (!string.IsNullOrEmpty(condition.Assigned_ID))
                    {
                        query = query.Where(o => o.m.Assigned_ID == condition.Assigned_ID);
                    }
                    if (!string.IsNullOrEmpty(condition.PO_Number))
                    {
                        query = query.Where(o => o.m.PO_Number.Contains(condition.PO_Number));
                    }
                    if (!string.IsNullOrEmpty(condition.Model_Number))
                    {
                        query = query.Where(o => o.m.Model_Number.Contains(condition.Model_Number));
                    }
                    if (!string.IsNullOrEmpty(condition.Remark))
                    {
                        query = query.Where(o => o.m.Description.Contains(condition.Remark));
                    }

                    result.TotalAmount = query.Count();

                    //分頁
                    if (condition.Page != null)
                    {
                        //選取該頁之資訊
                        query = query.OrderBy(m => m.m.Create_Time).Skip((condition.Page.Value - 1) * Convert.ToInt32(condition.PageSize)).Take(Convert.ToInt32(condition.PageSize));
                    }
                    result.Items = query.ToList().Select(o => MPGeneral.ToAssetModel(o.m, o.bo, o.bu));

                    result.condition = condition;
                }
            }
            return result;
        }

        public AssetHandleResult CreateMP(MPModel MP)
        {
            MPModel CreateMP = new MPModel()
            {
                 Header = MP.Header
            };
            CreateMP.Assets = MP.Assets.Select(o => GetAssetDetailByAssetNumber(o.Asset_Number));

            return Create_Asset(CreateMP);
        }

        public string CreateTRXID(string Type)
        {
            string CToDay = (DateTime.Today.Year - 1911).ToString().PadLeft(3, '0') + DateTime.Today.ToString("MMdd") + Type;
            string TRXID = "";

            using (var db = new AS_LandBankEntities())
            {
                TRXID = db.AS_Assets_MP_Header.Where(mp => mp.Transaction_Type == Type && mp.TRX_Header_ID.StartsWith(CToDay)).Max(o => o.TRX_Header_ID);
            }
            if (TRXID != null)
            {
                int Seq = Convert.ToInt32(TRXID.Substring(11)) + 1;
                TRXID = CToDay + Seq.ToString().PadLeft(6,'0');
            }
            else
            {
                TRXID = CToDay + "000001";
            }
                
            return TRXID;
        }

        public string CreateImport_Num()
        {
            string CToDay = (DateTime.Today.Year - 1911).ToString().PadLeft(3, '0') + DateTime.Today.ToString("MMdd");
            string Import_Num = "";

            using (var db = new AS_LandBankEntities())
            {
                Import_Num = db.AS_Assets_MP_Import.Where(i => i.Import_Num.StartsWith("A" + CToDay)).Max(i => i.Import_Num);
            }
            if (Import_Num != null)
            {
                //A1050110001
                int Seq = Convert.ToInt32(Import_Num.Substring(8)) + 1;
                Import_Num = "A" + CToDay + Seq.ToString().PadLeft(3, '0');
            }
            else
            {
                Import_Num = "A" + CToDay + "001";
            }

            return Import_Num;
        }

        public string CreateAssetNumber(string type1, string type2, string type3)
        {
            string AssetNumber = "";

            using (var db = new AS_LandBankEntities())
            {
                AssetNumber = ( from mp in db.AS_Assets_MP select new { Asset_Number = mp.Asset_Number } ).Concat(from mpr in db.AS_Assets_MP_HIS select new { Asset_Number = mpr.Asset_Number })
                              .Where(i => i.Asset_Number.StartsWith(type1 + type2)).Max(i => i.Asset_Number);
            }
            if (AssetNumber != null)
            {
                //30000310000001
                int Seq = Convert.ToInt32(AssetNumber.Substring(8)) + 1;
                AssetNumber = type1 + type2 + Seq.ToString().PadLeft(7, '0');
            }
            else
            {
                AssetNumber = type1 + type2 + "0000001";
            }

            return AssetNumber;
        }

        private string CreateImportReviewID()
        {
            string CToDay = (DateTime.Today.Year - 1911).ToString().PadLeft(3, '0') + DateTime.Today.ToString("MMdd");
            string ImportReviewID = "";

            using (var db = new AS_LandBankEntities())
            {
                ImportReviewID = db.AS_Assets_MP_ImportReview.Where(mp => mp.ImportReviewID.StartsWith("IR" + CToDay)).Max(o => o.ImportReviewID);
            }
            if (ImportReviewID != null)
            {
                //IR 1060808 0001
                int Seq = Convert.ToInt32(ImportReviewID.Substring(9)) + 1;
                ImportReviewID = "IR" + CToDay + Seq.ToString().PadLeft(4, '0');
            }
            else
            {
                ImportReviewID = "IR" + CToDay + "0001";
            }

            return ImportReviewID;
        }

        public AssetHandleResult HandleHeader(MPHeaderModel Header, IEnumerable<AssetModel> Assets = null)
        {
            AssetHandleResult Result = new AssetHandleResult()
            {
                TRX_Header_ID = Header.TRX_Header_ID
            };

            using (var db = new AS_LandBankEntities())
            {
                using (var trans = new TransactionScope())
                {
                    //Header
                    //Insert
                    if (!db.AS_Assets_MP_Header.Any(h => h.TRX_Header_ID == Header.TRX_Header_ID))
                    {
                        db.Entry(MPGeneral.ToAS_Assets_MP_Header(Header)).State = System.Data.Entity.EntityState.Added;
                        db.SaveChanges();
                    }
                    //Update
                    else
                    {
                        var MP_Header = db.AS_Assets_MP_Header.First(h => h.TRX_Header_ID == Header.TRX_Header_ID);
                        MP_Header.Book_Type = Header.Book_Type;
                        MP_Header.Office_Branch = Header.Office_Branch;
                        MP_Header.Source = Header.Source;
                        MP_Header.OriOffice_Branch = !string.IsNullOrEmpty(MP_Header.OriOffice_Branch) ? Header.OriOffice_Branch : "";
                        MP_Header.DisposeReason = !string.IsNullOrEmpty(MP_Header.DisposeReason) ? Header.DisposeReason : "";
                        MP_Header.Remark = Header.Remark;
                        MP_Header.Last_Updated_By = "";
                        MP_Header.Last_Updated_Time = DateTime.Now;
                        MP_Header.SummarySDate = Header.SummarySDate;
                        MP_Header.SummaryEDate = Header.SummaryEDate;
                        db.Entry(MP_Header).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    //目前只有盤點要用
                    if(Assets != null)
                    {
                        foreach(var item in Assets)
                        {
                            if (db.AS_Assets_MP_HIS.Any(o => o.TRX_Header_ID == Header.TRX_Header_ID && o.Asset_Number == item.Asset_Number))
                            {
                                AS_Assets_MP_HIS His = db.AS_Assets_MP_HIS.First(o => o.TRX_Header_ID == Header.TRX_Header_ID && o.Asset_Number == item.Asset_Number);
                                His.InventoryCheck = item.InventoryCheck;
                                His.InventoryCheckMemo = item.InventoryCheckMemo;
                                His.Last_Updated_Time = DateTime.Now;
                                db.Entry(His).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }
                    trans.Complete();
                }
            }
            return Result;
        }

        public bool GetDocID(string DocCode,out int DocID)
        {
            bool result = true;
            using (var db = new AS_LandBankEntities())
            {
                result = db.AS_Doc_Name.Any(m => m.Doc_No == DocCode);
                DocID = (result) ? db.AS_Doc_Name.First(m => m.Doc_No == DocCode).ID : 0;
            }
            return (DocID != 0);
        }

        public bool CopyAsset(string CopyAssetID, int CopyCount)
        {
            bool Result = true;

            using (var db = new AS_LandBankEntities())
            {
                int ID = Convert.ToInt32(CopyAssetID);
                AS_Assets_MP_HIS Asset = db.AS_Assets_MP_HIS.First(o=>o.ID == ID);
                string[] type = Asset.Asset_Category_Code.Split('.');

                for(int i = 0;i < CopyCount;i++)
                {
                    Asset.Asset_Number = CreateAssetNumber(type[0], type[1], type[2]);
                    db.Entry(Asset).State = System.Data.Entity.EntityState.Added;
                    db.SaveChanges();

                    //Record
                    AS_Assets_MP_Record Record = new AS_Assets_MP_Record()
                    {
                        TRX_Header_ID = Asset.TRX_Header_ID,
                        Asset_Number = Asset.Asset_Number,
                        Created_By = Asset.Created_By,
                        Create_Time = Asset.Create_Time,
                        Last_Updated_By = Asset.Last_Updated_By,
                        Last_Updated_Time = Asset.Last_Updated_Time
                    };
                    db.Entry(Record).State = System.Data.Entity.EntityState.Added;
                    db.SaveChanges();
                }
            }

            return Result;
        }

        public bool UpdateFlowStatus(string TRX_Header_ID, string FlowStatus)
        {
            bool Result = true;

            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_MP_Header Header = db.AS_Assets_MP_Header.First(o => o.TRX_Header_ID == TRX_Header_ID);
                Header.Flow_Status = FlowStatus;
                Header.Last_Updated_Time = DateTime.Now;
                db.Entry(Header).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }

            return Result;
        }

        public bool DeleteAsset(int AssetID)
        {
            bool result = true;
            try
            {
                using (var db = new AS_LandBankEntities())
                {
                    db.AS_Assets_MP_HIS.RemoveRange(db.AS_Assets_MP_HIS.Where(m => m.ID == AssetID));
                    db.SaveChanges();
                }
            } 
            catch(Exception ex)
            {
                result = false;
            }
            
            return result;
        }

        //動產盤點通知
        public bool MPInventoryNotice(string TRX_Header_ID)
        {
            bool result = true;
            try
            {
                using (var db = new AS_LandBankEntities())
                {
                    using (var trans = new TransactionScope())
                    {
                        //update表單狀態
                        var Header = db.AS_Assets_MP_Header.First(o => o.TRX_Header_ID == TRX_Header_ID);
                        //已核准
                        Header.Flow_Status = "2";
                        Header.Last_Updated_Time = DateTime.Now;
                        db.Entry(Header).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();


                        //新增各分行盤點回報單
                        var query = db.AS_Keep_Position.Where(o => o.Layer == 2).ToList();

                        foreach(var poistion in query)
                        {
                            string Type = "A62";
                            //新增表頭
                            AS_Assets_MP_Header header = new AS_Assets_MP_Header()
                            {
                                Book_Type = "NTD_MP_IFRS",
                                Office_Branch = poistion.Keep_Position_Code,
                                Transaction_Type = Type,
                                Source = "H",
                                Flow_Status = "0",
                                TRX_Header_ID = CreateTRXID(Type),
                                OriOffice_Branch = "",
                                Remark = "",
                                DisposeReason = "",
                                Created_By = "",
                                Create_Time = DateTime.Now,
                                Last_Updated_By = ""
                            };

                            db.Entry(header).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();


                            //抓此分行下的動產
                            var AssetList = db.AS_Assets_MP.Where(o => o.Assigned_Branch.Contains(poistion.Keep_Position_Code));

                            foreach(var item in AssetList)
                            {
                                AS_Assets_MP_HIS His = new AS_Assets_MP_HIS()
                                {
                                    TRX_Header_ID = header.TRX_Header_ID,
                                    Assets_Parent_Number = (!string.IsNullOrEmpty(item.Assets_Parent_Number)) ? item.Assets_Parent_Number : "",
                                    IsOversea = (!string.IsNullOrEmpty(item.IsOversea)) ? item.IsOversea : "",
                                    Asset_Category_Code = (!string.IsNullOrEmpty(item.Asset_Category_Code)) ? item.Asset_Category_Code : "",
                                    Assets_Name = (!string.IsNullOrEmpty(item.Assets_Name)) ? item.Assets_Name : "",
                                    Assets_Alias = (!string.IsNullOrEmpty(item.Assets_Alias)) ? item.Assets_Alias : "",
                                    Asset_Number = (!string.IsNullOrEmpty(item.Asset_Number)) ? item.Asset_Number : "",
                                    Assets_Original_ID = (!string.IsNullOrEmpty(item.Assets_Original_ID)) ? item.Assets_Original_ID : "",
                                    Life_Years = item.Life_Years,
                                    Assets_Category_ID = (!string.IsNullOrEmpty(item.Assets_Category_ID)) ? item.Assets_Category_ID : "",
                                    Deprn_Method_Code = (!string.IsNullOrEmpty(item.Deprn_Method_Code)) ? item.Deprn_Method_Code : "",
                                    Date_Placed_In_Service = item.Date_Placed_In_Service,
                                    Assets_Unit = item.Assets_Unit,
                                    Location_Disp = (!string.IsNullOrEmpty(item.Location_Disp)) ? item.Location_Disp : "",
                                    Assigned_Branch = (!string.IsNullOrEmpty(item.Assigned_Branch)) ? item.Assigned_Branch : "",
                                    Assigned_ID = (!string.IsNullOrEmpty(item.Assigned_ID)) ? item.Assigned_ID : "",
                                    PO_Number = (!string.IsNullOrEmpty(item.PO_Number)) ? item.PO_Number : "",
                                    PO_Description = (!string.IsNullOrEmpty(item.PO_Description)) ? item.PO_Description : "",
                                    Transaction_Date = item.Transaction_Date,
                                    Currency = !string.IsNullOrEmpty(item.Currency) ? item.Currency : "",
                                    Description = (!string.IsNullOrEmpty(item.Description)) ? item.Description : "",
                                    Post_Date = item.Post_Date,
                                    Created_By = item.Created_By,
                                    Create_Time = item.Create_Time,
                                    Last_Updated_By = (!string.IsNullOrEmpty(item.Last_Updated_By)) ? item.Last_Updated_By : "",
                                    Last_Updated_Time = item.Last_Updated_Time,
                                    Assets_Fixed_Cost = item.Assets_Fixed_Cost,
                                    AssignedSignUp = "",
                                    Deprn_Reserve = item.Deprn_Reserve,
                                    Import_Num = "",
                                    Model_Number = item.Model_Number,
                                    Salvage_Value = item.Salvage_Value
                                };

                                db.Entry(His).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                        }
                        trans.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }


        #region == 抓動產相關代碼20170830,暫時放這 ==
        public IEnumerable<CodeItem> GetAssetMain_Kind(string Type)
        {
            IEnumerable<CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                if(Type == "MP")
                {
                    result = (from m in db.AS_Asset_Category_Main_Kinds
                                  //先加在這邊
                              where m.No != "1" && m.No != "2"
                              select new CodeItem()
                              {
                                  Value = m.No,
                                  Text = m.Name
                              }).ToList();
                }
                else if(Type == "Land")
                {
                    result = (from m in db.AS_Asset_Category_Main_Kinds
                                  //先加在這邊
                              where m.No == "1"
                              select new CodeItem()
                              {
                                  Value = m.No,
                                  Text = m.Name
                              }).ToList();
                }
                else if (Type == "Build")
                {
                    result = (from m in db.AS_Asset_Category_Main_Kinds
                                  //先加在這邊
                              where m.No == "2"
                              select new CodeItem()
                              {
                                  Value = m.No,
                                  Text = m.Name
                              }).ToList();
                }
                else
                {
                    result = (from m in db.AS_Asset_Category_Main_Kinds
                                  //先加在這邊
                              select new CodeItem()
                              {
                                  Value = m.No,
                                  Text = m.Name
                              }).ToList();
                }

            }
            return result;
        }

        public IEnumerable<CodeItem> GetAssetDetail_Kind(string MainKind)
        {
            IEnumerable<CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                int MainID = Convert.ToInt32(MainKind);
                result = (from m in db.AS_Asset_Category_Detail_Kinds
                          where m.Main_Kind_ID == MainID
                          select new CodeItem()
                         {
                             Value = m.No,
                             Text = m.Name
                         }).ToList();
            }
            return result;
        }

        public IEnumerable<CodeItem> GetAssetUse_Types()
        {
            IEnumerable<CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                result = (from m in db.AS_Asset_Category_Use_Types
                         select new CodeItem()
                         {
                             Value = m.No,
                             Text = m.Name
                         }).ToList();
            }
            return result;
        }

        public IEnumerable<CodeItem> GetKeep_Position(string Target, string Branch = "", string Dept = "", string SubDept = "")
        {
            IEnumerable<CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                switch (Target)
                {
                    case "Branch":
                        result = (from m in db.AS_Keep_Position
                                  select new { m.Keep_Branch_Code,m.Keep_Branch_Name } ).Distinct().ToList()
                                  .Where(o=>o.Keep_Branch_Code != null)
                                  .Select(o => new CodeItem()
                                  {
                                         Value = (!string.IsNullOrEmpty(o.Keep_Branch_Code)) ? o.Keep_Branch_Code.PadLeft(3, '0') : "",
                                         Text = o.Keep_Branch_Name
                                  });
                        break;
                    case "Dept":
                        result = (from m in db.AS_Keep_Position
                                  where m.Keep_Branch_Code == Branch
                                  select new { m.Keep_Dept, m.Keep_Dept_Name }).Distinct().ToList()
                                  .Where(o=>o.Keep_Dept != null && o.Keep_Dept != "")
                                  .Select(o => new CodeItem()
                                  {
                                      Value = o.Keep_Dept,
                                      Text  = o.Keep_Dept_Name
                                  });
                        break;
                    case "SubDept":
                        result = (from m in db.AS_Keep_Position
                                  where m.Keep_Branch_Code == Branch && m.Keep_Dept == Dept
                                  select new { m.Keep_SubDept_Code, m.Keep_SubDept_Name }).Distinct().ToList()
                                  .Where(o => o.Keep_SubDept_Code != null && o.Keep_SubDept_Code != "")
                                  .Select(o => new CodeItem()
                                  {
                                      Value = o.Keep_SubDept_Code,
                                      Text = o.Keep_SubDept_Name
                                  });
                        break;
                    case "Floor":
                        result = (from m in db.AS_Keep_Position
                                  where m.Keep_Branch_Code == Branch && m.Keep_Dept == Dept && m.Keep_SubDept_Code == SubDept
                                  select new { m.Keep_Floor, m.Keep_Floor_Name }).Distinct().ToList()
                                  .Where(o => o.Keep_Floor != null && o.Keep_Floor != "")
                                  .Select(o => new CodeItem()
                                  {
                                      Value = o.Keep_Floor,
                                      Text = o.Keep_Floor_Name
                                  });
                        break;
                }        
            }
            return result;
        }

        public IEnumerable<CodeItem> GetUser(string Branch, string Dept)
        {
            IEnumerable<CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from m in db.AS_Users
                            select m;
                if(!string.IsNullOrEmpty(Branch))
                {
                    query = query.Where(o=>o.Branch_Code == Branch);
                }
                if (!string.IsNullOrEmpty(Dept))
                {
                    query = query.Where(o => o.Department_Code == Dept);
                }

                result = query.ToList().Select(o => new CodeItem()
                {
                    Value = o.User_Code,
                    Text = o.User_Name
                });
            }
            return result;
        }

        public string GetAccountItem(string AssetDetailKind)
        {
            string result = "";
            using (var db = new AS_LandBankEntities())
            {
                var query = (from s in db.AS_GL_Trade_Acc_Sub
                             join a in db.AS_GL_Account on s.Account equals a.Account
                             join d in
                             (
                                from dd in db.AS_GL_Trade_Define
                                where dd.Trade_Type == "1"
                                select dd
                             ) on s.Master_No equals d.Master_No
                             where s.Asset_Category_code == AssetDetailKind && a.DB_CR == "D"
                             select new { s.Account, a.Account_Name }).ToList();

                if(query.Any())
                {
                    result = query.First().Account + query.First().Account_Name;
                }
            }
            return result;
        }

        public IEnumerable<CodeItem> GetCodeItem(string CodeClass)
        {
            IEnumerable<CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                result = (from m in db.AS_VW_Code_Table
                          where m.Class == CodeClass
                          select new CodeItem()
                          {
                              Value = m.Code_ID,
                              Text = m.Text
                          }).ToList().OrderBy(o=>o.Value);
            }
            return result;
        }

        public IEnumerable<CodeItem> SearchAssetsByCategoryCode(string CategoryCode)
        {
            IEnumerable<CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                result = (from m in db.AS_Assets_MP
                          where m.Asset_Category_Code == CategoryCode
                          select new CodeItem()
                          {
                              Value = m.Asset_Number,
                              Text = m.Assets_Name
                          }).ToList();
            }
            return result;
        }

        public IEnumerable<DeprnRecordModel> GetDeprnRecord(string AssetNumber)
        {
            IEnumerable<DeprnRecordModel> result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = (from m in
                            (
                                from m in db.AS_Assets_MP_Deprn
                                where m.Asset_Number == AssetNumber && m.Post_Date != null
                                select m
                            ) join h in db.AS_Assets_MP_Header on m.TRX_Header_ID equals h.TRX_Header_ID
                            join hc in
                            (
                               from hc in db.AS_VW_Code_Table
                               where hc.Class == "MPForm"
                               select hc
                            ) on h.Transaction_Type equals hc.Code_ID
                            select new {m,h,hc }).ToList();

                if(query != null)
                {
                    result = query.Select(o => new DeprnRecordModel()
                    {
                         TrxID = o.m.TRX_Header_ID,
                         AdjustedCost = o.m.Adjusted_Cost,
                         DeprnCost = o.m.Deprn_Cost,
                         HeaderTypeName = o.hc.Text,
                         PostDate = (o.m.Post_Date.HasValue)? o.m.Post_Date.Value.ToString("yyyy/MM/dd"): "",
                         Remark = o.h.Remark
                    });
                }
                
            }
            return result;
        }

        public UserDetail GetUserInfo(int ID)
        {
            UserDetail user = new UserDetail();
            using (var db = new AS_LandBankEntities())
            {
                if(db.AS_Users.Any(o=>o.ID == ID))
                {
                    AS_Users ASUser = db.AS_Users.First(o => o.ID == ID);
                    user.ID = ASUser.ID;
                    user.UserName = ASUser.User_Name;
                    user.UserCode = ASUser.User_Code;
                    user.BranchCode = ASUser.Branch_Code;
                    user.BranchName = ASUser.Branch_Name;
                    user.DepartmentCode = ASUser.Department_Code;
                    user.DepartmentName = ASUser.Department_Name;
                }
            }
            return user;
        }

        public bool DeleteForm(string TRX_Header_ID)
        {
            bool result = true;
            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        //Header
                        db.AS_Assets_MP_Header.RemoveRange(db.AS_Assets_MP_Header.Where(m => m.TRX_Header_ID == TRX_Header_ID));
                        db.SaveChanges();

                        //His
                        db.AS_Assets_MP_HIS.RemoveRange(db.AS_Assets_MP_HIS.Where(m => m.TRX_Header_ID == TRX_Header_ID));
                        db.SaveChanges();
                    }

                    trans.Complete();
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public AssetModel GetAssetInfoByCategory_ID(string Assets_Category_ID)
        {
            AssetModel result = null;
            using (var db = new AS_LandBankEntities())
            {
                if(db.AS_Life_table.Any(o=>o.Life_Code_No == Assets_Category_ID))
                {
                    AS_Life_table Obj = db.AS_Life_table.First(o => o.Life_Code_No == Assets_Category_ID);
                    result = new AssetModel()
                    {
                         Assets_Name = Obj.Asset_Text,
                         Life_Years = Convert.ToInt32(Obj.Life_Years)
                    };
                }
            }
            return result;
        }
        #endregion
    }
}
