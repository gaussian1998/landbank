﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MPModels.ReportModel
{
    public class MPReportModel : AbstractEncryptionDTO
    {
        public string Asset_Number { get; set; }
        public string Asset_Category_Code { get; set; }
        public string Assets_Category_ID { get; set; }
        public string Assets_Name { get; set; }
        public string Model_Number { get; set; }
        public Nullable<System.DateTime> Transaction_Date { get; set; }
        public string Children { get; set; }
        public int Life_Years { get; set; }
        public int Assets_Unit { get; set; }
        public Nullable<decimal> Assets_Fixed_Cost { get; set; }

        public string PO_Number { get; set; }
        public string Assigned_Branch { get; set; }
        public string Remark { get; set; }
        public string Location_Disp { get; set; }

        /// <summary>啟用日期</summary>
        public Nullable<System.DateTime> Date_Placed_In_Service { get; set; }

        #region== 折舊 ==
        public Nullable<decimal> Deprn_Reserve { get; set; }
        public Nullable<decimal> Salvage_Value { get; set; }
        public Nullable<decimal> Net_Value { get; set; }
        public int Life_Month { get; set; }
        public decimal Deprn_Cost { get; set; }
        public DateTime? Deprn_Date { get; set; }
        #endregion


        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }

    }
}
