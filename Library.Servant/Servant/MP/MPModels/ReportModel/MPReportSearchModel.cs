﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MPModels.ReportModel
{
    public class MPReportSearchModel : AbstractEncryptionDTO
    {
        public string ReportName { get; set; }
        public string JurisdictionAssignedBranch { get; set; }
        public string JurisdictionOfficeBranch { get; set; }
        public string UseAssignedBranch { get; set; }
        public string UseOfficeBranch { get; set; }
        public string BookTypeCode { get; set; }
        public string AssetNumberTop { get; set; }
        public string AssetNumberEnd { get; set; }
        public Nullable<System.DateTime> LifeYears { get; set; }
        public Nullable<System.DateTime> DatePlacedInService { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
