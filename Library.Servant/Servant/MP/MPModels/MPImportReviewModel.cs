﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MP.MPModels
{
    public class MPImportReviewModel : AbstractEncryptionDTO
    {
        public string IRID { get; set; }
        public IEnumerable<MPImportModel> Imports { get; set; }

        public IEnumerable<MPModel> MPs { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
