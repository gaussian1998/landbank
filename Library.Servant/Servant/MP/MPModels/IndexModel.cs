﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MP.MPModels
{
    public class IndexModel : AbstractEncryptionDTO
    {
        public IEnumerable<MPHeaderModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public SearchModel condition { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
