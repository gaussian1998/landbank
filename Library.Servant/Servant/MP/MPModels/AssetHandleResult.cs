﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MP.MPModels
{
    public class AssetHandleResult : AbstractEncryptionDTO
    {
        /// <summary>
        /// 單據編號
        /// </summary>
        public string TRX_Header_ID { get; set; }

        /// <summary>
        /// 批次編號
        /// </summary>
        public string ImportNum { get; set; }

        /// <summary>
        /// 批號送審ID
        /// </summary>
        public string ImportReviewID { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
