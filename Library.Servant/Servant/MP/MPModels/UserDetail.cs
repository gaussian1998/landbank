﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MP.MPModels
{
    public class UserDetail : AbstractEncryptionDTO
    {
        public int ID { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string BranchCode { get; set; }
        public bool IsHeadOffice
        {
           get
           {
                return this.BranchCode == "001";
           }
        }
        public string BranchName { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
