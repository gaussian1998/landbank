﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MP.MPModels
{
    public class AssetsSearchModel : AbstractEncryptionDTO
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }
        public int ID { get; set; }
        public string Asset_Category_Code { get; set; }

        public string Assets_Name { get; set; }

        public string Assets_Alias { get; set; }

        public string Asset_Number { get; set; }
        public string Asset_NumberBegin { get; set; }
        public string Asset_NumberEnd { get; set; }

        public string Assets_Category_ID { get; set; }
        public string Assets_Category_IDBegin { get; set; }

        public string Assets_Category_IDEnd { get; set; }

        public DateTime? Date_Placed_In_Service_Start { get; set; }

        public DateTime? Date_Placed_In_Service_End { get; set; }

        public DateTime? Deprn_Date { get; set; }

        public decimal? Salvage_Value_Start { get; set; }

        public decimal? Salvage_Value_End { get; set; }

        public string Location_Disp { get; set; }

        public string Assigned_Branch { get; set; }

        public string Assigned_ID { get; set; }

        public string PO_Number { get; set; }

        public string Model_Number { get; set; }

        public string Remark { get; set; }

        public string IsOversea { get; set; }

        #region == 資產複製使用 ==
        public string CopyAssetID { get; set; }
        public int CopyCount { get; set; }
        #endregion

        #region == 資產類別 ==
        public string MainKind { get; set; }
        public string AssetDetailKind { get; set; }
        public string CategoryCode { get; set; }
        #endregion

        #region == 保管單位 ==
        public string KeepPosition { get; set; }
        #endregion

        #region == 保管人單位 ==
        public string Branch { get; set; }
        public string Dept { get; set; }
        #endregion

        #region == 代碼類別 ==
        public string CodeClass { get; set; }
        #endregion
        #region == 抓資產類型變數 ==
        public string AssetMainKindType { get; set; }
        #endregion
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
