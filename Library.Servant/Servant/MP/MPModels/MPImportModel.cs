﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MP.MPModels
{
    public class MPImportModel : AbstractEncryptionDTO
    {
        public int ID { get; set; }
        public string Import_Num { get; set; }
        public int Import_Year { get; set; }
        public int Import_Month { get; set; }
        public string PO_Number { get; set; }
        public string Office_Branch { get; set; }
        public string Office_BranchName { get; set; }
        public string Status { get; set; }
        public string StatusName { get; set; }
        public string Source { get; set; }
        public string Remark { get; set; }
        public System.DateTime Create_Time { get; set; }
        public string Created_By { get; set; }
        public string Created_ByName { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public string Last_Updated_By { get; set; }

        public int ImportAssetsCount { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
