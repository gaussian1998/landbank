﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MP.MPModels
{
    public class ImportSearchModel : AbstractEncryptionDTO
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }

        public string ImportNum { get; set; }

        public string ImportNumBegin { get; set; }

        public string ImportNumEnd { get; set; }

        public DateTime? ImportBeginDate { get; set; }

        public DateTime? ImportEndDate { get; set; }

        public string PONumberBegin { get; set; }

        public string PONumberEnd { get; set; }

        public int? ImportYear { get; set; }

        public int? ImportMonth { get; set; }

        public string Status { get; set; }

        public bool LastRecord { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
