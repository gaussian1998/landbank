﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MP.MPModels
{
    public class MPHeaderModel : AbstractEncryptionDTO
    {
        public int ID { get; set; }
        public string TRX_Header_ID { get; set; }
        public string Transaction_Type { get; set; }
        public string Transaction_TypeName { get; set; }
        public string Source { get; set; }
        public string SourceName { get; set; }
        public string Book_Type { get; set; }
        public string OriOffice_Branch { get; set; }
        public string OriOffice_BranchName { get; set; }
        public string Office_Branch { get; set; }
        public string Office_BranchName { get; set; }
        public string DisposeReason { get; set; }
        public string Remark { get; set; }
        public string Flow_Status { get; set; }
        public string Flow_StatusName { get; set; }
        public System.DateTime Create_Time { get; set; }
        public string Created_By { get; set; }
        public string Created_ByName { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public string Last_Updated_By { get; set; }

        /// <summary>
        /// 資產數量
        /// </summary>
        public int AssetsCount { get; set; }

        //盤點用到
        public DateTime? SummarySDate { get; set; }
        public DateTime? SummaryEDate { get; set; }


        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
