﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MP.MPModels
{
    public class CodeItem
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }
}
