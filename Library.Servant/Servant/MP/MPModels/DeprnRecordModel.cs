﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MP.MPModels
{
    public class DeprnRecordModel
    {
        public string PostDate { get; set; }
        public string HeaderTypeName { get; set; }
        public string TrxID { get; set; }
        public string Remark { get; set; }
        public decimal DeprnCost { get; set; }
        public decimal AdjustedCost { get; set; }
    }
}
