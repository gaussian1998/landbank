﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MP.MPModels
{
    public class AssetModel : AbstractEncryptionDTO
    {
        public int ID { get; set; }
        public string Asset_Number { get; set; }
        #region==資產編號三種類別==
        public string type1 { get; set; }
        public string type2 { get; set; }
        public string type3 { get; set; }
        #endregion
        public string Assets_Original_ID { get; set; }
        public string Assets_Category_ID { get; set; }
        public string Assets_Name { get; set; }
        public string Assets_Alias { get; set; }
        public int Assets_Unit { get; set; }
        public string Assets_Parent_Number { get; set; }
        public string IsOversea { get; set; }
        public string Asset_Category_Code { get; set; }
        public string Location_Disp { get; set; }
        public int Life_Years { get; set; }
        public string Deprn_Method_Code { get; set; }
        public Nullable<System.DateTime> Date_Placed_In_Service { get; set; }
        public string Assigned_Branch { get; set; }
        public string Assigned_BranchName { get; set; }
        public string Assigned_ID { get; set; }
        public string Assigned_IDName { get; set; }
        public string Assigned_IDEmail { get; set; }
        public string Assigned_IDPhone { get; set; }
        public string PO_Number { get; set; }
        public string PO_Description { get; set; }
        public string Model_Number { get; set; }
        public Nullable<System.DateTime> Transaction_Date { get; set; }
        public decimal Assets_Fixed_Cost { get; set; }
        public decimal Deprn_Reserve { get; set; }
        public decimal Salvage_Value { get; set; }
        public string Currency { get; set; }
        public string Description { get; set; }
        public string AccessoryEquipment { get; set; }
        public string PropertyUnit { get; set; }
        public string MachineCode { get; set; }
        public string LicensePlateNumber { get; set; }
        public Nullable<System.DateTime> Post_Date { get; set; }
        public string ImportNum { get; set; }
        public System.DateTime Create_Time { get; set; }
        public string Created_By { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public string Last_Updated_By { get; set; }

        public string AssignedSignUp { get; set; }

        public Nullable<bool> Not_Deprn_Flag { get; set; }

        #region==動產盤點==
        public string InventoryCheck { get; set; }
        public string InventoryCheckMemo { get; set; }
        #endregion
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }

    }
}
