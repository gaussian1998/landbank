﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;

namespace Library.Servant.Servant.MP.MPModels
{
    public class MPModel : AbstractEncryptionDTO
    {
        public MPHeaderModel Header { get; set; }

        public IEnumerable<AssetModel> Assets { get; set; }

        #region==批次匯入審查欄位==
        //分行
        public string Assigned_Branch { get; set; }
        //經辦
        public string HandledBy { get; set; }
        //經辦電話
        public string Phone { get; set; }
        //經辦Email
        public string Email { get; set; }
        #endregion

        #region == 折舊紀錄 ==
        public IEnumerable<DeprnRecordModel> DeprnRecord { get; set; }
        #endregion

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
