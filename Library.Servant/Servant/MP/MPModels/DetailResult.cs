﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.MP.MPModels
{
    public class DetailResult : AbstractEncryptionDTO
    {
        /// <summary>資產編號</summary>
        public string AssetsNumber { get; set; }

        /// <summary>原資產編號</summary>
        public string OriginalAssetsNumber { get; set; }

        /// <summary>資產分類</summary>
        public string AssetCategoryCode { get; set; }

        /// <summary>財物名稱</summary>
        public string AssetsName { get; set; }

        /// <summary>資產別名</summary>
        public string AssetsAlias { get; set; }

        /// <summary>耐用年限</summary>
        public int LifeYears { get; set; }

        /// <summary>財務編號</summary>
        public string AssetsCategoryNUM { get; set; }

        /// <summary>折舊方式</summary>
        public string DeprnMethodCode { get; set; }

        /// <summary>帳本</summary>
        public string BookTypeCode { get; set; }

        /// <summary>啟用日期</summary>
        public DateTime DatePlacedInService { get; set; }

        /// <summary>單位量</summary>
        public int AssetsUnit { get; set; }

        /// <summary>放置地點</summary>
        public int LocationDisp { get; set; }

        /// <summary>資產會計科目</summary>
        public int AssestsSubject { get; set; }

        /// <summary>拆舊會計科目</summary>
        public int DeprnSubject { get; set; }

        /// <summary>保管單位</summary>
        public string AssignedBranch { get; set; }

        /// <summary>保管人</summary>
        public string AssignedID { get; set; }

        /// <summary>採購案號</summary>
        public string PONo { get; set; }

        /// <summary>採購描述</summary>
        public string PODescription { get; set; }

        /// <summary>廠牌規格</summary>
        public string ModelNumber { get; set; }

        /// <summary>驗收日期</summary>
        public string TransactionDate { get; set; }

        /// <summary>附屬設備</summary>
        public List<string> AttachEqus { get; set; }

        /// <summary>財務單位</summary>
        public string FianceUnit { get; set; }

        /// <summary>管轄單位</summary>
        public string OfficeBranch { get; set; }

        /// <summary>資產成本</summary>
        public long FixedAssetsCost { get; set; }

        /// <summary>累計折舊</summary>
        public long DeprnReserve { get; set; }

        /// <summary>殘值</summary>
        public long SalvageValue { get; set; }

        /// <summary>備註</summary>
        public string Remark { get; set; }

        /// <summary>建立時間</summary>
        public DateTime CreateTime { get; set; }

        /// <summary>建立人員</summary>
        public string CreatedBy { get; set; }

        /// <summary>最後異動時間</summary>
        public string LastUpdatedTime { get; set; }

        /// <summary>最後異動人員</summary>
        public string LastUpdatedBy { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
