﻿using Library.Entity.Edmx;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MP.MPModels
{
    public class MPImportPostModel : AbstractEncryptionDTO
    {
        public MPImportModel import { get; set; }
        public IEnumerable<AssetModel> AssetList { get; set; }

        public int ImportID { get; set; }

        public int ID { get; set; }


        public string IRID { get; set;}
        public string[] ImportList { get; set; }
        public MPHeaderModel Header { get; set; }


        public IEnumerable<AS_VW_MP_ImportHis> ImportHis { get; set; }


        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
