﻿using Library.Entity.Edmx;
using Library.Servant.Servant.MP.MPModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MP
{
    public interface IMPServant
    {
        IndexModel Index(SearchModel condition);

        MPModel GetDetail(string TRXHeaderID);

        MPImportReviewModel GetImportReviewDetail(string IRID);

        ImportIndexModel ImportIndex(ImportSearchModel condition);

        ImportReviewIndexModel ImportReviewIndex();

        IEnumerable<AS_VW_MP_ImportHis> GetImportData(int ImportID);
        AssetsIndexModel GetImportAssets(int ImportID);

        AssetHandleResult Create_Asset(MPModel model);

        AssetHandleResult CreateToMP(string TRXHeaderID);

        AssetHandleResult CreateImport(MPImportModel import,IEnumerable<AssetModel> AssetList);

        AssetHandleResult CreateImportReview(string IRID,string[] ImportList,MPHeaderModel Header);

        AssetHandleResult Update_Asset(MPModel model);

        AssetHandleResult UpdateToMP(string TRXHeaderID);

        AssetModel GetAssetDetail(int ID);

        AssetModel GetImportAssetDetail(int ID);

        MPHeaderModel GetHeader(string AssetNumber, string Type);

        AssetsIndexModel SearchAssets(AssetsSearchModel condition);

        AssetHandleResult CreateMP(MPModel MP);

        string CreateTRXID(string Type);

        string CreateImport_Num();

        string CreateAssetNumber(string type1,string type2,string type3);

        AssetHandleResult HandleHeader(MPHeaderModel Header,IEnumerable<AssetModel> Assets = null);

        bool GetDocID(string DocCode,out int DocID);

        bool CopyAsset(string CopyAssetID,int CopyCount);

        bool UpdateFlowStatus(string TRX_Header_ID, string FlowStatus);

        bool DeleteAsset(int AssetID);

        bool DeleteForm(string TRX_Header_ID);

        IEnumerable<DeprnRecordModel> GetDeprnRecord(string AssetNumber);

        bool MPInventoryNotice(string TRX_Header_ID);

        AssetModel GetAssetMPDetail(int ID);

        bool AssetsDeprnUpdate(IEnumerable<AssetModel> assets);




        #region == 抓動產相關代碼20170830,暫時放這 ==
        IEnumerable<CodeItem> GetAssetMain_Kind(string Type);

        IEnumerable<CodeItem> GetAssetDetail_Kind(string MainKind);

        IEnumerable<CodeItem> GetAssetUse_Types();

        IEnumerable<CodeItem> GetKeep_Position(string Target, string Branch = "", string Dept = "", string SubDept = "");

        IEnumerable<CodeItem> GetUser(string Branch,string Dept);

        //取得會計科目
        string GetAccountItem(string AssetDetailKind);

        IEnumerable<CodeItem> GetCodeItem(string CodeClass);

        IEnumerable<CodeItem> SearchAssetsByCategoryCode(string CategoryCode);

        AssetModel GetAssetDetailByAssetNumber(string AssetNumber);

        AssetModel GetAssetInfoByCategory_ID(string Assets_Category_ID);
        #endregion

        #region == 抓目前登入者資訊 ==
        UserDetail GetUserInfo(int ID);
        #endregion
    }
}
