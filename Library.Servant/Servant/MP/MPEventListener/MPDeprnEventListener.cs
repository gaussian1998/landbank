﻿using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servants.AbstractFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MP.MPEventListener
{
    public class MPDeprnEventListener : ExampleListener, IHttpGetProvider
    {
        private IMPServant MPServant = ServantAbstractFactory.MP();

        public HttpGetViewModel GetUrl(string FormNo, string carryData)
        {
            return new HttpGetViewModel { ControllerName = "MPDeprn", ActionName = "Edit", Parameter = "id=" + FormNo + "&signmode=Y" };
        }

        public override bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            MPServant.UpdateToMP(formNo);
            return true;
        }
    }
}
