﻿namespace Library.Servant.Servant.QueryUserRole.Models
{
    public class ConditionResult
    {
        public bool IsSaved { get; set; }
        public bool IsModified { get; set; }
        public bool IsSubmited { get; set; }
    }
}
