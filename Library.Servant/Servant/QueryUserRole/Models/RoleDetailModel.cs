﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.QueryUserRole.Models
{
    public class RoleDetailModel
    {
        public bool IsChecked { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public string DocType { get; set; }
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public List<string> Programs { get; set; }
    }
}
