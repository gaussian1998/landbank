﻿using System;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.QueryUserRole.Models
{
    public class SearchModel : InvasionEncryption
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string FlowCode { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
    }
}
