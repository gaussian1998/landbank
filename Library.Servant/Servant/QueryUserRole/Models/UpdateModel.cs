﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;
using System;

namespace Library.Servant.Servant.QueryUserRole.Models
{
    public class UpdateModel : InvasionEncryption
    {
        public int UpdateUserID { get; set; }
        public string UserCode { get; set; }
        public string FlowCode { get; set; }
        public List<Tuple<int, string, bool>> TotalRoles { get; set; } //todo_julius 還是要改
    }
}
