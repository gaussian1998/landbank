﻿using System;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;
using Library.Servant.Enums;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.QueryUserRole.Models
{
    public class ListItemResult : InvasionEncryption
    {
        public string FlowCode { get; set; }
        public DateTime FlowDate { get; set; }
        public string Type { get; set; }
        public int Count { get; set; }
        public string AppliedUser { get; set; }
        public string Status { get; set; }
        public string AppliedUserName
        {
            get {
                return StaticDataServant.GetUserName(int.Parse(AppliedUser));
            }
        }
        public string StatusName
        {
            get {
                return StaticDataServant.GetStatusText(Status);
            }
        }
    }
}
