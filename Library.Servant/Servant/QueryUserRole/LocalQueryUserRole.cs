﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Entity.Edmx;
using Library.Servant.Servant.QueryUserRole.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.QueryUserRole
{
    public class LocalQueryUserRole : IQueryUserRole
    {
        public IndexResult Index(SearchModel search)
        {
            var isSkipFlowCode = string.IsNullOrEmpty(search.FlowCode);
            var isSkipBranch = string.IsNullOrEmpty(search.Branch);
            var isSkipDepartment = string.IsNullOrEmpty(search.Department);

            var pageList = AS_RoleFlow_Header_Records.Page(search.Page, search.PageSize, m => m.ID,
                m => (isSkipFlowCode || m.Flow_Code == search.FlowCode) &&
                     (m.AS_Users_Role.Any(sub => (isSkipBranch || sub.AS_Users.Branch_Code == search.Branch) &&
                                                 (isSkipDepartment || sub.AS_Users.Department_Code == search.Department))),
                entity => new ListItemResult
                {
                    FlowCode = entity.Flow_Code,
                    Status = entity.Transaction_Status,
                    Type = entity.Transaction_Type,
                    Count = entity.AS_Role_Flow.Count(),
                    FlowDate = entity.Create_Time.Value, //todo_julius 改schema
                    AppliedUser = entity.Created_By.ToString()
                });

            return new IndexResult
            {
                Items = pageList.Items,
                TotalAmount = pageList.TotalAmount
            };
        }

        public DetailModel Detail(string userCode, string flowCode)
        {
            try {
                var id = int.Parse(userCode);
                var userInfo = AS_Users_Records.First(m => m.ID == id);
                var selected = AS_Users_Role_Records.Find(m => m.AS_RoleFlow_Header.Flow_Code == flowCode).
                    Select(m => m.Role_ID).ToList();

                var formInfo = AS_RoleFlow_Header_Records.First(m => m.Flow_Code == flowCode);

                return new DetailModel
                {
                    UserCode = userInfo.User_Code,
                    UserName = userInfo.User_Name,
                    BranchName = userInfo.Branch_Code,
                    Department = userInfo.Department_Name,
                    HeaderInfo = new HeaderModel
                    {
                        FlowCode = formInfo.Flow_Code,
                        Status = formInfo.Transaction_Status,
                        AppliedUserId = formInfo.Created_By.Value,
                        //Type = formInfo.Transaction_Type
                        Reason = formInfo.Application_Reson
                    },
                    TotalRoles = AS_Role_Records.FindAll(m => m).AsEnumerable().Select(m => new RoleDetailModel
                    {
                        IsChecked = selected.Contains(m.ID),
                        ID = m.ID,
                        Name = m.Role_Name,
                        DocType = m.Doc_Type,
                        Programs = m.AS_Programs_Roles.Select(p => p.AS_Programs.Programs_Name).ToList()
                    }).ToList()
                };
            }
            catch (Exception ex) {
                return new DetailModel
                {
                    HeaderInfo = new HeaderModel(),
                    TotalRoles = AS_Role_Records.FindAll(m => m).AsEnumerable().Select(m => new RoleDetailModel
                    {
                        IsChecked = false,
                        ID = m.ID,
                        Name = m.Role_Name,
                        DocType = m.Doc_Type,
                        Programs = m.AS_Programs_Roles.Select(p => p.AS_Programs.Programs_Name).ToList()
                    }).ToList()
                };
            }
        }
    }
}
