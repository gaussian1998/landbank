﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.QueryUserRole.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.QueryUserRole
{
    public interface IQueryUserRole
    {
        IndexResult Index(SearchModel search);

        DetailModel Detail(string userCode, string flowCode);
    }
}
