﻿using System.Linq;
using Library.Entity.Edmx;
using Library.Servant.Servant.AccountingEntryStatistics.Models;
using Library.Servant.Servant.CodeTable;
using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Models;


namespace Library.Servant.Servant.AccountingEntryStatistics
{
    public class LocalAccountingEntryStatisticsServant : IAccountingEntryStatisticsServant
    {
        public BranchDepartmentOptions Options()
        {
            return BranchDepartmentOptions.Query();
        }

        public IndexResult Index(BranchSearchModel model)
        {
            var branchs = LocalCodeTableServant.BranchDictionary();
            return new IndexResult {

                Items = AS_GL_Acc_Months_Records.FindConvert(

                    model.Expression(),
                    entity => ((FR_Accounting_Report)entity).SetBranch(branchs[entity.Branch_Code])

                 ).OrderBy(report => report.FR_Account).ToList()
            };
        }
    }
}
