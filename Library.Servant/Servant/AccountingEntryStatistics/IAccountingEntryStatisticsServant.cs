﻿using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.AccountingEntryStatistics.Models;


namespace Library.Servant.Servant.AccountingEntryStatistics
{
    public interface IAccountingEntryStatisticsServant
    {
        BranchDepartmentOptions Options();
        IndexResult Index(BranchSearchModel model);
    }
}
