﻿using Library.Servant.Servant.CodeTable.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.AccountingEntryStatistics.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.AccountingEntryStatistics
{
    public class RemoteAccountingEntryStatisticsServant : IAccountingEntryStatisticsServant
    {
        public BranchDepartmentOptions Options()
        {
            return RemoteServant.Post<VoidModel, BranchDepartmentOptions>(

                            new VoidModel { },
                            "/AccountingEntryStatisticsServant/Options"
           );
        }

        public IndexResult Index(BranchSearchModel model)
        {
            return RemoteServant.Post<BranchSearchModel, IndexResult>(

                            model,
                            "/AccountingEntryStatisticsServant/Index"
            );
        }
    }
}
