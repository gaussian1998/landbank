﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.ExchangeRate.Model;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.ExchangeRate
{
    public interface IExchangeRateServant
    {
        MessageModel Create(CreateModel Create);
        IndexResult Index(SearchModel Search);
        MessageModel BatchDelete(BatchDeleteModel BatchDelete);
        DetailModel Detail(int ID);
    }
}
