﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.ExchangeRate.Model;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.ExchangeRate
{
    public class RemoteExchangeRateServant : IExchangeRateServant
    {
        public MessageModel BatchDelete(BatchDeleteModel BatchDelete)
        {
            return RemoteServant.Post<BatchDeleteModel, MessageModel>(
                            BatchDelete,
                            "/ExchangeRateServant/BatchDelete"
           );
        }

        public MessageModel Create(CreateModel Create)
        {
            return RemoteServant.Post<CreateModel, MessageModel>(
                            Create,
                            "/ExchangeRateServant/Create"
           );
        }

        public DetailModel Detail(int ID)
        {
            return RemoteServant.Post<IntModel, DetailModel>(
                            new IntModel { Value=ID},
                            "/ExchangeRateServant/Detail"
           );
        }

        public IndexResult Index(SearchModel Search)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(
                            Search,
                            "/ExchangeRateServant/Index"
           );
        }
    }
}
