﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.ExchangeRate.Model;
using Library.Entity.Edmx;
using Library.Common.Models;

namespace Library.Servant.Servant.ExchangeRate
{
    public class LocalExchangeRateServant : IExchangeRateServant
    {
        public MessageModel BatchDelete(BatchDeleteModel BatchDelete)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            foreach (int id in BatchDelete.IDList)
            {
                var item = AS_Rate_Records.First(x => x.ID == id);
                CreateModel _model = new CreateModel
                {
                    BranchCode = Convert.ToString(item.Branch_Code),
                    Department = item.Department,
                    AccountingBook = item.Book_Type,
                    Currency = item.Currency,
                    LastUpdateBy = BatchDelete.LastUpdateBy
                };

                decimal branchCode = Convert.ToDecimal(item.Branch_Code);
                string currency = item.Currency;

                AS_Rate_Records.Delete(x => x.ID == id);

                updateBranchCurrencyRate(_model);
            }
            return message;
        }

        public MessageModel Create(CreateModel Create)
        {
            int branchCode = Convert.ToInt32(Create.BranchCode);

            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            var currency = AS_Currency_Records.First(x => x.Currency == Create.Currency);
            Create.Basis = currency.Basis;
            //int found = AS_Rate_Records.Count(x => 
            //    x.Branch_Code == branchCode && 
            //    x.Department == Create.Department);
            int found = AS_Rate_Records.Count(x =>
               x.Rate_ID == Create.RateID &&
               x.Currency == Create.Currency);
            if (found > 0)
            {
                //update data
              //  AS_Rate_Records.Update(x =>
              //      x.Branch_Code == branchCode &&
              //      x.Department == Create.Department,

              //entity =>
              //    {
              //        entity.Rate_ID = Create.RateID;
              //        entity.Rate = Create.Rate;
              //        entity.Input_Date = Create.InputDate;
              //    });
                AS_Rate_Records.Update(x =>
                    x.Rate_ID == Create.RateID &&
               x.Currency == Create.Currency,

             entity =>
             {
                 entity.Book_Type = Create.AccountingBook;
                 entity.Currency = Create.Currency;
                 entity.Rate_ID = Create.RateID;
                 entity.Rate = Create.Rate;
                 entity.Input_Date = Create.InputDate;
                 entity.Basis = Create.Basis;
                 entity.Days_Of_Year = Create.InputDate.DayOfYear;
             });
            }
            else
            {
               

                //create data
                AS_Rate_Records.Create(new AS_Rate
                {
                    //Branch_Code = Convert.ToDecimal(Create.BranchCode),
                    //Department = Create.Department,
                    Book_Type = Create.AccountingBook,
                    Currency = Create.Currency,
                    Rate_ID = Create.RateID,
                    Rate = Create.Rate,
                    Input_Date = Create.InputDate,
                    Basis = Create.Basis,
                    Days_Of_Year = Create.InputDate.DayOfYear
                });
            }

            //update branch currency rate
            updateBranchCurrencyRate(Create);
            return message;
        }

        public DetailModel Detail(int ID)
        {
            var dbResult = AS_Rate_Records.First(x => x.ID == ID);
            DetailModel result = new DetailModel
            {
                ID = dbResult.ID,
                //BranchCode = Convert.ToInt32(dbResult.Branch_Code),
                //DepartmentName = dbResult.Department,
                Currency = dbResult.Currency,
                AccountingBook = dbResult.Book_Type,
                RateID = dbResult.Rate_ID,
                Rate = Convert.ToDecimal(dbResult.Rate),
                RateDate = Convert.ToDateTime(dbResult.Input_Date).ToString("yyyy-MM-dd"),
                DayOfYear = Convert.ToDateTime(dbResult.Input_Date).DayOfYear
            };
            return result;
        }

        public IndexResult Index(SearchModel Search)
        {
            bool isSkipBranchCode = Search.BranchCodeId == -1 ? true : false;
            bool isSkipAccountingBook = Search.AccountingBookCodeId == "-1" ? true : false;

            int realPage = (Search.CurrentPage - 1) >= 0 ? Search.CurrentPage - 1 : 0;

            IndexResult result = new IndexResult
            {
                Conditions = Search,
                Page = null
            };

            AS_RateRepository _rateRepo = RepositoryHelper.GetAS_RateRepository();

            var query = _rateRepo.Where(x => (isSkipAccountingBook || x.Book_Type == Search.AccountingBookCodeId) && (isSkipBranchCode || x.Branch_Code == Search.BranchCodeId));
            int totalAmount = query.Count();
            IEnumerable<AS_Rate> _dbResult = query.OrderByDescending(x => x.Input_Date).Skip(realPage * Search.PageSize).Take(Search.PageSize).ToList();
            List<DetailModel> _displayList = new List<DetailModel>();
            foreach (var item in _dbResult)
            {
                string branchCode = String.Format("{0:000}", item.Branch_Code);
                DetailModel _item = new DetailModel
                {
                    ID = item.ID,
                    //BranchCode = Convert.ToInt32(item.Branch_Code),
                    //BranchName = AS_Code_Table_Records.First(m => m.Class == "C01" && m.Code_ID == branchCode).Text,
                    //DepartmentName = item.Department,
                    AccountingBook = item.Book_Type,
                    Currency = item.Currency,
                    RateID = item.Rate_ID,
                    RateDate = Convert.ToDateTime(item.Input_Date).ToString("yyyy-MM-dd"),
                    Rate = Convert.ToDecimal(item.Rate)
                };
                _displayList.Add(_item);
            }
            PageList<DetailModel> _page = new PageList<DetailModel>()
            {
                TotalAmount = totalAmount,
                Items = _displayList
            };

            result.Page = _page;

            return result;
        }

        private void updateBranchCurrencyRate(CreateModel Create)
        {
            //update branch code
            AS_RateRepository _rateRepo = RepositoryHelper.GetAS_RateRepository();
            //decimal branchCode = Convert.ToDecimal(Create.BranchCode);
            List<int> _listBranchCode = AS_Branch_Currency_Records.Find(x => x.Currency == Create.Currency).Select(x =>(int)x.Branch_Code).ToList();
            //var lastRateItem = _rateRepo.All().Where(x => x.Branch_Code == branchCode && x.Currency == Create.Currency).OrderByDescending(x => x.Input_Date).FirstOrDefault();
            var lastRateItem = _rateRepo.All().Where(x =>  x.Currency == Create.Currency).OrderByDescending(x => x.Input_Date).FirstOrDefault();

            if (_listBranchCode == null || _listBranchCode.Count<=0)
                return;

            var lastRate = Convert.ToDecimal(lastRateItem.Rate);

            //int foundCount = AS_Branch_Currency_Records.Count(x => x.Branch_Code == branchCode && x.Currency == Create.Currency);
            foreach (int _branchCode in _listBranchCode)
            {
                int foundCount = AS_Branch_Currency_Records.Count(x =>  x.Currency == Create.Currency);
            if (foundCount > 0)
            {
                foreach(AS_Branch_Currency aSBranchCurrency in AS_Branch_Currency_Records.Find(x => x.Currency == Create.Currency)) { 
                     AS_Branch_Currency_Records.Update(x =>
                x.Branch_Code == aSBranchCurrency.Branch_Code &&
                x.Currency == Create.Currency,
                entity =>
                {
                    entity.Rate = lastRate;
                    entity.Book_Type = Create.AccountingBook;
                    entity.Last_Updated_Time = DateTime.Now;
                    entity.Last_Updated_By = Create.LastUpdateBy;
                });
                    }
               
            }
            //else
            //{
            //    //need to create branch currency
            //    AS_Branch_Currency_Records.Create(new AS_Branch_Currency
            //    {
            //        Branch_Code = _branchCode,
            //        Department = null,
            //        Book_Type = Create.AccountingBook,
            //        Currency = Create.Currency,
            //        Rate = lastRate,
            //        Last_Updated_By = Create.LastUpdateBy,
            //        Last_Updated_Time = DateTime.Now
            //    });
            //}
            }
        }
    }
}
