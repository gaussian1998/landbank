﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Common.Models;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.ExchangeRate.Model
{
    public class SearchModel : InvasionEncryption
    {
        public int BranchCodeId { get; set; }
        public string AccountingBookCodeId { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }

    }
}
