﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Common.Models;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.ExchangeRate.Model
{
    public class BatchDeleteModel : InvasionEncryption
    {
        public IList<int> IDList { get; set; }
        public int LastUpdateBy { get; set; }
    }
}
