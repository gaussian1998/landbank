﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Common.Models;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.ExchangeRate.Model
{
    public class DetailModel : InvasionEncryption
    {
        public int ID { get; set; }
        public string BranchName { get; set; }
        public int BranchCode { get; set; }
        public string DepartmentName { get; set; }
        public string AccountingBook { get; set; }
        public string Currency { get; set; }
        public string RateID { get; set; }
        public Decimal Rate { get; set; }
        public string RateDate { get; set; }
        public int DayOfYear { get; set; }
        public Decimal Basis { get; set; }
    }
}
