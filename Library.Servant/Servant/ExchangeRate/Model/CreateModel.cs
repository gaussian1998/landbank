﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Common.Models;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.ExchangeRate.Model
{
    public class CreateModel : InvasionEncryption
    {
        public string BranchCode { get; set; }
        public string Department { get; set; }
        public string AccountingBook { get; set; }
        public string Currency { get; set; }
        public string RateID { get; set; }
        public decimal Rate { get; set; }
        public DateTime InputDate { get; set; }
        public decimal Basis { get; set; }
        public int LastUpdateBy { get; set; }
    }
}
