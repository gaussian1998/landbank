﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.KeepPosition.Models;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.KeepPosition
{
    public class LocalKeepPositionServant : IKeepPositionServant
    {
        public MessageModel BatchDelete(BatchDeleteModel BatchDelete)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            foreach (int id in BatchDelete.IDList)
            {
                AS_Keep_Position_Records.Delete(x => x.ID == id);
            }
            return message;
        }

        public MessageModel Create(DetailModel Create)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };

            AS_Keep_PositionRepository _repo = RepositoryHelper.GetAS_Keep_PositionRepository();
            List<AS_Keep_Position> result = BreakDown(Create.PositionCode, Create.PositionName);
            int ParentId = 1;
            DateTime lastUpdateTime = DateTime.Now;

            foreach (var item in result)
            {
                //get id first
                AS_Keep_Position found = _repo.All().FirstOrDefault(x => x.Keep_Position_Code == item.Keep_Position_Code);
                if (found == null)
                {
                    found = new AS_Keep_Position
                    {
                        Parent_ID = ParentId,
                        Layer = item.Layer,
                        Keep_Position_Code = item.Keep_Position_Code,
                        Keep_Position_Name = item.Keep_Position_Name,
                        Keep_Branch_Code = item.Keep_Branch_Code,
                        Keep_Branch_Name = item.Keep_Branch_Name,
                        Keep_Dept = String.IsNullOrEmpty(item.Keep_Dept) ? null : item.Keep_Dept,
                        Keep_Dept_Name = String.IsNullOrEmpty(item.Keep_Dept_Name) ? null : item.Keep_Dept_Name,
                        Keep_SubDept_Code = String.IsNullOrEmpty(item.Keep_SubDept_Code) ? null : item.Keep_SubDept_Code,
                        Keep_SubDept_Name = String.IsNullOrEmpty(item.Keep_SubDept_Name) ? null : item.Keep_SubDept_Name,
                        Keep_Floor = String.IsNullOrEmpty(item.Keep_Floor) ? null : item.Keep_Floor,
                        Keep_Floor_Name = String.IsNullOrEmpty(item.Keep_Floor_Name) ? null : item.Keep_Floor_Name,
                        Last_Updated_By = Create.LastUpdateBy,
                        Last_Updated_Time = lastUpdateTime
                    };

                    AS_Keep_Position_Records.Create(found);
                }
                ParentId = found.ID;
            }
            return message;
        }

        public MessageModel Update(DetailModel Update)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };

            AS_Keep_PositionRepository _repo = RepositoryHelper.GetAS_Keep_PositionRepository();
            List<AS_Keep_Position> result = BreakDown(Update.PositionCode, Update.PositionName);
            int ParentId = 1;
            DateTime lastUpdateTime = DateTime.Now;

            for (int i= 0; i<result.Count - 1; i++)
            {
                var item = result[i];
                //get id first
                AS_Keep_Position found = _repo.All().FirstOrDefault(x => x.Keep_Position_Code == item.Keep_Position_Code);
                if (found == null)
                {
                    found = new AS_Keep_Position
                    {
                        Parent_ID = ParentId,
                        Layer = item.Layer,
                        Keep_Position_Code = item.Keep_Position_Code,
                        Keep_Position_Name = item.Keep_Position_Name,
                        Keep_Branch_Code = item.Keep_Branch_Code,
                        Keep_Branch_Name = item.Keep_Branch_Name,
                        Keep_Dept = String.IsNullOrEmpty(item.Keep_Dept) ? null : item.Keep_Dept,
                        Keep_Dept_Name = String.IsNullOrEmpty(item.Keep_Dept_Name) ? null : item.Keep_Dept_Name,
                        Keep_SubDept_Code = String.IsNullOrEmpty(item.Keep_SubDept_Code) ? null : item.Keep_SubDept_Code,
                        Keep_SubDept_Name = String.IsNullOrEmpty(item.Keep_SubDept_Name) ? null : item.Keep_SubDept_Name,
                        Keep_Floor = String.IsNullOrEmpty(item.Keep_Floor) ? null : item.Keep_Floor,
                        Keep_Floor_Name = String.IsNullOrEmpty(item.Keep_Floor_Name) ? null : item.Keep_Floor_Name,
                        Last_Updated_By = Update.LastUpdateBy,
                        Last_Updated_Time = lastUpdateTime
                    };

                    AS_Keep_Position_Records.Create(found);
                }
                ParentId = found.ID;
            }

            //update the last one
            var lastItem = result[result.Count - 1];
            AS_Keep_Position_Records.Update(x => x.ID == Update.ID, m => {
                m.Layer = lastItem.Layer;
                m.Parent_ID = ParentId;
                m.Keep_Position_Code = lastItem.Keep_Position_Code;
                m.Keep_Position_Name = lastItem.Keep_Position_Name;
                m.Keep_Branch_Code = lastItem.Keep_Branch_Code;
                m.Keep_Branch_Name = lastItem.Keep_Branch_Name;
                m.Keep_Dept = lastItem.Keep_Dept;
                m.Keep_Dept_Name = lastItem.Keep_Dept_Name;
                m.Keep_SubDept_Code = lastItem.Keep_SubDept_Code;
                m.Keep_SubDept_Name = lastItem.Keep_SubDept_Name;
                m.Keep_Floor = lastItem.Keep_Floor;
                m.Keep_Floor_Name = lastItem.Keep_Floor_Name;
                m.Last_Updated_By = Update.LastUpdateBy;
                m.Last_Updated_Time = lastUpdateTime;
            });

            return message;
        }

        public IndexResult Index(SearchModel Search)
        {
            bool skipLayer = Search.Layer == -1 ? true : false;
            bool skipBranch = string.IsNullOrEmpty(Search.BranchCode) ? true : false;
            bool skipDept = String.IsNullOrEmpty(Search.DeptCode) ? true : false;
            bool skipSubDept = String.IsNullOrEmpty(Search.SubDeptCode) ? true : false;

            IndexResult _result = new IndexResult
            {
                Conditions = Search,
                Page = AS_Keep_Position_Records.Page(
                    Search.CurrentPage,
                    Search.PageSize,
                    x => x.Keep_Position_Code,
                    x => (skipBranch || x.Keep_Branch_Code == Search.BranchCode) && 
                         (skipDept || x.Keep_Dept == Search.DeptCode) &&
                         (skipSubDept || x.Keep_SubDept_Code == Search.SubDeptCode) &&
                         (skipLayer || x.Layer == Search.Layer),
                    m => new DetailModel {
                        ID = m.ID,
                        ParentID = (m.Parent_ID ?? 0),
                        BranchCode = m.Keep_Branch_Code,
                        BranchName = m.Keep_Branch_Name,
                        DeptCode = m.Keep_Dept,
                        DeptName = m.Keep_Dept_Name,
                        SubDeptCode = m.Keep_SubDept_Code,
                        SubDeptName = m.Keep_SubDept_Name,
                        PositionCode = m.Keep_Position_Code,
                        PositionName = m.Keep_Position_Name,
                        Layer = m.Layer,
                        Floor = m.Keep_Floor,
                        FloorName = m.Keep_Floor_Name
                    }
                    )
            };
            return _result;

        }


        public DetailModel Detail(int ID)
        {
            DetailModel result = AS_Keep_Position_Records.First(x => x.ID == ID, m => new DetailModel
            {
                ID = m.ID,
                ParentID = (m.Parent_ID ?? 0),
                BranchCode = m.Keep_Branch_Code,
                BranchName = m.Keep_Branch_Code+ m.Keep_Branch_Name,
                DeptCode = m.Keep_Dept,
                DeptName = m.Keep_Dept+ m.Keep_Dept_Name,
                SubDeptCode = m.Keep_SubDept_Code,
                SubDeptName = m.Keep_SubDept_Name,
                PositionCode = m.Keep_Position_Code,
                PositionName = m.Keep_Position_Name,
                Layer = m.Layer,
                Floor = m.Keep_Floor,
                FloorName = m.Keep_Floor_Name
            });

            return result;
        }

        public List<SelectedModel> GetSubDept()
        {
            List<SelectedModel> result = AS_Keep_Position_Records.Find(x => x.Layer == 3, m => new SelectedModel { Value = m.Keep_Position_Code, Name = m.Keep_Position_Code+ m.Keep_SubDept_Name });
            return result;
        }

        private List<AS_Keep_Position> BreakDown(string PositionCode, string PositionName)
        {
            List<string> codes = PositionCode.Split('.').ToList();
            List<string> names = PositionName.Split('/').ToList();

            List<AS_Keep_Position> result = new List<AS_Keep_Position>();
            string previousCode = "";
            string previousName = "";

            string branchCode, branchName, deptCode, deptName, subDeptCode, subDeptName, floorCode, floorName;
            branchCode = branchName = deptCode = deptName = subDeptCode = subDeptName = floorCode = floorName = String.Empty;

            for (int i = 0; i < codes.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        branchCode = codes[i];
                        branchName = names[i];
                        break;

                    case 1:
                        deptCode = codes[i];
                        deptName = names[i];
                        break;

                    case 2:
                        subDeptCode = codes[i];
                        subDeptName = names[i];
                        break;

                    case 3:
                        floorCode = codes[i];
                        floorName = names[i];
                        break;
                }

                if (previousCode == "")
                {
                    previousCode = codes[i];
                }
                else
                {
                    previousCode += String.Format(".{0}", codes[i]);
                }

                if (previousName == "")
                {
                    previousName = names[i];
                }
                else
                {
                    previousName += String.Format("/{0}", names[i]);
                }

                AS_Keep_Position item = new AS_Keep_Position
                {
                    Layer = (i+1),
                    Keep_Position_Code = previousCode,
                    Keep_Position_Name = previousName,
                    Keep_Branch_Code = branchCode,
                    Keep_Branch_Name = branchName,
                    Keep_Dept = deptCode,
                    Keep_Dept_Name = deptName,
                    Keep_SubDept_Code = subDeptCode,
                    Keep_SubDept_Name = subDeptName,
                    Keep_Floor = floorCode,
                    Keep_Floor_Name = floorName
                };
                result.Add(item);
            }

            return result;
        }
    }
}
