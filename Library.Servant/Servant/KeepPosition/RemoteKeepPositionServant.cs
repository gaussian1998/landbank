﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.KeepPosition.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.KeepPosition
{
    public class RemoteKeepPositionServant : IKeepPositionServant
    {
        public MessageModel BatchDelete(BatchDeleteModel BatchDelete)
        {
            return RemoteServant.Post<BatchDeleteModel, MessageModel>(
                            BatchDelete,
                            "/KeepPositionServant/BatchDelete"
           );
        }

        public MessageModel Create(DetailModel Create)
        {
            return RemoteServant.Post<DetailModel, MessageModel>(
                           Create,
                           "/KeepPositionServant/Create"
          );
        }

        public IndexResult Index(SearchModel Search)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(
                           Search,
                           "/KeepPositionServant/Index"
          );
        }

        public MessageModel Update(DetailModel Update)
        {
            return RemoteServant.Post<DetailModel, MessageModel>(
                           Update,
                           "/KeepPositionServant/Update"
          );
        }

        public DetailModel Detail(int ID)
        {
            return RemoteServant.Post<IntModel, DetailModel>(
                           new IntModel { Value=ID},
                           "/KeepPositionServant/Detail"
          );
        }

        public List<SelectedModel> GetSubDept()
        {
            return (RemoteServant.Post<VoidModel, ListModel>(
                           new VoidModel {},
                           "/KeepPositionServant/GetSubDept"
          )).ListSelectedModel;
        }
    }
}
