﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.KeepPosition.Models
{
    public class DetailModel : InvasionEncryption
    {
        public int ID { get; set; }
        public int Layer { get; set; }
        public int ParentID { get; set; }
        public string PositionCode { get; set; }
        public string PositionName { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string DeptCode { get; set; }
        public string DeptName { get; set; }
        public string SubDeptCode { get; set; }
        public string SubDeptName { get; set; }
        public string Floor { get; set; }
        public string FloorName { get; set; }
        public int LastUpdateBy { get; set; }
    }
}
