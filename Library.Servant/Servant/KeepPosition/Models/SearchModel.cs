﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.KeepPosition.Models
{
    public class SearchModel : InvasionEncryption
    {
        public int Layer { get; set; }
        public string BranchCode { get; set; }
        public string DeptCode { get; set; }
        public string SubDeptCode { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
    }
}
