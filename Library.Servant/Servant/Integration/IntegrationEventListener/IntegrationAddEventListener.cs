﻿using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servants.AbstractFactory;

namespace Library.Servant.Servant.Integration.IntegrationEventListener
{
    class IntegrationAddEventListener : ExampleListener, IHttpGetProvider
    {
        private IIGServant IGServant = ServantAbstractFactory.Integration();

        public HttpGetViewModel GetUrl(string FormNo, string carryData)
        {
            return new HttpGetViewModel { ControllerName = "IntegrationAdded", ActionName = "Edit", Parameter = "id=" + FormNo + "&signmode=Y&CanEdit=false"};
        }

        //進行簽核時發生
        public override bool OnApproval(string formNo, string carry, AS_LandBankEntities context)
        {
            bool result = true;
            IGServant.UpdateApprovalStatus(formNo, "1");
            return result;
        }

        //簽核通過時發生
        public override bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            bool result = true;
            IGServant.UpdateApprovalStatus(formNo, "2");
            return result;
        }
       
        //被退簽時發生
        public override bool OnReject(string formNo, string carry, AS_LandBankEntities context)
        {
            bool result = true;
            IGServant.UpdateApprovalStatus(formNo, "3");
            return result;
        }

        //被經辦抽回時發生
        public override bool OnWithdraw(string formNo, string carry, AS_LandBankEntities context)
        {
            bool result = true;
            IGServant.UpdateApprovalStatus(formNo, "4");
            return result;
        }

        //簽核廢棄時發生
        public override bool OnRevoke(string formNo, string carry, AS_LandBankEntities context)
        {
            bool result = true;
            IGServant.UpdateApprovalStatus(formNo, "5");
            return result;
        }
    }
}
