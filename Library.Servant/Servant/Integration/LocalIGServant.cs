﻿using Library.Servant.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Integration.IntegrationModels;
using Library.Servant.Repository.Integration;
using Library.Entity.Edmx;
using System.Transactions;

namespace Library.Servant.Servant.Integration
{
    public class LocalIGServant : IIGServant
    {
        /// <summary>
        /// 取得單據列表
        /// </summary>
        /// <returns></returns>
        public IndexModel Index(SearchModel condition)
        {
            IndexModel result = null;

            var query = IntegrationGerneral.GetIndex(condition);

            if (query != null)
            {
                result = new IndexModel();
                result.TotalAmount = query.Count();
                result.Items = query;
                result.condition = condition;

                //分頁
                if (condition.Page != null)
                {
                    //選取該頁之資訊
                    result.Items = query.OrderByDescending(m => m.Create_Time).Skip((condition.Page.Value - 1) * Convert.ToInt32(condition.PageSize)).Take(Convert.ToInt32(condition.PageSize));
                }
            }

            return result;
        }

        /// <summary>
        /// 新增單據編號
        /// </summary>
        /// <returns></returns>
        public string CreateTRXID(string Type)
        {
            string CToDay = (DateTime.Today.Year - 1911).ToString().PadLeft(3, '0') + DateTime.Today.ToString("MMdd") + Type;
            string TRXID = "";

            using (var db = new AS_LandBankEntities())
            {
                TRXID = db.AS_Leases_Integration_Header.Where(mp => mp.Transaction_Type == Type && mp.TRX_Header_ID.StartsWith(CToDay)).Max(o => o.TRX_Header_ID);
            }
            if (TRXID != null)
            {
                int Seq = Convert.ToInt32(TRXID.Substring(11)) + 1;
                TRXID = CToDay + Seq.ToString().PadLeft(6, '0');
            }
            else
            {
                TRXID = CToDay + "000001";
            }
            DeleteContractByTRXHeaderID(TRXID);
            return TRXID;
        }

        /// <summary>
        /// 更新簽核狀態
        /// </summary>
        /// <returns></returns>
        public bool UpdateApprovalStatus(string TRXHeaderID, string FlowStatus)
        {
            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {

                        AS_Leases_Integration_Header header = db.AS_Leases_Integration_Header.First(o => o.TRX_Header_ID == TRXHeaderID);
                        header.Last_Updated_Time = DateTime.Now;
                        header.Flow_Status = FlowStatus;
                        db.Entry(header).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();                        
                    }
                    trans.Complete();
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// 取得租約編號
        /// </summary>
        /// <returns></returns>
        public string CreateContractTRXID(string Type)
        {
            string CToDay = (DateTime.Today.Year - 1911).ToString().PadLeft(3, '0') + "-" + Type;
            string TRXID = "";

            using (var db = new AS_LandBankEntities())
            {
                TRXID = db.AS_Leases_Headers.Where(mp => mp.LEASE_CATEGORY == Type && mp.LEASE_NUMBER.StartsWith(CToDay)).Max(o => o.LEASE_NUMBER);
            }
            if (TRXID != null)
            {
                int Seq = Convert.ToInt32(TRXID.Substring(5)) + 1;
                TRXID = CToDay + Seq.ToString().PadLeft(4, '0');
            }
            else
            {
                TRXID = CToDay + "0001";
            }

            return TRXID;
        }

        /// <summary>
        /// 取得詳細資料
        /// </summary>
        /// <returns></returns>
        public IntegrationModel GetIntegrationDetail(string TRXHeaderID, string Transaction_Type)
        {
            IntegrationModel result = IntegrationGerneral.GetIntegrationDetail(TRXHeaderID, Transaction_Type);
            return result;
        }

        /// <summary>
        /// 刪除單據
        /// </summary>
        /// <returns></returns>
        public bool DeleteIntegration(int ID)
        {
            bool result = true;
            try
            {
                using (var db = new AS_LandBankEntities())
                {                    
                    db.AS_Leases_Integration_Header.Remove(db.AS_Leases_Integration_Header.FirstOrDefault(m => m.ID == ID));
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// 取得租約詳細資料
        /// </summary>
        /// <returns></returns>
        public ContactModel GetContractDetail(int id)
        {
            ContactModel result = IntegrationGerneral.GetContractDetail(id);
            return result;
        }

        /// <summary>
        /// 新增/編輯單據
        /// </summary>
        /// <returns></returns>
        public bool CreateIntegration(IntegrationHeaderModel model)
        {
            try
            {
                using (var db = new AS_LandBankEntities())
                {
                    if (!db.AS_Leases_Integration_Header.Any(h => h.TRX_Header_ID == model.TRX_Header_ID))
                    {
                        AS_Leases_Integration_Header header = IntegrationGerneral.ToIntegrationEntity(model);
                        db.AS_Leases_Integration_Header.Add(header);
                        db.Entry(header).State = System.Data.Entity.EntityState.Added;
                    }
                    else
                    {
                        AS_Leases_Integration_Header header = db.AS_Leases_Integration_Header.FirstOrDefault(m => m.TRX_Header_ID == model.TRX_Header_ID);
                        header.TRX_Header_ID = model.TRX_Header_ID;
                        header.Transaction_Time = model.Transaction_Time;
                        header.Flow_Status = model.Flow_Status;
                        header.Source = model.Source;
                        header.Book_Type = model.Book_Type;
                        header.Office_Branch = model.Office_Branch;
                        header.OriOffice_Branch = model.OriOffice_Branch;
                        header.Remark = model.Remark;
                        header.Last_Updated_By = model.Last_Updated_By;
                        header.Last_Updated_Time = model.Last_Updated_Time;
                        db.Entry(header).State = System.Data.Entity.EntityState.Modified;                        
                    }
                    db.SaveChanges();
                };
                return true;                
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 新增/編輯租約
        /// </summary>
        /// <returns></returns>
        public bool CreateContract(ContactModel model)
        {
            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        #region 新增/編輯租約表頭資料
                        if (!db.AS_Leases_Headers.Any(h => h.LEASE_NUMBER == model.LEASE_NUMBER))
                        {
                            AS_Leases_Headers header = IntegrationGerneral.ToContractEntity(model);
                            db.AS_Leases_Headers.Add(header);
                            db.Entry(header).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }
                        else
                        {
                            AS_Leases_Headers header = db.AS_Leases_Headers.FirstOrDefault(m => m.LEASE_NUMBER == model.LEASE_NUMBER);
                            header.OLD_LEASE_NUMBER = model.OLD_LEASE_NUMBER;
                            header.TRANSACTION_TYPE = model.TRANSACTION_TYPE;
                            header.OFFICER_BRANCH = model.OFFICER_BRANCH;
                            header.LEASE_TYPE = model.LEASE_TYPE;
                            header.LEASE_TRANSACTION_TYPE = model.LEASE_TRANSACTION_TYPE;
                            header.LEASE_STATUS = model.LEASE_STATUS;
                            header.PAYMENT_METHOD = model.PAYMENT_METHOD;
                            header.BUSINESS_TYPE = model.BUSINESS_TYPE;
                            header.PAYMENT_TYPE = model.PAYMENT_TYPE;
                            header.CHECK_TYPE = model.CHECK_TYPE;
                            header.LEASE_INCEPTION_DATE = model.LEASE_INCEPTION_DATE;
                            header.LEASE_TERMINATION_DATE = model.LEASE_TERMINATION_DATE;
                            header.ACCOUNT_NUMBER = model.ACCOUNT_NUMBER;
                            header.DESCRIPTION = model.DESCRIPTION;
                            header.ACCEPT_DATE = model.ACCEPT_DATE;
                            header.LEASE_CATEGORY = model.LEASE_CATEGORY;
                            header.LEASE_AREA = model.LEASE_AREA;
                            header.LEASE_RATE = model.LEASE_RATE;
                            header.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
                            header.LAST_UPDATE_DATE = model.Last_Updated_Time;
                            db.Entry(header).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        #endregion

                        int ContractID = db.AS_Leases_Headers.FirstOrDefault(m => m.LEASE_NUMBER == model.LEASE_NUMBER && m.TRANSACTION_TYPE == model.TRANSACTION_TYPE).ID;

                        #region 新增/編輯租金資料
                        List<int> RentIDs = new List<int>();
                        foreach (RentModel item in model.RentList)
                        {
                            RentIDs.Add(item.ID);
                        }
                        db.AS_Lease_Rent.RemoveRange(db.AS_Lease_Rent.Where(m => !RentIDs.Contains(m.ID) && m.LEASE_HEADER_ID == ContractID));
                        db.SaveChanges();
                        foreach (RentModel rent in model.RentList)
                        {
                            if (rent.ID == 0)
                            {
                                rent.LEASE_HEADER_ID = ContractID;
                                rent.LEASE_NUMBER = model.LEASE_NUMBER;
                                rent.Created_By = model.CREATED_BY;
                                rent.Last_Updated_By = model.Last_Updated_By;
                                AS_Lease_Rent header = IntegrationGerneral.ToRentEntity(rent);
                                db.AS_Lease_Rent.Add(header);
                                db.Entry(header).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                            else
                            {
                                AS_Lease_Rent header = db.AS_Lease_Rent.FirstOrDefault(m => m.ID == rent.ID);
                                header.START_DATE = rent.START_DATE;
                                header.END_DATE = rent.END_DATE;
                                header.CALCULATE_METHOD = rent.CALCULATE_METHOD;
                                header.PAYMENT_PERIOD = rent.PAYMENT_PERIOD;
                                header.DUE_DAY = (string.IsNullOrEmpty(rent.DUE_DAY)) ? 0 : int.Parse(rent.DUE_DAY);
                                header.LAST_PAYMENT = rent.LAST_PAYMENT;
                                header.NEXT_PAYMENT = rent.NEXT_PAYMENT;
                                header.EFFECTIVE_DATE = rent.EFFECTIVE_DATE;
                                header.PRICE = rent.PRICE;
                                header.CURRENCY = rent.CURRENCY;
                                header.CURRENCY_VALUE = rent.CURRENCY_VALUE;
                                header.DESCRIPTION = model.DESCRIPTION;
                                header.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
                                header.LAST_UPDATE_DATE = model.Last_Updated_Time;
                                db.Entry(header).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        #endregion

                        #region 新增/編輯管理費資料
                        List<int> ManageIDs = new List<int>();
                        foreach (ManageExpenseModel item in model.ManageList)
                        {
                            ManageIDs.Add(item.ID);
                        }
                        db.AS_Lease_ManageExpense.RemoveRange(db.AS_Lease_ManageExpense.Where(m => !ManageIDs.Contains(m.ID) && m.LEASE_HEADER_ID == ContractID));
                        db.SaveChanges();
                        foreach (ManageExpenseModel item in model.ManageList)
                        {
                            if (item.ID == 0)
                            {
                                item.LEASE_HEADER_ID = ContractID;
                                item.LEASE_NUMBER = model.LEASE_NUMBER;
                                item.Created_By = model.CREATED_BY;
                                item.Last_Updated_By = model.Last_Updated_By;
                                AS_Lease_ManageExpense header = IntegrationGerneral.ToManageExpenseEntity(item);
                                db.AS_Lease_ManageExpense.Add(header);
                                db.Entry(header).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                            else
                            {
                                AS_Lease_ManageExpense header = db.AS_Lease_ManageExpense.FirstOrDefault(m => m.ID == item.ID);
                                header.START_DATE = item.START_DATE;
                                header.END_DATE = item.END_DATE;
                                header.CALCULATE_METHOD = item.CALCULATE_METHOD;
                                header.PAYMENT_PERIOD = item.PAYMENT_PERIOD;
                                header.DUE_DAY = (string.IsNullOrEmpty(item.DUE_DAY)) ? 0 : int.Parse(item.DUE_DAY);
                                header.LAST_PAYMENT = item.LAST_PAYMENT;
                                header.NEXT_PAYMENT = item.NEXT_PAYMENT;
                                header.EFFECTIVE_DATE = item.EFFECTIVE_DATE;
                                header.PRICE = item.PRICE;
                                header.CURRENCY = item.CURRENCY;
                                header.CURRENCY_VALUE = item.CURRENCY_VALUE;
                                header.DESCRIPTION = item.DESCRIPTION;
                                header.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
                                header.LAST_UPDATE_DATE = model.Last_Updated_Time;
                                db.Entry(header).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        #endregion

                        #region 新增/編輯保證金資料
                        List<int> MarginIDs = new List<int>();
                        foreach (MarginModel item in model.MarginList)
                        {
                            MarginIDs.Add(item.ID);
                        }
                        db.AS_Lease_Margin.RemoveRange(db.AS_Lease_Margin.Where(m => !MarginIDs.Contains(m.ID) && m.LEASE_HEADER_ID == ContractID));
                        db.SaveChanges();

                        foreach (MarginModel item in model.MarginList)
                        {
                            if (item.ID == 0)
                            {
                                item.LEASE_HEADER_ID = ContractID;
                                item.LEASE_NUMBER = model.LEASE_NUMBER;
                                item.Created_By = model.CREATED_BY;
                                item.Last_Updated_By = model.Last_Updated_By;
                                AS_Lease_Margin header = IntegrationGerneral.ToMarginEntity(item);
                                db.AS_Lease_Margin.Add(header);
                                db.Entry(header).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                            else
                            {
                                AS_Lease_Margin header = db.AS_Lease_Margin.FirstOrDefault(m => m.ID == item.ID);
                                header.RECEIVED_DATE = item.RECEIVED_DATE;
                                header.RETURN_DATE = item.RETURN_DATE;
                                header.BANK = item.BANK;
                                header.PRICE = item.PRICE;
                                header.CURRENCY = item.CURRENCY;
                                header.CURRENCY_VALUE = item.CURRENCY_VALUE;
                                header.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
                                header.LAST_UPDATE_DATE = model.Last_Updated_Time;
                                db.Entry(header).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        #endregion

                        #region 新增/編輯土地資料
                        List<int> LandIDs = new List<int>();
                        foreach (LandModel item in model.LandList)
                        {
                            LandIDs.Add(item.ID);
                        }
                        db.AS_Leases_Land.RemoveRange(db.AS_Leases_Land.Where(m => !LandIDs.Contains(m.ID) && m.LEASE_HEADER_ID == ContractID));
                        db.SaveChanges();
                        foreach (LandModel item in model.LandList)
                        {
                            if (item.ID == 0)
                            {
                                item.LEASE_HEADER_ID = ContractID;
                                item.LEASE_NUMBER = model.LEASE_NUMBER;
                                item.Created_By = model.CREATED_BY;
                                item.Last_Updated_By = model.Last_Updated_By;
                                AS_Leases_Land header = IntegrationGerneral.ToLandEntity(item);
                                db.AS_Leases_Land.Add(header);
                                db.Entry(header).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                            else
                            {
                                AS_Leases_Land header = db.AS_Leases_Land.FirstOrDefault(m => m.ID == item.ID);
                                header.CITY_NAME = item.CITY_NAME;
                                header.DISTRICT_NAME = item.DISTRICT_NAME;
                                header.SECTION_NAME = item.SECTION_NAME;
                                header.SUBSECTION_NAME = item.SUBSECTION_NAME;
                                header.PARENT_LAND_NUMBER = item.PARENT_LAND_NUMBER;
                                header.FILIAL_LAND_NUMBER = item.FILIAL_LAND_NUMBER;
                                header.LAND_USE_TYPE = item.LAND_USE_TYPE;
                                header.AUTHORIZED_AREA = item.AUTHORIZED_AREA;
                                header.ANNOUNCE_PRICE = item.ANNOUNCE_PRICE;
                                header.LESSEE_AREA = item.LESSEE_AREA;
                                header.LEASE_RATE = item.LEASE_RATE;
                                header.RENTAL_AMOUNT = item.RENTAL_AMOUNT;
                                header.DESCRIPTION = item.DESCRIPTION;
                                header.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
                                header.LAST_UPDATE_DATE = model.Last_Updated_Time;
                                db.Entry(header).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        #endregion

                        #region 新增/編輯停車位資料
                        List<int> ParkingIDs = new List<int>();
                        foreach (ParkingModel item in model.ParkingList)
                        {
                            ParkingIDs.Add(item.ID);
                        }
                        db.AS_Leases_Build_Parking.RemoveRange(db.AS_Leases_Build_Parking.Where(m => !ParkingIDs.Contains(m.ID) && m.LEASE_HEADER_ID == ContractID));
                        db.SaveChanges();

                        foreach (ParkingModel item in model.ParkingList)
                        {
                            if (item.ID == 0)
                            {
                                item.LEASE_HEADER_ID = ContractID;
                                item.LEASE_NUMBER = model.LEASE_NUMBER;
                                item.Created_By = model.CREATED_BY;
                                item.Last_Updated_By = model.Last_Updated_By;
                                AS_Leases_Build_Parking header = IntegrationGerneral.ToParkingEntity(item);
                                db.AS_Leases_Build_Parking.Add(header);
                                db.Entry(header).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                            else
                            {
                                AS_Leases_Build_Parking header = db.AS_Leases_Build_Parking.FirstOrDefault(m => m.ID == item.ID);
                                header.CITY_NAME = item.CITY_NAME;
                                header.DISTRICT_NAME = item.DISTRICT_NAME;
                                header.SECTION_NAME = item.SECTION_NAME;
                                header.SUBSECTION_NAME = item.SUBSECTION_NAME;
                                header.PARKING_NO = item.PARKING_NO;
                                header.PARKING_USE_TYPE = item.PARKING_USE_TYPE;
                                header.FLOOR = item.FLOOR;
                                header.DESCRIPTION = item.DESCRIPTION;
                                header.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
                                header.LAST_UPDATE_DATE = model.Last_Updated_Time;
                                db.Entry(header).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        #endregion

                        #region 新增/編輯房屋資料
                        List<int> BuildIDs = new List<int>();
                        foreach (BuildModel item in model.BuildList)
                        {
                            BuildIDs.Add(item.ID);
                        }
                        db.AS_Leases_Build.RemoveRange(db.AS_Leases_Build.Where(m => !BuildIDs.Contains(m.ID) && m.LEASE_HEADER_ID == ContractID));
                        db.SaveChanges();
                        foreach (BuildModel item in model.BuildList)
                        {
                            if (item.ID == 0)
                            {
                                item.LEASE_HEADER_ID = ContractID;
                                item.LEASE_NUMBER = model.LEASE_NUMBER;
                                item.Created_By = model.CREATED_BY;
                                item.Last_Updated_By = model.Last_Updated_By;
                                AS_Leases_Build header = IntegrationGerneral.ToBuildEntity(item);
                                db.AS_Leases_Build.Add(header);
                                db.Entry(header).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                            else
                            {
                                AS_Leases_Build header = db.AS_Leases_Build.FirstOrDefault(m => m.ID == item.ID);
                                header.CITY_NAME = item.CITY_NAME;
                                header.DISTRICT_NAME = item.DISTRICT_NAME;
                                header.SECTION_NAME = item.SECTION_NAME;
                                header.SUBSECTION_NAME = item.SUBSECTION_NAME;
                                header.ADDRESS = item.ADDRESS;
                                header.BUSINESS_USE_TYPE = item.BUSINESS_USE_TYPE;
                                header.ATTACHED_BUILD = item.ATTACHED_BUILD;
                                header.HOUSE_ARCADE = item.HOUSE_ARCADE;
                                header.CONVEX = item.CONVEX;
                                header.ARCADE = item.ARCADE;
                                header.COMMON_PART = item.COMMON_PART;
                                header.RIGHT_AREA = item.RIGHT_AREA;
                                header.LEASES_AREA = item.LEASES_AREA;
                                header.FLOOR = item.FLOOR;
                                header.ARCADE_AREA = item.ARCADE_AREA;
                                header.SHARE_PUBLIC_AREA = item.SHARE_PUBLIC_AREA;
                                header.DESCRIPTION = item.DESCRIPTION;
                                header.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
                                header.LAST_UPDATE_DATE = model.Last_Updated_Time;
                                db.Entry(header).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        #endregion

                        #region 新增/編輯繳款資料
                        List<int> PaymentIDs = new List<int>();
                        foreach (PaymentAccountModel item in model.PaymentList)
                        {
                            PaymentIDs.Add(item.ID);
                        }
                        db.AS_Lease_Payment_Account.RemoveRange(db.AS_Lease_Payment_Account.Where(m => !PaymentIDs.Contains(m.ID) && m.LEASE_HEADER_ID == ContractID));
                        db.SaveChanges();
                        foreach (PaymentAccountModel item in model.PaymentList)
                        {
                            if (item.ID == 0)
                            {
                                item.LEASE_HEADER_ID = ContractID;
                                item.LEASE_NUMBER = model.LEASE_NUMBER;
                                item.Created_By = model.CREATED_BY;
                                item.Last_Updated_By = model.Last_Updated_By;
                                AS_Lease_Payment_Account header = IntegrationGerneral.ToPaymentAccountEntity(item);
                                db.AS_Lease_Payment_Account.Add(header);
                                db.Entry(header).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                            else
                            {
                                AS_Lease_Payment_Account header = db.AS_Lease_Payment_Account.FirstOrDefault(m => m.ID == item.ID);
                                header.ACCOUNT = item.ACCOUNT;
                                header.BANK = item.BANK;
                                header.ACCOUNT_NAME = item.ACCOUNT_NAME;
                                header.PAYMENT_METHOD = item.PAYMENT_METHOD;
                                header.DESCRIPTION = item.DESCRIPTION;
                                header.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
                                header.LAST_UPDATE_DATE = model.Last_Updated_Time;
                                db.Entry(header).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        #endregion                       

                        #region 新增/編輯商業火災綜合保險資料
                        List<int> FireIDs = new List<int>();
                        foreach (InsuranceModel item in model.FireList)
                        {
                            FireIDs.Add(item.ID);
                        }
                        db.AS_Lease_Insurance.RemoveRange(db.AS_Lease_Insurance.Where(m => !FireIDs.Contains(m.ID) && m.LEASE_HEADER_ID == ContractID && m.INSURANCE_TYPE == "1"));
                        db.SaveChanges();
                        foreach (InsuranceModel item in model.FireList)
                        {
                            if (item.ID == 0)
                            {
                                item.LEASE_HEADER_ID = ContractID;
                                item.LEASE_NUMBER = model.LEASE_NUMBER;
                                item.Created_By = model.CREATED_BY;
                                item.Last_Updated_By = model.Last_Updated_By;
                                AS_Lease_Insurance header = IntegrationGerneral.ToInsuranceEntity(item);
                                db.AS_Lease_Insurance.Add(header);
                                db.Entry(header).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                            else
                            {
                                AS_Lease_Insurance header = db.AS_Lease_Insurance.FirstOrDefault(m => m.ID == item.ID && m.INSURANCE_TYPE == "1");
                                header.COMPANY = item.COMPANY;
                                header.INSURANCE_NO = item.INSURANCE_NO;
                                header.COMPANY_CONTACT = item.COMPANY_CONTACT;
                                header.COMPANY_PHONE = item.COMPANY_PHONE;
                                header.APPLICANT = item.APPLICANT;
                                header.INSURANT = item.INSURANT;
                                header.START_DATE = item.START_DATE;
                                header.END_DATE = item.END_DATE;
                                header.PRICE = item.PRICE;
                                header.DESCRIPTION = item.DESCRIPTION;
                                header.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
                                header.LAST_UPDATE_DATE = model.Last_Updated_Time;
                                db.Entry(header).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        #endregion

                        #region 新增/編輯公共意外責任保險資料
                        List<int> PublicIDs = new List<int>();
                        foreach (InsuranceModel item in model.PublicList)
                        {
                            PublicIDs.Add(item.ID);
                        }
                        db.AS_Lease_Insurance.RemoveRange(db.AS_Lease_Insurance.Where(m => !PublicIDs.Contains(m.ID) && m.LEASE_HEADER_ID == ContractID && m.INSURANCE_TYPE == "2"));
                        db.SaveChanges();
                        foreach (InsuranceModel item in model.PublicList)
                        {
                            if (item.ID == 0)
                            {
                                item.LEASE_HEADER_ID = ContractID;
                                item.LEASE_NUMBER = model.LEASE_NUMBER;
                                item.Created_By = model.CREATED_BY;
                                item.Last_Updated_By = model.Last_Updated_By;
                                AS_Lease_Insurance header = IntegrationGerneral.ToInsuranceEntity(item);
                                db.AS_Lease_Insurance.Add(header);
                                db.Entry(header).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                            else
                            {
                                AS_Lease_Insurance header = db.AS_Lease_Insurance.FirstOrDefault(m => m.ID == item.ID && m.INSURANCE_TYPE == "2");
                                header.COMPANY = item.COMPANY;
                                header.INSURANCE_NO = item.INSURANCE_NO;
                                header.COMPANY_CONTACT = item.COMPANY_CONTACT;
                                header.COMPANY_PHONE = item.COMPANY_PHONE;
                                header.APPLICANT = item.APPLICANT;
                                header.INSURANT = item.INSURANT;
                                header.START_DATE = item.START_DATE;
                                header.END_DATE = item.END_DATE;
                                header.PRICE = item.PRICE;
                                header.DESCRIPTION = item.DESCRIPTION;
                                header.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
                                header.LAST_UPDATE_DATE = model.Last_Updated_Time;
                                db.Entry(header).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        #endregion

                        #region 新增/編輯備註資料
                        List<int> NoteIDs = new List<int>();
                        foreach (NoteModel rent in model.NoteList)
                        {
                            NoteIDs.Add(rent.ID);
                        }
                        db.AS_Lease_Note.RemoveRange(db.AS_Lease_Note.Where(m => !NoteIDs.Contains(m.ID) && m.LEASE_HEADER_ID == ContractID));
                        db.SaveChanges();
                        foreach (NoteModel item in model.NoteList)
                        {
                            if (item.ID == 0)
                            {
                                item.LEASE_HEADER_ID = ContractID;
                                item.LEASE_NUMBER = model.LEASE_NUMBER;
                                item.Created_By = model.CREATED_BY;
                                item.Last_Updated_By = model.Last_Updated_By;
                                AS_Lease_Note header = IntegrationGerneral.ToNoteEntity(item);
                                db.AS_Lease_Note.Add(header);
                                db.Entry(header).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                            else
                            {
                                AS_Lease_Note header = db.AS_Lease_Note.FirstOrDefault(m => m.ID == item.ID);
                                header.NOTE_TYPE = item.NOTE_TYPE;
                                header.CALC_START_DATE = item.CALC_START_DATE;
                                header.CALC_END_DATE = item.CALC_END_DATE;
                                header.CALCULATE_METHOD = item.CALCULATE_METHOD;
                                header.PRICE = item.PRICE;
                                header.PAYMENT_PERIOD = item.PAYMENT_PERIOD;
                                header.DESCRIPTION = item.DESCRIPTION;
                                header.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
                                header.LAST_UPDATE_DATE = model.Last_Updated_Time;
                                db.Entry(header).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        #endregion
                    };
                    trans.Complete();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 刪除租約
        /// </summary>
        /// <returns></returns>
        public bool DeleteContract(int ID)
        {
            bool result = true;
            try
            {
                using (var db = new AS_LandBankEntities())
                {
                    db.AS_Leases_Headers.Remove(db.AS_Leases_Headers.FirstOrDefault(m => m.ID == ID));
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }


        /// <summary>
        /// 刪除租約
        /// </summary>
        /// <returns></returns>
        public bool DeleteContractByTRXHeaderID(string TRXHeaderID)
        {
            bool result = true;
            try
            {
                using (var db = new AS_LandBankEntities())
                {
                    db.AS_Leases_Headers.RemoveRange(db.AS_Leases_Headers.Where(m => m.TRX_Header_ID == TRXHeaderID));
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// 取得租約列表
        /// </summary>
        /// <returns></returns>
        public ContractSearchIndexModel GetContractList(ContractSearchModel condition)
        {
            ContractSearchIndexModel result = null;

            var query = IntegrationGerneral.GetContractList(condition);

            if (query != null)
            {
                result = new ContractSearchIndexModel();
                if (query.Items != null)
                {
                    result.TotalAmount = query.Items.Count();
                    result.Items = query.Items;
                }
                else {
                    result.TotalAmount = 0;                    
                }
                result.condition = condition;

                //分頁
                if (condition.Page != null && result.Items != null)
                {
                    //選取該頁之資訊
                    result.Items = query.Items.OrderByDescending(m => m.CREATED_TIME).Skip((condition.Page.Value - 1) * Convert.ToInt32(condition.PageSize)).Take(Convert.ToInt32(condition.PageSize));
                }
            }

            return result;
        }

        public bool ImportContract(IntegrationModel model) {

            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    
                    foreach (string id in model.IDs) {
                        int ID = int.Parse(id);
                        AS_Leases_Headers header = new AS_Leases_Headers();
                        AS_Leases_Headers item = db.AS_Leases_Headers.FirstOrDefault(h => h.ID == ID);
                        
                        #region 新增租約表頭資料
                        header.LEASE_NUMBER = item.LEASE_NUMBER;       
                        header.OLD_LEASE_NUMBER = item.OLD_LEASE_NUMBER;
                        header.OFFICER_BRANCH = item.OFFICER_BRANCH;
                        header.LEASE_TYPE = item.LEASE_TYPE;
                        header.LEASE_TRANSACTION_TYPE = item.LEASE_TRANSACTION_TYPE;
                        header.LEASE_STATUS = item.LEASE_STATUS;
                        header.PAYMENT_METHOD = item.PAYMENT_METHOD;
                        header.BUSINESS_TYPE = item.BUSINESS_TYPE;
                        header.PAYMENT_TYPE = item.PAYMENT_TYPE;
                        header.CHECK_TYPE = item.CHECK_TYPE;
                        header.LEASE_INCEPTION_DATE = item.LEASE_INCEPTION_DATE;
                        header.LEASE_TERMINATION_DATE = item.LEASE_TERMINATION_DATE;
                        header.ACCOUNT_NUMBER = item.ACCOUNT_NUMBER;
                        header.DESCRIPTION = item.DESCRIPTION;
                        header.ACCEPT_DATE = item.ACCEPT_DATE;
                        header.LEASE_CATEGORY = item.LEASE_CATEGORY;
                        header.LEASE_AREA = item.LEASE_AREA;
                        header.LEASE_RATE = item.LEASE_RATE;
                      
                        header.TRANSACTION_TYPE = model.Header.Transaction_Type;
                        header.TRX_Header_ID = model.Header.TRX_Header_ID;
                        header.CREATED_BY = int.Parse(model.Header.Created_By);
                        header.CREATION_DATE = model.Header.Create_Time;
                        header.LAST_UPDATED_BY = int.Parse(model.Header.Created_By);
                        header.LAST_UPDATE_DATE = model.Header.Create_Time;
                        db.AS_Leases_Headers.Add(header);
                        db.Entry(header).State = System.Data.Entity.EntityState.Added;
                        db.SaveChanges();
                        #endregion

                        int ContractID = db.AS_Leases_Headers.FirstOrDefault(m => m.LEASE_NUMBER == header.LEASE_NUMBER && m.TRANSACTION_TYPE == header.TRANSACTION_TYPE).ID;

                        #region 新增/編輯租金資料                    
                        
                        foreach (AS_Lease_Rent value in db.AS_Lease_Rent.Where(m => m.LEASE_HEADER_ID.Value.ToString() == item.ID.ToString()).DefaultIfEmpty())
                        {
                            if (value == null)
                                continue;
                            AS_Lease_Rent entity = new AS_Lease_Rent();
                            entity.LEASE_HEADER_ID = ContractID;
                            entity.LEASE_NUMBER = value.LEASE_NUMBER;
                            entity.START_DATE = value.START_DATE;
                            entity.END_DATE = value.END_DATE;
                            entity.CALCULATE_METHOD = value.CALCULATE_METHOD;
                            entity.PAYMENT_PERIOD = value.PAYMENT_PERIOD;
                            entity.DUE_DAY = value.DUE_DAY;
                            entity.LAST_PAYMENT = value.LAST_PAYMENT;
                            entity.NEXT_PAYMENT = value.NEXT_PAYMENT;
                            entity.EFFECTIVE_DATE = value.EFFECTIVE_DATE;
                            entity.PRICE = value.PRICE;
                            entity.CURRENCY = value.CURRENCY;
                            entity.DESCRIPTION = value.DESCRIPTION;
                            entity.CREATED_BY = item.CREATED_BY;
                            entity.CREATION_DATE = model.Header.Create_Time;
                            db.AS_Lease_Rent.Add(entity);
                            db.Entry(entity).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }
                        #endregion

                        #region 新增/編輯管理費資料   
                        foreach (AS_Lease_ManageExpense value in db.AS_Lease_ManageExpense.Where(m => m.LEASE_HEADER_ID.Value.ToString() == item.ID.ToString()).DefaultIfEmpty())
                        {
                            if (value == null)
                                continue;
                            AS_Lease_ManageExpense entity = new AS_Lease_ManageExpense();
                            entity.LEASE_HEADER_ID = ContractID;
                            entity.LEASE_NUMBER = value.LEASE_NUMBER;
                            entity.START_DATE = value.START_DATE;
                            entity.END_DATE = value.END_DATE;
                            entity.CALCULATE_METHOD = value.CALCULATE_METHOD;
                            entity.PAYMENT_PERIOD = value.PAYMENT_PERIOD;
                            entity.DUE_DAY = value.DUE_DAY;
                            entity.LAST_PAYMENT = value.LAST_PAYMENT;
                            entity.NEXT_PAYMENT = value.NEXT_PAYMENT;
                            entity.EFFECTIVE_DATE = value.EFFECTIVE_DATE;
                            entity.PRICE = value.PRICE;
                            entity.CURRENCY = value.CURRENCY;
                            entity.DESCRIPTION = value.DESCRIPTION;
                            entity.CREATED_BY = item.CREATED_BY;
                            entity.CREATION_DATE = model.Header.Create_Time;
                            db.AS_Lease_ManageExpense.Add(entity);
                            db.Entry(entity).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }
                        #endregion

                        #region 新增/編輯保證金資料
                        foreach (AS_Lease_Margin value in db.AS_Lease_Margin.Where(m => m.LEASE_HEADER_ID.Value.ToString() == item.ID.ToString()).DefaultIfEmpty())
                        {
                            if (value == null)
                                continue;
                            AS_Lease_Margin entity = new AS_Lease_Margin();
                            entity.LEASE_HEADER_ID = ContractID;
                            entity.LEASE_NUMBER = value.LEASE_NUMBER;
                            entity.RECEIVED_DATE = value.RECEIVED_DATE;
                            entity.RETURN_DATE = value.RETURN_DATE;
                            entity.BANK = value.BANK;
                            entity.PRICE = value.PRICE;
                            entity.CURRENCY = value.CURRENCY;
                            entity.CREATED_BY = item.CREATED_BY;
                            entity.CREATION_DATE = model.Header.Create_Time;
                            db.AS_Lease_Margin.Add(entity);
                            db.Entry(entity).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }
                        #endregion

                        #region 新增/編輯土地資料
                        foreach (AS_Leases_Land value in db.AS_Leases_Land.Where(m => m.LEASE_HEADER_ID.Value.ToString() == item.ID.ToString()).DefaultIfEmpty())
                        {
                            if (value == null)
                                continue;
                            AS_Leases_Land entity = new AS_Leases_Land();
                            entity.LEASE_HEADER_ID = ContractID;
                            entity.LEASE_NUMBER = value.LEASE_NUMBER;
                            entity.CITY_NAME = value.CITY_NAME;
                            entity.DISTRICT_NAME = value.DISTRICT_NAME;
                            entity.SECTION_NAME = value.SECTION_NAME;
                            entity.SUBSECTION_NAME = value.SUBSECTION_NAME;
                            entity.PARENT_LAND_NUMBER = value.PARENT_LAND_NUMBER;
                            entity.FILIAL_LAND_NUMBER = value.FILIAL_LAND_NUMBER;
                            entity.LAND_USE_TYPE = value.LAND_USE_TYPE;
                            entity.AUTHORIZED_AREA = value.AUTHORIZED_AREA;
                            entity.ANNOUNCE_PRICE = value.ANNOUNCE_PRICE;
                            entity.LESSEE_AREA = value.LESSEE_AREA;
                            entity.LEASE_RATE = value.LEASE_RATE;
                            entity.RENTAL_AMOUNT = value.RENTAL_AMOUNT;
                            entity.DESCRIPTION = value.DESCRIPTION;
                            entity.CREATED_BY = item.CREATED_BY;
                            entity.CREATION_DATE = model.Header.Create_Time;
                            db.AS_Leases_Land.Add(entity);
                            db.Entry(entity).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }
                        #endregion

                        #region 新增/編輯停車位資料
                        foreach (AS_Leases_Build_Parking value in db.AS_Leases_Build_Parking.Where(m => m.LEASE_HEADER_ID.Value.ToString() == item.ID.ToString()).DefaultIfEmpty())
                        {
                            if (value == null)
                                continue;
                            AS_Leases_Build_Parking entity = new AS_Leases_Build_Parking();
                            entity.LEASE_HEADER_ID = ContractID;
                            entity.LEASE_NUMBER = value.LEASE_NUMBER;
                            entity.CITY_NAME = value.CITY_NAME;
                            entity.DISTRICT_NAME = value.DISTRICT_NAME;
                            entity.SECTION_NAME = value.SECTION_NAME;
                            entity.SUBSECTION_NAME = value.SUBSECTION_NAME;
                            entity.PARKING_NO = value.PARKING_NO;
                            entity.PARKING_USE_TYPE = value.PARKING_USE_TYPE;
                            entity.FLOOR = value.FLOOR;
                            entity.DESCRIPTION = value.DESCRIPTION;
                            entity.CREATED_BY = item.CREATED_BY;
                            entity.CREATION_DATE = model.Header.Create_Time;
                            db.AS_Leases_Build_Parking.Add(entity);
                            db.Entry(entity).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }
                        #endregion

                        #region 新增/編輯房屋資料
                        foreach (AS_Leases_Build value in db.AS_Leases_Build.Where(m => m.LEASE_HEADER_ID.Value.ToString() == item.ID.ToString()).DefaultIfEmpty())
                        {
                            if (value == null)
                                continue;
                            AS_Leases_Build entity = new AS_Leases_Build();
                            entity.LEASE_HEADER_ID = ContractID;
                            entity.LEASE_NUMBER = value.LEASE_NUMBER;
                            entity.CITY_NAME = value.CITY_NAME;
                            entity.DISTRICT_NAME = value.DISTRICT_NAME;
                            entity.SECTION_NAME = value.SECTION_NAME;
                            entity.SUBSECTION_NAME = value.SUBSECTION_NAME;
                            entity.ADDRESS = value.ADDRESS;
                            entity.BUSINESS_USE_TYPE = value.BUSINESS_USE_TYPE;
                            entity.ATTACHED_BUILD = value.ATTACHED_BUILD;
                            entity.HOUSE_ARCADE = value.HOUSE_ARCADE;
                            entity.CONVEX = value.CONVEX;
                            entity.ARCADE = value.ARCADE;
                            entity.COMMON_PART = value.COMMON_PART;
                            entity.RIGHT_AREA = value.RIGHT_AREA;
                            entity.LEASES_AREA = value.LEASES_AREA;
                            entity.FLOOR = value.FLOOR;
                            entity.ARCADE_AREA = value.ARCADE_AREA;
                            entity.SHARE_PUBLIC_AREA = value.SHARE_PUBLIC_AREA;
                            entity.DESCRIPTION = value.DESCRIPTION;
                            entity.CREATED_BY = item.CREATED_BY;
                            entity.CREATION_DATE = model.Header.Create_Time;
                            db.AS_Leases_Build.Add(entity);
                            db.Entry(entity).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }                  
                        #endregion

                        #region 新增/編輯繳款資料
                        foreach (AS_Lease_Payment_Account value in db.AS_Lease_Payment_Account.Where(m => m.LEASE_HEADER_ID.Value.ToString() == item.ID.ToString()).DefaultIfEmpty())
                        {
                            if (value == null)
                                continue;
                            AS_Lease_Payment_Account entity = new AS_Lease_Payment_Account();
                            entity.LEASE_HEADER_ID = ContractID;
                            entity.LEASE_NUMBER = value.LEASE_NUMBER;
                            entity.ACCOUNT = value.ACCOUNT;
                            entity.BANK = value.BANK;
                            entity.ACCOUNT_NAME = value.ACCOUNT_NAME;
                            entity.PAYMENT_METHOD = value.PAYMENT_METHOD;
                            entity.DESCRIPTION = value.DESCRIPTION;
                            entity.CREATED_BY = item.CREATED_BY;
                            entity.CREATION_DATE = model.Header.Create_Time;
                            db.AS_Lease_Payment_Account.Add(entity);
                            db.Entry(entity).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }                     
                        #endregion                       

                        #region 新增/編輯商業火災綜合保險資料
                        foreach (AS_Lease_Insurance value in db.AS_Lease_Insurance.Where(m => m.LEASE_HEADER_ID.Value.ToString() == item.ID.ToString() && m.INSURANCE_TYPE == "1").DefaultIfEmpty())
                        {
                            if (value == null)
                                continue;
                            AS_Lease_Insurance entity = new AS_Lease_Insurance();
                            entity.LEASE_HEADER_ID = ContractID;
                            entity.LEASE_NUMBER = value.LEASE_NUMBER;
                            entity.COMPANY = value.COMPANY;
                            entity.INSURANCE_NO = value.INSURANCE_NO;
                            entity.COMPANY_CONTACT = value.COMPANY_CONTACT;
                            entity.COMPANY_PHONE = value.COMPANY_PHONE;
                            entity.APPLICANT = value.APPLICANT;
                            entity.INSURANT = value.INSURANT;
                            entity.START_DATE = value.START_DATE;
                            entity.END_DATE = value.END_DATE;
                            entity.PRICE = value.PRICE;
                            entity.DESCRIPTION = value.DESCRIPTION;
                            entity.CREATED_BY = item.CREATED_BY;
                            entity.CREATION_DATE = model.Header.Create_Time;
                            db.AS_Lease_Insurance.Add(entity);
                            db.Entry(entity).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }                      
                        #endregion

                        #region 新增/編輯公共意外責任保險資料
                        foreach (AS_Lease_Insurance value in db.AS_Lease_Insurance.Where(m => m.LEASE_HEADER_ID.Value.ToString() == item.ID.ToString() && m.INSURANCE_TYPE == "2").DefaultIfEmpty())
                        {
                            if (value == null)
                                continue;
                            AS_Lease_Insurance entity = new AS_Lease_Insurance();
                            entity.LEASE_HEADER_ID = ContractID;
                            entity.LEASE_NUMBER = value.LEASE_NUMBER;
                            entity.COMPANY = value.COMPANY;
                            entity.INSURANCE_NO = value.INSURANCE_NO;
                            entity.COMPANY_CONTACT = value.COMPANY_CONTACT;
                            entity.COMPANY_PHONE = value.COMPANY_PHONE;
                            entity.APPLICANT = value.APPLICANT;
                            entity.INSURANT = value.INSURANT;
                            entity.START_DATE = value.START_DATE;
                            entity.END_DATE = value.END_DATE;
                            entity.PRICE = value.PRICE;
                            entity.DESCRIPTION = value.DESCRIPTION;
                            entity.CREATED_BY = item.CREATED_BY;
                            entity.CREATION_DATE = model.Header.Create_Time;
                            db.AS_Lease_Insurance.Add(entity);
                            db.Entry(entity).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }                       
                        #endregion

                        #region 新增/編輯備註資料
                        foreach (AS_Lease_Note value in db.AS_Lease_Note.Where(m => m.LEASE_HEADER_ID.Value.ToString() == item.ID.ToString()).DefaultIfEmpty())
                        {
                            if (value == null)
                                continue;
                            AS_Lease_Note entity = new AS_Lease_Note();
                            entity.LEASE_HEADER_ID = ContractID;
                            entity.LEASE_NUMBER = value.LEASE_NUMBER;
                            entity.NOTE_TYPE = value.NOTE_TYPE;
                            entity.CALC_START_DATE = value.CALC_START_DATE;
                            entity.CALC_END_DATE = value.CALC_END_DATE;
                            entity.CALCULATE_METHOD = value.CALCULATE_METHOD;
                            entity.PRICE = value.PRICE;
                            entity.PAYMENT_PERIOD = value.PAYMENT_PERIOD;
                            entity.DESCRIPTION = value.DESCRIPTION;
                            entity.CREATED_BY = item.CREATED_BY;
                            entity.CREATION_DATE = model.Header.Create_Time;
                            db.AS_Lease_Note.Add(entity);
                            db.Entry(entity).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }                       
                        #endregion
                    }
                    

                    trans.Complete();
                }
            }
            return true;
        }

        public IEnumerable<Library.Servant.Servant.MP.MPModels.CodeItem> GetCityList()
        {
            IEnumerable<Library.Servant.Servant.MP.MPModels.CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                result = (from m in db.AS_VW_Code_City
                          orderby m.City_Code ascending
                          select new Library.Servant.Servant.MP.MPModels.CodeItem()
                          {
                              Value = m.City_Code,
                              Text = m.City_Name
                          }).ToList();
            }
            return result;
        }

        public IEnumerable<Library.Servant.Servant.MP.MPModels.CodeItem> GetDistrict(string CITY_NAME)
        {
            IEnumerable<Library.Servant.Servant.MP.MPModels.CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                result = (from m in db.AS_VW_Code_District
                          orderby m.City_Code
                          where m.City_Code == CITY_NAME
                          select new Library.Servant.Servant.MP.MPModels.CodeItem()
                          {
                              Value = m.District_Code,
                              Text = m.District_Name
                          }).ToList();
            }
            return result;
        }

        public bool GetDocID(string DocCode, out int DocID)
        {
            bool result = true;
            using (var db = new AS_LandBankEntities())
            {
                result = db.AS_Doc_Name.Any(m => m.Doc_No == DocCode);
                DocID = (result) ? db.AS_Doc_Name.First(m => m.Doc_No == DocCode).ID : 0;
            }
            return (DocID != 0);
        }
    }

}
