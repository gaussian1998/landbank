﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;

namespace Library.Servant.Servant.Integration.IntegrationModels
{
    public class LandModel : AbstractEncryptionDTO
    {
        public int ID { get; set; }
        public int LEASE_HEADER_ID { get; set; }//承租契約表頭ID
        public string LEASE_NUMBER { get; set; }//承租契約單號
        public string CITY_NAME { get; set; }//縣市
        public string DISTRICT_NAME { get; set; }//鄉鎮市區
        public string SECTION_NAME { get; set; }//段
        public string SUBSECTION_NAME { get; set; }//小段
        public string PARENT_LAND_NUMBER { get; set; }//母地號
        public string FILIAL_LAND_NUMBER { get; set; }//子地號
        public string LAND_USE_TYPE { get; set; }//使用分區
        public int AUTHORIZED_AREA { get; set; }//登記面積
        public int ANNOUNCE_PRICE { get; set; }//公告地價
        public int LESSEE_AREA { get; set; }//承租面積
        public int LEASE_RATE { get; set; }//年租率
        public int RENTAL_AMOUNT { get; set; }//租金
        public string DESCRIPTION { get; set; }//備註
        public List<string> FILES { get; set; }//上傳檔案        
        public string Created_By { get; set; }
        public string Created_ByName { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public string Last_Updated_By { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
