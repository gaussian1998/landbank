﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;

namespace Library.Servant.Servant.Integration.IntegrationModels
{
    public class PaymentAccountModel : AbstractEncryptionDTO
    {
        public int ID { get; set; }
        public int LEASE_HEADER_ID { get; set; }//承租契約表頭ID
        public string LEASE_NUMBER { get; set; }//承租契約單號
        public string BANK { get; set; }//扣款銀行
        public string ACCOUNT { get; set; }//扣款銀行
        public string ACCOUNT_NAME { get; set; }//戶名        
        public string PAYMENT_METHOD { get; set; }//繳款方式
        public string DESCRIPTION { get; set; }//備註
        public List<string> FILES { get; set; }//上傳檔案        
        public string Created_By { get; set; }
        public string Created_ByName { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public string Last_Updated_By { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
