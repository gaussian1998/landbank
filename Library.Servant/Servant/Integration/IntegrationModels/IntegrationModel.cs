﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Integration.IntegrationModels;
using Library.Interface;
using Library.Servant.SymmetryEncryption;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.Integration.IntegrationModels
{
    public class IntegrationModel : AbstractEncryptionDTO
    {
        public string[] IDs { get; set; }

        public IntegrationHeaderModel Header { get; set; }

        public IEnumerable<ContactModel> Contracts { get; set; }

        #region==批次匯入審查欄位==
        //分行
        public string Assigned_Branch { get; set; }
        //經辦
        public string HandledBy { get; set; }
        //經辦電話
        public string Phone { get; set; }
        //經辦Email
        public string Email { get; set; }
        #endregion

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }   
}
