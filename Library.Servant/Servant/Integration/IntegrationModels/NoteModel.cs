﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;

namespace Library.Servant.Servant.Integration.IntegrationModels
{
    public class NoteModel : AbstractEncryptionDTO
    {
        public int ID { get; set; }
        public int LEASE_HEADER_ID { get; set; }//承租契約表頭ID
        public string LEASE_NUMBER { get; set; }//承租契約單號
        public string NOTE_TYPE { get; set; }//類別
        public DateTime? CALC_START_DATE { get; set; }//計算開始時間
        public DateTime? CALC_END_DATE { get; set; }//計算結束時間
        public string CALCULATE_METHOD { get; set; }//計算類別      
        public int PRICE { get; set; }//每期租金
        public string CURRENCY { get; set; }//幣別
        public DateTime? PAYMENT_PERIOD { get; set; }//繳納期間
        public string DESCRIPTION { get; set; }//備註
        public string Created_By { get; set; }
        public string Created_ByName { get; set; }
        public Nullable<System.DateTime> Create_Time { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public string Last_Updated_By { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
