﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;

namespace Library.Servant.Servant.Integration.IntegrationModels
{
    public class ContractSearchIndexModel : AbstractEncryptionDTO
    {
        public IEnumerable<ContactModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public ContractSearchModel condition { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
