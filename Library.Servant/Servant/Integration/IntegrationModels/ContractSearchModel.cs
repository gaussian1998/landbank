﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Library.Servant.Servant.Integration.IntegrationModels
{
    public class ContractSearchModel : AbstractEncryptionDTO
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }
        public int PageSizeType { get; set; }

        /// <summary>租約類別</summary>
        public string Type { get; set; }

        /// <summary>租約事務</summary>
        public string Business_Type { get; set; }

        /// <summary>租約類型</summary>
        public string Category { get; set; }

        /// <summary>業務別</summary>
        public string TRANSACTION_TYPE { get; set; }

        /// <summary>租約狀態</summary>
        public string Status { get; set; }

        /// <summary>年租率-起</summary>
        public string RateStart { get; set; }

        /// <summary>年租率-迄</summary>
        public string RateEnd { get; set; }

        /// <summary>租約編號-起</summary>
        public string TRXHeaderIDBgn { get; set; }

        /// <summary>租約編號-迄</summary>
        public string TRXHeaderIDEnd { get; set; }

        /// <summary>原租約編號-起</summary>
        public string OldTRXHeaderIDBgn { get; set; }

        /// <summary>原租約編號-迄</summary>
        public string OldTRXHeaderIDEnd { get; set; }

        /// <summary>租賃期間-起</summary>
        public DateTime? BeginDate { get; set; }

        /// <summary>租賃期間-迄</summary>
        public DateTime? EndDate { get; set; }

        /// <summary>核准日期-起</summary>
        public DateTime? ApplyBeginDate { get; set; }

        /// <summary>核准日期-迄</summary>
        public DateTime? ApplyEndDate { get; set; }

        /// <summary>管轄單位</summary>
        public string OfficeBranch { get; set; }

        /// <summary>帳號(關鍵字)</summary>
        public string Account { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
