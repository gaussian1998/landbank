﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Integration.IntegrationModels
{
    public class ContactModel : AbstractEncryptionDTO
    {
        public int ID { get; set; }
        public string TRX_Header_ID { get; set; }//單據編號
        public string LEASE_NUMBER { get; set; }//租約編號
        public string OLD_LEASE_NUMBER { get; set; }
        public string TRANSACTION_TYPE { get; set; }
        public string OFFICER_BRANCH { get; set; }//管轄單位ID
        public string OfficeBranchName { get; set; }//管轄單位Name
        public string LEASE_TYPE { get; set; }//租約類型
        public string LEASE_TRANSACTION_TYPE { get; set; }//租約事務
        public string LEASE_STATUS { get; set; }//租約狀態
        public string PAYMENT_METHOD { get; set; }        
	    public string BUSINESS_TYPE { get; set; }//業務別     
        public string PAYMENT_TYPE { get; set; }
	    public string CHECK_TYPE { get; set; }
        public DateTime? ACCEPT_DATE { get; set; }//簽核日期
        public string CREATED_BY { get; set; }
        public DateTime CREATED_TIME { get; set; }
        public DateTime? LEASE_INCEPTION_DATE { get; set; }//租賃期起
        public DateTime? LEASE_TERMINATION_DATE { get; set; }//租賃期迄
        public string ACCOUNT_NUMBER { get; set; }//帳號
        public string DESCRIPTION { get; set; }//備註
        public string LEASE_CATEGORY { get; set; }//類別
        public string LEASE_AREA { get; set; }//地區
        public decimal? LEASE_RATE { get; set; }//年租率
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
        public string Last_Updated_By { get; set; }

        public List<ParkingModel> ParkingList { get; set; }
        public List<LandModel> LandList { get; set; }
        public List<BuildModel> BuildList { get; set; }
        public List<RentModel> RentList { get; set; }
        public List<ManageExpenseModel> ManageList { get; set; }
        public List<PaymentAccountModel> PaymentList { get; set; }
        public List<MarginModel> MarginList { get; set; }
        public List<NoteModel> NoteList { get; set; }
        public List<InsuranceModel> FireList { get; set; }
        public List<InsuranceModel> PublicList { get; set; }

        public ContactModel NewContract { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }

    }
}
