﻿using Library.Servant.Common;
using Library.Servant.Servant.Integration.IntegrationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Integration
{
    public class RemoteIGServant : IIGServant
    {
        public IndexModel Index(SearchModel condition)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, IndexModel>
            (
                condition,
                Config.ServantDomain + "/IGServant/Index"
            );

            return Tools.ExceptionConvert(result);
        }

        public string CreateTRXID(string Type)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                new SearchModel() { Transaction_Type = Type },
                Config.ServantDomain + "/IGServant/CreateTRXID"
            );

            return Tools.ExceptionConvert(result).StrResult;
        }

        public string CreateContractTRXID(string Type)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
           (
               new SearchModel() { Transaction_Type = Type },
               Config.ServantDomain + "/IGServant/CreateContractTRXID"
           );

            return Tools.ExceptionConvert(result).StrResult;
        }

        public IntegrationModel GetIntegrationDetail(string TRXHeaderID, string Transaction_Type)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, IntegrationModel>
            (
                new SearchModel() { TRXHeaderID = TRXHeaderID , Transaction_Type = Transaction_Type},
                Config.ServantDomain + "/IGServant/GetIntegrationDetail"
            );

            return Tools.ExceptionConvert(result);
        }

        public bool DeleteIntegration(int ID)
        {
            var result = ProtocolService.EncryptionPost<ContactModel, StringResult>
            (
                new ContactModel() { ID = ID },
                Config.ServantDomain + "/IGServant/DeleteIntegration"
            );

            return Tools.ExceptionConvert(result);
        }

        public ContactModel GetContractDetail(int id)
        {
           var result = ProtocolService.EncryptionPost<ContactModel, ContactModel>
           (
               new ContactModel() { ID = id },
               Config.ServantDomain + "/IGServant/GetContractDetail"
           );

            return Tools.ExceptionConvert(result);
        }

        public bool CreateIntegration(IntegrationHeaderModel model)
        {
            var result = ProtocolService.EncryptionPost<IntegrationHeaderModel, StringResult>
           (
               model,
               Config.ServantDomain + "/IGServant/CreateContract"
           );
            return Tools.ExceptionConvert(result);
        }

        public bool CreateContract(ContactModel model)
        {
            var result = ProtocolService.EncryptionPost<ContactModel, StringResult>
           (
               model,
               Config.ServantDomain + "/IGServant/CreateContract"
           );
            return Tools.ExceptionConvert(result);
        }

        public bool DeleteContract(int ID)
        {
          var result = ProtocolService.EncryptionPost<ContactModel, StringResult>
          (
              new ContactModel() { ID = ID },
              Config.ServantDomain + "/IGServant/DeleteContract"
          );
          return Tools.ExceptionConvert(result);
        }

        public bool DeleteContractByTRXHeaderID(string TRXHeaderID)
        {
          var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
          (
              new SearchModel() { TRXHeaderID = TRXHeaderID },
              Config.ServantDomain + "/IGServant/DeleteContractByTRXHeaderID"
          );
            return Tools.ExceptionConvert(result);
        }

        public bool UpdateApprovalStatus(string TRXHeaderID, string FlowStatus)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                new SearchModel() { TRXHeaderID = TRXHeaderID, FlowStatus = FlowStatus },
                Config.ServantDomain + "/IGServant/DeleteContractByTRXHeaderID"
            );
            return Tools.ExceptionConvert(result);
        }

        public ContractSearchIndexModel GetContractList(ContractSearchModel model)
        {
            var result = ProtocolService.EncryptionPost<ContractSearchModel, ContractSearchIndexModel>
          (
              model,
              Config.ServantDomain + "/IGServant/GetContractList"
          );
            return Tools.ExceptionConvert(result);
        }

        public bool ImportContract(IntegrationModel model)
        {
            var result = ProtocolService.EncryptionPost<IntegrationModel, IntegrationModels.StringResult>
            (
              model,
              Config.ServantDomain + "/IGServant/GetContractList"
            );
            return Tools.ExceptionConvert(result);
        }

        public IEnumerable<Library.Servant.Servant.MP.MPModels.CodeItem> GetCityList()
        {
            var result = ProtocolService.EncryptionPost<LandModel, Library.Servant.Servant.MP.MPModels.CodeItemModel>
            (
                 new LandModel() { },
                 Config.ServantDomain + "/IGServant/GetCityList"
            );
            return Tools.ExceptionConvert(result).Items;
        }

        public IEnumerable<Library.Servant.Servant.MP.MPModels.CodeItem> GetDistrict(string CITY_NAME)
        {
            var result = ProtocolService.EncryptionPost<LandModel, Library.Servant.Servant.MP.MPModels.CodeItemModel>
             (
                  new LandModel() { CITY_NAME = CITY_NAME },
                  Config.ServantDomain + "/IGServant/GetDistrict"
             );
            return Tools.ExceptionConvert(result).Items;
        }

        public bool GetDocID(string DocCode, out int DocID)
        {
            DocID = 0;
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                new SearchModel() { DocCode = DocCode },
                Config.ServantDomain + "/IGServant/GetDocID"
            );
            if (result != null)
            {
                DocID = Convert.ToInt32(Tools.ExceptionConvert(result).StrResult);
            }

            return Tools.ExceptionConvert(result).IsSuccess;
        }
    }
}
