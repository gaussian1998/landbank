﻿using System;
using System.Collections.Generic;
using Library.Servant.Servant.Integration.IntegrationModels;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Integration
{
    public interface IIGServant
    {
        bool CreateIntegration(IntegrationHeaderModel model);
        bool CreateContract(ContactModel model);
        bool ImportContract(IntegrationModel model);
        bool DeleteIntegration(int ID);
        bool DeleteContract(int ID);
        bool DeleteContractByTRXHeaderID(string TRXHeaderID);
        bool UpdateApprovalStatus(string FormNo, string FlowStatus);
        string CreateTRXID(string Type); 
        string CreateContractTRXID(string Type);
        IndexModel Index(SearchModel condition);
        IntegrationModel GetIntegrationDetail(string TRXHeaderID, string Transaction_Type);
        ContactModel GetContractDetail(int id);
        ContractSearchIndexModel GetContractList(ContractSearchModel model);
        bool GetDocID(string DocCode, out int DocID);

        IEnumerable<Library.Servant.Servant.MP.MPModels.CodeItem> GetCityList();
        IEnumerable<Library.Servant.Servant.MP.MPModels.CodeItem> GetDistrict(string CITY_NAME);
    }
}