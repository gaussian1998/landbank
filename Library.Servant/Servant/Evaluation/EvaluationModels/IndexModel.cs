﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Evaluation.EvaluationModels
{
    
    public class EJObject : AbstractEncryptionDTO
    {
        public JObject Obj { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
    public class EInt : AbstractEncryptionDTO
    {
        public int type { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
    public class EString : AbstractEncryptionDTO
    {
        public string type { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
    
}
