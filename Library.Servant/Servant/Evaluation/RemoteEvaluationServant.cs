﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Evaluation.EvaluationModels;
using Library.Servant.Common;

namespace Library.Servant.Servant.Evaluation
{
    public class RemoteEvaluationServant : IEvaluationServant
    {
        public EJObject GetMaxNo(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/GetMaxNo"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetUser(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/GetUser"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetReport(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/GetReport"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject UpdateRemark(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/UpdateRemark"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject GetDocID(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/GetDocID"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject OnFinish(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/OnFinish"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject OnI200Finish(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/OnI200Finish"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject GetPicList(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/GetPicList"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject SavePic(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/SavePic"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject UpdateDetail(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/UpdateDetail"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject GetHisIns(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/GetHisIns"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetHisInsA(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/GetHisInsA"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject ContentDetail(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/ContentDetail"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject ContentDetailA(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/ContentDetailA"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetDetail(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/GetDetail"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetDetailA(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/GetDetailA"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject Index(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/Index"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject IndexA(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/IndexA"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetTarget(EJObject condition)
        {

            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/EvaluationServant/GetTarget"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetCategoryKind(EInt type)
        {
            var result = ProtocolService.EncryptionPost<EInt, EJObject>
            (
                type,
                Config.ServantDomain + "/EvaluationServant/GetCategoryKind"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject GetCategoryType(EInt type)
        {
            var result = ProtocolService.EncryptionPost<EInt, EJObject>
            (
                type,
                Config.ServantDomain + "/EvaluationServant/GetCategoryType"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject DocumentType(EInt type)
        {
            var result = ProtocolService.EncryptionPost<EInt, EJObject>
            (
                type,
                Config.ServantDomain + "/EvaluationServant/DocumentType"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject DocumentTypeOne(EInt type)
        {
            var result = ProtocolService.EncryptionPost<EInt, EJObject>
            (
                type,
                Config.ServantDomain + "/EvaluationServant/DocumentTypeOne"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject GetDPM(EInt type)
        {
            var result = ProtocolService.EncryptionPost<EInt, EJObject>
            (
                type,
                Config.ServantDomain + "/EvaluationServant/GetDPM"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject GetOrder(EString type)
        {
            var result = ProtocolService.EncryptionPost<EString, EJObject>
            (
                type,
                Config.ServantDomain + "/EvaluationServant/GetOrder"
            );
            return Tools.ExceptionConvert(result);
        }


        public EJObject GetCode(EString type)
        {
            var result = ProtocolService.EncryptionPost<EString, EJObject>
            (
                type,
                Config.ServantDomain + "/EvaluationServant/GetCode"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject GetBuild(EJObject model)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                model,
                Config.ServantDomain + "/EvaluationServant/GetBuild"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject GetLand(EJObject model)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                model,
                Config.ServantDomain + "/EvaluationServant/GetLand"
            );

            return Tools.ExceptionConvert(result);
        }

        public EJObject UpdateOrder(EJObject model)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                model,
                Config.ServantDomain + "/EvaluationServant/UpdateOrder"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject UpdateOrderA(EJObject model)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                model,
                Config.ServantDomain + "/EvaluationServant/UpdateOrderA"
            );

            return Tools.ExceptionConvert(result);
        }

    }
}
