﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Evaluation.EvaluationModels;
using System.Transactions;
using Library.Entity.Edmx;
using Library.Servant.Repository.Land;
using Library.Servant.Repository.Evaluation;

namespace Library.Servant.Servant.Evaluation
{
    public class LocalEvaluationServant : IEvaluationServant
    {
        public EJObject Index(EJObject condition)
        {
            EJObject result = null;

            var query = EvaluationGeneral.GetIndex(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetReport(EJObject condition)
        {
            EJObject result = null;

            var query = EvaluationGeneral.GetReport(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject UpdateRemark(EJObject condition)
        {
            EJObject result = null;

            var query = EvaluationGeneral.UpdateRemark(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject GetUser(EJObject condition)
        {
            EJObject result = null;

            var query = EvaluationGeneral.GetUser(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject GetDocID(EJObject condition)
        {
            EJObject result = null;

            var query = EvaluationGeneral.GetDocID(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject OnFinish(EJObject condition)
        {
            EJObject result = null;

            var query = EvaluationGeneral.OnFinish(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject OnI200Finish(EJObject condition)
        {
            EJObject result = null;

            var query = EvaluationGeneral.OnI200Finish(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject GetPicList(EJObject condition)
        {
            EJObject result = null;

            var query = EvaluationGeneral.GetPicList(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject SavePic(EJObject condition)
        {
            EJObject result = null;

            var query = EvaluationGeneral.SavePic(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject UpdateDetail(EJObject condition)
        {
            EJObject result = null;

            var query = EvaluationGeneral.UpdateDetail(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject GetHisIns(EJObject condition)
        {
            EJObject result = null;

            var query = EvaluationGeneral.GetHisIns(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetHisInsA(EJObject condition)
        {
            EJObject result = null;

            var query = EvaluationGeneral.GetHisInsA(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject ContentDetail(EJObject condition)
        {
            EJObject result = null;

            var query = EvaluationGeneral.ContentDetail(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject GetMaxNo(EJObject condition)
        {
            EJObject result = null;

            var query = EvaluationGeneral.GetMaxNo(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetBuild(EJObject condition)
        {

            EJObject result = null;
            var query = EvaluationGeneral.GetBuild(condition);
            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject GetLand(EJObject condition)
        {

            EJObject result = null;
            var query = EvaluationGeneral.GetLand(condition);
            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject GetDetail(EJObject condition)
        {

            EJObject result = null;
            var query = EvaluationGeneral.GetDetail(condition);
            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetTarget(EJObject condition)
        {

            EJObject result = null;
            var query = EvaluationGeneral.GetTarget(condition);
            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetCategoryKind(EInt type)
        {
            EJObject result = new EJObject();

            var query = EvaluationGeneral.GetCategoryKind(type);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetCategoryType(EInt type)
        {
            EJObject result = new EJObject();

            var query = EvaluationGeneral.GetCategoryType(type);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject DocumentType(EInt type)
        {
            EJObject result = new EJObject();

            var query = EvaluationGeneral.DocumentType(type);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject DocumentTypeOne(EInt type)
        {
            EJObject result = new EJObject();

            var query = EvaluationGeneral.DocumentTypeOne(type);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetDPM(EInt type)
        {
            EJObject result = new EJObject();

            var query = EvaluationGeneral.GetDPM(type);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetCode(EString type)
        {
            EJObject result = new EJObject();

            var query = EvaluationGeneral.GetBookCode(type);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetOrder(EString ID)
        {
            EJObject result = new EJObject();

            var query = EvaluationGeneral.GetOrder(ID);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject UpdateOrder(EJObject model2)
        {
            EJObject result = new EJObject();

            var query = EvaluationGeneral.UpdateOrder(model2);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject IndexA(EJObject condition)
        {
            EJObject result = null;

            var query = EvaluationGeneral.GetIndexA(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject ContentDetailA(EJObject condition)
        {
            EJObject result = null;

            var query = EvaluationGeneral.ContentDetailA(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetDetailA(EJObject condition)
        {

            EJObject result = null;
            var query = EvaluationGeneral.GetDetailA(condition);
            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject UpdateOrderA(EJObject model2)
        {
            EJObject result = new EJObject();

            var query = EvaluationGeneral.UpdateOrderA(model2);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
    }
}
