﻿using Library.Entity.Edmx;
using Library.Servant.Servant.Evaluation.EvaluationModels;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servants.AbstractFactory;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Evaluation.EvaluationAssignEventListener
{
    public class EvaluationAssignEventListener : ExampleListener, IHttpGetProvider
    {
        private IEvaluationServant EvaluationServant = ServantAbstractFactory.Evaluation();

        public HttpGetViewModel GetUrl(string FormNo, string carryData)
        {
            return new HttpGetViewModel { ControllerName = "EvaluationAssign", ActionName = "Edit", Parameter = "id=" + FormNo + "" };
        }

        public override bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            bool result = true;
            EJObject s = new EJObject();
            s.Obj = new JObject();
            s.Obj["No"] = formNo;
              EvaluationServant.OnI200Finish(s);
            return result;
        }
      
    }
}
