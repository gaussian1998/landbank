﻿using Library.Entity.Edmx;
using Library.Servant.Servant.Evaluation.EvaluationModels;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servants.AbstractFactory;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Evaluation.EvaluationEventListener
{
    public class BuildEvaluationEventListener : ExampleListener, IHttpGetProvider
    {
        private IEvaluationServant EvaluationServant = ServantAbstractFactory.Evaluation();

        public HttpGetViewModel GetUrl(string FormNo, string carryData)
        {
            return new HttpGetViewModel { ControllerName = "Evaluation", ActionName = "Edit", Parameter = "id=" + FormNo + "" };
        }

        public override bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            bool result = true;
            EJObject s = new EJObject();
            s.Obj = new JObject();
            s.Obj["No"] = formNo;
              EvaluationServant.OnFinish(s);
            return result;
        }
      
    }
}
