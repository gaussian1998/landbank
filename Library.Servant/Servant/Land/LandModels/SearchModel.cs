﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Land.LandModels
{
    public class SearchModel : AbstractEncryptionDTO
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }

        /// <summary>單據類型</summary>
        public string Transaction_Type { get; set; }

        public string TRXHeaderID { get; set; }

        public string AssetNumber { get; set; }

        /// <summary>狀態</summary>
        public string FlowStatus { get; set; }

        /// <summary>帳本</summary>
        public string BookTypeCode { get; set; }

        /// <summary>單據處理日期-起</summary>
        public DateTime? BeginDate { get; set; }

        /// <summary>單據處理日期-迄</summary>
        public DateTime? EndDate { get; set; }

        /// <summary>入帳日期</summary>
        public DateTime? PostDate { get; set; }

        /// <summary>管轄單位</summary>
        public string OfficeBranch { get; set; }

        /// <summary>備註(關鍵字)</summary>
        public string Remark { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
        /// <summary>表單流程代號(單據類型 + 來源類型)</summary>
        public string DocCode { get; set; }
    }
}
