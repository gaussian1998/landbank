﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Land.LandModels
{
    public class AssetsSearchModel : AbstractEncryptionDTO
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }
        public string City_Code { get; set; }
        public string District_Code { get; set; }
        public string Section_Code { get; set; }
        public string CategoryCode { get; set; }

        public string Filial_Land_Number_Begin { get; set; }
        public string Filial_Land_Number_End { get; set; }

        public string AssetNumber { get; set; }
        public string AssetMainKindType { get; set; }
        public string CodeClass { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
        #region == 資產類別 ==
        public string MainKind { get; set; }
        public string AssetDetailKind { get; set; }
        #endregion
    }
}
