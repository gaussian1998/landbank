﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Land.LandModels
{
    public class LandModel : AbstractEncryptionDTO
    {
        public LandHeaderModel Header { get; set; }

        public IEnumerable<AssetLandModel> Assets { get; set; }

        /// <summary>
        /// 土地分割or土地合併資訊
        /// </summary>
        public IEnumerable<AssetLandModel> ExtraAssets { get; set; }

        /// <summary>
        /// 土地處分
        /// </summary>
        public IEnumerable<AssetLandRetireModel> RetireAssets { get; set; }

        /// <summary>
        /// 土地異動
        /// </summary>
        public IEnumerable<AssetLandChangeModel> ChangeAssets { get; set; }

        //土地處理
        public LandDispose LD { get; set; }



        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
