﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Land.LandModels
{
    public class LandDispose
    {
        #region ==成本維護==
        public int AddCost { get; set; }
        public int LessCost { get; set; }
        #endregion

        #region ==土地分割==
        public int LandSplitCount { get; set; }
        #endregion

        #region ==土地合併==
        public string LandMergeTarget { get; set; }
        #endregion

        #region ==土地處分==
        public string Reason_Code { get; set; }
        public decimal Retire_Cost { get; set; }
        public decimal Sell_Amount { get; set; }
        public decimal Sell_Cost { get; set; }
        public decimal New_Cost { get; set; }
        public decimal Reval_Adjustment_Amount { get; set; }
        public decimal Reval_Reserve { get; set; }
        #endregion
    }
}
