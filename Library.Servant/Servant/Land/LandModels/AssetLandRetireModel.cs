﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Land.LandModels
{
    public class AssetLandRetireModel : AbstractEncryptionDTO
    {
        public int ID { get; set; }
        public string TRX_Header_ID { get; set; }
        public string Asset_Land_ID { get; set; }
        public string Serial_Number { get; set; }
        public string Asset_Number { get; set; }
        public string Parent_Asset_Number { get; set; }
        public string City_Code { get; set; }
        public string City_Name { get; set; }
        public string District_Code { get; set; }
        public string District_Name { get; set; }
        public string Section_Code { get; set; }
        public string Section_Name { get; set; }
        public string Sub_Section_Name { get; set; }
        public string Parent_Land_Number { get; set; }
        public string Filial_Land_Number { get; set; }
        public string Build_Address { get; set; }
        public string Build_Number { get; set; }
        public decimal Old_Cost { get; set; }
        public decimal Retire_Cost { get; set; }
        public decimal Sell_Amount { get; set; }
        public decimal Sell_Cost { get; set; }
        public decimal New_Cost { get; set; }
        public decimal Reval_Adjustment_Amount { get; set; }
        public decimal Reval_Reserve { get; set; }
        public decimal Reval_Land_VAT { get; set; }
        public string Reason_Code { get; set; }
        public string Description { get; set; }
        public int Account_Type { get; set; }
        public int Receipt_Type { get; set; }
        public string Retire_Cost_Account { get; set; }
        public string Sell_Amount_Account { get; set; }
        public string Sell_Cost_Account { get; set; }
        public string Reval_Adjustment_Amount_Account { get; set; }
        public string Reval_Reserve_Account { get; set; }
        public string Reval_Land_VAT_Account { get; set; }
        public Nullable<System.DateTime> Create_Time { get; set; }
        public string Created_By { get; set; }
        public string Created_By_Name { get; set; }
        public Nullable<System.DateTime> Last_UpDatetimed_Time { get; set; }
        public string Last_UpDatetimed_By { get; set; }
        public string Last_UpDatetimed_By_Name { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
