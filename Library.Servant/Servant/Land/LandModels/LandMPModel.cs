﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Land.LandModels
{
    public class LandMPModel : AbstractEncryptionDTO
    {
        public LandHeaderModel Header { get; set; }

        public IEnumerable<AssetLandMPModel> MPAssets { get; set; }

        /// <summary>
        /// 處分
        /// </summary>
        public IEnumerable<AssetLandMPRetireModel> RetireAssets { get; set; }

        /// <summary>
        /// 異動
        /// </summary>
        public IEnumerable<AssetLandMPChangeModel> ChangeAssets { get; set; }

        //土地處理
        public LandDispose LD { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
