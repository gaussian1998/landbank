﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Land.LandModels
{
    public class AssetsMPSearchModel : AbstractEncryptionDTO
    {
        public int? Page { get; set; }
        public int PageSize { get; set; }
        public string Asset_Number { get; set; }
        public string Parent_Asset_Number { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
        #region == 資產類別 ==
        public string MainKind { get; set; }
        public string AssetDetailKind { get; set; }
        #endregion
    }
}
