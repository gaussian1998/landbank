﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Land.LandModels
{
    public class AssetsMPIndexModel : AbstractEncryptionDTO
    {
        public IEnumerable<AssetLandMPModel> Items { get; set; }
        public int TotalAmount { get; set; }
        public AssetsMPSearchModel condition { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
