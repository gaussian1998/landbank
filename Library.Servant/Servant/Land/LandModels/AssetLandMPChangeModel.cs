﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Land.LandModels
{
    public class AssetLandMPChangeModel : AbstractEncryptionDTO
    {
        public int ID { get; set; }
        public string TRX_Header_ID { get; set; }
        public string AS_Assets_Land_MP_Record_ID { get; set; }
        public string UPD_File { get; set; }
        public string Field_Name { get; set; }
        public string Modify_Type { get; set; }
        public string Before_Data { get; set; }
        public string After_Data { get; set; }
        public Nullable<System.DateTime> Create_Time { get; set; }
        public string Created_By { get; set; }
        public string Created_By_Name { get; set; }
        public Nullable<System.DateTime> Last_UpDatetimed_Time { get; set; }
        public string Last_UpDatetimed_By { get; set; }
        public string Last_UpDatetimed_By_Name { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
