﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.LandModels.ReportModel
{
    public class LandReportModel : AbstractEncryptionDTO
    {
       
        public string Asset_Number { get; set; }
        public decimal Area_Size { get; set; }
       
        public string Used_Partition { get; set; }
        public int Used_Partition_Amount { get; set; }//Used_Partition group by後各有多少數量
        
        public decimal? Current_Cost { get; set; }
        public decimal? Reval_Reserve { get; set; }
        public decimal? Deprn_Amount { get; set; }
        public string City_Name { get; set; }
        public string District_Name { get; set; }
        public string Section_Name { get; set; }//段
        public string Sub_Section_Name { get; set; }//小段
        public string Parent_Land_Number { get; set; }
        public string Filial_Land_Number { get; set; }
        public decimal Authorized_Area { get; set; }
        public decimal Announce_Amount { get; set; }//公告現值
        public decimal All_Announce_Amount { get; set; }//公告現值總額=Authorized_Area*Announce_Amount
        public decimal BonusNumber { get; set; }//加成數
        public decimal Recoverable_amount {get;set; } //可回收金額All_Announce_Amount*加成數
        public decimal BookValueBalanceForAssetImpairment { get; set; }//帳面價值餘額-資產減損明細表Current_Cost + Reval_Reserve - Deprn_Amount
        public decimal RiseInNumber { get; set; }//補提減損數Current_Cost + Reval_Reserve - Deprn_Amount - Deprn_Amount
        public decimal CountTheNumberofAccounts { get; set; }//帳列數
        public decimal NumberOfPreviousYears { get; set; }//以前年度減少數
        public decimal NumberOfRevolutions { get; set; }//沖轉數
        public decimal RevalAdjustmentAmountBalanceForAssetImpairment { get; set; }//未實現重估增值餘額=沖轉數+帳列數
        public decimal ReversalImpairmentLossAssets { get; set; }//資產減損迴轉利益
        public decimal LossOfAssets { get; set; }//資產減損損失
        public decimal TotalForAssetRevaluationList { get; set; }//合計
        public decimal RevaluationAfterBookValue { get; set; }//重估后帳面價值
        public decimal ValueAddedMoney { get; set; }//增值金額        
        public decimal RevaluationCountTheNumberofAccounts { get; set; }//重估后帳列數
        public decimal ShouldAdded { get; set; }//應補提（沖轉)數
        public decimal Reval_Land_VAT { get; set; }//估計應付土地增值稅
        public decimal Reval_Land_VATCountTheNumberofAccounts { get; set; }//估計應付土地增值稅帳列數
        public decimal Reval_Land_VATRevaluationCountTheNumberofAccounts { get; set; }//估計應付土地增值稅重估后帳列數
        public decimal Reval_Land_VATNumberOfRevolutions { get; set; }//估計應付土地增值稅應補提（沖轉)數
        public decimal Reval_Adjustment_Amount { get; set; }//未實現重估增值
        public decimal Reval_Adjustment_AmountCountTheNumberofAccounts { get; set; }//未實現重估增值帳列數
        public decimal Reval_Adjustment_AmountRevaluationCountTheNumberofAccounts { get; set; }//未實現重估增值重估后帳列數
        public decimal Reval_Adjustment_AmountNumberOfRevolutions { get; set; }//未實現重估增值應補提（沖轉)數
        public decimal SpecialLoss { get; set; }//特別損失
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
