﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Land.LandModels;
using Library.Servant.Enums;
using Library.Entity.Edmx;
using System.Transactions;
using Library.Servant.Repository.Land;


namespace Library.Servant.Servant.Land
{
    public class LocalLandServant : ILandServant
    {
        public AssetHandleResult CreateToLand(string TRXHeaderID)
        {
            AssetHandleResult result = new AssetHandleResult() { };

            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        AS_TRX_Headers Header = db.AS_TRX_Headers.First(o => o.Form_Number == TRXHeaderID);
                        Header.Last_UpDatetimed_Time = DateTime.Now;
                        Header.Transaction_Status = "2";
                        db.Entry(Header).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        var query = from m in db.AS_Assets_Land_Record
                                    where m.TRX_Header_ID == TRXHeaderID
                                    select m;

                        foreach (var item in query)
                        {
                            int ID = 1;
                            if (db.AS_Assets_Land.Count() > 0)
                            {
                                ID = db.AS_Assets_Land.Max(o => o.ID) + 1;
                            }
                            AS_Assets_Land Land = LandGeneral.ToAS_Assets_LandFromAS_Assets_Land_Record(item);
                            Land.ID = ID;
                            Land.Office_Branch = Header.Office_Branch;
                            Land.Book_Type_Code = Header.Book_Type_Code;
                            Land.Old_Asset_Category_code = "";
                            Land.City_Name = "";
                            Land.District_Name = "";
                            Land.Section_Name = "";
                            db.Entry(Land).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }
                    }

                    trans.Complete();
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public AssetHandleResult CreateLand(LandModel Land)
        {
            LandModel CreateLand = new LandModel()
            {
                Header = Land.Header,
                LD = Land.LD
            };
            if (Land.Assets!= null && Land.Assets.Any(p => p != null))
            {
                CreateLand.Assets = Land.Assets.Select(o => GetAssetDetailByAssetNumber(o.Asset_Number));
            }
            if (Land.RetireAssets != null && Land.RetireAssets.Any(p => p != null))
            {
                CreateLand.RetireAssets = Land.RetireAssets.Select(o => GetAssetRetireDetailByAssetNumber(o.Asset_Number));
            }
            return Create_Asset(CreateLand);
        }
        public AssetHandleResult CreateLandMP(LandMPModel Land)
        {
            LandMPModel CreateLand = new LandMPModel()
            {
                Header = Land.Header,
                MPAssets = Land.MPAssets
            };
            return Create_MPAsset(CreateLand);
        }
        public AssetHandleResult CreateLandMPRetire(LandMPModel Land)
        {
            LandMPModel CreateLand = new LandMPModel()
            {
                Header = Land.Header,
                MPAssets = Land.MPAssets
            };
            return Create_MPAssetRetire(CreateLand);
        }
        private AssetLandModel GetAssetDetailByAssetNumber(string AssetNumber)
        {
            AssetLandModel asset = null;

            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_Land AS_Land = (from a in db.AS_Assets_Land
                                          where a.Asset_Number == AssetNumber
                                          select a).FirstOrDefault();
                if (AS_Land != null)
                {
                    asset = LandGeneral.ToAssetLandModel(AS_Land);
                }
            }

            return asset;
        }
        private AssetLandRetireModel GetAssetRetireDetailByAssetNumber(string AssetNumber)
        {
            AssetLandRetireModel asset = null;

            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_Land AS_Land = (from a in db.AS_Assets_Land
                                          where a.Asset_Number == AssetNumber
                                          select a).FirstOrDefault();
                if (AS_Land != null)
                {
                    asset = LandGeneral.ToAssetRetireLandModel(AS_Land);
                }
            }

            return asset;
        }

        public string CreateTRXID(string Type)
        {
            string CToDay = (DateTime.Today.Year - 1911).ToString().PadLeft(3, '0') + DateTime.Today.ToString("MMdd") + Type.PadRight(4, '0');//原編號為B110, 補滿四碼
            string TRXID = "";

            using (var db = new AS_LandBankEntities())
            {
                TRXID = db.AS_TRX_Headers.Where(o => o.Transaction_Type == Type && o.Form_Number.StartsWith(CToDay)).Max(o => o.Form_Number);
            }
            if (TRXID != null)
            {
                int Seq = Convert.ToInt32(TRXID.Substring(11)) + 1;
                TRXID = CToDay + Seq.ToString().PadLeft(6, '0');
            }
            else
            {
                TRXID = CToDay + "000001";
            }

            return TRXID;
        }

        public AssetHandleResult Create_Asset(LandModel model)
        {
            //try
            //{
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    //Header
                    AS_TRX_Headers Header = LandGeneral.ToAS_Assets_Land_Header(model.Header);
                    //if (!db.AS_Assets_Land_Header.Any(h => h.TRX_Header_ID == Header.Form_Number))
                    //{
                    //    db.Entry(Header).State = System.Data.Entity.EntityState.Added;
                    //    db.SaveChanges();
                    //}
                    if (Header.Transaction_Type == "B210")
                    {
                        var oldAssetList = db.AS_Assets_Land_Record.Where(land_record => land_record.TRX_Header_ID == Header.Form_Number);
                        if (oldAssetList != null && oldAssetList.Count() > 0)
                        {
                            foreach (AS_Assets_Land_Record oldAsset in oldAssetList)
                            {
                                db.Entry(oldAsset).State = System.Data.Entity.EntityState.Deleted;
                                db.SaveChanges();
                            }
                        }

                    }

                    Decimal TotalAreaSize = 0, TotalOriginalCost = 0;
                    if (Header.Transaction_Type == "B230")   //土地處分單
                    {
                        if (model.RetireAssets != null && model.RetireAssets.Any(p => p != null))
                        {
                            foreach (var asset in model.RetireAssets)
                            {
                                AS_Assets_Land_Retire Record = LandGeneral.ToAS_Assets_Land_Retire(Header.Form_Number, asset, model.LD);
                                db.Entry(Record).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                        }
                    }

                    if (model.Assets != null && model.Assets.Any(p => p != null))
                    {
                        foreach (var asset in model.Assets)
                        {
                            //if (Header.Transaction_Type == "B310") //土地改良新增
                            //{
                            //    AS_Assets_Land_MP_Record MPRecord = LandGeneral.ToAS_Assets_Land_MP_Record(Header.Form_Number, asset);
                            //    db.Entry(MPRecord).State = System.Data.Entity.EntityState.Added;
                            //    db.SaveChanges();
                            //    continue;
                            //}
                            //Record
                            AS_Assets_Land_Record Record = LandGeneral.ToAS_Assets_Land_Record(Header.Form_Number, asset);
                            db.Entry(Record).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                            TotalAreaSize += Record.Area_Size;
                            TotalOriginalCost += Record.Original_Cost;


                            //土地額外處理
                            if (model.LD != null)
                            {
                                switch (Header.Transaction_Type)
                                {
                                    case "B300":
                                        //土地成本維護
                                        #region
                                        AS_Asset_Cost Cost = new AS_Asset_Cost()
                                        {
                                            TRX_Header_ID = Header.Form_Number,
                                            Asset_Number = asset.Asset_Number,
                                            Old_Cost = asset.Current_Cost,
                                            Add_Cost = model.LD.AddCost,
                                            Less_Cost = model.LD.LessCost,
                                            Created_By = asset.Created_By,
                                            Create_Time = DateTime.Now,
                                            Last_Updated_By = ""
                                        };
                                        db.Entry(Cost).State = System.Data.Entity.EntityState.Added;
                                        db.SaveChanges();
                                        #endregion
                                        break;
                                    case "B210":
                                        //case "B400":原代碼, 後來應改為三碼
                                        //土地分割
                                        #region
                                        for (int i = 1; i <= model.LD.LandSplitCount; i++)
                                        {
                                            AS_Assets_Land_Split Split = new AS_Assets_Land_Split()
                                            {
                                                TRX_Header_ID = Header.Form_Number,
                                                Old_Asset_Land_ID = Record.Asset_Land_ID,
                                                Asset_Land_ID = "",
                                                Serial_Number = (i + 1).ToString(),
                                                Office_Branch = Header.Office_Branch,
                                                Asset_Number = CreateLandSplitAssetNumber(),
                                                Book_Type_Code = Header.Book_Type_Code,
                                                Asset_Category_code = Record.Asset_Category_code,
                                                Location_Disp = Record.Location_Disp,
                                                Old_Asset_Number = Record.Asset_Number,
                                                Description = Record.Description,
                                                City_Code = Record.City_Code,
                                                City_Name = Record.City_Name,
                                                District_Code = Record.District_Code,
                                                District_Name = Record.District_Name,
                                                Section_Code = Record.Section_Code,
                                                Section_Name = Record.Section_Name,
                                                Sub_Section_Name = Record.Sub_Section_Name,
                                                Parent_Land_Number = Record.Parent_Land_Number,
                                                Filial_Land_Number = Record.Filial_Land_Number,
                                                Authorization_Number = Record.Authorization_Number,
                                                Authorized_name = Record.Authorized_name,
                                                Area_Size = Record.Area_Size,
                                                Authorized_Scope_Molecule = Record.Authorized_Scope_Molecule,
                                                Authorized_Scope_Denomminx = Record.Authorized_Scope_Denomminx,
                                                Authorized_Area = Record.Authorized_Area,
                                                Used_Type = Record.Used_Type,
                                                Is_Marginal_Land = Record.Is_Marginal_Land,
                                                Own_Area = Record.Own_Area,
                                                NONOwn_Area = Record.NONOwn_Area,
                                                Original_Cost = Record.Original_Cost,
                                                Deprn_Amount = Record.Deprn_Amount,
                                                Reval_Land_VAT = Record.Reval_Land_VAT,
                                                Reval_Adjustment_Amount = Record.Reval_Adjustment_Amount,
                                                Business_Area_Size = Record.Business_Area_Size,
                                                NONBusiness_Area_Size = Record.NONBusiness_Area_Size,
                                                Current_Cost = Record.Current_Cost,
                                                Reval_Reserve = Record.Reval_Reserve,
                                                Business_Book_Amount = Record.Business_Book_Amount,
                                                NONBusiness_Book_Amount = Record.NONBusiness_Book_Amount,
                                                Business_Reval_Reserve = Record.Business_Reval_Reserve,
                                                NONBusiness_Reval_Reserve = Record.NONBusiness_Reval_Reserve,
                                                Datetime_Placed_In_Service = Record.Datetime_Placed_In_Service,
                                                Year_Number = (int)Record.Year_Number,
                                                Announce_Amount = Record.Announce_Amount,
                                                Announce_Price = Record.Announce_Price,
                                                Tax_Type = Record.Tax_Type,
                                                Reduction_reason = Record.Reduction_reason,
                                                Reduction_Area = Record.Reduction_Area,
                                                Declared_Price = Record.Declared_Price,
                                                Dutiable_Amount = Record.Dutiable_Amount,
                                                Remark1 = Record.Remark1,
                                                Remark2 = Record.Remark2,
                                                Current_units = Record.Current_units,
                                                Split_Type = "N",
                                                Used_Status = Record.Used_Status,
                                                Created_By_Name = Record.Created_By_Name,
                                                Obtained_Method = Record.Obtained_Method,
                                                Used_Partition = Record.Used_Partition,
                                                Create_Time = DateTime.Now,
                                                Created_By = Record.Created_By,
                                                Last_UpDatetimed_Time = DateTime.Now,
                                                Last_UpDatetimed_By = Record.Last_UpDatetimed_By,
                                                Last_UpDatetimed_By_Name = Record.Last_UpDatetimed_By_Name

                                            };
                                            db.Entry(Split).State = System.Data.Entity.EntityState.Added;
                                            db.SaveChanges();
                                        }
                                        #endregion
                                        break;
                                }
                            }

                            //土地額外處理(合併)
                            if (model.LD != null && !string.IsNullOrEmpty(model.LD.LandMergeTarget))
                            {
                                //取得合併目標資產
                                if (db.AS_Assets_Land.Any(m => m.Asset_Number == model.LD.LandMergeTarget))
                                {
                                    #region
                                    AS_Assets_Land Target = db.AS_Assets_Land.Where(m => m.Asset_Number == model.LD.LandMergeTarget).First();

                                    AS_Assets_Land_ML Merge = new AS_Assets_Land_ML()
                                    {
                                        Asset_Number = model.LD.LandMergeTarget,
                                        TRX_Header_ID = Header.Form_Number,
                                        Current_units = Target.Current_units,
                                        Datetime_Placed_In_Service = Target.Datetime_Placed_In_Service,
                                        Old_Asset_Land_ID = Target.ID.ToString(),
                                        Old_Asset_Number = Target.Old_Asset_Number,
                                        Asset_Category_code = Target.Asset_Category_code,
                                        Location_Disp = Target.Location_Disp,
                                        Description = Target.Description,
                                        City_Code = Target.City_Code,
                                        District_Code = Target.District_Code,
                                        Section_Code = Target.Section_Code,
                                        Sub_Section_Name = Target.Sub_Section_Name,
                                        Parent_Land_Number = Target.Parent_Land_Number,
                                        Filial_Land_Number = Target.Filial_Land_Number,
                                        Authorization_Number = Target.Authorization_Number,
                                        Authorized_name = Target.Authorized_name,
                                        Area_Size = TotalAreaSize,
                                        Authorized_Scope_Molecule = Target.Authorized_Scope_Molecule,
                                        Authorized_Scope_Denomminx = Target.Authorized_Scope_Denomminx,
                                        Authorized_Area = Target.Authorized_Area,
                                        Used_Type = Target.Used_Type,
                                        Used_Status = Target.Used_Status,
                                        Obtained_Method = Target.Obtained_Method,
                                        Used_Partition = Target.Used_Partition,
                                        Is_Marginal_Land = Target.Is_Marginal_Land,
                                        Own_Area = Target.Own_Area,
                                        NONOwn_Area = Target.NONOwn_Area,
                                        Original_Cost = TotalOriginalCost,
                                        Deprn_Amount = Target.Deprn_Amount,
                                        Current_Cost = Target.Current_Cost,
                                        Reval_Adjustment_Amount = Target.Reval_Adjustment_Amount,
                                        Reval_Land_VAT = Target.Reval_Land_VAT,
                                        Reval_Reserve = Target.Reval_Reserve,
                                        Business_Area_Size = Target.Business_Area_Size,
                                        Business_Book_Amount = Target.Business_Book_Amount,
                                        Business_Reval_Reserve = Target.Business_Reval_Reserve,
                                        NONBusiness_Area_Size = Target.NONBusiness_Area_Size,
                                        NONBusiness_Book_Amount = Target.NONBusiness_Book_Amount,
                                        NONBusiness_Reval_Reserve = Target.NONBusiness_Reval_Reserve,
                                        Year_Number = Target.Year_Number,
                                        Announce_Amount = Target.Announce_Amount,
                                        Announce_Price = Target.Announce_Price,
                                        Tax_Type = Target.Tax_Type,
                                        Reduction_reason = Target.Reduction_reason,
                                        Reduction_Area = Target.Reduction_Area,
                                        Declared_Price = Target.Declared_Price,
                                        Dutiable_Amount = Target.Dutiable_Amount,
                                        Remark1 = Target.Remark1,
                                        Remark2 = Target.Remark2,
                                        Create_Time = Target.Create_Time,
                                        Created_By = Target.Created_By,
                                        Created_By_Name = Target.Created_By_Name,
                                        Last_UpDatetimed_Time = Target.Last_UpDatetimed_Time,
                                        Last_UpDatetimed_By = Target.Last_UpDatetimed_By,
                                        Last_UpDatetimed_By_Name = Target.Last_UpDatetimed_By_Name
                                    };
                                    db.Entry(Merge).State = System.Data.Entity.EntityState.Added;
                                    db.SaveChanges();
                                }
                                #endregion
                            }

                            //土地異動(只改分類)
                            #region 土地分類修改
                            if (Header.Transaction_Type == "B240")
                            {
                                string changeFieldName = "Asset_Category_code";
                                AS_Assets_Land_Change AssetChange = db.AS_Assets_Land_Change.FirstOrDefault(
                                    o => o.TRX_Header_ID == model.Header.Form_Number
                                        && o.Field_Name == changeFieldName);

                                if (AssetChange != null)
                                {
                                    AssetChange.After_Data = asset.New_Asset_Category_code;
                                    db.Entry(AssetChange).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    AS_Assets_Land_Change newAssetChange = new AS_Assets_Land_Change();
                                    newAssetChange.TRX_Header_ID = Header.Form_Number;
                                    newAssetChange.AS_Assets_Land_ID = string.IsNullOrEmpty(asset.Asset_Land_ID) ? 0 : int.Parse(asset.Asset_Land_ID);
                                    newAssetChange.Field_Name = changeFieldName;
                                    newAssetChange.Modify_Type = "U";
                                    newAssetChange.Before_Data = asset.Asset_Category_code;
                                    newAssetChange.After_Data = asset.Asset_Category_code;
                                    newAssetChange.Create_Time = asset.Create_Time;
                                    newAssetChange.Created_By = asset.Created_By;
                                    newAssetChange.Created_By_Name = asset.Created_By_Name;
                                    newAssetChange.Last_UpDatetimed_Time = asset.Last_Updated_Time;
                                    newAssetChange.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);
                                    newAssetChange.Last_UpDatetimed_By_Name = asset.Last_Updated_By_Name;
                                    db.Entry(newAssetChange).State = System.Data.Entity.EntityState.Added;
                                    db.SaveChanges();
                                }
                            }
                            #endregion
                        }
                    }
                }

                trans.Complete();
            }
            //}
            //catch(Exception ex)
            //{

            //}
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }

        public AssetHandleResult Create_MPAsset(LandMPModel model)
        {
            //try
            //{
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    //Header
                    AS_TRX_Headers Header = LandGeneral.ToAS_Assets_Land_Header(model.Header);
                    //if (!db.AS_Assets_Land_Header.Any(h => h.TRX_Header_ID == Header.Form_Number))
                    //{
                    //    db.Entry(Header).State = System.Data.Entity.EntityState.Added;
                    //    db.SaveChanges();
                    //}

                    Decimal TotalAreaSize = 0, TotalOriginalCost = 0;
                    
                    if (model.MPAssets != null && model.MPAssets.Any(p => p != null))
                    {
                        foreach (var asset in model.MPAssets)
                        {
                            AS_Assets_Land_MP_Record MPRecord = LandGeneral.ToAS_Assets_Land_MP_Record(Header.Form_Number, asset);
                            db.Entry(MPRecord).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                            continue;
                        }
                    }
                }

                trans.Complete();
            }
            //}
            //catch(Exception ex)
            //{

            //}
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }
        public AssetHandleResult Create_MPAssetRetire(LandMPModel model)
        {
            //try
            //{
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    //Header
                    AS_TRX_Headers Header = LandGeneral.ToAS_Assets_Land_Header(model.Header);
                    //if (!db.AS_Assets_Land_Header.Any(h => h.TRX_Header_ID == Header.Form_Number))
                    //{
                    //    db.Entry(Header).State = System.Data.Entity.EntityState.Added;
                    //    db.SaveChanges();
                    //}

                    Decimal TotalAreaSize = 0, TotalOriginalCost = 0;

                    if (model.RetireAssets != null && model.RetireAssets.Any(p => p != null))
                    {
                        foreach (var asset in model.RetireAssets)
                        {
                            AS_Assets_Land_MP_Retire MPRecord = LandGeneral.ToAS_Assets_Land_MP_Retire(Header.Form_Number, asset);
                            MPRecord.Reason_Code = model.LD.Reason_Code;
                            MPRecord.Retire_Cost = model.LD.Retire_Cost;
                            MPRecord.Sell_Cost = model.LD.Sell_Cost;
                            MPRecord.Sell_Amount = model.LD.Sell_Amount;
                            MPRecord.Reval_Adjustment_Amount = model.LD.Reval_Adjustment_Amount;
                            MPRecord.Reval_Reserve = model.LD.Reval_Reserve;
                            db.Entry(MPRecord).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                            continue;
                        }
                    }
                }

                trans.Complete();
            }
            //}
            //catch(Exception ex)
            //{

            //}
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }
        public AssetHandleResult Create_MPAssetChange(LandMPModel model)
        {
            //try
            //{
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    if (model.MPAssets != null && model.MPAssets.Any(p => p != null))
                    {
                        foreach (var asset in model.MPAssets)
                        {
                            //Header
                            AS_TRX_Headers Header = LandGeneral.ToAS_Assets_Land_Header(model.Header);

                            AS_Assets_Land_MP_Record MPRecord = LandGeneral.ToAS_Assets_Land_MP_Record(Header.Form_Number, asset);
                            db.Entry(MPRecord).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();

                            string changeFieldName = "Asset_Category_code";
                            AS_Assets_Land_MP_Change_Record AssetChange = db.AS_Assets_Land_MP_Change_Record.FirstOrDefault(
                                o => o.TRX_Header_ID == model.Header.Form_Number
                                    && o.Field_Name == changeFieldName);

                            if (AssetChange != null)
                            {
                                AssetChange.After_Data = asset.New_Asset_Category_code;
                                db.Entry(AssetChange).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                AS_Assets_Land_MP_Change_Record newAssetChange = new AS_Assets_Land_MP_Change_Record();
                                newAssetChange.ID = 0;
                                newAssetChange.TRX_Header_ID = Header.Form_Number;
                                newAssetChange.AS_Assets_Land_MP_Record_ID = asset.ID;
                                newAssetChange.UPD_File = "";
                                newAssetChange.Field_Name = changeFieldName;
                                newAssetChange.Modify_Type = "U";
                                newAssetChange.Before_Data = asset.Asset_Category_Code;
                                newAssetChange.After_Data = asset.Asset_Category_Code;
                                newAssetChange.Create_Time = asset.Create_Time;
                                newAssetChange.Created_By = asset.Created_By;
                                newAssetChange.Created_By_Name = asset.Created_By_Name;
                                newAssetChange.Last_UpDatetimed_Time = asset.Last_Updated_Time;
                                newAssetChange.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);
                                newAssetChange.Last_UpDatetimed_By_Name = asset.Last_Updated_By_Name;
                                db.Entry(newAssetChange).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                        }
                    }
                }

                trans.Complete();
            }
            //}
            //catch(Exception ex)
            //{

            //}
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }
        public AssetHandleResult Create_MPAssetLefeChange(LandMPModel model)
        {
            //try
            //{
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    if (model.MPAssets != null && model.MPAssets.Any(p => p != null))
                    {
                        foreach (var asset in model.MPAssets)
                        {
                            //Header
                            AS_TRX_Headers Header = LandGeneral.ToAS_Assets_Land_Header(model.Header);

                            AS_Assets_Land_MP_Record MPRecord = LandGeneral.ToAS_Assets_Land_MP_Record(Header.Form_Number, asset);
                            db.Entry(MPRecord).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();

                            string changeFieldName1 = "Life_Years", changeFieldName2 = "Life_Months";
                            AS_Assets_Land_MP_Change_Record AssetChange1 = db.AS_Assets_Land_MP_Change_Record.FirstOrDefault(
                                o => o.TRX_Header_ID == model.Header.Form_Number
                                    && o.Field_Name == changeFieldName1);
                            AS_Assets_Land_MP_Change_Record AssetChange2 = db.AS_Assets_Land_MP_Change_Record.FirstOrDefault(
                               o => o.TRX_Header_ID == model.Header.Form_Number
                                   && o.Field_Name == changeFieldName2);
                            if (AssetChange1 != null)
                            {
                                AssetChange1.After_Data = asset.New_Life_Years.ToString();
                                db.Entry(AssetChange1).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                AS_Assets_Land_MP_Change_Record newAssetChange = new AS_Assets_Land_MP_Change_Record();
                                newAssetChange.ID = 0;
                                newAssetChange.TRX_Header_ID = Header.Form_Number;
                                newAssetChange.AS_Assets_Land_MP_Record_ID = asset.ID;
                                newAssetChange.UPD_File = "";
                                newAssetChange.Field_Name = changeFieldName1;
                                newAssetChange.Modify_Type = "U";
                                newAssetChange.Before_Data = asset.Life_Years.ToString();
                                newAssetChange.After_Data = asset.Life_Years.ToString();
                                newAssetChange.Create_Time = asset.Create_Time;
                                newAssetChange.Created_By = asset.Created_By;
                                newAssetChange.Created_By_Name = asset.Created_By_Name;
                                newAssetChange.Last_UpDatetimed_Time = asset.Last_Updated_Time;
                                newAssetChange.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);
                                newAssetChange.Last_UpDatetimed_By_Name = asset.Last_Updated_By_Name;
                                db.Entry(newAssetChange).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                            if (AssetChange2 != null)
                            {
                                AssetChange2.After_Data = asset.New_Life_Months.ToString();
                                db.Entry(AssetChange1).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                AS_Assets_Land_MP_Change_Record newAssetChange = new AS_Assets_Land_MP_Change_Record();
                                newAssetChange.ID = 0;
                                newAssetChange.TRX_Header_ID = Header.Form_Number;
                                newAssetChange.AS_Assets_Land_MP_Record_ID = asset.ID;
                                newAssetChange.UPD_File = "";
                                newAssetChange.Field_Name = changeFieldName2;
                                newAssetChange.Modify_Type = "U";
                                newAssetChange.Before_Data = asset.Life_Months.ToString();
                                newAssetChange.After_Data = asset.Life_Months.ToString();
                                newAssetChange.Create_Time = asset.Create_Time;
                                newAssetChange.Created_By = asset.Created_By;
                                newAssetChange.Created_By_Name = asset.Created_By_Name;
                                newAssetChange.Last_UpDatetimed_Time = asset.Last_Updated_Time;
                                newAssetChange.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);
                                newAssetChange.Last_UpDatetimed_By_Name = asset.Last_Updated_By_Name;
                                db.Entry(newAssetChange).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                        }
                    }
                }

                trans.Complete();
            }
            //}
            //catch(Exception ex)
            //{

            //}
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }

        private string CreateLandSplitAssetNumber()
        {
            string CToDay = (DateTime.Today.Year - 1911).ToString().PadLeft(3, '0') + DateTime.Today.ToString("MMdd");
            string AssetNumber = "";

            using (var db = new AS_LandBankEntities())
            {
                AssetNumber = db.AS_Assets_Land_Split.Where(o => o.Asset_Number.StartsWith(CToDay)).Max(o => o.Asset_Number);
            }
            if (AssetNumber != null)
            {
                int Seq = Convert.ToInt32(AssetNumber.Substring(8)) + 1;
                AssetNumber = CToDay + "-" + Seq.ToString().PadLeft(7, '0');
            }
            else
            {
                AssetNumber = CToDay + "-" + "0000001";
            }

            return AssetNumber;
        }

        public AssetLandModel GetAssetLandDetail(int ID, string TableName)
        {
            AssetLandModel asset = null;
            using (var db = new AS_LandBankEntities())
            {
                if (TableName == "LandSplit")
                {
                    AS_Assets_Land_Split AssetSplit = (from s in db.AS_Assets_Land_Split
                                                       where s.ID == ID
                                                       select s).FirstOrDefault();

                    if (AssetSplit != null)
                    {
                        asset = LandGeneral.ToAssetLandModel(AssetSplit);
                    }
                }
                else if (TableName == "LandConsolidation")
                {
                    AS_Assets_Land_ML AssetMerge = (from m in db.AS_Assets_Land_ML
                                                    where m.ID == ID
                                                    select m).FirstOrDefault();

                    if (AssetMerge != null)
                    {
                        asset = LandGeneral.ToAssetLandModel(AssetMerge);
                    }
                }
                else
                {
                    AS_Assets_Land_Record AssetLand = (from a in db.AS_Assets_Land_Record
                                                       where a.ID == ID
                                                       select a).FirstOrDefault();

                    if (AssetLand != null)
                    {
                        asset = LandGeneral.ToAssetLandModel(AssetLand);

                        var query = from b in db.AS_Asset_Cost
                                    where b.TRX_Header_ID == AssetLand.TRX_Header_ID &&
                                          b.Asset_Number == AssetLand.Asset_Number
                                    select b;

                        if (query.Any())
                        {
                            asset.AddCost = query.First().Add_Cost;
                            asset.LessCost = query.First().Less_Cost;
                        }
                    }
                    if (TableName == "LandReclassify") // 土地重分類
                    {
                        string changeFieldName = "Asset_Category_code";
                        AS_Assets_Land_Change AssetChange = (from m in db.AS_Assets_Land_Change
                                                        where m.TRX_Header_ID == AssetLand.TRX_Header_ID &&
                                                                m.Field_Name == changeFieldName
                                                             select m).FirstOrDefault();

                        if (AssetChange != null)
                        {
                            asset.New_Asset_Category_code = AssetChange.After_Data;
                        }
                    }
                }


            }

            return asset;
        }
        public AssetLandMPModel GetAssetLandMPDetail(int ID, string TableName)
        {
            AssetLandMPModel asset = null;
            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_Land_MP_Record AssetLandMP = (from a in db.AS_Assets_Land_MP_Record
                                                      where a.ID == ID
                                                   select a).FirstOrDefault();

                if (AssetLandMP != null)
                {
                    asset = LandGeneral.ToAssetLandMPModel(AssetLandMP);
                }
            }

            return asset;
        }
        public AssetLandMPRetireModel GetAssetLandMPRetireDetail(int ID)
        {
            AssetLandMPRetireModel asset = null;
            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_Land_MP_Retire AssetLandMPRetire = (from a in db.AS_Assets_Land_MP_Retire
                                                              where a.ID == ID
                                                        select a).FirstOrDefault();

                if (AssetLandMPRetire != null)
                {
                    asset = LandGeneral.ToAssetLandMPRetireModel(AssetLandMPRetire);
                }
            }

            return asset;
        }
        public AssetLandRetireModel GetAssetLandRetireDetail(int ID)
        {
            AssetLandRetireModel asset = null;
            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_Land_Retire AssetRetire = (from s in db.AS_Assets_Land_Retire
                                                    where s.ID == ID
                                                   select s).FirstOrDefault();

                if (AssetRetire != null)
                {
                    asset = LandGeneral.ToAssetRetireLand(AssetRetire);
                }
            }

            return asset;
        }

        public AssetLandChangeModel GetAssetLandChangeDetail(int ID)
        {
            AssetLandChangeModel asset = null;
            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_Land_Change AssetChange = (from s in db.AS_Assets_Land_Change
                                                     where s.ID == ID
                                                     select s).FirstOrDefault();

                if (AssetChange != null)
                {
                    asset = LandGeneral.ToAssetChangeLand(AssetChange);
                }
            }

            return asset;
        }
        public AssetLandModel GetAssetFormLandByAssetNumber(string AssetNumber)
        {
            AssetLandModel asset = null;
            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_Land AssetLand = (from a in db.AS_Assets_Land
                                            where a.Asset_Number == AssetNumber
                                            select a).FirstOrDefault();

                if (AssetLand != null)
                {
                    asset = LandGeneral.ToAssetLandModel(AssetLand);
                }
            }

            return asset;
        }

        public IEnumerable<CodeItem> SearchAssetsByCategoryCode(string CategoryCode)
        {
            IEnumerable<CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                result = (from m in db.AS_Assets_Land
                          where m.Asset_Category_code == CategoryCode
                          select new CodeItem()
                          {
                              Value = m.Asset_Number,
                              Text = m.City_Name+m.District_Name+m.Section_Name+m.Sub_Section_Name
                          }).ToList();
            }
            return result;
        }

        public LandModel GetDetail(string TRXHeaderID)
        {
            LandModel result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in
                            (
                                from h in db.AS_TRX_Headers
                                where h.Form_Number == TRXHeaderID
                                select h
                            )
                            join mas in db.AS_Assets_Land_Record on mh.Form_Number equals mas.TRX_Header_ID into temp
                            join mes in db.AS_Assets_Land_Retire on mh.Form_Number equals mes.TRX_Header_ID into retire
                            join mchange in db.AS_Assets_Land_Change on mh.Form_Number equals mchange.TRX_Header_ID  into changerecord
                            from mas in temp.DefaultIfEmpty()
                            select new { mh, mas, retire, changerecord };

                if (query != null && query.Count() > 0)
                {
                    LandModel lmode = new LandModel();
                    lmode.Header = query.ToList().Select(o => LandGeneral.ToLandHeaderModel(o.mh)).First();
                    if (lmode.Header.Transaction_Type != "B230")
                    {
                        lmode.Assets = query.ToList().Select(o => LandGeneral.ToAssetLandModel(o.mas));
                    }
                    result = new LandModel()
                    {
                        Header = query.ToList().Select(o => LandGeneral.ToLandHeaderModel(o.mh)).First(),
                        Assets = query.ToList().Select(o => LandGeneral.ToAssetLandModel(o.mas))
                    };
                    if (result.Header.Transaction_Type == "B240")//土地重分類
                    {
                        AssetLandModel assets= result.Assets.FirstOrDefault();
                        if (assets != null)
                        {
                            var changerecord = query.ToList().FirstOrDefault().changerecord.FirstOrDefault();
                            if (changerecord != null)
                                assets.New_Asset_Category_code = changerecord.After_Data;
                        }
                    }
                }

                //分割要再額外抓Split檔
                if (result.Header.Transaction_Type == "B210")
                {
                    var splitList = db.AS_Assets_Land_Split.Where(s => s.TRX_Header_ID == TRXHeaderID).ToList();
                    if (splitList.Count > 0)
                    {
                        List<AssetLandModel> modelList = new List<AssetLandModel>();
                        foreach (AS_Assets_Land_Split splitData in splitList)
                        {
                            AssetLandModel o = LandGeneral.ToAssetLandModel(splitData);
                            if (o != null)
                                modelList.Add(o);
                        }
                        if (modelList.Count > 0)
                            result.ExtraAssets = modelList;
                        //var assetList = splitList.Select(o => o!= null && LandGeneral.ToAssetLandModel(o));

                    }
                    //result.ExtraAssets = db.AS_Assets_Land_Split.Where(s => s.TRX_Header_ID == TRXHeaderID).ToList().Select(o => LandGeneral.ToAssetLandModel(o));
                }
                else if (result.Header.Transaction_Type == "B220")
                {
                    var mergeList = db.AS_Assets_Land_ML.Where(s => s.TRX_Header_ID == TRXHeaderID).ToList();
                    if (mergeList.Count > 0)
                    {
                        List<AssetLandModel> modelList = new List<AssetLandModel>();
                        foreach (AS_Assets_Land_ML mergeData in mergeList)
                        {
                            AssetLandModel o = LandGeneral.ToAssetLandModel(mergeData);
                            if (o != null)
                                modelList.Add(o);
                        }
                        if (modelList.Count > 0)
                            result.ExtraAssets = modelList;
                        //var assetList = splitList.Select(o => o!= null && LandGeneral.ToAssetLandModel(o));

                    }
                    //result.ExtraAssets = db.AS_Assets_Land_ML.Where(m => m.TRX_Header_ID == TRXHeaderID).ToList().Select(o => LandGeneral.ToAssetLandModel(o));
                }
            }

            return result;
        }

        public LandMPModel GetMPDetail(string TRXHeaderID)
        {
            LandMPModel result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in
                            (
                                from h in db.AS_TRX_Headers
                                where h.Form_Number == TRXHeaderID
                                select h
                            )
                            join mes in db.AS_Assets_Land_MP_Record on mh.Form_Number equals mes.TRX_Header_ID into temp
                            from mas in temp.DefaultIfEmpty()
                            join mrs in db.AS_Assets_Land_MP_Retire on mh.Form_Number equals mrs.TRX_Header_ID into temp2
                            from mrs in temp2.DefaultIfEmpty()
                            select new { mh, mas, mrs };

                if (query != null && query.Count() > 0)
                {
                    //LandMPModel lmode = new LandMPModel();
                    //lmode.Header = query.ToList().Select(o => LandGeneral.ToLandHeaderModel(o.mh)).First();
                    result = new LandMPModel();
                    result.Header = query.ToList().Select(o => LandGeneral.ToLandHeaderModel(o.mh)).First();
                    if (query.ToList()[0].mas != null)
                    {
                        result.MPAssets = query.ToList().Select(o => LandGeneral.ToAssetLandMPModel(o.mas));
                    }
                    else
                        result.MPAssets = new  List<AssetLandMPModel>();
                    if (query.ToList()[0].mrs != null)
                    {
                        result.RetireAssets = query.ToList().Select(o => LandGeneral.ToAssetLandMPRetireModel(o.mrs));
                    }
                    else
                        result.RetireAssets = new List<AssetLandMPRetireModel>();
                }
            }

            return result;
        }
        public LandModel GetRetireDetail(string TRXHeaderID)
        {
            LandModel result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in
                            (
                                from h in db.AS_TRX_Headers
                                where h.Form_Number == TRXHeaderID
                                select h
                            )
                            join mes in db.AS_Assets_Land_Retire on mh.Form_Number equals mes.TRX_Header_ID into retire
                            from mas in retire.DefaultIfEmpty()
                            select new { mh, mas };

                if (query != null && query.Count() > 0)
                {
                    LandModel lmode = new LandModel();
                    result = new LandModel()
                    {
                        Header = query.ToList().Select(o => LandGeneral.ToLandHeaderModel(o.mh)).First(),
                        RetireAssets = query.ToList().Select(o => LandGeneral.ToAssetRetireLand(o.mas))
                    };
                }
            }

            return result;
        }

        public bool GetDocID(string DocCode, out int DocID)
        {
            bool result = true;
            string newDocCode = DocCode.Replace("0H", "H");
            using (var db = new AS_LandBankEntities())
            {
                result = db.AS_Doc_Name.Any(m => m.Doc_No == newDocCode);
                DocID = (result) ? db.AS_Doc_Name.First(m => m.Doc_No == newDocCode).ID : 0;
            }
            return result;
        }

        public LandHeaderModel GetHeader(string AssetNumber, string Type)
        {
            LandHeaderModel result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mr in
                            (
                                from r in db.AS_TRX_Headers
                                where r.Form_Number == AssetNumber
                                select r
                            )
                            join mh in
                            (
                                from h in db.AS_TRX_Headers
                                where h.Transaction_Type == Type
                                select h
                            ) on mr.Form_Number equals mh.Form_Number
                            select mh;
                if (query != null)
                {
                    result = query.ToList().Select(o => LandGeneral.ToLandHeaderModel(o)).FirstOrDefault();
                }
            }
            return result;
        }

        public LandHeaderModel GetHeaderData(string TRXHeaderID)
        {
            LandHeaderModel result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from h in db.AS_TRX_Headers
                            where h.Form_Number == TRXHeaderID
                            select h;

                if (query != null)
                {
                    result = query.ToList().Select(o => LandGeneral.ToLandHeaderModel(o)).FirstOrDefault();
                }
            }
            return result;
        }

        public AssetHandleResult HandleHeader(LandHeaderModel Header)
        {
            AssetHandleResult Result = new AssetHandleResult()
            {
                TRX_Header_ID = Header.Form_Number
            };

            using (var db = new AS_LandBankEntities())
            {
                //Header
                //Insert
                if (!db.AS_TRX_Headers.Any(h => h.Form_Number == Header.Form_Number))
                {
                    db.Entry(LandGeneral.ToAS_Assets_Land_Header(Header)).State = System.Data.Entity.EntityState.Added;
                    db.SaveChanges();
                }
                //Update
                else
                {
                    var Land_Header = db.AS_TRX_Headers.First(h => h.Form_Number == Header.Form_Number);
                    Land_Header.Book_Type_Code = Header.Book_Type_Code;
                    Land_Header.Office_Branch = Header.Office_Branch;
                    Land_Header.Transaction_Datetime_Entered = Header.Transaction_Datetime_Entered;
                    Land_Header.Description = Header.Description;
                    Land_Header.Auto_Post = Header.Auto_Post;
                    Land_Header.Amotized_Adjustment_Flag = Header.Amotized_Adjustment_Flag;
                    Land_Header.Last_UpDatetimed_By = 0;
                    Land_Header.Last_UpDatetimed_Time = DateTime.Now;
                    db.Entry(Land_Header).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }

            return Result;
        }

        public IndexModel Index(SearchModel condition)
        {
            IndexModel result = new IndexModel();
            result.condition = condition;

            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_TRX_Headers
                            join mc in
                            (
                               from mr in db.AS_TRX_Headers
                               group mr by new { mr.Form_Number } into mrg
                               select new
                               {
                                   TRX_Header_ID = mrg.Key.Form_Number,
                                   Count = mrg.Count()
                               }
                            ) on mh.Form_Number equals mc.TRX_Header_ID into mhtemp
                            from mc in mhtemp.DefaultIfEmpty()
                                //join office in db.AS_Keep_Position on mh.Office_Branch equals office.Keep_Dept.Keep_Position_Code into toffice
                            join office in db.AS_Keep_Position.Where(p=>p.Parent_ID.Value ==1) on  mh.Office_Branch  equals office.Keep_Dept into toffice
                           
                        from office in toffice.DefaultIfEmpty()
                        join flow in
                        (
                           from ff in db.AS_Code_Table
                           where ff.Class == "P02"
                           select ff
                        ) on mh.Transaction_Status equals flow.Code_ID into tflow
                        from flow in tflow.DefaultIfEmpty()
                        select new { mh, mc, office, flow };

                if (!string.IsNullOrEmpty(condition.Transaction_Type))
                {
                    query = query.Where(m => m.mh.Transaction_Type == condition.Transaction_Type);
                }

                if (!string.IsNullOrEmpty(condition.TRXHeaderID))
                {
                    query = query.Where(m => m.mh.Form_Number == condition.TRXHeaderID);
                }

                if (!string.IsNullOrEmpty(condition.FlowStatus))
                {
                    query = query.Where(m => m.mh.Transaction_Status == condition.FlowStatus);
                }

                if (!string.IsNullOrEmpty(condition.BookTypeCode))
                {
                    query = query.Where(m => m.mh.Book_Type_Code == condition.BookTypeCode);
                }

                DateTime CompareDate = new DateTime();

                if (condition.BeginDate != null)
                {
                    CompareDate = condition.BeginDate.Value.AddDays(1);
                    query = query.Where(m => m.mh.Create_Time >= condition.BeginDate && m.mh.Create_Time < CompareDate);
                }

                if (condition.PostDate != null)
                {
                    CompareDate = condition.PostDate.Value.AddDays(1);
                    query = query.Where(m => m.mh.Accounting_Datetime >= condition.PostDate && m.mh.Accounting_Datetime < CompareDate);
                }

                if (!string.IsNullOrEmpty(condition.OfficeBranch))
                {
                    query = query.Where(m => m.mh.Office_Branch == condition.OfficeBranch);
                }

                if (!string.IsNullOrEmpty(condition.Remark))
                {
                    query = query.Where(m => m.mh.Description == condition.Remark);
                }

                result.TotalAmount = query.Count();
                //分頁
                if (condition.Page != null)
                {
                    query = query.OrderByDescending(m => m.mh.Create_Time).Skip((condition.Page.Value - 1) * Convert.ToInt32(condition.PageSize)).Take(Convert.ToInt32(condition.PageSize));
                }

                result.Items = query.ToList().Select(o => new LandHeaderModel()
                {
                    ID = o.mh.ID,
                    Form_Number = o.mh.Form_Number,
                    Transaction_Type = o.mh.Transaction_Type,
                    Transaction_Status = o.mh.Transaction_Status,
                    Book_Type_Code = o.mh.Book_Type_Code,
                    Transaction_Datetime_Entered = o.mh.Transaction_Datetime_Entered,
                    Accounting_Datetime = o.mh.Accounting_Datetime,
                    Office_Branch = o.mh.Office_Branch,
                    Description = o.mh.Description,
                    Create_Time = o.mh.Create_Time,
                    Created_By = o.mh.Created_By,
                    Last_UpDatetimed_Time = o.mh.Last_UpDatetimed_Time,
                    Last_UpDatetimed_By = o.mh.Last_UpDatetimed_By,
                    AssetsCount = (o.mc != null) ? o.mc.Count : 0,
                    OfficeBranchName = (o.office != null) ? o.office.Keep_Position_Name : "",
                    TransactionStatusName = (o.flow != null) ? o.flow.Text : "",
                    Amotized_Adjustment_Flag = o.mh.Amotized_Adjustment_Flag,
                    Amotized_Adjustment_Flag_Name = string.IsNullOrEmpty(o.mh.Amotized_Adjustment_Flag) ? "" : ((Enums.Amotized_Adjustment_Flag)(int.Parse(o.mh.Amotized_Adjustment_Flag))).GetDisplayName()
                });
            }

            return result;
        }

        public AssetsIndexModel SearchAssets(AssetsSearchModel condition)
        {
            AssetsIndexModel result = new AssetsIndexModel();
            result.condition = condition;

            using (var db = new AS_LandBankEntities())
            {
                var query = from m in db.AS_Assets_Land
                            join citycode in
                            (
                               from cc in db.AS_VW_Code_Table
                               where cc.Class == "City"
                               select cc
                            ) on m.City_Code equals citycode.Code_ID into tcitycode
                            from citycode in tcitycode.DefaultIfEmpty()
                            join districtcode in
                            (
                               from dd in db.AS_Land_Code
                               select new { dd.District_Code, dd.District_Name, dd.City_Code }
                            ).Distinct() on new { m.City_Code, m.District_Code } equals new { districtcode.City_Code, districtcode.District_Code } into tdistrictcode
                            from districtcode in tdistrictcode.DefaultIfEmpty()
                            join sectioncode in
                            (
                               from dd in db.AS_Land_Code
                               select new { dd.District_Code, dd.District_Name, dd.City_Code, dd.Section_Code, dd.Sectioni_Name }
                            ).Distinct() on new { m.City_Code, m.District_Code, m.Section_Code } equals new { sectioncode.City_Code, sectioncode.District_Code, sectioncode.Section_Code } into tsectioncode
                            from sectioncode in tsectioncode.DefaultIfEmpty()
                            select new { m, citycode, districtcode, sectioncode };

                if (!string.IsNullOrEmpty(condition.AssetNumber))
                {
                    query = query.Where(o => o.m.Asset_Number == condition.AssetNumber);
                }
                if (!string.IsNullOrEmpty(condition.City_Code))
                {
                    query = query.Where(o => o.m.City_Code == condition.City_Code);
                }
                if (!string.IsNullOrEmpty(condition.District_Code))
                {
                    query = query.Where(o => o.m.District_Code == condition.District_Code);
                }
                if (!string.IsNullOrEmpty(condition.Section_Code))
                {
                    query = query.Where(o => o.m.Section_Code == condition.Section_Code);
                }
                if (!string.IsNullOrEmpty(condition.Filial_Land_Number_Begin))
                {
                    query = query.Where(o => String.Compare(o.m.Filial_Land_Number, condition.Filial_Land_Number_Begin) >= 0);
                }
                if (!string.IsNullOrEmpty(condition.Filial_Land_Number_End))
                {
                    query = query.Where(o => String.Compare(o.m.Filial_Land_Number, condition.Filial_Land_Number_End) <= 0);
                }
                result.TotalAmount = query.Count();

                //分頁
                if (condition.Page != null)
                {
                    query = query.OrderByDescending(o => o.m.Create_Time).Skip((condition.Page.Value - 1) * Convert.ToInt32(condition.PageSize)).Take(Convert.ToInt32(condition.PageSize));
                }

                result.Items = (query.Count() > 0) ? query.ToList().Select(o => new AssetLandModel()
                {
                    ID = o.m.ID,
                    Asset_Number = o.m.Asset_Number,
                    Current_units = o.m.Current_units,
                    Datetime_Placed_In_Service = o.m.Datetime_Placed_In_Service,
                    Old_Asset_Number = o.m.Old_Asset_Number,
                    Asset_Category_code = o.m.Asset_Category_code,
                    Location_Disp = o.m.Location_Disp,
                    Description = o.m.Description,
                    Urban_Renewal = o.m.Urban_Renewal,
                    City_Code = o.m.City_Code,
                    City_Name = (o.citycode != null) ? o.citycode.Text : "",
                    District_Code = o.m.District_Code,
                    District_Name = (o.districtcode != null) ? o.districtcode.District_Name : "",
                    Section_Code = o.m.Section_Code,
                    Section_Name = (o.sectioncode != null) ? o.sectioncode.Sectioni_Name : "",
                    Sub_Section_Name = o.m.Sub_Section_Name,
                    Parent_Land_Number = o.m.Parent_Land_Number,
                    Filial_Land_Number = o.m.Filial_Land_Number,
                    Authorization_Number = o.m.Authorization_Number,
                    Authorized_name = o.m.Authorized_name,
                    Area_Size = o.m.Area_Size,
                    Authorized_Scope_Molecule = o.m.Authorized_Scope_Molecule,
                    Authorized_Scope_Denomminx = o.m.Authorized_Scope_Denomminx,
                    Authorized_Area = o.m.Authorized_Area,
                    Used_Type = o.m.Used_Type,
                    Used_Status = o.m.Used_Status,
                    Obtained_Method = o.m.Obtained_Method,
                    Used_Partition = o.m.Used_Partition,
                    Is_Marginal_Land = o.m.Is_Marginal_Land,
                    Own_Area = o.m.Own_Area,
                    NONOwn_Area = o.m.NONOwn_Area,
                    Original_Cost = o.m.Original_Cost,
                    Deprn_Amount = o.m.Deprn_Amount,
                    Current_Cost = o.m.Current_Cost,
                    Reval_Adjustment_Amount = o.m.Reval_Adjustment_Amount,
                    Reval_Land_VAT = o.m.Reval_Land_VAT,
                    Reval_Reserve = o.m.Reval_Reserve,
                    Business_Area_Size = o.m.Business_Area_Size,
                    Business_Book_Amount = o.m.Business_Book_Amount,
                    Business_Reval_Reserve = o.m.Business_Reval_Reserve,
                    NONBusiness_Area_Size = o.m.NONBusiness_Area_Size,
                    NONBusiness_Book_Amount = o.m.NONBusiness_Book_Amount,
                    NONBusiness_Reval_Reserve = o.m.NONBusiness_Reval_Reserve,
                    Year_Number = o.m.Year_Number,
                    Announce_Amount = o.m.Announce_Amount,
                    Announce_Price = o.m.Announce_Price,
                    Tax_Type = o.m.Tax_Type,
                    Reduction_reason = o.m.Reduction_reason,
                    Transfer_Price = o.m.Transfer_Price,
                    Reduction_Area = o.m.Reduction_Area,
                    Declared_Price = o.m.Declared_Price,
                    Dutiable_Amount = o.m.Dutiable_Amount,
                    Remark1 = o.m.Remark1,
                    Remark2 = o.m.Remark2,
                    Retire_Cost = o.m.Retire_Cost,
                    Sell_Amount = o.m.Sell_Amount,
                    Sell_Cost = o.m.Sell_Cost,
                    Delete_Reason = o.m.Delete_Reason,
                    Land_Item = o.m.Land_Item,
                    CROP = o.m.CROP,
                    Create_Time = o.m.Create_Time,
                    Created_By = o.m.Created_By,
                    Last_Updated_Time = o.m.Last_UpDatetimed_Time,
                    Last_Updated_By = o.m.Last_UpDatetimed_By.HasValue ? o.m.Last_UpDatetimed_By.Value.ToString() : ""
                }) : new List<AssetLandModel>();

            }
            return result;
        }
        public AssetsMPIndexModel SearchAssetsMP(AssetsMPSearchModel condition)
        {
            AssetsMPIndexModel result = new AssetsMPIndexModel();
            result.condition = condition;

            using (var db = new AS_LandBankEntities())
            {
                var query = from m in db.AS_Assets_Land_MP
                            select new { m};

                result.TotalAmount = query.Count();

                //分頁
                if (condition.Page != null)
                {
                    query = query.OrderByDescending(o => o.m.Create_Time).Skip((condition.Page.Value - 1) * Convert.ToInt32(condition.PageSize)).Take(Convert.ToInt32(condition.PageSize));
                }

                result.Items = (query.Count() > 0) ? query.ToList().Select(o => new AssetLandMPModel()
                {
                    ID = o.m.ID,
                    Asset_Number = o.m.Asset_Number,
                    Parent_Asset_Number = o.m.Parent_Asset_Number,
                    Old_Asset_Number = o.m.Old_Asset_Number,
                    Asset_Category_Code = o.m.Asset_Category_code,
                    Date_Placed_In_Service = o.m.Datetime_Placed_In_Service,
                    Current_units = o.m.Current_units.HasValue ? o.m.Current_units.Value : 0,
                    Deprn_Method_Code = o.m.Deprn_Method_Code,
                    Life_Years = o.m.Life_Years.HasValue ? o.m.Life_Years.Value : 0,
                    Life_Months = o.m.Life_Months.HasValue ? o.m.Life_Months.Value : 0,
                    Location_Disp = o.m.Location_Disp,
                    PO_Number = o.m.PO_Number,
                    PO_Destination = o.m.PO_Destination,
                    Model_Number = o.m.Model_Number,
                    Transaction_Date = o.m.Transaction_Date,
                    Assets_Unit = o.m.Assets_Unit,
                    Accessory_Equipment = o.m.Accessory_Equipment,
                    Assigned_NUM = o.m.Assigned_NUM,
                    Asset_Category_Num = o.m.Asset_Category_NUM,
                    Asset_Structure = o.m.Asset_Structure,
                    Current_Cost = o.m.Current_Cost.HasValue ? o.m.Current_Cost.Value : 0,
                    Deprn_Reserve = o.m.Deprn_Reserve.HasValue ? o.m.Deprn_Reserve.Value : 0,
                    Salvage_Value = o.m.Salvage_Value.HasValue ? o.m.Salvage_Value.Value : 0,
                    Remark = o.m.Remark,
                    Create_Time = o.m.Create_Time.HasValue ? o.m.Create_Time.Value : DateTime.Now,
                    Created_By = o.m.Created_By.HasValue ? o.m.Created_By.Value.ToString() : "",
                    Last_Updated_Time = o.m.Last_UpDatetimed_Time,
                    Last_Updated_By = o.m.Last_UpDatetimed_By.HasValue ? o.m.Last_UpDatetimed_By.Value.ToString() : ""
                }) : new List<AssetLandMPModel>();

            }
            return result;
        }

        public AssetHandleResult UpdateToLand(string TRXHeaderID)
        {
            AssetHandleResult result = new AssetHandleResult() { };

            try
            {
                using (var trans = new TransactionScope())
                {
                    using (var db = new AS_LandBankEntities())
                    {
                        AS_Assets_Land_Header Header = db.AS_Assets_Land_Header.First(o => o.TRX_Header_ID == TRXHeaderID);
                        Header.Last_Updated_Time = DateTime.Now;
                        Header.Transaction_Status = "2";
                        db.Entry(Header).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        var query = from m in db.AS_Assets_Land_Record
                                    where m.TRX_Header_ID == TRXHeaderID
                                    select m;

                        foreach (var his in query)
                        {
                            var asset = db.AS_Assets_Land.FirstOrDefault(o => o.Asset_Number == his.Asset_Number);

                            if (asset != null)
                            {
                                asset.Current_units = his.Current_units;
                                asset.Datetime_Placed_In_Service = his.Datetime_Placed_In_Service;
                                asset.Old_Asset_Number = his.Old_Asset_Number;
                                asset.Asset_Category_code = his.Asset_Category_code;
                                asset.Location_Disp = his.Location_Disp;
                                asset.Description = his.Description;
                                asset.Urban_Renewal = his.Urban_Renewal;
                                asset.City_Code = his.City_Code;
                                asset.District_Code = his.District_Code;
                                asset.Section_Code = his.Section_Code;
                                asset.Sub_Section_Name = his.Sub_Section_Name;
                                asset.Parent_Land_Number = his.Parent_Land_Number;
                                asset.Filial_Land_Number = his.Filial_Land_Number;
                                asset.Authorization_Number = his.Authorization_Number;
                                asset.Authorized_name = his.Authorized_name;
                                asset.Area_Size = his.Area_Size;
                                asset.Authorized_Scope_Molecule = his.Authorized_Scope_Molecule;
                                asset.Authorized_Scope_Denomminx = his.Authorized_Scope_Denomminx;
                                asset.Authorized_Area = his.Authorized_Area;
                                asset.Used_Type = his.Used_Type;
                                asset.Used_Status = his.Used_Status;
                                asset.Obtained_Method = his.Obtained_Method;
                                asset.Used_Partition = his.Used_Partition;
                                asset.Is_Marginal_Land = his.Is_Marginal_Land;
                                asset.Own_Area = his.Own_Area;
                                asset.NONOwn_Area = his.NONOwn_Area;
                                asset.Original_Cost = his.Original_Cost;
                                asset.Deprn_Amount = his.Deprn_Amount;
                                asset.Current_Cost = his.Current_Cost;
                                asset.Reval_Adjustment_Amount = his.Reval_Adjustment_Amount;
                                asset.Reval_Land_VAT = his.Reval_Land_VAT;
                                asset.Reval_Reserve = his.Reval_Reserve;
                                asset.Business_Area_Size = his.Business_Area_Size;
                                asset.Business_Book_Amount = his.Business_Book_Amount;
                                asset.Business_Reval_Reserve = his.Business_Reval_Reserve;
                                asset.NONBusiness_Area_Size = his.NONBusiness_Area_Size;
                                asset.NONBusiness_Book_Amount = his.NONBusiness_Book_Amount;
                                asset.NONBusiness_Reval_Reserve = his.NONBusiness_Reval_Reserve;
                                asset.Year_Number = his.Year_Number;
                                asset.Announce_Amount = his.Announce_Amount;
                                asset.Announce_Price = his.Announce_Price;
                                asset.Tax_Type = his.Tax_Type;
                                asset.Reduction_reason = his.Reduction_reason;
                                asset.Transfer_Price = his.Transfer_Price;
                                asset.Reduction_Area = his.Reduction_Area;
                                asset.Declared_Price = his.Declared_Price;
                                asset.Dutiable_Amount = his.Dutiable_Amount;
                                asset.Remark1 = his.Remark1;
                                asset.Remark2 = his.Remark2;
                                asset.Land_Item = his.Land_Item;
                                asset.CROP = his.CROP;
                                asset.Last_UpDatetimed_Time = his.Last_UpDatetimed_Time;
                                asset.Last_UpDatetimed_By = his.Last_UpDatetimed_By;
                                db.Entry(asset).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }

                    trans.Complete();
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }
        private void UpdateChangeData(string TRX_Header_ID, string AS_Assets_Land_ID, AS_Assets_Land_Record Record, AssetLandModel model)
        {
            string userID = model.Last_Updated_By, userName = model.Last_Updated_By_Name;
            if (Record.Asset_Number != model.Asset_Number)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Asset_Number", 
                    Record.Asset_Number, model.Asset_Number, userID, userName);
            }
            if (Record.Asset_Category_code != model.Asset_Category_code)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Asset_Category_code", 
                    Record.Asset_Category_code, model.Asset_Category_code, userID, userName);
            }
            if (Record.Location_Disp != model.Location_Disp)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Location_Disp", 
                    Record.Location_Disp, model.Location_Disp, userID, userName);
            }
            if (Record.Old_Asset_Number != model.Old_Asset_Number)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Old_Asset_Number",
                    Record.Old_Asset_Number, model.Old_Asset_Number, userID, userName);
            }
            if (Record.Description != model.Description)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Description",
                    Record.Description, model.Description, userID, userName);
            }
            if (Record.City_Name != model.City_Name)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "City_Name",
                    Record.City_Name, model.City_Name, userID, userName);
            }
            if (Record.District_Code != model.District_Code)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "District_Code",
                    Record.District_Code, model.District_Code, userID, userName);
            }
            if (Record.District_Name != model.District_Name)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "District_Name",
                    Record.District_Name, model.District_Name, userID, userName);
            }
            if (Record.Section_Code != model.Section_Code)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Section_Code",
                    Record.Section_Code, model.Section_Code, userID, userName);
            }
            if (Record.Section_Name != model.Section_Name)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Section_Name",
                    Record.Section_Name, model.Section_Name, userID, userName);
            }
            if (Record.Sub_Section_Name != model.Sub_Section_Name)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Sub_Section_Name",
                    Record.Sub_Section_Name, model.Sub_Section_Name, userID, userName);
            }
            if (Record.Parent_Land_Number != model.Parent_Land_Number)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Parent_Land_Number",
                    Record.Parent_Land_Number, model.Parent_Land_Number, userID, userName);
            }
            if (Record.Filial_Land_Number != model.Filial_Land_Number)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Filial_Land_Number",
                    Record.Filial_Land_Number, model.Filial_Land_Number, userID, userName);
            }
            if (Record.Authorization_Number != model.Authorization_Number)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Authorization_Number",
                    Record.Authorization_Number, model.Authorization_Number, userID, userName);
            }
            if (Record.Authorized_name != model.Authorized_name)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Authorized_name",
                    Record.Authorized_name, model.Authorized_name, userID, userName);
            }
            if (Record.Area_Size != model.Area_Size)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Area_Size",
                    Record.Area_Size.ToString(), model.Area_Size.ToString(), userID, userName);
            }
            if (Record.Authorized_Scope_Molecule != model.Authorized_Scope_Molecule)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Authorized_Scope_Molecule",
                    Record.Authorized_Scope_Molecule.ToString(), model.Authorized_Scope_Molecule.ToString(), userID, userName);
            }
            if (Record.Authorized_Scope_Denomminx != model.Authorized_Scope_Denomminx)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Authorized_Scope_Denomminx",
                    Record.Authorized_Scope_Denomminx.ToString(), model.Authorized_Scope_Denomminx.ToString(), userID, userName);
            }
            if (Record.Authorized_Area != model.Authorized_Area)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Authorized_Area",
                    Record.Authorized_Area.ToString(), model.Authorized_Area.ToString(), userID, userName);
            }
            if (Record.Used_Type != model.Used_Type)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Used_Type",
                    Record.Used_Type, model.Used_Type, userID, userName);
            }
            if (Record.Used_Status != model.Used_Status)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Used_Status",
                    Record.Used_Status, model.Used_Status, userID, userName);
            }
            if (Record.Obtained_Method != model.Obtained_Method)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Obtained_Method",
                    Record.Obtained_Method, model.Obtained_Method, userID, userName);
            }
            if (Record.Used_Partition != model.Used_Partition)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Used_Partition",
                    Record.Used_Partition, model.Used_Partition, userID, userName);
            }
            if (Record.Is_Marginal_Land != model.Is_Marginal_Land)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Is_Marginal_Land",
                    Record.Is_Marginal_Land, model.Is_Marginal_Land, userID, userName);
            }
            if (Record.Own_Area != model.Own_Area)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Own_Area",
                    Record.Own_Area.ToString(), model.Own_Area.ToString(), userID, userName);
            }
            if (Record.NONOwn_Area != model.NONOwn_Area)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "NONOwn_Area",
                    Record.NONOwn_Area.ToString(), model.NONOwn_Area.ToString(), userID, userName);
            }
            if (Record.Original_Cost != model.Original_Cost)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Original_Cost",
                    Record.Original_Cost.ToString(), model.Original_Cost.ToString(), userID, userName);
            }
            if (Record.Deprn_Amount != model.Deprn_Amount)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Deprn_Amount",
                    Record.Deprn_Amount.ToString(), model.Deprn_Amount.ToString(), userID, userName);
            }
            if (Record.Reval_Land_VAT != model.Reval_Land_VAT)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Reval_Land_VAT",
                    Record.Reval_Land_VAT.ToString(), model.Reval_Land_VAT.ToString(), userID, userName);
            }
            if (Record.Reval_Adjustment_Amount != model.Reval_Adjustment_Amount)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Reval_Adjustment_Amount",
                    Record.Reval_Adjustment_Amount.ToString(), model.Reval_Adjustment_Amount.ToString(), userID, userName);
            }
            if (Record.Business_Area_Size != model.Business_Area_Size)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Business_Area_Size",
                    Record.Business_Area_Size.ToString(), model.Business_Area_Size.ToString(), userID, userName);
            }
            if (Record.NONBusiness_Area_Size != model.NONBusiness_Area_Size)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "NONBusiness_Area_Size",
                    Record.NONBusiness_Area_Size.ToString(), model.NONBusiness_Area_Size.ToString(), userID, userName);
            }
            if (Record.Current_Cost != model.Current_Cost)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Current_Cost",
                    Record.Current_Cost.ToString(), model.Current_Cost.ToString(), userID, userName);
            }
            if (Record.Reval_Reserve != model.Reval_Reserve)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Reval_Reserve",
                    Record.Reval_Reserve.ToString(), model.Reval_Reserve.ToString(), userID, userName);
            }
            if (Record.Business_Book_Amount != model.Business_Book_Amount)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Business_Book_Amount",
                    Record.Business_Book_Amount.ToString(), model.Business_Book_Amount.ToString(), userID, userName);
            }
            if (Record.NONBusiness_Book_Amount != model.NONBusiness_Book_Amount)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "NONBusiness_Book_Amount",
                    Record.NONBusiness_Book_Amount.ToString(), model.NONBusiness_Book_Amount.ToString(), userID, userName);
            }
            if (Record.Business_Reval_Reserve != model.Business_Reval_Reserve)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Business_Reval_Reserve",
                    Record.Business_Reval_Reserve.ToString(), model.Business_Reval_Reserve.ToString(), userID, userName);
            }
            if (Record.Business_Reval_Reserve != model.Business_Reval_Reserve)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "NONBusiness_Reval_Reserve",
                    Record.NONBusiness_Reval_Reserve.ToString(), model.NONBusiness_Reval_Reserve.ToString(), userID, userName);
            }
            if (Record.Datetime_Placed_In_Service != model.Datetime_Placed_In_Service)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Datetime_Placed_In_Service",
                    Record.Datetime_Placed_In_Service.ToString(), model.Datetime_Placed_In_Service.ToString(), userID, userName);
            }
            if (Record.Year_Number != model.Year_Number)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Year_Number",
                    Record.Year_Number.ToString(), model.Year_Number.ToString(), userID, userName);
            }
            if (Record.Announce_Amount != model.Announce_Amount)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Announce_Amount",
                    Record.Announce_Amount.ToString(), model.Announce_Amount.ToString(), userID, userName);
            }
            if (Record.Announce_Price != model.Announce_Price)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Announce_Price",
                    Record.Announce_Price.ToString(), model.Announce_Price.ToString(), userID, userName);
            }
            if (Record.Tax_Type != model.Tax_Type)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Tax_Type",
                    Record.Tax_Type, model.Tax_Type, userID, userName);
            }
            if (Record.Reduction_reason != model.Reduction_reason)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Reduction_reason",
                    Record.Reduction_reason, model.Reduction_reason, userID, userName);
            }
            if (Record.Reduction_Area != model.Reduction_Area)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Reduction_Area",
                    Record.Reduction_Area.ToString(), model.Reduction_Area.ToString(), userID, userName);
            }
            if (Record.Declared_Price != model.Declared_Price)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Declared_Price",
                    Record.Declared_Price.ToString(), model.Declared_Price.ToString(), userID, userName);
            }
            if (Record.Dutiable_Amount != model.Dutiable_Amount)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Dutiable_Amount",
                    Record.Dutiable_Amount.ToString(), model.Dutiable_Amount.ToString(), userID, userName);
            }
            if (Record.Remark1 != model.Remark1)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Remark1",
                    Record.Remark1.ToString(), model.Remark1.ToString(), userID, userName);
            }
            if (Record.Remark2 != model.Remark2)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Remark2",
                    Record.Remark2.ToString(), model.Remark2.ToString(), userID, userName);
            }
            if (Record.Current_units != model.Current_units)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Current_units",
                    Record.Current_units.ToString(), model.Current_units.ToString(), userID, userName);
            }
            if (Record.Transfer_Price != model.Transfer_Price)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Transfer_Price",
                    Record.Transfer_Price.ToString(), model.Transfer_Price.ToString(), userID, userName);
            }
            if (Record.Urban_Renewal != model.Urban_Renewal)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Urban_Renewal",
                    Record.Urban_Renewal.ToString(), model.Urban_Renewal.ToString(), userID, userName);
            }
            if (Record.CROP != model.CROP)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "LAND_ITEM",
                    Record.CROP.ToString(), model.CROP.ToString(), userID, userName);
            }
            if (Record.Expense_Account != model.Expense_Account)
            {
                UpgradeChangeLog(TRX_Header_ID, AS_Assets_Land_ID, "Expense_Account",
                    Record.Expense_Account.ToString(), model.Expense_Account.ToString(), userID, userName);
            }
        }
        private void UpgradeChangeLog(string TRX_Header_ID, string AS_Assets_Land_ID, string Field_Name,
            string Before_Data, string After_Data, string userID, string userName)
        {
            string Modify_Type = "";
            if (string.IsNullOrEmpty(Before_Data))
            {
                Modify_Type = "A";
            }
            else if (string.IsNullOrEmpty(After_Data))
            {
                Modify_Type = "D";
            }
            else
            {
                Modify_Type = "U";
            }
            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_Land_Change AssetChange = db.AS_Assets_Land_Change.FirstOrDefault(
                                   o => o.TRX_Header_ID == TRX_Header_ID
                                       && o.Field_Name == Field_Name);

                if (AssetChange != null)
                {
                    AssetChange.After_Data = After_Data;
                    db.Entry(AssetChange).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    AS_Assets_Land_Change newAssetChange = new AS_Assets_Land_Change();
                    newAssetChange.TRX_Header_ID = TRX_Header_ID;
                    newAssetChange.AS_Assets_Land_ID = string.IsNullOrEmpty(AS_Assets_Land_ID)?0:int.Parse(AS_Assets_Land_ID);
                    newAssetChange.Field_Name = Field_Name;
                    newAssetChange.Modify_Type = Modify_Type;
                    newAssetChange.Before_Data = Before_Data;
                    newAssetChange.After_Data = After_Data;
                    newAssetChange.Create_Time = DateTime.Now;
                    newAssetChange.Created_By = userID;
                    newAssetChange.Created_By_Name = userName;
                    newAssetChange.Last_UpDatetimed_Time = DateTime.Now;
                    newAssetChange.Last_UpDatetimed_By = string.IsNullOrEmpty(userID) ? 0 : decimal.Parse(userID);
                    newAssetChange.Last_UpDatetimed_By_Name = userName;
                    db.Entry(newAssetChange).State = System.Data.Entity.EntityState.Added;
                    db.SaveChanges();
                }
            }
        }
        public AssetHandleResult Update_Asset(LandModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (var asset in model.Assets)
                    {
                        //MP_His Update 
                        AS_Assets_Land_Record Record = db.AS_Assets_Land_Record.FirstOrDefault(m => m.ID == asset.ID);
                        UpdateChangeData(model.Header.Form_Number, asset.Asset_Land_ID, Record, asset);
                        Record.Current_units = asset.Current_units;
                        if(asset.Datetime_Placed_In_Service != null)
                            Record.Datetime_Placed_In_Service = asset.Datetime_Placed_In_Service;
                        if (asset.Old_Asset_Number != null)
                            Record.Old_Asset_Number = !string.IsNullOrEmpty(asset.Old_Asset_Number) ? asset.Old_Asset_Number : "";
                        Record.Asset_Category_code = !string.IsNullOrEmpty(asset.Asset_Category_code) ? asset.Asset_Category_code : "";
                        Record.Location_Disp = !string.IsNullOrEmpty(asset.Location_Disp) ? asset.Location_Disp : "";
                        Record.Description = !string.IsNullOrEmpty(asset.Description) ? asset.Description : "";
                        Record.Urban_Renewal = !string.IsNullOrEmpty(asset.Urban_Renewal) ? asset.Urban_Renewal : "";
                        Record.City_Code = !string.IsNullOrEmpty(asset.City_Code) ? asset.City_Code : "";
                        Record.City_Name = !string.IsNullOrEmpty(asset.City_Name) ? asset.City_Name : "";
                        Record.District_Code = !string.IsNullOrEmpty(asset.District_Code) ? asset.District_Code : "";
                        Record.District_Name = !string.IsNullOrEmpty(asset.District_Name) ? asset.District_Name : "";
                        Record.Section_Code = !string.IsNullOrEmpty(asset.Section_Code) ? asset.Section_Code : "";
                        Record.Section_Name = !string.IsNullOrEmpty(asset.Section_Name) ? asset.Section_Name : "";
                        Record.Sub_Section_Name = !string.IsNullOrEmpty(asset.Sub_Section_Name) ? asset.Sub_Section_Name : "";
                        Record.Parent_Land_Number = !string.IsNullOrEmpty(asset.Parent_Land_Number) ? asset.Parent_Land_Number : "";
                        Record.Filial_Land_Number = !string.IsNullOrEmpty(asset.Filial_Land_Number) ? asset.Filial_Land_Number : "";
                        Record.Authorization_Number = !string.IsNullOrEmpty(asset.Authorization_Number) ? asset.Authorization_Number : "";
                        Record.Authorized_name = !string.IsNullOrEmpty(asset.Authorized_name) ? asset.Authorized_name : "";
                        Record.Area_Size = asset.Area_Size;
                        Record.Authorized_Scope_Molecule = asset.Authorized_Scope_Molecule;
                        Record.Authorized_Scope_Denomminx = asset.Authorized_Scope_Denomminx;
                        Record.Authorized_Area = asset.Authorized_Area;
                        Record.Used_Type = !string.IsNullOrEmpty(asset.Used_Type) ? asset.Used_Type : "";
                        Record.Used_Status = !string.IsNullOrEmpty(asset.Used_Status) ? asset.Used_Status : "";
                        Record.Obtained_Method = !string.IsNullOrEmpty(asset.Obtained_Method) ? asset.Obtained_Method : "";
                        Record.Used_Partition = !string.IsNullOrEmpty(asset.Used_Partition) ? asset.Used_Partition : "";
                        Record.Is_Marginal_Land = !string.IsNullOrEmpty(asset.Is_Marginal_Land) ? asset.Is_Marginal_Land : "";
                        Record.Own_Area = asset.Own_Area;
                        Record.NONOwn_Area = asset.NONOwn_Area;
                        Record.Original_Cost = asset.Original_Cost;
                        Record.Deprn_Amount = asset.Deprn_Amount;
                        Record.Current_Cost = asset.Current_Cost;
                        Record.Reval_Adjustment_Amount = asset.Reval_Adjustment_Amount;
                        Record.Reval_Land_VAT = asset.Reval_Land_VAT;
                        Record.Reval_Reserve = asset.Reval_Reserve;
                        Record.Business_Area_Size = asset.Business_Area_Size;
                        Record.Business_Book_Amount = asset.Business_Book_Amount;
                        Record.Business_Reval_Reserve = asset.Business_Reval_Reserve;
                        Record.NONBusiness_Area_Size = asset.NONBusiness_Area_Size;
                        Record.NONBusiness_Book_Amount = asset.NONBusiness_Book_Amount;
                        Record.NONBusiness_Reval_Reserve = asset.NONBusiness_Reval_Reserve;
                        Record.Year_Number = (int)asset.Year_Number;
                        Record.Announce_Amount = decimal.Round(asset.Announce_Amount, 0);
                        Record.Announce_Price = asset.Announce_Price;
                        Record.Tax_Type = !string.IsNullOrEmpty(asset.Tax_Type) ? asset.Tax_Type : "";
                        Record.Reduction_reason = !string.IsNullOrEmpty(asset.Reduction_reason) ? asset.Reduction_reason : "";
                        Record.Transfer_Price = asset.Transfer_Price;
                        Record.Reduction_Area = asset.Reduction_Area;
                        Record.Declared_Price = asset.Declared_Price;
                        Record.Dutiable_Amount = asset.Dutiable_Amount;
                        Record.Remark1 = !string.IsNullOrEmpty(asset.Remark1) ? asset.Remark1 : "";
                        Record.Remark2 = !string.IsNullOrEmpty(asset.Remark2) ? asset.Remark2 : "";
                        Record.Land_Item = !string.IsNullOrEmpty(asset.Land_Item) ? asset.Land_Item : "";
                        Record.CROP = !string.IsNullOrEmpty(asset.CROP) ? asset.CROP : "";
                        Record.Last_UpDatetimed_Time =DateTime.Now;
                        Record.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);
                        Record.Last_UpDatetimed_By_Name = asset.Last_Updated_By_Name;
                        db.Entry(Record).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        //土地成本維護
                        AS_Asset_Cost Cost = db.AS_Asset_Cost.FirstOrDefault(o => o.TRX_Header_ID == model.Header.Form_Number && o.Asset_Number == asset.Asset_Number);

                        if (Cost != null)
                        {
                            Cost.Add_Cost = asset.AddCost;
                            Cost.Less_Cost = asset.LessCost;
                            db.Entry(Cost).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }

                        //土地異動(只改分類)
                        if (!string.IsNullOrEmpty(asset.New_Asset_Category_code))
                        {
                            string changeFieldName = "Asset_Category_code";
                            AS_Assets_Land_Change AssetChange = db.AS_Assets_Land_Change.FirstOrDefault(
                                o => o.TRX_Header_ID == model.Header.Form_Number 
                                    && o.Field_Name == changeFieldName);

                            if (AssetChange != null)
                            {
                                AssetChange.After_Data = asset.New_Asset_Category_code;
                                db.Entry(AssetChange).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                AssetChange = new AS_Assets_Land_Change();
                                AssetChange.TRX_Header_ID = asset.TRXHeaderID;
                                AssetChange.AS_Assets_Land_ID = asset.Asset_Land_ID == ""?0:int.Parse(asset.Asset_Land_ID);
                                AssetChange.Field_Name = changeFieldName;
                                AssetChange.Modify_Type = "U";
                                AssetChange.Before_Data = asset.Asset_Category_code;
                                AssetChange.After_Data = asset.New_Asset_Category_code;
                                AssetChange.Create_Time = DateTime.Now;
                                AssetChange.Created_By = asset.Created_By;
                                AssetChange.Created_By_Name = asset.Created_By_Name;
                                AssetChange.Last_UpDatetimed_Time = DateTime.Now;
                                AssetChange.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);
                                AssetChange.Last_UpDatetimed_By_Name = asset.Last_Updated_By_Name;
                                db.Entry(AssetChange).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                        }

                    }
                }

                trans.Complete();
            }
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }
        public AssetHandleResult Update_MPAsset(LandMPModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (var asset in model.MPAssets)
                    {
                        //MP_His Update 
                        AS_Assets_Land_MP_Record Record = db.AS_Assets_Land_MP_Record.FirstOrDefault(m => m.ID == asset.ID);
                        //UpdateChangeData(model.Header.Form_Number, asset.Asset_Land_ID, Record, asset);
                        Record.Asset_Number = !string.IsNullOrEmpty(asset.Asset_Number) ? asset.Asset_Number : "";
                        Record.Parent_Asset_Number = !string.IsNullOrEmpty(asset.Parent_Asset_Number) ? asset.Parent_Asset_Number : "";
                        Record.Asset_Category_code = !string.IsNullOrEmpty(asset.Asset_Category_Code) ? asset.Asset_Category_Code : "";
                        Record.Old_Asset_Number = !string.IsNullOrEmpty(asset.Old_Asset_Number) ? asset.Old_Asset_Number : "";
                        if (asset.Date_Placed_In_Service != null)
                            Record.Datetime_Placed_In_Service = asset.Date_Placed_In_Service;
                        asset.Current_units = asset.Current_units;
                        Record.Deprn_Method_Code = !string.IsNullOrEmpty(asset.Deprn_Method_Code) ? asset.Deprn_Method_Code : "";
                        Record.Life_Years = asset.Life_Years;
                        Record.Life_Months = asset.Life_Months;
                        Record.Location_Disp = !string.IsNullOrEmpty(asset.Location_Disp) ? asset.Location_Disp : "";
                        Record.PO_Number = !string.IsNullOrEmpty(asset.PO_Number) ? asset.PO_Number : "";
                        Record.PO_Destination = !string.IsNullOrEmpty(asset.PO_Destination) ? asset.PO_Destination : "";
                        Record.Model_Number = !string.IsNullOrEmpty(asset.Model_Number) ? asset.Model_Number : "";
                        Record.Transaction_Date = asset.Transaction_Date;
                        Record.Assets_Unit = !string.IsNullOrEmpty(asset.Assets_Unit) ? asset.Assets_Unit : "";
                        Record.Accessory_Equipment = !string.IsNullOrEmpty(asset.Accessory_Equipment) ? asset.Accessory_Equipment : "";
                        Record.Assigned_NUM = !string.IsNullOrEmpty(asset.Assigned_NUM) ? asset.Assigned_NUM : "";
                        Record.Asset_Category_NUM = !string.IsNullOrEmpty(asset.Asset_Category_Num) ? asset.Asset_Category_Num : "";
                        Record.Description = !string.IsNullOrEmpty(asset.Description) ? asset.Description : "";
                        Record.Asset_Structure = !string.IsNullOrEmpty(asset.Asset_Structure) ? asset.Asset_Structure : "";
                        Record.Current_Cost = asset.Current_Cost;
                        Record.Deprn_Reserve = asset.Deprn_Reserve;
                        Record.Salvage_Value = asset.Salvage_Value;
                        Record.Last_UpDatetimed_Time = DateTime.Now;
                        Record.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);
                        Record.Last_UpDatetimed_By_Name = asset.Last_Updated_By_Name;
                        db.Entry(Record).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        ////土地成本維護
                        //AS_Asset_Cost Cost = db.AS_Asset_Cost.FirstOrDefault(o => o.TRX_Header_ID == model.Header.Form_Number && o.Asset_Number == asset.Asset_Number);

                        //if (Cost != null)
                        //{
                        //    Cost.Add_Cost = asset.AddCost;
                        //    Cost.Less_Cost = asset.LessCost;
                        //    db.Entry(Cost).State = System.Data.Entity.EntityState.Modified;
                        //    db.SaveChanges();
                        //}

                        ////土地異動(只改分類)
                        //if (!string.IsNullOrEmpty(asset.New_Asset_Category_code))
                        //{
                        //    string changeFieldName = "Asset_Category_code";
                        //    AS_Assets_Land_Change AssetChange = db.AS_Assets_Land_Change.FirstOrDefault(
                        //        o => o.TRX_Header_ID == model.Header.Form_Number
                        //            && o.Field_Name == changeFieldName);

                        //    if (AssetChange != null)
                        //    {
                        //        AssetChange.After_Data = asset.New_Asset_Category_code;
                        //        db.Entry(AssetChange).State = System.Data.Entity.EntityState.Modified;
                        //        db.SaveChanges();
                        //    }
                        //    else
                        //    {
                        //        AssetChange = new AS_Assets_Land_Change();
                        //        AssetChange.TRX_Header_ID = asset.TRXHeaderID;
                        //        AssetChange.AS_Assets_Land_ID = asset.Asset_Land_ID == "" ? 0 : int.Parse(asset.Asset_Land_ID);
                        //        AssetChange.Field_Name = changeFieldName;
                        //        AssetChange.Modify_Type = "U";
                        //        AssetChange.Before_Data = asset.Asset_Category_code;
                        //        AssetChange.After_Data = asset.New_Asset_Category_code;
                        //        AssetChange.Create_Time = DateTime.Now;
                        //        AssetChange.Created_By = asset.Created_By;
                        //        AssetChange.Created_By_Name = asset.Created_By_Name;
                        //        AssetChange.Last_UpDatetimed_Time = DateTime.Now;
                        //        AssetChange.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);
                        //        AssetChange.Last_UpDatetimed_By_Name = asset.Last_Updated_By_Name;
                        //        db.Entry(AssetChange).State = System.Data.Entity.EntityState.Added;
                        //        db.SaveChanges();
                        //    }
                        //}

                    }
                }

                trans.Complete();
            }
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }
        public AssetHandleResult Update_AssetChangClass(LandModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (var asset in model.Assets)
                    {
                        AS_Assets_Land_Record Record = db.AS_Assets_Land_Record.FirstOrDefault(m => m.ID == asset.ID);
                        Record.Last_UpDatetimed_Time = DateTime.Now;
                        Record.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);
                        Record.Last_UpDatetimed_By_Name = asset.Last_Updated_By_Name;
                        db.Entry(Record).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        //土地異動(只改分類)
                        if (!string.IsNullOrEmpty(asset.New_Asset_Category_code))
                        {
                            string changeFieldName = "Asset_Category_code";
                            AS_Assets_Land_Change AssetChange = db.AS_Assets_Land_Change.FirstOrDefault(
                                o => o.TRX_Header_ID == model.Header.Form_Number
                                    && o.Field_Name == changeFieldName);

                            if (AssetChange != null)
                            {
                                AssetChange.After_Data = asset.New_Asset_Category_code;
                                db.Entry(AssetChange).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                AssetChange = new AS_Assets_Land_Change();
                                AssetChange.TRX_Header_ID = asset.TRXHeaderID;
                                AssetChange.AS_Assets_Land_ID = string.IsNullOrEmpty(asset.Asset_Land_ID) ? 0 : int.Parse(asset.Asset_Land_ID);
                                AssetChange.Field_Name = changeFieldName;
                                AssetChange.Modify_Type = "U";
                                AssetChange.Before_Data = asset.Asset_Category_code;
                                AssetChange.After_Data = asset.New_Asset_Category_code;
                                AssetChange.Create_Time = DateTime.Now;
                                AssetChange.Created_By = asset.Created_By;
                                AssetChange.Created_By_Name = asset.Created_By_Name;
                                AssetChange.Last_UpDatetimed_Time = DateTime.Now;
                                AssetChange.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);
                                AssetChange.Last_UpDatetimed_By_Name = asset.Last_Updated_By_Name;
                                db.Entry(AssetChange).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                        }

                    }
                }

                trans.Complete();
            }
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }
        public AssetHandleResult Update_AssetMPChangClass(LandMPModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (var asset in model.MPAssets)
                    {
                        AS_Assets_Land_MP_Record Record = db.AS_Assets_Land_MP_Record.FirstOrDefault(m => m.ID == asset.ID);
                        Record.Last_UpDatetimed_Time = DateTime.Now;
                        Record.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);
                        Record.Last_UpDatetimed_By_Name = asset.Last_Updated_By_Name;
                        db.Entry(Record).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        //土地異動(只改分類)
                        if (!string.IsNullOrEmpty(asset.New_Asset_Category_code))
                        {
                            string changeFieldName = "Asset_Category_code";
                            AS_Assets_Land_MP_Change_Record AssetChange = db.AS_Assets_Land_MP_Change_Record.FirstOrDefault(
                                o => o.TRX_Header_ID == model.Header.Form_Number
                                    && o.Field_Name == changeFieldName);

                            if (AssetChange != null)
                            {
                                AssetChange.After_Data = asset.New_Asset_Category_code;
                                db.Entry(AssetChange).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                AssetChange = new AS_Assets_Land_MP_Change_Record();
                                AssetChange.TRX_Header_ID = asset.TRX_Header_ID;
                                AssetChange.AS_Assets_Land_MP_Record_ID = AssetChange.ID;
                                AssetChange.Field_Name = changeFieldName;
                                AssetChange.Modify_Type = "U";
                                AssetChange.Before_Data = asset.Asset_Category_Code;
                                AssetChange.After_Data = asset.New_Asset_Category_code;
                                AssetChange.Create_Time = DateTime.Now;
                                AssetChange.Created_By = asset.Created_By;
                                AssetChange.Created_By_Name = asset.Created_By_Name;
                                AssetChange.Last_UpDatetimed_Time = DateTime.Now;
                                AssetChange.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);
                                AssetChange.Last_UpDatetimed_By_Name = asset.Last_Updated_By_Name;
                                db.Entry(AssetChange).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                            }
                        }

                    }
                }

                trans.Complete();
            }
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }
        public AssetHandleResult Update_AssetMPLifeChang(LandMPModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (var asset in model.MPAssets)
                    {
                        AS_Assets_Land_MP_Record Record = db.AS_Assets_Land_MP_Record.FirstOrDefault(m => m.ID == asset.ID);
                        Record.Last_UpDatetimed_Time = DateTime.Now;
                        Record.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);
                        Record.Last_UpDatetimed_By_Name = asset.Last_Updated_By_Name;
                        db.Entry(Record).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        string changeFieldName1 = "Life_Years", changeFieldName2 = "Life_Months";
                        AS_Assets_Land_MP_Change_Record AssetChange1 = db.AS_Assets_Land_MP_Change_Record.FirstOrDefault(
                            o => o.TRX_Header_ID == model.Header.Form_Number
                                && o.Field_Name == changeFieldName1);
                        AS_Assets_Land_MP_Change_Record AssetChange2 = db.AS_Assets_Land_MP_Change_Record.FirstOrDefault(
                           o => o.TRX_Header_ID == model.Header.Form_Number
                               && o.Field_Name == changeFieldName2);
                        if (AssetChange1 != null)
                        {
                            AssetChange1.After_Data = asset.New_Life_Years.ToString();
                            db.Entry(AssetChange1).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            AS_Assets_Land_MP_Change_Record newAssetChange = new AS_Assets_Land_MP_Change_Record();
                            newAssetChange.ID = 0;
                            newAssetChange.TRX_Header_ID = model.Header.Form_Number;
                            newAssetChange.AS_Assets_Land_MP_Record_ID = asset.ID;
                            newAssetChange.UPD_File = "";
                            newAssetChange.Field_Name = changeFieldName1;
                            newAssetChange.Modify_Type = "U";
                            newAssetChange.Before_Data = asset.Life_Years.ToString();
                            newAssetChange.After_Data = asset.New_Life_Years.ToString();
                            newAssetChange.Create_Time = DateTime.Now;
                            newAssetChange.Created_By = asset.Created_By;
                            newAssetChange.Created_By_Name = asset.Created_By_Name;
                            newAssetChange.Last_UpDatetimed_Time = DateTime.Now;
                            newAssetChange.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);
                            newAssetChange.Last_UpDatetimed_By_Name = asset.Last_Updated_By_Name;
                            db.Entry(newAssetChange).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }
                        if (AssetChange2 != null)
                        {
                            AssetChange2.After_Data = asset.New_Life_Months.ToString();
                            db.Entry(AssetChange1).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            AS_Assets_Land_MP_Change_Record newAssetChange = new AS_Assets_Land_MP_Change_Record();
                            newAssetChange.ID = 0;
                            newAssetChange.TRX_Header_ID = model.Header.Form_Number;
                            newAssetChange.AS_Assets_Land_MP_Record_ID = asset.ID;
                            newAssetChange.UPD_File = "";
                            newAssetChange.Field_Name = changeFieldName2;
                            newAssetChange.Modify_Type = "U";
                            newAssetChange.Before_Data = asset.Life_Months.ToString();
                            newAssetChange.After_Data = asset.New_Life_Months.ToString();
                            newAssetChange.Create_Time = DateTime.Now;
                            newAssetChange.Created_By = asset.Created_By;
                            newAssetChange.Created_By_Name = asset.Created_By_Name;
                            newAssetChange.Last_UpDatetimed_Time = DateTime.Now;
                            newAssetChange.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);
                            newAssetChange.Last_UpDatetimed_By_Name = asset.Last_Updated_By_Name;
                            db.Entry(newAssetChange).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                        }
                    }
                }

                trans.Complete();
            }
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }

        public AssetHandleResult Delete_Asset(LandModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (var asset in model.Assets)
                    {
                        //MP_His Update 
                        AS_Assets_Land_Record Record = db.AS_Assets_Land_Record.FirstOrDefault(m => m.ID == asset.ID);

                        db.Entry(Record).State = System.Data.Entity.EntityState.Deleted;
                        db.SaveChanges();

                        ////土地成本維護
                        //AS_Asset_Cost Cost = db.AS_Asset_Cost.FirstOrDefault(o => o.TRX_Header_ID == model.Header.Form_Number && o.Asset_Number == asset.Asset_Number);

                        //if (Cost != null)
                        //{
                        //    Cost.Add_Cost = asset.AddCost;
                        //    Cost.Less_Cost = asset.LessCost;
                        //    db.Entry(Cost).State = System.Data.Entity.EntityState.Modified;
                        //    db.SaveChanges();
                        //}
                        if (model.Header.Transaction_Type == "B240")// 土地異動分類
                        {
                            var changeList = db.AS_Assets_Land_Change.Where(p => p.TRX_Header_ID == model.Header.Form_Number);
                            if (changeList!= null && changeList.Count() > 0)
                            {
                                foreach(AS_Assets_Land_Change dbChange in changeList)
                                {
                                    db.Entry(dbChange).State = System.Data.Entity.EntityState.Deleted;
                                    db.SaveChanges();
                                }
                            }
                        }

                    }
                    if (model.Header.Transaction_Type == "B220")// 土地合併
                    {
                        int dbCount = db.AS_Assets_Land_Record.Count(p => p.TRX_Header_ID == model.Header.Form_Number);
                        if (dbCount <= 0)
                        {
                            AS_Assets_Land_ML Record = db.AS_Assets_Land_ML.FirstOrDefault(m => m.TRX_Header_ID == model.Header.Form_Number);

                            db.Entry(Record).State = System.Data.Entity.EntityState.Deleted;
                            db.SaveChanges();
                        }
                    }
                }

                trans.Complete();
            }
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }
        public AssetHandleResult Delete_MPAsset(LandMPModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (var asset in model.MPAssets)
                    {
                        //MP_His Update 
                        AS_Assets_Land_MP_Record Record = db.AS_Assets_Land_MP_Record.FirstOrDefault(m => m.ID == asset.ID);

                        db.Entry(Record).State = System.Data.Entity.EntityState.Deleted;
                        db.SaveChanges();

                        if (model.Header.Transaction_Type == "B340")// 土地異動分類
                        {
                            var changeList = db.AS_Assets_Land_MP_Change_Record.Where(p => p.TRX_Header_ID == model.Header.Form_Number);
                            if (changeList != null && changeList.Count() > 0)
                            {
                                foreach (AS_Assets_Land_MP_Change_Record dbChange in changeList)
                                {
                                    db.Entry(dbChange).State = System.Data.Entity.EntityState.Deleted;
                                    db.SaveChanges();
                                }
                            }
                        }
                        if (model.Header.Transaction_Type == "B350")// 資產改良使用年限異動
                        {
                            var changeList = db.AS_Assets_Land_MP_Change_Record.Where(p => p.TRX_Header_ID == model.Header.Form_Number);
                            if (changeList != null && changeList.Count() > 0)
                            {
                                foreach (AS_Assets_Land_MP_Change_Record dbChange in changeList)
                                {
                                    db.Entry(dbChange).State = System.Data.Entity.EntityState.Deleted;
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                }
                trans.Complete();
            }
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }
        public AssetHandleResult Delete_AssetRetire(LandModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (var asset in model.RetireAssets)
                    {
                        //MP_His Update 
                        AS_Assets_Land_Retire Record = db.AS_Assets_Land_Retire.FirstOrDefault(m => m.ID == asset.ID);

                        db.Entry(Record).State = System.Data.Entity.EntityState.Deleted;
                        db.SaveChanges();
                    }
                }

                trans.Complete();
            }
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }
        public AssetHandleResult Delete_MPAssetRetire(LandMPModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (var asset in model.RetireAssets)
                    {
                        //MP_His Update 
                        AS_Assets_Land_MP_Retire Record = db.AS_Assets_Land_MP_Retire.FirstOrDefault(m => m.ID == asset.ID);

                        db.Entry(Record).State = System.Data.Entity.EntityState.Deleted;
                        db.SaveChanges();
                    }
                }

                trans.Complete();
            }
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }
        public AssetHandleResult Update_SplitAsset(LandModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (var asset in model.Assets)
                    {
                        //LandSplit Update 
                        AS_Assets_Land_Split Record = db.AS_Assets_Land_Split.FirstOrDefault(m => m.ID == asset.ID);

                        Record.Current_units = asset.Current_units;
                        Record.Datetime_Placed_In_Service = asset.Datetime_Placed_In_Service;
                        Record.Old_Asset_Number = !string.IsNullOrEmpty(asset.Old_Asset_Number) ? asset.Old_Asset_Number : "";
                        Record.Asset_Category_code = !string.IsNullOrEmpty(asset.Asset_Category_code) ? asset.Asset_Category_code : "";
                        Record.Location_Disp = !string.IsNullOrEmpty(asset.Location_Disp) ? asset.Location_Disp : "";
                        Record.Description = !string.IsNullOrEmpty(asset.Description) ? asset.Description : "";
                        Record.City_Code = !string.IsNullOrEmpty(asset.City_Code) ? asset.City_Code : "";
                        Record.City_Name = !string.IsNullOrEmpty(asset.City_Name) ? asset.City_Name : "";
                        Record.District_Code = !string.IsNullOrEmpty(asset.District_Code) ? asset.District_Code : "";
                        Record.District_Name = !string.IsNullOrEmpty(asset.District_Name) ? asset.District_Name : "";
                        Record.Section_Code = !string.IsNullOrEmpty(asset.Section_Code) ? asset.Section_Code : "";
                        Record.Section_Name = !string.IsNullOrEmpty(asset.Section_Name) ? asset.Section_Name : "";
                        Record.Sub_Section_Name = !string.IsNullOrEmpty(asset.Sub_Section_Name) ? asset.Sub_Section_Name : "";
                        Record.Parent_Land_Number = !string.IsNullOrEmpty(asset.Parent_Land_Number) ? asset.Parent_Land_Number : "";
                        Record.Filial_Land_Number = !string.IsNullOrEmpty(asset.Filial_Land_Number) ? asset.Filial_Land_Number : "";
                        Record.Authorization_Number = !string.IsNullOrEmpty(asset.Authorization_Number) ? asset.Authorization_Number : "";
                        Record.Authorized_name = !string.IsNullOrEmpty(asset.Authorized_name) ? asset.Authorized_name : "";
                        Record.Area_Size = asset.Area_Size;
                        Record.Authorized_Scope_Molecule = asset.Authorized_Scope_Molecule;
                        Record.Authorized_Scope_Denomminx = asset.Authorized_Scope_Denomminx;
                        Record.Authorized_Area = asset.Authorized_Area;
                        Record.Used_Type = !string.IsNullOrEmpty(asset.Used_Type) ? asset.Used_Type : "";
                        Record.Used_Status = !string.IsNullOrEmpty(asset.Used_Status) ? asset.Used_Status : "";
                        Record.Obtained_Method = !string.IsNullOrEmpty(asset.Obtained_Method) ? asset.Obtained_Method : "";
                        Record.Used_Partition = !string.IsNullOrEmpty(asset.Used_Partition) ? asset.Used_Partition : "";
                        Record.Is_Marginal_Land = !string.IsNullOrEmpty(asset.Is_Marginal_Land) ? asset.Is_Marginal_Land : "";
                        Record.Own_Area = asset.Own_Area;
                        Record.NONOwn_Area = asset.NONOwn_Area;
                        Record.Original_Cost = asset.Original_Cost;
                        Record.Deprn_Amount = asset.Deprn_Amount;
                        Record.Current_Cost = asset.Current_Cost;
                        Record.Reval_Adjustment_Amount = asset.Reval_Adjustment_Amount;
                        Record.Reval_Land_VAT = asset.Reval_Land_VAT;
                        Record.Reval_Reserve = asset.Reval_Reserve;
                        Record.Business_Area_Size = asset.Business_Area_Size;
                        Record.Business_Book_Amount = asset.Business_Book_Amount;
                        Record.Business_Reval_Reserve = asset.Business_Reval_Reserve;
                        Record.NONBusiness_Area_Size = asset.NONBusiness_Area_Size;
                        Record.NONBusiness_Book_Amount = asset.NONBusiness_Book_Amount;
                        Record.NONBusiness_Reval_Reserve = asset.NONBusiness_Reval_Reserve;
                        Record.Year_Number = (int)asset.Year_Number;
                        Record.Announce_Amount = asset.Announce_Amount;
                        Record.Announce_Price = asset.Announce_Price;
                        Record.Tax_Type = !string.IsNullOrEmpty(asset.Tax_Type) ? asset.Tax_Type : "";
                        Record.Reduction_reason = !string.IsNullOrEmpty(asset.Reduction_reason) ? asset.Reduction_reason : "";
                        Record.Reduction_Area = asset.Reduction_Area;
                        Record.Declared_Price = asset.Declared_Price;
                        Record.Dutiable_Amount = asset.Dutiable_Amount;
                        Record.Remark1 = !string.IsNullOrEmpty(asset.Remark1) ? asset.Remark1 : "";
                        Record.Remark2 = !string.IsNullOrEmpty(asset.Remark2) ? asset.Remark2 : "";
                        Record.Last_UpDatetimed_Time = asset.Last_Updated_Time;

                        Record.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);

                        db.Entry(Record).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                trans.Complete();
            }
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }

        public AssetHandleResult Update_MergeAsset(LandModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (var asset in model.Assets)
                    {
                        //LandMerge Update 
                        AS_Assets_Land_ML Record = db.AS_Assets_Land_ML.FirstOrDefault(m => m.ID == asset.ID);

                        Record.Current_units = asset.Current_units;
                        Record.Datetime_Placed_In_Service = asset.Datetime_Placed_In_Service;
                        Record.Old_Asset_Number = asset.Old_Asset_Number;
                        Record.Asset_Category_code = asset.Asset_Category_code;
                        Record.Location_Disp = asset.Location_Disp;
                        Record.Description = asset.Description;
                        Record.City_Code = asset.City_Code;
                        Record.District_Code = asset.District_Code;
                        Record.Section_Code = asset.Section_Code;
                        Record.Sub_Section_Name = asset.Sub_Section_Name;
                        Record.Parent_Land_Number = asset.Parent_Land_Number;
                        Record.Filial_Land_Number = asset.Filial_Land_Number;
                        Record.Authorization_Number = asset.Authorization_Number;
                        Record.Authorized_name = asset.Authorized_name;
                        Record.Area_Size = asset.Area_Size;
                        Record.Authorized_Scope_Molecule = asset.Authorized_Scope_Molecule;
                        Record.Authorized_Scope_Denomminx = asset.Authorized_Scope_Denomminx;
                        Record.Authorized_Area = asset.Authorized_Area;
                        Record.Used_Type = asset.Used_Type;
                        Record.Used_Status = asset.Used_Status;
                        Record.Obtained_Method = asset.Obtained_Method;
                        Record.Used_Partition = asset.Used_Partition;
                        Record.Is_Marginal_Land = asset.Is_Marginal_Land;
                        Record.Own_Area = asset.Own_Area;
                        Record.NONOwn_Area = asset.NONOwn_Area;
                        Record.Original_Cost = asset.Original_Cost;
                        Record.Deprn_Amount = asset.Deprn_Amount;
                        Record.Current_Cost = asset.Current_Cost;
                        Record.Reval_Adjustment_Amount = asset.Reval_Adjustment_Amount;
                        Record.Reval_Land_VAT = asset.Reval_Land_VAT;
                        Record.Reval_Reserve = asset.Reval_Reserve;
                        Record.Business_Area_Size = asset.Business_Area_Size;
                        Record.Business_Book_Amount = asset.Business_Book_Amount;
                        Record.Business_Reval_Reserve = asset.Business_Reval_Reserve;
                        Record.NONBusiness_Area_Size = asset.NONBusiness_Area_Size;
                        Record.NONBusiness_Book_Amount = asset.NONBusiness_Book_Amount;
                        Record.NONBusiness_Reval_Reserve = asset.NONBusiness_Reval_Reserve;
                        Record.Year_Number = (int)asset.Year_Number;
                        Record.Announce_Amount = asset.Announce_Amount;
                        Record.Announce_Price = asset.Announce_Price;
                        Record.Tax_Type = asset.Tax_Type;
                        Record.Reduction_reason = asset.Reduction_reason;
                        Record.Reduction_Area = asset.Reduction_Area;
                        Record.Declared_Price = asset.Declared_Price;
                        Record.Dutiable_Amount = asset.Dutiable_Amount;
                        Record.Remark1 = asset.Remark1;
                        Record.Remark2 = asset.Remark2;
                        Record.Last_UpDatetimed_Time = asset.Last_Updated_Time;
                        Record.Last_UpDatetimed_By = asset.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(asset.Last_Updated_By);

                        db.Entry(Record).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                trans.Complete();
            }
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }

        public AssetHandleResult Update_RetireAsset(LandModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (var asset in model.RetireAssets)
                    {
                        //LandSplit Update 
                        AS_Assets_Land_Retire Record = db.AS_Assets_Land_Retire.FirstOrDefault(m => m.ID == asset.ID);
                        Record.Old_Cost = asset.Old_Cost;
                        Record.Retire_Cost = asset.Retire_Cost;
                        Record.Sell_Amount = asset.Sell_Amount;
                        Record.Sell_Cost = asset.Sell_Cost;
                        Record.New_Cost = asset.New_Cost;
                        Record.Reval_Adjustment_Amount = asset.Reval_Adjustment_Amount;
                        Record.Reval_Reserve = asset.Reval_Reserve;
                        Record.Reval_Land_VAT = asset.Reval_Land_VAT;
                        Record.Reason_Code = asset.Reason_Code;
                        Record.Last_UpDatetimed_Time = asset.Last_UpDatetimed_Time;
                        Record.Last_UpDatetimed_By = asset.Last_UpDatetimed_By == "" ? (decimal?)null : decimal.Parse(asset.Last_UpDatetimed_By);
                        Record.Last_UpDatetimed_By_Name = string.IsNullOrEmpty(asset.Last_UpDatetimed_By_Name)? "":asset.Last_UpDatetimed_By_Name;
                        db.Entry(Record).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                trans.Complete();
            }
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }
        public AssetHandleResult Update_MPAssetRetire(LandMPModel model)
        {
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (var asset in model.RetireAssets)
                    {
                        //LandSplit Update 
                        AS_Assets_Land_MP_Retire Record = db.AS_Assets_Land_MP_Retire.FirstOrDefault(m => m.ID == asset.ID);
                        Record.Old_Cost = asset.Old_Cost;
                        Record.Retire_Cost = asset.Retire_Cost;
                        Record.Sell_Amount = asset.Sell_Amount;
                        Record.Sell_Cost = asset.Sell_Cost;
                        Record.New_Cost = asset.New_Cost;
                        Record.Reval_Adjustment_Amount = asset.Reval_Adjustment_Amount;
                        Record.Reval_Reserve = asset.Reval_Reserve;
                        Record.Reval_Land_VAT = asset.Reval_Land_VAT;
                        Record.Reason_Code = asset.Reason_Code;
                        Record.Last_UpDatetimed_Time = asset.Last_UpDatetimed_Time;
                        Record.Last_UpDatetimed_By = asset.Last_UpDatetimed_By == "" ? (decimal?)null : decimal.Parse(asset.Last_UpDatetimed_By);
                        Record.Last_UpDatetimed_By_Name = string.IsNullOrEmpty(asset.Last_UpDatetimed_By_Name)? "":asset.Last_UpDatetimed_By_Name;
                        db.Entry(Record).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                trans.Complete();
            }
            return new AssetHandleResult() { TRX_Header_ID = model.Header.Form_Number };
        }
        public bool UpdateFlowStatus(string TRX_Header_ID, string FlowStatus)
        {
            bool Result = true;

            using (var db = new AS_LandBankEntities())
            {
                AS_TRX_Headers Header = db.AS_TRX_Headers.First(o => o.Form_Number == TRX_Header_ID);
                Header.Transaction_Status = FlowStatus;
                Header.Last_UpDatetimed_Time = DateTime.Now;
                db.Entry(Header).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }

            return Result;
        }

        public string CreateAssetNumber(string type1, string type2)
        {
            string AssetNumber = "";

            using (var db = new AS_LandBankEntities())
            {
                AssetNumber = (from land in db.AS_Assets_Land select new { Asset_Number = land.Asset_Number }).Concat(from landr in db.AS_Assets_Land_Record select new { Asset_Number = landr.Asset_Number })
                              .Where(i => i.Asset_Number.StartsWith(type1 + type2)).Max(i => i.Asset_Number);
            }
            if (AssetNumber != null)
            {
                //30000310000001
                int Seq = Convert.ToInt32(AssetNumber.Substring(8)) + 1;
                AssetNumber = type1 + type2 + Seq.ToString().PadLeft(7, '0');
            }
            else
            {
                AssetNumber = type1 + type2 + "0000001";
            }

            return AssetNumber;
        }

        public string CreateMPAssetNumber(string type1, string type2)
        {
            string AssetNumber = "";

            using (var db = new AS_LandBankEntities())
            {
                AssetNumber = (from land in db.AS_Assets_Land_MP select new { Asset_Number = land.Asset_Number }).Concat(from landr in db.AS_Assets_Land_MP select new { Asset_Number = landr.Asset_Number })
                              .Where(i => i.Asset_Number.StartsWith(type1 + type2)).Max(i => i.Asset_Number);
            }
            if (AssetNumber != null)
            {
                //30000310000001
                int Seq = Convert.ToInt32(AssetNumber.Substring(8)) + 1;
                AssetNumber = type1 + type2 + Seq.ToString().PadLeft(7, '0');
            }
            else
            {
                AssetNumber = type1 + type2 + "0000001";
            }

            return AssetNumber;
        }
        public IEnumerable<Library.Servant.Servant.Land.LandModels.CodeItem> GetDistrict(string City)
        {
            IEnumerable<Library.Servant.Servant.Land.LandModels.CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                result = (from m in db.AS_Land_Code
                          where m.City_Code == City
                          select new Library.Servant.Servant.Land.LandModels.CodeItem()
                          {
                              Value = m.District_Code,
                              Text = m.District_Name
                          }).Distinct().ToList();
            }
            return result;
        }

        public IEnumerable<Library.Servant.Servant.Land.LandModels.CodeItem> GetSection(string City, string District)
        {
            IEnumerable<Library.Servant.Servant.Land.LandModels.CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                result = (from m in db.AS_Land_Code
                          where m.City_Code == City && m.District_Code == District
                          select new Library.Servant.Servant.Land.LandModels.CodeItem()
                          {
                              Value = m.Section_Code,
                              Text = m.Sectioni_Name
                          }).Distinct().ToList();
            }
            return result;
        }

        public IEnumerable<Library.Servant.Servant.Land.LandModels.CodeItem> GetSubSection(string City, string District, string Section)
        {
            IEnumerable<Library.Servant.Servant.Land.LandModels.CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                result = (from m in db.AS_Land_Code
                          where m.City_Code == City && m.District_Code == District && m.Section_Code == Section
                          select new Library.Servant.Servant.Land.LandModels.CodeItem()
                          {
                              Value = m.Sub_Sectioni_Name,
                              Text = m.Sub_Sectioni_Name
                          }).Distinct().ToList();
            }
            return result;
        }
        public IEnumerable<CodeItem> GetAssetMain_Kind(string Type)
        {
            IEnumerable<CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                if (Type == "MP")
                {
                    result = (from m in db.AS_Asset_Category_Main_Kinds
                                  //先加在這邊
                              where m.No != "1" && m.No != "2"
                              select new CodeItem()
                              {
                                  Value = m.No,
                                  Text = m.Name
                              }).ToList();
                }
                else if (Type == "Land")
                {
                    result = (from m in db.AS_Asset_Category_Main_Kinds
                                  //先加在這邊
                              where m.No == "1"
                              select new CodeItem()
                              {
                                  Value = m.No,
                                  Text = m.Name
                              }).ToList();
                }
                else if (Type == "LandMP")
                {
                    result = (from m in db.AS_Asset_Category_Main_Kinds
                                  //先加在這邊
                              where m.No == "9"
                              select new CodeItem()
                              {
                                  Value = m.No.ToString(),
                                  Text = m.Name
                              }).ToList();
                }
                else if (Type == "Build")
                {
                    result = (from m in db.AS_Asset_Category_Main_Kinds
                                  //先加在這邊
                              where m.No == "2"
                              select new CodeItem()
                              {
                                  Value = m.No,
                                  Text = m.Name
                              }).ToList();
                }
                else
                {
                    result = (from m in db.AS_Asset_Category_Main_Kinds
                                  //先加在這邊
                              select new CodeItem()
                              {
                                  Value = m.No,
                                  Text = m.Name
                              }).ToList();
                }

            }
            return result;
        }

        public IEnumerable<CodeItem> GetAssetDetail_Kind(string MainKind)
        {
            IEnumerable<CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                int MainID = Convert.ToInt32(MainKind);
                result = (from m in db.AS_Asset_Category_Detail_Kinds
                          where db.AS_Asset_Category_Main_Kinds.FirstOrDefault( p=> p.No == MainKind).ID == m.Main_Kind_ID 
                          select new CodeItem()
                          {
                              Value = m.No,
                              Text = m.Name
                          }).ToList();
            }
            return result;
        }

        public IEnumerable<CodeItem> GetAssetUse_Types()
        {
            IEnumerable<CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                result = (from m in db.AS_Asset_Category_Use_Types
                          select new CodeItem()
                          {
                              Value = m.No,
                              Text = m.Name
                          }).ToList();
            }
            return result;
        }
        public IEnumerable<CodeItem> GetCodeItem(string CodeClass)
        {
            IEnumerable<CodeItem> result = null;
            using (var db = new AS_LandBankEntities())
            {
                result = (from m in db.AS_VW_Code_Table
                          where m.Class == CodeClass
                          select new CodeItem()
                          {
                              Value = m.Code_ID,
                              Text = m.Text
                          }).ToList().OrderBy(o => o.Value);
            }
            return result;
        }


    }
}
