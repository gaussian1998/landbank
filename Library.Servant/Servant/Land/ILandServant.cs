﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Land.LandModels;

namespace Library.Servant.Servant.Land
{
    public interface ILandServant
    {
        IndexModel Index(SearchModel condition);

        LandModel GetDetail(string TRXHeaderID);

        LandMPModel GetMPDetail(string TRXHeaderID);

        LandModel GetRetireDetail(string TRXHeaderID);


        AssetHandleResult Create_Asset(LandModel model);

        AssetHandleResult Create_MPAssetRetire(LandMPModel model);

        AssetHandleResult Create_MPAssetChange(LandMPModel model);

        AssetHandleResult Create_MPAssetLefeChange(LandMPModel model);

        AssetHandleResult Create_MPAsset(LandMPModel model);

        AssetHandleResult CreateToLand(string TRXHeaderID);

        AssetHandleResult CreateLand(LandModel Land);

        AssetHandleResult CreateLandMP(LandMPModel Land);

        AssetHandleResult Update_Asset(LandModel model);

        AssetHandleResult Update_MPAsset(LandMPModel model);

        AssetHandleResult Update_MPAssetRetire(LandMPModel model);

        AssetHandleResult Delete_Asset(LandModel model);

        AssetHandleResult Delete_MPAsset(LandMPModel model);

        AssetHandleResult Delete_MPAssetRetire(LandMPModel model);

        AssetHandleResult Delete_AssetRetire(LandModel model);

        AssetHandleResult Update_SplitAsset(LandModel model);

        AssetHandleResult Update_RetireAsset(LandModel model);

        AssetHandleResult Update_MergeAsset(LandModel model);

        AssetHandleResult Update_AssetChangClass(LandModel model);

        AssetHandleResult Update_AssetMPChangClass(LandMPModel model);

        AssetHandleResult Update_AssetMPLifeChang(LandMPModel model);

        AssetHandleResult UpdateToLand(string TRXHeaderID);

        AssetLandModel GetAssetLandDetail(int ID, string TableName);

        AssetLandMPModel GetAssetLandMPDetail(int ID, string TableName);

        AssetLandMPRetireModel GetAssetLandMPRetireDetail(int ID);

        AssetLandRetireModel GetAssetLandRetireDetail(int ID);

        AssetLandModel GetAssetFormLandByAssetNumber(string AssetNumber);

        LandHeaderModel GetHeader(string AssetNumber, string Type);

        LandHeaderModel GetHeaderData(string TRXHeaderID);

        AssetsIndexModel SearchAssets(AssetsSearchModel condition);

        AssetsMPIndexModel SearchAssetsMP(AssetsMPSearchModel condition);

        IEnumerable<CodeItem> SearchAssetsByCategoryCode(string CategoryCode);

        string CreateTRXID(string Type);
        string CreateAssetNumber(string type1, string type2);
        string CreateMPAssetNumber(string type1, string type2);
        AssetHandleResult HandleHeader(LandHeaderModel Header);

        bool GetDocID(string DocCode, out int DocID);

        bool UpdateFlowStatus(string TRX_Header_ID, string FlowStatus);

        IEnumerable<Library.Servant.Servant.Land.LandModels.CodeItem> GetDistrict(string City);
        IEnumerable<Library.Servant.Servant.Land.LandModels.CodeItem> GetSection(string City,string District);
        IEnumerable<Library.Servant.Servant.Land.LandModels.CodeItem> GetSubSection(string City, string District ,string Section);
        IEnumerable<CodeItem> GetAssetMain_Kind(string Type);
        IEnumerable<CodeItem> GetAssetDetail_Kind(string MainKind);
        IEnumerable<CodeItem> GetAssetUse_Types();
        IEnumerable<CodeItem> GetCodeItem(string CodeClass);
    }
}
