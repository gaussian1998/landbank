﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Land.LandModels;
using Library.Servant.Common;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.Land
{
    public class RemoteLandServant : ILandServant
    {
        public IndexModel Index(SearchModel condition)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, IndexModel>
            (
                condition,
                Config.ServantDomain + "/LandServant/Index"
            );

            return Tools.ExceptionConvert(result);
        }
        public LandModel GetDetail(string TRXHeaderID)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, LandModel>
          (
              new SearchModel() { TRXHeaderID = TRXHeaderID },
              Config.ServantDomain + "/LandServant/GetDetail"
          );

            return Tools.ExceptionConvert(result);
        }
        public LandMPModel GetMPDetail(string TRXHeaderID)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, LandMPModel>
          (
              new SearchModel() { TRXHeaderID = TRXHeaderID },
              Config.ServantDomain + "/LandServant/GetMPDetail"
          );

            return Tools.ExceptionConvert(result);
        }
        public LandModel GetRetireDetail(string TRXHeaderID)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, LandModel>
          (
              new SearchModel() { TRXHeaderID = TRXHeaderID },
              Config.ServantDomain + "/LandServant/GetRetireDetail"
          );

            return Tools.ExceptionConvert(result);
        }
        public LandModel GetAssetLandChangeDetail(string TRXHeaderID)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, LandModel>
          (
              new SearchModel() { TRXHeaderID = TRXHeaderID },
              Config.ServantDomain + "/LandServant/GetAssetLandChangeDetail"
          );

            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Create_Asset(LandModel model)
        {
            var result = ProtocolService.EncryptionPost<LandModel, AssetHandleResult>
            (
                model,
                Config.ServantDomain + "/LandServant/Create_Asset"
            );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Create_MPAssetChange(LandMPModel model)
        {
            var result = ProtocolService.EncryptionPost<LandMPModel, AssetHandleResult>
            (
                model,
                Config.ServantDomain + "/LandServant/Create_MPAssetChange"
            );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Create_MPAssetLefeChange(LandMPModel model)
        {
            var result = ProtocolService.EncryptionPost<LandMPModel, AssetHandleResult>
            (
                model,
                Config.ServantDomain + "/LandServant/Create_MPAssetLefeChange"
            );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Create_MPAssetRetire(LandMPModel model)
        {
            var result = ProtocolService.EncryptionPost<LandMPModel, AssetHandleResult>
            (
                model,
                Config.ServantDomain + "/LandServant/Create_MPAssetRetire"
            );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Create_MPAsset(LandMPModel model)
        {
            var result = ProtocolService.EncryptionPost<LandMPModel, AssetHandleResult>
            (
                model,
                Config.ServantDomain + "/LandServant/Create_MPAsset"
            );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult CreateToLand(string TRXHeaderID)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, AssetHandleResult>
            (
                 new SearchModel() { TRXHeaderID = TRXHeaderID },
                 Config.ServantDomain + "/LandServant/CreateToLand"
            );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult CreateLand(LandModel Land)
        {
            var result = ProtocolService.EncryptionPost<LandModel, AssetHandleResult>
             (
                  Land,
                  Config.ServantDomain + "/LandServant/CreateLand"
             );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult CreateLandMP(LandMPModel Land)
        {
            var result = ProtocolService.EncryptionPost<LandMPModel, AssetHandleResult>
             (
                  Land,
                  Config.ServantDomain + "/LandServant/CreateLandMP"
             );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Update_Asset(LandModel model)
        {
            var result = ProtocolService.EncryptionPost<LandModel, AssetHandleResult>
           (
               model,
               Config.ServantDomain + "/LandServant/Update_Asset"
           );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Update_MPAsset(LandMPModel model)
        {
            var result = ProtocolService.EncryptionPost<LandMPModel, AssetHandleResult>
           (
               model,
               Config.ServantDomain + "/LandServant/Update_MPAsset"
           );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Update_AssetMPLifeChang(LandMPModel model)
        {
            var result = ProtocolService.EncryptionPost<LandMPModel, AssetHandleResult>
           (
               model,
               Config.ServantDomain + "/LandServant/Update_AssetMPLifeChang"
           );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Delete_Asset(LandModel model)
        {
            var result = ProtocolService.EncryptionPost<LandModel, AssetHandleResult>
            (
                model,
                Config.ServantDomain + "/LandServant/Delete_Asset"
            );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Delete_MPAsset(LandMPModel model)
        {
            var result = ProtocolService.EncryptionPost<LandMPModel, AssetHandleResult>
            (
                model,
                Config.ServantDomain + "/LandServant/Delete_MPAsset"
            );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Delete_AssetRetire(LandModel model)
        {
            var result = ProtocolService.EncryptionPost<LandModel, AssetHandleResult>
            (
                model,
                Config.ServantDomain + "/LandServant/Delete_AssetRetire"
            );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Delete_MPAssetRetire(LandMPModel model)
        {
            var result = ProtocolService.EncryptionPost<LandMPModel, AssetHandleResult>
            (
                model,
                Config.ServantDomain + "/LandServant/Delete_MPAssetRetire"
            );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Update_SplitAsset(LandModel model)
        {
            var result = ProtocolService.EncryptionPost<LandModel, AssetHandleResult>
           (
               model,
               Config.ServantDomain + "/LandServant/Update_SplitAsset"
           );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Update_AssetChangClass(LandModel model)
        {
            var result = ProtocolService.EncryptionPost<LandModel, AssetHandleResult>
           (
               model,
               Config.ServantDomain + "/LandServant/Update_AssetChangClass"
           );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Update_AssetMPChangClass(LandMPModel model)
        {
            var result = ProtocolService.EncryptionPost<LandMPModel, AssetHandleResult>
           (
               model,
               Config.ServantDomain + "/LandServant/Update_AssetMPChangClass"
           );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Update_RetireAsset(LandModel model)
        {
            var result = ProtocolService.EncryptionPost<LandModel, AssetHandleResult>
           (
               model,
               Config.ServantDomain + "/LandServant/Update_RetireAsset"
           );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Update_MPAssetRetire(LandMPModel model)
        {
            var result = ProtocolService.EncryptionPost<LandMPModel, AssetHandleResult>
           (
               model,
               Config.ServantDomain + "/LandServant/Update_MPAssetRetire"
           );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult Update_MergeAsset(LandModel model)
        {

            var result = ProtocolService.EncryptionPost<LandModel, AssetHandleResult>
           (
               model,
               Config.ServantDomain + "/LandServant/Update_MergeAsset"
           );
            return Tools.ExceptionConvert(result);
        }
        public AssetHandleResult UpdateToLand(string TRXHeaderID)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, AssetHandleResult>
            (
                 new SearchModel() { TRXHeaderID = TRXHeaderID },
                 Config.ServantDomain + "/LandServant/UpdateToLand"
            );
            return Tools.ExceptionConvert(result);
        }
        public AssetLandModel GetAssetLandDetail(int ID, string TableName)
        {
            var result = ProtocolService.EncryptionPost<AssetsDetailSearchModel, AssetLandModel>
             (
                  new AssetsDetailSearchModel() { ID = ID, TableName= TableName },
                  Config.ServantDomain + "/LandServant/GetAssetLandDetail"
             );
            return Tools.ExceptionConvert(result);
        }
        public AssetLandMPModel GetAssetLandMPDetail(int ID, string TableName)
        {
            var result = ProtocolService.EncryptionPost<AssetsDetailSearchModel, AssetLandMPModel>
             (
                  new AssetsDetailSearchModel() { ID = ID, TableName= TableName },
                  Config.ServantDomain + "/LandServant/GetAssetLandMPDetail"
             );
            return Tools.ExceptionConvert(result);
        }
        public AssetLandRetireModel GetAssetLandRetireDetail(int ID)
        {
            var result = ProtocolService.EncryptionPost<AssetsDetailSearchModel, AssetLandRetireModel>
             (
                  new AssetsDetailSearchModel() { ID = ID},
                  Config.ServantDomain + "/LandServant/GetAssetLandRetireDetail"
             );
            return Tools.ExceptionConvert(result);
        }
        public AssetLandMPRetireModel GetAssetLandMPRetireDetail(int ID)
        {
            var result = ProtocolService.EncryptionPost<AssetsDetailSearchModel, AssetLandMPRetireModel>
             (
                  new AssetsDetailSearchModel() { ID = ID},
                  Config.ServantDomain + "/LandServant/GetAssetLandMPRetireDetail"
             );
            return Tools.ExceptionConvert(result);
        }
        public AssetLandModel GetAssetFormLandByAssetNumber(string AssetNumber)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, AssetLandModel>
             (
                  new AssetsSearchModel() { AssetNumber = AssetNumber },
                  Config.ServantDomain + "/LandServant/GetAssetDetailByAssetNumber"
             );
            return Tools.ExceptionConvert(result);
        }
        public LandHeaderModel GetHeaderData(string TRXHeaderID)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, LandHeaderModel>
             (
                  new SearchModel() { TRXHeaderID = TRXHeaderID},
                  Config.ServantDomain + "/LandServant/GetHeaderData"
             );
            return Tools.ExceptionConvert(result);
        }

        public LandHeaderModel GetHeader(string AssetNumber, string Type)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, LandHeaderModel>
             (
                  new SearchModel() { AssetNumber = AssetNumber, Transaction_Type = Type },
                  Config.ServantDomain + "/LandServant/GetHeader"
             );
            return Tools.ExceptionConvert(result);
        }
        public AssetsIndexModel SearchAssets(AssetsSearchModel condition)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, AssetsIndexModel>
             (
                  condition,
                  Config.ServantDomain + "/LandServant/SearchAssets"
             );
            return Tools.ExceptionConvert(result);
        }
        public AssetsMPIndexModel SearchAssetsMP(AssetsMPSearchModel condition)
        {
            var result = ProtocolService.EncryptionPost<AssetsMPSearchModel, AssetsMPIndexModel>
             (
                  condition,
                  Config.ServantDomain + "/LandServant/SearchAssetsMP"
             );
            return Tools.ExceptionConvert(result);
        }


        public string CreateTRXID(string Type)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
             (
                  new SearchModel() { Transaction_Type = Type},
                  Config.ServantDomain + "/LandServant/CreateTRXID"
             );
            return Tools.ExceptionConvert(result).StrResult;
        }
        public string CreateAssetNumber(string type1, string type2)
        {
            var result = ProtocolService.EncryptionPost<AssetLandModel, StringResult>
            (
                 new AssetLandModel() { type1 = type1, type2 = type2},
                 Config.ServantDomain + "/LandServant/CreateAssetNumber"
            );
            return Tools.ExceptionConvert(result).StrResult;
        }
        public string GetMPAssetNumber(string type1, string type2)
        {
            var result = ProtocolService.EncryptionPost<AssetLandModel, StringResult>
            (
                 new AssetLandModel() { type1 = type1, type2 = type2 },
                 Config.ServantDomain + "/LandServant/GetMPAssetNumber"
            );
            return Tools.ExceptionConvert(result).StrResult;
        }
        public string CreateMPAssetNumber(string type1, string type2)
        {
            var result = ProtocolService.EncryptionPost<AssetLandMPModel, StringResult>
            (
                 new AssetLandMPModel() { type1 = type1, type2 = type2},
                 Config.ServantDomain + "/LandServant/CreateMPAssetNumber"
            );
            return Tools.ExceptionConvert(result).StrResult;
        }

        public AssetHandleResult HandleHeader(LandHeaderModel Header)
        {
            var result = ProtocolService.EncryptionPost<LandHeaderModel, AssetHandleResult>
            (
                Header,
                Config.ServantDomain + "/LandServant/HandleHeader"
            );
            return Tools.ExceptionConvert(result);
        }
        public bool GetDocID(string DocCode, out int DocID)
        {
            DocID = 0;
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                new SearchModel() { DocCode = DocCode },
                Config.ServantDomain + "/LandServant/GetDocID"
            );
            if (result != null)
            {
                DocID = Convert.ToInt32(Tools.ExceptionConvert(result).StrResult);
            }

            return Tools.ExceptionConvert(result).IsSuccess;
        }

        public bool UpdateFlowStatus(string TRX_Header_ID, string FlowStatus)
        {
            var result = ProtocolService.EncryptionPost<SearchModel, StringResult>
            (
                 new SearchModel() { TRXHeaderID = TRX_Header_ID, FlowStatus = FlowStatus },
                 Config.ServantDomain + "/LandServant/UpdateFlowStatus"
            );
            return Tools.ExceptionConvert(result).IsSuccess;
        }

        public IEnumerable<Library.Servant.Servant.Land.LandModels.CodeItem> SearchAssetsByCategoryCode(string CategoryCode)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, CodeItemModel>
             (
                  new AssetsSearchModel() { CategoryCode = CategoryCode },
                  Config.ServantDomain + "/LandServant/SearchAssetsByCategoryCode"
             );
            return Tools.ExceptionConvert(result).Items;
        }


        public IEnumerable<Library.Servant.Servant.Land.LandModels.CodeItem> GetDistrict(string City)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, CodeItemModel>
             (
                  new AssetsSearchModel() { City_Code = City },
                  Config.ServantDomain + "/LandServant/GetDistrict"
             );
            return Tools.ExceptionConvert(result).Items;
        }

        public IEnumerable<Library.Servant.Servant.Land.LandModels.CodeItem> GetSection(string City, string District)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, CodeItemModel>
             (
                  new AssetsSearchModel() { City_Code = City, District_Code = District },
                  Config.ServantDomain + "/LandServant/GetSection"
             );
            return Tools.ExceptionConvert(result).Items;
        }

        public IEnumerable<Library.Servant.Servant.Land.LandModels.CodeItem> GetSubSection(string City, string District, string Section)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, CodeItemModel>
             (
                  new AssetsSearchModel() { City_Code = City, District_Code = District, Section_Code = Section },
                  Config.ServantDomain + "/LandServant/GetSubSection"
             );
            return Tools.ExceptionConvert(result).Items;
            throw new NotImplementedException();
        }
        public IEnumerable<CodeItem> GetCodeItem(string CodeClass)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, CodeItemModel>
             (
                  new AssetsSearchModel() { CodeClass = CodeClass },
                  Config.ServantDomain + "/LandServant/GetCodeItem"
             );
            return Tools.ExceptionConvert(result).Items;
        }
        public IEnumerable<CodeItem> GetAssetMain_Kind(string Type)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, CodeItemModel>
            (
                 new AssetsSearchModel() { AssetMainKindType = Type },
                 Config.ServantDomain + "/LandServant/GetAssetMain_Kind"
            );
            return Tools.ExceptionConvert(result).Items;
        }

        public IEnumerable<CodeItem> GetAssetDetail_Kind(string MainKind)
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, CodeItemModel>
             (
                  new AssetsSearchModel() { MainKind = MainKind },
                  Config.ServantDomain + "/LandServant/GetAssetDetail_Kind"
             );
            return Tools.ExceptionConvert(result).Items;
        }

        public IEnumerable<CodeItem> GetAssetUse_Types()
        {
            var result = ProtocolService.EncryptionPost<AssetsSearchModel, CodeItemModel>
             (
                  new AssetsSearchModel(),
                  Config.ServantDomain + "/LandServant/GetAssetUse_Types"
             );
            return Tools.ExceptionConvert(result).Items;
        }

    }
}
