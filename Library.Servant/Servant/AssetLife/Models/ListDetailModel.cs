﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AssetLife.Models
{
    public class ListDetailModel : InvasionEncryption
    {
        public List<DetailModel> List { get; set; }
        public int LastUpdateBy { get; set; }
    }
}
