﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AssetLife.Models
{
    public class DetailModel : InvasionEncryption
    {
        public int ID { get; set; }

        public string LifeCodeNo { get; set; }
        public string LifeCode1 { get; set; }
        public string LifeCode2 { get; set; }
        public string LifeCode3 { get; set; }
        public string LifeCode4 { get; set; }
        public string LifeCode5 { get; set; }
        public string AssetText { get; set; }
        public decimal LifeYears { get; set; }
        public decimal LifeMonths { get; set; }
        public bool NotDeprnFlag { get; set; }
        public DateTime LastUpdatedTime { get; set; }
        public decimal LastUpdatedBy { get; set; }
    }
}
