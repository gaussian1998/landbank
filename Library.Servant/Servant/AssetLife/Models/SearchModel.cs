﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AssetLife.Models
{
    public class SearchModel : InvasionEncryption
    {
        public string AssetName { get; set; }
        public string BeginCode { get; set; }
        public string EndCode { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }

    }
}
