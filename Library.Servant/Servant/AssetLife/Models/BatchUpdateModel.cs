﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AssetLife.Models
{
    public class BatchUpdateModel : InvasionEncryption
    {
        public List<BatchItemModel> List { get; set; }
        public int LastUpdateBy { get; set; }
    }

    public class BatchItemModel : InvasionEncryption
    {
        public int ID { get; set; }
        public decimal LifeYears { get; set; }
        public decimal SalvageValue { get; set; }
        public bool NoDeprn { get; set; }
        public bool IsActive { get; set; }
    }
}
