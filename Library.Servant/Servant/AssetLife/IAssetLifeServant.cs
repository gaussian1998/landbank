﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Common.Models;
using Library.Servant.Servant.AssetLife.Models;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.AssetLife
{
    public interface IAssetLifeServant
    {
        IndexResult Index(SearchModel Search);
        MessageModel BatchUpdate(BatchUpdateModel Batch);
        MessageModel Create(DetailModel Create);
        DetailModel Detail(int Id);
        MessageModel Update(DetailModel Update);
        byte[] ExportData(SearchModel Search);
        MessageModel ImportData(List<DetailModel> Import, int LastUpdatedBy);
    }
}
