﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Common.Models;
using Library.Servant.Servant.AssetLife.Models;
using Library.Servant.Servant.Common.Models;
using Library.Entity.Edmx;
using System.Data.SqlClient;

namespace Library.Servant.Servant.AssetLife
{
    public class LocalAssetLifeServant : IAssetLifeServant
    {
        public MessageModel BatchUpdate(BatchUpdateModel Batch)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            DateTime updateTime = DateTime.Now;

            foreach (var item in Batch.List)
            {
                AS_Life_table_Records.Update(x => x.ID == item.ID, m => {
                    m.Life_Years = item.LifeYears;
                    m.Life_Months = item.LifeYears * 12;
                    m.Not_Deprn_Flag = item.NoDeprn;
                    m.Last_Updated_By = Batch.LastUpdateBy;
                    m.Last_Updated_Time = DateTime.Now;
                });
        
            }
            return message;
        }

        public MessageModel Create(DetailModel Create)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            AS_Life_table_Records.Create(new AS_Life_table
            {
                Life_Code_No = Create.LifeCodeNo,
                Life_Code_1 = Create.LifeCode1,
                Life_Code_2 = Create.LifeCode2,
                Life_Code_3 = Create.LifeCode3,
                Life_Code_4 = Create.LifeCode4,
                Life_Code_5 = Create.LifeCode5,
                Asset_Text = Create.AssetText,
                Life_Years = Create.LifeYears,
                Life_Months = Create.LifeMonths,
                Not_Deprn_Flag = Create.NotDeprnFlag,
                Last_Updated_By = Create.LastUpdatedBy,
                Last_Updated_Time = DateTime.Now
            });
            return message;

        }

        public DetailModel Detail(int Id)
        {
            DetailModel result = null;
            result = AS_Life_table_Records.First(x => x.ID == Id, m=> new DetailModel {
                ID = m.ID,
                LifeCodeNo = m.Life_Code_No,
                LifeCode1 = m.Life_Code_1,
                LifeCode2 = m.Life_Code_2,
                LifeCode3 = m.Life_Code_3,
                LifeCode4 = m.Life_Code_4,
                LifeCode5 = m.Life_Code_5,
                AssetText = m.Asset_Text,
                LifeYears = m.Life_Years,
                LifeMonths = m.Life_Months,
                NotDeprnFlag = m.Not_Deprn_Flag
            });
            return result;
        }

        public MessageModel Update(DetailModel Update)
        {
            MessageModel message = new MessageModel { IsSuccess = true };
            AS_Life_table_Records.Update(x => x.ID == Update.ID, m => {
                m.Life_Code_No = Update.LifeCodeNo;
                m.Life_Code_1 = Update.LifeCode1;
                m.Life_Code_2 = Update.LifeCode2;
                m.Life_Code_3 = Update.LifeCode3;
                m.Life_Code_4 = Update.LifeCode4;
                m.Life_Code_5 = Update.LifeCode5;
                m.Asset_Text = Update.AssetText;
                m.Life_Years = Update.LifeYears;
                m.Life_Months = Update.LifeMonths;
                m.Not_Deprn_Flag = Update.NotDeprnFlag;
            });
            return message;
        }

        public byte[] ExportData(SearchModel Search)
        {
            string sqlCommand = "select ID, Life_Code_No as LifeCodeNo, Life_Code_1 as LifeCode1, Life_Code_2 as LifeCode2, Life_Code_3 as LifeCode3, Life_Code_4 as LifeCode4, Life_Code_5 as LifeCode5, Asset_Text as AssetText, Life_Years as LifeYears, Not_Deprn_Flag as NotDeprn from AS_Life_table (nolock) where 0=0";

            if (!String.IsNullOrEmpty(Search.BeginCode))
            {
                sqlCommand += " and Life_Code_No >= @BeginCode";
            }
            else
            {
                Search.BeginCode = "";
            }

            if (!String.IsNullOrEmpty(Search.EndCode))
            {
                sqlCommand += " and Life_Code_No <= @EndCode";
            }
            else
            {
                Search.EndCode = "";
            }

            if (!String.IsNullOrEmpty(Search.AssetName))
            {
                sqlCommand += " and Asset_Text like '%' + @AssetName + '%'";
            }
            else
            {
                Search.AssetName = "";
            }

            byte[] header = { 0xEF, 0xBB, 0xBF };
            var sb = new StringBuilder();
            sb.Append("編號, 資產類別編號, 編碼1, 編碼2, 編碼3, 編碼4, 編碼5, 資產名稱, 年限(年), 不提列折舊");

            List<DetailModel> data;
            using (var context = new AS_LandBankEntities())
            {
                data = context.Database.SqlQuery<DetailModel>(sqlCommand,
                        new SqlParameter("@AssetName", Search.AssetName),
                        new SqlParameter("@BeginCode", Search.BeginCode),
                        new SqlParameter("@EndCode", Search.EndCode)).ToList();
            }

            foreach (var item in data)
            {
                sb.AppendFormat("{0}{1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}",
                    Environment.NewLine,
                    item.ID,
                    item.LifeCodeNo,
                    item.LifeCode1,
                    item.LifeCode2,
                    item.LifeCode3,
                    item.LifeCode4,
                    item.LifeCode5,
                    item.AssetText,
                    item.LifeYears,
                    item.NotDeprnFlag ? "是" : "否");
            }

            return header.Concat(Encoding.UTF8.GetBytes(sb.ToString())).ToArray();

        }

        
        public IndexResult Index(SearchModel Search)
        {
            bool skipBeginCode = false;
            bool skipEndCode = false;
            bool skipAssetName = false;
            if (String.IsNullOrEmpty(Search.BeginCode))
            {
                skipBeginCode = true;
            }
            if(String.IsNullOrEmpty(Search.EndCode))
            {
                skipEndCode = true;
            }
            if (String.IsNullOrEmpty(Search.AssetName))
            {
                skipAssetName = true;
            }
            IndexResult result = new IndexResult
            {
                Conditions = Search,
                Results = new PageList<DetailModel>()
            };

            result.Results = AS_Life_table_Records.Page(
                Search.CurrentPage,
                Search.PageSize,
                x => x.Life_Code_No, 
                x => 
                    (skipAssetName || x.Asset_Text.Contains(Search.AssetName)) &&
                    (skipBeginCode || x.Life_Code_No.CompareTo(Search.BeginCode) >=0 ) &&
                    (skipEndCode || x.Life_Code_No.CompareTo(Search.EndCode) <=0 ),
                m => new DetailModel
                {
                    ID = m.ID,
                    LifeCodeNo = m.Life_Code_No,
                    LifeCode1 = m.Life_Code_1,
                    LifeCode2 = m.Life_Code_2,
                    LifeCode3 = m.Life_Code_3,
                    LifeCode4 = m.Life_Code_4,
                    LifeCode5 = m.Life_Code_5,
                    LifeYears = m.Life_Years,
                    LifeMonths = m.Life_Months,
                    NotDeprnFlag = m.Not_Deprn_Flag,
                    AssetText = m.Asset_Text
                }
                );

            return result;
        }

        public MessageModel ImportData(List<DetailModel> Import, int LastUpdatedBy)
        {
            MessageModel message = new MessageModel { IsSuccess = true };
            if (Import.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("delete from AS_Life_table;");
                string insertCommnd = "INSERT [AS_Life_table] ([Life_Code_No], [Life_Code_1], [Life_Code_2], [Life_Code_3], [Life_Code_4], [Life_Code_5], [Asset_Text], [Life_Years], [Life_Months], [Not_Deprn_Flag], [Last_Updated_Time], [Last_Updated_By]) VALUES (N'{0}', N'{1}', N'{2}', N'{3}', N'{4}', N'{5}', N'{6}', {7}, {8}, {9}, getdate(), {10});";
                int i = 0;
                int totalCount = 0;

                foreach (var item in Import)
                {
                    if (i == 1000)
                    {
                        using (var context = new AS_LandBankEntities())
                        {
                            context.Database.ExecuteSqlCommand(sb.ToString());
                        }
                        sb.Clear();
                        i = 0;
                    }

                    sb.AppendFormat(insertCommnd,
                        item.LifeCodeNo, item.LifeCode1, item.LifeCode2,
                        item.LifeCode3, item.LifeCode4, item.LifeCode5,
                        item.AssetText, item.LifeYears, item.LifeYears * 12, item.NotDeprnFlag ? 1: 0,
                        LastUpdatedBy);

                        i++;
                        totalCount++;
                }

                using (var context = new AS_LandBankEntities())
                {
                    context.Database.ExecuteSqlCommand(sb.ToString());
                }

            }
            return message;

        }
    }
}
