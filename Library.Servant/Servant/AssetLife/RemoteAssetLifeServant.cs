﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Common.Models;
using Library.Servant.Servant.AssetLife.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.AssetLife
{
    public class RemoteAssetLifeServant : IAssetLifeServant
    {
        public MessageModel BatchUpdate(BatchUpdateModel Batch)
        {
            return RemoteServant.Post<BatchUpdateModel, MessageModel>(
                           Batch,
                           "/AssetLifeServant/BatchUpdate"
          );
        }

        public MessageModel Create(DetailModel Create)
        {
            return RemoteServant.Post<DetailModel, MessageModel>(
                          Create,
                          "/AssetLifeServant/Create"
         );
        }

        public DetailModel Detail(int Id)
        {
            return RemoteServant.Post<IntModel, DetailModel>(
                        new IntModel { Value=Id},
                        "/AssetLifeServant/Detail"
       );
        }

        public byte[] ExportData(SearchModel Search)
        {
            return (RemoteServant.Post<SearchModel, ByteModel>(
                      Search,
                       "/AssetLifeServant/ExportData")).Value;
        }

        public MessageModel ImportData(List<DetailModel> Import, int LastUpdatedBy)
        {
            ListDetailModel _listDetailModel = new ListDetailModel();
            _listDetailModel.List = new List<DetailModel>();
            _listDetailModel.List = Import;
            _listDetailModel.LastUpdateBy = LastUpdatedBy;
            return RemoteServant.Posttext<ListDetailModel, MessageModel>(
                       _listDetailModel,
                        "/AssetLifeServant/ImportData");
        }

        public IndexResult Index(SearchModel Search)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(
                       Search,
                        "/AssetLifeServant/Index"
       );
        }

        public MessageModel Update(DetailModel Update)
        {
            return RemoteServant.Post<DetailModel, MessageModel>(
                       Update,
                        "/AssetLifeServant/Update"
       );
        }
    }
}
