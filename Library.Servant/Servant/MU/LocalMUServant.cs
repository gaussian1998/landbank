﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.MU.MUModels;
using System.Transactions;
using Library.Entity.Edmx;
using Library.Servant.Repository.Land;
using Library.Servant.Repository.MU;

namespace Library.Servant.Servant.MU
{
    public class LocalMUServant : IMUServant
    {
        public EJObject Index(EJObject condition)
        {
            EJObject result = null;

            var query = MUGeneral.GetIndex(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetDetail2(EJObject condition)
        {
            EJObject result = null;

            var query = MUGeneral.GetDetail2(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject ImportLandData(EJObject condition)
        {
            EJObject result = null;

            var query = MUGeneral.ImportLandData(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject GetMaxNo(EJObject condition)
        {
            EJObject result = null;

            var query = MUGeneral.GetMaxNo(condition);

            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetApplyOrder(EJObject condition)
        {

            EJObject result = null;
            var query = MUGeneral.GetApplyOrder(condition);
            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        
        public EJObject GetLand(EJObject condition)
        {

            EJObject result = null;
            var query = MUGeneral.GetLand(condition);
            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetLandF300(EJObject condition)
        {

            EJObject result = null;
            var query = MUGeneral.GetLandF300(condition);
            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetDetail(EJObject condition)
        {

            EJObject result = null;
            var query = MUGeneral.GetDetail(condition);
            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetTarget(EJObject condition)
        {

            EJObject result = null;
            var query = MUGeneral.GetTarget(condition);
            if (query != null)
            {
                result = new EJObject();
                result.Obj = query;
            }
            return result;
        }
        public EJObject DocumentType(EInt type)
        {
            EJObject result = new EJObject();

            var query = MUGeneral.DocumentType(type);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject DocumentTypeOne(EInt type)
        {
            EJObject result = new EJObject();

            var query = MUGeneral.DocumentTypeOne(type);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetDPM(EInt type)
        {
            EJObject result = new EJObject();

            var query = MUGeneral.GetDPM(type);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject GetCode(EString type)
        {
            EJObject result = new EJObject();

            var query = MUGeneral.GetBookCode(type);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject EDOCServiceTest(EString type)
        {
            EJObject result = new EJObject();
            return result;
        }
        public EJObject GetOrder(EString ID)
        {
            EJObject result = new EJObject();

            var query = MUGeneral.GetOrder(ID);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject UpdateOrder(EJObject model2)
        {
            EJObject result = new EJObject();

            var query = MUGeneral.UpdateOrder(model2);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EJObject UpdateF200Order(EJObject model2)
        {
            EJObject result = new EJObject();

            var query = MUGeneral.UpdateF200Order(model2);
            if (query != null)
            {
                result.Obj = query;
            }
            return result;
        }
        public EString ApplyEDOC(EString type)
        {
            EString result = new EString();
            return result;
        }
        public EString ApplyQuery(EString type)
        {
            EString result = new EString();
            return result;
        }
        public EString ExportEDOCData(EString type)
        {
            EString result = new EString();
            return result;
        }
        
    }
}
