﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.MU.MUModels;
using Library.Servant.Common;

namespace Library.Servant.Servant.MU
{
    public class RemoteMUServant : IMUServant
    {
        public EJObject GetMaxNo(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/MUServant/GetMaxNo"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetDetail2(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/MUServant/GetDetail2"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject ImportLandData(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/MUServant/ImportLandData"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject GetDetail(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/MUServant/GetDetail"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject Index(EJObject condition)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/MUServant/Index"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetApplyOrder(EJObject condition)
        {

            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/MUServant/GetApplyOrder"
            );

            return Tools.ExceptionConvert(result);
        }
        
        public EJObject GetTarget(EJObject condition)
        {

            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                condition,
                Config.ServantDomain + "/MUServant/GetTarget"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject DocumentType(EInt type)
        {
            var result = ProtocolService.EncryptionPost<EInt, EJObject>
            (
                type,
                Config.ServantDomain + "/MUServant/DocumentType"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject DocumentTypeOne(EInt type)
        {
            var result = ProtocolService.EncryptionPost<EInt, EJObject>
            (
                type,
                Config.ServantDomain + "/MUServant/DocumentTypeOne"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject GetDPM(EInt type)
        {
            var result = ProtocolService.EncryptionPost<EInt, EJObject>
            (
                type,
                Config.ServantDomain + "/MUServant/GetDPM"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject GetOrder(EString type)
        {
            var result = ProtocolService.EncryptionPost<EString, EJObject>
            (
                type,
                Config.ServantDomain + "/MUServant/GetOrder"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject EDOCServiceTest(EString type)
        {
            var result = ProtocolService.EncryptionPost<EString, EJObject>
            (
                type,
                Config.ServantDomain + "/MUServant/EDOCServiceTest"
            );
            return Tools.ExceptionConvert(result);
        }

        public EJObject GetCode(EString type)
        {
            var result = ProtocolService.EncryptionPost<EString, EJObject>
            (
                type,
                Config.ServantDomain + "/MUServant/GetCode"
            );
            return Tools.ExceptionConvert(result);
        }
        public EJObject GetLand(EJObject model)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                model,
                Config.ServantDomain + "/MUServant/GetLand"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject GetLandF300(EJObject model)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                model,
                Config.ServantDomain + "/MUServant/GetLandF300"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject UpdateOrder(EJObject model)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                model,
                Config.ServantDomain + "/MUServant/UpdateOrder"
            );

            return Tools.ExceptionConvert(result);
        }
        public EJObject UpdateF200Order(EJObject model)
        {
            var result = ProtocolService.EncryptionPost<EJObject, EJObject>
            (
                model,
                Config.ServantDomain + "/MUServant/UpdateF200Order"
            );

            return Tools.ExceptionConvert(result);
        }
        public EString ApplyEDOC(EString type)
        {
            var result = ProtocolService.EncryptionPost<EString, EString>
            (
                type,
                Config.ServantDomain + "/MUServant/ApplyEDOC"
            );
            return Tools.ExceptionConvert(result);
        }
        public EString ApplyQuery(EString type)
        {
            var result = ProtocolService.EncryptionPost<EString, EString>
            (
                type,
                Config.ServantDomain + "/MUServant/ApplyQuery"
            );
            return Tools.ExceptionConvert(result);
        }
        public EString ExportEDOCData(EString type)
        {
            var result = ProtocolService.EncryptionPost<EString, EString>
            (
                type,
                Config.ServantDomain + "/MUServant/ExportEDOCData"
            );
            return Tools.ExceptionConvert(result);
        }
        
    }
}
