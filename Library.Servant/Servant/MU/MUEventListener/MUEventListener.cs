﻿using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalEventListener;
using Library.Servant.Servants.AbstractFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MU.MUEventListener
{
    public class MUEventListener : ExampleListener, IHttpGetProvider
    {
        private IMUServant MUServant = ServantAbstractFactory.MU();

        public HttpGetViewModel GetUrl(string FormNo, string carryData)
        {
            return new HttpGetViewModel { ControllerName = "MU", ActionName = "Edit", Parameter = "id=" + carryData + "&signmode=Y" };
        }

        public override bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            bool result = true;
           // MUServant.UpdateToMU(formNo);
            return result;
        }
    }
}
