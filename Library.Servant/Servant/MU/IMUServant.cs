﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.MU.MUModels;

namespace Library.Servant.Servant.MU
{
    public interface IMUServant
    {
        EJObject Index(EJObject condition);
        EJObject UpdateOrder(EJObject condition);
        EJObject UpdateF200Order(EJObject condition);
        EJObject GetMaxNo(EJObject condition);
        EJObject GetTarget(EJObject condition);
        EJObject DocumentType(EInt type);
        EJObject DocumentTypeOne(EInt type);
        EJObject GetDPM(EInt type);
        EJObject GetCode(EString type);
        EJObject GetOrder(EString type);
        EJObject EDOCServiceTest(EString type);
        EJObject GetDetail(EJObject condition);
        EJObject GetLand(EJObject condition); 
        EJObject GetLandF300(EJObject condition);
        EJObject GetApplyOrder(EJObject condition);
        EString ApplyQuery(EString type); 
        EString ApplyEDOC(EString type);
        EString ExportEDOCData(EString type);
        EJObject ImportLandData(EJObject condition);
        EJObject GetDetail2(EJObject condition); 
    }
}
