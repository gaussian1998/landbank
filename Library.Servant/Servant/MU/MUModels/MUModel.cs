﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.MU.MUModels
{
    public class MUModel : AbstractEncryptionDTO
    {
        public MUHeaderModel Header { get; set; }

        public IEnumerable<AssetModel> Assets { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
