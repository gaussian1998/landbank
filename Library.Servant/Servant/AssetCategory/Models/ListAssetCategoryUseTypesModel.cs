﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AssetCategory.Models
{
    public class ListAssetCategoryUseTypesModel : InvasionEncryption
    {
        public List<AssetCategoryUseTypesModel> ListData { get; set; }
    }
}
