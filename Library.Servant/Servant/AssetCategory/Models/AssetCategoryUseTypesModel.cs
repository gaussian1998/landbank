﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AssetCategory.Models
{
    public class AssetCategoryUseTypesModel : InvasionEncryption
    {
        public int ID { get; set; }
        public string No { get; set; }
        public string Name { get; set; }
        public Nullable<int> Last_Updated_By { get; set; }
    }
}
