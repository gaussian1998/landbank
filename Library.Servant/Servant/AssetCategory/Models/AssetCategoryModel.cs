﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.AssetCategory.Models
{
    public class AssetCategoryModel : InvasionEncryption
    {
        public int ID { get; set; }
        public Nullable<int> Last_Updated_By { get; set; }
        public int Main_Kind_ID { get; set; }
        public int Detail_Kind_ID { get; set; }
        public int Use_Type_ID { get; set; }
        public decimal Kind { get; set; }
        public decimal Layer { get; set; }
        public Nullable<int> Parent_ID { get; set; }
        public string Asset_Category_Code { get; set; }
        public string Asset_Category_Name { get; set; }
        public string Main_Kind { get; set; }
        public string Detail_Kind { get; set; }
        public string Use_Type { get; set; }
        public bool Is_Active { get; set; }
        public string Deprn_Method { get; set; }
        public decimal Life_Years { get; set; }
        public decimal Life_Months { get; set; }
        public decimal Salvage_Value { get; set; }
        public bool Not_Deprn_Flag { get; set; }
        public int SerialNo_Used { get; set; }
        public Nullable<System.DateTime> Last_Updated_Time { get; set; }
    }
}
