﻿using System.Collections.Generic;
using Library.Entity.Edmx;
using Library.Servant.Servant.AssetCategory.Models;

namespace Library.Servant.Servant.AssetCategory
{
    public interface IAssetCategoryServant
    {
        int UserId { get; set; }

        void AddNewCategory(AssetCategoryModel category);
        void AddNewDetailKind(AssetCategoryDetailKindsModel newDetailKind);
        void AddNewMainKind(AssetCategoryMainKindsModel newMainKind);
        void AddNewUserType(AssetCategoryUseTypesModel newUseType);
        void DeleteCategory(AssetCategoryModel category);
        void DeleteDetailKind(int mainKindId, string no);
        void DeleteMainKind(string no);
        void DeleteUserType(string no);
        AssetCategoryModel GetCategory(int mainKindId, int detailKindId, int useTypeId);
        AssetCategoryDetailKindsModel GetDetailKind(int id);
        AssetCategoryDetailKindsModel GetDetailKind(int mainKindId, string no);
        AssetCategoryMainKindsModel GetMainKindByNo(string mainKindNo);
        List<AssetCategoryMainKindsModel> GetMainKinds();
        AssetCategoryUseTypesModel GetUseTypeByNo(string no);
        List<AssetCategoryUseTypesModel> GetUseTypes();
        void UpdateDetailKind(AssetCategoryDetailKindsModel oldDetailKind);
        void UpdateMainKind(AssetCategoryMainKindsModel oldMainKind);
        void UpdateUseType(AssetCategoryUseTypesModel oldUseType);
        List<AssetCategoryDetailKindsModel> GetListDetailKindByMainKindNo(string mainKindNo);
    }
}