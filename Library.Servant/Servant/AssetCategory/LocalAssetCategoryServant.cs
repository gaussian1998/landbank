﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Entity.Edmx;
using Library.Servant.Servant.AssetCategory.Models;

namespace Library.Servant.Servant.AssetCategory
{
    public class LocalAssetCategoryServant : IAssetCategoryServant
    {
        IAS_Asset_Category_Main_KindsRepository repoMainKinds = RepositoryHelper.GetAS_Asset_Category_Main_KindsRepository();
        IAS_Asset_Category_Detail_KindsRepository repoDetailKinds = RepositoryHelper.GetAS_Asset_Category_Detail_KindsRepository();
        IAS_Asset_Category_Use_TypesRepository repoUseTypes = RepositoryHelper.GetAS_Asset_Category_Use_TypesRepository();
        IAS_Asset_CategoryRepository repoCategory = RepositoryHelper.GetAS_Asset_CategoryRepository();

        public int UserId { get; set; }

        public AssetCategoryMainKindsModel GetMainKindByNo(string mainKindNo)
        {
            AS_Asset_Category_Main_Kinds _model= repoMainKinds.All().Include("AS_Asset_Category_Detail_Kinds").Include("AS_Asset_Category").FirstOrDefault(p => p.No == mainKindNo);
            AssetCategoryMainKindsModel _viewModel = new AssetCategoryMainKindsModel();
            if (_model != null && _model.No == mainKindNo) {
                _viewModel.ID = _model.ID;
                _viewModel.Name = _model.Name;
                _viewModel.No = _model.No;
                _viewModel.Last_Updated_By = _model.Last_Updated_By;
            }
            return _viewModel;
        }

        public List<AssetCategoryMainKindsModel> GetMainKinds()
        {
            List<AS_Asset_Category_Main_Kinds> _list = repoMainKinds.All().Include("AS_Asset_Category_Detail_Kinds").Include("AS_Asset_Category").ToList();
            List<AssetCategoryMainKindsModel> _listModel = new List<AssetCategoryMainKindsModel>();
            if (_list != null && _list.Count > 0)
            {
                foreach (AS_Asset_Category_Main_Kinds _data in _list)
                {
                    //先建立Detail資料
                    List<AssetCategoryDetailKindsModel> _listDetail = new List<AssetCategoryDetailKindsModel>();
                    if (_data.AS_Asset_Category_Detail_Kinds != null && _data.AS_Asset_Category_Detail_Kinds.Count > 0) {
                        foreach (AS_Asset_Category_Detail_Kinds _detailKind in _data.AS_Asset_Category_Detail_Kinds) {
                            _listDetail.Add(new Models.AssetCategoryDetailKindsModel {
                                ID= _detailKind.ID,
                                Main_Kind_ID= _detailKind.Main_Kind_ID,
                                No= _detailKind.No,
                                Name = _detailKind.Name
                            });
                        }
                    }
                    _listModel.Add(new AssetCategoryMainKindsModel
                    {
                        ID = _data.ID,
                        Name = _data.Name,
                        No = _data.No,
                        Last_Updated_By = _data.Last_Updated_By,
                        ListDetailData= _listDetail
                    });
                }
            }
            return _listModel;
        }
        public List<AssetCategoryUseTypesModel> GetUseTypes()
        {
            List<AS_Asset_Category_Use_Types> _list =   repoUseTypes.All().Include("AS_Asset_Category").ToList();
            List<AssetCategoryUseTypesModel> _listModel = new List<AssetCategoryUseTypesModel>();
            if (_list != null && _list.Count > 0)
            {
                foreach (AS_Asset_Category_Use_Types _data in _list)
                {
                    _listModel.Add(new AssetCategoryUseTypesModel
                    {
                        ID = _data.ID,
                        Name = _data.Name,
                        No = _data.No,
                        Last_Updated_By = _data.Last_Updated_By
                    });
                }
            }
            return _listModel;
        }

        public void AddNewMainKind(AssetCategoryMainKindsModel newMainKind)
        {
            var entity = new AS_Asset_Category_Main_Kinds()
            {
                No = newMainKind.No,
                Name = newMainKind.Name,
                Is_Active = true,
                Last_Updated_By = newMainKind.Last_Updated_By,
                Last_Updated_Time = DateTime.Now
            };
            repoMainKinds.Add(entity);
            repoMainKinds.UnitOfWork.Commit();
        }
        public void UpdateMainKind(AssetCategoryMainKindsModel oldMainKind)
        {
            AS_Asset_Category_Main_Kinds_Records.Update(x => x.No == oldMainKind.No, entity => {
                entity.Name = oldMainKind.Name;
                entity.Last_Updated_By = oldMainKind.Last_Updated_By;
                entity.Last_Updated_Time = DateTime.Now;
            });
            //oldMainKind.Name = newMainKind.Name;
            //oldMainKind.Last_Updated_By = UserId;
            //oldMainKind.Last_Updated_Time = DateTime.Now;
            //AS_Asset_Category_Main_Kinds_Records.Create()
            //repoMainKinds.UnitOfWork.Commit();
        }

        public void DeleteMainKind(string no)
        {
            var oldMainKind = repoMainKinds.All().FirstOrDefault(p => p.No == no);
            repoMainKinds.Delete(oldMainKind);
            repoMainKinds.UnitOfWork.Commit();
        }

        public AssetCategoryDetailKindsModel GetDetailKind(int mainKindId, string no)
        {
            AS_Asset_Category_Detail_Kinds _model = repoDetailKinds.All().Include("AS_Asset_Category").FirstOrDefault(p => p.Main_Kind_ID == mainKindId && p.No == no);
            AssetCategoryDetailKindsModel _viewModel = new AssetCategoryDetailKindsModel();
            if (_model != null && _model.Main_Kind_ID == mainKindId && _model.No==no)
            {
                _viewModel.ID = _model.ID;
                _viewModel.Name = _model.Name;
                _viewModel.No = _model.No;
                _viewModel.Last_Updated_By = _model.Last_Updated_By;
                _viewModel.Main_Kind_ID = _model.Main_Kind_ID;
            }
            return _viewModel;
            //return repoDetailKinds.All().Include("AS_Asset_Category").FirstOrDefault(p => p.Main_Kind_ID == mainKindId && p.No == no);
        }
        public AssetCategoryDetailKindsModel GetDetailKind(int id)
        {
            AS_Asset_Category_Detail_Kinds _model = repoDetailKinds.All().Include("AS_Asset_Category").FirstOrDefault(p => p.ID == id);
            AssetCategoryDetailKindsModel _viewModel = new AssetCategoryDetailKindsModel();
            if (_model != null && _model.ID == id )
            {
                _viewModel.ID = _model.ID;
                _viewModel.Name = _model.Name;
                _viewModel.No = _model.No;
                _viewModel.Last_Updated_By = _model.Last_Updated_By;
                _viewModel.Main_Kind_ID = _model.Main_Kind_ID;
            }
            return _viewModel;
            //return repoDetailKinds.All().Include("AS_Asset_Category").FirstOrDefault(p => p.ID == id);
        }
        public List<AssetCategoryDetailKindsModel> GetListDetailKindByMainKindNo(string mainKindNo)
        {
            AS_Asset_Category_Main_Kinds _mainKinds = repoMainKinds.All().Include("AS_Asset_Category_Detail_Kinds").Include("AS_Asset_Category").FirstOrDefault(p => p.No == mainKindNo);
            List<AssetCategoryDetailKindsModel> _listModel = new List<AssetCategoryDetailKindsModel>();
            if (_mainKinds != null && _mainKinds.No == mainKindNo &&
                _mainKinds.AS_Asset_Category_Detail_Kinds.Count() > 0)
            {
                foreach (AS_Asset_Category_Detail_Kinds _detailKind in _mainKinds.AS_Asset_Category_Detail_Kinds)
                {
                    _listModel.Add(new AssetCategoryDetailKindsModel
                    {
                        ID = _detailKind.ID,
                        Name = _detailKind.Name,
                        No = _detailKind.No
                    });
                }
            }
            return _listModel;
        }
        public void AddNewDetailKind(AssetCategoryDetailKindsModel newDetailKind)
        {
            var p = new AS_Asset_Category_Detail_Kinds()
            {
                No = newDetailKind.No,
                Name = newDetailKind.Name,
                Is_Active = true,
                Main_Kind_ID = newDetailKind.Main_Kind_ID,
                Last_Updated_By = UserId,
                Last_Updated_Time = DateTime.Now
            };
            repoDetailKinds.Add(p);
            repoDetailKinds.UnitOfWork.Commit();
        }

        public void UpdateDetailKind(AssetCategoryDetailKindsModel oldDetailKind)
        {
            AS_Asset_Category_Detail_Kinds_Records.Update(x => x.ID == oldDetailKind.ID, entity => {
                entity.Name = oldDetailKind.Name;
                entity.No = oldDetailKind.No;
                entity.Last_Updated_By = oldDetailKind.Last_Updated_By;
                entity.Last_Updated_Time = DateTime.Now;
            });
            //oldDetailKind.Name = newDetailKind.Name;
            //oldDetailKind.No = newDetailKind.No;
            //oldDetailKind.Last_Updated_By = UserId;
            //oldDetailKind.Last_Updated_Time = DateTime.Now;
            //repoDetailKinds.UnitOfWork.Commit();
        }

        public void DeleteDetailKind(int mainKindId, string no)
        {
            var entity = repoDetailKinds.All().FirstOrDefault(p => p.Main_Kind_ID == mainKindId && p.No == no);
            //entity.Last_Updated_By = 0;
            //entity.Last_Updated_Time = DateTime.Now;
            repoDetailKinds.Delete(entity);
            repoDetailKinds.UnitOfWork.Commit();
        }

        public AssetCategoryUseTypesModel GetUseTypeByNo(string no)
        {
            AS_Asset_Category_Use_Types _model = repoUseTypes.All().Include("AS_Asset_Category").FirstOrDefault(p => p.No == no);
            AssetCategoryUseTypesModel _viewModel = new AssetCategoryUseTypesModel();
            if (_model != null && _model.No == no)
            {
                _viewModel.ID = _model.ID;
                _viewModel.Name = _model.Name;
                _viewModel.No = _model.No;
                _viewModel.Last_Updated_By = _model.Last_Updated_By;
                //_viewModel. = _model.Main_Kind_ID;
            }
            return _viewModel;
            //return repoUseTypes.All().Include("AS_Asset_Category").FirstOrDefault(p => p.No == no);
        }
        public void AddNewUserType(AssetCategoryUseTypesModel newUseType)
        {
            var entity = new AS_Asset_Category_Use_Types()
            {
                No = newUseType.No,
                Is_Active = true,
                Name = newUseType.Name,
                Last_Updated_By = newUseType.Last_Updated_By,
                Last_Updated_Time = DateTime.Now
            };
            repoUseTypes.Add(entity);
            repoUseTypes.UnitOfWork.Commit();
        }

        public void UpdateUseType(AssetCategoryUseTypesModel oldUseType)
        {
            AS_Asset_Category_Use_Types_Records.Update(x => x.No == oldUseType.No, entity => {
                entity.Name = oldUseType.Name;
                entity.Last_Updated_By = oldUseType.Last_Updated_By;
                entity.Last_Updated_Time = DateTime.Now;
            });
            //oldUseType.Name = newUseType.Name;
            //oldUseType.Last_Updated_By = UserId;
            //oldUseType.Last_Updated_Time = DateTime.Now;
            //repoUseTypes.UnitOfWork.Commit();
        }

        public void DeleteUserType(string no)
        {
            var entity = repoUseTypes.All().Include("AS_Asset_Category").FirstOrDefault(p => p.No == no); ;
            //entity.Last_Updated_By = UserId;
            //entity.Last_Updated_Time = DateTime.Now;
            repoUseTypes.Delete(entity);
            repoUseTypes.UnitOfWork.Commit();
        }


        public AssetCategoryModel GetCategory(int mainKindId, int detailKindId, int useTypeId)
        {
            AS_Asset_Category _model = repoCategory.All().FirstOrDefault(p =>
            p.Main_Kind_ID == mainKindId &&
            p.Detail_Kind_ID == detailKindId &&
            p.Use_Type_ID == useTypeId);
            AssetCategoryModel _viewModel = new AssetCategoryModel();
            if (_model != null && _model.Main_Kind_ID == mainKindId && 
                _model.Detail_Kind_ID == detailKindId && _model.Use_Type_ID== useTypeId)
            {
                _viewModel.ID= _model.ID;
                _viewModel.Main_Kind_ID = _model.Main_Kind_ID;
                _viewModel.Use_Type_ID = _model.Use_Type_ID;
                _viewModel.Detail_Kind_ID = _model.Detail_Kind_ID;
                _viewModel.Last_Updated_By = _model.Last_Updated_By;
                _viewModel.Kind = _model.Kind;
                _viewModel.Layer = _model.Layer;
                _viewModel.Parent_ID = _model.Parent_ID;
                _viewModel.Asset_Category_Code = _model.Asset_Category_Code;
                _viewModel.Asset_Category_Name = _model.Asset_Category_Name;
                _viewModel.Main_Kind = _model.Main_Kind;
                _viewModel.Detail_Kind = _model.Detail_Kind;
                _viewModel.Use_Type = _model.Use_Type;
                _viewModel.Is_Active = _model.Is_Active;
                _viewModel.Deprn_Method = _model.Deprn_Method;
                _viewModel.Life_Years = _model.Life_Years;
                _viewModel.Life_Months = _model.Life_Months;
                _viewModel.Salvage_Value = _model.Salvage_Value;
                _viewModel.Not_Deprn_Flag = _model.Not_Deprn_Flag;
                _viewModel.SerialNo_Used = _model.SerialNo_Used;
                _viewModel.Last_Updated_Time = _model.Last_Updated_Time; 
            }
            return _viewModel;
            //return repoCategory.All().FirstOrDefault(p =>
            //p.Main_Kind_ID == mainKindId &&
            //p.Detail_Kind_ID == detailKindId &&
            //p.Use_Type_ID == useTypeId);
        }

        public void AddNewCategory(AssetCategoryModel category)
        {
            var mainKind = repoMainKinds.All().First(p => p.ID == category.Main_Kind_ID);
            var detailKind = repoDetailKinds.All().First(p => p.ID == category.Detail_Kind_ID);
            var useType = repoUseTypes.All().First(p => p.ID == category.Use_Type_ID);
            var entity = new AS_Asset_Category()
            {
                Kind = 9,
                Layer = 4,
                Parent_ID = 0,
                Asset_Category_Code =
                    mainKind.No + "." +
                    detailKind.No + "." +
                    useType.No,
                Asset_Category_Name = detailKind.Name + "-" + useType.Name,
                Main_Kind = mainKind.No,
                Detail_Kind = detailKind.No,
                Use_Type = useType.No,
                Is_Active = true,
                Deprn_Method = "STL",
                Life_Years = 0,
                Life_Months = 0,
                Salvage_Value = 0,
                Not_Deprn_Flag = false,
                Last_Updated_By = UserId,
                Last_Updated_Time = DateTime.Now,
                Main_Kind_ID = category.Main_Kind_ID,
                Detail_Kind_ID = category.Detail_Kind_ID,
                Use_Type_ID = category.Use_Type_ID,
            };
            repoCategory.Add(entity);
            repoCategory.UnitOfWork.Commit();
        }

        public void DeleteCategory(AssetCategoryModel category)
        {
            var entity = repoCategory.All().FirstOrDefault(p =>
            p.Main_Kind_ID == category.Main_Kind_ID &&
            p.Detail_Kind_ID == category.Detail_Kind_ID &&
            p.Use_Type_ID == category.Use_Type_ID);
            repoCategory.Delete(entity);
            repoCategory.UnitOfWork.Commit();
        }
 
    }
}
