﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Entity.Edmx;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.AssetCategory.Models;

namespace Library.Servant.Servant.AssetCategory
{
    public class RemoteAssetCategoryServant : IAssetCategoryServant
    {
        //IAS_Asset_Category_Main_KindsRepository repoMainKinds = RepositoryHelper.GetAS_Asset_Category_Main_KindsRepository();
        //IAS_Asset_Category_Detail_KindsRepository repoDetailKinds = RepositoryHelper.GetAS_Asset_Category_Detail_KindsRepository();
        //IAS_Asset_Category_Use_TypesRepository repoUseTypes = RepositoryHelper.GetAS_Asset_Category_Use_TypesRepository();
        //IAS_Asset_CategoryRepository repoCategory = RepositoryHelper.GetAS_Asset_CategoryRepository();

        public int UserId { get; set; }

        public AssetCategoryMainKindsModel GetMainKindByNo(string mainKindNo)
        {
            return RemoteServant.Post<StringModel, AssetCategoryMainKindsModel>(
                           new StringModel { Value= mainKindNo },
                           "/AssetCategoryServant/GetMainKindByNo"
          );
        }

        public List<AssetCategoryMainKindsModel> GetMainKinds()
        {
            return (RemoteServant.Post<VoidModel, ListAssetCategoryMainKindsModel>(
                           new VoidModel { },
                           "/AssetCategoryServant/GetMainKinds"
          )).ListData;
            //return repoMainKinds.All().Include("AS_Asset_Category_Detail_Kinds").Include("AS_Asset_Category");
        }

        public List<AssetCategoryUseTypesModel> GetUseTypes()
        {
            return (RemoteServant.Post<VoidModel, ListAssetCategoryUseTypesModel>(
                         new VoidModel { },
                         "/AssetCategoryServant/GetUseTypes"
        )).ListData;
            //return repoUseTypes.All().Include("AS_Asset_Category");
        }

        public void AddNewMainKind(AssetCategoryMainKindsModel newMainKind)
        {
            var entity = new AssetCategoryMainKindsModel()
            {
                No = newMainKind.No,
                Name = newMainKind.Name,
                Last_Updated_By = UserId,
            };
            RemoteServant.Post<AssetCategoryMainKindsModel, VoidModel>(
                        entity,
                         "/AssetCategoryServant/AddNewMainKind"
        );

            //repoMainKinds.Add(entity);
            //repoMainKinds.UnitOfWork.Commit();
        }
        public void UpdateMainKind(AssetCategoryMainKindsModel oldMainKind)
        {
            oldMainKind.Last_Updated_By = UserId;
            RemoteServant.Post<AssetCategoryMainKindsModel, VoidModel>(
                      oldMainKind,
                      "/AssetCategoryServant/UpdateMainKind"
     );
            //repoMainKinds.UnitOfWork.Commit();
        }

        public void DeleteMainKind(string no)
        {
            RemoteServant.Post<StringModel, VoidModel>(
                         new StringModel { Value = no },
                         "/AssetCategoryServant/DeleteMainKind"
        );
            //var oldMainKind = repoMainKinds.All().FirstOrDefault(p => p.No == no);
            //repoMainKinds.Delete(oldMainKind);
            //repoMainKinds.UnitOfWork.Commit();
        }

        public AssetCategoryDetailKindsModel GetDetailKind(int mainKindId, string no)
        {
            var entity = new AssetCategoryDetailKindsModel()
            {
                No = no,
                Main_Kind_ID = mainKindId
            };
            return RemoteServant.Post<AssetCategoryDetailKindsModel, AssetCategoryDetailKindsModel>(
                        entity,
                        "/AssetCategoryServant/GetDetailKind"
       );
            //return repoDetailKinds.All().Include("AS_Asset_Category").FirstOrDefault(p => p.Main_Kind_ID == mainKindId && p.No == no);
        }
        public AssetCategoryDetailKindsModel GetDetailKind(int id)
        {
            return RemoteServant.Post<IntModel, AssetCategoryDetailKindsModel>(
                        new IntModel { Value = id },
                        "/AssetCategoryServant/GetDetailKindByID"
       );
            //return repoDetailKinds.All().Include("AS_Asset_Category").FirstOrDefault(p => p.ID == id);
        }
        public void AddNewDetailKind(AssetCategoryDetailKindsModel newDetailKind)
        {
            newDetailKind.Last_Updated_By = UserId;
            RemoteServant.Post<AssetCategoryDetailKindsModel, VoidModel>(
                        newDetailKind,
                        "/AssetCategoryServant/AddNewDetailKind");
        }

        public void UpdateDetailKind(AssetCategoryDetailKindsModel oldDetailKind)
        {
            oldDetailKind.Last_Updated_By = UserId; 
            RemoteServant.Post<AssetCategoryDetailKindsModel, VoidModel>(
                      oldDetailKind,
                      "/AssetCategoryServant/UpdateDetailKind"
     );
        }

        public void DeleteDetailKind(int mainKindId, string no)
        {
            var model = new AssetCategoryDetailKindsModel()
            {
                No= no,
                Main_Kind_ID = mainKindId,
                Last_Updated_By=UserId
            };
            RemoteServant.Post<AssetCategoryDetailKindsModel, VoidModel>(
                        model,
                         "/AssetCategoryServant/DeleteDetailKind"
        );
        }

        public AssetCategoryUseTypesModel GetUseTypeByNo(string no)
        {
            return RemoteServant.Post<StringModel, AssetCategoryUseTypesModel>(
                           new StringModel { Value = no },
                           "/AssetCategoryServant/GetUseTypeByNo"
          );
            //return repoUseTypes.All().Include("AS_Asset_Category").FirstOrDefault(p => p.No == no);
        }
        public void AddNewUserType(AssetCategoryUseTypesModel newUseType)
        {
            newUseType.Last_Updated_By = UserId;
            RemoteServant.Post<AssetCategoryUseTypesModel, VoidModel>(
                      newUseType,
                       "/AssetCategoryServant/AddNewUserType");
        }

        public void UpdateUseType(AssetCategoryUseTypesModel oldUseType)
        {
            oldUseType.Last_Updated_By = UserId;
            RemoteServant.Post<AssetCategoryUseTypesModel, VoidModel>(
                      oldUseType,
                      "/AssetCategoryServant/UpdateUseType"
     );
        }

        public void DeleteUserType(string no)
        {
            RemoteServant.Post<StringModel, VoidModel>(
                         new StringModel { Value = no },
                         "/AssetCategoryServant/DeleteUserType"
        );
        }
        public AssetCategoryModel GetCategory(int mainKindId, int detailKindId, int useTypeId)
        {
            AssetCategoryModel _assetCategory = new AssetCategoryModel
            {
                Main_Kind_ID= mainKindId,
                Detail_Kind_ID = detailKindId,
                Use_Type_ID= useTypeId
            };
            return RemoteServant.Post<AssetCategoryModel, AssetCategoryModel>(
                     _assetCategory,
                     "/AssetCategoryServant/GetCategory"
    );
            //return repoCategory.All().FirstOrDefault(p =>
            //p.Main_Kind_ID == mainKindId &&
            //p.Detail_Kind_ID == detailKindId &&
            //p.Use_Type_ID == useTypeId);
        }

        public void AddNewCategory(AssetCategoryModel category)
        {
            //var mainKind = repoMainKinds.All().First(p => p.ID == category.Main_Kind_ID);
            //var detailKind = repoDetailKinds.All().First(p => p.ID == category.Detail_Kind_ID);
            //var useType = repoUseTypes.All().First(p => p.ID == category.Use_Type_ID);
            //var entity = new AS_Asset_Category()
            //{
            //    Kind = 9,
            //    Layer = 4,
            //    Parent_ID = 0,
            //    Asset_Category_Code =
            //        mainKind.No + "." +
            //        detailKind.No + "." +
            //        useType.No,
            //    Asset_Category_Name = detailKind.Name + "-" + useType.Name,
            //    Main_Kind = mainKind.No,
            //    Detail_Kind = detailKind.No,
            //    Use_Type = useType.No,
            //    Is_Active = true,
            //    Deprn_Method = "STL",
            //    Life_Years = 0,
            //    Life_Months = 0,
            //    Salvage_Value = 0,
            //    Not_Deprn_Flag = false,
            //    Last_Updated_By = UserId,
            //    Last_Updated_Time = DateTime.Now,
            //    Main_Kind_ID = category.Main_Kind_ID,
            //    Detail_Kind_ID = category.Detail_Kind_ID,
            //    Use_Type_ID = category.Use_Type_ID,
            //};
            //repoCategory.Add(entity);
            //repoCategory.UnitOfWork.Commit();
            category.Last_Updated_By = UserId;
            RemoteServant.Post<AssetCategoryModel, VoidModel>(
                      category,
                      "/AssetCategoryServant/AddNewCategory"
     );
        }

        public void DeleteCategory(AssetCategoryModel category)
        {
            RemoteServant.Post<AssetCategoryModel, VoidModel>(
                        category,
                        "/AssetCategoryServant/DeleteCategory"
       );
            //var entity = GetCategory(category.Main_Kind_ID, category.Detail_Kind_ID, category.Use_Type_ID);
            //repoCategory.Delete(entity);
            //repoCategory.UnitOfWork.Commit();
        }
        public List<AssetCategoryDetailKindsModel> GetListDetailKindByMainKindNo(string mainKindNo) {
            return (RemoteServant.Post<StringModel, ListAssetCategoryDetailKindsModel>(
                        new StringModel { Value= mainKindNo },
                        "/AssetCategoryServant/GetListDetailKindByMainKindNo"
       )).ListData;
        }
    }
}
