﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Entity.Edmx;
using Library.Servant.Servant.AccountingCalendar.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.AccountingCalendar
{
    public class RemoteAccountingCalendarServant : IAccountingCalendarServant
    {
        public MessageModel BatchDelete(BatchDeleteModel BatchDelete)
        {
            return RemoteServant.Post<BatchDeleteModel, MessageModel>(
                           BatchDelete,
                           "/AccountingCalendarServant/BatchDelete"
          );
        }

        public MessageModel Create(DetailModel Create)
        {
            return RemoteServant.Post<DetailModel, MessageModel>(
                           Create,
                           "/AccountingCalendarServant/BatchDelete"
          );
        }

        public DetailModel Detail(int ID)
        {
            return RemoteServant.Post<IntModel, DetailModel>(
                           new IntModel { Value=ID},
                           "/AccountingCalendarServant/Detail"
          );
        }

        public IndexResult Index(SearchModel Search)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(
                         Search,
                          "/AccountingCalendarServant/Index"
         );
        }

        public MessageModel Update(DetailModel Update)
        {
            return RemoteServant.Post<DetailModel, MessageModel>(
                        Update,
                         "/AccountingCalendarServant/Update"
        );
        }
        public MessageModel Copy(CopyModel Copy)
        {
            return RemoteServant.Post<CopyModel, MessageModel>(
                         Copy,
                          "/AccountingCalendarServant/Copy");
        }

        public List<SelectedModel> GetCalendarYears()
        {
            ListModel _listModel = new ListModel();
            return (RemoteServant.Post<VoidModel, ListModel>(
                         new VoidModel { },
                          "/AccountingCalendarServant/GetCalendarYears")).ListSelectedModel;
        }
    }
}
