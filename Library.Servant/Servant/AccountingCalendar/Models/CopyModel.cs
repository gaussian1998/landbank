﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AccountingCalendar.Models
{
    public class CopyModel : InvasionEncryption
    {
        public decimal CopyYears { get; set; }
        public string AccountingBook { get; set; }
        public decimal TermBasis { get; set; }
        public decimal ToYears { get; set; }
        public bool CopyAll { get; set; }
        public int LastUpdatedBy { get; set; }
    }
}
