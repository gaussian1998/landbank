﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AccountingCalendar.Models
{
    public class DetailModel : InvasionEncryption
    {
        public int ID { get; set; }
        public string CalendarType { get; set; }
        public string AccountingBook { get; set; }
        public string CalendarValue { get; set; }
        public int Years { get; set; }
        public int Months { get; set; }
        public int Quater { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public int TermBasis { get; set; }
        public int LastUpdateBy { get; set; }
    }
}
