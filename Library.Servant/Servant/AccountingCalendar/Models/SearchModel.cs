﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AccountingCalendar.Models
{
    public class SearchModel : InvasionEncryption
    {
        public string AccountingYear { get; set; }
        public string AccountingBook { get; set; }
        public string AccountingCalendarType { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
    }
}
