﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingCalendar.Models;
using Library.Servant.Servant.Common.Models;
using Library.Entity.Edmx;
using Library.Common.Models;

namespace Library.Servant.Servant.AccountingCalendar
{
    public class LocalAccountingCalendarServant : IAccountingCalendarServant
    {
        public MessageModel BatchDelete(BatchDeleteModel BatchDelete)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            foreach (int id in BatchDelete.IDList)
            {
                AS_GL_Calendar_Records.DeleteFirst(x => x.ID == id);
            }
            return message;
        }

        public MessageModel Create(DetailModel Create)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            var item = new AS_GL_Calendar {
                Book_Type = Create.AccountingBook,
                GL_Term = Create.CalendarType,
                Calendar_Value = Create.CalendarValue,
                Years = Create.Years != 0 ? Create.Years : (decimal?)null,
                Months = Create.Months != 0 ? Create.Months : (decimal?)null,
                Quarter = Create.Quater != 0 ? Create.Quater : (decimal?)null,
                Date_Of_Begin = Create.BeginDate,
                Date_Of_End = Create.EndDate,
                Term_Basis = Create.TermBasis,
                Last_Updated_By = Create.LastUpdateBy,
                Last_Updated_Time = DateTime.Now
            };

            AS_GL_Calendar_Records.Create(item);
            return message;
        }

        public DetailModel Detail(int ID)
        {
            var item = AS_GL_Calendar_Records.First(x => x.ID == ID);
            DetailModel result = new DetailModel {
                ID = item.ID,
                CalendarType = item.GL_Term,
                AccountingBook = item.Book_Type,
                CalendarValue = item.Calendar_Value,
                Years = Convert.ToInt32(item.Years),
                Months = Convert.ToInt32(item.Months),
                Quater = Convert.ToInt32(item.Quarter),
                BeginDate = Convert.ToDateTime(item.Date_Of_Begin),
                EndDate = Convert.ToDateTime(item.Date_Of_End),
                TermBasis = Convert.ToInt32(item.Term_Basis)     
            };
            return result;
        }

        public MessageModel Update(DetailModel Update)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            AS_GL_Calendar_Records.Update(x => x.ID == Update.ID, m => {
                m.GL_Term = Update.CalendarType;
                m.Book_Type = Update.AccountingBook;
                m.Calendar_Value = Update.CalendarValue;
                m.Years = Update.Years != 0 ? Update.Years : m.Years;
                m.Months = Update.Months != 0 ? Update.Months : m.Months;
                m.Quarter = Update.Quater != 0 ? Update.Quater : m.Quarter;
                m.Date_Of_Begin = Update.BeginDate;
                m.Date_Of_End = Update.EndDate;
                m.Term_Basis = Update.TermBasis;
                m.Last_Updated_By = Update.LastUpdateBy;
                m.Last_Updated_Time = DateTime.Now;
            });
            return message;
        }

        public IndexResult Index(SearchModel Search)
        {
            IndexResult _result = new IndexResult {
                Conditions = Search,
                Page = null
            };
            decimal years = Convert.ToDecimal(Search.AccountingYear);
            bool skipCalendarType = false;
            if (Search.AccountingCalendarType == "A")
            {
                skipCalendarType = true;
            }

            PageList<AS_GL_Calendar> _dbResult = AS_GL_Calendar_Records.Page(
                Search.CurrentPage,
                Search.PageSize,
                x => x.Years,
                x =>(x.Years == years && 
                    (skipCalendarType || x.GL_Term == Search.AccountingCalendarType) && 
                    x.Book_Type == Search.AccountingBook)
                );

            PageList<DetailModel> _page = new PageList<DetailModel>();
            _page.TotalAmount = _dbResult.TotalAmount;
            _page.Items = new List<DetailModel>();

            foreach (var item in _dbResult.Items)
            {
                DetailModel _item = new DetailModel
                {
                    ID = item.ID,
                    CalendarType = item.GL_Term,
                    AccountingBook = item.Book_Type,
                    CalendarValue = item.Calendar_Value,
                    Years = Convert.ToInt32(item.Years),
                    Months = Convert.ToInt32(item.Months),
                    Quater = Convert.ToInt32(item.Quarter),
                    BeginDate = Convert.ToDateTime(item.Date_Of_Begin),
                    EndDate = Convert.ToDateTime(item.Date_Of_End),
                    TermBasis = Convert.ToInt32(item.Term_Basis)

                };
                _page.Items.Add(_item);
            }
            _result.Page = _page;
            return _result;
        }

        public MessageModel Copy(CopyModel Copy) {
            MessageModel message = new MessageModel { IsSuccess = true, Message = "" };
            string id = "";

            List<AS_GL_Calendar> dbResult = AS_GL_Calendar_Records.Find(x => x.Book_Type == Copy.AccountingBook && x.Years == Copy.CopyYears);
            if (dbResult == null || dbResult.Count == 0)
            {
                message.IsSuccess = false;
                message.MessagCode = MessageCode.NoDataToCopy.ToString();
            }
            else
            {
                if (!Copy.CopyAll)
                {
                    //year only
                    var item = dbResult.FirstOrDefault(x => x.GL_Term == "Y");
                    if (item == null)
                    {
                        message.IsSuccess = false;
                        message.MessagCode = MessageCode.NoDataToCopy.ToString();
                    }
                    else
                    {
                        string newCalendarValue = item.Calendar_Value.Replace(Copy.CopyYears.ToString(), Copy.ToYears.ToString());
                        DateTime beginDate = DateTime.Today;
                        DateTime endDate = DateTime.Today;

                        CalculateYearPeriod(Copy.CopyYears, Copy.TermBasis, ref beginDate, ref endDate);
                        AS_GL_Calendar newItem = new AS_GL_Calendar {
                            GL_Term = item.GL_Term,
                            Book_Type = item.Book_Type,
                            Calendar_Value = newCalendarValue,
                            Years = Copy.ToYears,
                            Months = item.Months,
                            Quarter = item.Quarter,
                            Date_Of_Begin = beginDate,
                            Date_Of_End = endDate,
                            Term_Basis = Copy.TermBasis,
                            Last_Updated_By = Copy.LastUpdatedBy,
                            Last_Updated_Time = DateTime.Now
                        };

                        AS_GL_Calendar_Records.Create(newItem);
                        id = newItem.ID.ToString();
                    }
                }
                else
                {
                    foreach (var item in dbResult)
                    {
                        string newCalendarValue = item.Calendar_Value.Replace(Copy.CopyYears.ToString(), Copy.ToYears.ToString());
                        DateTime beginDate = DateTime.Today;
                        DateTime endDate = DateTime.Today;

                        switch (item.GL_Term)
                        {
                            case "Y":
                                CalculateYearPeriod(Copy.ToYears, Copy.TermBasis, ref beginDate, ref endDate);
                                break;

                            case "Q":
                                CalculateQuarterPeriod(Copy.ToYears, Convert.ToDecimal(item.Quarter), Copy.TermBasis, ref beginDate, ref endDate);
                                break;

                            case "M":
                                CalculateMonthPeriod(Copy.ToYears, Convert.ToDecimal(item.Months), Copy.TermBasis, ref beginDate, ref endDate);
                                break;

                            case "H":
                                CalculateHalfYearPeriod(Copy.ToYears, Convert.ToDecimal(item.Months), Copy.TermBasis, ref beginDate, ref endDate);
                                break;
                        }

                        AS_GL_Calendar newItem = new AS_GL_Calendar
                        {
                            GL_Term = item.GL_Term,
                            Book_Type = item.Book_Type,
                            Calendar_Value = newCalendarValue,
                            Years = Copy.ToYears,
                            Months = item.Months,
                            Quarter = item.Quarter,
                            Date_Of_Begin = beginDate,
                            Date_Of_End = endDate,
                            Term_Basis = Copy.TermBasis,
                            Last_Updated_By = Copy.LastUpdatedBy,
                            Last_Updated_Time = DateTime.Now
                        };

                        AS_GL_Calendar_Records.Create(newItem);
                        if (id == "")
                        {
                            id = newItem.ID.ToString();
                        }
                        else
                        {
                            id += String.Format(",{0}", newItem.ID);
                        }
                    }
                }
            }
            message.Message = id;
            return message;
        }
        private void CalculateYearPeriod(decimal Year, decimal Term, ref DateTime BeginDate, ref DateTime EndDate)
        {
            EndDate = Convert.ToDateTime(String.Format("{0}-12-{1}", Year, Term == 99 ? 31 : Term));
            BeginDate = EndDate.AddYears(-1).AddDays(1);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Year"></param>
        /// <param name="Month">1st half, the month = 1; 2nd half the month = 7</param>
        /// <param name="Term"></param>
        /// <param name="BeginDate"></param>
        /// <param name="EndDate"></param>
        private void CalculateHalfYearPeriod(decimal Year, decimal Month, decimal Term, ref DateTime BeginDate, ref DateTime EndDate)
        {
            int endMonth = 6;
            int endDate = 30;
            if (Month == 7)
            {
                endMonth = 12;
                endDate = 31;
            }
            
            EndDate = Convert.ToDateTime(String.Format("{0}-{1}-{2}", Year, endMonth, Term == 99 ? endDate : Term));
            BeginDate = EndDate.AddMonths(-6).AddDays(1);
        }

        private void CalculateQuarterPeriod(decimal Year, decimal Quarter, decimal Term, ref DateTime BeginDate, ref DateTime EndDate)
        {
            int endMonth;
            int endDate;
            switch ( (int)Quarter )
            {
                default:
                case 1:
                    endMonth = 3;
                    endDate = 31;
                    break;
                case 2:
                    endMonth = 6;
                    endDate = 30;
                    break;
                case 3:
                    endMonth = 9;
                    endDate = 30;
                    break;
                case 4:
                    endMonth = 12;
                    endDate = 31;
                    break;
            }
            EndDate = Convert.ToDateTime(String.Format("{0}-{1}-{2}", Year, endMonth, Term == 99 ? endDate : Term));
            BeginDate = EndDate.AddMonths(-3).AddDays(1);
        }

        private void CalculateMonthPeriod(decimal Year, decimal Month, decimal Term, ref DateTime BeginDate, ref DateTime EndDate)
        {
            int endDate = DateTime.DaysInMonth((int)Year, (int)Month);
            EndDate = Convert.ToDateTime(String.Format("{0}-{1}-{2}", Year, Month, Term == 99 ? endDate : Term));
            BeginDate = EndDate.AddMonths(-1).AddDays(1);
        }


        public List<SelectedModel> GetCalendarYears()
        {
            List<SelectedModel> _list = new List<SelectedModel>();

            List<string>  _listYear=   AS_GL_Calendar_Records.Find(
                x => x.GL_Term == "Y").OrderByDescending(x=>x.Years).Select(x=>x.Years.ToString()).Distinct().ToList();
            foreach (string _year in _listYear) {
                _list.Add(new SelectedModel
                {
                    Value = _year,
                    Name = _year
                });
            }
            return _list;
        }
    }
}