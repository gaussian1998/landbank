﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingCalendar.Models;
using Library.Servant.Servant.Common.Models;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.AccountingCalendar
{
    public interface IAccountingCalendarServant
    {
        IndexResult Index(SearchModel Search);
        DetailModel Detail(int ID);
        MessageModel Create(DetailModel Create);
        MessageModel Update(DetailModel Update);
        MessageModel BatchDelete(BatchDeleteModel BatchDelete);
        MessageModel Copy(CopyModel Copy);
        List<SelectedModel> GetCalendarYears();
    }
}
