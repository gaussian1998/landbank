﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Overdue.OverdueModels;

namespace Library.Servant.Servant.Overdue
{
    public interface IOverdueServant
    {
        IndexModel Index(SearchModel condition);
    }
}
