﻿using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalEventListener;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Servant.Overdue.OverdueEventListener
{
    public class OverdueEventListener : ExampleListener, IHttpGetProvider
    {
        public HttpGetViewModel GetUrl(string FormNo, string carryData)
        {
            return new HttpGetViewModel { ControllerName = "Overdue", ActionName = "Edit", Parameter = "id=" + carryData + "&signmode=Y" };
        }

        public override bool OnFinish(string formNo, string carry, AS_LandBankEntities context)
        {
            bool result = true;
            return result;
        }
    }
}
