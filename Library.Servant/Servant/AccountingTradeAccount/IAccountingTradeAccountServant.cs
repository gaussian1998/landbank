﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingTradeAccount.Models;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.AccountingTradeAccount
{
    public interface IAccountingTradeAccountServant
    {
        MessageModel Create(DetailModel Create);
        MessageModel Update(DetailModel Update);
        MessageModel BatchDelete(BatchDeleteModel BatchDelete);
        IndexResult Index(SearchModel Search);
        DetailModel Detail(int ID);
    }
}
