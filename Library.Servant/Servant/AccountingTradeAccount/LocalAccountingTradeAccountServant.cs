﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingTradeAccount.Models;
using Library.Servant.Servant.Common.Models;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.AccountingTradeAccount
{
    public class LocalAccountingTradeAccountServant : IAccountingTradeAccountServant
    {
        private IAS_GL_Trade_AccountRepository _repo = RepositoryHelper.GetAS_GL_Trade_AccountRepository();

        public MessageModel BatchDelete(BatchDeleteModel BatchDelete)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            foreach (var id in BatchDelete.IDList)
            {
                var item = _repo.All().FirstOrDefault(x => x.ID == id);
                if (item != null)
                {
                    AS_GL_Trade_Acc_Sub_Records.Delete(x => x.Master_No == item.Master_No && x.Related_No == item.Related_No);

                    AS_GL_Trade_Account_Records.Delete(x => x.ID == id);
                }
            }
            return message;
        }

        public MessageModel Create(DetailModel Create)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            if (AS_GL_Trade_Account_Records.Any(x => x.Master_No == Create.MasterNo && x.Related_No == Create.RelatedNo))
            {
                message.IsSuccess = false;
                message.MessagCode = MessageCode.RelationNoExisted.ToString();
            }
            else
            {
                AS_GL_Trade_Account item = new AS_GL_Trade_Account
                {
                    Account_Key = "",
                    Master_No = Create.MasterNo,
                    Related_No = Create.RelatedNo,
                    Account = Create.Account,
                    DB_CR = Create.DBCR,
                    Asset_Category_code = Create.AssetCategoryCode,
                    Post_Sub_Account = Create.PostSubAccount,
                    Is_Sub_Account = Create.IsSubAccount
                };

                AS_GL_Trade_Account_Records.Create(item);
            }
            
            return message;
        }

        public MessageModel Update(DetailModel Update)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            AS_GL_Trade_Account_Records.Update(x => x.ID == Update.ID, m => {
                m.Master_No = Update.MasterNo;
                m.Related_No = Update.RelatedNo;
                m.Account = Update.Account;
                m.DB_CR = Update.DBCR;
                m.Asset_Category_code = Update.AssetCategoryCode;
                m.Post_Sub_Account = Update.PostSubAccount;
                m.Is_Sub_Account = Update.IsSubAccount;
            });
            return message;
        }

        public DetailModel Detail(int ID)
        {
            DetailModel result = AS_GL_Trade_Account_Records.First(x => x.ID == ID, m => new DetailModel {
                ID = m.ID,
                AccountKey = m.Account_Key,
                MasterNo = (int)m.Master_No,
                RelatedNo = (int)m.Related_No,
                Account = m.Account,
                DBCR = m.DB_CR,
                AssetCategoryCode = m.Asset_Category_code,
                PostSubAccount = m.Post_Sub_Account,
                IsSubAccount = m.Is_Sub_Account
            });

            IAS_GL_Trade_DefineRepository _repoT = RepositoryHelper.GetAS_GL_Trade_DefineRepository();
            AS_GL_Trade_Define td = _repoT.All().FirstOrDefault(x => x.Master_No == result.MasterNo);
            if (td != null)
            {
                result.MasterName = td.Master_Text;
            }
            IAS_GL_AccountRepository _repo = RepositoryHelper.GetAS_GL_AccountRepository();
            AS_GL_Account acc = _repo.All().FirstOrDefault(x => x.Account == result.Account);
            if (acc != null)
            {
                result.AccountName = acc.Account_Name;
                result.IFRSAccount = acc.IFRS_Account;
            }
            return result;
        }

        public IndexResult Index(SearchModel Search)
        {
            IndexResult result = new IndexResult {
                Conditions = Search,
                Page = AS_GL_Trade_Account_Records.Page(
                    Search.CurrentPage,
                    Search.PageSize,
                    x => x.Master_No,
                    x => (x.Master_No == Search.TradeNo),
                    m => new DetailModel
                    {
                        ID = m.ID,
                        AccountKey = m.Account_Key,
                        MasterNo = (int)m.Master_No,
                        RelatedNo = (int)m.Related_No,
                        Account = m.Account,
                        DBCR = m.DB_CR,
                        AssetCategoryCode = m.Asset_Category_code,
                        PostSubAccount = m.Post_Sub_Account,
                        IsSubAccount = m.Is_Sub_Account
                    }
                    )
            };

            foreach (var item in result.Page.Items)
            {
                IAS_GL_Trade_DefineRepository _repoT = RepositoryHelper.GetAS_GL_Trade_DefineRepository();
                AS_GL_Trade_Define td = _repoT.All().FirstOrDefault(x => x.Master_No == item.MasterNo);
                if (td != null)
                {
                    item.MasterName = td.Master_Text;
                }
                IAS_GL_AccountRepository _repo = RepositoryHelper.GetAS_GL_AccountRepository();
                AS_GL_Account acc = _repo.All().FirstOrDefault(x => x.Account == item.Account);
                if(acc != null)
                {
                    item.AccountName = acc.Account_Name;
                    item.IFRSAccount = acc.IFRS_Account;
                }
            }

            return result;
        }
    }
}
