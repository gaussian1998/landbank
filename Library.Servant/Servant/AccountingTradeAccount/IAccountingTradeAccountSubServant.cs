﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingTradeAccount.Models;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.AccountingTradeAccount
{
    public interface IAccountingTradeAccountSubServant
    {
        MessageModel Create(SubDetailModel Create);
        MessageModel Update(SubDetailModel Update);
        MessageModel BatchDelete(BatchDeleteModel BatchDelete);
        SubIndexResult Index(SubQueryModel Query);
        SubDetailModel Detail(int ID);
    }
}
