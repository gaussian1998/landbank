﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingTradeAccount.Models;
using Library.Servant.Servant.Common.Models;
using Library.Entity.Edmx;

namespace Library.Servant.Servant.AccountingTradeAccount
{
    public class LocalAccountingTradeAccountSubServant : IAccountingTradeAccountSubServant
    {
        public MessageModel BatchDelete(BatchDeleteModel BatchDelete)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            foreach (int id in BatchDelete.IDList)
            {
                AS_GL_Trade_Acc_Sub_Records.Delete(x => x.ID == id);
            }
            return message;
        }

        public MessageModel Create(SubDetailModel Create)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            if (AS_GL_Account_Records.Any(x => x.Account == Create.Account))
            {
                if (AS_GL_Trade_Acc_Sub_Records.Any(x => x.Master_No == Create.MasterNo && x.Related_No == Create.RelatedNo && x.Account == Create.Account))
                {
                    message.IsSuccess = false;
                    message.MessagCode = MessageCode.AccountSujectExisted.ToString();
                }
                else
                {
                    AS_GL_Trade_Acc_Sub item = new AS_GL_Trade_Acc_Sub
                    {
                        Master_No = Create.MasterNo,
                        Related_No = Create.RelatedNo,
                        Account = Create.Account,
                        DB_CR = Create.DBCR,
                        Asset_Category_code = Create.AssetCategoryCode
                    };
                    AS_GL_Trade_Acc_Sub_Records.Create(item);
                }
            }
            else
            {
                message.IsSuccess = false;
                message.MessagCode = MessageCode.AccountSubjectNotFund.ToString();
            }
            
            return message;
        }

        public MessageModel Update(SubDetailModel Update)
        {
            MessageModel message = new MessageModel { IsSuccess = true, MessagCode = "" };
            if (AS_GL_Account_Records.Any(x => x.Account == Update.Account))
            {
                if (AS_GL_Trade_Acc_Sub_Records.Any(x => x.Master_No == Update.MasterNo && x.Related_No == Update.RelatedNo && x.Account == Update.Account))
                {
                    message.IsSuccess = false;
                    message.MessagCode = MessageCode.AccountSujectExisted.ToString();
                }
                else
                {
                    AS_GL_Trade_Acc_Sub_Records.Update(x => x.ID == Update.ID, m => {
                        m.Master_No = Update.MasterNo;
                        m.Related_No = Update.RelatedNo;
                        m.Account = Update.Account;
                        m.DB_CR = Update.DBCR;
                        m.Asset_Category_code = Update.AssetCategoryCode;
                    });
                }
            }
            else
            {
                message.IsSuccess = false;
                message.MessagCode = MessageCode.AccountSubjectNotFund.ToString();
            }
            return message;
        }

        public SubDetailModel Detail(int ID)
        {
            SubDetailModel result = AS_GL_Trade_Acc_Sub_Records.First(x => x.ID == ID, m => new SubDetailModel
            {
                ID = m.ID,
                MasterNo = (int)m.Master_No,
                RelatedNo = (int)m.Related_No,
                Account = m.Account,
                DBCR = m.DB_CR,
                AssetCategoryCode = m.Asset_Category_code
            });

            AS_GL_Account acc = AS_GL_Account_Records.First(x => x.Account == result.Account);
            result.AccountName = acc.Account_Name;
            result.IFRSAccount = acc.IFRS_Account;
            return result;
        }

        public SubIndexResult Index(SubQueryModel Query)
        {
            decimal mNo = Convert.ToDecimal(Query.MasterNo);
            decimal rNo = Convert.ToDecimal(Query.RelatedNo);

            SubIndexResult result = new SubIndexResult
            {
                Conditions = Query,
                Page = AS_GL_Trade_Acc_Sub_Records.Page(
                    Query.CurrentPage,
                    Query.PageSize,
                    x => x.Account,
                    x => x.Master_No == mNo && x.Related_No == rNo,
                    m => new SubDetailModel {
                        ID = m.ID,
                        MasterNo =(int)m.Master_No,
                        RelatedNo = (int)m.Related_No,
                        Account = m.Account,
                        DBCR = m.DB_CR,
                        AssetCategoryCode = m.Asset_Category_code
                    }
                    )
            };

            foreach (var item in result.Page.Items)
            {
                AS_GL_Account acc = AS_GL_Account_Records.First(x => x.Account == item.Account);
                item.AccountName = acc.Account_Name;
                item.IFRSAccount = acc.IFRS_Account;
            }
            return result;
        }
    }
}
