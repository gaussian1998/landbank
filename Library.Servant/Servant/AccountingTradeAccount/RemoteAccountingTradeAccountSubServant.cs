﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.AccountingTradeAccount.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Servant.AccountingTradeAccount
{
    public class RemoteAccountingTradeAccountSubServant : IAccountingTradeAccountSubServant
    {
        public MessageModel BatchDelete(BatchDeleteModel BatchDelete)
        {
            return RemoteServant.Post<BatchDeleteModel, MessageModel>(
                            BatchDelete,
                            "/AccountingTradeAccountSubServant/BatchDelete"
           );
        }

        public MessageModel Create(SubDetailModel Create)
        {
            return RemoteServant.Post<SubDetailModel, MessageModel>(
                            Create,
                            "/AccountingTradeAccountSubServant/Create"
           );
        }

        public SubDetailModel Detail(int ID)
        {
            return RemoteServant.Post<IntModel, SubDetailModel>(
                            new IntModel { Value=ID},
                            "/AccountingTradeAccountSubServant/Detail"
           );
        }

        public SubIndexResult Index(SubQueryModel Query)
        {
            return RemoteServant.Post<SubQueryModel, SubIndexResult>(
                            Query,
                            "/AccountingTradeAccountSubServant/Index"
           );
        }

        public MessageModel Update(SubDetailModel Update)
        {
            return RemoteServant.Post<SubDetailModel, MessageModel>(
                            Update,
                            "/AccountingTradeAccountSubServant/Update"
           );
        }
    }
}
