﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AccountingTradeAccount.Models
{
    public class SearchModel : InvasionEncryption
    {
        public string TransType { get; set; }
        public string TradeType { get; set; }
        public decimal TradeNo { get; set; }
        public string TradeName { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
    }
}
