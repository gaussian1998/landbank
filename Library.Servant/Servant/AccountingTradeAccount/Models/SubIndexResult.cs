﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;
using Library.Common.Models;

namespace Library.Servant.Servant.AccountingTradeAccount.Models
{
    public class SubIndexResult : InvasionEncryption
    {
        public SubQueryModel Conditions { get; set; }
        public PageList<SubDetailModel> Page { get; set; }
    }
}
