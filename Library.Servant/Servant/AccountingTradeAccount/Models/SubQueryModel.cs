﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AccountingTradeAccount.Models
{
    public class SubQueryModel : InvasionEncryption
    {
        public string MasterNo { get; set; }
        public string RelatedNo { get; set; }
        public string MasterName { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
    }
}
