﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.AccountingTradeAccount.Models
{
    public class SubDetailModel : InvasionEncryption
    {
        public int ID { get; set; }
        public int MasterNo { get; set; }
        public int RelatedNo { get; set; }
        public string Account { get; set; }
        public string AccountName { get; set; }
        public string IFRSAccount { get; set; }
        public string DBCR { get; set; }
        public string AssetCategoryCode { get; set; }
    }
}
