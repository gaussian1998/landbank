﻿using System.Linq;
using Library.Servant.Servant.UserConfig.Models;
using System;
using Library.Servant.Servant.Common;
using System.Transactions;
using Library.Entity.Edmx;
using Library.Servant.Servant.UserConfig;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Enums;

namespace Library.Servant.Servant.UserConfig
{
    public class LocalUserConfigServant : IUserConfigServant
    {
        public string GetUserName(string userCode)
        {
            try {
                return AS_Users_Records.First(m => m.User_Code == userCode).User_Name;
            }
            catch (Exception ex) {
                return string.Empty;
            }
        }

        public UserInfoModel Index(int userID)
        {
            try {
                var departmentID = CodeTableClass.Department.GetClassID();
                var branchID = CodeTableClass.Branch.GetClassID();
                var departmentInfo = AS_Code_Table_Records.Find(m => m.Class == departmentID, entity => entity).ToDictionary(k => k.Code_ID, v => v.Text);
                var branchInfo = AS_Code_Table_Records.Find(m => m.Class == branchID, entity => entity).ToDictionary(k => k.Code_ID, v => v.Text);

                var userInfo = AS_Users_Records.First(m => m.ID == userID);
                var deputyInfo = userInfo.AS_Users_Deputy.OrderByDescending(m => m.Start_Time).FirstOrDefault();

                return new UserConfig.Models.UserInfoModel
                {
                    UserCode = userInfo.User_Code,
                    UserName = userInfo.User_Name,
                    BranchName = "",
                    Department = "",
                    Email = userInfo.Email,
                    IsEnable = userInfo.Is_Active == true ? true : false,
                    //IsManager =  ,
                    Remark = userInfo.Notes,
                    //Title = userInfo
                    DeputyRecordID = deputyInfo?.ID,
                    DeputyID = deputyInfo?.AS_Users1.User_Code,
                    DeputyName = deputyInfo?.AS_Users1.User_Name,
                    StartDeputy = deputyInfo?.Start_Time,
                    EndDeputy = deputyInfo?.End_Time,
                    TotalRoles = userInfo.AS_Users_Role.Where(m => m.Is_Enable).Select(m => new OptionModel<string, string>
                    {
                        Text = m.AS_Role.Role_Name,
                        Value = m.AS_Role.Role_Code
                    }).ToList()
                };
            }
            catch (Exception ex) {
                throw new Exception("To get user's information fail", ex);
            }
        }

        public VoidResult Update(UserConfig.Models.UserInfoModel model)
        {
            using (var trans = new TransactionScope()) {
                AS_Users_Records.Update(m => m.User_Code == model.UserCode, entity => {

                    entity.User_Name = model.UserName;
                    //todo_julius: the branch and department infromation
                    entity.Is_Active = model.IsEnable;
                    entity.Email = model.Email;
                    entity.Notes = model.Remark;
                    entity.Last_Updated_By = entity.ID; //todo_julius
                    entity.Last_Updated_Time = DateTime.Now;
                });
                
                if (!string.IsNullOrEmpty(model.DeputyID)) {
                    var userInfo = AS_Users_Records.First(m => m.User_Code == model.UserCode);
                    var deputyInfo = AS_Users_Records.First(m => m.User_Code == model.DeputyID);

                    if (model.DeputyRecordID != null) {
                        AS_Users_Deputy_Records.Update(m => m.ID == model.DeputyRecordID, entity => {
                            entity.User_ID = userInfo.ID;
                            entity.Deputy_User_ID = deputyInfo.ID;
                            entity.Start_Time = model.StartDeputy;
                            entity.End_Time = model.EndDeputy;
                            entity.Last_Updated_Time = DateTime.Now; //todo_julius
                            entity.Last_Updated_By = userInfo.ID; //todo_julius
                        });
                    }
                    else {
                        AS_Users_Deputy_Records.Create(new AS_Users_Deputy
                        {
                            User_ID = userInfo.ID,
                            Deputy_User_ID = deputyInfo.ID,
                            Start_Time = model.StartDeputy,
                            End_Time = model.EndDeputy,
                            Is_Active = false,
                            Last_Updated_Time = DateTime.Now,
                            Last_Updated_By = userInfo.ID
                        });
                    }
                }

                trans.Complete();
            }

            return new VoidResult();
        }


    }
}
