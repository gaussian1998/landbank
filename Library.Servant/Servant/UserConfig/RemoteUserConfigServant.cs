﻿using System;
using Library.Servant.Common;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.UserConfig.Models;


namespace Library.Servant.Servant.UserConfig
{
    public class RemoteUserConfigServant : IUserConfigServant
    {
        public string GetUserName(string userCode)
        {
            var result = ProtocolService.EncryptionPost<StringModel, StringModel>
            (
                new StringModel { Value= userCode },
                Config.ServantDomain + "/UserConfigServant/Index"
            );

            return Tools.ExceptionConvert(result).Value;
        }

        public UserInfoModel Index(int userID)
        {
            var result = ProtocolService.EncryptionPost<IntModel, UserInfoModel>
            (
                new IntModel { Value = userID },
                Config.ServantDomain + "/UserConfigServant/Index"
            );

            return Tools.ExceptionConvert(result);
        }

        public VoidResult Update(UserInfoModel model)
        {
            var result = ProtocolService.EncryptionPost<UserInfoModel, VoidResult>
            (
                model,
                Config.ServantDomain + "/UserConfigServant/Index"
            );

            return Tools.ExceptionConvert(result);
        }
    }
}
