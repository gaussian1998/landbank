﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.UserConfig.Models;

namespace Library.Servant.Servant.UserConfig
{
    public interface IUserConfigServant
    {
        UserInfoModel Index(int userID);
        VoidResult Update(UserInfoModel model);
        string GetUserName(string userCode);
    }
}
