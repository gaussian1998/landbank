﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.UserConfig.Models
{
    public class UserInfoModel : InvasionEncryption
    {
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string BranchName { get; set; }
        public string Department { get; set; }
        public bool IsEnable { get; set; }
        public bool IsManager { get; set; }
        public string Title { get; set; }
        public DateTime LeaveDate { get; set; }
        public string Email { get; set; }
        public string Remark { get; set; }
        public int? DeputyRecordID { get; set; }
        public string DeputyID { get; set; }
        public string DeputyName { get; set; }
        public Nullable<DateTime> StartDeputy { get; set; }
        public Nullable<DateTime> EndDeputy { get; set; }
        public List<OptionModel<string, string>> TotalRoles { get; set; }        
    }
}
