﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Servant.ApprovalRole.Models
{
    public class UpdateModel : InvasionEncryption
    {
        public int ID { get; set; }
        public string CodeID { get; set; }
        public string Text { get; set; }
        public bool IsActive { get; set; }
        public bool CancelCode { get; set; }
        public decimal? Value1 { get; set; }
        public decimal? Value2 { get; set; }
        public string Remark { get; set; }
        public string Parameter1 { get; set; }  
        public List<int> SelectedUsers { get; set; }
    }
}
