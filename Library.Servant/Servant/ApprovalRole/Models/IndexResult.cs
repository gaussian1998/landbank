﻿using Library.Common.Models;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace Library.Servant.Servant.ApprovalRole.Models
{
    public class IndexResult : InvasionEncryption
    {
        public PageList<DetailsResult> Page { get; set; }
        public List<OptionModel<string, string>> ActiveOptions { get; set; }
        public List<OptionModel<string, string>> CancelOptions { get; set; }
    }
}
