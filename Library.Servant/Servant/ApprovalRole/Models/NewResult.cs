﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.ApprovalRole.Models
{
    public class NewResult : AbstractEncryptionDTO
    {
        public string ClassName { get; set; }
        public bool IsActive { get; set; }
        public bool CancelCode { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
