﻿using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common.Models.Mapper;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Library.Servant.Servant.ApprovalRole.Models
{
    public class DetailsResult : CodeTableModel
    {

        public string ClassName
        {
            get
            {
                return Class_Name;
            }
            set
            {
                Class_Name = value;
            }
        }

        public string CodeID
        {
            get
            {
                return Code_ID;
            }
            set
            {
                Code_ID = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return Is_Active;
            }
            set
            {
                Is_Active = true;
            }
        }

        public bool CancelCode
        {
            get
            {
                return Cancel_Code;
            }
            set
            {
                Cancel_Code = value;
            }
        }
        
        public DateTime? LastUpdatedTime
        {
            get
            {
                return Last_Updated_Time;
            }
            set
            {
                Last_Updated_Time = value;
            }
        }
        public string OperationType
        {
            get
            {
                return Parameter1;
            }
            set
            {
                Parameter1 = value;
            }
        }
        public string OperationTypeName
        {
            get;set;
        }
        public List<OptionModel<int, string>> SelectedUsersOptions { get; set; }
        public List<int> SelectedUsers { get; set; }
    }
}
