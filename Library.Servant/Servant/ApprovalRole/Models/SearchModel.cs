﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Servant.ApprovalRole.Models
{
    public class SearchModel : AbstractEncryptionDTO
    {
        public string CodeID { get; set; }
        public bool? IsActive { get; set; }
        public bool? CancelCode { get; set; }
        public string OperationType { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
