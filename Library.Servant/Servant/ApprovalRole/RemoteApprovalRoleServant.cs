﻿using Library.Servant.Servant.ApprovalRole.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;


namespace Library.Servant.Servant.ApprovalRole
{
    public class RemoteApprovalRoleServant : IApprovalRoleServant
    {
        public NewResult New()
        {
            return RemoteServant.Post<VoidModel, NewResult>(

                new VoidModel { },
                "/ApprovalRoleServant/New"
            );
        }

        public IndexResult Index(SearchModel search)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(

                search,
                "/ApprovalRoleServant/Index"
            );
        }

        public VoidResult Create(CreateModel vo)
        {
            return RemoteServant.Post<CreateModel, VoidResult>(

                vo,
                "/ApprovalRoleServant/Create"
            );
        }

        public DetailsResult Details(int ID)
        {
            return RemoteServant.Post<IntModel, DetailsResult>(

                new IntModel { Value = ID },
                "/ApprovalRoleServant/Details"
            );
        }

        public VoidResult Update(UpdateModel vo)
        {
            return RemoteServant.Post<UpdateModel, VoidResult>(

                vo,
               "/ApprovalRoleServant/Update"
            );
        }
        public ListOptionModel<int, string> GetUserId(string branchCode)
        {
            return RemoteServant.Post<StringModel, ListOptionModel<int, string>>(
                new Common.Models.StringModel { Value= branchCode },
               "/ApprovalRoleServant/GetUserId"
            );
        }
        public bool CheckCodeId(UpdateModel vo)
        {
            return (RemoteServant.Post<UpdateModel, BoolResult>(
                vo,
               "/ApprovalRoleServant/CheckCodeId"
            )).Value;
        }
    }
}
