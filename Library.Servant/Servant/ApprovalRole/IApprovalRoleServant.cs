﻿using Library.Servant.Servant.ApprovalRole.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.ApprovalRole
{
    public interface IApprovalRoleServant
    {
        NewResult New();
        IndexResult Index(SearchModel search);
        DetailsResult Details(int ID);
        VoidResult Create(CreateModel vo);
        VoidResult Update(UpdateModel vo);
        ListOptionModel<int, string> GetUserId(string branchCode);
        bool CheckCodeId(UpdateModel updateModel);
    }
}
