﻿using System.Linq;
using System.Collections.Generic;
using Library.Common.Models;
using Library.Entity.Edmx;
using Library.Servant.Servant.ApprovalRole.Models;
using Library.Servant.Servant.CodeTable;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System;

namespace Library.Servant.Servant.ApprovalRole
{
    public class LocalApprovalRoleServant : IApprovalRoleServant
    {
        private IStaticDataServant _staticDataServant = new LocalStaticDataServant();
        public NewResult New()
        {
            return new NewResult
            {
                ClassName = ClassName,
                IsActive = true,
                CancelCode = false
            };
        }

        public IndexResult Index(SearchModel search)
        {
            PageList<DetailsResult> _listDetailsResult = Search(search);
            List<SelectedModel> _listdoctypes = _staticDataServant.GetDocTypes();
            foreach (DetailsResult _detailsResult in _listDetailsResult.Items) {
                if (_detailsResult.Parameter1 != null && _detailsResult.Parameter1 != "" && _detailsResult.CodeID.Substring(0,1)=="R") {
                    _detailsResult.OperationTypeName = _listdoctypes.Where(x => x.Value == _detailsResult.Parameter1).FirstOrDefault().Name;
                }
            }
            return new IndexResult {
                Page = _listDetailsResult,
                ActiveOptions = LocalCodeTableServant.IsAliveOptions(),
                CancelOptions = LocalCodeTableServant.CancelOptions()
            };
        }
        
        public DetailsResult Details(int ID)
        {
            DetailsResult _detailResult = AS_Code_Table_Records.First<DetailsResult>(m => m.ID == ID);
            //取得設定此角色人員
            List<AS_Users_FlowRole> _listFlowRole = AS_Users_FlowRole_Records.Find(x => x.FlowRoleID == _detailResult.ID);
            _detailResult.SelectedUsersOptions = new List<OptionModel<int, string>>();
            if (_listFlowRole != null && _listFlowRole.Count > 0) {
                _detailResult.SelectedUsersOptions=this.GetSelectedUserId(_listFlowRole.Select(x => x.UserID).ToList()).ListOptionModelItems;
            }
            return _detailResult;
        }

        public VoidResult Create(CreateModel vo)
        {
            AS_Code_Table_Records.Create( entity => {

                entity.Class = Class;
                entity.Class_Name = ClassName;
                entity.Code_ID = vo.CodeID;
                entity.Is_Active = vo.IsActive;
                entity.Cancel_Code = vo.CancelCode;
                entity.Value1 = vo.Value1;
                entity.Value2 = vo.Value2;
                entity.Text = vo.Text;
                entity.Remark = vo.Remark;
                entity.Parameter1 = vo.Parameter1;
            });
            AS_Code_Table _asCodeTable = AS_Code_Table_Records.Find(x => x.Class == Class && x.Code_ID== vo.CodeID).FirstOrDefault();
            DateTime _now = DateTime.Now;
            if (vo.SelectedUsers != null && vo.SelectedUsers.Count > 0)
            {
                foreach (int _userID in vo.SelectedUsers)
                {
                    AS_Users_FlowRole_Records.Create(entity =>
                    {
                        entity.UserID = _userID;
                        entity.LastUpdatedTime = _now;
                        entity.FlowRoleID = _asCodeTable.ID;
                    });
                }
            }
           
            return new VoidResult();
        }

        public VoidResult Update(UpdateModel vo)
        {
            AS_Code_Table_Records.Update( m => m.ID == vo.ID, entity => {

                entity.Code_ID = vo.CodeID;
                entity.Text = vo.Text;
                entity.Is_Active = vo.IsActive;
                entity.Cancel_Code = vo.CancelCode;
                entity.Value1 = vo.Value1;
                entity.Value2 = vo.Value2;
                entity.Remark = vo.Remark;
                entity.Parameter1 = vo.Parameter1;
            }  );
            DateTime _now = DateTime.Now;
            AS_Users_FlowRole_Records.Delete(m => m.FlowRoleID == vo.ID);
            if (vo.SelectedUsers != null && vo.SelectedUsers.Count > 0)
            {
                foreach (int _userID in vo.SelectedUsers)
                {
                    AS_Users_FlowRole_Records.Create(entity =>
                    {
                        entity.UserID = _userID;
                        entity.LastUpdatedTime = _now;
                        entity.FlowRoleID = vo.ID;
                    });
                }
            }
            return new VoidResult();
        }

        public static List<OptionModel<int, string>> Options()
        {
            return AS_Code_Table_Records.Find(m => m.Class == Class).Select(m => new OptionModel<int, string> { Value = m.ID, Text = m.Text }).ToList();
        }

        private static PageList<DetailsResult> Search(SearchModel search)
        {
            bool skipCodeID = string.IsNullOrWhiteSpace(search.CodeID);
            bool skipIsActive = !search.IsActive.HasValue;
            bool skipCancelCode = !search.CancelCode.HasValue;
            bool skipOperationType = string.IsNullOrWhiteSpace(search.OperationType);

            return AS_Code_Table_Records.Page<DetailsResult,int>(
                
                search.Page, 
                search.PageSize, 
                m => m.ID, 
                m =>   
                Class == m.Class &&
                (skipCodeID || search.CodeID == m.Code_ID) &&
                (skipIsActive || search.IsActive == m.Is_Active) &&
                (skipCancelCode || search.CancelCode == m.Cancel_Code) &&
                (skipOperationType|| search.OperationType==m.Parameter1)
            );
        }
        private ListOptionModel<int, string> GetSelectedUserId(List<int> selectedUserId)
        {
            List<AS_Users> _listUsers = AS_Users_Records.Find(x => selectedUserId.Contains(x.ID));
            ListOptionModel<int, string> _list = new ListOptionModel<int, string>();
            _list.ListOptionModelItems = new List<OptionModel<int, string>>();
            _list.ListOptionModelItems = _listUsers.Select(entity => new OptionModel<int, string>
            {
                Value = entity.ID,
                Text = entity.User_Name
            }).ToList();
            return _list;
        }
        public ListOptionModel<int, string> GetUserId(string branchCode)
        {
            List<AS_Users> _listUsers=AS_Users_Records.Find(x => x.Branch_Code == branchCode);
            ListOptionModel<int, string> _list = new ListOptionModel<int, string>();
            _list.ListOptionModelItems = new List<OptionModel<int, string>>();
            _list.ListOptionModelItems = _listUsers.Select(entity => new OptionModel<int, string>
            {
                Value = entity.ID,
                Text = entity.User_Name
            }).ToList();
            return _list;
        }
        public bool CheckCodeId(UpdateModel updateModel) {
            bool _pass = true;
            List<AS_Code_Table> _listASCodeTable = new List<AS_Code_Table>();
            _listASCodeTable = AS_Code_Table_Records.Find(x => x.Class == Class && x.Code_ID == updateModel.CodeID);
            if (updateModel.ID > 0) _listASCodeTable = _listASCodeTable.Where(x => x.ID != updateModel.ID).ToList();
            if (_listASCodeTable != null && _listASCodeTable.Count >0) {
                _pass = false;
            }
            return _pass;
        }
        private static readonly string Class = "R01";
        private static readonly string ClassName = "簽核角色";
    }
}
