﻿using System;
using System.Linq;
using System.Collections.Generic;
using Library.Servant.Servant.Common.Models.Mapper;
using Library.Entity.Edmx;


namespace Library.Servant.Servant.WorkFlow.Models
{
    public class DetailsResult : FlowNameModel
    {
        public ICollection<AS_Flow_Template> AS_Flow_Template
        {
            set
            {
                FlowRoles = new List<string> { };
                if ( value != null )
                {
                    var newest_flow = value.FirstOrDefault(m => m.Version == value.Max(e => e.Version));
                    if (newest_flow != null)
                        FlowRoles = newest_flow.AS_Flow_Template_Flow_Role.Select(m => m.AS_Code_Table.Text).ToList();
                }
            }
        }
        public List<string> FlowRoles {  get;  set;  }

        public string FlowCode
        {
            get
            {
                return Flow_Code;
            }
            set
            {
                Flow_Code = value;
            }
        }

        public string FlowType
        {
            get
            {
                return Flow_Type;
            }
            set
            {
                Flow_Type = value;
            }
        }

        public string FlowName
        {
            get
            {
                return Flow_Name;
            }
            set
            {
                Flow_Name = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return Is_Active;
            }
            set
            {
                Is_Active = value;
            }
        }

        public bool CancelCode
        {
            get
            {
                return Cancel_Code;
            }
            set
            {
                Cancel_Code = value;
            }
        }

        public DateTime? LastUpdatedTime
        {
            get
            {
                return Last_Updated_Time;
            }
            set
            {
                Last_Updated_Time = value;
            }
        }

        public decimal? LastUpdatedBy
        {
            get
            {
                return Last_Updated_By;
            }
            set
            {
                Last_Updated_By = value;
            }
        }
        public string OperationTypeName { get; set; }
    }
}
