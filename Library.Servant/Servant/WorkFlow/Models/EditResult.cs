﻿using System.Collections.Generic;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common.Models.Mapper;
using Library.Entity.Edmx;
using System.Linq;

namespace Library.Servant.Servant.WorkFlow.Models
{
    public class EditResult : FlowNameModel
    {
        public ICollection<AS_Flow_Template> AS_Flow_Template
        {
            set
            {
                Roles = new List<int> { };
                if (value != null  && value.Any())
                {
                    Version = value.Max(e => e.Version);
                    var newest_flow = value.FirstOrDefault(m => m.Version == Version);
                    if (newest_flow.AS_Flow_Template_Flow_Role != null)
                        Roles = newest_flow.AS_Flow_Template_Flow_Role.Select(m => m.Flow_Role_ID).ToList();
                }
            }
        }
        public decimal? Version { get; set; }
        public List<int> Roles { get; set; }
        public List<OptionModel<int, string>> CadidateFlowRoles { get; set; }

        public string FlowCode
        {
            get
            {
                return Flow_Code;
            }
            set
            {
                Flow_Code = value;
            }
        }

        public string FlowType
        {
            get
            {
                return Flow_Type;
            }
            set
            {
                Flow_Type = value;
            }
        }

        public string FlowName
        {
            get
            {
                return Flow_Name;
            }
            set
            {
                Flow_Name = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return Is_Active;
            }
            set
            {
                Is_Active = value;
            }
        }

        public bool CancelCode
        {
            get
            {
                return Cancel_Code;
            }
            set
            {
                Cancel_Code = value;
            }
        }
    }
}
