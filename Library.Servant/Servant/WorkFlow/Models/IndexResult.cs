﻿using Library.Common.Models;
using Library.Servant.Communicate;

namespace Library.Servant.Servant.WorkFlow.Models
{
    public class IndexResult : InvasionEncryption
    {
        public PageList<DetailsResult> Page { get; set; }
    }
}
