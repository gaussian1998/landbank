﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Servant.WorkFlow.Models
{
    public class NewResult : AbstractEncryptionDTO
    {
        public bool IsActive { get; set; }
        public bool CancelCode { get; set; }
        public List<OptionModel<int,string>> CadidateFlowRoles { get; set; }
        public decimal Version { get; set; }

        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
