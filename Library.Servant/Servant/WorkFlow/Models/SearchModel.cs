﻿using Library.Servant.Communicate;

namespace Library.Servant.Servant.WorkFlow.Models
{
    public class SearchModel : InvasionEncryption
    {
        public int FlowID { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string FlowCode { get; set; }
        public bool? IsActive { get; set; }
        public bool? CancelCode { get; set; }
        public string OperationType { get; set; }
    }
}
