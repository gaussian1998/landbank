﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Servant.WorkFlow.Models
{
    public class UpdateModel : AbstractEncryptionDTO
    {
        public int ID { get; set; }
        public string FlowCode { get; set; }
        public string FlowType { get; set; }
        public string FlowName { get; set; }
        public bool IsActive { get; set; }
        public bool CancelCode { get; set; }
        public string Remark { get; set; }
        public List<int> Roles { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authClient;
        }
    }
}
