﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Transactions;
using Library.Servant.Servant.ApprovalRole;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.WorkFlow.Models;
using Library.Entity.Edmx;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.WorkFlow
{
    public class LocalWorkFlowServant : IWorkFlowServant
    {
        private IStaticDataServant _staticDataServant = new LocalStaticDataServant();
        public NewResult New()
        {
            return new NewResult { IsActive = true, CancelCode = false, CadidateFlowRoles = LocalApprovalRoleServant.Options(), Version = 1 };
        }

        public IndexResult Index(SearchModel search)
        {
            bool skipFlowCode = string.IsNullOrWhiteSpace(search.FlowCode);
            bool skipIsActive = !search.IsActive.HasValue;
            bool skipCancelCode = !search.CancelCode.HasValue;
            bool skipOperationType= string.IsNullOrWhiteSpace(search.OperationType);
            bool skipID = search.FlowID > 0 ? false : true;
            IndexResult _indexResult = new IndexResult
            {
                Page = AS_Flow_Name_Records.Page<DetailsResult, int>(

                        search.Page,
                        search.PageSize,
                        m => m.ID,
                        m =>
                        (skipFlowCode || m.Flow_Code == search.FlowCode) &&
                        (skipIsActive || m.Is_Active == search.IsActive) &&
                        (skipCancelCode || m.Cancel_Code == search.CancelCode)&&
                        (skipOperationType || m.Flow_Type==search.OperationType)&&
                        (skipID || m.ID==search.FlowID)
                )
            };
            List<SelectedModel> _listdoctypes = _staticDataServant.GetDocTypes();
            List<DetailsResult> _listDetailsResult = new List<DetailsResult>();
            foreach (DetailsResult _detailsResult in _indexResult.Page.Items)
            {
                if (_detailsResult.FlowType != null && _detailsResult.FlowType != "")
                {
                    _detailsResult.OperationTypeName = _listdoctypes.Where(x => x.Value == _detailsResult.FlowType).FirstOrDefault().Name;
                }
                _listDetailsResult.Add(_detailsResult);
            }
            _indexResult.Page.Items = _listDetailsResult;
            return _indexResult;
        }

        public VoidResult Create(CreateModel vo)
        {
            AS_Flow_Name_Records.Create( entity => {

                entity.Flow_Code = vo.FlowCode;
                entity.Flow_Type = vo.FlowType;
                entity.Flow_Name = vo.FlowName;
                entity.Is_Active = vo.IsActive;
                entity.Cancel_Code = vo.CancelCode;
                entity.Remark = vo.Remark;
                Add(entity, vo.Roles);
            } );

            return new VoidResult();
        }

        public EditResult Details(int ID)
        {
            var result = AS_Flow_Name_Records.First<EditResult>(m => m.ID == ID);
            result.CadidateFlowRoles = LocalApprovalRoleServant.Options();

            return result;
        }

        public VoidResult Update(UpdateModel vo)
        {
            using (TransactionScope trans = new TransactionScope())
            {
                AS_Flow_Name_Records.Update(m => m.ID == vo.ID, entity => {

                    entity.Flow_Code = vo.FlowCode;
                    entity.Flow_Name = vo.FlowName;
                    entity.Flow_Type = vo.FlowType;
                    entity.Is_Active = vo.IsActive;
                    entity.Cancel_Code = vo.CancelCode;
                    entity.Remark = vo.Remark;
                    entity.Last_Updated_Time = DateTime.UtcNow;
                    Add( entity, vo.Roles );
                });

                trans.Complete();
            }
            return new VoidResult();
        }

        public VoidResult Delete(int ID)
        {
            AS_Flow_Name_Records.Delete( m => m.ID == ID );
            return new VoidResult();
        }


        private static void Add(AS_Flow_Name entity, List<int> Roles)
        {
            decimal Version = entity.AS_Flow_Template.Any() ? entity.AS_Flow_Template.Max(e => e.Version) + 1 : 1;
            entity.AS_Flow_Template.Add( new Entity.Edmx.AS_Flow_Template
            {
                Version = Version,
            });

            var roles = Roles ?? new List<int>();
            var template = entity.AS_Flow_Template.First(m => m.Version == Version);
            for (int step = 0; step < roles.Count; ++step)
            {
                template.AS_Flow_Template_Flow_Role.Add( new AS_Flow_Template_Flow_Role {

                    Flow_Role_ID = Roles[step],
                    Step_No = step + 1
                } );
            }
        }
        public bool CheckFlowCode(UpdateModel updateModel)
        {
            bool _pass = true;
            List<AS_Flow_Name> _listASFlowName = new List<AS_Flow_Name>();
            _listASFlowName = AS_Flow_Name_Records.Find(x => x.Flow_Code == updateModel.FlowCode);
            if (updateModel.ID > 0) _listASFlowName = _listASFlowName.Where(x => x.ID != updateModel.ID).ToList();
            if (_listASFlowName != null && _listASFlowName.Count > 0)
            {
                _pass = false;
            }
            return _pass;
        }
    }
}
