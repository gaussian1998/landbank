﻿using Library.Servant.Servant.Common;
using Library.Servant.Servant.WorkFlow.Models;

namespace Library.Servant.Servant.WorkFlow
{
    public interface IWorkFlowServant
    {
        NewResult New();
        VoidResult Create(CreateModel vo);
        IndexResult Index(SearchModel search);
        EditResult Details(int ID);
        VoidResult Update(UpdateModel vo);
        VoidResult Delete(int ID);
        bool CheckFlowCode(UpdateModel updateModel);
    }
}
