﻿using Library.Servant.Servant.Common;
using Library.Servant.Servant.WorkFlow.Models;
using Library.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Servant.WorkFlow
{
    public class RemoteWorkFlowServant : IWorkFlowServant
    {
        public NewResult New()
        {
            return RemoteServant.Post<VoidModel, NewResult>(

                new VoidModel { },
                "/WorkFlowServant/New"
            );
        }

        public IndexResult Index(SearchModel search)
        {
            return RemoteServant.Post<SearchModel, IndexResult>(

               search,
                "/WorkFlowServant/Index"
            );
        }

        public VoidResult Create(CreateModel vo)
        {
            return RemoteServant.Post<CreateModel, VoidResult>(

               vo,
                "/WorkFlowServant/Create"
            );
        }

        public VoidResult Delete(int ID)
        {
            return RemoteServant.Post<IntModel, VoidResult>(

                new IntModel { Value = ID },
                "/WorkFlowServant/Delete"
            );
        }

        public EditResult Details(int ID)
        {
            return RemoteServant.Post<IntModel, EditResult>(

                new IntModel { Value = ID },
                "/WorkFlowServant/Details"
            );
        }

        public VoidResult Update(UpdateModel vo)
        {
            return RemoteServant.Post<UpdateModel, VoidResult>(

                 vo,
                 "/WorkFlowServant/Update"
             );
        }
        public bool CheckFlowCode(UpdateModel vo)
        {
            return (RemoteServant.Post<UpdateModel, BoolResult>(
                vo,
               "/WorkFlowServant/CheckFlowCode"
            )).Value;
        }
    }
}
