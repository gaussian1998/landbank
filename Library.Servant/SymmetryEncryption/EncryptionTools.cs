﻿using Library.Common.Utility;
using Library.Utility;


namespace Library.Servant.SymmetryEncryption
{
    public static class EncryptionTools
    {
        public static readonly SimpleEncryption authClient = new SimpleEncryption("gr7x9834zryniu3txrb78fatchbxadsjzagqwgbz@%$gdxjw67562", "$@@!#3EYTfHJBDKSIY652Ry%$$@1IU8SHBur%^$@QBS");
        public static readonly SimpleEncryption authServer = new SimpleEncryption("kd9366yfdhv!$%9o3j20@34fghvuio1234HJKLLK99X", "@%^dfghJKOSA0908634fcxvjjl;'p!$%Hcksp00");
        public static readonly GzipEncryption gzip = new GzipEncryption();
        public static readonly NoEncryption noEncryption = new NoEncryption();
    }
}
