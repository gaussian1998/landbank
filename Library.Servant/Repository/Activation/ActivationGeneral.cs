﻿using Library.Entity.Edmx;
using Library.Servant.Servant.Activation.ActivationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Transactions;

namespace Library.Servant.Repository.Activation
{
    public static class ActivationGeneral
    {
        public static DateTime CvtS2D(string DS)
        {
            var s = DS.Split('-');
            DateTime rs = DateTime.Parse(
                (Int32.Parse(s[0])/*+1911*/).ToString() + "/" +
                s[1] + "/" +
                s[2]
                );
            return rs;
        }
        public static string CvtD2S(DateTime DS)
        {
            var s = DS.ToString("yyyy-MM-dd").Split('-');
            string rs =
                (Int32.Parse(s[0]) - 1911).ToString() + "-" +
                s[1] + "-" +
                s[2]
                ;
            return rs;
        }
        public static JObject GetDocID(EJObject T)
        {
            bool result = true;
            int ri = 0;
            using (var db = new AS_LandBankEntities())
            {
                result = db.AS_Doc_Name.Any(m => m.Doc_No == T.Obj["DocCode"].ToString());
                ri = (result) ? db.AS_Doc_Name.First(m => m.Doc_No == T.Obj["DocCode"].ToString()).ID : 0;
            }
            JObject r = new JObject();
            r = JObject.Parse("{\"DocID\":\""+ ri.ToString()+ "\"}");

            return r;
        }
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static IEnumerable<ActivationModel> GetIndex(SearchModel condition)
        {
            IEnumerable<ActivationModel> result = null;
            IEnumerable<JObject> result2 = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_Activation
                            join dtype in db.AS_Assets_DocumentType on mh.Document_Type equals dtype.ID
                            select new { mh, dtype.Name };
                /*
                if (!string.IsNullOrEmpty(condition.Transaction_Type))
                {
                    query = query.Where(m => m.mh.Transaction_Type == condition.Transaction_Type);
                }
                if (!string.IsNullOrEmpty(condition.Source))
                {
                    query = query.Where(m => m.mh.Source == condition.Source);
                }
                if (!string.IsNullOrEmpty(condition.TRXHeaderIDBgn))
                {
                    query = query.Where(m => String.Compare(m.mh.TRX_Header_ID, condition.TRXHeaderIDBgn) >= 0);
                }
                if (!string.IsNullOrEmpty(condition.TRXHeaderIDEnd))
                {
                    query = query.Where(m => String.Compare(m.mh.TRX_Header_ID, condition.TRXHeaderIDEnd) <= 0);
                }
                if (!string.IsNullOrEmpty(condition.BookTypeCode))
                {
                    query = query.Where(m => m.mh.Book_Type == condition.BookTypeCode);
                }
                if (condition.BeginDate != null)
                {
                    query = query.Where(m => m.mh.Create_Time >= condition.BeginDate);
                }
                if (condition.EndDate != null)
                {
                    query = query.Where(m => m.mh.Create_Time <= condition.EndDate);
                }
                if (!string.IsNullOrEmpty(condition.FlowStatus))
                {
                    string[] flows = condition.FlowStatus.Split(',');
                    query = query.Where(m => flows.Contains(m.mh.Flow_Status));
                }
                if (!string.IsNullOrEmpty(condition.OfficeBranch))
                {
                    query = query.Where(m => m.mh.Office_Branch == condition.OfficeBranch);
                }
                if (!string.IsNullOrEmpty(condition.Remark))
                {
                    query = query.Where(m => m.mh.Remark.Contains(condition.Remark));
                }
                */


                result2 = query.ToList().OrderByDescending(m => m.mh.Create_Time).Select(m => new JObject());

                result = query.ToList().OrderByDescending(m => m.mh.Create_Time).Select(m => new ActivationModel()
                {
                    ID = m.mh.ID,
                    ParantID = m.mh.ParantID,
                    Document_Type = m.mh.Document_Type,
                    Source_Type = m.mh.Source_Type,
                    Activation_No = m.mh.Activation_No,
                    BOOK_TYPE_CODE = m.mh.BOOK_TYPE_CODE,
                    BeginDate = m.mh.BeginDate,
                    EndDate = m.mh.EndDate,
                    Jurisdiction = m.mh.Jurisdiction1,
                    Type = m.mh.Type,
                    Activation_Target = m.mh.Activation_Target,
                    TEL = m.mh.TEL,
                    Email = m.mh.Email,
                    Remark = m.mh.Remark,
                    Create_Time = m.mh.Create_Time,
                    Created_By = m.mh.Created_By,
                    Last_Updated_Time = m.mh.Last_Updated_Time,
                    Last_Updated_By = m.mh.Last_Updated_By
                });
            }

            return result;
        }
        public static JObject GettestIndex(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());
           // pagecount = 3;
            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            string Source_Type = condition.Obj["Source_Type"].ToString();
            string Document_Type = condition.Obj["Document_Type"].ToString();
            string BOOK_TYPE_CODE = condition.Obj["BOOK_TYPE_CODE"].ToString();
            string bno = condition.Obj["bno"].ToString();
            string eno = condition.Obj["eno"].ToString();
            string Jurisdiction1 = condition.Obj["Jurisdiction1"].ToString();
            string Jurisdiction2 = condition.Obj["Jurisdiction2"].ToString();
            DateTime BeginDate;
            DateTime EndDate;
            if (!string.IsNullOrEmpty(condition.Obj["BeginDate"].ToString()))
            {
                BeginDate = DateTime.Parse(condition.Obj["BeginDate"].ToString());
            }
            else
            {
                BeginDate = DateTime.Parse("1900/01/01");
            }
            if (!string.IsNullOrEmpty(condition.Obj["EndDate"].ToString()))
            {
                EndDate = DateTime.Parse(condition.Obj["EndDate"].ToString());
            }
            else
            {
                EndDate = DateTime.Parse("9900/01/01");
            }
       
            string Remark = condition.Obj["Remark"].ToString();

            string[] State = ((JArray)condition.Obj["State"]).Select(m => (string)m).ToArray();
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_Activation
                            join dtype in db.AS_Assets_DocumentType on mh.Source_Type.ToString() equals dtype.ID.ToString()
                            into ps from dtype in ps.DefaultIfEmpty()
                            join dtype0 in db.AS_Assets_DocumentType on mh.Document_Type.ToString() equals dtype0.ID.ToString()
                            into ps0
                            from dtype0 in ps0.DefaultIfEmpty()
                            join j1 in (from AS_Department in db.AS_Department
                                        select new
                                        {
                                            AS_Department.Branch_Code
                                        }).Distinct() on mh.Jurisdiction1 equals j1.Branch_Code
                            join j2 in db.AS_Department on mh.Jurisdiction2 equals j2.Department
                            into p4 from j2 in p4.DefaultIfEmpty()
                            join t in db.AS_Assets_ActivationTarget on mh.Activation_Target equals t.ID
                            into p5 from t in p5.DefaultIfEmpty()
                            join u in db.AS_Users on mh.Created_By equals u.ID
                            into p6 from u in p6.DefaultIfEmpty()
                            join ty in db.AS_Code_Table on new { Code_ID = mh.Type.ToString(), Class = "T02" } equals
                                                           new { ty.Code_ID, ty.Class }
                            into p7
                            from ty in p7.DefaultIfEmpty()

                            select new {
                                ID = mh.ID,
                                NO = mh.Activation_No,
                                LY = dtype.Name,
                                DT = dtype0.Name,
                                DateY = (mh.BeginDate.Value.Year - 1911).ToString(),
                                DateM = (mh.BeginDate.Value.Month).ToString(),
                                DateD = (mh.BeginDate.Value.Day).ToString(),
                                J1 = j1.Branch_Code,
                                J1C = mh.Jurisdiction1,
                                J2 = j2.Department_Name,
                                J2C = mh.Jurisdiction2,
                                CR = u.User_Name,
                                TG = t.Name,
                                TY = ty.Text,
                                TYC = mh.Type,
                                CT = mh.Create_Time,
                                Remark = mh.Remark,
                                BeginDate = mh.BeginDate,
                                Source_Type = mh.Source_Type,
                                BOOK_TYPE_CODE = mh.BOOK_TYPE_CODE,
                                Document_Type = mh.Document_Type
                            };
                
                if (!string.IsNullOrEmpty(Document_Type))
                {
                    query = query.Where(m => m.Document_Type.ToString() == Document_Type);
                }
                
                if (!string.IsNullOrEmpty(Source_Type))
                {
                    query = query.Where(m => m.Source_Type.ToString() == Source_Type);
                }
                if (!string.IsNullOrEmpty(BOOK_TYPE_CODE))
                {
                    query = query.Where(m => m.BOOK_TYPE_CODE.ToString() == BOOK_TYPE_CODE);
                }
                if (!string.IsNullOrEmpty(Remark))
                {
                    query = query.Where(m => m.Remark.Contains(Remark));
                }
                if (!string.IsNullOrEmpty(Jurisdiction1))
                {
                    query = query.Where(m => m.J1C.Contains(Jurisdiction1));
                }
                if (!string.IsNullOrEmpty(Jurisdiction2))
                {
                    query = query.Where(m => m.J2C.Contains(Jurisdiction2));
                }
                if (!string.IsNullOrEmpty(Remark))
                {
                    query = query.Where(m => m.Remark.Contains(Remark));
                }
                if (!string.IsNullOrEmpty(bno))
                {
                    query = query.Where(m => string.Compare(m.NO, bno) >= 0);
                }
                if (!string.IsNullOrEmpty(eno))
                {
                    query = query.Where(m => string.Compare(m.NO, eno) <= 0);
                }
                query = query.Where(m => m.BeginDate >= BeginDate);
                query = query.Where(m => m.BeginDate <= EndDate);

                query = query.Where(m => State.Contains(m.TYC.ToString()));
                result = new JObject(new JProperty("Data",
                    query.ToList().OrderByDescending(m => m.CT)
                   .Skip(pagestart).Take(pagecount)
                    .Select(m => JToken.FromObject(m))
                    ));
                result["amount"] = query.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }
            return result;
        }
        public static JObject J200IndexList(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());
            // pagecount = 3;
            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;

            string Source_Type = condition.Obj["Source_Type"].ToString();
            string BOOK_TYPE_CODE = condition.Obj["BOOK_TYPE_CODE"].ToString();
            string bno = condition.Obj["bno"].ToString();
            string eno = condition.Obj["eno"].ToString();
            string Jurisdiction1 = condition.Obj["Jurisdiction1"].ToString();
            string Jurisdiction2 = condition.Obj["Jurisdiction2"].ToString();
            DateTime BeginDate;
            DateTime EndDate;
            if (!string.IsNullOrEmpty(condition.Obj["BeginDate"].ToString()))
            {
                BeginDate = DateTime.Parse(condition.Obj["BeginDate"].ToString());
            }
            else
            {
                BeginDate = DateTime.Parse("1900/01/01");
            }
            if (!string.IsNullOrEmpty(condition.Obj["EndDate"].ToString()))
            {
                EndDate = DateTime.Parse(condition.Obj["EndDate"].ToString());
            }
            else
            {
                EndDate = DateTime.Parse("9900/01/01");
            }

            string Remark = condition.Obj["Remark"].ToString();

            string[] State = ((JArray)condition.Obj["State"]).Select(m => (string)m).ToArray();
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_Activation
                            join dtype in db.AS_Assets_DocumentType on mh.Source_Type.ToString() equals dtype.ID.ToString()
                            into ps
                            from dtype in ps.DefaultIfEmpty()
                            join j1 in (from AS_Department in db.AS_Department
                                        select new
                                        {
                                            AS_Department.Branch_Code
                                        }).Distinct() on mh.Jurisdiction1 equals j1.Branch_Code
                            into p3
                            from j1 in p3.DefaultIfEmpty()
                           
                            join j2 in db.AS_Department on mh.Jurisdiction2 equals j2.Department
                            into p4
                            from j2 in p4.DefaultIfEmpty()
                            join t in db.AS_Assets_ActivationTarget on mh.Activation_Target equals t.ID
                            into p5
                            from t in p5.DefaultIfEmpty()
                            join u in db.AS_Users on mh.Created_By equals u.ID
                            into p6
                            from u in p6.DefaultIfEmpty()
                            join ty in db.AS_Code_Table on new { Code_ID = mh.Type.ToString(), Class = "T02" } equals
                                                           new { ty.Code_ID, ty.Class }
                            into p7
                            from ty in p7.DefaultIfEmpty()
                            select new
                            {
                                ID = mh.ID,
                                NO = mh.Activation_No,
                                LY = dtype.Name,
                                DateY = (mh.BeginDate.Value.Year - 1911).ToString(),
                                DateM = (mh.BeginDate.Value.Month).ToString(),
                                DateD = (mh.BeginDate.Value.Day).ToString(),
                                J1 = j1.Branch_Code,
                                J1C = mh.Jurisdiction1,
                                J2 = j2.Department_Name,
                                J2C = mh.Jurisdiction2,
                                CR = u.User_Name,
                                TG = t.Name,
                                TY = ty.Text,
                                TYC = mh.Type,
                                CT = mh.Create_Time,
                                Remark = mh.Remark,
                                BeginDate = mh.BeginDate,
                                Source_Type = mh.Source_Type,
                                BOOK_TYPE_CODE = mh.BOOK_TYPE_CODE,
                                Document_Type = mh.Document_Type
                            };
                query = query.Where(m => m.Document_Type.ToString() == "1002");
                if (!string.IsNullOrEmpty(Source_Type))
                {
                    query = query.Where(m => m.Source_Type.ToString() == Source_Type);
                }
                if (!string.IsNullOrEmpty(BOOK_TYPE_CODE))
                {
                    query = query.Where(m => m.BOOK_TYPE_CODE.ToString() == BOOK_TYPE_CODE);
                }
                if (!string.IsNullOrEmpty(Remark))
                {
                    query = query.Where(m => m.Remark.Contains(Remark));
                }
                if (!string.IsNullOrEmpty(Jurisdiction1))
                {
                    query = query.Where(m => m.J1C.Contains(Jurisdiction1));
                }
                if (!string.IsNullOrEmpty(Jurisdiction2))
                {
                    query = query.Where(m => m.J2C.Contains(Jurisdiction2));
                }
                if (!string.IsNullOrEmpty(Remark))
                {
                    query = query.Where(m => m.Remark.Contains(Remark));
                }
                if (!string.IsNullOrEmpty(bno))
                {
                    query = query.Where(m => string.Compare(m.NO, bno) >= 0);
                }
                if (!string.IsNullOrEmpty(eno))
                {
                    query = query.Where(m => string.Compare(m.NO, eno) <= 0);
                }
                query = query.Where(m => m.BeginDate >= BeginDate);
                query = query.Where(m => m.BeginDate <= EndDate);

                query = query.Where(m => State.Contains(m.TYC.ToString()));
                result = new JObject(new JProperty("Data",
                    query.ToList().OrderByDescending(m => m.CT)
                   .Skip(pagestart).Take(pagecount)
                    .Select(m => JToken.FromObject(m))
                    ));
                result["amount"] = query.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }
            return result;
        }
        
        public static JObject GetAB(EJObject condition)
        {
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());

            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            long AID = Int64.Parse(condition.Obj["AID"].ToString());
            using (var db = new AS_LandBankEntities())
            {
                var query = from ABU in db.AS_Assets_Activation_LBDetail
                          
                            select new
                            {
                                AID = ABU.Activation_ID,
                                BID = ABU.LBID,
                                TYPE = ABU.Type
                            };
                query = query.Where(m => m.AID == AID).Where(m => m.TYPE == 0);
                result = new JObject(new JProperty("Data",
                    query.ToList().OrderBy(m => m.BID)
                   
                    .Select(m => JToken.FromObject(m))
                    ));

                result["amount"] = query.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }
            return result;
        }
        public static JObject GetAL(EJObject condition)
        {
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());

            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            long AID = Int64.Parse(condition.Obj["AID"].ToString());
            using (var db = new AS_LandBankEntities())
            {
                var query = from ABU in db.AS_Assets_Activation_LBDetail

                            select new
                            {
                                AID = ABU.Activation_ID,
                                BID = ABU.LBID,
                                TYPE = ABU.Type
                            };
                query = query.Where(m => m.AID == AID).Where(m => m.TYPE == 1);
                result = new JObject(new JProperty("Data",
                    query.ToList().OrderBy(m => m.BID)

                    .Select(m => JToken.FromObject(m))
                    ));

                result["amount"] = query.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }
            return result;
        }
        public static JObject GetAPB(EJObject condition)
        {
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());

            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            long AID = Int64.Parse(condition.Obj["AID"].ToString());
            using (var db = new AS_LandBankEntities())
            {
                var query = from ABU in db.AS_Assets_PlanningBranch

                            select new
                            {
                                AID = ABU.Activation_ID,
                                BID = ABU.BranchID
                            };
                query = query.Where(m => m.AID == AID);
                result = new JObject(new JProperty("Data",
                    query.ToList().OrderBy(m => m.BID)

                    .Select(m => JToken.FromObject(m))
                    ));

                result["amount"] = query.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }
            return result;
        }
        public static JObject GetBuild(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());

            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            string[] BID = new string[0]  ;
            if (condition.Obj["ID"]!=null)
             BID = ((JArray)condition.Obj["ID"]).Select(m => (string)m).ToArray();

            using (var db = new AS_LandBankEntities())
            {
                var query = from bu in db.AS_Assets_Build
                            join d in db.AS_Assets_Build_Detail on bu.ID equals d.Assets_Build_ID
                            into ps
                            from d in ps.DefaultIfEmpty()
                            group d by new
                            {
                                bu.ID,
                                bu.Asset_Number,
                                bu.Asset_Category_Code,
                                bu.Description,
                                bu.Business_Book_Amount,
                                bu.Date_Placed_In_Service,
                                bu.City_Name,
                                bu.District_Name,
                                bu.Sectioni_Name,
                                bu.Sub_Sectioni_Name,
                                bu.Build_Address,
                                // bu.Authorized_Area,
                                bu.Current_Cost,
                                bu.Used_Type,
                                bu.Used_Status
                            } into g
                            select new
                            {
                                g.Key.ID,
                                g.Key.Asset_Number,
                                g.Key.Asset_Category_Code,
                                g.Key.Description,
                                g.Key.Business_Book_Amount,
                                g.Key.Date_Placed_In_Service,
                                g.Key.City_Name,
                                g.Key.District_Name,
                                g.Key.Sectioni_Name,
                                g.Key.Sub_Sectioni_Name,
                                g.Key.Build_Address,
                                //g.Key.Authorized_Area,
                                g.Key.Current_Cost,
                                g.Key.Used_Type,
                                g.Key.Used_Status,
                                Area_Size = (decimal?)g.Sum(p => p.Area_Size)
                            };
                           /*
                            select new
                            {
                                ID = bu.ID,
                                  Asset_Number = bu.Asset_Number,
                                  Asset_Category_Code = bu.Asset_Category_Code,
                                  Description = bu.Description,
                                  Business_Book_Amount = bu.Business_Book_Amount,
                                  Date_Placed_In_Service = bu.Date_Placed_In_Service,
                                  Build_Address=bu.Build_Address,
                                //Authorized_Area = bu.Authorized_Area,
                                Current_Cost = bu.Current_Cost,
                                Used_Type = bu.Used_Type,
                                //Used_Partition = bu.Used_Partition,
                                Used_Status = bu.Used_Status
                            };*/
                if (BID.Length > 0)
                {
                    query = query.Where(m => BID.Contains(m.ID.ToString()));
                }
               
                
                result = new JObject(new JProperty("Data",
                    query.ToList().OrderBy(m => m.Asset_Number)
                   .Skip(pagestart).Take(pagecount)
                    .Select(m => JToken.FromObject(m))
                    ));

                result["amount"] = query.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }
            return result;
        }
        public static JObject GetLand(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());

            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            string[] BID = new string[0];
            if (condition.Obj["ID"] != null)
                BID = ((JArray)condition.Obj["ID"]).Select(m => (string)m).ToArray();

            using (var db = new AS_LandBankEntities())
            {
                var query = from bu in db.AS_Assets_Land
                            select new
                            {
                                ID = bu.ID,
                                Asset_Number = bu.Asset_Number,
                                Description = bu.Description,
                                Area_Size = bu.Area_Size,
                                Current_Cost = bu.Current_Cost,
                                Used_Type = bu.Used_Type,
                                Used_Partition = bu.Used_Partition,
                                Used_Status = bu.Used_Status,
                                City_Name = bu.City_Name,
                                District_Name = bu.District_Name,
                                Sectioni_Name = bu.Section_Name,
                                Sub_Sectioni_Name = bu.Sub_Section_Name,
                                Authorization_Number = bu.Authorization_Number,
                                Asset_Category_code = bu.Asset_Category_code,
                                Business_Book_Amount = bu.Business_Book_Amount,
                                Datetime_Placed_In_Service = bu.Datetime_Placed_In_Service,
                             
                            };
               
                if (BID.Length > 0)
                {
                    query = query.Where(m => BID.Contains(m.ID.ToString()));
                }


                result = new JObject(new JProperty("Data",
                    query.ToList().OrderBy(m => m.Asset_Number)
                   .Skip(pagestart).Take(pagecount)
                    .Select(m => JToken.FromObject(m))
                    ));

                result["amount"] = query.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }
            return result;
        }
        public static JObject GetPB(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());

            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            string[] BID = new string[0];
            if (condition.Obj["ID"] != null)
                BID = ((JArray)condition.Obj["ID"]).Select(m => (string)m).ToArray();

            
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Department
                            join a in db.AS_Assets_Activation_PB2User on mh.Branch_Code.ToString() equals a.Branch_Code.ToString()
                            into g1
                            from a in g1.DefaultIfEmpty()
                            join b in db.AS_Users on a.User_Code.ToString() equals b.User_Code.ToString()
                            into g2
                            from b in g2.DefaultIfEmpty()
                            select new { mh,UN=b.User_Name,EM=b.Email };
                
                query = query.Where(m => m.mh.Branch_Code != "001");
                if (BID.Length > 0)
                {
                    query = query.Where(m => BID.Contains(m.mh.Branch_Code.ToString()));
                }
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Skip(pagestart).Take(pagecount).Select(m => JToken.FromObject(m))));
                result["amount"] = query.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }

            
            return result;
        }
        public static JObject GetTarget(SearchModel condition)
        {
            //IEnumerable<JObject> result = null;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_ActivationTarget
                            select new { mh };
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject GetTargetE(EJObject condition)
        {
            //IEnumerable<JObject> result = null;

            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_ActivationTarget
                            select new { mh };
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject GetMaxNo(EJObject condition)
        {
            //IEnumerable<JObject> result = null;

            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from AS_Assets_Activation in
                         (from AS_Assets_Activation in db.AS_Assets_Activation.AsEnumerable()
                          select new
                          {
                              Column1 = AS_Assets_Activation.Activation_No.Substring(12 - 1, 6),
                              Dummy = "x"
                          })
                            group AS_Assets_Activation by new { AS_Assets_Activation.Dummy } into g
                            select new
                            {
                                maxno = (int?)(Int32)g.Max(p => Convert.ToInt32(p.Column1) + 1)
                            };
                result = new JObject(new JProperty("Data", query.ToList().Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject DocumentType(EInt type1)
        {
            int type = type1.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_DocumentType
                            select new { mh};
                query = query.Where(m =>   m.mh.ID >= type*1000).Where(m => m.mh.ID < (type+1) * 1000);
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject DocumentTypeOne(EInt type1)
        {
            int type = type1.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_DocumentType
                            select new { mh };
                query = query.Where(m => m.mh.ID == type);
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject GetDPM(EInt type1)
        {
            int type = type1.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Department
                            select new { mh };
                if (type == 1)
                query = query.Where(m => m.mh.Branch_Code != "001");
                if (type == 2)
                    query = query.Where(m => m.mh.Branch_Code == "001");
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject GetOrder(EString no1)
        {
            string no = no1.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_Activation
                            join ty in db.AS_Code_Table on new { Code_ID = mh.Type.ToString(), Class = "T02" } equals
                                                           new { ty.Code_ID, ty.Class }
                            into p7
                            from ty in p7.DefaultIfEmpty()
                            select new { mh,
                                DateY = (mh.BeginDate.Value.Year /*- 1911*/).ToString(),
                                DateM = (mh.BeginDate.Value.Month).ToString(),
                                DateD = (mh.BeginDate.Value.Day).ToString(),
                                TY = ty.Text
                            };
                query = query.Where(m => m.mh.ID.ToString() == no);
                var query1 = from c in db.AS_Assets_Activation_LBDetail
                             select new { ID = c.ID, AID = c.Activation_ID ,TY = c.Type };
                query1 = query1.Where(m => m.AID.ToString() == no);
              
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
                result["LC"] = query1.Where(m => m.TY == 1).ToList().Count();
                result["BC"] = query1.Where(m => m.TY == 0).ToList().Count();
               
            }
            return result;
        }
        public static JObject GetBookCode(EString type1)
        {
         
            string type = type1.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Code_Table
                            select new { mh };
                query = query.Where(m => m.mh.Class == type);
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject UpdateOrder(EJObject model2)
        {
            JObject model = new JObject();
            model = model2.Obj;
            long nowidd = 0;
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    for (int i = 0; i < model.Count; i++)
                    {
                        AS_Assets_Activation AA;
                        AS_Assets_Activation_LBDetail ALB;
                        AS_Assets_PlanningBranch APB;
                            if (model["Data"]["ID"] == null)
                        {
                           
                            AA = new AS_Assets_Activation();
                            nowidd = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                            AA.ID = nowidd;
                            AA.Create_Time = DateTime.Now;
                            AA.Last_Updated_Time = DateTime.Now;
                            db.Entry(AA).State = System.Data.Entity.EntityState.Added;
                        }
                        else
                        {
                            long idd = 0;
                            Int64.TryParse(model["Data"]["ID"].ToString(), out idd);
                            AA = db.AS_Assets_Activation.FirstOrDefault(m => m.ID == idd);
                            nowidd = idd;
                            AA.Last_Updated_Time = DateTime.Now;
                            db.Entry(AA).State = System.Data.Entity.EntityState.Modified;
                        }
                        String[] BU = new String[0];
                        if (model["Data"]["DataB"] != null) { 
                            if (model["Data"]["DataB"].ToString() != "")
                            {
                                db.AS_Assets_Activation_LBDetail.RemoveRange(
                                    db.AS_Assets_Activation_LBDetail.Where(m => m.Activation_ID == nowidd).Where(m => m.Type == 0));
                                db.Configuration.AutoDetectChangesEnabled = false;
                                BU = model["Data"]["DataB"].ToString().Split(',');
                                for (int j = 0; j < BU.Length; j++) { 
                                    ALB = new AS_Assets_Activation_LBDetail();
                                    ALB.ID = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                                    ALB.Activation_ID = nowidd;
                                    ALB.LBID = Int32.Parse(BU[j]);
                                    ALB.Type = 0;
                                    db.AS_Assets_Activation_LBDetail.Add(ALB);
                                }
                            }
                        }
                        String[] LA = new String[0];
                        if (model["Data"]["DataL"] != null)
                        {
                            if (model["Data"]["DataL"].ToString() != "")
                            {
                                db.AS_Assets_Activation_LBDetail.RemoveRange(
                                    db.AS_Assets_Activation_LBDetail.Where(m => m.Activation_ID == nowidd).Where(m => m.Type == 1));
                                db.Configuration.AutoDetectChangesEnabled = false;
                                LA = model["Data"]["DataL"].ToString().Split(',');
                                for (int j = 0; j < LA.Length; j++)
                                {
                                    ALB = new AS_Assets_Activation_LBDetail();
                                    ALB.ID = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                                    ALB.Activation_ID = nowidd;
                                    ALB.LBID = Int32.Parse(LA[j]);
                                    ALB.Type = 1;
                                    db.AS_Assets_Activation_LBDetail.Add(ALB);
                                }
                            }
                        }
                        String[] PB = new String[0];
                        if (model["Data"]["DataPB"] != null) { 
                            if (model["Data"]["DataPB"].ToString() != "")
                            {
                                db.AS_Assets_PlanningBranch.RemoveRange(db.AS_Assets_PlanningBranch.Where(m => m.Activation_ID == nowidd));
                                db.Configuration.AutoDetectChangesEnabled = false;
                                PB = model["Data"]["DataPB"].ToString().Split(',');
                                for (int j = 0; j < PB.Length; j++)
                                {
                                    APB = new AS_Assets_PlanningBranch();
                                    APB.ID = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                                    APB.Activation_ID = nowidd;
                                    APB.BranchID = PB[j].ToString ();
                                    db.AS_Assets_PlanningBranch.Add(APB);
                                }
                            }
                        }
                        AA.BeginDate = ActivationGeneral.CvtS2D(model["Data"]["BeginDate"].ToString().Replace("/", "-"));
                        AA.Activation_No = model["Data"]["Activation_No"].ToString();
                        AA.Document_Type = Int64.Parse(model["Data"]["Document_Type"].ToString());
                        AA.Source_Type = Int32.Parse(model["Data"]["Source_Type"].ToString());
                        AA.BOOK_TYPE_CODE = model["Data"]["BOOK_TYPE_CODE"].ToString();
                        AA.Jurisdiction1 = model["Data"]["Jurisdiction1"].ToString();
                        AA.Jurisdiction2 = model["Data"]["Jurisdiction2"].ToString();
                        AA.Activation_Target = Int64.Parse(model["Data"]["Activation_Target"].ToString());
                        AA.Remark = model["Data"]["Remark"].ToString();
                        AA.Type = Int32.Parse(model["Data"]["Type"].ToString());
                        AA.Created_By = Int32.Parse(model["Data"]["Created_By"].ToString());
                        AA.Last_Updated_By = Int32.Parse(model["Data"]["Last_Updated_By"].ToString());

                     

                        db.SaveChanges();
                        db.Configuration.AutoDetectChangesEnabled = true;
                    }
                }
                trans.Complete();
            }
            JObject result = new JObject(new JProperty("TRX_Header_ID", nowidd.ToString()));
            return result;
        }
        #region== EntityToModel ==

        public static ActivationModel ToAssetModel(AS_Assets_Activation entity, AS_Keep_Position position = null, AS_Users user = null)
        {
            return new ActivationModel()
            {
                ID = entity.ID,
                ParantID = entity.ParantID,
                Document_Type = entity.Document_Type,
                Source_Type = entity.Source_Type,
                Activation_No = entity.Activation_No,
                BOOK_TYPE_CODE = entity.BOOK_TYPE_CODE,
                BeginDate = entity.BeginDate,
                EndDate = entity.EndDate,
                Jurisdiction = entity.Jurisdiction1,
                Type = entity.Type,
                Activation_Target = entity.Activation_Target,
                TEL = entity.TEL,
                Email = entity.Email,
                Remark = entity.Remark,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Last_Updated_Time = entity.Last_Updated_Time,
                Last_Updated_By = entity.Last_Updated_By

            };
        }

        
        #endregion

        #region== ModelToEntity ==
        

        public static AS_Assets_Activation ToAS_Assets_Activation(ActivationModel model)
        {
            return new AS_Assets_Activation()
            {
                ID = model.ID,
                ParantID = model.ParantID,
                Document_Type = model.Document_Type,
                Source_Type = model.Source_Type,
                Activation_No = model.Activation_No,
                BOOK_TYPE_CODE = model.BOOK_TYPE_CODE,
                BeginDate = model.BeginDate,
                EndDate = model.EndDate,
             
                Type = model.Type,
                Activation_Target = model.Activation_Target,
                TEL = model.TEL,
                Email = model.Email,
                Remark = model.Remark,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }
        
        #endregion
    }
}
