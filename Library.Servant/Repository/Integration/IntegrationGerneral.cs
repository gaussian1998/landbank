﻿using Library.Entity.Edmx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Integration.IntegrationModels;

namespace Library.Servant.Repository.Integration
{
    public class IntegrationGerneral
    {
        /// <summary>
        /// 取得controllername
        /// </summary>
        /// <returns></returns>
        public static string GetController(string FormCode)
        {
            string result = "";
            using (var db = new AS_LandBankEntities())
            {
                if (db.AS_VW_Code_Table.Any(o => o.Class == "IntegrationForm" && o.Code_ID == FormCode))
                {
                    result = db.AS_VW_Code_Table.First(o => o.Class == "IntegrationForm" && o.Code_ID == FormCode).Class_Name;
                }
            }
            return result;
        }

        /// <summary>
        /// 取得功能名稱
        /// </summary>
        /// <param name="Action"></param>
        /// <returns></returns>
        public static string GetFunctionTitle(string Action)
        {
            string FunctionName = "";
            switch (Action)
            {
                case "IntegrationSearch":
                    FunctionName = "承租契約查詢";
                    break;
                case "IntegrationAdded":
                    FunctionName = "承租契約新增";
                    break;
                case "IntegrationMaintain":
                    FunctionName = "承租契約維護";
                    break;
                case "IntegrationPay":
                    FunctionName = "承租租金繳納";
                    break;
                case "IntegrationRenewal":
                    FunctionName = "承租續租換約";
                    break;
                case "IntegrationTrack":
                    FunctionName = "承租進度追蹤";
                    break;
            }
            return FunctionName;
        }

        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static IEnumerable<IntegrationHeaderModel> GetIndex(SearchModel condition)
        {
            IEnumerable<IntegrationHeaderModel> result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Leases_Integration_Header
                            join mc in
                            (
                               from mr in db.AS_Leases_Headers
                               where ((condition.Transaction_Type.Equals("E140") && (mr.OLD_LEASE_NUMBER == null || mr.OLD_LEASE_NUMBER == "")) ||
                               ((!condition.Transaction_Type.Equals("E140"))))
                               group mr by new { mr.TRX_Header_ID } into mrg
                               select new
                               {
                                   TRX_Header_ID = mrg.Key.TRX_Header_ID,
                                   Count = mrg.Count()
                               }
                            ) on mh.TRX_Header_ID equals mc.TRX_Header_ID into mhtemp
                            from mc in mhtemp.DefaultIfEmpty()
                            join doc in
                            (
                                from docc in db.AS_VW_Code_Table
                                where docc.Class == "IntegrationForm"
                                select docc
                            ) on mh.Transaction_Type equals doc.Code_ID into tdoc
                            from doc in tdoc.DefaultIfEmpty()
                            join so in db.AS_VW_Code_Table
                            on new { Class = mh.Transaction_Type, Code_ID = mh.Source } equals new { so.Class, so.Code_ID } into tso
                            from so in tso.DefaultIfEmpty()
                            join u in db.AS_Users on mh.Created_By equals u.ID.ToString() into tu
                            from u in tu.DefaultIfEmpty()
                            join o in db.AS_Keep_Position on mh.Office_Branch equals o.Keep_Position_Code into to
                            from o in to.DefaultIfEmpty()
                            join f in
                            (
                               from ff in db.AS_Code_Table
                               where ff.Class == "P02"
                               select ff
                            ) on mh.Flow_Status equals f.Code_ID into tf
                            from f in tf.DefaultIfEmpty()
                            select new { mh, mc, doc, so, u, o, f };

                if (!string.IsNullOrEmpty(condition.Transaction_Type))
                {
                    query = query.Where(m => m.mh.Transaction_Type == condition.Transaction_Type);
                }
                if (!string.IsNullOrEmpty(condition.Source))
                {
                    query = query.Where(m => m.mh.Source == condition.Source);
                }
                if (!string.IsNullOrEmpty(condition.TRXHeaderIDBgn))
                {
                    query = query.Where(m => String.Compare(m.mh.TRX_Header_ID, condition.TRXHeaderIDBgn) >= 0);
                }
                if (!string.IsNullOrEmpty(condition.TRXHeaderIDEnd))
                {
                    query = query.Where(m => String.Compare(m.mh.TRX_Header_ID, condition.TRXHeaderIDEnd) <= 0);
                }
                if (!string.IsNullOrEmpty(condition.BookTypeCode))
                {
                    query = query.Where(m => m.mh.Book_Type == condition.BookTypeCode);
                }
                if (condition.BeginDate != null)
                {
                    query = query.Where(m => m.mh.Create_Time >= condition.BeginDate);
                }
                if (condition.EndDate != null)
                {
                    query = query.Where(m => m.mh.Create_Time <= condition.EndDate);
                }
                if (!string.IsNullOrEmpty(condition.FlowStatus))
                {
                    string[] flows = condition.FlowStatus.Split(',');
                    query = query.Where(m => flows.Contains(m.mh.Flow_Status));
                }
                if (!string.IsNullOrEmpty(condition.OfficeBranch))
                {
                    query = query.Where(m => m.mh.Office_Branch == condition.OfficeBranch);
                }
                if (!string.IsNullOrEmpty(condition.Remark))
                {
                    query = query.Where(m => m.mh.Remark.Contains(condition.Remark));
                }                
                result = query.ToList().OrderByDescending(m => m.mh.Create_Time).Select(m => new IntegrationHeaderModel()
                {
                    ID = m.mh.ID,
                    TRX_Header_ID = m.mh.TRX_Header_ID,
                    Transaction_Type = m.mh.Transaction_Type,
                    Transaction_TypeName = (m.doc != null) ? m.doc.Text : "",
                    Flow_Status = m.mh.Flow_Status,
                    Source = m.mh.Source,
                    SourceName = (m.so != null) ? m.so.Text : "",
                    Book_Type = m.mh.Book_Type,
                    Office_Branch = m.mh.Office_Branch,
                    OriOffice_Branch = m.mh.OriOffice_Branch,
                    Remark = m.mh.Remark,
                    AssetsCount = (m.mc != null) ? m.mc.Count : 0,
                    Created_ByName = (m.u != null) ? m.u.User_Name : "",
                    Create_Time = m.mh.Create_Time,
                    Flow_StatusName = (m.f != null) ? m.f.Text : "",
                    Office_BranchName = (m.o != null) ? m.o.Keep_Position_Name : ""
                });
            }

            return result;
        }


        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static ContractSearchIndexModel GetContractList(ContractSearchModel condition)
        {
            ContractSearchIndexModel result = null;
            using (var db = new AS_LandBankEntities())
            {
                var contractQuery = from mas in
                          (
                              from h in db.AS_Leases_Headers
                              select h
                          )
                          join bo in db.AS_Keep_Position on mas.OFFICER_BRANCH equals bo.Keep_Position_Code into tbo
                          from bo in tbo.DefaultIfEmpty()
                          select new { mas, bo };

                #region 查詢條件
                 if (!string.IsNullOrEmpty(condition.Business_Type))
                 {
                    string[] items = condition.Business_Type.Split(',');
                    contractQuery = contractQuery.Where(m => items.Contains(m.mas.BUSINESS_TYPE));
                 }
                 if (!string.IsNullOrEmpty(condition.Category))
                 {
                    string[] items = condition.Category.Split(',');
                    contractQuery = contractQuery.Where(m => items.Contains(m.mas.LEASE_CATEGORY));
                 }
                 if (!string.IsNullOrEmpty(condition.TRXHeaderIDBgn))
                 {
                    contractQuery = contractQuery.Where(m => String.Compare(m.mas.TRX_Header_ID, condition.TRXHeaderIDBgn) >= 0);
                 }
                 if (!string.IsNullOrEmpty(condition.TRXHeaderIDEnd))
                 {
                    contractQuery = contractQuery.Where(m => String.Compare(m.mas.TRX_Header_ID, condition.TRXHeaderIDEnd) <= 0);
                 }

                if (!string.IsNullOrEmpty(condition.OldTRXHeaderIDBgn))
                {
                    contractQuery = contractQuery.Where(m => String.Compare(m.mas.OLD_LEASE_NUMBER, condition.OldTRXHeaderIDBgn) >= 0);
                }
                if (!string.IsNullOrEmpty(condition.OldTRXHeaderIDEnd))
                {
                    contractQuery = contractQuery.Where(m => String.Compare(m.mas.OLD_LEASE_NUMBER, condition.OldTRXHeaderIDEnd) <= 0);
                }
                
                 if (!string.IsNullOrEmpty(condition.RateStart))
                 {
                    decimal value = decimal.Parse(condition.RateStart);
                    contractQuery = contractQuery.Where(m => m.mas.LEASE_RATE >= value);
                 }
                 if (!string.IsNullOrEmpty(condition.RateEnd))
                 {
                    decimal value = decimal.Parse(condition.RateEnd);
                    contractQuery = contractQuery.Where(m => m.mas.LEASE_RATE <= value);
                 }
                 if (condition.BeginDate != null)
                 {
                    contractQuery = contractQuery.Where(m => m.mas.LEASE_INCEPTION_DATE >= condition.BeginDate);
                 }
                 if (condition.EndDate != null)
                 {
                    contractQuery = contractQuery.Where(m => m.mas.LEASE_TERMINATION_DATE <= condition.EndDate);
                 }

                if (condition.ApplyBeginDate != null)
                {
                    contractQuery = contractQuery.Where(m => m.mas.ACCEPT_DATE >= condition.ApplyBeginDate);
                }
                if (condition.ApplyEndDate != null)
                {
                    contractQuery = contractQuery.Where(m => m.mas.ACCEPT_DATE <= condition.ApplyEndDate);
                }
                                
                if (!string.IsNullOrEmpty(condition.Account))
                {
                    contractQuery = contractQuery.Where(m => m.mas.ACCOUNT_NUMBER.Contains(condition.Account));
                }
                if (!string.IsNullOrEmpty(condition.OfficeBranch))
                {
                    contractQuery = contractQuery.Where(m => m.mas.OFFICER_BRANCH == condition.OfficeBranch);
                }
                if (!string.IsNullOrEmpty(condition.TRANSACTION_TYPE))
                {
                   string[] items = condition.TRANSACTION_TYPE.Split(',');
                   contractQuery = contractQuery.Where(m => items.Contains(m.mas.LEASE_TRANSACTION_TYPE));
                }
                if (!string.IsNullOrEmpty(condition.Status))
                {
                    //string[] flows = condition.FlowStatus.Split(',');
                    //contractQuery = contractQuery.Where(m => flows.Contains(m.mh.Flow_Status));
                }
                #endregion

                result = new ContractSearchIndexModel();
                
                if (contractQuery != null && contractQuery.Count() > 0)
                {
                    result.Items = contractQuery.ToList().Select(o => ToContactModel(o.mas, o.bo));
                }
            }        
            return result;
        }

        /// <summary>
        /// 取得表單編號
        /// </summary>
        /// <returns></returns>
        public static string GetFormCode(string Type)
        {
            string result = "";
            using (var db = new AS_LandBankEntities())
            {
                if (db.AS_VW_Code_Table.Any(o => o.Class == "IntegrationForm" && o.Class_Name == Type))
                {
                    result = db.AS_VW_Code_Table.First(o => o.Class == "IntegrationForm" && o.Class_Name == Type).Code_ID;
                }
            }
            return result;
        }

        /// <summary>
        /// 取得單據明細
        /// </summary>
        /// <param name="TRXHeaderID"></param>
        /// <returns></returns>
        public static IntegrationModel GetIntegrationDetail(string TRXHeaderID, string Transaction_Type)
        {
            IntegrationModel result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in
                            (
                                from h in db.AS_Leases_Integration_Header
                                where h.TRX_Header_ID == TRXHeaderID
                                select h
                            )
                            join mas in db.AS_Leases_Headers on mh.TRX_Header_ID equals mas.TRX_Header_ID into temp
                            from mas in temp.DefaultIfEmpty()
                            join doc in
                            (
                                from docc in db.AS_VW_Code_Table
                                where docc.Class == "IntegrationForm"
                                select docc
                            ) on mh.Transaction_Type equals doc.Code_ID into tdoc
                            from doc in tdoc.DefaultIfEmpty()
                            join u in db.AS_Users on mh.Created_By equals u.User_Code into tu
                            from u in tu.DefaultIfEmpty()
                            join o in db.AS_Keep_Position on mh.Office_Branch equals o.Keep_Position_Code into to
                            from o in to.DefaultIfEmpty()
                            join f in
                            (
                               from ff in db.AS_Code_Table
                               where ff.Class == "P02"
                               select ff
                            ) on mh.Flow_Status equals f.Code_ID into tf
                            from f in tf.DefaultIfEmpty()
                            join bo in db.AS_Keep_Position on mas.OFFICER_BRANCH equals bo.Keep_Position_Code into tbo
                            from bo in tbo.DefaultIfEmpty()
                            select new { mh, mas, doc, u, o, f, bo };//, bu, 
                         
                result = new IntegrationModel();
                if (query != null && query.Count() > 0)
                {
                    result.Header = query.ToList().Select(o => ToIntegrationHeaderModel(o.mh, o.doc, o.u, o.o, o.f)).First();
                }
                var contractQuery = from mas in
                      (
                          from h in db.AS_Leases_Headers
                          where h.TRX_Header_ID == TRXHeaderID
                          select h
                      )
                                    join bo in db.AS_Keep_Position on mas.OFFICER_BRANCH equals bo.Keep_Position_Code into tbo
                                    from bo in tbo.DefaultIfEmpty()
                                    select new { mas, bo };
                if (Transaction_Type.Equals("E140")) {
                    contractQuery = contractQuery.Where(o=> o.mas.OLD_LEASE_NUMBER == "" || o.mas.OLD_LEASE_NUMBER == null);
                }
                if (contractQuery != null && contractQuery.Count() > 0)
                {
                    List<ContactModel> Contracts = new List<ContactModel>();
                    foreach (var item in contractQuery.ToList().DefaultIfEmpty())
                    {
                        ContactModel model = ToContactModel(item.mas, item.bo);
                        var newContractQuery = from mas in
                          (
                              from h in db.AS_Leases_Headers
                              where h.OLD_LEASE_NUMBER == model.LEASE_NUMBER && h.TRANSACTION_TYPE.Equals(Transaction_Type)
                              select h
                          )
                                               join bo in db.AS_Keep_Position on mas.OFFICER_BRANCH equals bo.Keep_Position_Code into tbo
                                               from bo in tbo.DefaultIfEmpty()
                                               select new { mas, bo };
                        if (newContractQuery != null && newContractQuery.Count() > 0)
                        {
                            model.NewContract = newContractQuery.ToList().Select(o => ToContactModel(o.mas, o.bo)).First();
                        }
                        Contracts.Add(model);
                    }
                 
                    result.Contracts = Contracts;
                }
            }
            return result;
        }

        /// <summary>
        /// 取得租約明細
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static ContactModel GetContractDetail(int ID) {
            ContactModel result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in
                            (
                                from h in db.AS_Leases_Headers
                                where h.ID == ID
                                select h
                            )
                            join o in db.AS_Keep_Position on mh.OFFICER_BRANCH equals o.Keep_Position_Code into to
                            from o in to.DefaultIfEmpty()                            
                            select new { mh, o};

                if (query != null && query.Count() > 0)
                {
                    result = ToContactModel(query.ToList().SingleOrDefault().mh, query.ToList().SingleOrDefault().o);
                    result.RentList = new List<RentModel>();
                    result.ManageList = new List<ManageExpenseModel>();
                    result.LandList = new List<LandModel>();
                    result.MarginList = new List<MarginModel>();
                    result.FireList = new List<InsuranceModel>();
                    result.PublicList = new List<InsuranceModel>();
                    result.BuildList = new List<BuildModel>();
                    result.ParkingList = new List<ParkingModel>();
                    result.PaymentList = new List<PaymentAccountModel>();
                    result.NoteList = new List<NoteModel>();

                    decimal id = (decimal)ID;
                    foreach (AS_Lease_Rent item in db.AS_Lease_Rent.Where(o => o.LEASE_NUMBER == result.LEASE_NUMBER && o.LEASE_HEADER_ID == id)) {
                        result.RentList.Add(ToRentModel(item));
                    }

                    foreach (AS_Lease_ManageExpense item in db.AS_Lease_ManageExpense.Where(o => o.LEASE_NUMBER == result.LEASE_NUMBER && o.LEASE_HEADER_ID == id))
                    {
                        result.ManageList.Add(ToManageExpenseModel(item));
                    }

                    foreach (AS_Leases_Land item in db.AS_Leases_Land.Where(o => o.LEASE_NUMBER == result.LEASE_NUMBER && o.LEASE_HEADER_ID == id))
                    {
                        result.LandList.Add(ToLandModel(item));
                    }

                    foreach (AS_Lease_Margin item in db.AS_Lease_Margin.Where(o => o.LEASE_NUMBER == result.LEASE_NUMBER && o.LEASE_HEADER_ID == id))
                    {
                        result.MarginList.Add(ToMarginModel(item));
                    }

                    foreach (AS_Lease_Insurance item in db.AS_Lease_Insurance.Where(o => o.LEASE_NUMBER == result.LEASE_NUMBER && o.INSURANCE_TYPE == "1" && o.LEASE_HEADER_ID == id))
                    {
                        result.FireList.Add(ToInsuranceModel(item));
                    }

                    foreach (AS_Lease_Insurance item in db.AS_Lease_Insurance.Where(o => o.LEASE_NUMBER == result.LEASE_NUMBER && o.INSURANCE_TYPE == "2" && o.LEASE_HEADER_ID == id))
                    {
                        result.PublicList.Add(ToInsuranceModel(item));
                    }

                    foreach (AS_Leases_Build item in db.AS_Leases_Build.Where(o => o.LEASE_NUMBER == result.LEASE_NUMBER && o.LEASE_HEADER_ID == id))
                    {
                        result.BuildList.Add(ToBuildModel(item));
                    }

                    foreach (AS_Leases_Build_Parking item in db.AS_Leases_Build_Parking.Where(o => o.LEASE_NUMBER == result.LEASE_NUMBER && o.LEASE_HEADER_ID == id))
                    {
                        result.ParkingList.Add(ToParkingModel(item));
                    }

                    foreach (AS_Lease_Payment_Account item in db.AS_Lease_Payment_Account.Where(o => o.LEASE_NUMBER == result.LEASE_NUMBER && o.LEASE_HEADER_ID == id))
                    {
                        result.PaymentList.Add(ToPaymentAccountModel(item));
                    }

                    foreach (AS_Lease_Note item in db.AS_Lease_Note.Where(o => o.LEASE_NUMBER == result.LEASE_NUMBER && o.LEASE_HEADER_ID == id))
                    {
                        result.NoteList.Add(ToNoteModel(item));
                    }
                }                
            }

            return result;
        }


        #region== EntityToModel ==
        public static IntegrationHeaderModel ToIntegrationHeaderModel(AS_Leases_Integration_Header entity, AS_VW_Code_Table doc, AS_Users user
                                                                , AS_Keep_Position position, AS_Code_Table flow, int assetCount = 0)
        {
            return new IntegrationHeaderModel()
            {
                ID = entity.ID,
                TRX_Header_ID = entity.TRX_Header_ID,
                Transaction_Type = entity.Transaction_Type,
                Transaction_Time = entity.Transaction_Time,
                Transaction_TypeName = (doc != null) ? doc.Text : "",
                Flow_Status = entity.Flow_Status,
                Source = entity.Source,
                Book_Type = entity.Book_Type,
                Office_Branch = entity.Office_Branch,
                OriOffice_Branch = entity.OriOffice_Branch,
                Remark = entity.Remark,
                AssetsCount = assetCount,
                Created_ByName = (user != null) ? user.User_Name : "",
                Create_Time = entity.Create_Time,
                Flow_StatusName = (flow != null) ? flow.Text : "",
                Office_BranchName = (position != null) ? position.Keep_Position_Name : ""
            };
        }
        public static ContactModel ToContactModel(AS_Leases_Headers entity, AS_Keep_Position position) {
            if (entity == null) {
                return null;
            }
            return new ContactModel()
            {
                ID = entity.ID,
                TRX_Header_ID = entity.TRX_Header_ID,
                LEASE_NUMBER = entity.LEASE_NUMBER,
                OLD_LEASE_NUMBER = entity.OLD_LEASE_NUMBER,
                TRANSACTION_TYPE = entity.TRANSACTION_TYPE,
                OFFICER_BRANCH = entity.OFFICER_BRANCH,
                LEASE_TYPE = entity.LEASE_TYPE,
                LEASE_TRANSACTION_TYPE = entity.LEASE_TRANSACTION_TYPE,
                LEASE_STATUS = entity.LEASE_STATUS,
                PAYMENT_METHOD = entity.PAYMENT_METHOD,
                BUSINESS_TYPE = entity.BUSINESS_TYPE,
                PAYMENT_TYPE = entity.PAYMENT_TYPE,
                CHECK_TYPE = entity.CHECK_TYPE,
                LEASE_INCEPTION_DATE = entity.LEASE_INCEPTION_DATE,
                LEASE_TERMINATION_DATE = entity.LEASE_TERMINATION_DATE,
                ACCOUNT_NUMBER = entity.ACCOUNT_NUMBER,
                DESCRIPTION = entity.DESCRIPTION,
                OfficeBranchName = (position != null) ? position.Keep_Position_Name : "",
                ACCEPT_DATE = entity.ACCEPT_DATE,
                LEASE_CATEGORY = entity.LEASE_CATEGORY,
                LEASE_AREA = entity.LEASE_AREA,
                LEASE_RATE = entity.LEASE_RATE
            };
        }

        public static RentModel ToRentModel(AS_Lease_Rent model)
        {
            RentModel entity = new RentModel();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = int.Parse(model.LEASE_HEADER_ID.Value.ToString());
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.START_DATE = model.START_DATE;
            entity.END_DATE = model.END_DATE;
            entity.CALCULATE_METHOD = model.CALCULATE_METHOD;
            entity.PAYMENT_PERIOD = model.PAYMENT_PERIOD;
            entity.DUE_DAY = (model.DUE_DAY == null) ? "" : model.DUE_DAY.Value.ToString();
            entity.LAST_PAYMENT = model.LAST_PAYMENT;
            entity.NEXT_PAYMENT = model.NEXT_PAYMENT;
            entity.EFFECTIVE_DATE = model.EFFECTIVE_DATE;
            entity.PRICE = (model.PRICE == null) ? 0 : model.PRICE.Value;
            entity.CURRENCY = model.CURRENCY;
            entity.CURRENCY_VALUE = model.CURRENCY_VALUE;
            entity.DESCRIPTION = model.DESCRIPTION;
            return entity;
        }

        public static ManageExpenseModel ToManageExpenseModel(AS_Lease_ManageExpense model)
        {
            ManageExpenseModel entity = new ManageExpenseModel();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = int.Parse(model.LEASE_HEADER_ID.Value.ToString());
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.START_DATE = model.START_DATE;
            entity.END_DATE = model.END_DATE;
            entity.CALCULATE_METHOD = model.CALCULATE_METHOD;
            entity.PAYMENT_PERIOD = model.PAYMENT_PERIOD;
            entity.DUE_DAY = (model.DUE_DAY == null) ? "" : model.DUE_DAY.Value.ToString();
            entity.LAST_PAYMENT = model.LAST_PAYMENT;
            entity.NEXT_PAYMENT = model.NEXT_PAYMENT;
            entity.EFFECTIVE_DATE = model.EFFECTIVE_DATE;
            entity.PRICE = (model.PRICE == null) ? 0 : model.PRICE.Value;
            entity.CURRENCY = model.CURRENCY;
            entity.CURRENCY_VALUE = model.CURRENCY_VALUE;
            entity.DESCRIPTION = model.DESCRIPTION;
            return entity;
        }

        public static LandModel ToLandModel(AS_Leases_Land model)
        {
            LandModel entity = new LandModel();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = int.Parse(model.LEASE_HEADER_ID.Value.ToString());
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.CITY_NAME = model.CITY_NAME;
            entity.DISTRICT_NAME = model.DISTRICT_NAME;
            entity.SECTION_NAME = model.SECTION_NAME;
            entity.SUBSECTION_NAME = model.SUBSECTION_NAME;
            entity.PARENT_LAND_NUMBER = model.PARENT_LAND_NUMBER;
            entity.FILIAL_LAND_NUMBER = model.FILIAL_LAND_NUMBER;
            entity.LAND_USE_TYPE = model.LAND_USE_TYPE;
            entity.AUTHORIZED_AREA = (model.AUTHORIZED_AREA == null) ? 0 : int.Parse(model.AUTHORIZED_AREA.Value.ToString());            
            entity.ANNOUNCE_PRICE = (model.ANNOUNCE_PRICE == null) ? 0 : int.Parse(model.ANNOUNCE_PRICE.Value.ToString());            
            entity.LESSEE_AREA = (model.LESSEE_AREA == null) ? 0 : int.Parse(model.LESSEE_AREA.Value.ToString());            
            entity.LEASE_RATE = (model.LEASE_RATE == null) ? 0 : int.Parse(model.LEASE_RATE.Value.ToString());            
            entity.RENTAL_AMOUNT = (model.RENTAL_AMOUNT == null) ? 0 : int.Parse(model.RENTAL_AMOUNT.Value.ToString());            
            entity.DESCRIPTION = model.DESCRIPTION;
            return entity;
        }

        public static PaymentAccountModel ToPaymentAccountModel(AS_Lease_Payment_Account model)
        {
            PaymentAccountModel entity = new PaymentAccountModel();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = int.Parse(model.LEASE_HEADER_ID.Value.ToString());
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.ACCOUNT = model.ACCOUNT;
            entity.BANK = model.BANK;
            entity.ACCOUNT_NAME = model.ACCOUNT_NAME;
            entity.PAYMENT_METHOD = model.PAYMENT_METHOD;
            entity.DESCRIPTION = model.DESCRIPTION;
            return entity;
        }

        public static NoteModel ToNoteModel(AS_Lease_Note model)
        {
            NoteModel entity = new NoteModel();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = int.Parse(model.LEASE_HEADER_ID.Value.ToString());
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.NOTE_TYPE = model.NOTE_TYPE;
            entity.CALC_START_DATE = model.CALC_START_DATE;
            entity.CALC_END_DATE = model.CALC_END_DATE;
            entity.CALCULATE_METHOD = model.CALCULATE_METHOD;
            entity.PRICE = (model.PRICE == null) ? 0 : model.PRICE.Value;
            entity.PAYMENT_PERIOD = model.PAYMENT_PERIOD;
            entity.DESCRIPTION = model.DESCRIPTION;
            entity.Last_Updated_Time = model.LAST_UPDATE_DATE;
            entity.Create_Time = model.CREATION_DATE;
            return entity;
        }

        public static BuildModel ToBuildModel(AS_Leases_Build model)
        {
            BuildModel entity = new BuildModel();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = int.Parse(model.LEASE_HEADER_ID.Value.ToString());
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.CITY_NAME = model.CITY_NAME;
            entity.DISTRICT_NAME = model.DISTRICT_NAME;
            entity.SECTION_NAME = model.SECTION_NAME;
            entity.SUBSECTION_NAME = model.SUBSECTION_NAME;
            entity.ADDRESS = model.ADDRESS;
            entity.BUSINESS_USE_TYPE = model.BUSINESS_USE_TYPE;
            entity.ATTACHED_BUILD = model.ATTACHED_BUILD;
            entity.HOUSE_ARCADE = model.HOUSE_ARCADE;
            entity.CONVEX = model.CONVEX;
            entity.ARCADE = model.ARCADE;
            entity.COMMON_PART = model.COMMON_PART;
            entity.RIGHT_AREA = model.RIGHT_AREA;
            entity.LEASES_AREA = model.LEASES_AREA;
            entity.FLOOR = model.FLOOR;
            entity.ARCADE_AREA = model.ARCADE_AREA;
            entity.SHARE_PUBLIC_AREA = (model.SHARE_PUBLIC_AREA == null) ? 0 : int.Parse(model.SHARE_PUBLIC_AREA.Value.ToString());
            entity.DESCRIPTION = model.DESCRIPTION;
            return entity;
        }

        public static ParkingModel ToParkingModel(AS_Leases_Build_Parking model)
        {
            ParkingModel entity = new ParkingModel();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = int.Parse(model.LEASE_HEADER_ID.Value.ToString());
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.CITY_NAME = model.CITY_NAME;
            entity.DISTRICT_NAME = model.DISTRICT_NAME;
            entity.SECTION_NAME = model.SECTION_NAME;
            entity.SUBSECTION_NAME = model.SUBSECTION_NAME;
            entity.ADDRESS = model.ADDRESS;
            entity.PARKING_NO = model.PARKING_NO;
            entity.PARKING_USE_TYPE = model.PARKING_USE_TYPE;
            entity.FLOOR = model.FLOOR;
            entity.DESCRIPTION = model.DESCRIPTION;
            return entity;
        }

        public static InsuranceModel ToInsuranceModel(AS_Lease_Insurance model)
        {
            InsuranceModel entity = new InsuranceModel();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = int.Parse(model.LEASE_HEADER_ID.Value.ToString());
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.INSURANCE_TYPE = model.INSURANCE_TYPE;
            entity.COMPANY = model.COMPANY;
            entity.INSURANCE_NO = model.INSURANCE_NO;
            entity.COMPANY_CONTACT = model.COMPANY_CONTACT;
            entity.COMPANY_PHONE = model.COMPANY_PHONE;
            entity.APPLICANT = model.APPLICANT;
            entity.INSURANT = model.INSURANT;
            entity.START_DATE = model.START_DATE;
            entity.END_DATE = model.END_DATE;
            entity.PRICE = (model.PRICE == null) ? 0 : model.PRICE.Value;
            entity.DESCRIPTION = model.DESCRIPTION;
            return entity;
        }

        public static MarginModel ToMarginModel(AS_Lease_Margin model)
        {
            MarginModel entity = new MarginModel();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = int.Parse(model.LEASE_HEADER_ID.Value.ToString());
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.RECEIVED_DATE = model.RECEIVED_DATE;
            entity.RETURN_DATE = model.RETURN_DATE;
            entity.BANK = model.BANK;
            entity.PRICE = (model.PRICE == null) ? 0 : model.PRICE.Value;
            entity.CURRENCY = model.CURRENCY;
            entity.CURRENCY_VALUE = model.CURRENCY_VALUE;
            return entity;
        }
        #endregion

        #region== ModelToEntity ==
        public static AS_Leases_Integration_Header ToIntegrationEntity(IntegrationHeaderModel model) {

            return new AS_Leases_Integration_Header()
            {
                ID = model.ID,
                TRX_Header_ID = model.TRX_Header_ID,
                Transaction_Type = model.Transaction_Type,
                Transaction_Time = model.Transaction_Time,
                Flow_Status = model.Flow_Status,
                Source = model.Source,
                Book_Type = model.Book_Type,
                Office_Branch = model.Office_Branch,
                OriOffice_Branch = model.OriOffice_Branch,
                Remark = model.Remark,
                Created_By = model.Created_By,
                Create_Time = model.Create_Time,
                DisposeReason = model.DisposeReason,
                Last_Updated_By = model.Last_Updated_By,
                Last_Updated_Time = model.Last_Updated_Time
            };
        }

        public static AS_Leases_Headers ToContractEntity(ContactModel model)
        {

            AS_Leases_Headers entity = new AS_Leases_Headers();
            entity.ID = model.ID;
            entity.TRX_Header_ID = model.TRX_Header_ID;
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.OLD_LEASE_NUMBER = model.OLD_LEASE_NUMBER;
            entity.TRANSACTION_TYPE = model.TRANSACTION_TYPE;
            entity.OFFICER_BRANCH = model.OFFICER_BRANCH;
            entity.LEASE_TYPE = model.LEASE_TYPE;
            entity.LEASE_TRANSACTION_TYPE = model.LEASE_TRANSACTION_TYPE;
            entity.LEASE_STATUS = model.LEASE_STATUS;
            entity.PAYMENT_METHOD = model.PAYMENT_METHOD;
            entity.BUSINESS_TYPE = model.BUSINESS_TYPE;
            entity.PAYMENT_TYPE = model.PAYMENT_TYPE;
            entity.CHECK_TYPE = model.CHECK_TYPE;
            entity.LEASE_INCEPTION_DATE = model.LEASE_INCEPTION_DATE;
            entity.LEASE_TERMINATION_DATE = model.LEASE_TERMINATION_DATE;
            entity.ACCOUNT_NUMBER = model.ACCOUNT_NUMBER;
            entity.DESCRIPTION = model.DESCRIPTION;
            entity.ACCEPT_DATE = model.ACCEPT_DATE;
            entity.LEASE_CATEGORY = model.LEASE_CATEGORY;
            entity.LEASE_AREA = model.LEASE_AREA;
            entity.LEASE_RATE = model.LEASE_RATE;
            entity.CREATED_BY = int.Parse(model.CREATED_BY);
            entity.CREATION_DATE = model.CREATED_TIME;
            entity.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
            entity.LAST_UPDATE_DATE = model.Last_Updated_Time;
            return entity;
        }

        public static AS_Lease_Rent ToRentEntity(RentModel model) {
            AS_Lease_Rent entity = new AS_Lease_Rent();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = model.LEASE_HEADER_ID;
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.START_DATE = model.START_DATE;
            entity.END_DATE = model.END_DATE;
            entity.CALCULATE_METHOD = model.CALCULATE_METHOD;
            entity.PAYMENT_PERIOD = model.PAYMENT_PERIOD;
            entity.DUE_DAY = (string.IsNullOrEmpty(model.DUE_DAY))? 0 : int.Parse(model.DUE_DAY);
            entity.LAST_PAYMENT = model.LAST_PAYMENT;
            entity.NEXT_PAYMENT = model.NEXT_PAYMENT;
            entity.EFFECTIVE_DATE = model.EFFECTIVE_DATE;
            entity.PRICE = model.PRICE;
            entity.CURRENCY = model.CURRENCY;
            entity.CURRENCY_VALUE = model.CURRENCY_VALUE;
            entity.DESCRIPTION = model.DESCRIPTION;
            entity.CREATED_BY = int.Parse(model.Created_By);
            entity.CREATION_DATE = DateTime.Now;
            entity.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
            entity.LAST_UPDATE_DATE = DateTime.Now;
            return entity;
        }

        public static AS_Lease_ManageExpense ToManageExpenseEntity(ManageExpenseModel model)
        {
            AS_Lease_ManageExpense entity = new AS_Lease_ManageExpense();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = model.LEASE_HEADER_ID;
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.START_DATE = model.START_DATE;
            entity.END_DATE = model.END_DATE;
            entity.CALCULATE_METHOD = model.CALCULATE_METHOD;
            entity.PAYMENT_PERIOD = model.PAYMENT_PERIOD;
            entity.DUE_DAY = (string.IsNullOrEmpty(model.DUE_DAY)) ? 0 : int.Parse(model.DUE_DAY);
            entity.LAST_PAYMENT = model.LAST_PAYMENT;
            entity.NEXT_PAYMENT = model.NEXT_PAYMENT;
            entity.EFFECTIVE_DATE = model.EFFECTIVE_DATE;
            entity.PRICE = model.PRICE;
            entity.CURRENCY = model.CURRENCY;
            entity.CURRENCY_VALUE = model.CURRENCY_VALUE;
            entity.DESCRIPTION = model.DESCRIPTION;
            entity.CREATED_BY = int.Parse(model.Created_By);
            entity.CREATION_DATE = DateTime.Now;
            entity.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
            entity.LAST_UPDATE_DATE = DateTime.Now;
            return entity;
        }

        public static AS_Leases_Land ToLandEntity(LandModel model)
        {
            AS_Leases_Land entity = new AS_Leases_Land();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = model.LEASE_HEADER_ID;
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.CITY_NAME = model.CITY_NAME;
            entity.DISTRICT_NAME = model.DISTRICT_NAME;
            entity.SECTION_NAME = model.SECTION_NAME;
            entity.SUBSECTION_NAME = model.SUBSECTION_NAME;
            entity.PARENT_LAND_NUMBER = model.PARENT_LAND_NUMBER;
            entity.FILIAL_LAND_NUMBER = model.FILIAL_LAND_NUMBER;
            entity.LAND_USE_TYPE = model.LAND_USE_TYPE;
            entity.AUTHORIZED_AREA = model.AUTHORIZED_AREA;
            entity.ANNOUNCE_PRICE = model.ANNOUNCE_PRICE;
            entity.LESSEE_AREA = model.LESSEE_AREA;
            entity.LEASE_RATE = model.LEASE_RATE;
            entity.RENTAL_AMOUNT = model.RENTAL_AMOUNT;
            entity.DESCRIPTION = model.DESCRIPTION;
            entity.CREATED_BY = int.Parse(model.Created_By);
            entity.CREATION_DATE = DateTime.Now;
            entity.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
            entity.LAST_UPDATE_DATE = DateTime.Now;
            return entity;
        }

        public static AS_Lease_Margin ToMarginEntity(MarginModel model)
        {
            AS_Lease_Margin entity = new AS_Lease_Margin();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = model.LEASE_HEADER_ID;
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.RECEIVED_DATE = model.RECEIVED_DATE;
            entity.RETURN_DATE = model.RETURN_DATE;
            entity.BANK = model.BANK;
            entity.PRICE = model.PRICE;
            entity.CURRENCY = model.CURRENCY;
            entity.CURRENCY_VALUE = model.CURRENCY_VALUE;
            entity.CREATED_BY = int.Parse(model.Created_By);
            entity.CREATION_DATE = DateTime.Now;
            entity.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
            entity.LAST_UPDATE_DATE = DateTime.Now;
            return entity;
        }

        public static AS_Lease_Insurance ToInsuranceEntity(InsuranceModel model)
        {
            AS_Lease_Insurance entity = new AS_Lease_Insurance();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = model.LEASE_HEADER_ID;
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.INSURANCE_TYPE = model.INSURANCE_TYPE;
            entity.COMPANY = model.COMPANY;
            entity.INSURANCE_NO = model.INSURANCE_NO;
            entity.COMPANY_CONTACT = model.COMPANY_CONTACT;
            entity.COMPANY_PHONE = model.COMPANY_PHONE;
            entity.APPLICANT = model.APPLICANT;
            entity.INSURANT = model.INSURANT;
            entity.START_DATE = model.START_DATE;
            entity.END_DATE = model.END_DATE;
            entity.PRICE = model.PRICE;
            entity.DESCRIPTION = model.DESCRIPTION;
            entity.CREATED_BY = int.Parse(model.Created_By);
            entity.CREATION_DATE = DateTime.Now;
            entity.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
            entity.LAST_UPDATE_DATE = DateTime.Now;
            return entity;
        }

        public static AS_Lease_Payment_Account ToPaymentAccountEntity(PaymentAccountModel model)
        {
            AS_Lease_Payment_Account entity = new AS_Lease_Payment_Account();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = model.LEASE_HEADER_ID;
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.ACCOUNT = model.ACCOUNT;
            entity.BANK = model.BANK;
            entity.ACCOUNT_NAME = model.ACCOUNT_NAME;
            entity.PAYMENT_METHOD = model.PAYMENT_METHOD;
            entity.DESCRIPTION = model.DESCRIPTION;
            entity.CREATED_BY = int.Parse(model.Created_By);
            entity.CREATION_DATE = DateTime.Now;
            entity.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
            entity.LAST_UPDATE_DATE = DateTime.Now;
            return entity;
        }

        public static AS_Lease_Note ToNoteEntity(NoteModel model)
        {
            AS_Lease_Note entity = new AS_Lease_Note();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = model.LEASE_HEADER_ID;
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.NOTE_TYPE = model.NOTE_TYPE;
            entity.CALC_START_DATE = model.CALC_START_DATE;
            entity.CALC_END_DATE = model.CALC_END_DATE;
            entity.CALCULATE_METHOD = model.CALCULATE_METHOD;
            entity.PRICE = model.PRICE;
            entity.PAYMENT_PERIOD = model.PAYMENT_PERIOD;
            entity.DESCRIPTION = model.DESCRIPTION;
            entity.CREATED_BY = int.Parse(model.Created_By);
            entity.CREATION_DATE = DateTime.Now;
            entity.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
            entity.LAST_UPDATE_DATE = DateTime.Now;
            return entity;
        }

        public static AS_Leases_Build ToBuildEntity(BuildModel model)
        {
            AS_Leases_Build entity = new AS_Leases_Build();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = model.LEASE_HEADER_ID;
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.CITY_NAME = model.CITY_NAME;
            entity.DISTRICT_NAME = model.DISTRICT_NAME;
            entity.SECTION_NAME = model.SECTION_NAME;
            entity.SUBSECTION_NAME = model.SUBSECTION_NAME;
            entity.ADDRESS = model.ADDRESS;
            entity.BUSINESS_USE_TYPE = model.BUSINESS_USE_TYPE;
            entity.ATTACHED_BUILD = model.ATTACHED_BUILD;
            entity.HOUSE_ARCADE = model.HOUSE_ARCADE;
            entity.CONVEX = model.CONVEX;
            entity.ARCADE = model.ARCADE;
            entity.COMMON_PART = model.COMMON_PART;            
            entity.RIGHT_AREA = model.RIGHT_AREA;
            entity.LEASES_AREA = model.LEASES_AREA;
            entity.FLOOR = model.FLOOR;
            entity.ARCADE_AREA = model.ARCADE_AREA;
            entity.SHARE_PUBLIC_AREA = model.SHARE_PUBLIC_AREA;
            entity.DESCRIPTION = model.DESCRIPTION;
            entity.CREATED_BY = int.Parse(model.Created_By);
            entity.CREATION_DATE = DateTime.Now;
            entity.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
            entity.LAST_UPDATE_DATE = DateTime.Now;
            return entity;
        }

        public static AS_Leases_Build_Parking ToParkingEntity(ParkingModel model)
        {
            AS_Leases_Build_Parking entity = new AS_Leases_Build_Parking();
            entity.ID = model.ID;
            entity.LEASE_HEADER_ID = model.LEASE_HEADER_ID;
            entity.LEASE_NUMBER = model.LEASE_NUMBER;
            entity.CITY_NAME = model.CITY_NAME;
            entity.DISTRICT_NAME = model.DISTRICT_NAME;
            entity.SECTION_NAME = model.SECTION_NAME;
            entity.SUBSECTION_NAME = model.SUBSECTION_NAME;
            entity.PARKING_NO = model.PARKING_NO;
            entity.PARKING_USE_TYPE = model.PARKING_USE_TYPE;
            entity.FLOOR = model.FLOOR;
            entity.ADDRESS = model.ADDRESS;
            entity.DESCRIPTION = model.DESCRIPTION;
            entity.CREATED_BY = int.Parse(model.Created_By);
            entity.CREATION_DATE = DateTime.Now;
            entity.LAST_UPDATED_BY = int.Parse(model.Last_Updated_By);
            entity.LAST_UPDATE_DATE = DateTime.Now;
            return entity;
        }



        #endregion
    }
}
