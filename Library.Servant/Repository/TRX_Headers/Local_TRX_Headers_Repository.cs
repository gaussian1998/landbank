﻿using System;
using System.Linq.Expressions;
using Library.Entity.Edmx;
using Library.Servant.Repository.TRX_Headers.Models;

namespace Library.Servant.Repository.TRX_Headers
{
    public class Local_TRX_Headers_Repository : LocalGenericRepository<AS_TRX_Headers, ViewModel, ViewModel, ViewModel, ViewModel>
    {
        public override Expression<Func<AS_TRX_Headers, bool>> Predicate(ViewModel model)
        {
            return Expression(m => m.ID == model.ID);
        }

        public override Expression<Func<AS_TRX_Headers, bool>> Predicate(int ID)
        {
            return Expression( m => m.ID == ID );
        }
    }
}