﻿using Library.Entity.Edmx;
using Library.Servant.Repository.TRX_Headers.Models;

namespace Library.Servant.Repository.TRX_Headers
{
    public class Remote_TRX_Headers_Repository : RemoteGenericRepository<AS_TRX_Headers, ViewModel, ViewModel, ViewModel, ViewModel>
    {
        public Remote_TRX_Headers_Repository()
            :base("TRX_Headers_Repository")
        {}
    }
}