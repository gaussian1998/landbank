﻿using Library.Servant.Repository.Models;
using System;

namespace Library.Servant.Repository.TRX_Headers.Models
{
    public class ViewModel : AbstractCommunicateModel
    {
        public int ID { get; set; }
        public string Form_Number { get; set; }
        public string Office_Branch { get; set; }
        public string Book_Type_Code { get; set; }
        public string Transaction_Type { get; set; }
        public string Transaction_Status { get; set; }
        public System.DateTime Transaction__Datetime_Entered { get; set; }
        public Nullable<System.DateTime> Accounting_Datetime { get; set; }
        public string Description { get; set; }
        public string Amotized_Adjustment_Flag { get; set; }
        public Nullable<System.DateTime> Create_Time { get; set; }
        public Nullable<decimal> Created_By { get; set; }
        public string Created_By_Name { get; set; }
        public Nullable<System.DateTime> Last_UpDatetimed_Time { get; set; }
        public Nullable<decimal> Last_UpDatetimed_By { get; set; }
        public string Last_UpDatetimed_By_Name { get; set; }
    }
}