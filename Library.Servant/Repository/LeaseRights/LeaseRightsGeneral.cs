﻿using Library.Entity.Edmx;
using Library.Servant.Servant.MP.MPModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Library.Servant.Repository.LeaseRights
{
    public static class LeaseRightsGeneral
    {
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static IEnumerable<MPHeaderModel> GetIndex(SearchModel condition)
        {
            IEnumerable<MPHeaderModel> result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_LeaseRights_Header
                            join mc in
                            (
                               from mr in db.AS_LeaseRights_Record
                               group mr by new { mr.TRX_Header_ID } into mrg
                               select new
                               {
                                   TRX_Header_ID = mrg.Key.TRX_Header_ID,
                                   Count = mrg.Count()
                               }
                            ) on mh.TRX_Header_ID equals mc.TRX_Header_ID into mhtemp
                            from mc in mhtemp.DefaultIfEmpty()
                            join doc in
                            (
                                from docc in db.AS_VW_Code_Table
                                where docc.Class == "LeaseRightsForm"
                                select docc
                            ) on mh.Transaction_Type equals doc.Code_ID into tdoc
                            from doc in tdoc.DefaultIfEmpty()
                            join so in db.AS_Code_Table
                            on new { Class = mh.Transaction_Type, Code_ID = mh.Source } equals new { so.Class,so.Code_ID } into tso
                            from so in tso.DefaultIfEmpty()
                            join u in db.AS_Users on mh.Created_By equals u.ID.ToString() into tu
                            from u in tu.DefaultIfEmpty()
                            join o in db.AS_Keep_Position on mh.Office_Branch equals o.Keep_Position_Code into to
                            from o in to.DefaultIfEmpty()
                            join f in
                            (
                               from ff in db.AS_Code_Table
                               where ff.Class == "P02"
                               select ff
                            ) on mh.Flow_Status equals f.Code_ID into tf
                            from f in tf.DefaultIfEmpty()
                            select new { mh, mc ,doc,so ,u , o , f};

                if (!string.IsNullOrEmpty(condition.Transaction_Type))
                {
                    query = query.Where(m => m.mh.Transaction_Type == condition.Transaction_Type);
                }
                if(!string.IsNullOrEmpty(condition.Source))
                {
                    query = query.Where(m => m.mh.Source == condition.Source);
                }
                if(!string.IsNullOrEmpty(condition.TRXHeaderIDBgn))
                {
                    query = query.Where(m => String.Compare(m.mh.TRX_Header_ID, condition.TRXHeaderIDBgn) >= 0);
                }
                if (!string.IsNullOrEmpty(condition.TRXHeaderIDEnd))
                {
                    query = query.Where(m => String.Compare(m.mh.TRX_Header_ID, condition.TRXHeaderIDEnd) <= 0);
                }
                if(!string.IsNullOrEmpty(condition.BookTypeCode))
                {
                    query = query.Where(m => m.mh.Book_Type == condition.BookTypeCode);
                }
                if(condition.BeginDate != null)
                {
                    query = query.Where(m => m.mh.Create_Time >= condition.BeginDate);
                }
                if (condition.EndDate != null)
                {
                    query = query.Where(m => m.mh.Create_Time <= condition.EndDate);
                }
                if(!string.IsNullOrEmpty(condition.FlowStatus))
                {
                    string[] flows = condition.FlowStatus.Split(',');
                    query = query.Where(m => flows.Contains(m.mh.Flow_Status));
                }
                if(!string.IsNullOrEmpty(condition.OfficeBranch))
                {
                    query = query.Where(m => m.mh.Office_Branch == condition.OfficeBranch);
                }
                if(!string.IsNullOrEmpty(condition.Remark))
                {
                    query = query.Where(m => m.mh.Remark.Contains(condition.Remark));
                }
                
                result = query.ToList().OrderByDescending(m=>m.mh.Create_Time).Select(m => new MPHeaderModel()
                {
                    ID = m.mh.ID,
                    TRX_Header_ID = m.mh.TRX_Header_ID,
                    Transaction_Type = m.mh.Transaction_Type,
                    Transaction_TypeName = (m.doc != null) ? m.doc.Text : "",
                    Flow_Status = m.mh.Flow_Status,
                    Source = m.mh.Source,
                    SourceName = (m.so != null) ? m.so.Text :"",
                    Book_Type = m.mh.Book_Type,
                    Office_Branch = m.mh.Office_Branch,
                    Remark = m.mh.Remark,
                    AssetsCount = (m.mc != null) ? m.mc.Count : 0,
                    Created_ByName = (m.u != null) ? m.u.User_Name : "",
                    Create_Time = m.mh.Create_Time,
                    Flow_StatusName = (m.f != null) ? m.f.Text : "",
                    Office_BranchName = (m.o != null) ? m.o.Keep_Position_Name : ""
                });
            }

            return result;
        }

        /// <summary>
        /// 取的單據明細
        /// </summary>
        /// <param name="TRXHeaderID"></param>
        /// <returns></returns>
        public static MPModel GetDetail(string TRXHeaderID)
        {
            MPModel result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in
                            (
                                from h in db.AS_LeaseRights_Header
                                where h.TRX_Header_ID == TRXHeaderID
                                select h
                            )
                            join mas in db.AS_LeaseRights_Record on mh.TRX_Header_ID equals mas.TRX_Header_ID into temp
                            from mas in temp.DefaultIfEmpty()
                            join doc in
                            (
                                from docc in db.AS_VW_Code_Table
                                where docc.Class == "LeaseRightsForm"
                                select docc
                            ) on mh.Transaction_Type equals doc.Code_ID into tdoc
                            from doc in tdoc.DefaultIfEmpty()
                            join u in db.AS_Users on mh.Created_By equals u.User_Code into tu
                            from u in tu.DefaultIfEmpty()
                            join o in db.AS_Keep_Position on mh.Office_Branch equals o.Keep_Position_Code into to
                            from o in to.DefaultIfEmpty()
                            join f in
                            (
                               from ff in db.AS_Code_Table
                               where ff.Class == "P02"
                               select ff
                            ) on mh.Flow_Status equals f.Code_ID into tf
                            from f in tf.DefaultIfEmpty()
                            join bu in db.AS_Users on mas.Assigned_ID equals bu.User_Code into tbu
                            from bu in tbu.DefaultIfEmpty()
                            join bo in db.AS_Keep_Position on mas.Assigned_Branch equals bo.Keep_Position_Code into tbo
                            from bo in tbo.DefaultIfEmpty()
                            select new { mh, mas ,doc, u, o, f, bu, bo};

                if (query != null && query.Count() > 0)
                {
                    result = new MPModel()
                    {
                        Header = query.ToList().Select(o => ToMPHeaderModel(o.mh,o.doc,o.u,o.o,o.f)).First(),
                        Assets = query.ToList().Select(o => ToAssetModel(o.mas,o.bo,o.bu))
                    };
                }
            }

            return result;
        }


        /// <summary>
        /// 取得表單編號
        /// </summary>
        /// <returns></returns>
        public static string GetFormCode(string Type)
        {
            string result = "";
            using (var db = new AS_LandBankEntities())
            {
                if(db.AS_VW_Code_Table.Any(o => o.Class == "LeaseRightsForm" && o.Class_Name == Type))
                {
                    result = db.AS_VW_Code_Table.First(o => o.Class == "LeaseRightsForm" && o.Class_Name == Type).Code_ID;
                }
            }
            return result;
        }

        /// <summary>
        /// 取得controllername
        /// </summary>
        /// <returns></returns>
        public static string GetController(string FormCode)
        {
            string result = "";
            using (var db = new AS_LandBankEntities())
            {
                if (db.AS_VW_Code_Table.Any(o => o.Class == "LeaseRightsForm" && o.Code_ID == FormCode))
                {
                    result = db.AS_VW_Code_Table.First(o => o.Class == "LeaseRightsForm" && o.Code_ID == FormCode).Class_Name;
                }
            }
            return result;
        }

        #region== EntityToModel ==
        public static MPHeaderModel ToMPHeaderModel(AS_LeaseRights_Header entity, AS_VW_Code_Table doc, AS_Users user,AS_Keep_Position position, AS_Code_Table flow,int assetCount = 0)
        {
            return new MPHeaderModel()
            {
                ID = entity.ID,
                TRX_Header_ID = entity.TRX_Header_ID,
                Transaction_Type = entity.Transaction_Type,
                Transaction_TypeName = (doc != null) ? doc.Text : "",
                Flow_Status = entity.Flow_Status,
                Source = entity.Source,
                Book_Type = entity.Book_Type,
                Office_Branch = entity.Office_Branch,
                Remark = entity.Remark,
                AssetsCount = assetCount,
                Created_ByName = (user != null) ? user.User_Name : "",
                Create_Time = entity.Create_Time,
                Flow_StatusName = (flow != null) ? flow.Text : "",
                Office_BranchName = (position != null) ? position.Keep_Position_Name : "",
                DisposeReason = entity.DisposeReason
            };
        }

        public static AssetModel ToAssetModel(AS_LeaseRights entity, AS_Keep_Position position = null, AS_Users user = null)
        {
            return new AssetModel()
            {
                ID = entity.ID,
                Assets_Parent_Number = entity.Assets_Parent_Number,
                IsOversea = entity.IsOversea,
                Asset_Category_Code = entity.Asset_Category_Code,
                Assets_Name = entity.Assets_Name,
                Assets_Alias = entity.Assets_Alias,
                Asset_Number = entity.Asset_Number,
                Assets_Original_ID = entity.Assets_Original_ID,
                Life_Years = entity.Life_Years,
                Assets_Category_ID = entity.Assets_Category_ID,
                Deprn_Method_Code = entity.Deprn_Method_Code,
                Date_Placed_In_Service = (entity.Date_Placed_In_Service.HasValue) ? entity.Date_Placed_In_Service.Value : entity.Date_Placed_In_Service,
                Assets_Unit = entity.Assets_Unit,
                Location_Disp = entity.Location_Disp,
                Assigned_Branch = entity.Assigned_Branch,
                Assigned_BranchName = (position != null) ? position.Keep_Position_Name : "",
                Assigned_ID = entity.Assigned_ID,
                Assigned_IDName = (user != null) ? user.User_Name : "",
                PO_Number = entity.PO_Number,
                PO_Description = entity.PO_Description,
                Model_Number = entity.Model_Number,
                Transaction_Date = (entity.Transaction_Date.HasValue) ? entity.Transaction_Date.Value : entity.Transaction_Date,
                Assets_Fixed_Cost = (entity.Assets_Fixed_Cost.HasValue) ? entity.Assets_Fixed_Cost.Value : 0,
                Deprn_Reserve = (entity.Deprn_Reserve.HasValue) ? entity.Deprn_Reserve.Value : 0,
                Salvage_Value = (entity.Salvage_Value.HasValue) ? entity.Salvage_Value.Value : 0,
                Currency = entity.Currency,
                Description = entity.Description,
                Post_Date = entity.Post_Date,
                Created_By = entity.Created_By,
                Create_Time = entity.Create_Time,
                Last_Updated_By = entity.Last_Updated_By,
                Last_Updated_Time = entity.Last_Updated_Time,
                Not_Deprn_Flag = entity.Not_Deprn_Flag
            };
        }

        public static AssetModel ToAssetModel(AS_LeaseRights_Record entity, AS_Keep_Position position = null, AS_Users user = null)
        {
            AssetModel Result = null;
            if (entity != null)
            {
                Result = new AssetModel()
                {
                    ID = entity.ID,
                    Assets_Parent_Number = entity.Assets_Parent_Number,
                    IsOversea = entity.IsOversea,
                    Asset_Category_Code = entity.Asset_Category_Code,
                    Assets_Name = entity.Assets_Name,
                    Assets_Alias = entity.Assets_Alias,
                    Asset_Number = entity.Asset_Number,
                    Assets_Original_ID = entity.Assets_Original_ID,
                    Life_Years = entity.Life_Years,
                    Assets_Category_ID = entity.Assets_Category_ID,
                    Deprn_Method_Code = entity.Deprn_Method_Code,
                    Date_Placed_In_Service = (entity.Date_Placed_In_Service.HasValue) ? entity.Date_Placed_In_Service.Value : entity.Date_Placed_In_Service,
                    Assets_Unit = entity.Assets_Unit,
                    Location_Disp = entity.Location_Disp,
                    Assigned_Branch = entity.Assigned_Branch,
                    Assigned_BranchName = (position != null) ? position.Keep_Position_Name : "",
                    Assigned_ID = entity.Assigned_ID,
                    Assigned_IDName = (user != null) ? user.User_Name : "",
                    Assigned_IDEmail = (user != null) ? user.Email :"",
                    Assigned_IDPhone = (user != null) ? "" : "",
                    PO_Number = entity.PO_Number,
                    PO_Description = entity.PO_Description,
                    Model_Number = entity.Model_Number,
                    Transaction_Date = (entity.Transaction_Date.HasValue) ? entity.Transaction_Date.Value : entity.Transaction_Date,
                    Assets_Fixed_Cost = (entity.Assets_Fixed_Cost.HasValue) ? entity.Assets_Fixed_Cost.Value : 0,
                    Deprn_Reserve = (entity.Deprn_Reserve.HasValue) ? entity.Deprn_Reserve.Value : 0,
                    Salvage_Value = (entity.Salvage_Value.HasValue) ? entity.Salvage_Value.Value : 0,
                    Currency = entity.Currency,
                    Description = entity.Description,
                    AccessoryEquipment = entity.AccessoryEquipment,
                    LicensePlateNumber = entity.LicensePlateNumber,
                    MachineCode = entity.MachineCode,
                    PropertyUnit = entity.PropertyUnit,
                    Post_Date = entity.Post_Date,
                    ImportNum = entity.Import_Num,
                    Created_By = entity.Created_By,
                    Create_Time = entity.Create_Time,
                    Last_Updated_By = entity.Last_Updated_By,
                    Last_Updated_Time = entity.Last_Updated_Time,
                    InventoryCheck = entity.InventoryCheck,
                    InventoryCheckMemo = entity.InventoryCheckMemo,
                    AssignedSignUp = entity.AssignedSignUp
                };
            }

            return Result;
        }


        public static MPHisModel ToMPHisModel(AS_LeaseRights_Record entity)
        {
            return new MPHisModel()
            {
                ID = entity.ID,
                TRX_Header_ID = entity.TRX_Header_ID,
                Assets_Parent_Number = entity.Assets_Parent_Number,
                IsOversea = entity.IsOversea,
                Asset_Category_Code = entity.Asset_Category_Code,
                Assets_Name = entity.Assets_Name,
                Assets_Alias = entity.Assets_Alias,
                Asset_Number = entity.Asset_Number,
                Assets_Original_ID = entity.Assets_Original_ID,
                Life_Years = entity.Life_Years,
                Assets_Category_ID = entity.Assets_Category_ID,
                Deprn_Method_Code = entity.Deprn_Method_Code,
                Date_Placed_In_Service = entity.Date_Placed_In_Service.Value,
                Assets_Unit = entity.Assets_Unit,
                Location_Disp = entity.Location_Disp,
                Assigned_Branch = entity.Assigned_Branch,
                Assigned_ID = entity.Assigned_ID,
                PO_Number = entity.PO_Number,
                PO_Description = entity.PO_Description,
                Model_Number = entity.Model_Number,
                Transaction_Date = entity.Transaction_Date.Value,
                Assets_Fixed_Cost = entity.Assets_Fixed_Cost.Value,
                Deprn_Reserve = entity.Deprn_Reserve.Value,
                Salvage_Value = entity.Salvage_Value.Value,
                Currency = entity.Currency,
                Description = entity.Description,
                AccessoryEquipment = entity.AccessoryEquipment,
                PropertyUnit = entity.PropertyUnit,
                MachineCode = entity.MachineCode,
                LicensePlateNumber = entity.LicensePlateNumber,
                Post_Date = entity.Post_Date,
                ImportNum = entity.Import_Num
            };
        }
        #endregion

        #region== ModelToEntity ==
        public static AS_LeaseRights_Header ToAS_LeaseRights_Header(MPHeaderModel model)
        {
            return new AS_LeaseRights_Header()
            {
                ID = model.ID,
                TRX_Header_ID = model.TRX_Header_ID,
                Transaction_Type = model.Transaction_Type,
                Flow_Status = model.Flow_Status,
                Source = model.Source,
                Book_Type = model.Book_Type,
                Office_Branch = model.Office_Branch,
                Remark = model.Remark,
                DisposeReason = (model.DisposeReason != null) ? model.DisposeReason : "",
                Created_By = model.Created_By,
                Create_Time = model.Create_Time,
                Last_Updated_By = model.Last_Updated_By,
                Last_Updated_Time = model.Last_Updated_Time
            };
        }

        public static AS_LeaseRights ToAS_LeaseRights(AssetModel model)
        {
            return new AS_LeaseRights()
            {
                ID = model.ID,
                Assets_Parent_Number = model.Assets_Parent_Number,
                IsOversea = model.IsOversea,
                Asset_Category_Code = model.Asset_Category_Code,
                Assets_Name = model.Assets_Name,
                Assets_Alias = model.Assets_Alias,
                Asset_Number = model.Asset_Number,
                Assets_Original_ID = model.Assets_Original_ID,
                Life_Years = model.Life_Years,
                Assets_Category_ID = model.Assets_Category_ID,
                Deprn_Method_Code = model.Deprn_Method_Code,
                Date_Placed_In_Service = model.Date_Placed_In_Service.Value,
                Assets_Unit = model.Assets_Unit,
                Location_Disp = model.Location_Disp,
                Assigned_Branch = model.Assigned_Branch,
                Assigned_ID = model.Assigned_ID,
                PO_Number = model.PO_Number,
                PO_Description = model.PO_Description,
                Model_Number = model.Model_Number,
                Transaction_Date = model.Transaction_Date.Value,
                Assets_Fixed_Cost = model.Assets_Fixed_Cost,
                Deprn_Reserve = model.Deprn_Reserve,
                Salvage_Value = model.Salvage_Value,
                Currency = model.Currency,
                Description = model.Description,
                Post_Date = model.Post_Date,
                Created_By = model.Created_By,
                Create_Time = model.Create_Time,
                Last_Updated_By = model.Last_Updated_By,
                Last_Updated_Time = model.Last_Updated_Time
            };
        }

        public static AS_LeaseRights ToAS_LeaseRightsFromAS_LeaseRights_Record(AS_LeaseRights_Record his)
        {
            return new AS_LeaseRights()
            {
                Assets_Parent_Number = his.Assets_Parent_Number,
                IsOversea = his.IsOversea,
                Asset_Category_Code = his.Asset_Category_Code,
                Assets_Name = his.Assets_Name,
                Assets_Alias = his.Assets_Alias,
                Asset_Number = his.Asset_Number,
                Assets_Original_ID = his.Assets_Original_ID,
                Life_Years = his.Life_Years,
                Assets_Category_ID = his.Assets_Category_ID,
                Deprn_Method_Code = his.Deprn_Method_Code,
                Date_Placed_In_Service = his.Date_Placed_In_Service,
                Assets_Unit = his.Assets_Unit,
                Location_Disp = his.Location_Disp,
                Assigned_Branch = his.Assigned_Branch,
                Assigned_ID = his.Assigned_ID,
                PO_Number = his.PO_Number,
                PO_Description = his.PO_Description,
                Model_Number = his.Model_Number,
                Transaction_Date = his.Transaction_Date,
                Assets_Fixed_Cost = his.Assets_Fixed_Cost,
                Deprn_Reserve = his.Deprn_Reserve,
                Salvage_Value = his.Salvage_Value,
                Currency = his.Currency,
                Description = his.Description,
                Post_Date = his.Post_Date,
                Created_By = his.Created_By,
                Create_Time = his.Create_Time,
                Last_Updated_By = his.Last_Updated_By
            };
        }

        public static AS_LeaseRights_Record ToAS_LeaseRights_Record(string TRX_Header_ID,AssetModel model)
        {
            return new AS_LeaseRights_Record()
            {
                TRX_Header_ID = (!string.IsNullOrEmpty(TRX_Header_ID)) ? TRX_Header_ID : "",
                Assets_Parent_Number = (!string.IsNullOrEmpty(model.Assets_Parent_Number)) ? model.Assets_Parent_Number : "",
                IsOversea = (!string.IsNullOrEmpty(model.IsOversea)) ? model.IsOversea : "",
                Asset_Category_Code = (!string.IsNullOrEmpty(model.Asset_Category_Code)) ? model.Asset_Category_Code : "",
                Assets_Name = (!string.IsNullOrEmpty(model.Assets_Name)) ? model.Assets_Name : "",
                Assets_Alias = (!string.IsNullOrEmpty(model.Assets_Alias)) ? model.Assets_Alias : "",
                Asset_Number = (!string.IsNullOrEmpty(model.Asset_Number)) ? model.Asset_Number : "",
                Assets_Original_ID = (!string.IsNullOrEmpty(model.Assets_Original_ID)) ? model.Assets_Original_ID : "",
                Life_Years = model.Life_Years,
                Assets_Category_ID = (!string.IsNullOrEmpty(model.Assets_Category_ID)) ? model.Assets_Category_ID : "",
                Deprn_Method_Code = (!string.IsNullOrEmpty(model.Deprn_Method_Code)) ? model.Deprn_Method_Code : "",
                Date_Placed_In_Service = model.Date_Placed_In_Service,
                Assets_Unit = model.Assets_Unit,
                Location_Disp = (!string.IsNullOrEmpty(model.Location_Disp)) ? model.Location_Disp : "",
                Assigned_Branch = (!string.IsNullOrEmpty(model.Assigned_Branch)) ? model.Assigned_Branch : "",
                Assigned_ID = (!string.IsNullOrEmpty(model.Assigned_ID)) ? model.Assigned_ID : "",
                PO_Number = (!string.IsNullOrEmpty(model.PO_Number)) ? model.PO_Number : "",
                PO_Description = (!string.IsNullOrEmpty(model.PO_Description)) ? model.PO_Description : "",
                Model_Number = (!string.IsNullOrEmpty(model.Model_Number)) ? model.Model_Number : "",
                Transaction_Date = model.Transaction_Date,
                Assets_Fixed_Cost = model.Assets_Fixed_Cost,
                Deprn_Reserve = model.Deprn_Reserve,
                Salvage_Value = model.Salvage_Value,
                Currency = !string.IsNullOrEmpty(model.Currency) ? model.Currency : "",
                Description = (!string.IsNullOrEmpty(model.Description)) ? model.Description : "",
                AccessoryEquipment = model.AccessoryEquipment,
                PropertyUnit = model.PropertyUnit,
                MachineCode = model.MachineCode,
                LicensePlateNumber = model.LicensePlateNumber,
                Post_Date = model.Post_Date,
                Import_Num = (!string.IsNullOrEmpty(model.ImportNum)) ? model.ImportNum : "",
                Created_By = model.Created_By,
                Create_Time = model.Create_Time,
                Last_Updated_By = (!string.IsNullOrEmpty(model.Last_Updated_By)) ? model.Last_Updated_By : "",
                Last_Updated_Time = model.Last_Updated_Time,
                AssignedSignUp = (!string.IsNullOrEmpty(model.AssignedSignUp)) ? model.AssignedSignUp : ""
            };
        }

        public static AS_LeaseRights_Record ToAS_LeaseRights_Record(MPHisModel model)
        {
            return new AS_LeaseRights_Record()
            {
                TRX_Header_ID = (!string.IsNullOrEmpty(model.TRX_Header_ID)) ? model.TRX_Header_ID : "",
                Assets_Parent_Number = (!string.IsNullOrEmpty(model.Assets_Parent_Number)) ? model.Assets_Parent_Number : "",
                IsOversea = (!string.IsNullOrEmpty(model.IsOversea)) ? model.IsOversea : "",
                Asset_Category_Code = (!string.IsNullOrEmpty(model.Asset_Category_Code)) ? model.Asset_Category_Code : "",
                Assets_Name = (!string.IsNullOrEmpty(model.Assets_Name)) ? model.Assets_Name : "",
                Assets_Alias = (!string.IsNullOrEmpty(model.Assets_Alias)) ? model.Assets_Alias : "",
                Asset_Number = (!string.IsNullOrEmpty(model.Asset_Number)) ? model.Asset_Number : "",
                Assets_Original_ID = (!string.IsNullOrEmpty(model.Assets_Original_ID)) ? model.Assets_Original_ID : "",
                Life_Years = model.Life_Years,
                Assets_Category_ID = (!string.IsNullOrEmpty(model.Assets_Category_ID)) ? model.Assets_Category_ID : "",
                Deprn_Method_Code = (!string.IsNullOrEmpty(model.Deprn_Method_Code)) ? model.Deprn_Method_Code : "",
                Date_Placed_In_Service = model.Date_Placed_In_Service.Value,
                Assets_Unit = model.Assets_Unit,
                Location_Disp = (!string.IsNullOrEmpty(model.Location_Disp)) ? model.Location_Disp : "",
                Assigned_Branch = (!string.IsNullOrEmpty(model.Assigned_Branch)) ? model.Assigned_Branch : "",
                Assigned_ID = (!string.IsNullOrEmpty(model.Assigned_ID)) ? model.Assigned_ID : "",
                PO_Number = (!string.IsNullOrEmpty(model.PO_Number)) ? model.PO_Number : "",
                PO_Description = (!string.IsNullOrEmpty(model.PO_Description)) ? model.PO_Description : "",
                Model_Number = (!string.IsNullOrEmpty(model.Model_Number)) ? model.Model_Number : "",
                Transaction_Date = model.Transaction_Date.Value,
                Assets_Fixed_Cost = model.Assets_Fixed_Cost,
                Deprn_Reserve = model.Deprn_Reserve,
                Salvage_Value = model.Salvage_Value,
                Currency = model.Currency,
                Description = (!string.IsNullOrEmpty(model.Model_Number)) ? model.Description : "",
                AccessoryEquipment = model.AccessoryEquipment,
                PropertyUnit = model.PropertyUnit,
                MachineCode = model.MachineCode,
                LicensePlateNumber = model.LicensePlateNumber,
                Post_Date = model.Post_Date,
                Import_Num = (!string.IsNullOrEmpty(model.ImportNum)) ? model.ImportNum : "",
                Created_By = model.Created_By,
                Create_Time = model.Create_Time,
                Last_Updated_By = (!string.IsNullOrEmpty(model.Last_Updated_By)) ? model.Last_Updated_By : "",
                Last_Updated_Time = model.Last_Updated_Time
            };
        }
        #endregion
    }
}
