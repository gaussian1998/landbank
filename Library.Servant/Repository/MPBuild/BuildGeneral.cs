﻿using Library.Entity.Edmx;
using Library.Servant.Servant.Build.BuildModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Servant.Repository.Build
{
    public class BuildGeneral
    {
        #region== EntityToModel ==
        public static BuildTRXHeaderModel ToBuildTRXHeaderModel(AS_TRX_Headers entity)
        {
            return new BuildTRXHeaderModel()
            {
                ID = entity.ID,
                Form_Number = entity.Form_Number,
                Transaction_Type = entity.Transaction_Type,
                Transaction_Status = entity.Transaction_Status,
                Book_Type_Code = entity.Book_Type_Code,
                Transaction_Datetime_Entered = entity.Transaction_Datetime_Entered,
                Accounting_Datetime = entity.Accounting_Datetime,
                Office_Branch = entity.Office_Branch,
                Description = entity.Description,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Created_By_Name = entity.Created_By_Name,
                Last_UpDatetimed_Time = entity.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = entity.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = entity.Last_UpDatetimed_By_Name,
                Auto_Post = entity.Auto_Post,
                Amotized_Adjustment_Flag = entity.Amotized_Adjustment_Flag,
            };
        }
        public static BuildHeaderModel ToBuildHeaderModel(AS_Assets_Build_Header entity)
        {
            return new BuildHeaderModel()
            {
                ID = entity.ID,
                TRX_Header_ID = entity.TRX_Header_ID,
                Transaction_Type = entity.Transaction_Type,
                Transaction_Status = entity.Transaction_Status,
                Book_Type = entity.Book_Type,
                Transaction_Datetime_Entered = entity.Transaction_Datetime_Entered,
                Accounting_Datetime = entity.Accounting_Datetime,
                Office_Branch = entity.Office_Branch,
                Description = entity.Description,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Last_Updated_Time = entity.Last_Updated_Time,
                Last_Updated_By = entity.Last_Updated_By
            };
        }

        public static AssetModel ToAssetModel(AS_Assets_Build_Record entity)
        {
            return new AssetModel()
            {
                ID = entity.ID,
                Asset_Number = entity.Asset_Number,
                Asset_Category_Code = entity.Asset_Category_Code,
                Deprn_Method_Code = entity.Deprn_Method_Code,
                Life_Years = entity.Life_Years,
                Life_Months = entity.Life_Months,
                Location_Disp = entity.Location_Disp,
                Parent_Asset_Number = entity.Parent_Asset_Number,
                Old_Asset_Number = entity.Old_Asset_Number,
                Description = entity.Description,
                Authorization_Name = entity.Authorization_Name,
                Authorization_Number = entity.Authorization_Number,
                Building_STRU = entity.Building_STRU,
                Building_Total_Floor = entity.Building_Total_Floor,
                Build_Address = entity.Build_Address,
                Build_Number = entity.Build_Number,
                City_Code = entity.City_Code,
                District_Code = entity.District_Code,
                Section_Code = entity.Section_Code,
                Sub_Sectioni_Name = entity.Section_Code,   
                Used_Type = entity.Used_Type,
                Used_Status = entity.Used_Status,
                Obtained_Method = entity.Obtained_Method,
                Original_Cost = entity.Original_Cost,
                Salvage_Value = entity.Salvage_Value,
                Deprn_Amount = entity.Deprn_Amount,
                Reval_Adjustment_Amount = entity.Reval_Adjustment_Amount,
                Business_Area_Size = entity.Business_Area_Size,
                NONBusiness_Area_Size = entity.NONBusiness_Area_Size,
                Current_Cost = entity.Current_Cost,
                Reval_Reserve = entity.Reval_Reserve,
                Business_Book_Amount = entity.Business_Book_Amount,
                NONBusiness_Book_Amount = entity.NONBusiness_Book_Amount,
                Deprn_Reserve = entity.Deprn_Reserve,
                Business_Deprn_Reserve = entity.Business_Deprn_Reserve,
                NONBusiness_Deprn_Reserve = entity.NONBusiness_Deprn_Reserve,
                Date_Placed_In_Service = entity.Date_Placed_In_Service,
                Remark1 = entity.Remark1,
                Remark2 = entity.Remark2,
                Current_Units = entity.Current_Units,
                Delete_Reason = entity.Delete_Reason,
                Urban_Renewal = entity.Urban_Renewal,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Last_Updated_Time = entity.Last_Updated_Time,
                Last_Updated_By = entity.Last_Updated_By
            };
        }

        public static AssetMPModel ToAssetMPModel(AS_Assets_Build_MP_Record entity)
        {
            return new AssetMPModel()
            {
                ID = entity.ID,
                Assets_Build_MP_ID = entity.Assets_Build_MP_ID,
                TRX_Header_ID = entity.TRX_Header_ID,
                Asset_Number = entity.Asset_Number,
                Parent_Asset_Number = entity.Parent_Asset_Number,
                Asset_Category_Code = entity.Asset_Category_Code,
                Old_Asset_Number = entity.Old_Asset_Number,
                Book_Type = entity.Book_Type,
                Date_Placed_In_Service = entity.Date_Placed_In_Service??default(DateTime),
                Current_Units = entity.Current_Units,
                Deprn_Method_Code = entity.Deprn_Method_Code,
                Life_Years = entity.Life_Years,
                Life_Months = entity.Life_Months,
                Location_Disp = entity.Location_Disp,
                Description = entity.Description,
                PO_Number = entity.PO_Number,
                PO_Destination = entity.PO_Destination,
                Model_Number = entity.Model_Number,
                Transaction_Date = entity.Transaction_Date,
                Assets_Unit = entity.Assets_Unit,
                Accessory_Equipment = entity.Accessory_Equipment,
                Assigned_NUM = entity.Assigned_NUM,
                Asset_Category_NUM = entity.Asset_Category_NUM,
                //Assigned_ID = entity.Assigned_ID,
                Asset_Structure = entity.Asset_Structure,
                Current_Cost = entity.Current_Cost,
                Deprn_Reserve = entity.Deprn_Reserve,
                Salvage_Value = entity.Salvage_Value,
                Remark = entity.Remark,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Created_By_Name = entity.Created_By_Name,
                Last_UpDatetimed_Time = entity.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = entity.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = entity.Last_UpDatetimed_By_Name
            };
        }

        public static AssetMPModel ToAssetMPModel(AS_Assets_Build_MP entity)
        {
            return new AssetMPModel()
            {
                ID = entity.ID,
                Asset_Build_ID = entity.Asset_Build_ID,
                Asset_Number = entity.Asset_Number,
                Parent_Asset_Number = entity.Parent_Asset_Number,
                Asset_Category_Code = entity.Asset_Category_Code,
                Old_Asset_Number = entity.Old_Asset_Number,
                Book_Type = entity.Book_Type,
                Date_Placed_In_Service = entity.Date_Placed_In_Service,
                Current_Units = entity.Current_Units,
                Deprn_Method_Code = entity.Deprn_Method_Code,
                Life_Years = entity.Life_Years,
                Life_Months = entity.Life_Months,
                Location_Disp = entity.Location_Disp,
                Description = entity.Description,
                PO_Number = entity.PO_Number,
                PO_Destination = entity.PO_Destination,
                Model_Number = entity.Model_Number,
                Transaction_Date = entity.Transaction_Date,
                Assets_Unit = entity.Assets_Unit,
                Accessory_Equipment = entity.Accessory_Equipment,
                Assigned_NUM = entity.Assigned_NUM,
                Asset_Category_NUM = entity.Asset_Category_NUM,
                //Assigned_ID = entity.Assigned_ID,
                Asset_Structure = entity.Asset_Structure,
                Current_Cost = entity.Current_Cost,
                Deprn_Reserve = entity.Deprn_Reserve,
                Salvage_Value = entity.Salvage_Value,
                Remark = entity.Remark,
                Deprn_Type = entity.Deprn_Type,
                Not_Deprn_Flag = entity.Not_Deprn_Flag,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Created_By_Name = entity.Created_By_Name,
                Last_UpDatetimed_Time = entity.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = entity.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = entity.Last_UpDatetimed_By_Name
            };
        }

        public static AssetModel ToAssetModel(AS_Assets_Build entity)
        {
            return new AssetModel()
            {
                ID = entity.ID,
                Asset_Number = entity.Asset_Number,
                Asset_Category_Code = entity.Asset_Category_Code,
                Deprn_Method_Code = entity.Deprn_Method_Code,
                Life_Years = entity.Life_Years,
                Life_Months = entity.Life_Months,
                Location_Disp = entity.Location_Disp,
                Parent_Asset_Number = entity.Parent_Asset_Number,
                Old_Asset_Number = entity.Old_Asset_Number,
                Description = entity.Description,
                Authorization_Name = entity.Authorization_Name,
                Authorization_Number = entity.Authorization_Number,
                Building_STRU = entity.Building_STRU,
                Building_Total_Floor = entity.Building_Total_Floor,
                Build_Address = entity.Build_Address,
                Build_Number = entity.Build_Number,
                City_Code = entity.City_Code,
                District_Code = entity.District_Code,
                Section_Code = entity.Section_Code,
                Sub_Sectioni_Name = entity.Section_Code,
                Used_Type = entity.Used_Type,
                Used_Status = entity.Used_Status,
                Obtained_Method = entity.Obtained_Method,
                Original_Cost = entity.Original_Cost,
                Salvage_Value = entity.Salvage_Value,
                Deprn_Amount = entity.Deprn_Amount,
                Reval_Adjustment_Amount = entity.Reval_Adjustment_Amount,
                Business_Area_Size = entity.Business_Area_Size,
                NONBusiness_Area_Size = entity.NONBusiness_Area_Size,
                Current_Cost = entity.Current_Cost,
                Reval_Reserve = entity.Reval_Reserve,
                Business_Book_Amount = entity.Business_Book_Amount,
                NONBusiness_Book_Amount = entity.NONBusiness_Book_Amount,
                Deprn_Reserve = entity.Deprn_Reserve,
                Business_Deprn_Reserve = entity.Business_Deprn_Reserve,
                NONBusiness_Deprn_Reserve = entity.NONBusiness_Deprn_Reserve,
                Date_Placed_In_Service = entity.Date_Placed_In_Service,
                Remark1 = entity.Remark1,
                Remark2 = entity.Remark2,
                Current_Units = entity.Current_Units,
                Delete_Reason = entity.Delete_Reason,
                Urban_Renewal = entity.Urban_Renewal,
                Not_Deprn_Flag = entity.Not_Deprn_Flag,
                DEPRN_Counts = entity.DEPRN_Counts,
                Create_Time = entity.Create_Time,
                Created_By = string.IsNullOrEmpty(entity.Created_By)?(decimal?)null:Convert.ToDecimal(entity.Created_By),
                Last_Updated_Time = entity.Last_Updated_Time,
                Last_Updated_By = string.IsNullOrEmpty(entity.Last_Updated_By) ? (decimal?)null : Convert.ToDecimal(entity.Created_By),
                Created_By_Name = entity.Created_By_Name,
                Last_UpDatetimed_By_Name = entity.Last_UpDatetimed_By_Name
            };
        }
        public static BuildRetireModel ToBuildRetireModel(AS_Assets_Build_Retire entity)
        {
            return new BuildRetireModel()
            {
                ID = entity.ID,
                TRX_Header_ID = entity.TRX_Header_ID,
                Asset_Build_ID = entity.Asset_Build_ID,
                Serial_Number = entity.Serial_Number,
                Asset_Number = entity.Asset_Number,
                Parent_Asset_Number = entity.Parent_Asset_Number,
                Old_Cost = entity.Old_Cost,
                Retire_Cost = entity.Retire_Cost,
                Sell_Amount = entity.Sell_Amount,
                Sell_Cost = entity.Sell_Cost,
                New_Cost = entity.New_Cost,
                Reval_Adjustment_Amount = entity.Reval_Adjustment_Amount,
                Reval_Reserve = entity.Reval_Reserve,
                Reval_Land_VAT = entity.Reval_Land_VAT,
                Reason_Code = entity.Reason_Code,
                Description = entity.Description,
                Account_Type = entity.Account_Type,
                Receipt_Type = entity.Receipt_Type,
                Retire_Cost_Account = entity.Retire_Cost_Account,
                Sell_Amount_Account = entity.Sell_Amount_Account,
                Sell_Cost_Account = entity.Sell_Cost_Account,
                Reval_Adjustment_Amount_Account = entity.Reval_Adjustment_Amount_Account,
                Reval_Reserve_Account = entity.Reval_Reserve_Account,
                Reval_Land_VAT_Account = entity.Reval_Land_VAT_Account,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Created_By_Name = entity.Created_By_Name,
                Last_UpDatetimed_Time = entity.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = entity.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = entity.Last_UpDatetimed_By_Name
            };
        }
        public static BuildMPRetireModel ToBuildMPRetireModel(AS_Assets_Build_MP_Retire entity)
        {
            return new BuildMPRetireModel()
            {
                ID = entity.ID,
                TRX_Header_ID = entity.TRX_Header_ID,
                Assets_Build_MP_ID = entity.Assets_Build_MP_ID,
                Serial_Number = entity.Serial_Number,
                Asset_Number = entity.Asset_Number,
                Parent_Asset_Number = entity.Parent_Asset_Number,
                Old_Cost = entity.Old_Cost,
                Retire_Cost = entity.Retire_Cost,
                Sell_Amount = entity.Sell_Amount,
                Sell_Cost = entity.Sell_Cost,
                New_Cost = entity.New_Cost,
                Reval_Adjustment_Amount = entity.Reval_Adjustment_Amount,
                Reval_Reserve = entity.Reval_Reserve,
                Reval_Land_VAT = entity.Reval_Land_VAT,
                Reason_Code = entity.Reason_Code,
                Description = entity.Description,
                Account_Type = entity.Account_Type,
                Receipt_Type = entity.Receipt_Type,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Created_By_Name = entity.Created_By_Name,
                Last_UpDatetimed_Time = entity.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = entity.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = entity.Last_UpDatetimed_By_Name
            };
        }
        public static BuildCategoryModel ToBuildCategoryModel(AS_Assets_Build_Category entity)
        {
            return new BuildCategoryModel()
            {
                ID = entity.ID,
                TRX_Header_ID = entity.TRX_Header_ID,
                Asset_Build_ID = entity.Asset_Build_ID,
                Serial_Number = entity.Serial_Number,
                Asset_Number = entity.Asset_Number,
                Old_Asset_Category_Code = entity.Old_Asset_Category_Code,
                NEW_Asset_Category_Code = entity.NEW_Asset_Category_Code,
                Description = entity.Description,
                Expense_Account = entity.Expense_Account,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Created_By_Name = entity.Created_By_Name,
                Last_UpDatetimed_Time = entity.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = entity.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = entity.Last_UpDatetimed_By_Name
            };
        }
        public static BuildYearModel ToBuildYearModel(AS_Assets_Build_Years entity)
        {
            return new BuildYearModel()
            {
                ID = entity.ID,
                TRX_Header_ID = entity.TRX_Header_ID,
                Asset_Build_ID = entity.Asset_Build_ID,
                Serial_Number = entity.Serial_Number,
                Asset_Number = entity.Asset_Number,
                Old_Life_Years = entity.Old_Life_Years,
                Old_Life_Months = entity.Old_Life_Months,
                New_Life_Years = entity.New_Life_Years,
                New_Life_Months = entity.New_Life_Months,
                Description = entity.Description,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Created_By_Name = entity.Created_By_Name,
                Last_UpDatetimed_Time = entity.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = entity.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = entity.Last_UpDatetimed_By_Name
            };
        }
        public static BuildUserModel ToBuildUserModel(AS_Users entity)
        {
            return new BuildUserModel()
            {
                ID = entity.ID,
                User_Code = entity.User_Code,
                User_Name = entity.User_Name,
                Office_Branch = entity.Office_Branch,
                Is_Active = entity.Is_Active,
            };
        }
        public static BuildMPCategoryModel ToBuildMPCategoryModel(AS_Assets_Build_MP_Category entity)
        {
            return new BuildMPCategoryModel()
            {
                ID = entity.ID,
                TRX_Header_ID = entity.TRX_Header_ID,
                Asset_Build_MP_ID = entity.Asset_Build_MP_ID,
                Serial_Number = entity.Serial_Number,
                Asset_Number = entity.Asset_Number,
                Old_Asset_Category_Code = entity.Old_Asset_Category_Code,
                NEW_Asset_Category_Code = entity.NEW_Asset_Category_Code,
                Description = entity.Description,
                Expense_Account = entity.Expense_Account,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Created_By_Name = entity.Created_By_Name,
                Last_UpDatetimed_Time = entity.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = entity.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = entity.Last_UpDatetimed_By_Name
            };
        }
        public static BuildMPYearModel ToBuildMPYearModel(AS_Assets_Build_MP_Years entity)
        {
            return new BuildMPYearModel()
            {
                ID = entity.ID,
                TRX_Header_ID = entity.TRX_Header_ID,
                Asset_Build_MP_ID = entity.Asset_Build_MP_ID,
                Serial_Number = entity.Serial_Number,
                Asset_Number = entity.Asset_Number,
                Old_Life_Years = entity.Old_Life_Years,
                Old_Life_Months = entity.Old_Life_Months,
                New_Life_Years = entity.New_Life_Years,
                New_Life_Months = entity.New_Life_Months,
                Description = entity.Description,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Created_By_Name = entity.Created_By_Name,
                Last_UpDatetimed_Time = entity.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = entity.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = entity.Last_UpDatetimed_By_Name
            };
        }
        #endregion

        #region== ModelToEntity ==
        public static AS_TRX_Headers ToAS_TRX_Headers(BuildTRXHeaderModel model)
        {
            //dbo.AS_TRX_Headers
            return new AS_TRX_Headers()
            {
                ID = model.ID,
                Form_Number = model.Form_Number,
                Office_Branch = model.Office_Branch,
                Book_Type_Code = model.Book_Type_Code,
                Transaction_Type = model.Transaction_Type,
                Transaction_Status = model.Transaction_Status,
                Transaction_Datetime_Entered = model.Transaction_Datetime_Entered,
                Accounting_Datetime = model.Accounting_Datetime,
                Description = model.Description,
                Amotized_Adjustment_Flag = model.Amotized_Adjustment_Flag,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Created_By_Name = model.Created_By_Name,
                Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = model.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name,
                Auto_Post = model.Auto_Post
            };
        }

        public static AS_Assets_Build_Header ToAS_Assets_Build_Header(BuildHeaderModel model)
        {
            return new AS_Assets_Build_Header()
            {
                ID = model.ID,
                TRX_Header_ID = model.TRX_Header_ID,
                Transaction_Type = model.Transaction_Type,
                Transaction_Status = model.Transaction_Status,
                Book_Type = model.Book_Type,
                Transaction_Datetime_Entered = model.Transaction_Datetime_Entered,
                Accounting_Datetime = model.Accounting_Datetime,
                Office_Branch = model.Office_Branch,
                Description = model.Description,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }

        public static AS_Assets_Build_Record ToAS_Assets_Build_Record(string TRX_Header_ID, AssetModel model)
        {
            return new AS_Assets_Build_Record()
            {
                TRX_Header_ID = TRX_Header_ID,
                Asset_Number = model.Asset_Number,
                Asset_Category_Code = model.Asset_Category_Code,
                Deprn_Method_Code = model.Deprn_Method_Code,
                Life_Years = model.Life_Years,
                Life_Months = model.Life_Months,
                Location_Disp = model.Location_Disp,
                Parent_Asset_Number = model.Parent_Asset_Number,
                Old_Asset_Number = model.Old_Asset_Number,
                Description = model.Description,
                Authorization_Name = model.Authorization_Name,
                Authorization_Number = model.Authorization_Number,
                Building_STRU = model.Building_STRU,
                Building_Total_Floor = model.Building_Total_Floor,
                Build_Address = model.Build_Address,
                Build_Number = model.Build_Number,
                City_Code = model.City_Code,
                District_Code = model.District_Code,
                Section_Code = model.Section_Code,
                Sub_Sectioni_Name = model.Sub_Sectioni_Name,
                Used_Type = model.Used_Type,
                Used_Status = model.Used_Status,
                Obtained_Method = model.Obtained_Method,
                Original_Cost = model.Original_Cost,
                Salvage_Value = model.Salvage_Value,
                Deprn_Amount = model.Deprn_Amount,
                Reval_Adjustment_Amount = model.Reval_Adjustment_Amount,
                Business_Area_Size = model.Business_Area_Size,
                NONBusiness_Area_Size = model.NONBusiness_Area_Size,
                Current_Cost = model.Current_Cost,
                Reval_Reserve = model.Reval_Reserve,
                Business_Book_Amount = model.Business_Book_Amount,
                NONBusiness_Book_Amount = model.NONBusiness_Book_Amount,
                Deprn_Reserve = model.Deprn_Reserve,
                Business_Deprn_Reserve = model.Business_Deprn_Reserve,
                NONBusiness_Deprn_Reserve = model.NONBusiness_Deprn_Reserve,
                Date_Placed_In_Service = model.Date_Placed_In_Service,
                Remark1 = model.Remark1,
                Remark2 = model.Remark2,
                Current_Units = model.Current_Units,
                Delete_Reason = !string.IsNullOrEmpty(model.Delete_Reason) ? model.Delete_Reason : "",
                Urban_Renewal = model.Urban_Renewal,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }

        public static AS_Assets_Build_MP_Record ToAS_Assets_Build_MP_Record(int TRX_Header_ID, AssetMPModel model)
        {
            return new AS_Assets_Build_MP_Record()
            {
                Assets_Build_MP_ID = model.Assets_Build_MP_ID,
                TRX_Header_ID = TRX_Header_ID,
                Asset_Number = model.Asset_Number,
                Parent_Asset_Number = !string.IsNullOrEmpty(model.Parent_Asset_Number)? model.Parent_Asset_Number : "",
                Asset_Category_Code = model.Asset_Category_Code,
                Old_Asset_Number = model.Old_Asset_Number,
                Book_Type = model.Book_Type,
                Date_Placed_In_Service = model.Date_Placed_In_Service,
                Current_Units = model.Current_Units,
                Deprn_Method_Code = model.Deprn_Method_Code,
                Life_Years = model.Life_Years,
                Life_Months = model.Life_Months,
                Location_Disp = !string.IsNullOrEmpty(model.Location_Disp) ? model.Location_Disp : "",
                Description = model.Description??string.Empty,
                PO_Number = model.PO_Number,
                PO_Destination = model.PO_Destination,
                Model_Number = model.Model_Number,
                Transaction_Date = model.Transaction_Date,
                Assets_Unit = model.Assets_Unit,
                Accessory_Equipment = model.Accessory_Equipment,
                Assigned_NUM = model.Assigned_NUM,
                Asset_Category_NUM = model.Asset_Category_NUM,
                Asset_Structure = model.Asset_Structure,
                Current_Cost = model.Current_Cost,
                Deprn_Reserve = model.Deprn_Reserve,
                Salvage_Value = model.Salvage_Value,
                Remark = !string.IsNullOrEmpty(model.Remark) ? model.Remark : "",
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Created_By_Name = model.Created_By_Name,
                Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = model.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name
            };
        }
        #endregion

        /// <summary>
        /// 取得表單編號
        /// </summary>
        /// <returns></returns>
        public static string GetFormCode(string Type)
        {
            string result = "";
            using (var db = new AS_LandBankEntities())
            {
                if (db.AS_Code_Table.Any(o => o.Class == "BuildForm" && o.Parameter1 == Type))
                {
                    result = db.AS_Code_Table.First(o => o.Class == "BuildForm" && o.Parameter1 == Type).Code_ID;
                }
            }
            return result;

            //string result = "";
            //using (var db = new AS_LandBankEntities())
            //{
            //    if (db.AS_VW_Code_Table.Any(o => o.Class == "BuildForm" && o.Class_Name == Type))
            //    {
            //        result = db.AS_VW_Code_Table.First(o => o.Class == "BuildForm" && o.Class_Name == Type).Code_ID;
            //    }
            //}
            //return result;
        }
    }
}


