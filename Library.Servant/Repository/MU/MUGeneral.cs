﻿using Library.Entity.Edmx;
using Library.Servant.Servant.MU.MUModels;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Transactions;
using System.IO;
using System.Xml.Linq;

namespace Library.Servant.Repository.MU
{
    public static class MUGeneral
    {
        public static bool debuglog(string dd)
        {

            using (FileStream fs = new FileStream("D:\\ak.txt", FileMode.Append))
            {
                using (StreamWriter writer = new StreamWriter(fs))
                {
                    writer.WriteLine(dd);
                }
            }
            return true;

        }
        public static DateTime CvtS2D(string DS)
        {
            var s = DS.Split('-');
            DateTime rs = DateTime.Parse(
                (Int32.Parse(s[0])/*+1911*/).ToString() + "/" +
                s[1] + "/" +
                s[2]
                );
            return rs;
        }
        public static string CvtD2S(DateTime DS)
        {
            var s = DS.ToString("yyyy-MM-dd").Split('-');
            string rs =
                (Int32.Parse(s[0]) - 1911).ToString() + "-" +
                s[1] + "-" +
                s[2]
                ;
            return rs;
        }
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>

        public static JObject GetIndex(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());
            // pagecount = 3;
            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            string Source_Type = condition.Obj["Source_Type"].ToString();
            string Document_Type = condition.Obj["Document_Type"].ToString();
            string BOOK_TYPE_CODE = condition.Obj["BOOK_TYPE_CODE"].ToString();
            string bno = condition.Obj["bno"].ToString();
            string eno = condition.Obj["eno"].ToString();
            string Jurisdiction2 = condition.Obj["Office_Branch"].ToString();
            DateTime BeginDate;
            DateTime EndDate;
            if (!string.IsNullOrEmpty(condition.Obj["BeginDate"].ToString()))
            {
                BeginDate = DateTime.Parse(condition.Obj["BeginDate"].ToString());
            }
            else
            {
                BeginDate = DateTime.Parse("1900/01/01");
            }
            if (!string.IsNullOrEmpty(condition.Obj["EndDate"].ToString()))
            {
                EndDate = DateTime.Parse(condition.Obj["EndDate"].ToString());
            }
            else
            {
                EndDate = DateTime.Parse("9900/01/01");
            }

            string DESCRIPTION = condition.Obj["DESCRIPTION"].ToString();

            string[] State = ((JArray)condition.Obj["State"]).Select(m => (string)m).ToArray();
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_AssetMaster
                            join dtype in db.AS_Assets_DocumentType on mh.Source_Type.ToString() equals dtype.ID.ToString()
                            into ps
                            from dtype in ps.DefaultIfEmpty()
                            join dtype0 in db.AS_Assets_DocumentType on mh.Document_Type.ToString() equals dtype0.ID.ToString()
                            into ps0
                            from dtype0 in ps0.DefaultIfEmpty()

                            join j2 in db.AS_Department on mh.Office_Branch equals j2.Department
                            into p4
                            from j2 in p4.DefaultIfEmpty()

                            join u in db.AS_Users on mh.Created_By equals u.ID
                            into p6
                            from u in p6.DefaultIfEmpty()
                            join ty in db.AS_Code_Table on new { Code_ID = mh.Type.ToString(), Class = "T02" } equals
                                                           new { ty.Code_ID, ty.Class }
                            into p7
                            from ty in p7.DefaultIfEmpty()

                            select new
                            {
                                ID = mh.ID,
                                NO = mh.TRX_Header_ID,
                                LY = dtype.Name,
                                DT = dtype0.Name,
                                DateY = (mh.BeginDate.Value.Year - 1911).ToString(),
                                DateM = (mh.BeginDate.Value.Month).ToString(),
                                DateD = (mh.BeginDate.Value.Day).ToString(),
                                J2 = j2.Department_Name,
                                J2C = mh.Office_Branch,
                                CR = u.User_Name,
                                TY = ty.Text,
                                TYC = mh.Type,
                                CT = mh.Create_Time,
                                DESCRIPTION = mh.DESCRIPTION,
                                BeginDate = mh.BeginDate,
                                Source_Type = mh.Source_Type,
                                BOOK_TYPE_CODE = mh.BOOK_TYPE_CODE,
                                Document_Type = mh.Document_Type
                            };

                if (!string.IsNullOrEmpty(Document_Type))
                {
                    query = query.Where(m => m.Document_Type.ToString() == Document_Type);
                }

                if (!string.IsNullOrEmpty(Source_Type))
                {
                    query = query.Where(m => m.Source_Type.ToString() == Source_Type);
                }
                if (!string.IsNullOrEmpty(BOOK_TYPE_CODE))
                {
                    query = query.Where(m => m.BOOK_TYPE_CODE.ToString() == BOOK_TYPE_CODE);
                }
                if (!string.IsNullOrEmpty(DESCRIPTION))
                {
                    query = query.Where(m => m.DESCRIPTION.Contains(DESCRIPTION));
                }
                if (!string.IsNullOrEmpty(Jurisdiction2))
                {
                    query = query.Where(m => m.J2C.Contains(Jurisdiction2));
                }
                if (!string.IsNullOrEmpty(bno))
                {
                    query = query.Where(m => string.Compare(m.NO, bno) >= 0);
                }
                if (!string.IsNullOrEmpty(eno))
                {
                    query = query.Where(m => string.Compare(m.NO, eno) <= 0);
                }
                query = query.Where(m => m.BeginDate >= BeginDate);
                query = query.Where(m => m.BeginDate <= EndDate);

                query = query.Where(m => State.Contains(m.TYC.ToString()));
                result = new JObject(new JProperty("Data",
                    query.ToList().OrderByDescending(m => m.CT)
                   .Skip(pagestart).Take(pagecount)
                    .Select(m => JToken.FromObject(m))
                    ));
                result["amount"] = query.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }
            return result;
        }

        public static JObject GetTarget(EJObject condition)
        {
            //IEnumerable<JObject> result = null;

            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_ActivationTarget
                            select new { mh };
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject GetLand(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());

            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            string[] BID = new string[0];
            if (condition.Obj["ID"] != null)
                BID = ((JArray)condition.Obj["ID"]).Select(m => (string)m).ToArray();

            using (var db = new AS_LandBankEntities())
            {
                var query = from bu in db.AS_Assets_Land
                            select new
                            {
                                ID = bu.ID,
                                Asset_Number = bu.Asset_Number,

                                Area_Size = bu.Area_Size,
                                Current_Cost = bu.Current_Cost,
                                Used_Type = bu.Used_Type,
                                Used_Partition = bu.Used_Partition,
                                Used_Status = bu.Used_Status,
                                City_Name = bu.City_Name,
                                District_Name = bu.District_Name,
                                Sectioni_Name = bu.Section_Name,
                                Sub_Sectioni_Name = bu.Sub_Section_Name,
                                Authorization_Number = bu.Authorization_Number,
                                Asset_Category_code = bu.Asset_Category_code,
                                Business_Book_Amount = bu.Business_Book_Amount,
                                Datetime_Placed_In_Service = bu.Datetime_Placed_In_Service

                            };

                if (BID.Length > 0)
                {
                    query = query.Where(m => BID.Contains(m.ID.ToString()));
                }


                result = new JObject(new JProperty("Data",
                    query.ToList().OrderBy(m => m.Asset_Number)
                   .Skip(pagestart).Take(pagecount)
                    .Select(m => JToken.FromObject(m))
                    ));

                result["amount"] = query.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }
            return result;
        }

        public static JObject GetApplyOrder(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            JObject result = null;
            int UserID = (Int32)condition.Obj["UserID"];
            DateTime limitdate = DateTime.Now.AddMonths(-6);
            using (var db = new AS_LandBankEntities())
            {
                var query = from ma in db.AS_Assets_AssetMaster
                            where ma.Type == 2 && ma.Created_By == UserID && ma.Create_Time >= limitdate
                            select new
                            {

                                MANO = ma.TRX_Header_ID
                            };

                result = new JObject(new JProperty("Data",
                    query.ToList().OrderBy(m => m.MANO)
                    .Select(m => JToken.FromObject(m))
                    ));


            }
            return result;
        }
        public static JObject GetLandF300(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());

            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            string[] BID = new string[0];
            if (condition.Obj["ID"] != null)
                BID = ((JArray)condition.Obj["ID"]).Select(m => (string)m).ToArray();
            int UserID = (Int32)condition.Obj["UserID"];
            string mano = condition.Obj["MANO"].ToString();
            DateTime limitdate = DateTime.Now.AddMonths(-6);
            using (var db = new AS_LandBankEntities())
            {
                var query = from bu in db.AS_Assets_Land
                            join de in db.AS_Assets_Compare_Apply on bu.ID equals de.Land_ID
                            join ma in db.AS_Assets_AssetMaster on de.AssetMaster_ID equals ma.ID
                            where ma.Type == 2 && ma.Created_By == UserID && ma.Create_Time >= limitdate
                            select new
                            {
                                ID = bu.ID,
                                Asset_Number = bu.Asset_Number,
                                Area_Size = bu.Area_Size,
                                Current_Cost = bu.Current_Cost,
                                Used_Type = bu.Used_Type,
                                Used_Partition = bu.Used_Partition,
                                Used_Status = bu.Used_Status,
                                City_Name = bu.City_Name,
                                District_Name = bu.District_Name,
                                Sectioni_Name = bu.Section_Name,
                                Sub_Sectioni_Name = bu.Sub_Section_Name,
                                Authorization_Number = bu.Authorization_Number,
                                Asset_Category_code = bu.Asset_Category_code,
                                Business_Book_Amount = bu.Business_Book_Amount,
                                Datetime_Placed_In_Service = bu.Datetime_Placed_In_Service,
                                MANO = ma.TRX_Header_ID
                            };

                if (BID.Length > 0)
                {
                    query = query.Where(m => BID.Contains(m.ID.ToString()));
                }
                if (!string.IsNullOrEmpty(mano))
                {
                    query = query.Where(m => m.MANO == mano);
                }
                result = new JObject(new JProperty("Data",
                    query.ToList().OrderBy(m => m.Asset_Number)
                   .Skip(pagestart).Take(pagecount)
                    .Select(m => JToken.FromObject(m))
                    ));

                result["amount"] = query.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }
            return result;
        }
        public static JObject GetMaxNo(EJObject condition)
        {
            //IEnumerable<JObject> result = null;

            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from AS_Assets_AssetMaster in
                         (from AS_Assets_AssetMaster in db.AS_Assets_AssetMaster.AsEnumerable()
                          select new
                          {
                              Column1 = AS_Assets_AssetMaster.TRX_Header_ID.Substring(12 - 1, 6),
                              Dummy = "x"
                          })
                            group AS_Assets_AssetMaster by new { AS_Assets_AssetMaster.Dummy } into g
                            select new
                            {
                                maxno = (int?)(Int32)g.Max(p => Convert.ToInt32(p.Column1) + 1)
                            };
                result = new JObject(new JProperty("Data", query.ToList().Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject DocumentType(EInt type1)
        {
            int type = type1.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_DocumentType
                            select new { mh };
                query = query.Where(m => m.mh.ID >= type * 1000).Where(m => m.mh.ID < (type + 1) * 1000);
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject DocumentTypeOne(EInt type1)
        {
            int type = type1.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_DocumentType
                            select new { mh };
                query = query.Where(m => m.mh.ID == type);
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject GetDPM(EInt type1)
        {
            int type = type1.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Department
                            select new { mh };
                if (type == 1)
                    query = query.Where(m => m.mh.Branch_Code != "001");
                if (type == 2)
                    query = query.Where(m => m.mh.Branch_Code == "001");
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject GetDetail(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());

            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            string[] BID = new string[0];
            if (condition.Obj["ID"] != null)
                BID = ((JArray)condition.Obj["ID"]).Select(m => (string)m).ToArray();



            var bo = false;
            if (BID.Length >= 1)
            {
                if (((BID.Length == 1) & (BID[0] == "0")))
                    bo = true;
            }
            else
                bo = true;

            string aid = condition.Obj["AID"].ToString();
            using (var db = new AS_LandBankEntities())
            {
                var query = from d in db.AS_Assets_Compare_Assets.AsEnumerable()

                            where d.AssetMaster_ID.ToString() == aid &&
                             (BID.Contains(d.Land_ID.ToString()) || (bo))
                            select new
                            {
                                ID=d.ID,
                                Land_ID = d.Land_ID ?? 0,
                                Asset_Number = d.Old_Asset_Number,
                                Area_Size = d.Area_Size,
                                Land_Area_Size = d.Land_Area_Size,
                                Current_Cost = d.Original_Announce_Amount,
                                Announce_Amount = d.Announce_Amount,
                                Land_Announce_Amount = d.Land_Announce_Amount,
                            
                                City_Name = d.City_Name,
                                District_Name = d.District_Name,
                                Sectioni_Name = d.Section_Name,
                                Sub_Sectioni_Name = d.Sub_Section_Name,
                                Authorization_Number = d.Authorization_Number,
                                Land_Number = d.Parent_Land_Number + "-" + d.Filial_Land_Number
                            };

                var tmpq = query.Select(m => m.Land_ID).ToArray();

                var query2 = from l in db.AS_Assets_Land.AsEnumerable()
                             where
                             BID.Contains(l.ID.ToString()) &&
                             !(tmpq).Contains(l.ID)
                             select new
                             {
                                 ID = (long)0,
                                 Land_ID = l.ID,
                                 Asset_Number = l.Asset_Number,
                                 Area_Size = (decimal?)l.Area_Size,
                                 Land_Area_Size = (decimal?)null,
                                 Current_Cost = (decimal?)l.Current_Cost,
                                 Announce_Amount= (decimal?)l.Announce_Amount,
                                 Land_Announce_Amount = (decimal?)null,
                                
                                 City_Name = l.City_Name,
                                 District_Name = l.District_Name,
                                 Sectioni_Name = l.Section_Name,
                                 Sub_Sectioni_Name = l.Sub_Section_Name,
                                 Authorization_Number = l.Authorization_Number,
                                 Land_Number = l.Parent_Land_Number + "-" + l.Filial_Land_Number
                             };

                query = query.Concat(query2);
                var tmpq2 = query.Select(m => m.Land_ID).ToArray();


                result = new JObject(new JProperty("Data",
                    query.ToList().OrderBy(m => m.Asset_Number)
                   .Skip(pagestart).Take(pagecount)
                    .Select(m => JToken.FromObject(m))
                    ));

                result["tmpq2"] = JToken.FromObject(tmpq2);
                result["amount"] = query.ToList().Count;
                result["amount2"] = query2.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }

            return result;
        }
        public static JObject GetDetail2(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            JObject result = null;

            string ID = condition.Obj["ID"].ToString();
             

            using (var db = new AS_LandBankEntities())
            {
                var query = from d in db.AS_Assets_Compare_Assets.AsEnumerable()
                            .Select(m => m.ID.ToString() == ID)
                            select new { d };

                result = new JObject(new JProperty("Data",
                    query.ToList()
                    .Select(m => JToken.FromObject(m))
                    ));

            }

            return result;
        }
        public static JObject GetOrder(EString no1)
        {
            string no = no1.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_AssetMaster
                            join ty in db.AS_Code_Table on new { Code_ID = mh.Type.ToString(), Class = "T02" } equals
                                                           new { ty.Code_ID, ty.Class }
                            into p7
                            from ty in p7.DefaultIfEmpty()
                            select new
                            {
                                mh,
                                DateY = (mh.BeginDate.Value.Year /*- 1911*/).ToString(),
                                DateM = (mh.BeginDate.Value.Month).ToString(),
                                DateD = (mh.BeginDate.Value.Day).ToString(),
                                TY = ty.Text
                            };
                query = query.Where(m => m.mh.ID.ToString() == no);
                /* var query1 = from c in db.AS_Assets_Activation_LBDetail
                              select new { ID = c.ID, AID = c.Activation_ID, TY = c.Type };
                 query1 = query1.Where(m => m.AID.ToString() == no);*/

                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
                // result["LC"] = query1.Where(m => m.TY == 1).ToList().Count();
                // result["BC"] = query1.Where(m => m.TY == 0).ToList().Count();

            }
            return result;
        }
        public static JObject GetBookCode(EString type1)
        {

            string type = type1.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Code_Table
                            select new { mh };
                query = query.Where(m => m.mh.Class == type);
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject ApplyEDOC(EString type1)
        {
            JObject result = null;
            return result;
        }
        public static JObject ApplyQuery(EString type1)
        {
            JObject result = null;
            return result;
        }
        public static JObject ExportEDOCData(EString type1)
        {
            JObject result = null;
            return result;
        }

        public static JObject UpdateOrder(EJObject model2)
        {
            JObject model = new JObject();
            model = model2.Obj;
            long nowidd = 0;
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    for (int i = 0; i < model.Count; i++)
                    {
                        AS_Assets_AssetMaster AA;
                        AS_Assets_Compare_Assets ALB;

                        if (model["Data"]["ID"] == null)
                        {

                            AA = new AS_Assets_AssetMaster();
                            nowidd = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                            AA.ID = nowidd;
                            AA.Create_Time = DateTime.Now;
                            AA.Last_Updated_Time = DateTime.Now;
                            db.Entry(AA).State = System.Data.Entity.EntityState.Added;
                        }
                        else
                        {
                            long idd = 0;
                            Int64.TryParse(model["Data"]["ID"].ToString(), out idd);
                            AA = db.AS_Assets_AssetMaster.FirstOrDefault(m => m.ID == idd);
                            nowidd = idd;
                            AA.Last_Updated_Time = DateTime.Now;
                            db.Entry(AA).State = System.Data.Entity.EntityState.Modified;
                        }
                        String[] BU = new String[0];



                        if (model["Data"]["DataB"] != null)
                        {
                            if (model["Data"]["DataB"].ToString() != "")
                            {
                                
                                db.AS_Assets_Compare_Assets.RemoveRange(
                                   db.AS_Assets_Compare_Assets.Where(m => m.AssetMaster_ID == nowidd)
                                   .Where(m => !(BU).Contains(m.Land_ID.ToString()))
                                   );
                                db.Configuration.AutoDetectChangesEnabled = false;
                                BU = model["Data"]["DataB"].ToString().Split(',');
                                for (int j = 0; j < BU.Length; j++)
                                {
                                    var tmpq = from l in db.AS_Assets_Land.AsEnumerable()
                                               where
                                              l.ID.ToString() == BU[j]
                                               select new
                                               {
                                                   ID = (Int64)l.ID,
                                                   Asset_Number = l.Asset_Number,
                                                   Area_Size = (decimal?)l.Area_Size,
                                                   Current_Cost = (decimal?)l.Current_Cost,
                                                   City_Name = l.City_Name,
                                                   District_Name = l.District_Name,
                                                   Sectioni_Name = l.Section_Name,
                                                   Sub_Sectioni_Name = l.Sub_Section_Name,
                                                   Authorization_Number = l.Authorization_Number,
                                                   Parent_Land_Number = l.Parent_Land_Number,
                                                   Filial_Land_Number = l.Filial_Land_Number,

                                                   Authorized_name = l.Authorized_name,
                                                   Authorized_Scope_Denomminx = l.Authorized_Scope_Denomminx,
                                                   Authorized_Area = l.Authorized_Area,
                                                   Original_Cost = l.Original_Cost,
                                                   Business_Book_Amount = l.Business_Book_Amount,
                                                   NONBusiness_Book_Amount = l.NONBusiness_Book_Amount,
                                                   Year_Number = l.Year_Number,
                                                   Announce_Amount = l.Announce_Amount,
                                                   Announce_Price = l.Announce_Price,
                                                   Transfer_Price = l.Transfer_Price,
                                                   Reduction_Area = l.Reduction_Area,
                                                   Declared_Price = l.Declared_Price,
                                                   Dutiable_Amount = l.Dutiable_Amount,
                                                   Description = l.Description,
                                               };
                                    var Co = db.AS_Assets_Compare_Assets.AsEnumerable()
                                       .Where(m => m.AssetMaster_ID == nowidd)
                                       .Where(m => m.Land_ID == Int32.Parse(BU[j])).Count();
                                    ALB = new AS_Assets_Compare_Assets();
                                    if (Co == 0)
                                    {
                                        ALB.ID = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                                    }
                                    else
                                    {
                                        ALB = db.AS_Assets_Compare_Assets.AsEnumerable()
                                            .Where(m => m.AssetMaster_ID == nowidd)
                                            .FirstOrDefault(m => m.Land_ID == Int32.Parse(BU[j]));
                                    }
                                    
                                    ALB.AssetMaster_ID = nowidd;
                                    ALB.Land_ID = Int32.Parse(BU[j]);
                                    ALB.Old_Asset_Number = tmpq.ToList().First().Asset_Number;
                                    ALB.Area_Size = tmpq.ToList().First().Area_Size;
                                 

                                    ALB.City_Name = tmpq.ToList().First().City_Name;
                                    ALB.District_Name = tmpq.ToList().First().District_Name;
                                    ALB.Section_Name = tmpq.ToList().First().Sectioni_Name;
                                    ALB.Sub_Section_Name = tmpq.ToList().First().Sub_Sectioni_Name;
                                    ALB.Authorization_Number = tmpq.ToList().First().Authorization_Number;
                                    ALB.Parent_Land_Number = tmpq.ToList().First().Parent_Land_Number;
                                    ALB.Filial_Land_Number = tmpq.ToList().First().Filial_Land_Number;

                                    ALB.Authorized_name = tmpq.ToList().First().Authorized_name;
                                    ALB.Authorized_Scope_Denomminx = Int32.Parse(tmpq.ToList().First().Authorized_Scope_Denomminx.ToString());
                                    ALB.Authorized_Area = tmpq.ToList().First().Authorized_Area;
                                    ALB.Original_Cost = tmpq.ToList().First().Original_Cost;
                                    ALB.Current_Cost = tmpq.ToList().First().Current_Cost;
                                    ALB.Business_Book_Amount = tmpq.ToList().First().Business_Book_Amount;
                                    ALB.NONBusiness_Book_Amount = tmpq.ToList().First().NONBusiness_Book_Amount;
                                    ALB.Year_Number = Int32.Parse(tmpq.ToList().First().Year_Number.ToString());
                                    ALB.Announce_Amount = tmpq.ToList().First().Announce_Amount;
                                    ALB.Announce_Price = tmpq.ToList().First().Announce_Price;
                                    ALB.Transfer_Price = tmpq.ToList().First().Transfer_Price;
                                    ALB.Reduction_Area = tmpq.ToList().First().Reduction_Area;
                                    ALB.Declared_Price = tmpq.ToList().First().Declared_Price;
                                    ALB.Dutiable_Amount = tmpq.ToList().First().Dutiable_Amount;
                                    ALB.Description = tmpq.ToList().First().Description;
                                    //db.AS_Assets_Compare_Assets.Add(ALB);
                                    if (Co == 0)
                                    {
                                        db.Entry(ALB).State = System.Data.Entity.EntityState.Added;
                                    }
                                    else
                                    {
                                        db.Entry(ALB).State = System.Data.Entity.EntityState.Modified;
                                    }
                                  
                                }
                            }
                        }

                        AA.BeginDate = MUGeneral.CvtS2D(model["Data"]["BeginDate"].ToString().Replace("/", "-"));
                        AA.TRX_Header_ID = model["Data"]["TRX_Header_ID"].ToString();
                        AA.Document_Type = Int64.Parse(model["Data"]["Document_Type"].ToString());
                        AA.Source_Type = Int32.Parse(model["Data"]["Source_Type"].ToString());
                        AA.BOOK_TYPE_CODE = model["Data"]["BOOK_TYPE_CODE"].ToString();

                        AA.Office_Branch = model["Data"]["Office_Branch"].ToString();

                        AA.DESCRIPTION = model["Data"]["DESCRIPTION"].ToString();
                        AA.Type = Int32.Parse(model["Data"]["Type"].ToString());
                        AA.Created_By = Int32.Parse(model["Data"]["Created_By"].ToString());
                        AA.Last_Updated_By = Int32.Parse(model["Data"]["Last_Updated_By"].ToString());



                        db.SaveChanges();
                        db.Configuration.AutoDetectChangesEnabled = true;
                    }
                }
                trans.Complete();
            }
            JObject result = new JObject(new JProperty("TRX_Header_ID", nowidd.ToString()));
            return result;
        }
        public static JObject ImportLandData(EJObject model2)
        {

            JObject model = new JObject();
            model = model2.Obj;
            debuglog(model.ToString());
            var aa = XDocument.Parse(model["Data"].ToString());
            var root = aa.Element("EDOCExport").Element("EDOCData").Element("LandREG");
            IEnumerable<XElement> enumXML = aa.Element("EDOCExport").Element("EDOCData").Elements("LandREG");

            long nowidd = Int64.Parse(model["AssetMaster_ID"].ToString());
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    foreach (XElement item in enumXML)
                    {

                        AS_Assets_Compare_Assets AA;

                        AA = db.AS_Assets_Compare_Assets.AsEnumerable().Where(m => m.AssetMaster_ID == nowidd)
                                .FirstOrDefault(m => m.Parent_Land_Number + m.Filial_Land_Number == item.Element("LandNo").Value);

                        AA.Land_City_Name = item.Element("City").Value;
                        AA.Land_District_Name = item.Element("Area").Value;
                        AA.Land_Section_Name = item.Element("Section").Value;
                        AA.Land_Parent_Land_Number = item.Element("LandNo").Value.Substring(0,4);
                        AA.Land_Filial_Land_Number = item.Element("LandNo").Value.Substring(4,4);
                       
                        AA.Land_Area_Size = Decimal.Parse(item.Element("AreaSize").Value);
                        AA.Land_Year_Number = Int32.Parse(item.Element("PublicValue").Attribute("Date").Value.Substring(0,3));
                        AA.Land_Announce_Amount = Decimal.Parse(item.Element("PublicValue").Value);
                        //BTBLOW
                         AA.Land_Authorized_Scope_Denomminx = Int32.Parse(item.Element("BTBLOW").Element("RightsRange").Element("Denominator").Value);
                        AA.Land_Authorized_Scope_Molecule = Int32.Parse(item.Element("BTBLOW").Element("RightsRange").Element("Numerator").Value);
                        AA.Land_Authorization_Number = item.Element("BTBLOW").Element("B16").Value;
                       

                        AA.Last_Updated_Time = DateTime.Now;
                        db.Entry(AA).State = System.Data.Entity.EntityState.Modified;



                        db.SaveChanges();
                        db.Configuration.AutoDetectChangesEnabled = true;
                    }
                }
                trans.Complete();
            }
            JObject result = new JObject(new JProperty("TRX_Header_ID", nowidd.ToString()));
            return result;
        }

        public static JObject UpdateF200Order(EJObject model2)
        {
            JObject model = new JObject();
            model = model2.Obj;
            long nowidd = 0;
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    for (int i = 0; i < model.Count; i++)
                    {
                        AS_Assets_AssetMaster AA;
                        AS_Assets_Compare_Apply ALB;

                        if (model["Data"]["ID"] == null)
                        {

                            AA = new AS_Assets_AssetMaster();
                            nowidd = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                            AA.ID = nowidd;
                            AA.Create_Time = DateTime.Now;
                            AA.Last_Updated_Time = DateTime.Now;
                            db.Entry(AA).State = System.Data.Entity.EntityState.Added;
                        }
                        else
                        {
                            long idd = 0;
                            Int64.TryParse(model["Data"]["ID"].ToString(), out idd);
                            AA = db.AS_Assets_AssetMaster.FirstOrDefault(m => m.ID == idd);
                            nowidd = idd;
                            AA.Last_Updated_Time = DateTime.Now;
                            db.Entry(AA).State = System.Data.Entity.EntityState.Modified;
                        }
                        String[] BU = new String[0];



                        if (model["Data"]["DataB"] != null)
                        {
                            if (model["Data"]["DataB"].ToString() != "")
                            {
                                db.AS_Assets_Compare_Assets.RemoveRange(
                                    db.AS_Assets_Compare_Assets.Where(m => m.AssetMaster_ID == nowidd));
                                db.Configuration.AutoDetectChangesEnabled = false;
                                BU = model["Data"]["DataB"].ToString().Split(',');
                                for (int j = 0; j < BU.Length; j++)
                                {
                                    var tmpq = from l in db.AS_Assets_Land.AsEnumerable()
                                               where
                                              l.ID.ToString() == BU[j]
                                               select new
                                               {
                                                   ID = (Int64)l.ID,

                                               };
                                    ALB = new AS_Assets_Compare_Apply();
                                    ALB.ID = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                                    ALB.AssetMaster_ID = nowidd;
                                    ALB.Land_ID = Int32.Parse(BU[j]);
                                    ALB.Created_By = Int32.Parse(model["Data"]["Created_By"].ToString());
                                    ALB.Last_Updated_By = Int32.Parse(model["Data"]["Last_Updated_By"].ToString());
                                    db.AS_Assets_Compare_Apply.Add(ALB);
                                }
                            }
                        }

                        AA.BeginDate = MUGeneral.CvtS2D(model["Data"]["BeginDate"].ToString().Replace("/", "-"));
                        AA.TRX_Header_ID = model["Data"]["TRX_Header_ID"].ToString();
                        AA.Document_Type = Int64.Parse(model["Data"]["Document_Type"].ToString());
                        AA.Source_Type = Int32.Parse(model["Data"]["Source_Type"].ToString());
                        AA.BOOK_TYPE_CODE = model["Data"]["BOOK_TYPE_CODE"].ToString();

                        AA.Office_Branch = model["Data"]["Office_Branch"].ToString();

                        AA.DESCRIPTION = model["Data"]["DESCRIPTION"].ToString();
                        AA.Type = Int32.Parse(model["Data"]["Type"].ToString());
                        AA.Created_By = Int32.Parse(model["Data"]["Created_By"].ToString());
                        AA.Last_Updated_By = Int32.Parse(model["Data"]["Last_Updated_By"].ToString());



                        db.SaveChanges();
                        db.Configuration.AutoDetectChangesEnabled = true;
                    }
                }
                trans.Complete();
            }
            JObject result = new JObject(new JProperty("TRX_Header_ID", nowidd.ToString()));
            return result;
        }
    }
}
