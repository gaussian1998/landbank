﻿using Library.Entity.Edmx;
using Library.Servant.Servant.Evaluation.EvaluationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Transactions;
using System.IO;

namespace Library.Servant.Repository.Evaluation
{
    public static class EvaluationGeneral
    {
        public static bool debuglog2(string dd)
        {

            using (FileStream fs = new FileStream("D:\\ak.txt", FileMode.Append))
            {
                using (StreamWriter writer = new StreamWriter(fs))
                {
                    writer.WriteLine(dd);
                }
            }
            return true;

        }
        public static DateTime CvtS2D(string DS)
        {
            var s = DS.Split('-');
            DateTime rs = DateTime.Parse(
                (Int32.Parse(s[0])/*+1911*/).ToString() + "/" +
                s[1] + "/" +
                s[2]
                );
            return rs;
        }
        public static string CvtD2S(DateTime DS)
        {
            var s = DS.ToString("yyyy-MM-dd").Split('-');
            string rs =
                (Int32.Parse(s[0]) - 1911).ToString() + "-" +
                s[1] + "-" +
                s[2]
                ;
            return rs;
        }
        /// <summary>
        /// 單據查詢
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static JObject GetDocID(EJObject T)
        {
            bool result = true;
            int ri = 0;
            string DocNo = T.Obj["DocCode"].ToString();
            using (var db = new AS_LandBankEntities())
            {
                result = db.AS_Doc_Name.Any(m => m.Doc_No == DocNo);
                ri = (result) ? db.AS_Doc_Name.First(m => m.Doc_No == DocNo).ID : 0;
            }
            JObject r = new JObject();

            r["DocID"] = ri.ToString();
            debuglog2(r.ToString());
            return r;
        }
        public static JObject UpdateRemark(EJObject R)
        {
            //IEnumerable<JObject> result = null;
            string ID = R.Obj["ID"].ToString();
            string S = R.Obj["S"].ToString();
            int U = (int)R.Obj["User"];
            JObject result = null;
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    AS_Assets_Evaluation AA;
                    AA = db.AS_Assets_Evaluation.AsEnumerable().FirstOrDefault(m => m.ID.ToString() == ID);
                    AA.DESCRIPTION = S;
                    AA.Last_Updated_By = U;
                    AA.Last_Updated_Time = DateTime.Now;

                    db.Entry(AA).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                trans.Complete();
                result = new JObject(new JProperty("Data", "成功"));
            }
            return result;
        }
        
        public static JObject OnFinish(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            string No = condition.Obj["No"].ToString();
            JObject result = null;
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    AS_Assets_Evaluation AA;
                    AA = db.AS_Assets_Evaluation.FirstOrDefault(m => m.TRX_Header_ID == No);
                    AA.Type = 2;
                    db.Entry(AA).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                   
                    //房屋的
                   var  D = db.AS_Assets_Evaluation_Detail.Where(m => m.BID == AA.ID).Where(m=>m.Land_ID ==null).ToList();
                    foreach (var item in D)
                    {
                        AS_Assets_Build_Log Log = new AS_Assets_Build_Log();
                        Log.ID = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                        Log.BuildID = item.Build_ID;
                        Log.ChangeSource = AA.TRX_Header_ID;
                        Log.ChangeTime = DateTime.Now;
                        Log.ChangeUser = item.Last_Updated_By;
                        Log.Current_Cost = item.Current_Cost;
                        db.AS_Assets_Build_Log.Add(Log);
                    }
                    //土地的
                    var L = db.AS_Assets_Evaluation_Detail.Where(m => m.BID == AA.ID).Where(m => m.Land_ID != null).ToList();
                    foreach (var item in L)
                    {
                        AS_Assets_Land_Log Log = new AS_Assets_Land_Log();
                        Log.ID = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                        Log.LandID = item.Land_ID;
                        Log.ChangeSource = AA.TRX_Header_ID;
                        Log.ChangeTime = DateTime.Now;
                        Log.ChangeUser = item.Last_Updated_By;
                        Log.Current_Cost = item.Current_Cost;
                        db.AS_Assets_Land_Log.Add(Log);
                    }
                    db.SaveChanges();
                }
                trans.Complete();
                result = new JObject(new JProperty("Data", "成功"));
            }
            return result;
        }
        public static JObject OnI200Finish(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            string No = condition.Obj["No"].ToString();
            JObject result = null;
            long ID = 0;
            using (var trans = new TransactionScope())
            {
                long NewID = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                using (var db = new AS_LandBankEntities())
                {
                    AS_Assets_Evaluation AA;
                    AA = db.AS_Assets_Evaluation.FirstOrDefault(m => m.TRX_Header_ID == No);
                    ID = AA.ID;

                    AA.Type = 2;
                    db.Entry(AA).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                   
                        AS_Assets_Evaluation ALB = new AS_Assets_Evaluation();
                        ALB.ID = NewID;
                        ALB.Document_Type = 5001;
                        ALB.Source_Type = 6001;
                        ALB.TRX_Header_ID = CvtD2S(DateTime.Today).Replace("-", "") + "I300";
                        ALB.BOOK_TYPE_CODE = AA.BOOK_TYPE_CODE;
                        ALB.BeginDate = DateTime.Now;
                        ALB.Type = 0;
                        ALB.Office_Branch = AA.Office_Branch;
                        ALB.Create_Time = DateTime.Now;
                        ALB.Created_By = AA.Created_By;
                        ALB.Last_Updated_Time = DateTime.Now;
                        ALB.Last_Updated_By = AA.Created_By;
                        db.AS_Assets_Evaluation.Add(ALB);
                    foreach (var item in db.AS_Assets_Evaluation_Detail.Where(d => d.AID == ID)){
                        item.BID = NewID;
                    }

                    db.SaveChanges();
                }

                trans.Complete();
                result = new JObject(new JProperty("Data", "成功"));
            }
            return result;
        }
        public static JObject GetIndex(EJObject condition)
        {
            
            //IEnumerable<JObject> result = null;
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());
            // pagecount = 3;
            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            string Source_Type = condition.Obj["Source_Type"].ToString();
            string Document_Type = condition.Obj["Document_Type"].ToString();
            string BOOK_TYPE_CODE = condition.Obj["BOOK_TYPE_CODE"].ToString();
            string bno = condition.Obj["bno"].ToString();
            string eno = condition.Obj["eno"].ToString();
           
            string Jurisdiction2 = condition.Obj["Office_Branch"].ToString();
            DateTime BeginDate;
            DateTime EndDate;
           
            if (!string.IsNullOrEmpty(condition.Obj["BeginDate"].ToString()))
            {
                BeginDate = DateTime.Parse(condition.Obj["BeginDate"].ToString());
            }
            else
            {
                BeginDate = DateTime.Parse("1900/01/01");
            }
            if (!string.IsNullOrEmpty(condition.Obj["EndDate"].ToString()))
            {
                EndDate = DateTime.Parse(condition.Obj["EndDate"].ToString());
            }
            else
            {
                EndDate = DateTime.Parse("9900/01/01");
            }
            
            string DESCRIPTION = condition.Obj["DESCRIPTION"].ToString();
           
            string[] State = ((JArray)condition.Obj["State"]).Select(m => (string)m).ToArray();
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_Evaluation
                            join dtype in db.AS_Assets_DocumentType on mh.Source_Type.ToString() equals dtype.ID.ToString()
                            into ps
                            from dtype in ps.DefaultIfEmpty()
                            join dtype0 in db.AS_Assets_DocumentType on mh.Document_Type.ToString() equals dtype0.ID.ToString()
                            into ps0
                            from dtype0 in ps0.DefaultIfEmpty()
                            
                            join j2 in db.AS_Department on mh.Office_Branch equals j2.Department
                            into p4
                            from j2 in p4.DefaultIfEmpty()
                            
                            join u in db.AS_Users on mh.Created_By equals u.ID
                            into p6
                            from u in p6.DefaultIfEmpty()
                            join ty in db.AS_Code_Table on new { Code_ID = mh.Type.ToString(), Class = "T02" } equals
                                                           new { ty.Code_ID, ty.Class }
                            into p7
                            from ty in p7.DefaultIfEmpty()

                            select new
                            {
                                ID = mh.ID,
                                NO = mh.TRX_Header_ID,
                                LY = dtype.Name,
                                DT = dtype0.Name,
                                DateY = (mh.BeginDate.Value.Year - 1911).ToString(),
                                DateM = (mh.BeginDate.Value.Month).ToString(),
                                DateD = (mh.BeginDate.Value.Day).ToString(),
                                J2 = j2.Department_Name,
                                J2C = mh.Office_Branch,
                                CR = u.User_Name,
                                TY = ty.Text,
                                TYC = mh.Type,
                                CT = mh.Create_Time,
                                DESCRIPTION = mh.DESCRIPTION,
                                BeginDate = mh.BeginDate,
                                Source_Type = mh.Source_Type,
                                BOOK_TYPE_CODE = mh.BOOK_TYPE_CODE,
                                Document_Type = mh.Document_Type,
                                DC = (from d in db.AS_Assets_Evaluation_Detail where d.BID == (long?)mh.ID && d.Land_ID == null select d).ToList().Count(),
                                DDC = (from d in db.AS_Assets_Evaluation_Detail where d.BID == (long?)mh.ID && d.Land_ID == null && d.Type==3 select d).ToList().Count(),
                                EndDate = mh.EndDate,
                                DeadLine=mh.DeadLine
                            };

                if (!string.IsNullOrEmpty(Document_Type))
                {
                    query = query.Where(m => m.Document_Type.ToString() == Document_Type);
                }

                if (!string.IsNullOrEmpty(Source_Type))
                {
                    query = query.Where(m => m.Source_Type.ToString() == Source_Type);
                }
                if (!string.IsNullOrEmpty(BOOK_TYPE_CODE))
                {
                    query = query.Where(m => m.BOOK_TYPE_CODE.ToString() == BOOK_TYPE_CODE);
                }
                if (!string.IsNullOrEmpty(DESCRIPTION))
                {
                    query = query.Where(m => m.DESCRIPTION.Contains(DESCRIPTION));
                }
                if (!string.IsNullOrEmpty(Jurisdiction2))
                {
                    query = query.Where(m => m.J2C.Contains(Jurisdiction2));
                }
                if (!string.IsNullOrEmpty(bno))
                {
                    query = query.Where(m => string.Compare(m.NO, bno) >= 0);
                }
                if (!string.IsNullOrEmpty(eno))
                {
                    query = query.Where(m => string.Compare(m.NO, eno) <= 0);
                }
                query = query.Where(m => m.BeginDate >= BeginDate);
                query = query.Where(m => m.BeginDate <= EndDate);

                query = query.Where(m => State.Contains(m.TYC.ToString()));


                result = new JObject(new JProperty("Data",
                    query.ToList().OrderByDescending(m => m.CT)
                   .Skip(pagestart).Take(pagecount)
                    .Select(m => JToken.FromObject(m))
                    ));
                result["amount"] = query.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }
            return result;
        }
        public static JObject GetIndexA(EJObject condition)
        {

            //IEnumerable<JObject> result = null;
            //IEnumerable<JObject> result = null;
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());
            // pagecount = 3;
            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            string Source_Type = condition.Obj["Source_Type"].ToString();
            string Document_Type = condition.Obj["Document_Type"].ToString();
            string BOOK_TYPE_CODE = condition.Obj["BOOK_TYPE_CODE"].ToString();
            string bno = condition.Obj["bno"].ToString();
            string eno = condition.Obj["eno"].ToString();

            string Jurisdiction2 = condition.Obj["Office_Branch"].ToString();
            DateTime BeginDate;
            DateTime EndDate;

            if (!string.IsNullOrEmpty(condition.Obj["BeginDate"].ToString()))
            {
                BeginDate = DateTime.Parse(condition.Obj["BeginDate"].ToString());
            }
            else
            {
                BeginDate = DateTime.Parse("1900/01/01");
            }
            if (!string.IsNullOrEmpty(condition.Obj["EndDate"].ToString()))
            {
                EndDate = DateTime.Parse(condition.Obj["EndDate"].ToString());
            }
            else
            {
                EndDate = DateTime.Parse("9900/01/01");
            }

            string DESCRIPTION = condition.Obj["DESCRIPTION"].ToString();

            string[] State = ((JArray)condition.Obj["State"]).Select(m => (string)m).ToArray();
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_Evaluation
                            join dtype in db.AS_Assets_DocumentType on mh.Source_Type.ToString() equals dtype.ID.ToString()
                            into ps
                            from dtype in ps.DefaultIfEmpty()
                            join dtype0 in db.AS_Assets_DocumentType on mh.Document_Type.ToString() equals dtype0.ID.ToString()
                            into ps0
                            from dtype0 in ps0.DefaultIfEmpty()

                            join j2 in db.AS_Department on mh.Office_Branch equals j2.Department
                            into p4
                            from j2 in p4.DefaultIfEmpty()

                            join u in db.AS_Users on mh.Created_By equals u.ID
                            into p6
                            from u in p6.DefaultIfEmpty()
                            join ty in db.AS_Code_Table on new { Code_ID = mh.Type.ToString(), Class = "T02" } equals
                                                           new { ty.Code_ID, ty.Class }
                            into p7
                            from ty in p7.DefaultIfEmpty()

                            select new
                            {
                                ID = mh.ID,
                                NO = mh.TRX_Header_ID,
                                LY = dtype.Name,
                                DT = dtype0.Name,
                                DateY = (mh.BeginDate.Value.Year - 1911).ToString(),
                                DateM = (mh.BeginDate.Value.Month).ToString(),
                                DateD = (mh.BeginDate.Value.Day).ToString(),
                                J2 = j2.Department_Name,
                                J2C = mh.Office_Branch,
                                CR = u.User_Name,
                                TY = ty.Text,
                                TYC = mh.Type,
                                CT = mh.Create_Time,
                                DESCRIPTION = mh.DESCRIPTION,
                                BeginDate = mh.BeginDate,
                                Source_Type = mh.Source_Type,
                                BOOK_TYPE_CODE = mh.BOOK_TYPE_CODE,
                                Document_Type = mh.Document_Type,
                                DC = (from d in db.AS_Assets_Evaluation_Detail where d.AID == (long?)mh.ID && d.Land_ID == null select d).ToList().Count(),
                                DDC = (from d in db.AS_Assets_Evaluation_Detail where d.AID == (long?)mh.ID && d.Land_ID == null && d.Type == 3 select d).ToList().Count(),
                                EndDate = mh.EndDate,
                                DeadLine = mh.DeadLine
                            };

                if (!string.IsNullOrEmpty(Document_Type))
                {
                    query = query.Where(m => m.Document_Type.ToString() == Document_Type);
                }

                if (!string.IsNullOrEmpty(Source_Type))
                {
                    query = query.Where(m => m.Source_Type.ToString() == Source_Type);
                }
                if (!string.IsNullOrEmpty(BOOK_TYPE_CODE))
                {
                    query = query.Where(m => m.BOOK_TYPE_CODE.ToString() == BOOK_TYPE_CODE);
                }
                if (!string.IsNullOrEmpty(DESCRIPTION))
                {
                    query = query.Where(m => m.DESCRIPTION.Contains(DESCRIPTION));
                }
                if (!string.IsNullOrEmpty(Jurisdiction2))
                {
                    query = query.Where(m => m.J2C.Contains(Jurisdiction2));
                }
                if (!string.IsNullOrEmpty(bno))
                {
                    query = query.Where(m => string.Compare(m.NO, bno) >= 0);
                }
                if (!string.IsNullOrEmpty(eno))
                {
                    query = query.Where(m => string.Compare(m.NO, eno) <= 0);
                }
                query = query.Where(m => m.BeginDate >= BeginDate);
                query = query.Where(m => m.BeginDate <= EndDate);

                query = query.Where(m => State.Contains(m.TYC.ToString()));


                result = new JObject(new JProperty("Data",
                    query.ToList().OrderByDescending(m => m.CT)
                   .Skip(pagestart).Take(pagecount)
                    .Select(m => JToken.FromObject(m))
                    ));
                result["amount"] = query.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }
            return result;
        }
       
        public static JObject GetUser(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            string UserID = condition.Obj["UserID"].ToString();
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from us in db.AS_Users.AsEnumerable()
                            where us.ID.ToString() == UserID
                            select new {

                                us.ID,
                                Name = us.User_Name,
                                us.Branch_Code,
                                us.Branch_Name,
                                us.Department_Code,
                                us.Department_Name

                            };
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        
        public static JObject GetTarget(EJObject condition)
        {
            //IEnumerable<JObject> result = null;

            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_ActivationTarget
                            select new { mh };
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject GetLand(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());

            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            string[] BID = new string[0];
            if (condition.Obj["ID"] != null)
                BID = ((JArray)condition.Obj["ID"]).Select(m => (string)m).ToArray();



            using (var db = new AS_LandBankEntities())
            {
                var query = from bu in db.AS_Assets_Land.AsEnumerable()
                            select new
                            {
                                ID = bu.ID,
                                Asset_Number = bu.Asset_Number,
                               
                                Area_Size = bu.Area_Size,
                                Current_Cost = bu.Current_Cost,
                                Used_Type = bu.Used_Type,
                                Used_Partition = bu.Used_Partition,
                                Used_Status = bu.Used_Status,
                                City_Name = bu.City_Name,
                                District_Name = bu.District_Name,
                                Sectioni_Name = bu.Section_Name,
                                Sub_Sectioni_Name = bu.Sub_Section_Name,
                                Authorization_Number = bu.Authorization_Number,
                                Asset_Category_code = bu.Asset_Category_code,
                                Business_Book_Amount = bu.Business_Book_Amount,
                                Datetime_Placed_In_Service = bu.Datetime_Placed_In_Service,
                                LN = bu.Parent_Land_Number + bu.Filial_Land_Number
                            };

                if (BID.Length > 0)
                {
                    query = query.Where(m => BID.Contains(m.ID.ToString()));
                }
                ////最後新增的搜尋條件/////
                DateTime bdt;
                DateTime edt;

                if (!string.IsNullOrEmpty(condition.Obj["bdt"].ToString()))
                {
                    bdt = DateTime.Parse(condition.Obj["bdt"].ToString());
                }
                else
                {
                    bdt = DateTime.Parse("1900/01/01");
                }
                if (!string.IsNullOrEmpty(condition.Obj["edt"].ToString()))
                {
                    edt = DateTime.Parse(condition.Obj["edt"].ToString());
                }
                else
                {
                    edt = DateTime.Parse("9900/01/01");
                }
                if (!string.IsNullOrEmpty(condition.Obj["Asset_Category_code1"].ToString()))
                {
                    query = query.Where(m => m.Asset_Category_code.Split('.')[0] == condition.Obj["Asset_Category_code1"].ToString());
                }
                if (!string.IsNullOrEmpty(condition.Obj["Asset_Category_code2"].ToString()))
                {
                    query = query.Where(m => m.Asset_Category_code.Split('.')[1] == condition.Obj["Asset_Category_code2"].ToString());
                }
                if (!string.IsNullOrEmpty(condition.Obj["Asset_Category_code3"].ToString()))
                {
                    query = query.Where(m => m.Asset_Category_code.Split('.')[2] == condition.Obj["Asset_Category_code3"].ToString());
                }
                if (!string.IsNullOrEmpty(condition.Obj["bno"].ToString()))
                {
                    query = query.Where(m => string.Compare(m.Asset_Number, condition.Obj["bno"].ToString()) >= 0);
                }
                if (!string.IsNullOrEmpty(condition.Obj["eno"].ToString()))
                {
                    query = query.Where(m => string.Compare(m.Asset_Number, condition.Obj["eno"].ToString()) <= 0);
                }
                query = query.Where(m => m.Datetime_Placed_In_Service >= bdt);
                query = query.Where(m => m.Datetime_Placed_In_Service <= edt);
                if (!string.IsNullOrEmpty(condition.Obj["Section_Name"].ToString()))
                {
                    query = query.Where(m => m.Sectioni_Name.Contains(condition.Obj["Section_Name"].ToString()));
                }
                if (!string.IsNullOrEmpty(condition.Obj["Description"].ToString()))
                {
                    query = query.Where(m => m.Sectioni_Name.Contains(condition.Obj["Description"].ToString()));
                }
                if (!string.IsNullOrEmpty(condition.Obj["bln"].ToString()))
                {
                    query = query.Where(m => string.Compare(m.LN, condition.Obj["bln"].ToString()) >= 0);
                }
                if (!string.IsNullOrEmpty(condition.Obj["eln"].ToString()))
                {
                    query = query.Where(m => string.Compare(m.LN, condition.Obj["eln"].ToString()) <= 0);
                }
                ////最後新增的搜尋條件/////
                result = new JObject(new JProperty("Data",
                    query.ToList().OrderBy(m => m.Asset_Number)
                   .Skip(pagestart).Take(pagecount)
                    .Select(m => JToken.FromObject(m))
                    ));

                result["amount"] = query.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }
            return result;
        }
        public static JObject GetBuild(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());

            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            string[] BID = new string[0];
            if (condition.Obj["ID"] != null)
                BID = ((JArray)condition.Obj["ID"]).Select(m => (string)m).ToArray();

            using (var db = new AS_LandBankEntities())
            {
                var query = from bu in db.AS_Assets_Build
                            join d in db.AS_Assets_Build_Detail on bu.ID equals d.Assets_Build_ID
                            into ps
                            from d in ps.DefaultIfEmpty()
                            group d by new
                            {
                                bu.ID,
                                bu.Asset_Number,
                                bu.Asset_Category_Code,
                                bu.Description,
                                bu.Business_Book_Amount,
                                bu.Date_Placed_In_Service,
                                bu.City_Name,
                                bu.District_Name,
                                bu.Sectioni_Name,
                                bu.Sub_Sectioni_Name,
                                bu.Build_Address,
                                // bu.Authorized_Area,
                                bu.Current_Cost,
                                bu.Used_Type,
                                bu.Used_Status,
                             
                            } into g
                            select new
                            {
                                g.Key.ID,
                                g.Key.Asset_Number,
                                g.Key.Asset_Category_Code,
                                g.Key.Description,
                                g.Key.Business_Book_Amount,
                                g.Key.Date_Placed_In_Service,
                                g.Key.City_Name,
                                g.Key.District_Name,
                                g.Key.Sectioni_Name,
                                g.Key.Sub_Sectioni_Name,
                                g.Key.Build_Address,
                                //g.Key.Authorized_Area,
                                g.Key.Current_Cost,
                                g.Key.Used_Type,
                                g.Key.Used_Status,
                                Area_Size = (decimal?)g.Sum(p => p.Area_Size)
                            };
                /*
                 select new
                 {
                     ID = bu.ID,
                       Asset_Number = bu.Asset_Number,
                       Asset_Category_Code = bu.Asset_Category_Code,
                       Description = bu.Description,
                       Business_Book_Amount = bu.Business_Book_Amount,
                       Date_Placed_In_Service = bu.Date_Placed_In_Service,
                       Build_Address=bu.Build_Address,
                     //Authorized_Area = bu.Authorized_Area,
                     Current_Cost = bu.Current_Cost,
                     Used_Type = bu.Used_Type,
                     //Used_Partition = bu.Used_Partition,
                     Used_Status = bu.Used_Status
                 };*/
                if (BID.Length > 0)
                {
                    query = query.Where(m => BID.Contains(m.ID.ToString()));
                }
                ////最後新增的搜尋條件/////
                DateTime bdt;
                DateTime edt;

                if (!string.IsNullOrEmpty(condition.Obj["bdt"].ToString()))
                {
                    bdt = DateTime.Parse(condition.Obj["bdt"].ToString());
                }
                else
                {
                    bdt = DateTime.Parse("1900/01/01");
                }
                if (!string.IsNullOrEmpty(condition.Obj["edt"].ToString()))
                {
                    edt = DateTime.Parse(condition.Obj["edt"].ToString());
                }
                else
                {
                    edt = DateTime.Parse("9900/01/01");
                }
                if (!string.IsNullOrEmpty(condition.Obj["Asset_Category_code1"].ToString()))
                {
                    query = query.Where(m => m.Asset_Category_Code.Split('.')[0] == condition.Obj["Asset_Category_code1"].ToString());
                }
                if (!string.IsNullOrEmpty(condition.Obj["Asset_Category_code2"].ToString()))
                {
                    query = query.Where(m => m.Asset_Category_Code.Split('.')[1] == condition.Obj["Asset_Category_code2"].ToString());
                }
                if (!string.IsNullOrEmpty(condition.Obj["Asset_Category_code3"].ToString()))
                {
                    query = query.Where(m => m.Asset_Category_Code.Split('.')[2] == condition.Obj["Asset_Category_code3"].ToString());
                }
                if (!string.IsNullOrEmpty(condition.Obj["bno"].ToString()))
                {
                    query = query.Where(m => string.Compare(m.Asset_Number, condition.Obj["bno"].ToString()) >= 0);
                }
                if (!string.IsNullOrEmpty(condition.Obj["eno"].ToString()))
                {
                    query = query.Where(m => string.Compare(m.Asset_Number, condition.Obj["eno"].ToString()) <= 0);
                }
                query = query.Where(m => m.Date_Placed_In_Service >= bdt);
                query = query.Where(m => m.Date_Placed_In_Service <= edt);
                if (!string.IsNullOrEmpty(condition.Obj["Section_Name"].ToString()))
                {
                    query = query.Where(m => m.Sectioni_Name.Contains(condition.Obj["Section_Name"].ToString()));
                }
                if (!string.IsNullOrEmpty(condition.Obj["Description"].ToString()))
                {
                    query = query.Where(m => m.Sectioni_Name.Contains(condition.Obj["Description"].ToString()));
                }
                if (!string.IsNullOrEmpty(condition.Obj["bln"].ToString()))
                {
                    //query = query.Where(m => string.Compare(m.LN, condition.Obj["bln"].ToString()) >= 0);
                }
                if (!string.IsNullOrEmpty(condition.Obj["eln"].ToString()))
                {
                   // query = query.Where(m => string.Compare(m.LN, condition.Obj["eln"].ToString()) <= 0);
                }
                ////最後新增的搜尋條件/////

                result = new JObject(new JProperty("Data",
                    query.ToList().OrderBy(m => m.Asset_Number)
                   .Skip(pagestart).Take(pagecount)
                    .Select(m => JToken.FromObject(m))
                    ));

                result["amount"] = query.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }
            return result;
        }
        public static JObject GetMaxNo(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
           
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from AS_Assets_Evaluation in
                         (from AS_Assets_Evaluation in db.AS_Assets_Evaluation.AsEnumerable()
                          select new
                          {
                              Column1 = AS_Assets_Evaluation.TRX_Header_ID.Substring(12 - 1, 6),
                              Dummy = "x"
                          })
                            group AS_Assets_Evaluation by new { AS_Assets_Evaluation.Dummy } into g
                            select new
                            {
                                maxno = (int?)(Int32)g.Max(p => Convert.ToInt32(p.Column1) + 1)
                            };
                result = new JObject(new JProperty("Data", query.ToList().Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject GetHisInsA(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            string BuildID = condition.Obj["BuildID"].ToString();
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from d in db.AS_Assets_Evaluation_Detail.AsEnumerable()
                            where d.Build_ID.ToString() == BuildID && d.Land_ID == null
                            join m in db.AS_Assets_Evaluation on d.AID equals m.ID
                            join co in db.AS_Assets_DocumentType on m.Source_Type equals co.ID
                            join b in db.AS_Assets_Build on d.Build_ID equals b.ID
                            select new { m, d, ST = co.Name, CC = b.Current_Cost };

                result = new JObject(new JProperty("Data", query.ToList().OrderByDescending(m => m.m.TRX_Header_ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject GetHisIns(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            string BuildID = condition.Obj["BuildID"].ToString();
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from d in db.AS_Assets_Evaluation_Detail.AsEnumerable()
                            where d.Build_ID.ToString() == BuildID && d.Land_ID == null
                            join m in db.AS_Assets_Evaluation on d.BID equals m.ID
                            join co in db.AS_Assets_DocumentType on m.Source_Type equals co.ID
                            join b in db.AS_Assets_Build on d.Build_ID equals b.ID
                            select new { m, d, ST = co.Name,CC=b.Current_Cost };

                result = new JObject(new JProperty("Data", query.ToList().OrderByDescending(m => m.m.TRX_Header_ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        
        public static JObject DocumentType(EInt type1)
        {
           
            int type = type1.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_DocumentType
                            select new { mh };
                query = query.Where(m => m.mh.ID >= type * 1000).Where(m => m.mh.ID < (type + 1) * 1000);
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
            }
            
            return result;
        }
        public static JObject DocumentTypeOne(EInt type1)
        {
            int type = type1.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_DocumentType
                            select new { mh };
                query = query.Where(m => m.mh.ID == type);
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject GetDPM(EInt type1)
        {
            int type = type1.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Department
                            select new { mh };
                if (type == 1)
                    query = query.Where(m => m.mh.Branch_Code != "001");
                if (type == 2)
                    query = query.Where(m => m.mh.Branch_Code == "001");
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject GetDetail(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());

            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            string[] BID = new string[0];
            if (condition.Obj["ID"] != null)
                BID = ((JArray)condition.Obj["ID"]).Select(m => (string)m).ToArray();

          

            var bo = false;
            if (BID.Length >= 1)
            {
                if (((BID.Length == 1) & (BID[0] == "0")))
                    bo = true;
            }
            else
                bo = true;

            string aid = condition.Obj["AID"].ToString();
            using (var db = new AS_LandBankEntities())
            {
                var query = from d in db.AS_Assets_Evaluation_Detail.AsEnumerable()
                            join l in db.AS_Assets_Build on d.Build_ID equals l.ID
                        into ps
                            from l in ps.DefaultIfEmpty()
                            where d.BID.ToString() == aid && d.Land_ID == null &&
                             (BID.Contains(d.Build_ID.ToString()) || (bo))

                            select new
                            {
                                ID=d.ID,
                                Build_ID = d.Build_ID ?? 0,
                                Asset_Number = l.Asset_Number,
                               // Area_Size = l.Area_Size,
                                //Current_Cost = l.Announce_Amount,
                                Description = l.Description,
                                City_Name = l.City_Name,
                                District_Name = l.District_Name,
                                Sectioni_Name = l.Sectioni_Name,
                                Sub_Sectioni_Name = l.Sub_Sectioni_Name,
                                Authorization_Number = l.Build_Address,
                                BeginDate = ((d.BeginDate ?? DateTime.Now).Year - 1911).ToString() + "-" +
                                            ((d.BeginDate ?? DateTime.Now).Month).ToString() + "-" +
                                            ((d.BeginDate ?? DateTime.Now).Day).ToString(),
                                Detail_No = d.Detail_No,
                                TType = d.Type??1,
                                Asset_Category_code = l.Asset_Category_Code,
                                IsNew = false
                            };

                var tmpq = query.Select(m => m.Build_ID).ToArray();

                var query2 = from l in db.AS_Assets_Build.AsEnumerable()
                             where 
                             BID.Contains(l.ID.ToString())  && 
                             !(tmpq).Contains(l.ID)
                             select new
                             {
                                 ID = (Int64)0,
                                 Build_ID = l.ID,
                                 Asset_Number = l.Asset_Number,
                                 //Area_Size = l.Area_Size,
                                // Current_Cost = l.Current_Cost,
                                 Description = l.Description,
                                 City_Name = l.City_Name,
                                 District_Name = l.District_Name,
                                 Sectioni_Name = l.Sectioni_Name,
                                 Sub_Sectioni_Name = l.Sub_Sectioni_Name,
                                 Authorization_Number = l.Authorization_Number,
                                 BeginDate = ((DateTime.Now).Year - 1911).ToString() + "-" +
                                            ((DateTime.Now).Month).ToString() + "-" +
                                            (( DateTime.Now).Day).ToString(),
                                 Detail_No = "",
                                 TType = -1,
                                 Asset_Category_code =l.Asset_Category_Code,
                                 IsNew=true
                             };
                
                query = query.Concat(query2);
                var tmpq2 = query.Select(m => m.Build_ID).ToArray();
             

                result = new JObject(new JProperty("Data",
                    query.ToList().OrderBy(m => m.Asset_Number)
                   .Skip(pagestart).Take(pagecount)
                    .Select(m => JToken.FromObject(m))
                    ));

                result["tmpq2"] = JToken.FromObject(tmpq2);
                result["amount"] = query.ToList().Count;
                result["amount2"] = query2.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }
           
            return result;
        }
        public static JObject GetDetailA(EJObject condition)
        {
            //IEnumerable<JObject> result = null;
            JObject result = null;
            int pagecount = Int32.Parse(condition.Obj["PageSize"].ToString());

            int pagestart = (Int32.Parse(condition.Obj["Page"].ToString()) - 1) * pagecount;
            string[] BID = new string[0];
            if (condition.Obj["ID"] != null)
                BID = ((JArray)condition.Obj["ID"]).Select(m => (string)m).ToArray();



            var bo = false;
            if (BID.Length >= 1)
            {
                if (((BID.Length == 1) & (BID[0] == "0")))
                    bo = true;
            }
            else
                bo = true;

            string aid = condition.Obj["AID"].ToString();
            using (var db = new AS_LandBankEntities())
            {
                var query = from d in db.AS_Assets_Evaluation_Detail.AsEnumerable()
                            join l in db.AS_Assets_Build on d.Build_ID equals l.ID
                        into ps
                            from l in ps.DefaultIfEmpty()
                            where d.AID.ToString() == aid && d.Land_ID == null &&
                             (BID.Contains(d.Build_ID.ToString()) || (bo))

                            select new
                            {
                                ID = d.ID,
                                Build_ID = d.Build_ID ?? 0,
                                Asset_Number = l.Asset_Number,
                                // Area_Size = l.Area_Size,
                                //Current_Cost = l.Announce_Amount,
                                Description = l.Description,
                                City_Name = l.City_Name,
                                District_Name = l.District_Name,
                                Sectioni_Name = l.Sectioni_Name,
                                Sub_Sectioni_Name = l.Sub_Sectioni_Name,
                                Authorization_Number = l.Build_Address,
                                BeginDate = ((d.BeginDate ?? DateTime.Now).Year - 1911).ToString() + "-" +
                                            ((d.BeginDate ?? DateTime.Now).Month).ToString() + "-" +
                                            ((d.BeginDate ?? DateTime.Now).Day).ToString(),
                                Detail_No = d.Detail_No,
                                TType = d.Type ?? 1,
                                Asset_Category_code = l.Asset_Category_Code,
                                IsNew = false
                            };

                var tmpq = query.Select(m => m.Build_ID).ToArray();

                var query2 = from l in db.AS_Assets_Build.AsEnumerable()
                             where
                             BID.Contains(l.ID.ToString()) &&
                             !(tmpq).Contains(l.ID)
                             select new
                             {
                                 ID = (Int64)0,
                                 Build_ID = l.ID,
                                 Asset_Number = l.Asset_Number,
                                 //Area_Size = l.Area_Size,
                                 // Current_Cost = l.Current_Cost,
                                 Description = l.Description,
                                 City_Name = l.City_Name,
                                 District_Name = l.District_Name,
                                 Sectioni_Name = l.Sectioni_Name,
                                 Sub_Sectioni_Name = l.Sub_Sectioni_Name,
                                 Authorization_Number = l.Authorization_Number,
                                 BeginDate = ((DateTime.Now).Year - 1911).ToString() + "-" +
                                            ((DateTime.Now).Month).ToString() + "-" +
                                            ((DateTime.Now).Day).ToString(),
                                 Detail_No = "",
                                 TType = -1,
                                 Asset_Category_code = l.Asset_Category_Code,
                                 IsNew = true
                             };

                query = query.Concat(query2);
                var tmpq2 = query.Select(m => m.Build_ID).ToArray();


                result = new JObject(new JProperty("Data",
                    query.ToList().OrderBy(m => m.Asset_Number)
                   .Skip(pagestart).Take(pagecount)
                    .Select(m => JToken.FromObject(m))
                    ));

                result["tmpq2"] = JToken.FromObject(tmpq2);
                result["amount"] = query.ToList().Count;
                result["amount2"] = query2.ToList().Count;
                result["pagestart"] = pagestart;
                result["pagecount"] = pagecount;
            }

            return result;
        }
        public static JObject GetOrder(EString no1)
        {
            string no = no1.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_Evaluation
                            join ty in db.AS_Code_Table on new { Code_ID = mh.Type.ToString(), Class = "T02" } equals
                                                           new { ty.Code_ID, ty.Class }
                            into p7
                            from ty in p7.DefaultIfEmpty()
                            select new
                            {
                                mh,
                                DateY = (mh.BeginDate.Value.Year /*- 1911*/).ToString(),
                                DateM = (mh.BeginDate.Value.Month).ToString(),
                                DateD = (mh.BeginDate.Value.Day).ToString(),
                                TY = ty.Text
                            };
                query = query.Where(m => m.mh.ID.ToString() == no);
               /* var query1 = from c in db.AS_Assets_Activation_LBDetail
                             select new { ID = c.ID, AID = c.Activation_ID, TY = c.Type };
                query1 = query1.Where(m => m.AID.ToString() == no);*/

                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
               // result["LC"] = query1.Where(m => m.TY == 1).ToList().Count();
               // result["BC"] = query1.Where(m => m.TY == 0).ToList().Count();

            }
            return result;
        }
        public static JObject GetBookCode(EString type1)
        {

            string type = type1.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Code_Table
                            select new { mh };
                query = query.Where(m => m.mh.Class == type);
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.mh.ID).Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        public static JObject ContentDetail(EJObject type1)
        {

            JObject type = type1.Obj;
            string ID = type["id"].ToString();
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from m in db.AS_Assets_Evaluation_Detail
                            join l in db.AS_Assets_Build on m.Build_ID equals l.ID
                            join ma in db.AS_Assets_Evaluation on m.BID equals ma.ID
                            join co in db.AS_Assets_DocumentType on ma.Source_Type equals co.ID
                            join j2 in db.AS_Department on ma.Office_Branch equals j2.Department
                            from log in db.AS_Assets_Build_Log.Where(a => a.BuildID == m.Build_ID).Where(a=>a.ChangeTime < ma.DeadLine).DefaultIfEmpty()
                           
                            //where log.ChangeTime < ma.DeadLine
                            group log by new
                            {
                                m,
                                l,
                                CONAME=co.Name,
                                j2Department_Name = j2.Department_Name,
                                ma
                              
                            } into g
                            select new
                            {
                                g.Key.m,
                                g.Key.ma,
                                g.Key.l,
                                ST = g.Key.CONAME,
                                OB = g.Key.j2Department_Name,
                                logCurrent_Cost = g.Max(p => p.Current_Cost)
                            };
                //select new { m,l,ST=co.Name,OB= j2.Department_Name,ma };
                query = query.Where(m => m.m.ID.ToString() == ID);
                var query2 = from m in db.AS_Assets_Evaluation_Detail
                            join l in db.AS_Assets_Land on m.Land_ID equals l.ID
                             from log in db.AS_Assets_Land_Log.Where(a => a.LandID == m.Land_ID).Where(a => a.ChangeTime < query.FirstOrDefault().ma.DeadLine).DefaultIfEmpty()
                             group log by new
                             {
                                 m,
                                 l
                             } into g
                             select new
                             {
                                 g.Key.m,
                                 g.Key.l,
                                 logCurrent_Cost = g.Max(p => p.Current_Cost)
                             };
               // select new { m, l  };
                query2 = query2.Where(m => m.m.BID.ToString() == query.FirstOrDefault().m.BID.ToString())
                                  .Where(m => m.m.Build_ID.ToString() == query.FirstOrDefault().m.Build_ID.ToString())
                                .Where(m => m.m.Land_ID != null);
                var query3 = from m in db.AS_Assets_Build_Detail
                             where m.Assets_Build_ID == query.FirstOrDefault().m.Build_ID
                             select new { m };

                result = new JObject(new JProperty("Data", query.ToList().Select(m => JToken.FromObject(m))),
                    new JProperty("Data2", query2.ToList().Select(m => JToken.FromObject(m))),
                    new JProperty("Data3", query3.ToList().Sum(m=> m.m.Area_Size)));
            }
            return result;
        }
        public static JObject ContentDetailA(EJObject type1)
        {

            JObject type = type1.Obj;
            string ID = type["id"].ToString();
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from m in db.AS_Assets_Evaluation_Detail
                            join l in db.AS_Assets_Build on m.Build_ID equals l.ID
                            join ma in db.AS_Assets_Evaluation on m.AID equals ma.ID
                            join co in db.AS_Assets_DocumentType on ma.Source_Type equals co.ID
                            join j2 in db.AS_Department on ma.Office_Branch equals j2.Department
                            from log in db.AS_Assets_Build_Log.Where(a => a.BuildID == m.Build_ID).Where(a => a.ChangeTime < ma.DeadLine).DefaultIfEmpty()

                                //where log.ChangeTime < ma.DeadLine
                            group log by new
                            {
                                m,
                                l,
                                CONAME = co.Name,
                                j2Department_Name = j2.Department_Name,
                                ma

                            } into g
                            select new
                            {
                                g.Key.m,
                                g.Key.ma,
                                g.Key.l,
                                ST = g.Key.CONAME,
                                OB = g.Key.j2Department_Name,
                                logCurrent_Cost = g.Max(p => p.Current_Cost)
                            };
                //select new { m,l,ST=co.Name,OB= j2.Department_Name,ma };
                query = query.Where(m => m.m.ID.ToString() == ID);
                var query2 = from m in db.AS_Assets_Evaluation_Detail
                             join l in db.AS_Assets_Land on m.Land_ID equals l.ID
                             from log in db.AS_Assets_Land_Log.Where(a => a.LandID == m.Land_ID).Where(a => a.ChangeTime < query.FirstOrDefault().ma.DeadLine).DefaultIfEmpty()
                             group log by new
                             {
                                 m,
                                 l
                             } into g
                             select new
                             {
                                 g.Key.m,
                                 g.Key.l,
                                 logCurrent_Cost = g.Max(p => p.Current_Cost)
                             };
                // select new { m, l  };
                query2 = query2.Where(m => m.m.AID.ToString() == query.FirstOrDefault().m.AID.ToString())
                                  .Where(m => m.m.Build_ID.ToString() == query.FirstOrDefault().m.Build_ID.ToString())
                                .Where(m => m.m.Land_ID != null);
                var query3 = from m in db.AS_Assets_Build_Detail
                             where m.Assets_Build_ID == query.FirstOrDefault().m.Build_ID
                             select new { m };

                result = new JObject(new JProperty("Data", query.ToList().Select(m => JToken.FromObject(m))),
                    new JProperty("Data2", query2.ToList().Select(m => JToken.FromObject(m))),
                    new JProperty("Data3", query3.ToList().Sum(m => m.m.Area_Size)));
            }
            return result;
        }
        public static JObject ApplyEDOC(EString type1)
        {
            JObject result = null;
            return result;
        }
        
        public static JObject UpdateOrder(EJObject model2)
        {
            JObject model = new JObject();
            model = model2.Obj;
            long nowidd = 0;
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    for (int i = 0; i < model.Count; i++)
                    {
                        AS_Assets_Evaluation AA;
                        AS_Assets_Evaluation_Detail ALB;
                       
                        if (model["Data"]["ID"] == null)
                        {

                            AA = new AS_Assets_Evaluation();
                            nowidd = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                            AA.ID = nowidd;
                            AA.Create_Time = DateTime.Now;
                            AA.Last_Updated_Time = DateTime.Now;
                            db.Entry(AA).State = System.Data.Entity.EntityState.Added;
                        }
                        else
                        {
                            long idd = 0;
                            Int64.TryParse(model["Data"]["ID"].ToString(), out idd);
                            AA = db.AS_Assets_Evaluation.FirstOrDefault(m => m.ID == idd);
                            nowidd = idd;
                            AA.Last_Updated_Time = DateTime.Now;
                            db.Entry(AA).State = System.Data.Entity.EntityState.Modified;
                        }
                        String[] BU = new String[0];
                       
                       
                         
                        if (model["Data"]["DataB"] != null)
                        {
                            if (model["Data"]["DataB"].ToString() != "")
                            {
                                BU = model["Data"]["DataB"].ToString().Split(',');
                                db.AS_Assets_Evaluation_Detail.RemoveRange(
                                    db.AS_Assets_Evaluation_Detail.Where(m => m.BID == nowidd)
                                    .Where(m => !(BU).Contains(m.Build_ID.ToString()))
                                    );
                                db.Configuration.AutoDetectChangesEnabled = false;
                             
                                for (int j = 0; j < BU.Length; j++)
                                {
                                   var Co = db.AS_Assets_Evaluation_Detail.AsEnumerable()
                                        .Where(m => m.BID == nowidd)
                                        .Where(m => m.Build_ID == Int32.Parse(BU[j])).Count() ;
                                    if (Co == 0)
                                    {
                                        ALB = new AS_Assets_Evaluation_Detail();
                                        ALB.ID = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                                        ALB.BID = nowidd;
                                        ALB.Build_ID = Int32.Parse(BU[j]);
                                        ALB.BeginDate = DateTime.Now;
                                        ALB.Type = 1;
                                        db.AS_Assets_Evaluation_Detail.Add(ALB);

                                        var Lo = db.AS_Assets_Build_Land.AsEnumerable()
                                        .Where(m => m.Asset_Build_ID == Int32.Parse(BU[j])).ToList();
                                        foreach (var item in Lo)
                                        {
                                            ALB = new AS_Assets_Evaluation_Detail();
                                            ALB.ID = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                                            ALB.BID = nowidd;
                                            ALB.Build_ID = Int32.Parse(BU[j]);
                                            ALB.Land_ID = item.Asset_Land_ID;
                                            ALB.BeginDate = DateTime.Now;
                                            ALB.Type = 1;
                                            db.AS_Assets_Evaluation_Detail.Add(ALB);
                                        }
                                    }
                                    else
                                    {
                                        //todo


                                    }
                                }
                            }
                        }
                        
                        AA.BeginDate = EvaluationGeneral.CvtS2D(model["Data"]["BeginDate"].ToString().Replace("/", "-"));
                        AA.EndDate = EvaluationGeneral.CvtS2D(model["Data"]["EndDate"].ToString().Replace("/", "-"));
                        AA.DeadLine = EvaluationGeneral.CvtS2D(model["Data"]["DeadLine"].ToString().Replace("/", "-"));
                        AA.TRX_Header_ID = model["Data"]["TRX_Header_ID"].ToString();
                        AA.Document_Type = Int64.Parse(model["Data"]["Document_Type"].ToString());
                        AA.Source_Type = Int32.Parse(model["Data"]["Source_Type"].ToString());
                        AA.BOOK_TYPE_CODE = model["Data"]["BOOK_TYPE_CODE"].ToString();
                       
                        AA.Office_Branch = model["Data"]["Office_Branch"].ToString();
                     
                        AA.DESCRIPTION = model["Data"]["DESCRIPTION"].ToString();
                        AA.Type = Int32.Parse(model["Data"]["Type"].ToString());
                        AA.Created_By = Int32.Parse(model["Data"]["Created_By"].ToString());
                        AA.Last_Updated_By = Int32.Parse(model["Data"]["Last_Updated_By"].ToString());



                        db.SaveChanges();
                        db.Configuration.AutoDetectChangesEnabled = true;
                    }
                }
                trans.Complete();
            }
            JObject result = new JObject(new JProperty("TRX_Header_ID", nowidd.ToString()));
            return result;
        }
        public static JObject GetPicList(EJObject type1)
        {

            JObject type = type1.Obj;
            string ID = type["ID"].ToString();
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from m in db.AS_Assets_Evaluation_File
                           
                            select new { ID=m.ID.ToString(),PN =  m.FileName,DID=m.DetailID  };
                query = query.Where(m => m.DID.ToString() == ID);
                result = new JObject(new JProperty("Data", query.ToList().Select(m => JToken.FromObject(m))));
            }
            return result;
        }
        
        public static JObject SavePic(EJObject model2)
        {
            long nowidd = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
            using (var db = new AS_LandBankEntities())
            {
                AS_Assets_Evaluation_File AA = new AS_Assets_Evaluation_File();
               
                AA.ID = nowidd;
                AA.DetailID = Int64.Parse(model2.Obj["ID"].ToString());
                AA.FileName = model2.Obj["FileName"].ToString();
                db.Entry(AA).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();
            }
            JObject result = new JObject(new JProperty("ID", nowidd.ToString()));
            return result ;
        }
            
        public static JObject UpdateDetail(EJObject model2)
        {
            JObject model = new JObject();
            model = model2.Obj;
            long nowidd = 0;
           
           // Int64.TryParse(model["ID"].ToString(), out nowidd);
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    for (int i = 0; i < model["Data"]["Land"].Count(); i++)
                    {
                       AS_Assets_Evaluation_Detail AA;
                      
                        AA = db.AS_Assets_Evaluation_Detail.AsEnumerable()
                            .FirstOrDefault(m => m.ID.ToString() == model["Data"]["Land"][i]["ID"].ToString());
                       AA.Last_Updated_Time = DateTime.Now;
                       AA.Last_Updated_By = Int32.Parse(model["Last_Updated_By"].ToString());
                        decimal cc = 0;
                        decimal.TryParse(model["Data"]["Land"][i]["Current_Cost"].ToString(), out cc);
                       AA.Current_Cost = cc;
                        if (model["Data"]["Land"][i]["EvaluationType"] != null)
                            AA.EvaluationType = Int32.Parse(model["Data"]["Land"][i]["EvaluationType"].ToString());
                      
                       AA.EvaluationResult = model["Data"]["Land"][i]["EvaluationResult"].ToString();
                       AA.EvaluationTypeETC = model["Data"]["Land"][i]["EvaluationTypeETC"].ToString();
                        db.Entry(AA).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                      
                    }
               
                    AS_Assets_Evaluation_Detail BC;

                    BC = db.AS_Assets_Evaluation_Detail.AsEnumerable()
                             .FirstOrDefault(m => m.ID.ToString() == model["Data"]["Build"]["ID"].ToString());
                    BC.Last_Updated_Time = DateTime.Now;
                    BC.Last_Updated_By = Int32.Parse(model["Last_Updated_By"].ToString());
                    decimal dd = 0;
                    decimal.TryParse(model["Data"]["Build"]["Current_Cost"].ToString(), out dd);
                    BC.Current_Cost = dd;
                        if (model["Data"]["Build"]["EvaluationType"] != null)
                        BC.EvaluationType = Int32.Parse(model["Data"]["Build"]["EvaluationType"].ToString());
                    BC.DESCRIPTION = model["Data"]["Build"]["DESCRIPTION"].ToString();
                    BC.Type = (Int32)model["Data"]["Build"]["Type"];
                    BC.EvaluationResult = model["Data"]["Build"]["EvaluationResult"].ToString();
                    BC.EvaluationTypeETC = model["Data"]["Build"]["EvaluationTypeETC"].ToString();
                        db.Entry(BC).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                    
                }
                trans.Complete();
            }
            
            JObject result = new JObject(new JProperty("TRX_Header_ID", model["Data"]["Build"]["ID"].ToString()));
            return result;
        }
        public static JObject UpdateOrderA(EJObject model2)
        {
            JObject model = new JObject();
            model = model2.Obj;
            long nowidd = 0;
            using (var trans = new TransactionScope())
            {
                using (var db = new AS_LandBankEntities())
                {
                    for (int i = 0; i < model.Count; i++)
                    {
                        AS_Assets_Evaluation AA;
                        AS_Assets_Evaluation_Detail ALB;

                        if (model["Data"]["ID"] == null)
                        {

                            AA = new AS_Assets_Evaluation();
                            nowidd = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                            AA.ID = nowidd;
                            AA.Create_Time = DateTime.Now;
                            AA.Last_Updated_Time = DateTime.Now;
                            db.Entry(AA).State = System.Data.Entity.EntityState.Added;
                        }
                        else
                        {
                            long idd = 0;
                            Int64.TryParse(model["Data"]["ID"].ToString(), out idd);
                            AA = db.AS_Assets_Evaluation.FirstOrDefault(m => m.ID == idd);
                            nowidd = idd;
                            AA.Last_Updated_Time = DateTime.Now;
                            db.Entry(AA).State = System.Data.Entity.EntityState.Modified;
                        }
                        String[] BU = new String[0];


                        if (model["Data"]["DataB"] != null)
                        {
                            if (model["Data"]["DataB"].ToString() != "")
                            {
                                BU = model["Data"]["DataB"].ToString().Split(',');
                                db.AS_Assets_Evaluation_Detail.RemoveRange(
                                    db.AS_Assets_Evaluation_Detail.Where(m => m.AID == nowidd)
                                    .Where(m => !(BU).Contains(m.Build_ID.ToString()))
                                    );
                                db.Configuration.AutoDetectChangesEnabled = false;

                                for (int j = 0; j < BU.Length; j++)
                                {
                                    var Co = db.AS_Assets_Evaluation_Detail.AsEnumerable()
                                         .Where(m => m.AID == nowidd)
                                         .Where(m => m.Build_ID == Int32.Parse(BU[j])).Count();
                                    if (Co == 0)
                                    {
                                        ALB = new AS_Assets_Evaluation_Detail();
                                        ALB.ID = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                                        ALB.AID = nowidd;
                                        ALB.Build_ID = Int32.Parse(BU[j]);
                                        ALB.BeginDate = DateTime.Now;
                                        ALB.Type = 1;
                                        db.AS_Assets_Evaluation_Detail.Add(ALB);

                                        var Lo = db.AS_Assets_Build_Land.AsEnumerable()
                                        .Where(m => m.Asset_Build_ID == Int32.Parse(BU[j])).ToList();
                                        foreach (var item in Lo)
                                        {
                                            ALB = new AS_Assets_Evaluation_Detail();
                                            ALB.ID = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
                                            ALB.AID = nowidd;
                                            ALB.Build_ID = Int32.Parse(BU[j]);
                                            ALB.Land_ID = item.Asset_Land_ID;
                                            ALB.BeginDate = DateTime.Now;
                                            ALB.Type = 1;
                                            db.AS_Assets_Evaluation_Detail.Add(ALB);
                                        }
                                    }
                                    else
                                    {
                                        //todo


                                    }
                                }
                            }
                        }

                        AA.BeginDate = EvaluationGeneral.CvtS2D(model["Data"]["BeginDate"].ToString().Replace("/", "-"));
                        AA.EndDate = EvaluationGeneral.CvtS2D(model["Data"]["EndDate"].ToString().Replace("/", "-"));
                        AA.DeadLine = EvaluationGeneral.CvtS2D(model["Data"]["DeadLine"].ToString().Replace("/", "-"));
                        AA.TRX_Header_ID = model["Data"]["TRX_Header_ID"].ToString();
                        AA.Document_Type = Int64.Parse(model["Data"]["Document_Type"].ToString());
                        AA.Source_Type = Int32.Parse(model["Data"]["Source_Type"].ToString());
                        AA.BOOK_TYPE_CODE = model["Data"]["BOOK_TYPE_CODE"].ToString();

                        AA.Office_Branch = model["Data"]["Office_Branch"].ToString();

                        AA.DESCRIPTION = model["Data"]["DESCRIPTION"].ToString();
                        AA.Type = Int32.Parse(model["Data"]["Type"].ToString());
                        AA.Created_By = Int32.Parse(model["Data"]["Created_By"].ToString());
                        AA.Last_Updated_By = Int32.Parse(model["Data"]["Last_Updated_By"].ToString());



                        db.SaveChanges();
                        db.Configuration.AutoDetectChangesEnabled = true;
                    }
                }
                trans.Complete();
            }
            JObject result = new JObject(new JProperty("TRX_Header_ID", nowidd.ToString()));
            return result;
        }
        public static JObject GetReport(EJObject C)
        {

            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Assets_Evaluation
                            join d in db.AS_Assets_Evaluation_Detail on mh.ID equals d.BID
                            join l in db.AS_Assets_Land on d.Build_ID equals l.ID
                            join dt in db.AS_Assets_DocumentType on mh.Document_Type equals dt.ID
                            join so in db.AS_Assets_DocumentType on mh.Source_Type equals so.ID
                            select new {
                                d.Detail_No,
                                d.EvaluationResult,
                                d.EvaluationType,
                                d.BeginDate,
                                d.DESCRIPTION,
                                mh.TRX_Header_ID,
                                mh.Type,
                                Document_Type = dt.Name,
                                Source_Type = so.Name,
                                mh.BOOK_TYPE_CODE,
                                d.ID,
                                City_Name = l.City_Name,
                                District_Name = l.District_Name,
                                Sectioni_Name = l.Section_Name,
                                Sub_Sectioni_Name = l.Sub_Section_Name,
                                Authorization_Number = l.Authorization_Number,
                              
                            };
                
                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.ID).Select(m => JToken.FromObject(m))));
                
            }
            return result;
        }
        public static JObject GetCategoryKind(EInt C)
        {
            int CC = C.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Asset_Category_Detail_Kinds
                           where ((mh.Is_Active == true) & (mh.Main_Kind_ID == CC))
                            select new
                            {
                                mh.No,
                                mh.Name
                            };

                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.No).Select(m => JToken.FromObject(m))));

            }
            return result;
        }
        public static JObject GetCategoryType(EInt C)
        {
            int CC = C.type;
            JObject result = null;
            using (var db = new AS_LandBankEntities())
            {
                var query = from mh in db.AS_Asset_Category_Use_Types
                            where (mh.Is_Active == true) 
                            select new
                            {
                                mh.No,
                                mh.Name
                            };

                result = new JObject(new JProperty("Data", query.ToList().OrderBy(m => m.No).Select(m => JToken.FromObject(m))));

            }
            return result;
        }
    }
}
