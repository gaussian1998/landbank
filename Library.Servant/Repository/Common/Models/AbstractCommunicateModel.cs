﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;

namespace Library.Servant.Repository.Models
{
    public abstract class AbstractCommunicateModel : AbstractEncryptionDTO
    {
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
