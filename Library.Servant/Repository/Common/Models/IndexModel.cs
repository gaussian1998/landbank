﻿using Library.Interface;
using Library.Servant.Communicate;
using Library.Servant.SymmetryEncryption;
using System.Collections.Generic;

namespace Library.Servant.Repository.Models
{
    public class IndexModel<Item> : AbstractEncryptionDTO
    {
        public List<Item> Items { get; set; }
        public override IEncryption GetEncryption()
        {
            return EncryptionTools.authServer;
        }
    }
}
