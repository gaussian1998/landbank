﻿using System;
using Library.Servant.Repository.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Common;
using Library.Servant.Servant;
using Library.Servant.Servant.Common.Models;

namespace Library.Servant.Repository
{
    public abstract class RemoteGenericRepository<Entity, IndexItem, DetailsResult, CreateModel, UpdateModel> : IRepository<Entity, IndexItem, DetailsResult, CreateModel, UpdateModel>
            where Entity : class, new()
            where IndexItem : new()
            where DetailsResult : AbstractCommunicateModel, new()
            where CreateModel : AbstractCommunicateModel
            where UpdateModel : AbstractCommunicateModel
    {
        public IndexModel<IndexItem> Index()
        {
            var result = ProtocolService.EncryptionPost<VoidModel, IndexModel<IndexItem>>
            (
                new VoidModel { },
                Config.ServantDomain + "/"  + RemoteControllerName + "/Index"
            );

            return Tools.ExceptionConvert(result);
        }

        public VoidResult Create(CreateModel model)
        {
            var result = ProtocolService.EncryptionPost<CreateModel, VoidResult>
            (
                model,
                Config.ServantDomain + "/" + RemoteControllerName + "/Create"
            );

            return Tools.ExceptionConvert(result);
        }

        public DetailsResult Details(int ID)
        {
            var result = ProtocolService.EncryptionPost<IntModel, DetailsResult>
            (
                new IntModel { Value = ID },
                Config.ServantDomain + "/" + RemoteControllerName + "/Details"
            );

            return Tools.ExceptionConvert(result);
        }

        public VoidResult Update(UpdateModel model)
        {
            var result = ProtocolService.EncryptionPost<UpdateModel, VoidResult>
            (
                model,
                Config.ServantDomain + "/" + RemoteControllerName + "/Update"
            );

            return Tools.ExceptionConvert(result);
        }

        public VoidResult Delete(int ID)
        {
            var result = ProtocolService.EncryptionPost<IntModel, VoidResult>
            (
                 new IntModel { Value = ID },
                Config.ServantDomain + "/" + RemoteControllerName + "/Delete"
            );

            return Tools.ExceptionConvert(result);
        }

        protected RemoteGenericRepository(string RemoteControllerName)
        {
            this.RemoteControllerName = RemoteControllerName;
        }
        private string RemoteControllerName;
    }
}
