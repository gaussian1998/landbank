﻿using Library.Servant.Repository.Models;
using Library.Servant.Servant.Common;

namespace Library.Servant.Repository
{
    public interface IRepository<Entity, IndexItem, DetailsResult, CreateModel, UpdateModel>
    {
        IndexModel<IndexItem> Index();
        DetailsResult Details(int ID);
        VoidResult Create(CreateModel model);
        VoidResult Update(UpdateModel model);
        VoidResult Delete(int ID);
    }
}
