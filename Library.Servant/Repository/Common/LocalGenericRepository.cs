﻿using System;
using System.Linq.Expressions;
using LandBankEntity.ActiveRecords;
using Library.Servant.Servant.Common;
using Library.Servant.Repository.Models;

/*
 免責宣言: Sean (gaussian1998)

 以下作法會加速程式撰寫,但不利於維護
 除非專案很趕很趕,否則以產品角度不建議使用
 它要有用,必須配合一個稱為黑白洞的旁門走道範式
 這個作法可以讓當事人很快的可以建構後台程式,且對當事人是邏輯清晰的
 但由於教科書上根本沒人這麼教,所以會對新加入成員造成困擾
 但是既然您已經決定使用,代表專案很趕
 我想您也沒那麼在意後面的人到底好不好接手了是吧
 */
namespace Library.Servant.Repository
{
    public abstract class LocalGenericRepository<Entity, IndexItem, DetailsResult, CreateModel, UpdateModel> : IRepository<Entity, IndexItem, DetailsResult, CreateModel, UpdateModel>
            where Entity : class, new()
            where IndexItem : new()
            where DetailsResult : AbstractCommunicateModel, new()
            where CreateModel : AbstractCommunicateModel
            where UpdateModel : AbstractCommunicateModel
    {
        public IndexModel<IndexItem> Index()
        {
            return new IndexModel<IndexItem>
            {
                Items = GenericRecord<Entity>.FindAll<IndexItem>()
            };
        }

        public DetailsResult Details(int ID)
        {
            return GenericRecord<Entity>.First<DetailsResult>(Predicate(ID));
        }

        public VoidResult Create(CreateModel model)
        {
            GenericRecord<Entity>.Create(model);

            return new VoidResult();
        }

        public VoidResult Update(UpdateModel model)
        {
            GenericRecord<Entity>.UpdateFirst(Predicate(model), model);

            return new VoidResult();
        }

        public VoidResult Delete(int ID)
        {
            GenericRecord<Entity>.Delete(Predicate(ID));

            return new VoidResult();
        }

        protected static Expression<Func<Entity, bool>> Expression(Expression<Func<Entity, bool>> express)
        {
            return express;
        }

        public abstract Expression<Func<Entity, bool>> Predicate(int ID);
        public abstract Expression<Func<Entity, bool>> Predicate(UpdateModel model);
    }

}
