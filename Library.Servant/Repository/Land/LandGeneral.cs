﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.Land.LandModels;
using Library.Entity.Edmx;

namespace Library.Servant.Repository.Land
{
    public class LandGeneral
    {
        private static string CodeClass = "LandForm";
        /// <summary>
        /// 取得表單編號
        /// </summary>
        /// <returns></returns>
        public static string GetFormCode(string Type)
        {
            string result = "";
            using (var db = new AS_LandBankEntities())
            {
                if (db.AS_Code_Table.Any(o => o.Class == CodeClass && o.Parameter1 == Type))
                {
                    result = db.AS_Code_Table.First(o => o.Class == CodeClass  && o.Parameter1 == Type).Code_ID;
                }
            }
            return result;
        }

        /// <summary>
        /// 取得controllername
        /// </summary>
        /// <returns></returns>
        public static string GetController(string FormCode)
        {
            string result = "";
            using (var db = new AS_LandBankEntities())
            {
                if (db.AS_Code_Table.Any(o => o.Class == CodeClass && o.Code_ID == FormCode))
                {
                    result = db.AS_Code_Table.First(o => o.Class == CodeClass && o.Code_ID == FormCode).Parameter1;
                }
            }
            return result;
        }

        #region== Entity To Model ==
        public static LandHeaderModel ToLandHeaderModel(AS_TRX_Headers entity, int assetCount = 0)
        {
            return new LandHeaderModel()
            {
                ID = entity.ID,
                Form_Number = entity.Form_Number,
                Transaction_Type = entity.Transaction_Type,
                Transaction_Status = entity.Transaction_Status,
                Book_Type_Code = entity.Book_Type_Code,
                Transaction_Datetime_Entered = entity.Transaction_Datetime_Entered,
                Accounting_Datetime = entity.Accounting_Datetime,
                Office_Branch = entity.Office_Branch,
                Description = entity.Description,
                Auto_Post = entity.Auto_Post,
                Amotized_Adjustment_Flag = entity.Amotized_Adjustment_Flag,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Created_By_Name = entity.Created_By_Name,
                Last_UpDatetimed_Time = entity.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = entity.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name=entity.Last_UpDatetimed_By_Name,
                AssetsCount = assetCount
            };
        }

        public static AssetLandModel ToAssetLandModel(AS_Assets_Land entity)
        {
            return new AssetLandModel()
            {
                ID = entity.ID,
                Asset_Number = entity.Asset_Number,
                Current_units = entity.Current_units,
                Datetime_Placed_In_Service = entity.Datetime_Placed_In_Service,
                Old_Asset_Number = entity.Old_Asset_Number,
                Asset_Category_code = entity.Asset_Category_code,
                Location_Disp = entity.Location_Disp,
                Description = entity.Description,
                Urban_Renewal = entity.Urban_Renewal,
                City_Code = entity.City_Code,
                District_Code = entity.District_Code,
                Section_Code = entity.Section_Code,
                Sub_Section_Name = entity.Sub_Section_Name,
                Parent_Land_Number = entity.Parent_Land_Number,
                Filial_Land_Number = entity.Filial_Land_Number,
                Authorization_Number = entity.Authorization_Number,
                Authorized_name = entity.Authorized_name,
                Area_Size = entity.Area_Size,
                Authorized_Scope_Molecule = entity.Authorized_Scope_Molecule,
                Authorized_Scope_Denomminx = entity.Authorized_Scope_Denomminx,
                Authorized_Area = entity.Authorized_Area,
                Used_Type = entity.Used_Type,
                Used_Status = entity.Used_Status,
                Obtained_Method = entity.Obtained_Method,
                Used_Partition = entity.Used_Partition,
                Is_Marginal_Land = entity.Is_Marginal_Land,
                Own_Area = entity.Own_Area,
                NONOwn_Area = entity.NONOwn_Area,
                Original_Cost = entity.Original_Cost,
                Deprn_Amount = entity.Deprn_Amount,
                Current_Cost = entity.Current_Cost,
                Reval_Adjustment_Amount = entity.Reval_Adjustment_Amount,
                Reval_Land_VAT = entity.Reval_Land_VAT,
                Reval_Reserve = entity.Reval_Reserve,
                Business_Area_Size = entity.Business_Area_Size,
                Business_Book_Amount = entity.Business_Book_Amount,
                Business_Reval_Reserve = entity.Business_Reval_Reserve,
                NONBusiness_Area_Size = entity.NONBusiness_Area_Size,
                NONBusiness_Book_Amount = entity.NONBusiness_Book_Amount,
                NONBusiness_Reval_Reserve = entity.NONBusiness_Reval_Reserve,
                Year_Number = entity.Year_Number,
                Announce_Amount = entity.Announce_Amount,
                Announce_Price = entity.Announce_Price,
                Tax_Type = entity.Tax_Type,
                Reduction_reason = entity.Reduction_reason,
                Transfer_Price = entity.Transfer_Price,
                Reduction_Area = entity.Reduction_Area,
                Declared_Price = entity.Declared_Price,
                Dutiable_Amount = entity.Dutiable_Amount,
                Remark1 = entity.Remark1,
                Remark2 = entity.Remark2,
                Retire_Cost = entity.Retire_Cost,
                Sell_Amount = entity.Sell_Amount,
                Sell_Cost = entity.Sell_Cost,
                Delete_Reason = entity.Delete_Reason,
                Land_Item = entity.Land_Item,
                CROP = entity.CROP,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Last_Updated_Time = entity.Last_UpDatetimed_Time,
                Last_Updated_By = entity.Last_UpDatetimed_By.HasValue? entity.Last_UpDatetimed_By.Value.ToString():""
            };
        }

        public static AssetLandMPModel ToAssetLandMPModel(AS_Assets_Land_MP_Record entity)
        {
            return new AssetLandMPModel()
            {
                ID = entity.ID,
                Assets_Land_MP_ID = entity.Assets_Land_MP_ID.ToString(),
                TRX_Header_ID = entity.TRX_Header_ID,
                Asset_Number = entity.Asset_Number,
                Parent_Asset_Number = entity.Parent_Asset_Number,
                Asset_Category_Code = entity.Asset_Category_code,
                Old_Asset_Number = entity.Old_Asset_Number,
                Date_Placed_In_Service = entity.Datetime_Placed_In_Service,
                Current_units = entity.Current_units.HasValue? entity.Current_units.Value: 0,
                Deprn_Method_Code = entity.Deprn_Method_Code,
                Life_Years = entity.Life_Years.HasValue ? entity.Life_Years.Value : 0,
                Life_Months = entity.Life_Months.HasValue ? entity.Life_Months.Value : 0,
                Location_Disp = entity.Location_Disp,
                PO_Number = entity.PO_Number,
                PO_Destination = entity.PO_Destination,
                Model_Number = entity.Model_Number,
                Transaction_Date = entity.Transaction_Date,
                Assets_Unit = entity.Assets_Unit,
                Accessory_Equipment = entity.Accessory_Equipment,
                Assigned_NUM = entity.Assigned_NUM,
                Asset_Category_Num = entity.Asset_Category_NUM,
                Description = entity.Description,
                Asset_Structure = entity.Asset_Structure,
                Current_Cost = entity.Current_Cost.HasValue ? entity.Current_Cost.Value : 0,
                Deprn_Reserve = entity.Deprn_Reserve.HasValue ? entity.Deprn_Reserve.Value : 0,
                Salvage_Value = entity.Salvage_Value.HasValue ? entity.Salvage_Value.Value : 0,
                Remark = entity.Remark,
                Create_Time = entity.Create_Time.HasValue ? entity.Create_Time.Value : DateTime.Now,
                Created_By = entity.Created_By.HasValue?entity.Created_By.Value.ToString():"" ,
                Created_By_Name = entity.Created_By_Name,
                Last_Updated_Time = entity.Last_UpDatetimed_Time,
                Last_Updated_By = entity.Last_UpDatetimed_By.HasValue ? entity.Last_UpDatetimed_By.Value.ToString() : "",
                Last_Updated_By_Name = entity.Last_UpDatetimed_By_Name                 
            };
        }
        public static AssetLandMPRetireModel ToAssetLandMPRetireModel(AS_Assets_Land_MP_Retire entity)
        {
            return new AssetLandMPRetireModel()
            {
                ID = entity.ID,
                TRX_Header_ID = entity.TRX_Header_ID,
                Assets_Land_MP_ID = entity.Assets_Land_MP_ID.ToString(),
                Serial_Number = "",
                Asset_Number = entity.Asset_Number,
                Parent_Asset_Number = entity.Parent_Asset_Number,
                Description = entity.Description,
                Reval_Land_VAT = entity.Reval_Land_VAT.HasValue ? entity.Reval_Land_VAT.Value : 0,
                Reval_Reserve = entity.Reval_Reserve.HasValue ? entity.Reval_Reserve.Value : 0,
                Retire_Cost = entity.Retire_Cost.HasValue ? entity.Retire_Cost.Value : 0,
                Sell_Amount = entity.Sell_Amount.HasValue ? entity.Sell_Amount.Value : 0,
                Sell_Cost = entity.Sell_Cost.HasValue ? entity.Sell_Cost.Value : 0,
                New_Cost = entity.New_Cost.HasValue ? entity.New_Cost.Value : 0,
                Old_Cost = entity.Old_Cost.HasValue ? entity.Old_Cost.Value : 0,
                Reason_Code = entity.Reason_Code,
                Receipt_Type = entity.Receipt_Type.HasValue ? entity.Receipt_Type.Value : 0,
                Reval_Adjustment_Amount = entity.Reval_Adjustment_Amount.HasValue ? entity.Reval_Adjustment_Amount.Value : 0,
                SerialNo = "",
                Account_Type = entity.Account_Type.HasValue ? entity.Account_Type.Value : 0,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Created_By_Name = entity.Created_By_Name,
                Last_UpDatetimed_Time = entity.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = entity.Last_UpDatetimed_By.HasValue ? entity.Last_UpDatetimed_By.Value.ToString() : "",
                Last_UpDatetimed_By_Name = entity.Last_UpDatetimed_By_Name
            };
        }
        public static AssetLandRetireModel ToAssetRetireLandModel(AS_Assets_Land entity)
        {
            return new AssetLandRetireModel()
            {
                ID = entity.ID,
                TRX_Header_ID = "",
                Asset_Land_ID = "",
                Serial_Number = "",
                Asset_Number = entity.Asset_Number,
                Parent_Asset_Number = entity.Parent_Land_Number,
                City_Code = entity.City_Code,
                District_Code = entity.District_Code,
                Section_Code = entity.Section_Code,
                Sub_Section_Name = entity.Sub_Section_Name,
                Description = entity.Description,
                Parent_Land_Number = entity.Parent_Land_Number,
                Filial_Land_Number = entity.Filial_Land_Number,
                Reval_Adjustment_Amount = entity.Reval_Adjustment_Amount,
                Reval_Land_VAT = entity.Reval_Land_VAT,
                Reval_Reserve = entity.Reval_Reserve,
                Retire_Cost = entity.Retire_Cost,
                Sell_Amount = entity.Sell_Amount,
                Sell_Cost = entity.Sell_Cost,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Created_By_Name = entity.Created_By_Name,
                Last_UpDatetimed_Time = entity.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = entity.Last_UpDatetimed_By.HasValue?entity.Last_UpDatetimed_By.Value.ToString():"",
                Last_UpDatetimed_By_Name = entity.Last_UpDatetimed_By_Name
            };
        }
        public static AssetLandModel ToAssetLandModel(AS_Assets_Land_Split entity)
        {
            return new AssetLandModel()
            {
                TRXHeaderID = entity.TRX_Header_ID,
                Book_Type = (!string.IsNullOrEmpty(entity.Book_Type_Code)) ? entity.Book_Type_Code : "",
                City_Name = (!string.IsNullOrEmpty(entity.City_Name)) ? entity.City_Name : "",
                District_Name = (!string.IsNullOrEmpty(entity.District_Name)) ? entity.District_Name : "",
                Section_Name = (!string.IsNullOrEmpty(entity.Section_Name)) ? entity.Section_Name : "",
                Office_Branch = (!string.IsNullOrEmpty(entity.Office_Branch)) ? entity.Office_Branch : "",
                CROP = "",
                Delete_Reason = "",
                SerialNo = entity.Serial_Number,
                AddCost = 0,
                Sell_Cost = 0,
                Expense_Account = "",
                Land_Item = "",
                Retire_Cost = 0,
                LessCost = 0,
                Sell_Amount = 0,
                Old_Asset_Category_code = "",
                Transfer_Price = 0,
                Urban_Renewal = "",
                ID = entity.ID,
                Asset_Number = (!string.IsNullOrEmpty(entity.Asset_Number)) ? entity.Asset_Number : "",
                Current_units = entity.Current_units,
                Datetime_Placed_In_Service = entity.Datetime_Placed_In_Service,
                Old_Asset_Number = (!string.IsNullOrEmpty(entity.Old_Asset_Number)) ? entity.Old_Asset_Number : "",
                Asset_Category_code = (!string.IsNullOrEmpty(entity.Asset_Category_code)) ? entity.Asset_Category_code : "",
                Location_Disp = (!string.IsNullOrEmpty(entity.Location_Disp)) ? entity.Location_Disp : "",
                Description = (!string.IsNullOrEmpty(entity.Description)) ? entity.Description : "",
                City_Code = (!string.IsNullOrEmpty(entity.City_Code)) ? entity.City_Code : "",
                District_Code = (!string.IsNullOrEmpty(entity.District_Code)) ? entity.District_Code : "",
                Section_Code = (!string.IsNullOrEmpty(entity.Section_Code)) ? entity.Section_Code : "",
                Sub_Section_Name = (!string.IsNullOrEmpty(entity.Sub_Section_Name)) ? entity.Sub_Section_Name : "",
                Parent_Land_Number = (!string.IsNullOrEmpty(entity.Parent_Land_Number)) ? entity.Parent_Land_Number : "",
                Filial_Land_Number = (!string.IsNullOrEmpty(entity.Filial_Land_Number)) ? entity.Filial_Land_Number : "",
                Authorization_Number = (!string.IsNullOrEmpty(entity.Authorization_Number)) ? entity.Authorization_Number : "",
                Authorized_name = (!string.IsNullOrEmpty(entity.Authorized_name)) ? entity.Authorized_name : "",
                Area_Size = entity.Area_Size,
                Authorized_Scope_Molecule = entity.Authorized_Scope_Molecule,
                Authorized_Scope_Denomminx = entity.Authorized_Scope_Denomminx,
                Authorized_Area = entity.Authorized_Area,
                Used_Type = (!string.IsNullOrEmpty(entity.Used_Type)) ? entity.Used_Type : "",
                Used_Status = (!string.IsNullOrEmpty(entity.Used_Status)) ? entity.Used_Status : "",
                Obtained_Method = (!string.IsNullOrEmpty(entity.Obtained_Method)) ? entity.Obtained_Method : "",
                Used_Partition = (!string.IsNullOrEmpty(entity.Used_Partition)) ? entity.Used_Partition : "",
                Is_Marginal_Land = (!string.IsNullOrEmpty(entity.Is_Marginal_Land)) ? entity.Is_Marginal_Land : "",
                Own_Area = entity.Own_Area,
                NONOwn_Area = entity.NONOwn_Area,
                Original_Cost = entity.Original_Cost,
                Deprn_Amount = entity.Deprn_Amount,
                Current_Cost = entity.Current_Cost,
                Reval_Adjustment_Amount = entity.Reval_Adjustment_Amount,
                Reval_Land_VAT = entity.Reval_Land_VAT,
                Reval_Reserve = entity.Reval_Reserve,
                Business_Area_Size = entity.Business_Area_Size,
                Business_Book_Amount = entity.Business_Book_Amount,
                Business_Reval_Reserve = entity.Business_Reval_Reserve,
                NONBusiness_Area_Size = entity.NONBusiness_Area_Size,
                NONBusiness_Book_Amount = entity.NONBusiness_Book_Amount,
                NONBusiness_Reval_Reserve = entity.NONBusiness_Reval_Reserve,
                Year_Number = entity.Year_Number,
                Announce_Amount = entity.Announce_Amount,
                Announce_Price = entity.Announce_Price,
                Tax_Type = (!string.IsNullOrEmpty(entity.Tax_Type)) ? entity.Tax_Type : "",
                Reduction_reason = (!string.IsNullOrEmpty(entity.Reduction_reason)) ? entity.Reduction_reason : "",
                Reduction_Area = entity.Reduction_Area,
                Declared_Price = entity.Declared_Price,
                Dutiable_Amount = entity.Dutiable_Amount,
                Remark1 = (!string.IsNullOrEmpty(entity.Remark1)) ? entity.Remark1 : "",
                Remark2 = (!string.IsNullOrEmpty(entity.Remark2)) ? entity.Remark2 : "",
                Create_Time = entity.Create_Time,
                Created_By = (!string.IsNullOrEmpty(entity.Created_By)) ? entity.Created_By : "",
                Created_By_Name = (!string.IsNullOrEmpty(entity.Created_By_Name)) ? entity.Created_By_Name : "",
                Last_Updated_Time = entity.Last_UpDatetimed_Time,
                Last_Updated_By = entity.Last_UpDatetimed_By.HasValue ? entity.Last_UpDatetimed_By.Value.ToString(): "",
                Last_Updated_By_Name = (!string.IsNullOrEmpty(entity.Last_UpDatetimed_By_Name)) ? entity.Last_UpDatetimed_By_Name : ""
            };
        }

        public static AssetLandModel ToAssetLandModel(AS_Assets_Land_ML entity)
        {
            return new AssetLandModel()
            {
                //TRXHeaderID = entity.TRX_Header_ID,
                //ID = entity.ID,
                //Asset_Number = entity.Asset_Number,
                //Current_units = entity.Current_units,
                //Datetime_Placed_In_Service = entity.Datetime_Placed_In_Service,
                //Old_Asset_Number = entity.Old_Asset_Number,
                //Asset_Category_code = entity.Asset_Category_code,
                //Location_Disp = entity.Location_Disp,
                //Description = entity.Description,
                //City_Code = entity.City_Code,
                //District_Code = entity.District_Code,
                //Section_Code = entity.Section_Code,
                //Sub_Section_Name = entity.Sub_Section_Name,
                //Parent_Land_Number = entity.Parent_Land_Number,
                //Filial_Land_Number = entity.Filial_Land_Number,
                //Authorization_Number = entity.Authorization_Number,
                //Authorized_name = entity.Authorized_name,
                //Area_Size = entity.Area_Size,
                //Authorized_Scope_Molecule = entity.Authorized_Scope_Molecule,
                //Authorized_Scope_Denomminx = entity.Authorized_Scope_Denomminx,
                //Authorized_Area = entity.Authorized_Area,
                //Used_Type = entity.Used_Type,
                //Used_Status = entity.Used_Status,
                //Obtained_Method = entity.Obtained_Method,
                //Used_Partition = entity.Used_Partition,
                //Is_Marginal_Land = entity.Is_Marginal_Land,
                //Own_Area = entity.Own_Area,
                //NONOwn_Area = entity.NONOwn_Area,
                //Original_Cost = entity.Original_Cost,
                //Deprn_Amount = entity.Deprn_Amount,
                //Current_Cost = entity.Current_Cost,
                //Reval_Adjustment_Amount = entity.Reval_Adjustment_Amount,
                //Reval_Land_VAT = entity.Reval_Land_VAT,
                //Reval_Reserve = entity.Reval_Reserve,
                //Business_Area_Size = entity.Business_Area_Size,
                //Business_Book_Amount = entity.Business_Book_Amount,
                //Business_Reval_Reserve = entity.Business_Reval_Reserve,
                //NONBusiness_Area_Size = entity.NONBusiness_Area_Size,
                //NONBusiness_Book_Amount = entity.NONBusiness_Book_Amount,
                //NONBusiness_Reval_Reserve = entity.NONBusiness_Reval_Reserve,
                //Year_Number = entity.Year_Number,
                //Announce_Amount = entity.Announce_Amount,
                //Announce_Price = entity.Announce_Price,
                //Tax_Type = entity.Tax_Type,
                //Reduction_reason = entity.Reduction_reason,
                //Reduction_Area = entity.Reduction_Area,
                //Declared_Price = entity.Declared_Price,
                //Dutiable_Amount = entity.Dutiable_Amount,
                //Remark1 = entity.Remark1,
                //Remark2 = entity.Remark2,
                //Create_Time = entity.Create_Time,
                //Created_By = entity.Created_By,
                //Last_Updated_Time = entity.Last_UpDatetimed_Time,
                //Last_Updated_By = entity.Last_UpDatetimed_By.HasValue ? "" : entity.Last_UpDatetimed_By.Value.ToString()
                TRXHeaderID = entity.TRX_Header_ID,
                ID = entity.ID,
                Asset_Number = entity.Asset_Number,
                Current_units = entity.Current_units,
                Datetime_Placed_In_Service = entity.Datetime_Placed_In_Service,
                Old_Asset_Number = (!string.IsNullOrEmpty(entity.Old_Asset_Number)) ? entity.Old_Asset_Number : "",
                Asset_Category_code = (!string.IsNullOrEmpty(entity.Asset_Category_code)) ? entity.Asset_Category_code : "",
                Location_Disp = (!string.IsNullOrEmpty(entity.Location_Disp)) ? entity.Location_Disp : "",
                Description = (!string.IsNullOrEmpty(entity.Description)) ? entity.Description : "",
                Urban_Renewal = "",
                City_Code = (!string.IsNullOrEmpty(entity.City_Code)) ? entity.City_Code : "",
                City_Name = (!string.IsNullOrEmpty(entity.City_Name)) ? entity.City_Name : "",
                District_Code = (!string.IsNullOrEmpty(entity.District_Code)) ? entity.District_Code : "",
                District_Name = (!string.IsNullOrEmpty(entity.District_Name)) ? entity.District_Name : "",
                Section_Code = (!string.IsNullOrEmpty(entity.Section_Code)) ? entity.Section_Code : "",
                Section_Name = (!string.IsNullOrEmpty(entity.Section_Name)) ? entity.Section_Name : "",
                Sub_Section_Name = (!string.IsNullOrEmpty(entity.Sub_Section_Name)) ? entity.Sub_Section_Name : "",
                Parent_Land_Number = (!string.IsNullOrEmpty(entity.Parent_Land_Number)) ? entity.Parent_Land_Number : "",
                Filial_Land_Number = (!string.IsNullOrEmpty(entity.Filial_Land_Number)) ? entity.Filial_Land_Number : "",
                Authorization_Number = (!string.IsNullOrEmpty(entity.Authorization_Number)) ? entity.Authorization_Number : "",
                Authorized_name = (!string.IsNullOrEmpty(entity.Authorized_name)) ? entity.Authorized_name : "",
                Area_Size = entity.Area_Size,
                Authorized_Scope_Molecule = entity.Authorized_Scope_Molecule,
                Authorized_Scope_Denomminx = entity.Authorized_Scope_Denomminx,
                Authorized_Area = entity.Authorized_Area,
                Used_Type = (!string.IsNullOrEmpty(entity.Used_Type)) ? entity.Used_Type : "",
                Used_Status = (!string.IsNullOrEmpty(entity.Used_Status)) ? entity.Used_Status : "",
                Obtained_Method = (!string.IsNullOrEmpty(entity.Obtained_Method)) ? entity.Obtained_Method : "",
                Used_Partition = (!string.IsNullOrEmpty(entity.Used_Partition)) ? entity.Used_Partition : "",
                Is_Marginal_Land = (!string.IsNullOrEmpty(entity.Is_Marginal_Land)) ? entity.Is_Marginal_Land : "",
                Own_Area = entity.Own_Area,
                NONOwn_Area = entity.NONOwn_Area,
                Original_Cost = entity.Original_Cost,
                Deprn_Amount = entity.Deprn_Amount,
                Current_Cost = entity.Current_Cost,
                Reval_Adjustment_Amount = entity.Reval_Adjustment_Amount,
                Reval_Land_VAT = entity.Reval_Land_VAT,
                Reval_Reserve = entity.Reval_Reserve,
                Business_Area_Size = entity.Business_Area_Size,
                Business_Book_Amount = entity.Business_Book_Amount,
                Business_Reval_Reserve = entity.Business_Reval_Reserve,
                NONBusiness_Area_Size = entity.NONBusiness_Area_Size,
                NONBusiness_Book_Amount = entity.NONBusiness_Book_Amount,
                NONBusiness_Reval_Reserve = entity.NONBusiness_Reval_Reserve,
                Year_Number = (int)entity.Year_Number,
                Announce_Amount = entity.Announce_Amount,
                Announce_Price = entity.Announce_Price,
                Tax_Type = (!string.IsNullOrEmpty(entity.Tax_Type)) ? entity.Tax_Type : "",
                Reduction_reason = (!string.IsNullOrEmpty(entity.Reduction_reason)) ? entity.Reduction_reason : "",
                Transfer_Price = 0,
                Reduction_Area = entity.Reduction_Area,
                Declared_Price = entity.Declared_Price,
                Dutiable_Amount = entity.Dutiable_Amount,
                Remark1 = (!string.IsNullOrEmpty(entity.Remark1)) ? entity.Remark1 : "",
                Remark2 = (!string.IsNullOrEmpty(entity.Remark2)) ? entity.Remark2 : "",
                Land_Item = "",
                CROP = "",
                Create_Time = entity.Create_Time,
                Created_By = (!string.IsNullOrEmpty(entity.Created_By)) ? entity.Created_By : "",
                Created_By_Name = (!string.IsNullOrEmpty(entity.Created_By_Name)) ? entity.Created_By_Name : "",
                Last_Updated_Time = entity.Last_UpDatetimed_Time.HasValue ? entity.Last_UpDatetimed_Time.Value : DateTime.Now,
                Last_Updated_By = entity.Last_UpDatetimed_By.HasValue ? entity.Last_UpDatetimed_By.Value.ToString() : "",
                Last_Updated_By_Name = (!string.IsNullOrEmpty(entity.Last_UpDatetimed_By_Name)) ? entity.Last_UpDatetimed_By_Name : "",
                AddCost = 0,
                Book_Type = "",
                Delete_Reason = "",
                Expense_Account = "",
                LessCost = 0,
                Office_Branch = "",
                Old_Asset_Category_code = "",
                Retire_Cost = 0,
                Sell_Amount = 0,
                Sell_Cost = 0,
                SerialNo = ""
            };
        }

        public static AssetLandModel ToAssetLandModel(AS_Assets_Land_Record entity)
        {
            AssetLandModel Result = null;
            if (entity != null)
            {
                Result = new AssetLandModel()
                {
                    TRXHeaderID = entity.TRX_Header_ID,
                    ID = entity.ID,
                    Asset_Number = entity.Asset_Number,
                    Current_units = entity.Current_units,
                    Asset_Land_ID = entity.Asset_Land_ID,
                    Datetime_Placed_In_Service = entity.Datetime_Placed_In_Service,
                    Old_Asset_Number = (!string.IsNullOrEmpty(entity.Old_Asset_Number)) ? entity.Old_Asset_Number : "",
                    Asset_Category_code = (!string.IsNullOrEmpty(entity.Asset_Category_code)) ? entity.Asset_Category_code : "",
                    Location_Disp = (!string.IsNullOrEmpty(entity.Location_Disp)) ? entity.Location_Disp : "",
                    Description = (!string.IsNullOrEmpty(entity.Description)) ? entity.Description : "",
                    Urban_Renewal = (!string.IsNullOrEmpty(entity.Urban_Renewal)) ? entity.Urban_Renewal : "",
                    City_Code = (!string.IsNullOrEmpty(entity.City_Code)) ? entity.City_Code : "",
                    City_Name = (!string.IsNullOrEmpty(entity.City_Name)) ? entity.City_Name : "",
                    District_Code = (!string.IsNullOrEmpty(entity.District_Code)) ? entity.District_Code : "",
                    District_Name = (!string.IsNullOrEmpty(entity.District_Name)) ? entity.District_Name : "",
                    Section_Code = (!string.IsNullOrEmpty(entity.Section_Code)) ? entity.Section_Code : "",
                    Section_Name = (!string.IsNullOrEmpty(entity.Section_Name)) ? entity.Section_Name : "",
                    Sub_Section_Name = (!string.IsNullOrEmpty(entity.Sub_Section_Name)) ? entity.Sub_Section_Name : "",
                    Parent_Land_Number = (!string.IsNullOrEmpty(entity.Parent_Land_Number)) ? entity.Parent_Land_Number : "",
                    Filial_Land_Number = (!string.IsNullOrEmpty(entity.Filial_Land_Number)) ? entity.Filial_Land_Number : "",
                    Authorization_Number = (!string.IsNullOrEmpty(entity.Authorization_Number)) ? entity.Authorization_Number : "",
                    Authorized_name = (!string.IsNullOrEmpty(entity.Authorized_name)) ? entity.Authorized_name : "",
                    Area_Size = entity.Area_Size,
                    Authorized_Scope_Molecule = entity.Authorized_Scope_Molecule,
                    Authorized_Scope_Denomminx = entity.Authorized_Scope_Denomminx,
                    Authorized_Area = entity.Authorized_Area,
                    Used_Type = (!string.IsNullOrEmpty(entity.Used_Type)) ? entity.Used_Type : "",
                    Used_Status = (!string.IsNullOrEmpty(entity.Used_Status)) ? entity.Used_Status : "",
                    Obtained_Method = (!string.IsNullOrEmpty(entity.Obtained_Method)) ? entity.Obtained_Method : "",
                    Used_Partition = (!string.IsNullOrEmpty(entity.Used_Partition)) ? entity.Used_Partition : "",
                    Is_Marginal_Land = (!string.IsNullOrEmpty(entity.Is_Marginal_Land)) ? entity.Is_Marginal_Land : "",
                    Own_Area = entity.Own_Area,
                    NONOwn_Area = entity.NONOwn_Area,
                    Original_Cost = entity.Original_Cost,
                    Deprn_Amount = entity.Deprn_Amount,
                    Current_Cost = entity.Current_Cost,
                    Reval_Adjustment_Amount = entity.Reval_Adjustment_Amount,
                    Reval_Land_VAT = entity.Reval_Land_VAT,
                    Reval_Reserve = entity.Reval_Reserve,
                    Business_Area_Size = entity.Business_Area_Size,
                    Business_Book_Amount = entity.Business_Book_Amount,
                    Business_Reval_Reserve = entity.Business_Reval_Reserve,
                    NONBusiness_Area_Size = entity.NONBusiness_Area_Size,
                    NONBusiness_Book_Amount = entity.NONBusiness_Book_Amount,
                    NONBusiness_Reval_Reserve = entity.NONBusiness_Reval_Reserve,
                    Year_Number = (int)entity.Year_Number,
                    Announce_Amount = entity.Announce_Amount,
                    Announce_Price = entity.Announce_Price,
                    Tax_Type = (!string.IsNullOrEmpty(entity.Tax_Type)) ? entity.Tax_Type : "",
                    Reduction_reason = (!string.IsNullOrEmpty(entity.Reduction_reason)) ? entity.Reduction_reason : "",
                    Transfer_Price = entity.Transfer_Price,
                    Reduction_Area = entity.Reduction_Area,
                    Declared_Price = entity.Declared_Price,
                    Dutiable_Amount = entity.Dutiable_Amount,
                    Remark1 = (!string.IsNullOrEmpty(entity.Remark1)) ? entity.Remark1 : "",
                    Remark2 = (!string.IsNullOrEmpty(entity.Remark2)) ? entity.Remark2 : "",
                    Land_Item = (!string.IsNullOrEmpty(entity.Land_Item)) ? entity.Land_Item : "",
                    CROP = (!string.IsNullOrEmpty(entity.CROP)) ? entity.CROP : "",
                    Create_Time = entity.Create_Time,
                    Created_By = (!string.IsNullOrEmpty(entity.Created_By)) ? entity.Created_By : "",
                    Created_By_Name = (!string.IsNullOrEmpty(entity.Created_By_Name)) ? entity.Created_By_Name : "",
                    Last_Updated_Time = entity.Last_UpDatetimed_Time.HasValue? entity.Last_UpDatetimed_Time.Value: DateTime.Now,
                    Last_Updated_By = entity.Last_UpDatetimed_By.HasValue ? entity.Last_UpDatetimed_By.Value.ToString(): "",
                    Last_Updated_By_Name = (!string.IsNullOrEmpty(entity.Last_UpDatetimed_By_Name)) ? entity.Last_UpDatetimed_By_Name : "",
                    AddCost = 0,
                    Book_Type = "",
                    Delete_Reason = "",
                    Expense_Account = "",
                    LessCost = 0,
                    Office_Branch = "",
                    Old_Asset_Category_code = "",
                    Retire_Cost = 0,
                    Sell_Amount = 0,
                     Sell_Cost =0,
                      SerialNo= ""
                };
            }

            return Result;
        }
        public static AssetLandRetireModel ToAssetRetireLand(AS_Assets_Land_Retire entity)
        {
            AssetLandRetireModel Result = null;
            if (entity != null)
            {
                Result = new AssetLandRetireModel()
                {
                    TRX_Header_ID = entity.TRX_Header_ID,
                    ID = entity.ID,
                    Asset_Land_ID = entity.Assets_Land_ID,
                    Serial_Number = entity.Serial_Number,
                    Asset_Number = entity.Asset_Number,
                    Parent_Asset_Number = entity.Parent_Asset_Number,
                    City_Code = (!string.IsNullOrEmpty(entity.City_Code)) ? entity.City_Code : "",
                    City_Name = (!string.IsNullOrEmpty(entity.City_Name)) ? entity.City_Name : "",
                    District_Code = (!string.IsNullOrEmpty(entity.District_Code)) ? entity.District_Code : "",
                    District_Name = (!string.IsNullOrEmpty(entity.District_Name)) ? entity.District_Name : "",
                    Section_Code = (!string.IsNullOrEmpty(entity.Section_Code)) ? entity.Section_Code : "",
                    Section_Name = (!string.IsNullOrEmpty(entity.Section_Name)) ? entity.Section_Name : "",
                    Sub_Section_Name = (!string.IsNullOrEmpty(entity.Sub_Section_Name)) ? entity.Sub_Section_Name : "",
                    Parent_Land_Number = (!string.IsNullOrEmpty(entity.Parent_Land_Number)) ? entity.Parent_Land_Number : "",
                    Filial_Land_Number = (!string.IsNullOrEmpty(entity.Filial_Land_Number)) ? entity.Filial_Land_Number : "",
                    Build_Address = (!string.IsNullOrEmpty(entity.Build_Address)) ? entity.Build_Address : "",
                    Build_Number = (!string.IsNullOrEmpty(entity.Build_Number)) ? entity.Build_Number : "",
                    Old_Cost = entity.Old_Cost.HasValue?entity.Old_Cost.Value:0,
                    Retire_Cost = entity.Retire_Cost.HasValue?entity.Retire_Cost.Value:0,
                    Sell_Amount = entity.Sell_Amount.HasValue?entity.Sell_Amount.Value:0,
                    Sell_Cost = entity.Sell_Cost.HasValue?entity.Sell_Cost.Value:0,
                    New_Cost = entity.New_Cost.HasValue?entity.New_Cost.Value:0,
                    Reval_Adjustment_Amount = entity.Reval_Adjustment_Amount.HasValue?entity.Reval_Adjustment_Amount.Value:0,
                    Reval_Reserve = entity.Reval_Reserve.HasValue?entity.Reval_Reserve.Value:0,
                    Reval_Land_VAT = entity.Reval_Land_VAT.HasValue?entity.Reval_Land_VAT.Value:0,
                    Reason_Code = (!string.IsNullOrEmpty(entity.Reason_Code)) ? entity.Reason_Code : "",
                    Description = (!string.IsNullOrEmpty(entity.Description)) ? entity.Description : "",
                    Account_Type = entity.Account_Type.HasValue ? entity.Account_Type.Value : 0,
                    Receipt_Type = entity.Receipt_Type.HasValue ? entity.Receipt_Type.Value : 0,
                    Retire_Cost_Account = (!string.IsNullOrEmpty(entity.Retire_Cost_Account)) ? entity.Retire_Cost_Account : "",
                    Sell_Amount_Account = (!string.IsNullOrEmpty(entity.Sell_Amount_Account)) ? entity.Sell_Amount_Account : "",
                    Sell_Cost_Account = (!string.IsNullOrEmpty(entity.Sell_Cost_Account)) ? entity.Sell_Cost_Account : "",
                    Reval_Adjustment_Amount_Account = (!string.IsNullOrEmpty(entity.Reval_Adjustment_Amount_Account)) ? entity.Reval_Adjustment_Amount_Account : "",
                    Reval_Reserve_Account = (!string.IsNullOrEmpty(entity.Reval_Reserve_Account)) ? entity.Reval_Reserve_Account : "",
                    Reval_Land_VAT_Account = (!string.IsNullOrEmpty(entity.Reval_Land_VAT_Account)) ? entity.Reval_Land_VAT_Account : "",
                    Create_Time = entity.Create_Time,
                    Created_By = (!string.IsNullOrEmpty(entity.Created_By)) ? entity.Created_By : "",
                    Created_By_Name = (!string.IsNullOrEmpty(entity.Created_By_Name)) ? entity.Created_By_Name : "",
                    Last_UpDatetimed_Time = entity.Last_UpDatetimed_Time.HasValue ? entity.Last_UpDatetimed_Time.Value : DateTime.Now,
                    Last_UpDatetimed_By = entity.Last_UpDatetimed_By.HasValue ? entity.Last_UpDatetimed_By.Value.ToString() : "",
                    Last_UpDatetimed_By_Name = (!string.IsNullOrEmpty(entity.Last_UpDatetimed_By_Name)) ? entity.Last_UpDatetimed_By_Name : ""
                };
            }

            return Result;
        }
        public static AssetLandChangeModel ToAssetChangeLand(AS_Assets_Land_Change entity)
        {
            AssetLandChangeModel Result = null;
            if (entity != null)
            {
                Result = new AssetLandChangeModel()
                {
                    TRX_Header_ID = entity.TRX_Header_ID,
                    ID = entity.ID,
                    AS_Assets_Land_ID = entity.AS_Assets_Land_ID.HasValue? entity.AS_Assets_Land_ID.Value.ToString():"",
                    UPD_File = entity.UPD_File,
                    Field_Name = entity.Field_Name,
                    Modify_Type = entity.Modify_Type,
                    Before_Data = (!string.IsNullOrEmpty(entity.Before_Data)) ? entity.Before_Data : "",
                    After_Data = (!string.IsNullOrEmpty(entity.After_Data)) ? entity.After_Data : "",
                    Create_Time = entity.Create_Time,
                    Created_By = (!string.IsNullOrEmpty(entity.Created_By)) ? entity.Created_By : "",
                    Created_By_Name = (!string.IsNullOrEmpty(entity.Created_By_Name)) ? entity.Created_By_Name : "",
                    Last_UpDatetimed_Time = entity.Last_UpDatetimed_Time.HasValue ? entity.Last_UpDatetimed_Time.Value : DateTime.Now,
                    Last_UpDatetimed_By = entity.Last_UpDatetimed_By.HasValue ? entity.Last_UpDatetimed_By.Value.ToString() : "",
                    Last_UpDatetimed_By_Name = (!string.IsNullOrEmpty(entity.Last_UpDatetimed_By_Name)) ? entity.Last_UpDatetimed_By_Name : ""
                };
            }

            return Result;
        }

        public static AssetLandRecord ToAssetLandRecord(AS_Assets_Land_Record entity)
        {
            return new AssetLandRecord()
            {
                ID = entity.ID,
                TRX_Header_ID = entity.TRX_Header_ID,
                Asset_Number = entity.Asset_Number,
                Current_units = entity.Current_units,
                Datetime_Placed_In_Service = entity.Datetime_Placed_In_Service,
                Old_Asset_Number = entity.Old_Asset_Number,
                Asset_Category_code = entity.Asset_Category_code,
                Location_Disp = entity.Location_Disp,
                Description = entity.Description,
                Urban_Renewal = entity.Urban_Renewal,
                City_Code = entity.City_Code,
                District_Code = entity.District_Code,
                Section_Code = entity.Section_Code,
                Sub_Section_Name = entity.Sub_Section_Name,
                Parent_Land_Number = entity.Parent_Land_Number,
                Filial_Land_Number = entity.Filial_Land_Number,
                Authorization_Number = entity.Authorization_Number,
                Authorized_name = entity.Authorized_name,
                Area_Size = entity.Area_Size,
                Authorized_Scope_Molecule = entity.Authorized_Scope_Molecule,
                Authorized_Scope_Denomminx = entity.Authorized_Scope_Denomminx,
                Authorized_Area = entity.Authorized_Area,
                Used_Type = entity.Used_Type,
                Used_Status = entity.Used_Status,
                Obtained_Method = entity.Obtained_Method,
                Used_Partition = entity.Used_Partition,
                Is_Marginal_Land = entity.Is_Marginal_Land,
                Own_Area = entity.Own_Area,
                NONOwn_Area = entity.NONOwn_Area,
                Original_Cost = entity.Original_Cost,
                Deprn_Amount = entity.Deprn_Amount,
                Current_Cost = entity.Current_Cost,
                Reval_Adjustment_Amount = entity.Reval_Adjustment_Amount,
                Reval_Land_VAT = entity.Reval_Land_VAT,
                Reval_Reserve = entity.Reval_Reserve,
                Business_Area_Size = entity.Business_Area_Size,
                Business_Book_Amount = entity.Business_Book_Amount,
                Business_Reval_Reserve = entity.Business_Reval_Reserve,
                NONBusiness_Area_Size = entity.NONBusiness_Area_Size,
                NONBusiness_Book_Amount = entity.NONBusiness_Book_Amount,
                NONBusiness_Reval_Reserve = entity.NONBusiness_Reval_Reserve,
                Year_Number = entity.Year_Number,
                Announce_Amount = entity.Announce_Amount,
                Announce_Price = entity.Announce_Price,
                Tax_Type = entity.Tax_Type,
                Reduction_reason = entity.Reduction_reason,
                Transfer_Price = entity.Transfer_Price,
                Reduction_Area = entity.Reduction_Area,
                Declared_Price = entity.Declared_Price,
                Dutiable_Amount = entity.Dutiable_Amount,
                Remark1 = entity.Remark1,
                Remark2 = entity.Remark2,
                Land_Item = entity.Land_Item,
                CROP = entity.CROP,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Last_Updated_Time = entity.Last_UpDatetimed_Time,
                Last_Updated_By = entity.Last_UpDatetimed_By.HasValue ? "" : entity.Last_UpDatetimed_By.Value.ToString()
            };
        }
        #endregion

        #region== Model To Entity ==
        public static AS_TRX_Headers ToAS_Assets_Land_Header(LandHeaderModel model)
        {
            return new AS_TRX_Headers()
            {
                ID = model.ID,
                Form_Number = model.Form_Number,
                Transaction_Type = model.Transaction_Type,
                Transaction_Status = model.Transaction_Status,
                Book_Type_Code = model.Book_Type_Code,
                Transaction_Datetime_Entered = model.Transaction_Datetime_Entered,
                Accounting_Datetime = model.Accounting_Datetime,
                Office_Branch = model.Office_Branch,
                Auto_Post = model.Auto_Post,
                Amotized_Adjustment_Flag = model.Amotized_Adjustment_Flag,
                Description = model.Description,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Created_By_Name = model.Created_By_Name,
                Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = model.Last_UpDatetimed_By,
                Last_UpDatetimed_By_Name = model.Last_UpDatetimed_By_Name
            };
        }

        public static AS_Assets_Land ToAS_Assets_Land(AssetLandModel model)
        {
            return new AS_Assets_Land()
            {
                ID = model.ID,
                Asset_Number = model.Asset_Number,
                Current_units = model.Current_units,
                Datetime_Placed_In_Service = model.Datetime_Placed_In_Service,
                Old_Asset_Number = model.Old_Asset_Number,
                Asset_Category_code = model.Asset_Category_code,
                Location_Disp = model.Location_Disp,
                Description = model.Description,
                Urban_Renewal = model.Urban_Renewal,
                City_Code = model.City_Code,
                District_Code = model.District_Code,
                Section_Code = model.Section_Code,
                Sub_Section_Name = model.Sub_Section_Name,
                Parent_Land_Number = model.Parent_Land_Number,
                Filial_Land_Number = model.Filial_Land_Number,
                Authorization_Number = model.Authorization_Number,
                Authorized_name = model.Authorized_name,
                Area_Size = model.Area_Size,
                Authorized_Scope_Molecule = model.Authorized_Scope_Molecule,
                Authorized_Scope_Denomminx = model.Authorized_Scope_Denomminx,
                Authorized_Area = model.Authorized_Area,
                Used_Type = model.Used_Type,
                Used_Status = model.Used_Status,
                Obtained_Method = model.Obtained_Method,
                Used_Partition = model.Used_Partition,
                Is_Marginal_Land = model.Is_Marginal_Land,
                Own_Area = model.Own_Area,
                NONOwn_Area = model.NONOwn_Area,
                Original_Cost = model.Original_Cost,
                Deprn_Amount = model.Deprn_Amount,
                Current_Cost = model.Current_Cost,
                Reval_Adjustment_Amount = model.Reval_Adjustment_Amount,
                Reval_Land_VAT = model.Reval_Land_VAT,
                Reval_Reserve = model.Reval_Reserve,
                Business_Area_Size = model.Business_Area_Size,
                Business_Book_Amount = model.Business_Book_Amount,
                Business_Reval_Reserve = model.Business_Reval_Reserve,
                NONBusiness_Area_Size = model.NONBusiness_Area_Size,
                NONBusiness_Book_Amount = model.NONBusiness_Book_Amount,
                NONBusiness_Reval_Reserve = model.NONBusiness_Reval_Reserve,
                Year_Number = model.Year_Number,
                Announce_Amount = model.Announce_Amount,
                Announce_Price = model.Announce_Price,
                Tax_Type = model.Tax_Type,
                Reduction_reason = model.Reduction_reason,
                Transfer_Price = model.Transfer_Price,
                Reduction_Area = model.Reduction_Area,
                Declared_Price = model.Declared_Price,
                Dutiable_Amount = model.Dutiable_Amount,
                Remark1 = model.Remark1,
                Remark2 = model.Remark2,
                Retire_Cost = model.Retire_Cost,
                Sell_Amount = model.Sell_Amount,
                Sell_Cost = model.Sell_Cost,
                Delete_Reason = model.Delete_Reason,
                Land_Item = model.Land_Item,
                CROP = model.CROP,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Last_UpDatetimed_Time = model.Last_Updated_Time,
                Last_UpDatetimed_By = model.Last_Updated_By == ""?(decimal?)null:decimal.Parse (model.Last_Updated_By)
            };
        }

        public static AS_Assets_Land ToAS_Assets_LandFromAS_Assets_Land_Record(AS_Assets_Land_Record model)
        {
            return new AS_Assets_Land()
            {
                Asset_Number = model.Asset_Number,
                Current_units = model.Current_units,
                Datetime_Placed_In_Service = model.Datetime_Placed_In_Service,
                Old_Asset_Number = model.Old_Asset_Number,
                Asset_Category_code = model.Asset_Category_code,
                Location_Disp = model.Location_Disp,
                Description = model.Description,
                Urban_Renewal = model.Urban_Renewal,
                City_Code = model.City_Code,
                District_Code = model.District_Code,
                Section_Code = model.Section_Code,
                Sub_Section_Name = model.Sub_Section_Name,
                Parent_Land_Number = model.Parent_Land_Number,
                Filial_Land_Number = model.Filial_Land_Number,
                Authorization_Number = model.Authorization_Number,
                Authorized_name = model.Authorized_name,
                Area_Size = model.Area_Size,
                Authorized_Scope_Molecule = model.Authorized_Scope_Molecule,
                Authorized_Scope_Denomminx = model.Authorized_Scope_Denomminx,
                Authorized_Area = model.Authorized_Area,
                Used_Type = model.Used_Type,
                Used_Status = model.Used_Status,
                Obtained_Method = model.Obtained_Method,
                Used_Partition = model.Used_Partition,
                Is_Marginal_Land = model.Is_Marginal_Land,
                Own_Area = model.Own_Area,
                NONOwn_Area = model.NONOwn_Area,
                Original_Cost = model.Original_Cost,
                Deprn_Amount = model.Deprn_Amount,
                Current_Cost = model.Current_Cost,
                Reval_Adjustment_Amount = model.Reval_Adjustment_Amount,
                Reval_Land_VAT = model.Reval_Land_VAT,
                Reval_Reserve = model.Reval_Reserve,
                Business_Area_Size = model.Business_Area_Size,
                Business_Book_Amount = model.Business_Book_Amount,
                Business_Reval_Reserve = model.Business_Reval_Reserve,
                NONBusiness_Area_Size = model.NONBusiness_Area_Size,
                NONBusiness_Book_Amount = model.NONBusiness_Book_Amount,
                NONBusiness_Reval_Reserve = model.NONBusiness_Reval_Reserve,
                Year_Number = model.Year_Number,
                Announce_Amount = model.Announce_Amount,
                Announce_Price = model.Announce_Price,
                Tax_Type = model.Tax_Type,
                Reduction_reason = model.Reduction_reason,
                Transfer_Price = model.Transfer_Price,
                Reduction_Area = model.Reduction_Area,
                Declared_Price = model.Declared_Price,
                Dutiable_Amount = model.Dutiable_Amount,
                Remark1 = model.Remark1,
                Remark2 = model.Remark2,
                Land_Item = model.Land_Item,
                CROP = model.CROP,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = model.Last_UpDatetimed_By 
            };
        }

        public static AS_Assets_Land_Record ToAS_Assets_Land_Record(string TRX_Header_ID, AssetLandModel model)
        {
            return new AS_Assets_Land_Record()
            {
                TRX_Header_ID = TRX_Header_ID,
                ID = model.ID,
                Asset_Land_ID = "",
                Created_By_Name = "",
                Last_UpDatetimed_By_Name = "",
                
                Serial_Number = "", 
                Asset_Number = model.Asset_Number,
                Current_units = model.Current_units,
                Datetime_Placed_In_Service = model.Datetime_Placed_In_Service,
                Old_Asset_Number = !string.IsNullOrEmpty(model.Old_Asset_Number) ? model.Old_Asset_Number : "",
                Asset_Category_code = !string.IsNullOrEmpty(model.Asset_Category_code) ? model.Asset_Category_code : "",
                Location_Disp = !string.IsNullOrEmpty(model.Location_Disp) ? model.Location_Disp : "",
                Description = !string.IsNullOrEmpty(model.Description) ? model.Description : "",
                Urban_Renewal = !string.IsNullOrEmpty(model.Urban_Renewal)? model.Urban_Renewal : "",
                City_Code = !string.IsNullOrEmpty(model.City_Code) ? model.City_Code : "",
                City_Name = !string.IsNullOrEmpty(model.City_Name) ? model.City_Name : "",
                District_Code = !string.IsNullOrEmpty(model.District_Code) ? model.District_Code : "",
                District_Name = !string.IsNullOrEmpty(model.District_Name) ? model.District_Name : "",
                Section_Code = !string.IsNullOrEmpty(model.Section_Code) ? model.Section_Code : "",
                Section_Name = !string.IsNullOrEmpty(model.Section_Name) ? model.Section_Name : "",
                Sub_Section_Name = !string.IsNullOrEmpty(model.Sub_Section_Name) ? model.Sub_Section_Name : "",
                Parent_Land_Number = !string.IsNullOrEmpty(model.Parent_Land_Number) ? model.Parent_Land_Number : "",
                Filial_Land_Number = !string.IsNullOrEmpty(model.Filial_Land_Number) ? model.Filial_Land_Number : "",
                Authorization_Number = !string.IsNullOrEmpty(model.Authorization_Number) ? model.Authorization_Number : "",
                Authorized_name = !string.IsNullOrEmpty(model.Authorized_name) ? model.Authorized_name : "",
                Area_Size = model.Area_Size,
                Authorized_Scope_Molecule = model.Authorized_Scope_Molecule,
                Authorized_Scope_Denomminx = model.Authorized_Scope_Denomminx,
                Authorized_Area = model.Authorized_Area,
                Used_Type = !string.IsNullOrEmpty(model.Used_Type) ? model.Used_Type : "",
                Used_Status = !string.IsNullOrEmpty(model.Used_Status) ? model.Used_Status : "",
                Obtained_Method = !string.IsNullOrEmpty(model.Obtained_Method) ? model.Obtained_Method : "",
                Used_Partition = !string.IsNullOrEmpty(model.Used_Partition) ? model.Used_Partition : "",
                Is_Marginal_Land = !string.IsNullOrEmpty(model.Is_Marginal_Land) ? model.Is_Marginal_Land : "",
                Own_Area = model.Own_Area,
                NONOwn_Area = model.NONOwn_Area,
                Original_Cost = model.Original_Cost,
                Deprn_Amount = model.Deprn_Amount,
                Current_Cost = model.Current_Cost,
                Reval_Adjustment_Amount = model.Reval_Adjustment_Amount,
                Reval_Land_VAT = model.Reval_Land_VAT,
                Reval_Reserve = model.Reval_Reserve,
                Business_Area_Size = model.Business_Area_Size,
                Business_Book_Amount = model.Business_Book_Amount,
                Business_Reval_Reserve = model.Business_Reval_Reserve,
                NONBusiness_Area_Size = model.NONBusiness_Area_Size,
                NONBusiness_Book_Amount = model.NONBusiness_Book_Amount,
                NONBusiness_Reval_Reserve = model.NONBusiness_Reval_Reserve,
                Year_Number = (int)model.Year_Number,
                Announce_Amount = model.Announce_Amount,
                Announce_Price = model.Announce_Price,
                Tax_Type = !string.IsNullOrEmpty(model.Tax_Type) ? model.Tax_Type : "",
                Reduction_reason = !string.IsNullOrEmpty(model.Reduction_reason) ? model.Reduction_reason : "",
                Transfer_Price = model.Transfer_Price,
                Reduction_Area = model.Reduction_Area,
                Declared_Price = model.Declared_Price,
                Dutiable_Amount = model.Dutiable_Amount,
                Remark1 = !string.IsNullOrEmpty(model.Remark1)? model.Remark1:"",
                Remark2 = !string.IsNullOrEmpty(model.Remark2)? model.Remark2:"",
                Land_Item = !string.IsNullOrEmpty(model.Land_Item) ? model.Land_Item : "",
                CROP = model.CROP,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Last_UpDatetimed_Time = model.Last_Updated_Time,
                Last_UpDatetimed_By = model.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(model.Last_Updated_By),
                Expense_Account = !string.IsNullOrEmpty(model.Expense_Account) ? model.Expense_Account : "",
            };
        }
        public static AS_Assets_Land_MP_Record ToAS_Assets_Land_MP_Record(string TRX_Header_ID, AssetLandMPModel model)
        {
            return new AS_Assets_Land_MP_Record()
            {
                ID = model.ID,
                Assets_Land_MP_ID = 0,
                TRX_Header_ID = TRX_Header_ID,
                Asset_Number = !string.IsNullOrEmpty(model.Asset_Number) ? model.Asset_Number : "",
                Parent_Asset_Number = !string.IsNullOrEmpty(model.Parent_Asset_Number) ? model.Parent_Asset_Number : "",
                Asset_Category_code = !string.IsNullOrEmpty(model.Asset_Category_Code) ? model.Asset_Category_Code : "",
                Old_Asset_Number = !string.IsNullOrEmpty(model.Old_Asset_Number) ? model.Old_Asset_Number : "",
                Datetime_Placed_In_Service = model.Date_Placed_In_Service != null ? model.Date_Placed_In_Service : DateTime.Now,
                Current_units = model.Current_units,
                Deprn_Method_Code = !string.IsNullOrEmpty(model.Deprn_Method_Code) ? model.Deprn_Method_Code : "",
                Life_Years = model.Life_Years,
                Life_Months = model.Life_Months,
                Location_Disp = !string.IsNullOrEmpty(model.Location_Disp) ? model.Location_Disp : "",
                PO_Number = !string.IsNullOrEmpty(model.PO_Number) ? model.PO_Number : "",
                PO_Destination = !string.IsNullOrEmpty(model.PO_Destination) ? model.PO_Destination : "",
                Model_Number = !string.IsNullOrEmpty(model.Model_Number) ? model.Model_Number : "",
                Transaction_Date = model.Transaction_Date,
                Assets_Unit = !string.IsNullOrEmpty(model.Assets_Unit) ? model.Assets_Unit : "",
                Accessory_Equipment = !string.IsNullOrEmpty(model.Accessory_Equipment) ? model.Accessory_Equipment : "",
                Assigned_NUM = !string.IsNullOrEmpty(model.Assigned_NUM) ? model.Assigned_NUM : "",
                Asset_Category_NUM = !string.IsNullOrEmpty(model.Asset_Category_Num) ? model.Asset_Category_Num : "",
                Description = !string.IsNullOrEmpty(model.Description) ? model.Description : "",
                Asset_Structure = !string.IsNullOrEmpty(model.Asset_Structure) ? model.Asset_Structure : "",
                Current_Cost = model.Current_Cost,
                Deprn_Reserve = model.Deprn_Reserve,
                Salvage_Value = model.Salvage_Value,
                Remark = !string.IsNullOrEmpty(model.Remark) ? model.Remark : "",
                Created_By_Name = model.Created_By_Name,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By == ""?(decimal?)null:decimal.Parse(model.Created_By),
                Last_UpDatetimed_Time = model.Last_Updated_Time,
                Last_UpDatetimed_By = model.Last_Updated_By == "" ? (decimal?)null : decimal.Parse(model.Last_Updated_By)
            };
        }
        public static AS_Assets_Land_MP_Retire ToAS_Assets_Land_MP_Retire(string TRX_Header_ID, AssetLandMPRetireModel model)
        {
            return new AS_Assets_Land_MP_Retire()
            {
                ID = model.ID,
                TRX_Header_ID = TRX_Header_ID,
                Parent_Asset_Number= model.Parent_Asset_Number,
                Serial_Number = !string.IsNullOrEmpty(model.SerialNo) ? model.SerialNo : "",
                Asset_Number = !string.IsNullOrEmpty(model.Asset_Number) ? model.Asset_Number : "",
                Old_Cost = model.Old_Cost,
                Retire_Cost = model.Retire_Cost,
                Sell_Cost = model.Sell_Cost,
                Sell_Amount = model.Sell_Amount,
                New_Cost = model.New_Cost,
                Reval_Adjustment_Amount = model.Reval_Adjustment_Amount,
                Reval_Reserve = model.Reval_Reserve,
                Reval_Land_VAT = model.Reval_Land_VAT, //dl.Reval_Land_VAT,
                Reason_Code = !string.IsNullOrEmpty(model.Reason_Code) ? model.Reason_Code : "",
                Account_Type = model.Account_Type,
                Receipt_Type = model.Receipt_Type,
                Description = !string.IsNullOrEmpty(model.Description) ? model.Description : "",
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Created_By_Name = !string.IsNullOrEmpty(model.Created_By_Name) ? model.Created_By_Name : "",
                Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = model.Last_UpDatetimed_By == "" ? (decimal?)null : decimal.Parse(model.Last_UpDatetimed_By),
                Last_UpDatetimed_By_Name = !string.IsNullOrEmpty(model.Last_UpDatetimed_By_Name) ? model.Last_UpDatetimed_By_Name : ""
            };
        }
        public static AS_Assets_Land_Retire ToAS_Assets_Land_Retire(string TRX_Header_ID, AssetLandRetireModel model, LandDispose dl)
        {
            return new AS_Assets_Land_Retire()
            {
                ID = model.ID,
                TRX_Header_ID = TRX_Header_ID,
                Assets_Land_ID = "",
                Serial_Number = !string.IsNullOrEmpty(model.SerialNo) ? model.SerialNo : "",
                Asset_Number = !string.IsNullOrEmpty(model.Asset_Number) ? model.Asset_Number : "",
                Parent_Land_Number = !string.IsNullOrEmpty(model.Parent_Land_Number) ? model.Parent_Land_Number : "",
                Filial_Land_Number = !string.IsNullOrEmpty(model.Filial_Land_Number) ? model.Filial_Land_Number : "",
                City_Code = !string.IsNullOrEmpty(model.City_Code) ? model.City_Code : "",
                City_Name = !string.IsNullOrEmpty(model.City_Name) ? model.City_Name : "",
                District_Code = !string.IsNullOrEmpty(model.District_Code) ? model.District_Code : "",
                District_Name = !string.IsNullOrEmpty(model.District_Name) ? model.District_Name : "",
                Section_Code = !string.IsNullOrEmpty(model.Section_Code) ? model.Section_Code : "",
                Section_Name = !string.IsNullOrEmpty(model.Section_Name) ? model.Section_Name : "",
                Sub_Section_Name = !string.IsNullOrEmpty(model.Sub_Section_Name) ? model.Sub_Section_Name : "",
                Build_Address = !string.IsNullOrEmpty(model.Build_Address) ? model.Build_Address : "",
                Build_Number = !string.IsNullOrEmpty(model.Build_Number) ? model.Build_Number : "",
                Old_Cost = model.Old_Cost,
                Retire_Cost = dl.Retire_Cost,
                Sell_Cost=dl.Sell_Cost,
                Sell_Amount = dl.Sell_Amount,
                New_Cost = dl.New_Cost,
                Reval_Adjustment_Amount = dl.Reval_Adjustment_Amount,
                Reval_Reserve = dl.Reval_Reserve,
                Reval_Land_VAT = 0, //dl.Reval_Land_VAT,
                Reason_Code = !string.IsNullOrEmpty(model.Reason_Code) ? model.Reason_Code : "",
                Account_Type = model.Account_Type,
                Receipt_Type = model.Receipt_Type,
                Retire_Cost_Account = !string.IsNullOrEmpty(model.Retire_Cost_Account) ? model.Retire_Cost_Account : "",
                Sell_Amount_Account = !string.IsNullOrEmpty(model.Sell_Amount_Account) ? model.Sell_Amount_Account : "",
                Sell_Cost_Account = !string.IsNullOrEmpty(model.Sell_Cost_Account) ? model.Sell_Cost_Account : "",
                Reval_Adjustment_Amount_Account = !string.IsNullOrEmpty(model.Reval_Adjustment_Amount_Account) ? model.Reval_Adjustment_Amount_Account : "",
                Reval_Reserve_Account = !string.IsNullOrEmpty(model.Reval_Reserve_Account) ? model.Reval_Reserve_Account : "",
                Reval_Land_VAT_Account = !string.IsNullOrEmpty(model.Reval_Land_VAT_Account) ? model.Reval_Land_VAT_Account : "",
                Description = !string.IsNullOrEmpty(model.Description) ? model.Description : "",
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Created_By_Name = !string.IsNullOrEmpty(model.Created_By_Name) ? model.Created_By_Name : "",
                Last_UpDatetimed_Time = model.Last_UpDatetimed_Time,
                Last_UpDatetimed_By = model.Last_UpDatetimed_By == "" ? (decimal?)null : decimal.Parse(model.Last_UpDatetimed_By),
                Last_UpDatetimed_By_Name = !string.IsNullOrEmpty(model.Last_UpDatetimed_By_Name) ? model.Last_UpDatetimed_By_Name : ""

            };
        }
        #endregion
    }
}
