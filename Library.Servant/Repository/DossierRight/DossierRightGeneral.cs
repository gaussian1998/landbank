﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Servant.Servant.DossierRight.DossierRightModels;
using Library.Entity.Edmx;

namespace Library.Servant.Repository.DossierRight
{
    public class DossierRightGeneral
    {
        private static string CodeTableClass = "DossierRightForm";
        /// <summary>
        /// 取得表單編號
        /// </summary>
        /// <returns></returns>
        public static string GetFormCode(string Type)
        {
            string result = "";
            using (var db = new AS_LandBankEntities())
            {
                if (db.AS_Code_Table.Any(o => o.Class == CodeTableClass && o.Parameter1 == Type))
                {
                    result = db.AS_Code_Table.First(o => o.Class == CodeTableClass && o.Parameter1 == Type).Code_ID;
                }
            }
            return result;
        }
        /// <summary>
        /// 取得表單名稱
        /// </summary>
        /// <returns></returns>
        public static string GetFormName(string Type)
        {
            string result = "";
            using (var db = new AS_LandBankEntities())
            {
                if (db.AS_Code_Table.Any(o => o.Class == CodeTableClass && o.Parameter1 == Type))
                {
                    result = db.AS_Code_Table.First(o => o.Class == CodeTableClass && o.Parameter1 == Type).Text;
                }
            }
            return result;
        }
        /// <summary>
        /// 取得來源別名稱 (標準代碼)
        /// </summary>
        /// <returns></returns>
        public static string GetSourceName(string source)
        {
            switch (source.ToUpper())
            {
                case "H":
                    return "總行個別新增";
                case "B":
                    return "分行個別新增";
                default:
                    return "NULL";
            }
        }
        /// <summary>
        /// 取得controllername
        /// </summary>
        /// <returns></returns>
        public static string GetController(string FormCode)
        {
            string result = "";
            using (var db = new AS_LandBankEntities())
            {
                if (db.AS_Code_Table.Any(o => o.Class == CodeTableClass && o.Code_ID == FormCode))
                {
                    result = db.AS_Code_Table.First(o => o.Class == CodeTableClass && o.Code_ID == FormCode).Parameter1;
                }
            }
            return result;
        }
        /// <summary>
        /// 取得controller 的 Function Name (來源: code table)
        /// </summary>
        /// <returns></returns>
        public static string GetFunctionName(string controllerName)
        {
            string result = "";
            using (var db = new AS_LandBankEntities())
            {
                if (db.AS_Code_Table.Any(o => o.Class == CodeTableClass && o.Parameter1 == controllerName))
                {
                    result = db.AS_Code_Table.First(o => o.Class == CodeTableClass && o.Parameter1 == controllerName).Text;
                }
            }
            return result;
        }
        /// <summary>
        /// 取得controller 的 Function Name (來源: code table)
        /// </summary>
        /// <returns></returns>
        public static string GetFunctionNameByCode(string code)
        {
            string result = "";
            using (var db = new AS_LandBankEntities())
            {
                if (db.AS_Code_Table.Any(o => o.Class == CodeTableClass && o.Code_ID == code))
                {
                    result = db.AS_Code_Table.First(o => o.Class == CodeTableClass && o.Code_ID == code).Text;
                }
            }
            return result;
        }
        #region Entity To Model

        public static HeaderModel ToHeaderModel(AS_Assets_Right_Headers entity)
        {
            return new HeaderModel()
            {
                ID = entity.ID,
                Form_Number = entity.Form_Number,
                Office_Branch_Code = entity.Office_Branch,
                Source = entity.Source,
                Transaction_Type = entity.Transaction_Type,
                Transaction_Status = entity.Transaction_Status,
                Transaction_Datetime_Entered = entity.Transaction_Datetime_Entered,
                Description = entity.Description,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Last_Updated_Time = entity.Last_Updated_Time,
                Last_Updated_By = entity.Last_Updated_By
            };
        }
        public static DossierRightRecordModel ToRecordModel(AS_Assets_Right_Record entity)
        {
            if (entity == null)
                return null;
            return new DossierRightRecordModel()
            {
                ID = entity.ID,
                AS_Assets_Right_ID = entity.AS_Assets_Right_ID,
                Authorization_Category = entity.Authorization_Category,
                AS_Assets_Right_Header_ID = entity.AS_Assets_Right_Header_ID,
                Asset_Number = entity.Asset_Number,
                City_Code = entity.City_Code,
                City_Name = entity.City_Name,
                District_Code = entity.District_Code,
                District_Name = entity.District_Name,
                Section_Code = entity.Section_Code,
                Section_Name = entity.Section_Name,
                Sub_Section_Name = entity.Sub_Section_Name,
                Parent_Land_Number = entity.Parent_Land_Number,
                Filial_Land_Number = entity.Filial_Land_Number,
                Build_Number = entity.Build_Number,
                Build_Address = entity.Build_Address,
                Building_STRU = entity.Building_STRU,
                Used_Type = entity.Used_Type,
                Registration_Date = entity.Registration_Date,
                RightBook_Date = entity.RightBook_Date,
                Authorization_Number = entity.Authorization_Number,
                Right_UploadFile = entity.Right_UploadFile,
                Description = entity.Description,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Last_Updated_Time = entity.Last_Updated_Time,
                Last_Updated_By = entity.Last_Updated_By
            };
        }
        public static DossierRightRecordModel MainModelToRecordModel(AS_Assets_Right entity)
        {
            if (entity == null)
                return null;
            return new DossierRightRecordModel()
            {
                ID = entity.ID,
                Authorization_Category = entity.Authorization_Category,
                Asset_Number = entity.Asset_Number,
                City_Code = entity.City_Code,
                City_Name = entity.City_Name,
                District_Code = entity.District_Code,
                District_Name = entity.District_Name,
                Section_Code = entity.Section_Code,
                Section_Name = entity.Section_Name,
                Sub_Section_Name = entity.Sub_Section_Name,
                Parent_Land_Number = entity.Parent_Land_Number,
                Filial_Land_Number = entity.Filial_Land_Number,
                Build_Number = entity.Build_Number,
                Build_Address = entity.Build_Address,
                Building_STRU = entity.Building_STRU,
                Used_Type = entity.Used_Type,
                Registration_Date = entity.Registration_Date,
                RightBook_Date = entity.RightBook_Date,
                Authorization_Number = entity.Authorization_Number,
                Right_UploadFile = entity.Right_UploadFile,
                Description = entity.Description,
                Create_Time = entity.Create_Time,
                Created_By = entity.Created_By,
                Last_Updated_Time = entity.Last_Updated_Time,
                Last_Updated_By = entity.Last_Updated_By
            };
        }
        #endregion
        #region   Model To Entity
        public static AS_Assets_Right_Headers To_Header(HeaderModel model)
        {
            return new AS_Assets_Right_Headers()
            {
                ID = model.ID,
                Form_Number = model.Form_Number,
                Office_Branch = model.Office_Branch_Code,
                Source = model.Source,
                Transaction_Type = model.Transaction_Type,
                Transaction_Status = model.Transaction_Status,
                Transaction_Datetime_Entered = model.Transaction_Datetime_Entered,
                Description = model.Description,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }
        public static AS_Assets_Right_Record To_Record(string Header_ID, DossierRightRecordModel model)
        {
            return new AS_Assets_Right_Record()
            {
                ID = model.ID,
                AS_Assets_Right_ID = string.IsNullOrEmpty(model.AS_Assets_Right_ID)?"": model.AS_Assets_Right_ID,
                Authorization_Category = string.IsNullOrEmpty(model.Authorization_Category) ? "" : model.Authorization_Category,
                AS_Assets_Right_Header_ID = Header_ID,
                Asset_Number = string.IsNullOrEmpty(model.Asset_Number) ? "" : model.Asset_Number,
                City_Code = string.IsNullOrEmpty(model.City_Code) ? "" : model.City_Code,
                City_Name = string.IsNullOrEmpty(model.City_Name) ? "" : model.City_Name,
                District_Code = string.IsNullOrEmpty(model.District_Code) ? "" : model.District_Code,
                District_Name = string.IsNullOrEmpty(model.District_Name) ? "" : model.District_Name,
                Section_Code = string.IsNullOrEmpty(model.Section_Code) ? "" : model.Section_Code,
                Section_Name = string.IsNullOrEmpty(model.Section_Name) ? "" : model.Section_Name,
                Sub_Section_Name = string.IsNullOrEmpty(model.Sub_Section_Name) ? "" : model.Sub_Section_Name,
                Parent_Land_Number = string.IsNullOrEmpty(model.Parent_Land_Number) ? "" : model.Parent_Land_Number,
                Filial_Land_Number = string.IsNullOrEmpty(model.Filial_Land_Number) ? "" : model.Filial_Land_Number,
                Build_Number = string.IsNullOrEmpty(model.Build_Number) ? "" : model.Build_Number,
                Build_Address = string.IsNullOrEmpty(model.Build_Address) ? "" : model.Build_Address,
                Building_STRU = string.IsNullOrEmpty(model.Building_STRU) ? "" : model.Building_STRU,
                Used_Type = string.IsNullOrEmpty(model.Used_Type) ? "" : model.Used_Type,
                Registration_Date = model.Registration_Date,
                RightBook_Date = model.RightBook_Date,
                Authorization_Number = string.IsNullOrEmpty(model.Authorization_Number) ? "" : model.Authorization_Number,
                Right_UploadFile = string.IsNullOrEmpty(model.Right_UploadFile) ? "" : model.Right_UploadFile,
                Description = string.IsNullOrEmpty(model.Description) ? "" : model.Description,
                Create_Time = model.Create_Time,
                Created_By = model.Created_By,
                Last_Updated_Time = model.Last_Updated_Time,
                Last_Updated_By = model.Last_Updated_By
            };
        }
        #endregion
    }
}
