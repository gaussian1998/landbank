﻿using System;
using System.ComponentModel.DataAnnotations;
using Library.Servant.Servant.Common;

namespace Library.Servant.Enums
{
    public enum ApprovalStatus
    {
        [Display(Name = "新增")]
        Create = 0,
        [Display(Name = "簽核中")]
        Auditing = 1,
        [Display(Name = "已核準")]
        Approved = 2,
        [Display(Name = "退回修改")]
        RejectForEdit = 3,
        [Display(Name = "抽回修改")]
        CallbackForEdit = 4,
        [Display(Name = "廢棄")]
        Discard = 5
    }

    public static partial class Extend
    {
        private static DisplayAttribute GetDisplayAttribute(this ApprovalStatus status)
        {
            try {
                var memInfo = status.GetType().GetMember(status.ToString());
                var prefixInfo = memInfo[0].GetCustomAttributes(typeof(DisplayAttribute), false);
                return (DisplayAttribute)prefixInfo[0];
            }
            catch (Exception ex) {
                throw new Exception("", ex);
            }
        }

        public static string GetDisplayName(this ApprovalStatus status)
        {
            try {
                return StaticDataServant.GetStatusText(((int)status).ToString());
            }
            catch (Exception ex) {
                throw new Exception("", ex);
            }
        }
        public static string GetName(string status)
        {
            if (!string.IsNullOrEmpty(status))
            {
                ApprovalStatus e;
                int p;
                if (int.TryParse(status, out p))
                {
                    if (Enum.IsDefined(typeof(ApprovalStatus), p))
                    {
                        e = (ApprovalStatus)p;
                        return e.GetDisplayAttribute().Name;
                    }
                }
            }
            return string.Empty;
        }
    }
}
