﻿using System;
using System.ComponentModel.DataAnnotations;
using Library.Servant.Servant.Common;

namespace Library.Servant.Enums
{
    public enum Amotized_Adjustment_Flag
    {
        [Display(Name = "未記帳")]
        NONAccount = 0,
        [Display(Name = "記帳")]
        Write = 1,
        [Display(Name = "核帳")]
        Pass = 2,
        [Display(Name = "拋轉")]
        Transfer = 3
    }

    public static partial class Extend
    {
        private static DisplayAttribute GetDisplayAttribute(this Amotized_Adjustment_Flag status)
        {
            try {
                var memInfo = status.GetType().GetMember(status.ToString());
                var prefixInfo = memInfo[0].GetCustomAttributes(typeof(DisplayAttribute), false);
                return (DisplayAttribute)prefixInfo[0];
            }
            catch (Exception ex) {
                throw new Exception("", ex);
            }
        }

        public static string GetDisplayName(this Amotized_Adjustment_Flag status)
        {
            try {
                return status.GetDisplayAttribute().Name;
            }
            catch (Exception ex) {
                throw new Exception("", ex);
            }
        }
    }
}
