﻿using System;
using Library.Servant.Attributes;

namespace Library.Servant.Enums
{
    public enum CodeTableClass
    {
        [ClassID("C02")]
        Department,
        [ClassID("C01")]
        Branch,
        [ClassID("R02")]
        TransactionStatus
    }

    public static partial class Extend
    {
        private static ClassIDAttribute GetCodeTableInfoInfo(this CodeTableClass type)
        {
            try {
                var memInfo = type.GetType().GetMember(type.ToString());
                var prefixInfo = memInfo[0].GetCustomAttributes(typeof(ClassIDAttribute), false);
                return (ClassIDAttribute)prefixInfo[0];
            }
            catch (Exception ex) {
                throw new Exception("", ex);
            }
        }

        public static string GetClassID(this CodeTableClass type)
        {
            try {
                return type.GetCodeTableInfoInfo().ClassID;
            }
            catch (Exception ex) {
                throw new Exception("", ex);
            }
        }
    }
}
