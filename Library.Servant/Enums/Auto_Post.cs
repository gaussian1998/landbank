﻿using System;
using System.ComponentModel.DataAnnotations;
using Library.Servant.Servant.Common;

namespace Library.Servant.Enums
{
    public enum Auto_Post
    {
        [Display(Name = "非自動")]
        NONAuto = 0,
        [Display(Name = "記錄")]
        Write = 1,
        [Display(Name = "自動核帳")]
        AutoPass = 2
    }

    public static partial class Extend
    {
        private static DisplayAttribute GetDisplayAttribute(this Auto_Post status)
        {
            try {
                var memInfo = status.GetType().GetMember(status.ToString());
                var prefixInfo = memInfo[0].GetCustomAttributes(typeof(DisplayAttribute), false);
                return (DisplayAttribute)prefixInfo[0];
            }
            catch (Exception ex) {
                throw new Exception("", ex);
            }
        }

        public static string GetDisplayName(this Auto_Post status)
        {
            try {
                return status.GetDisplayAttribute().Name;
            }
            catch (Exception ex) {
                throw new Exception("", ex);
            }
        }
    }
}
