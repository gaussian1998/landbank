﻿using System;
using Library.Servant.Attributes;

namespace Library.Servant.Enums
{
    public enum FormType
    {
        [FormTypeInfo("T110", "人員權限新增單")]
        RoleCreate,
        [FormTypeInfo("T120", "人員權限職務輪調單")]
        RoleRotation,
        [FormTypeInfo("T130", "人員權限其他單")]
        RoleOther,
        [FormTypeInfo("T200", "人員權限異動單")]
        RoleCreateWithModified,
        [FormTypeInfo("T210", "人員權限職務輪調異動單")]
        RoleRotationWithModified,
        [FormTypeInfo("T220", "人員權限其他單異動單")]
        RoleOtherWithModified,
        [FormTypeInfo("P100", "公告事項新增單")]
        PostCreate,
        [FormTypeInfo("P200", "公告事項修改單")]
        PostModified,
        [FormTypeInfo("", "Unknow")]
        Undefined
    }

    public static partial class Extend
    {
        private static FormTypeInfoAttribute GetFormTypeInfo(this FormType type)
        {
            try {
                var memInfo = type.GetType().GetMember(type.ToString());
                var prefixInfo = memInfo[0].GetCustomAttributes(typeof(FormTypeInfoAttribute), false);
                return (FormTypeInfoAttribute)prefixInfo[0];
            }
            catch(Exception ex) {
                throw new Exception("Form type information hasn't been defined", ex);
            }
        }

        public static string GetNewFormNO(this FormType type)
        {
            try {
                Random ran = new Random();

                // prefix + yyyyMMddHHmmssff + 1 random code
                return type.GetPrefixCode() + DateTime.Now.ToUniversalTime().ToString("yyyyMMddHHmmssff") + (ran.Next()%10).ToString();
            }
            catch(Exception ex) {
                throw new Exception("Get new form number fail", ex);
            }
        }

        public static string GetPrefixCode(this FormType type)
        {
            try {
                return type.GetFormTypeInfo().PrefixCode;
            }
            catch (Exception ex) {
                throw new Exception("Get prefix code fail", ex);
            }
        }

        public static string GetFormName(this FormType type)
        {
            try {
                return type.GetFormTypeInfo().FormName;
            }
            catch (Exception ex) {
                throw new Exception("Get form name fail", ex);
            }
        }
    }
}