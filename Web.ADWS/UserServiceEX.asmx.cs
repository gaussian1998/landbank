﻿using System;
using System.Linq;
using System.Web.Services;

namespace Web.ADWS
{
    /// <summary>
    /// Summary description for UserServiceEX
    /// </summary>
    [WebService(Namespace = "http://microsoft.com/taiwan/mcs")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class UserServiceEX : System.Web.Services.WebService, IUserServiceExSoap
    {
        private static ResponseDocument NullUser()
        {
            return new ResponseDocument
            {
                Status = new Status { StatusCode = -1, Desciption = "User Not Exist" },
                Users = new UserInfo[] { }
            };
        }

        public ResponseDocument IdVerify(string id, string password)
        {
            if(password == "Lb1234567890")
                return IdInquiry(id, "");
            else
                return NullUser();
        }

        public ResponseDocument IdInquiry(string id, string idSearchBase)
        {
            UserInfo userInfo = FakeUser.Values.SingleOrDefault(user => user.Id == id);
            if (userInfo == null)
                return NullUser();
            else
                return new ResponseDocument
                {
                    Status = new Status { StatusCode = 0, Desciption = "User Exist" },
                    Users = new UserInfo[] { userInfo }
                };
        }

        public ResponseDocument AggregateInquiry(string department, string groupName)
        {
            return new ResponseDocument
            {
                Status = new Status { StatusCode = 0, Desciption = "User Exist" },
                Users = FakeUser.Values.Where( m => m.Department == department).ToArray()
            };
        }

        public bool AddMember(string id, string groupName)
        {
            throw new NotImplementedException();
        }

        public Status AddMemberEx(string id, string idSearchBase, string groupName, string groupSearchBase)
        {
            throw new NotImplementedException();
        }



        public ResponseDocumentEx AggregateInquiryEx(string department, string groupName, string idSearchBase, string groupSearchBase, int EnableFlag)
        {
            throw new NotImplementedException();
        }

        public Status ChangePassword(string id, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public Status ChangePasswordEx(string id, string oldPassword, string newPassword, string searchBase)
        {
            throw new NotImplementedException();
        }

        public Status CreateComputer(string id, string containerDN, string description, string managedByDN, string joinDomainUserList)
        {
            throw new NotImplementedException();
        }

        public Status CreateMailbox(string employeeID, string idSearchBase, string email)
        {
            throw new NotImplementedException();
        }

        public Status CreateMailboxEx(string id, string idSearchBase, string email, string mdb)
        {
            throw new NotImplementedException();
        }

        public Status CreateNewUser(string employeeID, string givenName, string password)
        {
            throw new NotImplementedException();
        }

        public Status DeleteComputer(string id, string containerDN)
        {
            throw new NotImplementedException();
        }

        public Status DeleteMailboxEx(string id, string idSearchBase)
        {
            throw new NotImplementedException();
        }

        public Status DisableUser(string id)
        {
            throw new NotImplementedException();
        }

        public Status DisableUserEx(string id, string searchBase)
        {
            throw new NotImplementedException();
        }

        public Status EnableUser(string id)
        {
            throw new NotImplementedException();
        }

        public Status EnableUserEx(string id, string searchBase)
        {
            throw new NotImplementedException();
        }

        public ResponseDocumentEx GetManagedBy(string id, string idSearchBase, string objectType)
        {
            throw new NotImplementedException();
        }

        public DepartmentsResponseDocument GetSubDepartment(string departmentName)
        {
            throw new NotImplementedException();
        }

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }



        public ResponseDocumentEx IdInquiryEx(string id, string idSearchBase)
        {
            throw new NotImplementedException();
        }



        public Status MoveUser(string id, string sourceDirectoryPath, string targetDirectoryPath)
        {
            throw new NotImplementedException();
        }

        public ComputerResponseDocument QueryComputer(string id, string containerDN)
        {
            throw new NotImplementedException();
        }

        public bool RemoveMember(string id, string groupName)
        {
            throw new NotImplementedException();
        }

        public Status RemoveMemberEx(string id, string idSearchBase, string groupName, string groupSearchBase)
        {
            throw new NotImplementedException();
        }

        public Status ResetComputer(string id, string containerDN, string description, string managedByDN, string joinDomainUserList)
        {
            throw new NotImplementedException();
        }

        public Status ResetPassword(string id, string newPassword)
        {
            throw new NotImplementedException();
        }

        public Status ResetPasswordEx(string id, string newPassword, string searchBase)
        {
            throw new NotImplementedException();
        }

        public Status SetManagedBy(string id, string idSearchBase, string objectType, string managerId, string managerIdSearchBase)
        {
            throw new NotImplementedException();
        }

        public Status SetProperties(string id, string idSearchBase, string[] propertyNames, string[] propertyValues)
        {
            throw new NotImplementedException();
        }

        public Status SetUserDepartment(string id, string idSearchBase, string value)
        {
            throw new NotImplementedException();
        }

        public Status SetUserDisplayName(string id, string idSearchBase, string value)
        {
            throw new NotImplementedException();
        }

        public Status SetUserTitle(string id, string idSearchBase, string value)
        {
            throw new NotImplementedException();
        }

        public Status UnlockUser(string id, string idSearchBase)
        {
            throw new NotImplementedException();
        }
    }
}
