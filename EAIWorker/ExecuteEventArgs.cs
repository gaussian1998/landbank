﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAIWorker
{
    public class ExecuteEventArgs
    {
        public string Infomation { get; set; }
        public bool IsCommitWork { get; set; }
        public string CommitFilePath { get; set; }
    }
}
