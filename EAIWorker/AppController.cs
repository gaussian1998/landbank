﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using System.Xml;

namespace EAIWorker
{
    public static class AppController
    {
        private static string controlPath;

        public static void Initialize()
        {
            string controlFolder = Path.Combine(Directory.GetCurrentDirectory(), "control");
            controlPath = Path.Combine(controlFolder, String.Format(@"{0}.xml", DateTime.Today.ToString("yyyyMMdd")));
            if (!Directory.Exists(controlFolder))
            {
                Directory.CreateDirectory(controlFolder);
            }

            if (!File.Exists(controlPath))
            {
                XmlDocument xmldoc = new XmlDocument();
                string limit = ConfigurationManager.AppSettings["DailyWorkingLimit"];
                xmldoc.LoadXml(String.Format("<root available='{0}' inProcess='false'/>", limit));
                xmldoc.Save(controlPath);
            }
        }

        public static void ResetInProcess()
        {
            if (File.Exists(controlPath))
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.Load(controlPath);
                xmldoc.DocumentElement.SetAttribute("inProcess", "false");
                xmldoc.Save(controlPath);
            }
        }

        public static void StartWork()
        {
            if (!File.Exists(controlPath))
            {
                Initialize();
            }
            if (!IsBusy())
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.Load(controlPath);
                xmldoc.DocumentElement.SetAttribute("inProcess", "true");
                xmldoc.Save(controlPath);
            }
        }

        public static bool IsBusy()
        {
            bool isBusy = false;
            if (File.Exists(controlPath))
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.Load(controlPath);
                isBusy = Convert.ToBoolean(xmldoc.DocumentElement.GetAttribute("inProcess"));
            }
            else
            {
                Initialize();
            }
            return isBusy;
        }

        public static int AvailableCount()
        {
            if (!File.Exists(controlPath))
            {
                Initialize();
            }

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.Load(controlPath);
            return Convert.ToInt32(xmldoc.DocumentElement.GetAttribute("available"));

        }

        public static void CommitWork()
        {
            if (!File.Exists(controlPath))
            {
                Initialize();
            }

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.Load(controlPath);
            int i = Convert.ToInt32(xmldoc.DocumentElement.GetAttribute("available"));
            if (i > 0)
            {
                xmldoc.DocumentElement.SetAttribute("available", (i - 1).ToString());
                xmldoc.Save(controlPath);
            }
            else
            {
                ResetInProcess();
            }
        }
    }
}
