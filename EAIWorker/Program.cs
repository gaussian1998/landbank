﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using EAIWorker.Works;

namespace EAIWorker
{
    class Program
    {
        static void Main(string[] args)
        {
            AppController.Initialize();
            try
            {
                AppLog("=========================");
                AppLog("App started, check last worker status.");
                if (AppController.IsBusy())
                {
                    AppLog("Last worker still working, finish this worker, wait for next time called.");
                }
                else
                {
                    int a = AppController.AvailableCount();
                    if (a > 0)
                    {
                        string[] files = Directory.GetFiles(SourcePath);
                        if (files.Length == 0)
                        {
                            AppLog("No files need to be processed");
                        }
                        else
                        {
                            AppController.StartWork();

                            IWork demo = new DemoWork();
                            demo.ExecuteEvent += new WorkerEventHandler(WorkEventHandler);

                            int processCount = Math.Min(a, files.Length);
                            for (int i = 0; i < processCount; i++)
                            {
                                demo.DoWork(files[i]);
                            }
                            AppController.ResetInProcess();
                        }
                    }
                    else
                    {
                        AppLog("Today's quota used. no work will be launched.");
                    }
                }
                AppLog("End of this time's call");
            }
            catch (System.Exception e)
            {
                ExceptionLog(e);
            }
        }

        private static void WorkEventHandler(object sender, ExecuteEventArgs e)
        {
            if (e.IsCommitWork)
            {
                try
                {
                    if (!Directory.Exists(ArchivePath))
                    {
                        Directory.CreateDirectory(ArchivePath);
                    }

                    string sourceFileName = Path.GetFileName(e.CommitFilePath);
                    string newFilePath = Path.Combine(ArchivePath, sourceFileName);
                    File.Move(e.CommitFilePath, newFilePath);

                    AppController.CommitWork();
                }
                catch (System.Exception ex) {
                    ExceptionLog(ex);
                }
            }

            DetailLog(sender, e);
        }

        #region paths
        private static string SourcePath
        {
            get
            {
                string basePath = Directory.GetCurrentDirectory();
                return Path.Combine(basePath, ConfigurationManager.AppSettings["SourceDocumentFolder"]);
            }
        }
        private static string ArchivePath
        {
            get
            {
                string basePath = Directory.GetCurrentDirectory();
                return Path.Combine(basePath, ConfigurationManager.AppSettings["ArchiveFolder"]);
            }
        }
        private static string ExceptionLogsPath
        {
            get
            {
                string basePath = Directory.GetCurrentDirectory();
                return Path.Combine(basePath, ConfigurationManager.AppSettings["ExceptionLogsFolder"]);
            }
        }
        private static string DetailLogsPath
        {
            get
            {
                string basePath = Directory.GetCurrentDirectory();
                return Path.Combine(basePath, ConfigurationManager.AppSettings["DetailLogsFolder"]);
            }
        }

        private static string AppLogsPath
        {
            get
            {
                string basePath = Directory.GetCurrentDirectory();
                return Path.Combine(basePath, ConfigurationManager.AppSettings["AppLogsFolder"]);
            }
        }
        #endregion

        #region log mathods
        private static void ExceptionLog(Exception e)
        {
            try
            {
                if (!Directory.Exists(ExceptionLogsPath))
                {
                    Directory.CreateDirectory(ExceptionLogsPath);
                }

                AppLog("WARNING: EXCEPTION OCCUR!!!");

                string _FilePath = String.Format(@"{0}\{1}.txt", ExceptionLogsPath, DateTime.Now.ToString("yyyyMMddHH"));
                string _Message = String.Format("{3}{0}:{1}{3}{2}", DateTime.Now.ToString(), e.Message, e.StackTrace, Environment.NewLine);
                File.AppendAllText(_FilePath, _Message);

            }
            catch (System.Exception) { }
        }

        private static void AppLog(string message)
        {
            try
            {
                if (!Directory.Exists(AppLogsPath))
                {
                    Directory.CreateDirectory(AppLogsPath);
                }
                string _FilePath = String.Format(@"{0}\{1}.txt", AppLogsPath, DateTime.Now.ToString("yyyyMMddHH"));
                string _Message = String.Format("{2}{0}:{1}", DateTime.Now.ToString(), message, Environment.NewLine);
                File.AppendAllText(_FilePath, _Message);

            }
            catch (System.Exception) { }
        }

        private static void DetailLog(object sender, ExecuteEventArgs e)
        {
            try
            {

                if (!Directory.Exists(DetailLogsPath))
                {
                    Directory.CreateDirectory(DetailLogsPath);
                }
                string _FilePath = String.Format(@"{0}\{1}.txt", DetailLogsPath, DateTime.Now.ToString("yyyyMMddHH"));
                string _Message = String.Format("{2}{0}:{1}", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss ffffff"), e.Infomation, Environment.NewLine);
                File.AppendAllText(_FilePath, _Message);

            }
            catch (System.Exception) { }
        }
        #endregion

    }
}
