﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAIWorker.Works
{
    public class DemoWork : IWork
    {
        public event WorkerEventHandler ExecuteEvent;

        public void DoWork(string FilePath)
        {

            //your code here.
            ExecuteEvent(this, new ExecuteEventArgs { Infomation = "hey, i start to working" });

            ExecuteEvent(this, new ExecuteEventArgs { Infomation = "假的 你的眼睛業障重", IsCommitWork = true, CommitFilePath = FilePath });
        }
    }
}
