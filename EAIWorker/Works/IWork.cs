﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAIWorker;

namespace EAIWorker.Works
{
    public delegate void WorkerEventHandler(object sender, ExecuteEventArgs e);
    public interface IWork
    {
        event WorkerEventHandler ExecuteEvent;
        void DoWork(string FilePath);

    }
}
