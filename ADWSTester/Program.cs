﻿using System;
using System.Text;

namespace ADWSTester
{
    class Program
    {
        static void Main(string[] args)
        {
            TestIdVerify();
            TestQueryAll();
        }

        static void TestQueryAll()
        {
            var proxy = new UserServiceEx();
            proxy.Url = "http://adws.azurewebsites.net/UserServiceEX.asmx";

            var rs =  proxy.AggregateInquiry("00E","");
            foreach (var user in rs.Users)
            {
                Console.WriteLine("user.id: " + user.Id);
                Console.WriteLine("user.Title: " + user.Title);
                Console.WriteLine("user.Department: " + user.Department);
                Console.WriteLine("user.SubDepartment: " + user.SubDepartment);
                Console.WriteLine("user.DistinguishedName: " + user.DistinguishedName);
                Console.WriteLine("user.DisplayName: " + user.DisplayName);
            }

            Console.WriteLine("status : " + rs.Status.StatusCode + "  + description :" + rs.Status.Desciption + "+++++++++");
            Console.ReadLine();
        }

        static void TestIdVerify()
        {
            var proxy = new UserServiceEx();
            proxy.Url = "http://adws.azurewebsites.net/UserServiceEX.asmx";
            var rs = proxy.IdVerify("080383", "Lb1234567890");

            foreach (var user in rs.Users)
            {
                Console.WriteLine("user.id: " + user.Id);
                Console.WriteLine("user.Title: " + user.Title);
                Console.WriteLine("user.Department: " + user.Department);
                Console.WriteLine("user.SubDepartment: " + user.SubDepartment);
                Console.WriteLine("user.DistinguishedName: " + user.DistinguishedName);
                Console.WriteLine("user.DisplayName: " + user.DisplayName);
            }

            Console.WriteLine("status : " + rs.Status.StatusCode + "  + description :" + rs.Status.Desciption + "+++++++++" );
            Console.ReadLine();
        }
    }
}
