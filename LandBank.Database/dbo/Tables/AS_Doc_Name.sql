﻿CREATE TABLE [dbo].[AS_Doc_Name] (
    [ID]                INT            IDENTITY (1, 1) NOT NULL,
    [Doc_No]            VARCHAR (4)    NULL,
    [Doc_Type]          NVARCHAR (40)  NULL,
    [Doc_Type_Name]     NVARCHAR (40)  NULL,
    [Doc_Name]          NVARCHAR (100) NULL,
    [Is_Active]         BIT            DEFAULT ((1)) NOT NULL,
    [Cancel_Code]       BIT            DEFAULT ((0)) NOT NULL,
    [Remark]            NVARCHAR (100) NULL,
    [Last_Updated_Time] DATETIME       NULL,
    [Last_Updated_By]   NUMERIC (15)   NULL,
    CONSTRAINT [PK_AS_Doc_Name] PRIMARY KEY CLUSTERED ([ID] ASC)
);
