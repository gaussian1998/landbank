
CREATE TABLE [dbo].[AS_Assets_Build_Detail_Record](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Assets_Land_Build_ID] [int] NOT NULL,
	[TRX_Header_ID] [varchar](18) NOT NULL,
	[Asset_Type] [varchar](20) NOT NULL,
	[Floor_Uses] [nvarchar](240) NOT NULL,
	[Area_Size] [numeric](20, 2) NOT NULL,
	[Authorized_Scope_Moldcule] [numeric](20, 0) NOT NULL,
	[Authorized_Scope_Denominx] [numeric](20, 0) NOT NULL,
	[Authorized_Area] [numeric](20, 2) NOT NULL,
	[Create_Time] [datetime] NOT NULL,
	[Created_By] [varchar](15) NOT NULL,
	[Last_Updated_Time] [datetime] NULL,
	[Last_Updated_By] [varchar](15) NOT NULL,
 CONSTRAINT [PK_AS_Assets_Build_Detail_Record] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AS_Assets_Build_Detail_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_Detail_Record_TRX_Heard_ID]  DEFAULT ('') FOR [TRX_Header_ID]
GO

ALTER TABLE [dbo].[AS_Assets_Build_Detail_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_Detail_Record_Asset_Type]  DEFAULT ('') FOR [Asset_Type]
GO

ALTER TABLE [dbo].[AS_Assets_Build_Detail_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_Detail_Record_Floor_Uses]  DEFAULT ('') FOR [Floor_Uses]
GO

ALTER TABLE [dbo].[AS_Assets_Build_Detail_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_Detail_Record_Area_Size]  DEFAULT ((0)) FOR [Area_Size]
GO

ALTER TABLE [dbo].[AS_Assets_Build_Detail_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_Detail_Record_Authorized_Scope_Moldcule]  DEFAULT ((0)) FOR [Authorized_Scope_Moldcule]
GO

ALTER TABLE [dbo].[AS_Assets_Build_Detail_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_Detail_Record_Authorized_Scope_Denominx]  DEFAULT ((0)) FOR [Authorized_Scope_Denominx]
GO

ALTER TABLE [dbo].[AS_Assets_Build_Detail_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_Detail_Record_Authorized_Area]  DEFAULT ((0)) FOR [Authorized_Area]
GO

ALTER TABLE [dbo].[AS_Assets_Build_Detail_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_Detail_Record_Created_By]  DEFAULT ('') FOR [Created_By]
GO

ALTER TABLE [dbo].[AS_Assets_Build_Detail_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_Detail_Record_Last_Updated_By]  DEFAULT ('') FOR [Last_Updated_By]
GO


