
CREATE TABLE [dbo].[AS_Assets_MP_Import](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Import_Num] [varchar](20) NOT NULL,
	[Import_Year] [int] NOT NULL,
	[Import_Month] [int] NOT NULL,
	[PO_Number] [nvarchar](30) NOT NULL,
	[Office_Branch] [varchar](30) NOT NULL,
	[Status] [varchar](20) NOT NULL,
	[Source] [varchar](20) NOT NULL,
	[Remark] [nvarchar](600) NOT NULL,
	[Create_Time] [datetime] NOT NULL,
	[Created_By] [varchar](15) NOT NULL,
	[Last_Updated_Time] [datetime] NULL,
	[Last_Updated_By] [varchar](15) NOT NULL,
 CONSTRAINT [PK_AS_Assets_MP_Import] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AS_Assets_MP_Import] ADD  CONSTRAINT [DF_AS_Assets_MP_Import_Import_Num]  DEFAULT ('') FOR [Import_Num]
GO

ALTER TABLE [dbo].[AS_Assets_MP_Import] ADD  CONSTRAINT [DF_AS_Assets_MP_Import_Office_Branch]  DEFAULT ('') FOR [Office_Branch]
GO

ALTER TABLE [dbo].[AS_Assets_MP_Import] ADD  CONSTRAINT [DF_AS_Assets_MP_Import_Remark]  DEFAULT ('') FOR [Remark]
GO

ALTER TABLE [dbo].[AS_Assets_MP_Import] ADD  CONSTRAINT [DF_AS_Assets_MP_Import_Create_Time]  DEFAULT (getdate()) FOR [Create_Time]
GO

ALTER TABLE [dbo].[AS_Assets_MP_Import] ADD  CONSTRAINT [DF_AS_Assets_MP_Import_Created_By]  DEFAULT ('') FOR [Created_By]
GO

ALTER TABLE [dbo].[AS_Assets_MP_Import] ADD  CONSTRAINT [DF_AS_Assets_MP_Import_Last_Updated_By]  DEFAULT ('') FOR [Last_Updated_By]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_MP_Import', @level2type=N'COLUMN',@level2name=N'Create_Time'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立人員' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_MP_Import', @level2type=N'COLUMN',@level2name=N'Created_By'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後異動時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_MP_Import', @level2type=N'COLUMN',@level2name=N'Last_Updated_Time'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後異動人員' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_MP_Import', @level2type=N'COLUMN',@level2name=N'Last_Updated_By'
GO


