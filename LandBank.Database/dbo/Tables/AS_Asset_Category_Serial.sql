﻿CREATE TABLE [dbo].[AS_Asset_Category_Serial] (
    [ID]                  INT          IDENTITY (1, 1) NOT NULL,
    [Asset_Category_Code] VARCHAR (30) DEFAULT ('') NOT NULL,
    [Main_Kind]           VARCHAR (2)  NULL,
    [Detail_Kind]         VARCHAR (6)  NULL,
    [Use_Type]            VARCHAR (2)  NULL,
    [Is_Active]           BIT          DEFAULT ((1)) NOT NULL,
    [SerialNo_Used]       NUMERIC (7)  DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AS_Asset_Category_Serial] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Asset_Category_Serial1]
    ON [dbo].[AS_Asset_Category_Serial]([Asset_Category_Code] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Asset_Category_Serial2]
    ON [dbo].[AS_Asset_Category_Serial]([Main_Kind] ASC, [Detail_Kind] ASC, [Use_Type] ASC);