CREATE TABLE [dbo].[AS_Assets_Build_Land_Record](
	[Asset_Build_ID] [int] NOT NULL,
	[Asset_Land_ID] [int] NOT NULL,
	[TRX_Header_ID] [varchar](18) NOT NULL,
	[Create_Time] [datetime] NOT NULL,
	[Created_By] [varchar](15) NOT NULL,
	[Last_Updated_Time] [datetime] NULL,
	[Last_Updated_By] [varchar](15) NOT NULL,
 CONSTRAINT [PK_AS_Assets_Build_Land_Record] PRIMARY KEY CLUSTERED 
(
	[Asset_Build_ID] ASC,
	[Asset_Land_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AS_Assets_Build_Land_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_Land_Record_TRX_Heard_ID]  DEFAULT ('') FOR [TRX_Header_ID]
GO

ALTER TABLE [dbo].[AS_Assets_Build_Land_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_Land_Record_Create_Time]  DEFAULT (getdate()) FOR [Create_Time]
GO

ALTER TABLE [dbo].[AS_Assets_Build_Land_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_Land_Record_Created_By]  DEFAULT ('') FOR [Created_By]
GO

ALTER TABLE [dbo].[AS_Assets_Build_Land_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_Land_Record_Last_Updated_By]  DEFAULT ('') FOR [Last_Updated_By]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_Land_Record', @level2type=N'COLUMN',@level2name=N'Asset_Build_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�g�a�D��ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_Land_Record', @level2type=N'COLUMN',@level2name=N'Asset_Land_ID'
GO


