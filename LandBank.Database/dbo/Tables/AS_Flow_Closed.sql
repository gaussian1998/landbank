CREATE TABLE [dbo].[AS_Flow_Closed] (
    [ID]                INT            IDENTITY (1, 1) NOT NULL,
    [FLow_Open_ID]      INT            NULL,
    [Form_No]           VARCHAR (17)   DEFAULT ('') NOT NULL,
    [Doc_Type]          NVARCHAR (40)  NULL,
    [Doc_No]            VARCHAR (4)    NULL,
    [Table_ID]          INT            NULL,
    [Last_ID]           INT            NULL,
    [Last_Reject_Memo]  NVARCHAR (60)  NULL,
    [Branch_Code]       VARCHAR (3)    NOT NULL,
    [Apply_User_ID]     NUMERIC (15)   NULL,
    [Issue_Time]        DATETIME       NULL,
    [Flow_Code]         VARCHAR (10)   NULL,
    [Version]           NUMERIC (3)    NULL,
    [Step_No]           NUMERIC (2)    NULL,
    [End_Point]         BIT            DEFAULT ((0)) NOT NULL,
    [Flow_Status]       NUMERIC (1)    DEFAULT ((0)) NOT NULL,
    [Gate_Others]       NUMERIC (15)   NULL,
    [Gate_Pass]         BIT            NULL,
    [Result]            NUMERIC (1)    NULL,
    [Audit_Opinion]     NVARCHAR (MAX) NULL,
    [Current_Role_ID]   VARCHAR (10)   NULL,
    [Current_User_ID]   NUMERIC (15)   NULL,
    [Agent_User_ID]     NUMERIC (15)   NULL,
    [Reject_Role_ID]    VARCHAR (10)   NULL,
    [Reject_User_ID]    VARCHAR (10)   NULL,
    [Step_Role_ID]      VARCHAR (10)   NULL,
    [Step_User_ID]      NUMERIC (15)   NULL,
    [Form_Name]         VARCHAR (100)  NULL,
    [Form_Version]      NUMERIC (2)    NULL,
    [Mgr_Branch_Code]   VARCHAR (3)    NULL,
    [Mgr_Sub_Depart]    NVARCHAR (10)  NULL,
    [Last_Updated_Time] DATETIME       NULL,
    [Last_Updated_By]   NUMERIC (15)   NULL
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_AS_Flow_Closed1]
    ON [dbo].[AS_Flow_Closed]([Form_No] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Closed1]
    ON [dbo].[AS_Flow_Closed]([FLow_Open_ID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Closed3]
    ON [dbo].[AS_Flow_Closed]([Branch_Code] ASC, [Doc_No] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Closed4]
    ON [dbo].[AS_Flow_Closed]([Apply_User_ID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Closed5]
    ON [dbo].[AS_Flow_Closed]([Issue_Time] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Closed6]
    ON [dbo].[AS_Flow_Closed]([Current_User_ID] ASC);


