CREATE TABLE [dbo].[AS_Asset_Cost](
	[TRX_Header_ID] [varchar](18) NOT NULL,
	[Asset_Number] [varchar](30) NOT NULL,
	[Old_Cost] [numeric](20, 2) NOT NULL,
	[Add_Cost] [numeric](20, 2) NOT NULL,
	[Less_Cost] [numeric](20, 2) NOT NULL,
	[New_Cost]  AS (([Old_Cost]+[Add_Cost])-[Less_Cost]),
	[Create_Time] [datetime] NOT NULL,
	[Created_By] [varchar](15) NOT NULL,
	[Last_Updated_Time] [datetime] NULL,
	[Last_Updated_By] [varchar](15) NOT NULL,
 CONSTRAINT [PK_AS_Asset_Cost] PRIMARY KEY CLUSTERED 
(
	[TRX_Header_ID] ASC,
	[Asset_Number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AS_Asset_Cost] ADD  CONSTRAINT [DF_AS_Asset_Cost_Old_Cost]  DEFAULT ((0)) FOR [Old_Cost]
GO

ALTER TABLE [dbo].[AS_Asset_Cost] ADD  CONSTRAINT [DF_AS_Asset_Cost_Add_Cost]  DEFAULT ((0)) FOR [Add_Cost]
GO

ALTER TABLE [dbo].[AS_Asset_Cost] ADD  CONSTRAINT [DF_AS_Asset_Cost_Less_Cost]  DEFAULT ((0)) FOR [Less_Cost]
GO

ALTER TABLE [dbo].[AS_Asset_Cost] ADD  CONSTRAINT [DF_AS_Asset_Cost_Create_Time]  DEFAULT (getdate()) FOR [Create_Time]
GO

ALTER TABLE [dbo].[AS_Asset_Cost] ADD  CONSTRAINT [DF_AS_Asset_Cost_Created_By]  DEFAULT ('') FOR [Created_By]
GO

ALTER TABLE [dbo].[AS_Asset_Cost] ADD  CONSTRAINT [DF_AS_Asset_Cost_Last_Updated_By]  DEFAULT ('') FOR [Last_Updated_By]
GO


