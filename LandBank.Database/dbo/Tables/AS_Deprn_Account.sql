CREATE TABLE [dbo].[AS_Deprn_Account] (
    [ID]              INT          IDENTITY (1, 1) NOT NULL,
    [Book_Type]       VARCHAR (20) DEFAULT ('') NOT NULL,
    [Trans_Type]      CHAR (1)     DEFAULT ('') NOT NULL,
    [Master_No]       NUMERIC (6)  DEFAULT ((0)) NOT NULL,
    [Asset_Category]  VARCHAR (30) DEFAULT ('') NOT NULL,
    [Account]         VARCHAR (12) DEFAULT ('') NOT NULL,
    [Assets_Account]  VARCHAR (12) DEFAULT ('') NOT NULL,
    [Deprn_Account]   VARCHAR (12) DEFAULT ('') NOT NULL,
    [Expense_Account] VARCHAR (12) DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_AS_Deprn_Account] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Deprn_Account1]
    ON [dbo].[AS_Deprn_Account]([Trans_Type] ASC, [Asset_Category] ASC);


