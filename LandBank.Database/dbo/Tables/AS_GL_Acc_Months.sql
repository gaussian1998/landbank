﻿CREATE TABLE [dbo].[AS_GL_Acc_Months] (
    [ID]                INT             IDENTITY (1, 1) NOT NULL,
    [Book_Type]         VARCHAR (20)    DEFAULT ('') NOT NULL,
    [Branch_Code]       VARCHAR (3)     DEFAULT ('') NOT NULL,
    [Department]        VARCHAR (20)    DEFAULT ('') NOT NULL,
    [Branch_Ind]        CHAR (1)        DEFAULT ('') NOT NULL,
    [Account]           VARCHAR (12)    DEFAULT ('') NOT NULL,
    [Account_Name]      NVARCHAR (40)   DEFAULT ('') NOT NULL,
    [Asset_Liablty]     CHAR (1)        DEFAULT ('') NOT NULL,
    [Category]          CHAR (1)        DEFAULT ('') NOT NULL,
    [Currency]          VARCHAR (4)     DEFAULT ('') NOT NULL,
    [GL_Account]        VARCHAR (20)    DEFAULT ('') NOT NULL,
    [Master_No]         NUMERIC (6)     DEFAULT ((0)) NOT NULL,
    [Open_Date]         DATE            DEFAULT ('') NOT NULL,
    [Open_Type]         VARCHAR (3)     DEFAULT ('') NOT NULL,
    [Trans_Type]        CHAR (1)        DEFAULT ('') NOT NULL,
    [Trade_Type]        CHAR (3)        DEFAULT ('') NOT NULL,
    [Account_Key]       VARCHAR (10)    DEFAULT ('') NOT NULL,
    [Trans_Seq]         VARCHAR (30)    DEFAULT ('') NOT NULL,
    [Post_Seq]          INT             DEFAULT ((0)) NOT NULL,
    [DB_CR]             CHAR (1)        DEFAULT ('') NOT NULL,
    [Real_Memo]         CHAR (1)        DEFAULT ('') NOT NULL,
    [Stat_Balance_Date] DATE            NULL,
    [Status]            CHAR (1)        DEFAULT ('') NOT NULL,
    [Asset_Number]      VARCHAR (30)    DEFAULT ('') NOT NULL,
    [Lease_Number]      VARCHAR (9)     DEFAULT ('') NOT NULL,
    [Virtual_Account]   VARCHAR (14)    DEFAULT ('') NOT NULL,
    [Payment_User]      NVARCHAR (60)   DEFAULT ('') NOT NULL,
    [Payment_User_ID]   VARCHAR (20)    DEFAULT ('') NOT NULL,
    [Batch_Seq]         VARCHAR (20)    DEFAULT ('') NOT NULL,
    [Import_Number]     VARCHAR (20)    DEFAULT ('') NOT NULL,
    [Soft_Lock]         BIT             DEFAULT ((0)) NOT NULL,
    [Close_Date]        DATE            NULL,
    [Stat_Frequency]    NUMERIC (6)     DEFAULT ((0)) NOT NULL,
    [Post_Date]         DATE            NULL,
    [Amount]            NUMERIC (15, 2) DEFAULT ((0)) NOT NULL,
    [Value_Date]        DATE            NULL,
    [Settle_Date]       DATE            NULL,
    [Notes]             NVARCHAR (100)  DEFAULT ('') NOT NULL,
    [Notes1]            NVARCHAR (100)  DEFAULT ('') NOT NULL,
    [Last_Updated_Time] DATETIME        DEFAULT ('') NOT NULL,
    [Last_Updated_By]   NUMERIC (15)    DEFAULT ((0)) NOT NULL,
    [Is_Updated]        BIT             DEFAULT ((0)) NOT NULL,
    [Updated_Notes]     NVARCHAR (100)  DEFAULT ('') NOT NULL,
    [EAI_Time]          DATETIME        NULL,
    [EAI_Seq]           VARCHAR (20)    DEFAULT ('') NOT NULL,
    [Deprn_Months]      VARCHAR (6)     DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_AS_GL_Acc_Months] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Months1]
    ON [dbo].[AS_GL_Acc_Months]([Batch_Seq] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Months2]
    ON [dbo].[AS_GL_Acc_Months]([Account] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Months3]
    ON [dbo].[AS_GL_Acc_Months]([Open_Date] ASC, [Account] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Months4]
    ON [dbo].[AS_GL_Acc_Months]([Open_Date] ASC, [Branch_Code] ASC, [Account] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Months5]
    ON [dbo].[AS_GL_Acc_Months]([Branch_Code] ASC, [Open_Date] ASC, [Account] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Months6]
    ON [dbo].[AS_GL_Acc_Months]([Trans_Seq] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Months7]
    ON [dbo].[AS_GL_Acc_Months]([Post_Date] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Months8]
    ON [dbo].[AS_GL_Acc_Months]([Settle_Date] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Months9]
    ON [dbo].[AS_GL_Acc_Months]([Import_Number] ASC);