﻿
CREATE TABLE [dbo].[AS_Leases_Integration_Header](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TRX_Header_ID] [varchar](18) NOT NULL,
	[Transaction_Type] [varchar](4) NOT NULL,
	[Source] [varchar](1) NOT NULL,
	[Transaction_Time] [datetime] NULL,
	[Book_Type] [varchar](20) NOT NULL,
	[OriOffice_Branch] [varchar](30) NOT NULL,
	[Office_Branch] [varchar](30) NOT NULL,
	[DisposeReason] [varchar](30) NOT NULL,
	[Remark] [nvarchar](600) NOT NULL,
	[Flow_Status] [varchar](1) NOT NULL,
	[Create_Time] [datetime] NOT NULL,
	[Created_By] [varchar](15) NOT NULL,
	[Last_Updated_Time] [datetime] NULL,
	[Last_Updated_By] [varchar](15) NOT NULL,
 CONSTRAINT [PK_AS_Assets_Lease_Header] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AS_Leases_Integration_Header] ADD  CONSTRAINT [DF_AS_Assets_Lease_Header_Book_Type]  DEFAULT ('') FOR [Book_Type]
GO

ALTER TABLE [dbo].[AS_Leases_Integration_Header] ADD  CONSTRAINT [DF_AS_Assets_Lease_Header_OriOffice_Branch]  DEFAULT ('') FOR [OriOffice_Branch]
GO

ALTER TABLE [dbo].[AS_Leases_Integration_Header] ADD  CONSTRAINT [DF_AS_Assets_Lease_Header_Office_Branch]  DEFAULT ('') FOR [Office_Branch]
GO

ALTER TABLE [dbo].[AS_Leases_Integration_Header] ADD  CONSTRAINT [DF_AS_Assets_Lease_Header_DisposeReason]  DEFAULT ('') FOR [DisposeReason]
GO

ALTER TABLE [dbo].[AS_Leases_Integration_Header] ADD  CONSTRAINT [DF_AS_Assets_Lease_Header_Remark]  DEFAULT ('') FOR [Remark]
GO

ALTER TABLE [dbo].[AS_Leases_Integration_Header] ADD  CONSTRAINT [DF_AS_Assets_Lease_Header_Flow_Status]  DEFAULT ('') FOR [Flow_Status]
GO

ALTER TABLE [dbo].[AS_Leases_Integration_Header] ADD  CONSTRAINT [DF_AS_Assets_Lease_Header_Create_Time]  DEFAULT (getdate()) FOR [Create_Time]
GO

ALTER TABLE [dbo].[AS_Leases_Integration_Header] ADD  CONSTRAINT [DF_AS_Assets_Lease_Header_Last_Updated_By]  DEFAULT ('') FOR [Last_Updated_By]
GO
