﻿CREATE TABLE [dbo].[AS_Assets_AssetMaster] (
    [ID]                BIGINT         NOT NULL,
    [Document_Type]     BIGINT         NULL,
    [Source_Type]       BIGINT         NULL,
    [TRX_Header_ID]     NVARCHAR (17)  NULL,
    [BOOK_TYPE_CODE]    VARCHAR (20)   NULL,
    [BeginDate]         DATETIME       NULL,
    [EndDate]           DATETIME       NULL,
    [Type]              INT            NULL,
    [DESCRIPTION]       NVARCHAR (600) NULL,
    [Office_Branch]     VARCHAR (3)    NULL,
    [Create_Time]       DATETIME       NULL,
    [Created_By]        INT            NULL,
    [Last_Updated_Time] DATETIME       NULL,
    [Last_Updated_By]   INT            NULL,
    CONSTRAINT [PK_AS_Assets_AssetMaster] PRIMARY KEY CLUSTERED ([ID] ASC)
);

