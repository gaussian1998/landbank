﻿CREATE TABLE [dbo].[AS_Asset_Category_Main_Kinds] (
    [ID]                INT            IDENTITY (1, 1) NOT NULL,
    [No]                VARCHAR (2)    NOT NULL,
    [Name]              NVARCHAR (100) NOT NULL,
    [Is_Active]         BIT            CONSTRAINT [DF_AS_Assets_Main_Category_Kinds_Is_Active] DEFAULT ((1)) NOT NULL,
    [Last_Updated_Time] DATETIME       NULL,
    [Last_Updated_By]   INT            NULL,
    CONSTRAINT [PK_AS_Assets_Main_Category_Kinds] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_AS_Asset_Category_Main_Kinds]
    ON [dbo].[AS_Asset_Category_Main_Kinds]([No] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'目前沒有作用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category_Main_Kinds', @level2type = N'COLUMN', @level2name = N'Is_Active';

