﻿CREATE TABLE [dbo].[AS_Post_Header]
(
	[ID] INT NOT NULL IDENTITY , 
    [Flow_Code] VARCHAR(30) NULL, 
    [Transaction_Status] VARCHAR NULL, 
    [Transaction_Datetime] DATETIME NULL, 
    [Transaction_Type] VARCHAR(4) NULL, 
    [User_ID] INT NULL, 
    [Office_Branch] VARCHAR(3) NULL, 
    [Create_Time] DATETIME NULL, 
    [Created_By] INT NULL, 
    [Last_Updated_Time] DATETIME NULL, 
    [Last_Updated_By] INT NULL, 
    CONSTRAINT [PK_AS_Post_Header] PRIMARY KEY ([ID]), 
    CONSTRAINT [FK_AS_Post_Header_AS_Users] FOREIGN KEY ([Created_By]) REFERENCES [AS_Users]([ID]),
	CONSTRAINT [FK_AS_Post_Header_AS_Users_LastUpdate] FOREIGN KEY ([Last_Updated_By]) REFERENCES [AS_Users]([ID])
)

GO

CREATE INDEX [IX_AS_Post_Header_ID_FlowCode] ON [dbo].[AS_Post_Header] ([ID], [Flow_Code])
