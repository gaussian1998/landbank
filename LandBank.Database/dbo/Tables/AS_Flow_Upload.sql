﻿CREATE TABLE [dbo].[AS_Flow_Upload] (
    [ID]            INT           IDENTITY (1, 1) NOT NULL,
    [FormNo]        VARCHAR (17)  NOT NULL,
    [Path]          VARCHAR (200) NOT NULL,
    [FileName]      VARCHAR (200) NOT NULL,
    [SourceName]    VARCHAR (10)  NOT NULL,
    [UploadUserID]  NUMERIC (15)  NOT NULL,
    [UploadTime]    DATETIME      NOT NULL,
    [Log_Record_ID] INT           NOT NULL,
    CONSTRAINT [PK_AS_Flow_Upload] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Upload_FormNo]
    ON [dbo].[AS_Flow_Upload]([FormNo] ASC);
