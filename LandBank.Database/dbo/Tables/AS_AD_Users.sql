﻿CREATE TABLE [dbo].[AS_AD_Users] (
    [ID]                INT   IDENTITY(1,1)   NOT NULL,
    [USER_ID]                       NUMERIC (15)   NOT NULL,
    [USER_NAME]                     NVARCHAR (100) NOT NULL,
    [LAST_UPDATE_DATE]              DATE           NULL,
    [LAST_UPDATED_BY]               NUMERIC (15)   NULL,
    [CREATION_DATE]                 DATE           NULL,
    [CREATED_BY]                    NUMERIC (15)   NULL,
    [LAST_UPDATE_LOGIN]             NUMERIC (15)   NULL,
    [ENCRYPTED_FOUNDATION_PASSWORD] NVARCHAR (100) NULL,
    [ENCRYPTED_USER_PASSWORD]       NVARCHAR (100) NULL,
    [SESSION_NUMBER]                NUMERIC (18)   NULL,
    [START_DATE]                    DATE           NULL,
    [END_DATE]                      DATE           NULL,
    [DESCRIPTION]                   VARCHAR (240)  NULL,
    [LAST_LOGON_DATE]               DATE           NULL,
    [PASSWORD_DATE]                 DATE           NULL,
    [PASSWORD_ACCESSES_LEFT]        NUMERIC (15)   NULL,
    [PASSWORD_LIFESPAN_ACCESSES]    NUMERIC (15)   NULL,
    [PASSWORD_LIFESPAN_DAYS]        NUMERIC (15)   NULL,
    [EMPLOYEE_ID]                   NUMERIC (15)   NULL,
    [EMAIL_ADDRESS]                 NVARCHAR (240) NULL,
    [FAX]                           NVARCHAR (80)  NULL,
    [CUSTOMER_ID]                   NUMERIC (15)   NULL,
    [SUPPLIER_ID]                   NUMERIC (15)   NULL,
    [WEB_PASSWORD]                  NVARCHAR (240) NULL,
    [USER_GUID]                     VARCHAR (240)  NULL,
    [GCN_CODE_COMBINATION_ID]       NUMERIC (15)   NULL,
    [STATUS]                        NVARCHAR (30)  NULL,
    [UPDATE_FLAG]                   NVARCHAR (30)  NULL,
    [PERSON_PARTY_ID]               NUMERIC (18)   NULL,
    [EXPLOYEE_NAME]                 NVARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_AD_Users_USER_ID]
    ON [dbo].[AS_AD_Users]([USER_ID] ASC);
