﻿CREATE TABLE [dbo].[AS_GL_Calendar] (
    [ID]                INT          IDENTITY (1, 1) NOT NULL,
    [GL_Term]           CHAR (1)     NULL,
    [Book_Type]         VARCHAR (20) NULL,
    [Calendar_Value]    VARCHAR (10) NULL,
    [Years]             NUMERIC (4)  NULL,
    [Months]            NUMERIC (2)  NULL,
    [Quarter]           NUMERIC (1)  NULL,
    [Date_Of_Begin]     DATE         NULL,
    [Date_Of_End]       DATE         NULL,
    [Term_Basis]        NUMERIC (3)  DEFAULT ((0)) NOT NULL,
    [Last_Updated_Time] DATETIME     NULL,
    [Last_Updated_By]   NUMERIC (15) NULL,
    CONSTRAINT [PK_AS_GL_Calendar] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Calendar1]
    ON [dbo].[AS_GL_Calendar]([GL_Term] ASC, [Calendar_Value] ASC, [Date_Of_Begin] ASC, [Date_Of_End] ASC);

