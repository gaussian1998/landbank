﻿CREATE TABLE [dbo].[AS_RoleFlow_Header] (
    [ID]                   INT           IDENTITY (1, 1) NOT NULL,
    [Flow_Code]            VARCHAR (30)  NOT NULL,
    [Transaction_Status]   VARCHAR (15)  NOT NULL,
    [Transaction_Datetime] DATETIME      NULL,
    [Transaction_Type]     VARCHAR (4)   NULL,
    [Application_Reson]    VARCHAR (100) NULL,
    [Version]              VARCHAR (2)   NOT NULL,
    [Create_Time]          DATETIME      NULL,
    [Created_By]           INT           NULL,
    [Last_Updated_Time]    DATETIME      NULL,
    [Last_Updated_By]      INT           NULL,
    CONSTRAINT [PK_AS_RoleFlow_Header] PRIMARY KEY CLUSTERED ([ID] ASC)
);