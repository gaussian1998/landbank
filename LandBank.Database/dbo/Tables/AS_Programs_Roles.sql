﻿CREATE TABLE [dbo].[AS_Programs_Roles] (
    [Program_ID]  INT           NOT NULL,
    [Role_ID]     INT           NOT NULL,
    [Description] VARCHAR (255) NULL,
    CONSTRAINT [PK_AS_Programs_Roles] PRIMARY KEY CLUSTERED ([Program_ID] ASC, [Role_ID] ASC),
    CONSTRAINT [FK_AS_Programs_Roles_AS_Programs] FOREIGN KEY ([Program_ID]) REFERENCES [dbo].[AS_Programs] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_AS_Programs_Roles_AS_Roles] FOREIGN KEY ([Role_ID]) REFERENCES [dbo].[AS_Role] ([ID]) ON DELETE CASCADE
);
