﻿CREATE TABLE [dbo].[AS_File_System]
(
	[ID] INT NOT NULL IDENTITY,
	[FileName] NVARCHAR(384) NOT NULL,
	[IsDirectory] bit NOT NULL,
	[Directory] INT NULL,
	[Owner] NVARCHAR(256) NULL,
	[Group] NVARCHAR(256) NULL,
	[Data] varbinary(max) NULL, 
    CONSTRAINT [PK_AS_File_System] PRIMARY KEY ([ID]), 
    CONSTRAINT [FK_AS_File_System_Self] FOREIGN KEY ([Directory]) REFERENCES [AS_File_System]([ID])
)




GO

CREATE INDEX [IX_AS_File_System_FileName] ON [dbo].[AS_File_System] ([FileName])
