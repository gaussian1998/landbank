CREATE TABLE [dbo].[AS_Assets_PlanningBranch] (
    [ID]                BIGINT       NOT NULL,
    [Activation_ID]     BIGINT       NULL,
    [BranchID]          VARCHAR (50) NULL,
    [Create_Time]       DATETIME     NULL,
    [Created_By]        INT          NULL,
    [Last_Updated_Time] DATETIME     NULL,
    [Last_Updated_By]   INT          NULL,
    CONSTRAINT [PK_AS_Assets_PlanningBranch] PRIMARY KEY CLUSTERED ([ID] ASC)
);

