﻿CREATE TABLE [dbo].[ContentFile]
(
	[FileId] BIGINT IDENTITY(1,1) NOT NULL,
	[MessageId] BIGINT NULL,
	[OriginalFileName]  NVARCHAR(200) NULL,
	[RealFileName]		NVARCHAR(200) NULL,
	[RealFilePath]		NVARCHAR(500) NULL,
	[Creator]           NVARCHAR (50) NOT NULL,
    [CreateDateTime]    DATETIME      NOT NULL,
    [Updator]           NVARCHAR (50) NULL,
    [UpdateDateTime]    DATETIME      NULL,
	CONSTRAINT [PK_ContentFile] PRIMARY KEY CLUSTERED ([FileId] ASC)
)
