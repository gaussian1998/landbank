CREATE TABLE [dbo].[AS_Assets_Activation_LBDetail] (
    [ID]            BIGINT NOT NULL,
    [Activation_ID] BIGINT NULL,
    [Type]          INT    NULL,
    [LBID]          INT    NULL,
    CONSTRAINT [PK_AS_Assets_Activation_LBDetail] PRIMARY KEY CLUSTERED ([ID] ASC)
);

