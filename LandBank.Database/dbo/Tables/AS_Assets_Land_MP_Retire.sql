CREATE TABLE [dbo].[AS_Assets_Land_MP_Retire] (
    [ID]                       INT             IDENTITY (1, 1) NOT NULL,
    [Assets_Land_MP_ID]        NUMERIC (20)    NOT NULL,
    [TRX_Header_ID]            VARCHAR (18)    CONSTRAINT [DF_AS_Assets_Land_MP_Retire_TRX_Header_ID] DEFAULT ('') NOT NULL,
    [Serial_Number]            NVARCHAR (20)   NOT NULL,
    [Asset_Number]             NVARCHAR (30)   NOT NULL,
    [Parent_Asset_Number]      VARCHAR (30)    NOT NULL,
    [Old_Cost]                 DECIMAL (20, 2) NULL,
    [Retire_Cost]              DECIMAL (20, 2) NULL,
    [Sell_Amount]              DECIMAL (20, 2) NULL,
    [Sell_Cost]                DECIMAL (20, 2) NULL,
    [New_Cost]                 DECIMAL (20, 2) NULL,
    [Reval_Adjustment_Amount]  DECIMAL (20, 2) NULL,
    [Reval_Reserve]            DECIMAL (20, 2) NULL,
    [Reval_Land_VAT]           DECIMAL (20, 2) NULL,
    [Reason_Code]              VARCHAR (15)    NULL,
    [Description]              NVARCHAR (600)  NULL,
    [Account_Type]             INT             NULL,
    [Receipt_Type]             INT             NULL,
    [Create_Time]              DATETIME        NULL,
    [Created_By]               VARCHAR (15)    CONSTRAINT [DF_AS_Assets_Land_MP_Retire_Created_By] DEFAULT ('') NULL,
    [Created_By_Name]          NVARCHAR (100)  NULL,
    [Last_UpDatetimed_Time]    DATETIME        NULL,
    [Last_UpDatetimed_By]      DECIMAL (15)    NULL,
    [Last_UpDatetimed_By_Name] NVARCHAR (100)  NULL,
    CONSTRAINT [PK_AS_Assets_Land_MP_Retire] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'土地改良主檔ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Assets_Land_MP_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'單據編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'TRX_Header_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'項次', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Serial_Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'財產編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Asset_Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'母資產編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Parent_Asset_Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'帳面成本', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Old_Cost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'處分成本', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Retire_Cost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'變賣收入', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Sell_Amount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'變賣成本', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Sell_Cost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'異動後成本', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'New_Cost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'未實現重估增值', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Reval_Adjustment_Amount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'帳面金額-重估增值', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Reval_Reserve';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'估計應付土地增值稅', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Reval_Land_VAT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'處分原因', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Reason_Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'摘要', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Description';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'獲利/損失註記', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Account_Type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收款方式', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Receipt_Type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Create_Time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立人員 ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Created_By';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立人員姓名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Created_By_Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最後異動時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Last_UpDatetimed_Time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最後異動人員 ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Last_UpDatetimed_By';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最後異動人員姓名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Retire', @level2type = N'COLUMN', @level2name = N'Last_UpDatetimed_By_Name';

