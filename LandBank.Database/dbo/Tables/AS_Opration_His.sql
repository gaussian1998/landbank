﻿CREATE TABLE [dbo].[AS_Opration_His] (
    [ID]              INT          IDENTITY (1, 1) NOT NULL,
    [User_ID]         INT          NULL,
    [Branch_Code]     VARCHAR (3)  NULL,
    [Department_Code] VARCHAR (3)  NULL,
    [Login_Datetime]  DATETIME     NULL,
    [Last_request]    DATETIME     NULL,
    [Logout_Datetime] DATETIME     NULL,
    [User_IP]         VARCHAR (20) NULL,
    CONSTRAINT [PK_AS_Opration_His] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_AS_Opration_His_AS_Users] FOREIGN KEY ([User_ID]) REFERENCES [dbo].[AS_Users] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Opration_His_User_ID]
    ON [dbo].[AS_Opration_His]([User_ID] ASC);
