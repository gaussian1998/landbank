CREATE TABLE [dbo].[AS_Keep_Position](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Layer] [int] NOT NULL,
	[Parent_ID] [int] NULL,
	[Keep_Position_Code] [varchar](60) NULL,
	[Keep_Position_Name] [nvarchar](100) NULL,
	[Keep_Branch_Code] [varchar](3) NOT NULL,
	[Keep_Branch_Name] [nvarchar](40) NULL,
	[Keep_Dept] [varchar](20) NULL,
	[Keep_Dept_Name] [nvarchar](40) NULL,
	[Keep_SubDept_Code] [varchar](10) NULL,
	[Keep_SubDept_Name] [nvarchar](40) NULL,
	[Keep_Floor] [varchar](10) NULL,
	[Keep_Floor_Name] [nvarchar](40) NULL,
	[Last_Updated_Time] [datetime] NULL,
	[Last_Updated_By] [numeric](15, 0) NULL,
 CONSTRAINT [PK_AS_Keep_Position] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AS_Keep_Position] ADD  DEFAULT ((0)) FOR [Layer]
GO

ALTER TABLE [dbo].[AS_Keep_Position] ADD  DEFAULT ('') FOR [Keep_Branch_Code]
GO


