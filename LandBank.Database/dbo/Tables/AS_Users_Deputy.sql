﻿CREATE TABLE [dbo].[AS_Users_Deputy] (
    [ID]                INT      IDENTITY (1, 1) NOT NULL,
    [User_ID]           INT      NULL,
    [Deputy_User_ID]    INT      NULL,
    [Start_Time]        DATETIME NULL,
    [End_Time]          DATETIME NULL,
    [Is_Active]         BIT      NULL,
    [Last_Updated_Time] DATETIME NULL,
    [Last_Updated_By]   INT      NULL,
    CONSTRAINT [PK_AS_Users_Deputy] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_AS_Users_Deputy_AS_Users_Deputy] FOREIGN KEY ([Deputy_User_ID]) REFERENCES [dbo].[AS_Users] ([ID]),
    CONSTRAINT [FK_AS_Users_Deputy_AS_Users] FOREIGN KEY ([User_ID]) REFERENCES [dbo].[AS_Users] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Users_Deputy_IsActive_StartTime_UserID]
    ON [dbo].[AS_Users_Deputy]([Is_Active] ASC, [Start_Time] ASC, [User_ID] ASC);
