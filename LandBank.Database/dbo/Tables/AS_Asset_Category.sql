﻿CREATE TABLE [dbo].[AS_Asset_Category] (
    [ID]                  INT            IDENTITY (1, 1) NOT NULL,
    [Kind]                NUMERIC (1)    CONSTRAINT [DF__AS_Asset_C__Kind__1DB06A4F] DEFAULT ((0)) NOT NULL,
    [Layer]               NUMERIC (1)    CONSTRAINT [DF__AS_Asset___Layer__1EA48E88] DEFAULT ((0)) NOT NULL,
    [Parent_ID]           INT            NULL,
    [Asset_Category_Code] VARCHAR (30)   NULL,
    [Asset_Category_Name] NVARCHAR (100) NULL,
    [Main_Kind]           VARCHAR (2)    NOT NULL,
    [Detail_Kind]         VARCHAR (6)    NOT NULL,
    [Use_Type]            VARCHAR (2)    NOT NULL,
    [Is_Active]           BIT            CONSTRAINT [DF__AS_Asset___Is_Ac__1F98B2C1] DEFAULT ((1)) NOT NULL,
    [Deprn_Method]        VARCHAR (12)   CONSTRAINT [DF__AS_Asset___Deprn__208CD6FA] DEFAULT ('STL') NOT NULL,
    [Life_Years]          NUMERIC (3)    CONSTRAINT [DF__AS_Asset___Life___2180FB33] DEFAULT ((0)) NOT NULL,
    [Life_Months]         NUMERIC (6)    CONSTRAINT [DF__AS_Asset___Life___22751F6C] DEFAULT ((0)) NOT NULL,
    [Salvage_Value]       NUMERIC (6)    CONSTRAINT [DF__AS_Asset___Salva__236943A5] DEFAULT ((0)) NOT NULL,
    [Not_Deprn_Flag]      BIT            CONSTRAINT [DF__AS_Asset___Not_D__245D67DE] DEFAULT ((0)) NOT NULL,
    [Main_Kind_ID]        INT            NOT NULL,
    [Detail_Kind_ID]      INT            NOT NULL,
    [Use_Type_ID]         INT            NOT NULL,
    [SerialNo_Used]       INT            CONSTRAINT [DF_AS_Asset_Category_SerialNo_Used] DEFAULT ((0)) NOT NULL,
    [Last_Updated_Time]   DATETIME       NULL,
    [Last_Updated_By]     INT            NULL,
    CONSTRAINT [PK_AS_Asset_Category] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_AS_Asset_Category_AS_Asset_Category_Detail_Kinds] FOREIGN KEY ([Detail_Kind_ID]) REFERENCES [dbo].[AS_Asset_Category_Detail_Kinds] ([ID]),
    CONSTRAINT [FK_AS_Asset_Category_AS_Asset_Category_Main_Kinds] FOREIGN KEY ([Main_Kind_ID]) REFERENCES [dbo].[AS_Asset_Category_Main_Kinds] ([ID]),
    CONSTRAINT [FK_AS_Asset_Category_AS_Asset_Category_Use_Types] FOREIGN KEY ([Use_Type_ID]) REFERENCES [dbo].[AS_Asset_Category_Use_Types] ([ID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Kind';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Layer';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Parent_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Asset_Category_Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Asset_Category_Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Main_Kind';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Detail_Kind';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Use_Type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Is_Active';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Deprn_Method';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Life_Years';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Life_Months';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Salvage_Value';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Not_Deprn_Flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Main_Kind_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Detail_Kind_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Use_Type_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Last_Updated_Time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Asset_Category', @level2type = N'COLUMN', @level2name = N'Last_Updated_By';