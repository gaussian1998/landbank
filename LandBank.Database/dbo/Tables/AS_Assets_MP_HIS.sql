﻿CREATE TABLE [dbo].[AS_Assets_MP_HIS] (
    [ID]                     INT            IDENTITY (1, 1) NOT NULL,
    [TRX_Header_ID]          VARCHAR (18)   NOT NULL,
    [Asset_Number]           VARCHAR (30)   NOT NULL,
    [Assets_Original_ID]     VARCHAR (30)   CONSTRAINT [DF_AS_Assets_MP_HIS_Assets_Original_ID] DEFAULT ('') NOT NULL,
    [Assets_Category_ID]     NVARCHAR (30)  CONSTRAINT [DF_AS_Assets_MP_HIS_Assets_Category_ ID] DEFAULT ('') NOT NULL,
    [Assets_Name]            NVARCHAR (100) CONSTRAINT [DF_AS_Assets_MP_HIS_Assets_Name] DEFAULT ('') NOT NULL,
    [Assets_Alias]           NVARCHAR (100) CONSTRAINT [DF_AS_Assets_MP_HIS_Assets_Alias] DEFAULT ('') NOT NULL,
    [Assets_Unit]            INT            NOT NULL,
    [Assets_Parent_Number]   VARCHAR (30)   CONSTRAINT [DF_AS_Assets_MP_HIS_Assets_Parent_ID] DEFAULT ('') NOT NULL,
    [IsOversea]              VARCHAR (1)    CONSTRAINT [DF_AS_Assets_MP_HIS_IsOversea] DEFAULT ('') NOT NULL,
    [Asset_Category_Code]    NVARCHAR (30)  CONSTRAINT [DF_AS_Assets_MP_HIS_Asset_Category_Code] DEFAULT ('') NOT NULL,
    [Location_Disp]          VARCHAR (30)   CONSTRAINT [DF_AS_Assets_MP_HIS_Location_Disp] DEFAULT ('') NOT NULL,
    [Life_Years]             INT            NOT NULL,
    [Deprn_Method_Code]      VARCHAR (10)   CONSTRAINT [DF_AS_Assets_MP_HIS_Deprn_Method_Code] DEFAULT ('') NOT NULL,
    [Date_Placed_In_Service] DATETIME       NULL,
    [Assigned_Branch]        NVARCHAR (30)  CONSTRAINT [DF_AS_Assets_MP_HIS_Assigned_Branch] DEFAULT ('') NOT NULL,
    [Assigned_ID]            VARCHAR (30)   CONSTRAINT [DF_AS_Assets_MP_HIS_Assigned_ID] DEFAULT ('') NOT NULL,
    [AssignedSignUp]         VARCHAR (1)    CONSTRAINT [DF_AS_Assets_MP_HIS_AssignedSignUp] DEFAULT ('') NOT NULL,
    [InventoryCheck]         VARCHAR (1)    NULL,
    [InventoryCheckMemo]     NVARCHAR (500) NULL,
    [PO_Number]              NVARCHAR (30)  CONSTRAINT [DF_AS_Assets_MP_HIS_PO_Number] DEFAULT ('') NOT NULL,
    [PO_Description]         NVARCHAR (250) CONSTRAINT [DF_AS_Assets_MP_HIS_PO_Description] DEFAULT ('') NOT NULL,
    [Model_Number]           NVARCHAR (360) CONSTRAINT [DF_AS_Assets_MP_HIS_Model_Number] DEFAULT ('') NOT NULL,
    [Transaction_Date]       DATETIME       NULL,
    [Assets_Fixed_Cost]      NUMERIC (20)   NULL,
    [Deprn_Reserve]          NUMERIC (20)   NULL,
    [Salvage_Value]          NUMERIC (20)   NULL,
    [Currency]               VARCHAR (20)   CONSTRAINT [DF_AS_Assets_MP_HIS_Currency] DEFAULT ('') NOT NULL,
    [Description]            NVARCHAR (600) CONSTRAINT [DF_AS_Assets_MP_HIS_Description] DEFAULT ('') NOT NULL,
    [Post_Date]              DATETIME       NULL,
    [Import_Num]             VARCHAR (20)   CONSTRAINT [DF_AS_Assets_MP_HIS_Import_Num] DEFAULT ('') NOT NULL,
    [Create_Time]            DATETIME       CONSTRAINT [DF_AS_Assets_MP_HIS_CreateTime] DEFAULT (getdate()) NOT NULL,
    [Created_By]             VARCHAR (15)   CONSTRAINT [DF_AS_Assets_MP_HIS_CreatedBy] DEFAULT ('') NOT NULL,
    [Last_Updated_Time]      DATETIME       NULL,
    [Last_Updated_By]        VARCHAR (15)   CONSTRAINT [DF_AS_Assets_MP_HIS_UpdatedBy] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK__AS_Asset__3214EC270AEA5535] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�ʲ����v�O����', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'��ڽs��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'TRX_Header_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�겣�s��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Asset_Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'��겣�s��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Assets_Original_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�]���s��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Assets_Category_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�]���W��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Assets_Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�]���O�W', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Assets_Alias';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'���q', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Assets_Unit';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'���겣ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Assets_Parent_Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�ꤺ/���~�겣', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'IsOversea';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�겣����', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Asset_Category_Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'��m�a�I', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Location_Disp';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�@�Φ~���~��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Life_Years';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'���¤覡', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Deprn_Method_Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�ҥΤ��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Date_Placed_In_Service';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�O�޳��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Assigned_Branch';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�O�ޤH', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Assigned_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'���ʮ׸�', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'PO_Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'���ʴy�z', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'PO_Description';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�t�P�W��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Model_Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�禬���', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Transaction_Date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�겣����', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Assets_Fixed_Cost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�֭p����', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Deprn_Reserve';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�ݭ�', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Salvage_Value';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�K�n', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Description';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�X�b��/�L�b��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Post_Date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�إ߮ɶ�', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Create_Time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�إߤH��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Created_By';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�̫Ყ�ʮɶ�', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Last_Updated_Time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�̫Ყ�ʤH��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_HIS', @level2type = N'COLUMN', @level2name = N'Last_Updated_By';

