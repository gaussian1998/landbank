﻿CREATE TABLE [dbo].[AS_GL_Ledger] (
    [ID]                 INT             IDENTITY (1, 1) NOT NULL,
    [Book_Type]          VARCHAR (20)    DEFAULT ('') NOT NULL,
    [Account_Type]       CHAR (1)        DEFAULT ('') NOT NULL,
    [Account]            VARCHAR (12)    DEFAULT ('') NOT NULL,
    [Account_Name]       NVARCHAR (40)   DEFAULT ('') NOT NULL,
    [Asset_Liablty]      CHAR (1)        DEFAULT ('') NOT NULL,
    [Category]           CHAR (1)        DEFAULT ('') NOT NULL,
    [Currency]           VARCHAR (4)     DEFAULT ('') NOT NULL,
    [Branch_Code]        VARCHAR (3)     DEFAULT ('') NOT NULL,
    [Department]         VARCHAR (20)    DEFAULT ('') NOT NULL,
    [GL_Account]         VARCHAR (20)    DEFAULT ('') NOT NULL,
    [Close_Date]         DATE            NULL,
    [Stat_Frequency]     NUMERIC (6)     DEFAULT ((0)) NOT NULL,
    [Earliest_Post_Date] DATE            NULL,
    [Imbalance_Amount]   NUMERIC (17, 3) DEFAULT ((0)) NOT NULL,
    [Last_Balance]       NUMERIC (17, 3) DEFAULT ((0)) NOT NULL,
    [Next_Statement]     NUMERIC (9)     DEFAULT ((0)) NOT NULL,
    [Open_Date]          DATE            NULL,
    [Soft_Lock]          BIT             DEFAULT ((0)) NOT NULL,
    [Stat_Balance_Date]  DATE            NULL,
    [Status]             CHAR (1)        DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_AS_GL_Ledger] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Ledger1]
    ON [dbo].[AS_GL_Ledger]([Account] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Ledger2]
    ON [dbo].[AS_GL_Ledger]([Branch_Code] ASC, [Close_Date] ASC, [Account] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Ledger3]
    ON [dbo].[AS_GL_Ledger]([Close_Date] ASC, [Account] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Ledger4]
    ON [dbo].[AS_GL_Ledger]([GL_Account] ASC);
