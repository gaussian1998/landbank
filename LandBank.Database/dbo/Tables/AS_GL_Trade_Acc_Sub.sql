CREATE TABLE [dbo].[AS_GL_Trade_Acc_Sub] (
    [ID]                  INT          IDENTITY (1, 1) NOT NULL,
    [Master_No]           NUMERIC (6)  DEFAULT ((0)) NOT NULL,
    [Related_No]          NUMERIC (6)  DEFAULT ((0)) NOT NULL,
    [Account]             VARCHAR (12) DEFAULT ('') NOT NULL,
    [DB_CR]               CHAR (1)     DEFAULT ('') NOT NULL,
    [Asset_Category_code] VARCHAR (30) DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_AS_GL_Trade_Acc_Sub] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Trade_Acc_Sub1]
    ON [dbo].[AS_GL_Trade_Acc_Sub]([Master_No] ASC, [Related_No] ASC);


