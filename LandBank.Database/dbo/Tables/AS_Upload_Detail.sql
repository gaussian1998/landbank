﻿CREATE TABLE [dbo].[AS_Upload_Detail]
(
	[ID] INT NOT NULL IDENTITY , 
    [SubSystem_Code] VARCHAR(12) NULL, 
    [Path] NVARCHAR(255) NULL, 
    [File_Name] VARCHAR(100) NULL, 
    [Last_Updated_Time] DATETIME NULL, 
    [Last_Updated_By] INT NULL, 
    CONSTRAINT [PK_AS_Upload_Detail] PRIMARY KEY ([ID]) 
)

GO

CREATE INDEX [IX_AS_Upload_Detail_PostID] ON [dbo].[AS_Upload_Detail] ([SubSystem_Code])

GO

CREATE INDEX [IX_AS_Upload_Detail_PostID_FileName] ON [dbo].[AS_Upload_Detail] ([SubSystem_Code], [File_Name])
