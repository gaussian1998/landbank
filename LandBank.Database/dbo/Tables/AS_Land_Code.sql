CREATE TABLE [dbo].[AS_Land_Code] (
    [ID]                INT            IDENTITY (1, 1) NOT NULL,
    [City_Code]         VARCHAR (1)    DEFAULT ('') NOT NULL,
    [City_Name]         NVARCHAR (255) DEFAULT ('') NOT NULL,
    [District_Code]     VARCHAR (2)    DEFAULT ('') NOT NULL,
    [District_Name]     NVARCHAR (255) DEFAULT ('') NOT NULL,
    [Section_Code]      VARCHAR (4)    DEFAULT ('') NOT NULL,
    [Sectioni_Name]     NVARCHAR (255) DEFAULT ('') NOT NULL,
    [Sub_Sectioni_Name] NVARCHAR (255) DEFAULT ('') NOT NULL,
    [Remark]            NVARCHAR (255) DEFAULT ('') NOT NULL,
    [Office_Name]       NVARCHAR (255) DEFAULT ('') NOT NULL,
    [Office_Code]       VARCHAR (2)    DEFAULT ('') NOT NULL,
    [Office_Area_Code]  VARCHAR (4)    DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_AS_Land_Code] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Land_Code1]
    ON [dbo].[AS_Land_Code]([City_Code] ASC, [District_Code] ASC, [Section_Code] ASC);

