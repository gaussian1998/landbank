CREATE TABLE [dbo].[AS_Department] (
    [ID]                INT            IDENTITY (1, 1) NOT NULL,
    [Branch_Code]       VARCHAR (3)    NOT NULL,
    [Department]        VARCHAR (20)   NOT NULL,
    [Department_Name]   NVARCHAR (100) NULL,
    [Is_Active]         BIT            DEFAULT ((1)) NOT NULL,
    [Cancel_Code]       BIT            DEFAULT ((0)) NOT NULL,
    [Last_Updated_Time] DATETIME       NULL,
    [Last_Updated_By]   NUMERIC (15)   NULL,
    CONSTRAINT [PK_AS_Department] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_AS_Department1]
    ON [dbo].[AS_Department]([Branch_Code] ASC, [Department] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Department1]
    ON [dbo].[AS_Department]([Department] ASC);


