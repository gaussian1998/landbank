﻿CREATE TABLE [dbo].[AS_Branch_Currency] (
    [ID]                INT              IDENTITY (1, 1) NOT NULL,
    [Branch_Code]       NUMERIC (3)      NULL,
    [Department]        VARCHAR (20)     NULL,
    [Book_Type]         VARCHAR (20)     NULL,
    [Currency]          VARCHAR (4)      NULL,
    [Rate]              NUMERIC (14, 10) DEFAULT ((0)) NOT NULL,
    [Last_Updated_Time] DATETIME         NULL,
    [Last_Updated_By]   NUMERIC (15)     NULL,
    CONSTRAINT [PK_AS_Branch_Currency] PRIMARY KEY CLUSTERED ([ID] ASC)
);
