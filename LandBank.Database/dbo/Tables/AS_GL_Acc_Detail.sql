﻿CREATE TABLE [dbo].[AS_GL_Acc_Detail] (
    [ID]                INT             IDENTITY (1, 1) NOT NULL,
    [Book_Type]         VARCHAR (20)    DEFAULT ('') NOT NULL,
    [Branch_Code]       VARCHAR (3)     DEFAULT ('') NOT NULL,
    [Department]        VARCHAR (20)    DEFAULT ('') NOT NULL,
    [Branch_Ind]        CHAR (1)        DEFAULT ('') NOT NULL,
    [Account]           VARCHAR (12)    DEFAULT ('') NOT NULL,
    [Account_Name]      NVARCHAR (40)   DEFAULT ('') NOT NULL,
    [Asset_Liablty]     CHAR (1)        DEFAULT ('') NOT NULL,
    [Category]          CHAR (1)        DEFAULT ('') NOT NULL,
    [Currency]          VARCHAR (4)     DEFAULT ('') NOT NULL,
    [GL_Account]        VARCHAR (20)    DEFAULT ('') NOT NULL,
    [Master_No]         NUMERIC (6)     DEFAULT ((0)) NOT NULL,
    [Open_Date]         DATE            DEFAULT ('') NOT NULL,
    [Open_Type]         VARCHAR (3)     DEFAULT ('') NOT NULL,
    [Trans_Type]        CHAR (1)        DEFAULT ('') NOT NULL,
    [Trade_Type]        CHAR (3)        DEFAULT ('') NOT NULL,
    [Account_Key]       VARCHAR (10)    DEFAULT ('') NOT NULL,
    [Trans_Seq]         VARCHAR (30)    DEFAULT ('') NOT NULL,
    [Post_Seq]          INT             DEFAULT ((0)) NOT NULL,
    [DB_CR]             CHAR (1)        DEFAULT ('') NOT NULL,
    [Real_Memo]         CHAR (1)        DEFAULT ('') NOT NULL,
    [Stat_Balance_Date] DATE            DEFAULT ('') NULL,
    [Status]            CHAR (1)        DEFAULT ('') NOT NULL,
    [Asset_Number]      VARCHAR (30)    DEFAULT ('') NOT NULL,
    [Lease_Number]      VARCHAR (9)     DEFAULT ('') NOT NULL,
    [Virtual_Account]   VARCHAR (14)    DEFAULT ('') NOT NULL,
    [Payment_User]      NVARCHAR (60)   DEFAULT ('') NOT NULL,
    [Payment_User_ID]   VARCHAR (20)    DEFAULT ('') NOT NULL,
    [Batch_Seq]         VARCHAR (20)    DEFAULT ('') NOT NULL,
    [Import_Number]     VARCHAR (20)    DEFAULT ('') NOT NULL,
    [Soft_Lock]         BIT             DEFAULT ((0)) NOT NULL,
    [Close_Date]        DATE            DEFAULT ('') NULL,
    [Stat_Frequency]    NUMERIC (6)     DEFAULT ((0)) NOT NULL,
    [Post_Date]         DATE            DEFAULT ('') NULL,
    [Amount]            NUMERIC (15, 2) DEFAULT ((0)) NOT NULL,
    [Value_Date]        DATE            DEFAULT ('') NULL,
    [Settle_Date]       DATE            DEFAULT ('') NULL,
    [Notes]             NVARCHAR (100)  DEFAULT ('') NOT NULL,
    [Notes1]            NVARCHAR (100)  DEFAULT ('') NOT NULL,
    [Last_Updated_Time] DATETIME        DEFAULT ('') NOT NULL,
    [Last_Updated_By]   NUMERIC (15)    DEFAULT ((0)) NOT NULL,
    [Is_Updated]        BIT             DEFAULT ((0)) NOT NULL,
    [Updated_Notes]     NVARCHAR (100)  DEFAULT ('') NOT NULL,
    [EAI_Time]          DATETIME        NULL,
    [EAI_Seq]           VARCHAR (20)    DEFAULT ('') NOT NULL,
    [Deprn_Months]      VARCHAR (6)     DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_AS_GL_Acc_Detail] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Detail1]
    ON [dbo].[AS_GL_Acc_Detail]([Batch_Seq] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Detail2]
    ON [dbo].[AS_GL_Acc_Detail]([Account] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Detail3]
    ON [dbo].[AS_GL_Acc_Detail]([Open_Date] ASC, [Account] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Detail4]
    ON [dbo].[AS_GL_Acc_Detail]([Open_Date] ASC, [Branch_Code] ASC, [Account] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Detail5]
    ON [dbo].[AS_GL_Acc_Detail]([Branch_Code] ASC, [Open_Date] ASC, [Account] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Detail6]
    ON [dbo].[AS_GL_Acc_Detail]([Trans_Seq] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Detail7]
    ON [dbo].[AS_GL_Acc_Detail]([Post_Date] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Detail8]
    ON [dbo].[AS_GL_Acc_Detail]([Settle_Date] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Acc_Detail9]
    ON [dbo].[AS_GL_Acc_Detail]([Import_Number] ASC);

