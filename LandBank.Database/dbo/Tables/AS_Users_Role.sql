﻿CREATE TABLE [dbo].[AS_Users_Role] (
    [ID]                  INT         IDENTITY (1, 1) NOT NULL,
    [User_ID]             INT         NOT NULL,
    [Role_ID]             INT         NOT NULL,
    [Is_Enable]           BIT         DEFAULT ((0)) NOT NULL,
    [Applied_RoleFlow_ID] INT         NULL,
    [Branch_Code]         VARCHAR (3) NULL,
    [Is_Super_Role]       BIT         DEFAULT ((0)) NULL,
    [Department_ID]       INT         NULL,
    [Create_Time]         DATETIME    NULL,
    [Created_By]          INT         NULL,
    [Last_Updated_Time]   DATETIME    NULL,
    [Last_Updated_By]     INT         NULL,
    CONSTRAINT [PK_AS_Users_Role] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [UK_AS_Users_Role_UserID_Role_ID_IsEnable] UNIQUE NONCLUSTERED ([User_ID] ASC, [Role_ID] ASC, [Is_Enable] ASC, [Applied_RoleFlow_ID] ASC),
    CONSTRAINT [FK_AS_Users_Role_User_ID] FOREIGN KEY ([User_ID]) REFERENCES [dbo].[AS_Users] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_AS_Users_Role_ID] FOREIGN KEY ([Role_ID]) REFERENCES [dbo].[AS_Role] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_AS_Users_Role_RoleFlow_Header] FOREIGN KEY ([Applied_RoleFlow_ID]) REFERENCES [dbo].[AS_RoleFlow_Header] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Users_Role_BranchCode_UserID]
    ON [dbo].[AS_Users_Role]([Branch_Code] ASC, [User_ID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Users_Role_BranchCode_Department_UserID]
    ON [dbo].[AS_Users_Role]([Branch_Code] ASC, [Department_ID] ASC, [User_ID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Users_Role_UserID]
    ON [dbo].[AS_Users_Role]([User_ID] ASC);
