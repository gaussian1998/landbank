CREATE TABLE [dbo].[AS_Asset_Family] (
    [ID]                   INT          IDENTITY (1, 1) NOT NULL,
    [Layer]                NUMERIC (1)  DEFAULT ((0)) NOT NULL,
    [Family_Type]          NUMERIC (1)  NULL,
    [Main_Kind]            VARCHAR (1)  NULL,
    [Parent_Category_Code] VARCHAR (30) NULL,
    [Parent_ID]            INT          NULL,
    [Parent_Asset_Number]  VARCHAR (30) NULL,
    [Asset_Category_Code]  VARCHAR (30) NULL,
    [Asset_Number]         VARCHAR (30) NULL,
    [Deprn_Method]         VARCHAR (12) NULL,
    [Life_Years]           NUMERIC (3)  DEFAULT ((0)) NOT NULL,
    [Life_Months]          NUMERIC (6)  DEFAULT ((0)) NOT NULL,
    [Salvage_Value]        NUMERIC (6)  DEFAULT ((0)) NOT NULL,
    [Not_Deprn_Flag]       BIT          DEFAULT ((0)) NOT NULL,
    [Branch_Code]          NUMERIC (3)  NULL,
    [Department]           VARCHAR (20) NULL,
    [Last_Updated_Time]    DATETIME     NULL,
    [Last_Updated_By]      NUMERIC (15) NULL,
    CONSTRAINT [PK_AS_Asset_Family] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Asset_Family1]
    ON [dbo].[AS_Asset_Family]([Asset_Category_Code] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Asset_Family2]
    ON [dbo].[AS_Asset_Family]([Layer] ASC, [Parent_Asset_Number] ASC, [Asset_Number] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Asset_Family3]
    ON [dbo].[AS_Asset_Family]([Layer] ASC, [Parent_ID] ASC);

