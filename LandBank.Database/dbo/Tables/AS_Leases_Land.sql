﻿
CREATE TABLE [dbo].[AS_Leases_Land](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LEASE_HEADER_ID] [numeric](18, 0) NULL,
	[LEASE_NUMBER] [nvarchar](20) NULL,
	[LAND_ASSET_NUMBER] [varchar](30) NULL,
	[LAND_ASSET_ID] [numeric](18, 0) NULL,
	[CITY_NAME] [nvarchar](255) NULL,
	[DISTRICT_NAME] [nvarchar](255) NULL,
	[SECTION_NAME] [nvarchar](255) NULL,
	[SUBSECTION_NAME] [nvarchar](255) NULL,
	[PARENT_LAND_NUMBER] [nvarchar](30) NULL,
	[FILIAL_LAND_NUMBER] [nvarchar](30) NULL,
	[LAND_USE_TYPE] [nvarchar](30) NULL,
	[ANNOUNCE_PRICE] [numeric](18, 0) NULL,
	[AUTHORIZED_AREA] [numeric](18, 0) NULL,
	[LESSEE_AREA] [numeric](18, 0) NULL,
	[RENTAL_AMOUNT] [numeric](18, 0) NULL,
	[LEASE_RATE] [numeric](18, 0) NULL,
	[DISABILITY_LOWINCOME_FLAG] [bit] NULL,
	[DISABILITY_LOWINCOME_REDUCTION_AMOUNT] [int] NULL,
	[ARCADE_REDUCTION_AMOUNT] [bit] NULL,
	[ARCADE_DISABILITY_AMOUNT] [int] NULL,
	[DESCRIPTION] [nvarchar](250) NULL,
	[LAST_UPDATE_DATE] [date] NULL,
	[LAST_UPDATED_BY] [int] NULL,
	[CREATION_DATE] [date] NULL,
	[CREATED_BY] [numeric](15, 0) NULL,
	[LAST_UPDATE_LOGIN] [numeric](15, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
