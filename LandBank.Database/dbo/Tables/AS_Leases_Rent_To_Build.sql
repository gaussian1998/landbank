﻿CREATE TABLE [dbo].[AS_Leases_Rent_To_Build] (
    [ID]                INT           NOT NULL,
    [LEASE_HEADER_ID]   NUMERIC (18)  NULL,
    [DOORPLATE_LANE]    VARCHAR (400) NULL,
    [BUILDING_STRU]     VARCHAR (40)  NULL,
    [FLOOR]             NUMERIC (18)  NULL,
    [TOTAL_FLOOR]       NUMERIC (18)  NULL,
    [TOTAL_AREA]        NUMERIC (18)  NULL,
    [ARCADE_AREA]       NUMERIC (18)  NULL,
    [DESCRIPTION]       VARCHAR (250) NULL,
    [LAST_UPDATE_DATE]  DATE          NULL,
    [LAST_UPDATED_BY]   NUMERIC (15)  NULL,
    [CREATION_DATE]     DATE          NULL,
    [CREATED_BY]        NUMERIC (15)  NULL,
    [LAST_UPDATE_LOGIN] NUMERIC (15)  NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);
