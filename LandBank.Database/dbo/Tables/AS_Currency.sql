﻿CREATE TABLE [dbo].[AS_Currency] (
    [ID]             INT           IDENTITY (1, 1) NOT NULL,
    [Currency]       VARCHAR (4)   NULL,
    [Country]        VARCHAR (4)   NULL,
    [Ex_Rate_Type]   CHAR (1)      NULL,
    [Currency_Name]  NVARCHAR (10) NULL,
    [Swift_Currency] VARCHAR (4)   NULL,
    [Basis]          NUMERIC (3)   DEFAULT ((0)) NOT NULL,
    [Decimal]        NUMERIC (2)   DEFAULT ((0)) NOT NULL,
    [Soft_Lock]      NUMERIC (2)   DEFAULT ((0)) NOT NULL,
    [Truncation]     NUMERIC (2)   DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AS_Currency] PRIMARY KEY CLUSTERED ([ID] ASC)
);
