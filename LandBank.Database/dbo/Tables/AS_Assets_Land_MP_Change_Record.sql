CREATE TABLE [dbo].[AS_Assets_Land_MP_Change_Record] (
    [ID]                          INT            IDENTITY (1, 1) NOT NULL,
    [TRX_Header_ID]               VARCHAR (18)   CONSTRAINT [DF_AS_Assets_Land_MP_Change_Record_TRX_Header_ID] DEFAULT ('') NULL,
    [AS_Assets_Land_MP_Record_ID] INT            NULL,
    [UPD_File]                    VARCHAR (1)    NULL,
    [Field_Name]                  VARCHAR (240)  NULL,
    [Modify_Type]                 VARCHAR (240)  NULL,
    [Before_Data]                 NVARCHAR (600) NULL,
    [After_Data]                  NVARCHAR (600) NULL,
    [Create_Time]                 DATETIME       NULL,
    [Created_By]                  VARCHAR (15)   CONSTRAINT [DF_AS_Assets_Land_MP_Change_Record_Created_By] DEFAULT ('') NULL,
    [Created_By_Name]             NVARCHAR (100) NULL,
    [Last_UpDatetimed_Time]       DATETIME       NULL,
    [Last_UpDatetimed_By]         DECIMAL (15)   NULL,
    [Last_UpDatetimed_By_Name]    NVARCHAR (100) NULL,
    CONSTRAINT [PK_AS_Assets_Land_MP_Change_Record] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Change_Record', @level2type = N'COLUMN', @level2name = N'ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'單據編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Change_Record', @level2type = N'COLUMN', @level2name = N'TRX_Header_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'土地改良單據明細記錄檔ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Change_Record', @level2type = N'COLUMN', @level2name = N'AS_Assets_Land_MP_Record_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'異動檔案', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Change_Record', @level2type = N'COLUMN', @level2name = N'UPD_File';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'異動欄位', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Change_Record', @level2type = N'COLUMN', @level2name = N'Field_Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'異動狀態', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Change_Record', @level2type = N'COLUMN', @level2name = N'Modify_Type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'異動前資料', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Change_Record', @level2type = N'COLUMN', @level2name = N'Before_Data';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'異動後資料', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Change_Record', @level2type = N'COLUMN', @level2name = N'After_Data';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Change_Record', @level2type = N'COLUMN', @level2name = N'Create_Time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立人員 ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Change_Record', @level2type = N'COLUMN', @level2name = N'Created_By';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立人員姓名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Change_Record', @level2type = N'COLUMN', @level2name = N'Created_By_Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最後異動時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Change_Record', @level2type = N'COLUMN', @level2name = N'Last_UpDatetimed_Time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最後異動人員 ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Change_Record', @level2type = N'COLUMN', @level2name = N'Last_UpDatetimed_By';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最後異動人員姓名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Change_Record', @level2type = N'COLUMN', @level2name = N'Last_UpDatetimed_By_Name';

