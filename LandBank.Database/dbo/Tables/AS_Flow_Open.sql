﻿CREATE TABLE [dbo].[AS_Flow_Open] (
    [ID]                INT            IDENTITY (1, 1) NOT NULL,
    [Doc_Type]          NVARCHAR (40)  NULL,
    [Doc_No]            VARCHAR (4)    NULL,
    [Table_ID]          INT            NULL,
    [Last_ID]           INT            NULL,
    [Last_Reject_Memo]  NVARCHAR (60)  NULL,
    [Branch_Code]       VARCHAR (3)    NULL,
    [Flow_Code]         VARCHAR (10)   NULL,
    [Version]           NUMERIC (3)    NULL,
    [End_Point]         BIT            DEFAULT ((0)) NOT NULL,
    [Flow_Status]       NUMERIC (1)    DEFAULT ((0)) NOT NULL,
    [Gate_Others]       NUMERIC (15)   NULL,
    [Gate_Pass]         BIT            NULL,
    [Result]            NUMERIC (1)    NULL,
    [Audit_Opinion]     NVARCHAR (MAX) NULL,
    [Current_Role_ID]   VARCHAR (10)   NULL,
    [Current_User_ID]   INT            NULL,
    [Agent_User_ID]     NUMERIC (15)   NULL,
    [Reject_Role_ID]    VARCHAR (10)   NULL,
    [Reject_User_ID]    VARCHAR (10)   NULL,
    [Step_Role_ID]      INT            NULL,
    [Step_User_ID]      INT            NULL,
    [Form_Name]         VARCHAR (100)  NULL,
    [Form_Version]      NUMERIC (2)    NULL,
    [Mgr_Branch_Code]   NUMERIC (3)    NULL,
    [Mgr_Sub_Depart]    NVARCHAR (10)  NULL,
    [Last_Updated_Time] DATETIME       NULL,
    [Last_Updated_By]   NUMERIC (15)   NULL,
    [Form_No]           VARCHAR (256)  DEFAULT ('') NOT NULL,
    [Step_No]           NUMERIC (2)    NULL,
    [Issue_Time]        DATETIME       NOT NULL,
    [Event_Listener]    VARCHAR (50)   NOT NULL,
    [Carry]             NVARCHAR (MAX) NOT NULL,
    [Apply_User_ID]     INT            NOT NULL,
    [Agent_Flow_ID]     INT            NOT NULL,
    CONSTRAINT [PK_AS_Flow_Open] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_AS_Flow_Open_AS_Agent_Flow] FOREIGN KEY ([Agent_Flow_ID]) REFERENCES [dbo].[AS_Agent_Flow] ([ID]),
    CONSTRAINT [FK_AS_Flow_Open_AS_Users] FOREIGN KEY ([Apply_User_ID]) REFERENCES [dbo].[AS_Users] ([ID]),
    CONSTRAINT [FK_AS_Flow_Open_AS_Users_1] FOREIGN KEY ([Current_User_ID]) REFERENCES [dbo].[AS_Users] ([ID]),
    CONSTRAINT [FK_AS_Flow_Open_AS_Users_2] FOREIGN KEY ([Step_User_ID]) REFERENCES [dbo].[AS_Users] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_AS_Flow_Open1]
    ON [dbo].[AS_Flow_Open]([Form_No] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Open1]
    ON [dbo].[AS_Flow_Open]([Doc_Type] ASC, [Doc_No] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Open2]
    ON [dbo].[AS_Flow_Open]([Doc_No] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Open3]
    ON [dbo].[AS_Flow_Open]([Branch_Code] ASC, [Doc_No] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Open4]
    ON [dbo].[AS_Flow_Open]([Apply_User_ID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Open5]
    ON [dbo].[AS_Flow_Open]([Issue_Time] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Open6]
    ON [dbo].[AS_Flow_Open]([Current_User_ID] ASC);
