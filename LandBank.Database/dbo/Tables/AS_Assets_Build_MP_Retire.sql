USE [AS_LandBank]
GO

/****** Object:  Table [dbo].[AS_Assets_Build_MP_Retire]    Script Date: 2017/11/23 下午 04:59:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AS_Assets_Build_MP_Retire](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TRX_Header_ID] [int] NOT NULL,
	[Assets_Build_MP_ID] [int] NOT NULL,
	[Serial_Number] [numeric](20, 0) NOT NULL,
	[Asset_Number] [nvarchar](30) NOT NULL,
	[Parent_Asset_Number] [varchar](30) NULL,
	[Old_Cost] [numeric](18, 2) NULL,
	[Retire_Cost] [numeric](18, 2) NULL,
	[Sell_Amount] [numeric](18, 2) NULL,
	[Sell_Cost] [numeric](18, 2) NULL,
	[New_Cost] [numeric](18, 2) NULL,
	[Reval_Adjustment_Amount] [numeric](18, 2) NULL,
	[Reval_Reserve] [numeric](18, 2) NULL,
	[Reval_Land_VAT] [numeric](18, 2) NULL,
	[Reason_Code] [varchar](15) NULL,
	[Description] [nvarchar](600) NULL,
	[Account_Type] [int] NULL,
	[Receipt_Type] [numeric](18, 2) NULL,
	[Create_Time] [datetime] NULL,
	[Created_By] [numeric](15, 0) NULL,
	[Created_By_Name] [nvarchar](100) NULL,
	[Last_UpDatetimed_Time] [datetime] NULL,
	[Last_UpDatetimed_By] [numeric](15, 0) NULL,
	[Last_UpDatetimed_By_Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_AS_Assets_Build_MP_Retire] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'單據表頭ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'TRX_Header_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'重大組成主檔ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'Assets_Build_MP_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'項次' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'Serial_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'財產編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'Asset_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'母資產編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'Parent_Asset_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'帳面成本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'Old_Cost'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'處分成本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'Retire_Cost'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'變賣收入' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'Sell_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'變賣成本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'Sell_Cost'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'異動後成本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'New_Cost'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'未實現重估增值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'Reval_Adjustment_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'帳面金額-重估增值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'Reval_Reserve'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'估計應付土地增值稅' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'Reval_Land_VAT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'處分原因' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'Reason_Code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'摘要' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'Description'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'獲利/損失註記' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'Account_Type'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收款方式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Retire', @level2type=N'COLUMN',@level2name=N'Receipt_Type'
GO


