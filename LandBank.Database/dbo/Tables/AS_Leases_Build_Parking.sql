﻿
CREATE TABLE [dbo].[AS_Leases_Build_Parking](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LEASE_HEADER_ID] [numeric](18, 0) NULL,
	[LEASE_NUMBER] [nvarchar](20) NULL,
	[LEASE_BUILD_ID] [numeric](18, 0) NULL,
	[PARKING_NO] [nvarchar](30) NULL,
	[PARKING_RENTAL] [numeric](18, 0) NULL,
	[DATE_FROM] [date] NULL,
	[DATE_TO] [date] NULL,
	[CITY_NAME]	nvarchar(50) NULL,
	[DISTRICT_NAME] nvarchar(50) NULL,
	[SECTION_NAME] nvarchar(50) NULL,
	[SUBSECTION_NAME] nvarchar(50) NULL,	
	[ADDRESS] [nvarchar](100) NULL,
	[PARKING_USE_TYPE] nvarchar(15) NULL,
	[FLOOR] nvarchar(15) NULL,
	[DESCRIPTION] [nvarchar](250) NULL,
	[LAST_UPDATE_DATE] [date] NULL,
	[LAST_UPDATED_BY] [numeric](15, 0) NULL,
	[CREATION_DATE] [date] NULL,
	[CREATED_BY] [numeric](15, 0) NULL,
	[LAST_UPDATE_LOGIN] [numeric](15, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
