CREATE TABLE [dbo].[AS_Assets_ReNew_Record](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TRX_Header_ID] [varchar](18) NOT NULL,
	[Asset_Number] [varchar](30) NOT NULL,
	[Current_units] [numeric](20, 0) NOT NULL,
	[Datetime_Placed_In_Service] [datetime] NOT NULL,
	[Old_Asset_Number] [varchar](30) NOT NULL,
	[Asset_Category_code] [nvarchar](30) NOT NULL,
	[Location_Disp] [varchar](30) NOT NULL,
	[Description] [nvarchar](600) NOT NULL,
	[Urban_Renewal] [varchar](225) NOT NULL,
	[City_Code] [varchar](20) NOT NULL,
	[District_Code] [varchar](20) NOT NULL,
	[Section_Code] [varchar](20) NOT NULL,
	[Sub_Section_Name] [nvarchar](255) NOT NULL,
	[Parent_Land_Number] [varchar](20) NOT NULL,
	[Filial_Land_Number] [varchar](20) NOT NULL,
	[Authorization_Number] [nvarchar](600) NOT NULL,
	[Authorized_name] [nvarchar](150) NOT NULL,
	[Area_Size] [numeric](20, 0) NOT NULL,
	[Authorized_Scope_Molecule] [numeric](20, 0) NOT NULL,
	[Authorized_Scope_Denomminx] [numeric](20, 0) NOT NULL,
	[Authorized_Area] [numeric](20, 2) NOT NULL,
	[Used_Type] [varchar](4) NOT NULL,
	[Used_Status] [varchar](4) NOT NULL,
	[Obtained_Method] [varchar](4) NOT NULL,
	[Used_Partition] [varchar](20) NOT NULL,
	[Is_Marginal_Land] [char](1) NOT NULL,
	[Own_Area] [numeric](20, 2) NOT NULL,
	[NONOwn_Area] [numeric](20, 2) NOT NULL,
	[Original_Cost] [numeric](20, 2) NOT NULL,
	[Deprn_Amount] [numeric](20, 2) NOT NULL,
	[Current_Cost] [numeric](20, 2) NOT NULL,
	[Reval_Adjustment_Amount] [numeric](20, 2) NOT NULL,
	[Reval_Land_VAT] [numeric](20, 2) NOT NULL,
	[Reval_Reserve] [numeric](20, 2) NOT NULL,
	[Business_Area_Size] [numeric](20, 2) NOT NULL,
	[Business_Book_Amount] [numeric](20, 2) NOT NULL,
	[Business_Reval_Reserve] [numeric](20, 2) NOT NULL,
	[NONBusiness_Area_Size] [numeric](20, 2) NOT NULL,
	[NONBusiness_Book_Amount] [numeric](20, 2) NOT NULL,
	[NONBusiness_Reval_Reserve] [numeric](20, 2) NOT NULL,
	[Year_Number] [numeric](3, 0) NOT NULL,
	[Announce_Amount] [numeric](20, 2) NOT NULL,
	[Announce_Price] [numeric](20, 2) NOT NULL,
	[Tax_Type] [varchar](4) NOT NULL,
	[Reduction_reason] [varchar](4) NOT NULL,
	[Transfer_Price] [numeric](20, 2) NOT NULL,
	[Reduction_Area] [numeric](20, 2) NOT NULL,
	[Declared_Price] [numeric](20, 2) NOT NULL,
	[Dutiable_Amount] [numeric](20, 2) NOT NULL,
	[Remark1] [nvarchar](600) NOT NULL,
	[Remark2] [nvarchar](600) NOT NULL,
	[Retire_Cost] [numeric](20, 2) NOT NULL,
	[Sell_Amount] [numeric](20, 2) NOT NULL,
	[Sell_Cost] [numeric](20, 2) NOT NULL,
	[Delete_Reason] [varchar](30) NOT NULL,
	[Land_Item] [varchar](20) NOT NULL,
	[CROP] [nvarchar](2) NOT NULL,
	[ReNewPart] [varchar](50) NOT NULL,
	[Create_Time] [datetime] NULL,
	[Created_By] [varchar](15) NOT NULL,
	[Last_Updated_Time] [datetime] NULL,
	[Last_Updated_By] [varchar](15) NOT NULL,
 CONSTRAINT [PK_AS_Assets_ReNew_Record] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_TRX_Heard_ID]  DEFAULT ('') FOR [TRX_Header_ID]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Old_Asset_Number]  DEFAULT ('') FOR [Old_Asset_Number]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Asset_Category_code]  DEFAULT ('') FOR [Asset_Category_code]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Location_Disp]  DEFAULT ('') FOR [Location_Disp]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Description]  DEFAULT ('') FOR [Description]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Urban_Renewal]  DEFAULT ('') FOR [Urban_Renewal]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Sub_Section_Name]  DEFAULT ('') FOR [Sub_Section_Name]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Authorization_Number]  DEFAULT ('') FOR [Authorization_Number]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Authorized_name]  DEFAULT ('') FOR [Authorized_name]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Reduction_reason]  DEFAULT ('') FOR [Reduction_reason]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Remark1]  DEFAULT ('') FOR [Remark1]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Remark2]  DEFAULT ('') FOR [Remark2]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Retire_Cost]  DEFAULT ((0)) FOR [Retire_Cost]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Sell_Amount]  DEFAULT ((0)) FOR [Sell_Amount]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Sell_Cost]  DEFAULT ((0)) FOR [Sell_Cost]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Delete_Reason]  DEFAULT ('') FOR [Delete_Reason]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Land_Item]  DEFAULT ('') FOR [Land_Item]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_CROP]  DEFAULT ('') FOR [CROP]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_ReNewPart]  DEFAULT ('') FOR [ReNewPart]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Created_By]  DEFAULT ('') FOR [Created_By]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Record] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Record_Last_Updated_By]  DEFAULT ('') FOR [Last_Updated_By]
GO


