
CREATE TABLE [dbo].[AS_Assets_Land_Change](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TRX_Header_ID] [varchar](18) NULL CONSTRAINT [DF_AS_Assets_Land_Change_TRX_Header_ID]  DEFAULT (''),
	[AS_Assets_Land_ID] [int] NULL,
	[UPD_File] [varchar](1) NULL,
	[Field_Name] [varchar](240) NULL,
	[Modify_Type] [varchar](240) NULL,
	[Before_Data] [nvarchar](600) NULL,
	[After_Data] [nvarchar](600) NULL,
	[Create_Time] [datetime] NULL,
	[Created_By] [varchar](15) NULL CONSTRAINT [DF_AS_Assets_Land_Change_Created_By]  DEFAULT (''),
	[Created_By_Name] [nvarchar](100) NULL,
	[Last_UpDatetimed_Time] [datetime] NULL,
	[Last_UpDatetimed_By] [decimal](15, 0) NULL,
	[Last_UpDatetimed_By_Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_AS_Assets_Land_Change] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Change', @level2type=N'COLUMN',@level2name=N'ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'單據編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Change', @level2type=N'COLUMN',@level2name=N'TRX_Header_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'土地主檔ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Change', @level2type=N'COLUMN',@level2name=N'AS_Assets_Land_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'異動檔案' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Change', @level2type=N'COLUMN',@level2name=N'UPD_File'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'異動欄位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Change', @level2type=N'COLUMN',@level2name=N'Field_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'異動狀態' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Change', @level2type=N'COLUMN',@level2name=N'Modify_Type'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'異動前資料' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Change', @level2type=N'COLUMN',@level2name=N'Before_Data'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'異動後資料' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Change', @level2type=N'COLUMN',@level2name=N'After_Data'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Change', @level2type=N'COLUMN',@level2name=N'Create_Time'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立人員 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Change', @level2type=N'COLUMN',@level2name=N'Created_By'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立人員姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Change', @level2type=N'COLUMN',@level2name=N'Created_By_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後異動時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Change', @level2type=N'COLUMN',@level2name=N'Last_UpDatetimed_Time'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後異動人員 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Change', @level2type=N'COLUMN',@level2name=N'Last_UpDatetimed_By'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後異動人員姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Change', @level2type=N'COLUMN',@level2name=N'Last_UpDatetimed_By_Name'
GO


