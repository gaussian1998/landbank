﻿CREATE TABLE [dbo].[AS_Flow_Deputy] (
    [ID]         INT           IDENTITY (1, 1) NOT NULL,
    [txdate]     DATE          NULL,
    [Department] NVARCHAR (40) NULL,
    [UserID]     NUMERIC (15)  NULL,
    [DeputyID]   NUMERIC (15)  NULL,
    [StartTime]  DATETIME      NULL,
    [EndTime]    DATETIME      NULL,
    CONSTRAINT [PK_AS_Flow_Deputy] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Deputy_txdate_UserID_StartTime]
    ON [dbo].[AS_Flow_Deputy]([txdate] ASC, [UserID] ASC, [StartTime] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Deputy_txdate_DeputyID]
    ON [dbo].[AS_Flow_Deputy]([txdate] ASC, [DeputyID] ASC);
