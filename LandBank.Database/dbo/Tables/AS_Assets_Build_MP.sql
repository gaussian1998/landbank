

CREATE TABLE [dbo].[AS_Assets_Build_MP](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Asset_Build_ID] [int] NOT NULL,
	[Asset_Number] [varchar](30) NOT NULL,
	[Parent_Asset_Number] [varchar](30) NOT NULL,
	[Asset_Category_Code] [varchar](30) NOT NULL,
	[Old_Asset_Number] [varchar](30) NOT NULL,
	[Book_Type] [varchar](15) NOT NULL,
	[Date_Placed_In_Service] [datetime] NULL,
	[Current_Units] [numeric](20, 0) NOT NULL,
	[Deprn_Method_Code] [varchar](100) NOT NULL,
	[Life_Years] [numeric](20, 0) NOT NULL,
	[Life_Months] [numeric](20, 0) NOT NULL,
	[Location_Disp] [varchar](100) NOT NULL,
	[Description] [nvarchar](600) NOT NULL,
	[PO_Number] [nvarchar](30) NOT NULL,
	[PO_Destination] [nvarchar](250) NOT NULL,
	[Model_Number] [nvarchar](360) NOT NULL,
	[Transaction_Date] [datetime] NOT NULL,
	[Assets_Unit] [nvarchar](30) NOT NULL,
	[Accessory_Equipment] [nvarchar](600) NOT NULL,
	[Assigned_NUM] [nvarchar](30) NOT NULL,
	[Asset_Category_NUM] [nvarchar](30) NOT NULL,
	[Asset_Category_Name] [nvarchar](600) NOT NULL,
	[Asset_Structure] [nvarchar](50) NOT NULL,
	[Current_Cost] [numeric](18, 2) NOT NULL,
	[Salvage_Value] [numeric](18, 2) NOT NULL,
	[Deprn_Reserve] [numeric](18, 2) NOT NULL,
	[Remark] [nvarchar](500) NOT NULL,
	[Deprn_Type] [int] NULL,
	[Not_Deprn_Flag] [bit] NOT NULL,
	[Create_Time] [datetime] NOT NULL,
	[Created_By] [numeric](15, 0) NOT NULL,
	[Created_By_Name] [nvarchar](100) NULL,
	[Last_UpDatetimed_Time] [datetime] NULL,
	[Last_UpDatetimed_By] [numeric](15, 0) NOT NULL,
	[Last_UpDatetimed_By_Name] [nvarchar](100) NULL,
	[Officer_Branch] [varchar](30) NOT NULL,
	[DEPRN_Counts] [int] NULL,
	[Deprn_Amount] [numeric](20, 2) NOT NULL,
 CONSTRAINT [PK_AS_Assets_Build_MP] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Asset_Build_ID]  DEFAULT ((0)) FOR [Asset_Build_ID]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Asset_Number]  DEFAULT ('') FOR [Asset_Number]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Parent_Asset_Number]  DEFAULT ('') FOR [Parent_Asset_Number]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Asset_Category_Code]  DEFAULT ('') FOR [Asset_Category_Code]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Old_Asset_Number]  DEFAULT ('') FOR [Old_Asset_Number]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Book_Type]  DEFAULT ('') FOR [Book_Type]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Current_Units]  DEFAULT ((0)) FOR [Current_Units]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Deprn_Method_Code]  DEFAULT ('STL') FOR [Deprn_Method_Code]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Life_Years]  DEFAULT ((0)) FOR [Life_Years]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Life_Months]  DEFAULT ((0)) FOR [Life_Months]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Location_Disp]  DEFAULT ('') FOR [Location_Disp]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Description]  DEFAULT ('') FOR [Description]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_PO_Number]  DEFAULT ('') FOR [PO_Number]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_PO_Destination]  DEFAULT ('') FOR [PO_Destination]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Model_Number]  DEFAULT ('') FOR [Model_Number]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Assets_Unit]  DEFAULT ('') FOR [Assets_Unit]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Accessory_Equipment]  DEFAULT ('') FOR [Accessory_Equipment]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Assigned_NUM]  DEFAULT ('') FOR [Assigned_NUM]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Asset_Category_NUM]  DEFAULT ('') FOR [Asset_Category_NUM]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Asset_Category_Name]  DEFAULT ('') FOR [Asset_Category_Name]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Asset_Structure]  DEFAULT ('') FOR [Asset_Structure]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Current_Cost]  DEFAULT ((0)) FOR [Current_Cost]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Salvage_Value]  DEFAULT ((0)) FOR [Salvage_Value]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Deprn_Reserve]  DEFAULT ((0)) FOR [Deprn_Reserve]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Asset_Remark]  DEFAULT ('') FOR [Remark]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Not_Deprn_Flag]  DEFAULT ((0)) FOR [Not_Deprn_Flag]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Create_Time]  DEFAULT (getdate()) FOR [Create_Time]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Created_By]  DEFAULT ((0)) FOR [Created_By]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Last_UpDatetimed_By]  DEFAULT ((0)) FOR [Last_UpDatetimed_By]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  DEFAULT ('') FOR [Officer_Branch]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP] ADD  DEFAULT ((0)) FOR [Deprn_Amount]
GO


