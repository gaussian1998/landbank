
CREATE TABLE [dbo].[AS_Assets_Land_Retire](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TRX_Header_ID] [nvarchar](20) NOT NULL,
	[Assets_Land_ID] [nvarchar](20) NOT NULL,
	[Serial_Number] [nvarchar](20) NOT NULL,
	[Asset_Number] [nvarchar](30) NOT NULL,
	[Parent_Asset_Number] [varchar](30) NULL,
	[City_Code] [varchar](20) NULL,
	[City_Name] [nvarchar](255) NULL,
	[District_Code] [varchar](20) NULL,
	[District_Name] [nvarchar](255) NULL,
	[Section_Code] [varchar](20) NULL,
	[Section_Name] [nvarchar](255) NULL,
	[Sub_Section_Name] [nvarchar](255) NULL CONSTRAINT [DF_AS_Assets_Land_Retire_Sub_Section_Name]  DEFAULT (''),
	[Parent_Land_Number] [varchar](20) NULL,
	[Filial_Land_Number] [varchar](20) NULL,
	[Build_Address] [nvarchar](1200) NULL,
	[Build_Number] [varchar](8) NULL,
	[Old_Cost] [decimal](20, 2) NULL,
	[Retire_Cost] [decimal](20, 2) NULL,
	[Sell_Amount] [decimal](20, 2) NULL,
	[Sell_Cost] [decimal](20, 2) NULL,
	[New_Cost] [decimal](20, 2) NULL,
	[Reval_Adjustment_Amount] [decimal](20, 2) NULL,
	[Reval_Reserve] [decimal](20, 2) NULL,
	[Reval_Land_VAT] [decimal](20, 2) NULL,
	[Reason_Code] [varchar](15) NULL,
	[Description] [nvarchar](600) NULL,
	[Account_Type] [int] NULL,
	[Receipt_Type] [int] NULL,
	[Retire_Cost_Account] [varchar](300) NULL,
	[Sell_Amount_Account] [varchar](300) NULL,
	[Sell_Cost_Account] [varchar](300) NULL,
	[Reval_Adjustment_Amount_Account] [varchar](300) NULL,
	[Reval_Reserve_Account] [varchar](300) NULL,
	[Reval_Land_VAT_Account] [varchar](300) NULL,
	[Create_Time] [datetime] NULL,
	[Created_By] [varchar](15) NULL CONSTRAINT [DF_AS_Assets_Land_Retire_Created_By]  DEFAULT (''),
	[Created_By_Name] [nvarchar](100) NULL,
	[Last_UpDatetimed_Time] [datetime] NULL,
	[Last_UpDatetimed_By] [decimal](15, 0) NULL,
	[Last_UpDatetimed_By_Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_AS_Assets_Land_Retire] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'單據表頭SUID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'TRX_Header_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'土地主檔ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Assets_Land_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'項次' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Serial_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'財產編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Asset_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'母資產編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Parent_Asset_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-縣市代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'City_Code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-縣市名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'City_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-鄉鎮市區代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'District_Code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-鄉鎮市區名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'District_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-段代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Section_Code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-段名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Section_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-小段名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Sub_Section_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'土地地號-母地號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Parent_Land_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'土地地號-子地號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Filial_Land_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'房屋門牌' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Build_Address'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'房屋建號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Build_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'帳面成本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Old_Cost'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'處分成本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Retire_Cost'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'變賣收入' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Sell_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'變賣成本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Sell_Cost'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'異動後成本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'New_Cost'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'未實現重估增值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Reval_Adjustment_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'帳面金額-重估增值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Reval_Reserve'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'估計應付土地增值稅' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Reval_Land_VAT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'處分原因' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Reason_Code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'摘要' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Description'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'獲利/損失註記' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Account_Type'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收款方式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Receipt_Type'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'處分成本_會計科目' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Retire_Cost_Account'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'變賣收入_會計科目' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Sell_Amount_Account'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'變賣成本_會計科目' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Sell_Cost_Account'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'未實現重估增值_會計科目' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Reval_Adjustment_Amount_Account'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reval_Reserve _Account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Reval_Reserve_Account'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reval_Land_VAT _Account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Reval_Land_VAT_Account'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Create_Time'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立人員 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Created_By'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立人員姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Created_By_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後異動時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Last_UpDatetimed_Time'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後異動人員 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Last_UpDatetimed_By'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後異動人員姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Retire', @level2type=N'COLUMN',@level2name=N'Last_UpDatetimed_By_Name'
GO


