﻿
CREATE TABLE [dbo].[AS_Leases_Build](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LEASE_HEADER_ID] [numeric](18, 0) NULL,
	[LEASE_NUMBER] [nvarchar](20) NULL,
	[BUILD_ASSET_NUMBER] [varchar](30) NULL,
	[BUILD_ASSET_ID] [numeric](18, 0) NULL,
	[BUSINESS_USE_TYPE] [nvarchar](50) NULL,
	[CITY_NAME] [nvarchar](255) NULL,
	[DISTRICT_NAME] [nvarchar](255) NULL,
	[SECTION_NAME] [nvarchar](255) NULL,
	[SUBSECTION_NAME] [nvarchar](255) NULL,
	[AUTHORIZATION_NUMBER] [varchar](600) NULL,
	[BUILD_NUMBER] [nvarchar](150) NULL,
	[DOORPLATE_ROAD] [nvarchar](150) NULL,
	[DOORPLATE_LANE] [nvarchar](150) NULL,
	[RIGHT_AREA] nvarchar(20) NULL,
	[LEASES_AREA] nvarchar(20) NULL,
	[ARCADE_AREA] nvarchar(20) NULL,
	[FLOOR] nvarchar(10) NULL,	
	[AUTHORIZED_AREA] [numeric](18, 0) NULL,
	[RENTAL_AREA] [numeric](18, 0) NULL,
	[COMMON_PART] [nvarchar](50) NULL,
	[ADDRESS] [nvarchar](100) NULL,
	[SHARE_PUBLIC_AREA] [numeric](18, 0) NULL,
	[ALLOCATION_PUBLIC_AREA] [numeric](18, 0) NULL,
	[ATTACHED_BUILD] [nvarchar](150) NULL,
	[HOUSE_ARCADE] [nvarchar](150) NULL,
	[ARCADE] [nvarchar](150) NULL,
	[CONVEX] [nvarchar](150) NULL,
	[DESCRIPTION] [nvarchar](250) NULL,
	[LAST_UPDATE_DATE] [date] NULL,
	[LAST_UPDATED_BY] [numeric](15, 0) NULL,
	[CREATION_DATE] [date] NULL,
	[CREATED_BY] [numeric](15, 0) NULL,
	[LAST_UPDATE_LOGIN] [numeric](15, 0) NULL	
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO