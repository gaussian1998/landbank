CREATE TABLE [dbo].[AS_Assets_Build_MP_Record](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Assets_Build_MP_ID] [int] NOT NULL,
	[TRX_Header_ID] [int] NOT NULL,
	[Asset_Number] [varchar](30) NOT NULL,
	[Parent_Asset_Number] [varchar](30) NULL,
	[Asset_Category_Code] [varchar](30) NULL,
	[Old_Asset_Number] [varchar](30) NULL,
	[Book_Type] [varchar](15) NOT NULL,
	[Date_Placed_In_Service] [datetime] NULL,
	[Current_Units] [numeric](20, 2) NULL,
	[Deprn_Method_Code] [varchar](30) NOT NULL,
	[Life_Years] [numeric](20, 0) NULL,
	[Life_Months] [numeric](20, 0) NULL,
	[Location_Disp] [varchar](100) NULL,
	[Description] [nvarchar](600) NULL,
	[PO_Number] [nvarchar](30) NULL,
	[PO_Destination] [nvarchar](250) NULL,
	[Model_Number] [nvarchar](360) NULL,
	[Transaction_Date] [datetime] NULL,
	[Assets_Unit] [nvarchar](30) NULL,
	[Accessory_Equipment] [nvarchar](600) NULL,
	[Assigned_NUM] [varchar](30) NULL,
	[Asset_Category_NUM] [nvarchar](30) NULL,
	[Asset_Structure] [varchar](20) NULL,
	[Current_Cost] [numeric](18, 2) NULL,
	[Deprn_Reserve] [numeric](18, 2) NULL,
	[Salvage_Value] [numeric](18, 2) NULL,
	[Remark] [nvarchar](500) NULL,
	[Create_Time] [datetime] NULL,
	[Created_By] [numeric](15, 0) NULL,
	[Created_By_Name] [nvarchar](100) NULL,
	[Last_UpDatetimed_Time] [datetime] NULL,
	[Last_UpDatetimed_By] [numeric](15, 0) NULL,
	[Last_UpDatetimed_By_Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_AS_Assets_Build_MP_Record] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AS_Assets_Build_MP_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Record_Asset_Build_ID]  DEFAULT ((0)) FOR [Assets_Build_MP_ID]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Record_TRX_Heard_ID]  DEFAULT ((0)) FOR [TRX_Header_ID]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Record_Asset_Number]  DEFAULT ('') FOR [Asset_Number]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Record_Book_Type]  DEFAULT ('RE_IFRS') FOR [Book_Type]
GO

ALTER TABLE [dbo].[AS_Assets_Build_MP_Record] ADD  CONSTRAINT [DF_AS_Assets_Build_MP_Record_Deprn_Method_Code]  DEFAULT ('STL') FOR [Deprn_Method_Code]
GO
