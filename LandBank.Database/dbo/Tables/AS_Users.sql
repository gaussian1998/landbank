﻿CREATE TABLE [dbo].[AS_Users] (
    [ID]                INT            IDENTITY (1, 1) NOT NULL,
    [User_Code]         NVARCHAR (10)  NOT NULL,
    [User_Name]         NVARCHAR (100) NOT NULL,
    [Branch_Code]       VARCHAR (3)    NULL,
    [Branch_Name]       NVARCHAR (100) NULL,
    [Department_Code]   VARCHAR (30)   NULL,
    [Department_Name]   NVARCHAR (100) NULL,
    [Department_ID]     INT            NULL,
    [Office_Branch]     VARCHAR (3)    NULL,
    [Is_Active]         BIT            DEFAULT ((0)) NULL,
    [Quit_Date]         DATETIME       NULL,
    [Email]             VARCHAR (100)  NULL,
    [Notes]             TEXT           NULL,
    [Created_By]        INT            NULL,
    [Last_Updated_Time] DATETIME       NULL,
    [Last_Updated_By]   INT            NULL,
    [Delete_Flag]       BIT            DEFAULT ((0)) NULL,
	[Title]             VARCHAR (20)   NOT NULL,
	[Sub_Dept_Code]     VARCHAR (20)   NOT NULL,
    CONSTRAINT [PK_AS_Users] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [UK_AS_Users_User_ID] UNIQUE NONCLUSTERED ([User_Code] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Users_Department_UserID]
    ON [dbo].[AS_Users]([Department_ID] ASC);
