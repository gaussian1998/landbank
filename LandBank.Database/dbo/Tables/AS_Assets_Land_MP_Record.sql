CREATE TABLE [dbo].[AS_Assets_Land_MP_Record] (
    [ID]                         INT             IDENTITY (1, 1) NOT NULL,
    [Assets_Land_MP_ID]          NUMERIC (20)    NOT NULL,
    [TRX_Header_ID]              VARCHAR (18)    CONSTRAINT [DF_AS_Assets_Land_MP_Record_TRX_Header_ID] DEFAULT ('') NOT NULL,
    [Asset_Number]               VARCHAR (30)    NOT NULL,
    [Parent_Asset_Number]        VARCHAR (30)    NOT NULL,
    [Asset_Category_code]        NVARCHAR (30)   CONSTRAINT [DF_AS_Assets_Land_MP_Record_Asset_Category_code] DEFAULT ('') NOT NULL,
    [Old_Asset_Number]           VARCHAR (30)    CONSTRAINT [DF_AS_Assets_Land_MP_Record_Old_Asset_Number] DEFAULT ('') NULL,
    [Datetime_Placed_In_Service] DATETIME        NOT NULL,
    [Current_units]              NUMERIC (20)    NULL,
    [Deprn_Method_Code]          VARCHAR (12)    NOT NULL,
    [Life_Years]                 NUMERIC (20)    NULL,
    [Life_Months]                NUMERIC (20)    NULL,
    [Location_Disp]              VARCHAR (30)    CONSTRAINT [DF_AS_Assets_Land_MP_Record_Location_Disp] DEFAULT ('') NULL,
    [PO_Number]                  NVARCHAR (30)   NULL,
    [PO_Destination]             NVARCHAR (250)  NULL,
    [Model_Number]               NVARCHAR (360)  NULL,
    [Transaction_Date]           DATE            NULL,
    [Assets_Unit]                NVARCHAR (30)   CONSTRAINT [DF_AS_Assets_Land_MP_Record_Assets_Unit] DEFAULT (N'財物單位') NULL,
    [Accessory_Equipment]        NVARCHAR (600)  NULL,
    [Assigned_NUM]               NVARCHAR (30)   NULL,
    [Asset_Category_NUM]         NVARCHAR (30)   NULL,
    [Description]                NVARCHAR (600)  NULL,
    [Asset_Structure]            VARCHAR (2)     NULL,
    [Current_Cost]               NUMERIC (18, 2) NULL,
    [Deprn_Reserve]              NUMERIC (18, 2) NULL,
    [Salvage_Value]              NUMERIC (18, 2) NULL,
    [Remark]                     NVARCHAR (240)  NULL,
    [Deprn_Type]                 INT             NULL,
    [Create_Time]                DATETIME        NULL,
    [Created_By]                 DECIMAL (15)    NULL,
    [Created_By_Name]            NVARCHAR (100)  NULL,
    [Last_UpDatetimed_Time]      DATETIME        NULL,
    [Last_UpDatetimed_By]        DECIMAL (15)    NULL,
    [Last_UpDatetimed_By_Name]   NVARCHAR (100)  NULL,
    CONSTRAINT [PK_AS_Assets_Land_MP_Record] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'土地改良主檔ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Assets_Land_MP_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'單據編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'TRX_Header_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'財產編號(資產編號)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Asset_Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'母資產編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Parent_Asset_Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產分類', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Asset_Category_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'原資產編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Old_Asset_Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'啟用日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Datetime_Placed_In_Service';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'單位量', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Current_units';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'折舊方式', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Deprn_Method_Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'耐用年限年數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Life_Years';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'耐用年限月數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Life_Months';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'放置地點', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Location_Disp';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'採購案號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'PO_Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'採購描述', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'PO_Destination';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'廠牌規格', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Model_Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'驗收日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Transaction_Date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'附屬設備', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Accessory_Equipment';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'保管人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Assigned_NUM';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'財物編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Asset_Category_NUM';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'財物名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Description';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產結構', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Asset_Structure';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產成本', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Current_Cost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'累計折舊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Deprn_Reserve';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殘值', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Salvage_Value';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'備註', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Remark';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'折舊註記', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Deprn_Type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Create_Time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立人員 ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Created_By';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立人員姓名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Created_By_Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最後異動時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Last_UpDatetimed_Time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最後異動人員 ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Last_UpDatetimed_By';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最後異動人員姓名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_Land_MP_Record', @level2type = N'COLUMN', @level2name = N'Last_UpDatetimed_By_Name';

