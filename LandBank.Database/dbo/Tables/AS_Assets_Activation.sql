CREATE TABLE [dbo].[AS_Assets_Activation] (
    [ID]                BIGINT         NOT NULL,
    [ParantID]          BIGINT         NULL,
    [Document_Type]     BIGINT         NULL,
    [Source_Type]       INT            NULL,
    [Activation_No]     VARCHAR (17)   NULL,
    [BOOK_TYPE_CODE]    VARCHAR (20)   NULL,
    [BeginDate]         DATETIME       NULL,
    [EndDate]           DATETIME       NULL,
    [Jurisdiction1]     VARCHAR (200)  NULL,
    [Jurisdiction2]     VARCHAR (200)  NULL,
    [Jurisdiction3]     VARCHAR (200)  NULL,
    [Type]              INT            NULL,
    [Activation_Target] BIGINT         NULL,
    [TEL]               VARCHAR (100)  NULL,
    [Email]             VARCHAR (100)  NULL,
    [Remark]            NVARCHAR (MAX) NULL,
    [Create_Time]       DATETIME       NULL,
    [Created_By]        INT            NULL,
    [Last_Updated_Time] DATETIME       NULL,
    [Last_Updated_By]   INT            NULL,
    CONSTRAINT [PK_AS_Assets_Activation] PRIMARY KEY CLUSTERED ([ID] ASC)
);

