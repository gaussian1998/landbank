CREATE TABLE [dbo].[AS_Assets_MP_Record] (
    [ID]                INT          IDENTITY (1, 1) NOT NULL,
    [TRX_Header_ID]     VARCHAR (18) NOT NULL,
    [Asset_Number]      VARCHAR (30) NOT NULL,
    [Create_Time]       DATETIME     CONSTRAINT [DF_AS_Assets_MP_Record_CreateTime] DEFAULT (getdate()) NOT NULL,
    [Created_By]        VARCHAR (15) CONSTRAINT [DF_AS_Assets_MP_Record_CreatedBy] DEFAULT ('') NOT NULL,
    [Last_Updated_Time] DATETIME     NULL,
    [Last_Updated_By]   VARCHAR (15) CONSTRAINT [DF_AS_Assets_MP_Record_UpdatedBy] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_AS_Assets_MP_Record] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'單據、動產關聯檔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Record';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Record', @level2type = N'COLUMN', @level2name = N'ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'單據編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Record', @level2type = N'COLUMN', @level2name = N'TRX_Header_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Record', @level2type = N'COLUMN', @level2name = N'Asset_Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Record', @level2type = N'COLUMN', @level2name = N'Create_Time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立人員', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Record', @level2type = N'COLUMN', @level2name = N'Created_By';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最後異動時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Record', @level2type = N'COLUMN', @level2name = N'Last_Updated_Time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最後異動人員', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Record', @level2type = N'COLUMN', @level2name = N'Last_Updated_By';

