﻿CREATE TABLE [dbo].[AS_Connection]
(
	[ID] INT NOT NULL IDENTITY , 
    [SubSystem_Code] VARCHAR(12) NULL, 
    [Path] NVARCHAR(255) NULL, 
    [Supper_Connection] VARCHAR(255) NULL, 
    [Last_Updated_Time] DATETIME NULL, 
    [Last_Updated_By] NUMERIC(15) NULL, 
    CONSTRAINT [PK_AS_Connection] PRIMARY KEY ([ID]) 
)

GO

CREATE INDEX [IX_AS_Upload_Detail_PostID] ON [dbo].[AS_Connection] ([SubSystem_Code])
