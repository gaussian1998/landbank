﻿CREATE TABLE [dbo].[AS_GL_Trade_Item] (
    [ID]                INT             IDENTITY (1, 1) NOT NULL,
    [Open_Type]         VARCHAR (3)     NULL,
    [Account_Type]      CHAR (1)        NULL,
    [Account]           VARCHAR (12)    NULL,
    [Branch_Ind]        CHAR (1)        NULL,
    [Trans_Type]        CHAR (1)        NULL,
    [Account_Key]       VARCHAR (10)    NULL,
    [DB_CR]             CHAR (1)        NULL,
    [Real_Memo]         CHAR (1)        NULL,
    [Master_No]         NUMERIC (6)     NULL,
    [Item]              VARCHAR (3)     NULL,
    [Amount]            NUMERIC (15, 2) DEFAULT ((0)) NOT NULL,
    [Amount_Field_Name] NVARCHAR (40)   NULL,
    CONSTRAINT [PK_AS_GL_Trade_Item] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Trade_Item1]
    ON [dbo].[AS_GL_Trade_Item]([Master_No] ASC, [Item] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Trade_Item2]
    ON [dbo].[AS_GL_Trade_Item]([Account] ASC);
