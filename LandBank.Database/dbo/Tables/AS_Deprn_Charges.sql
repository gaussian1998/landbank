CREATE TABLE [dbo].[AS_Deprn_Charges] (
    [ID]                   INT             IDENTITY (1, 1) NOT NULL,
    [Office_Branch]        VARCHAR (3)     DEFAULT ('') NOT NULL,
    [Trans_Type]           CHAR (1)        DEFAULT ('') NOT NULL,
    [Assets_Number]        VARCHAR (30)    DEFAULT ('') NOT NULL,
    [Period]               INT             DEFAULT ((0)) NOT NULL,
    [Original_Cost]        NUMERIC (18, 2) DEFAULT ((0)) NOT NULL,
    [Deprn_Reserve]        NUMERIC (18, 2) DEFAULT ((0)) NOT NULL,
    [Net_Value]            NUMERIC (18, 2) DEFAULT ((0)) NOT NULL,
    [Period_Deprn_Charges] NUMERIC (18, 2) DEFAULT ((0)) NOT NULL,
    [Rest_Counts]          INT             DEFAULT ((0)) NOT NULL,
    [Last_Updated_Time]    DATETIME        NULL,
    [Last_Updated_By]      NUMERIC (15)    NULL,
    CONSTRAINT [PK_AS_Deprn_Charges] PRIMARY KEY CLUSTERED ([ID] ASC)
);

