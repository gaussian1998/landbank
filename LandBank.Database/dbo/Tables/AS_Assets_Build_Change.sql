﻿CREATE TABLE [dbo].[AS_Assets_Build_Change](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TRX_Header_ID] [int] NULL,
	[AS_Assets_Build_ID] [int] NULL,
	[UPD_File] [varchar](1) NULL,
	[Field_Name] [varchar](240) NULL,
	[Modify_Type] [varchar](1) NULL,
	[Before_Data] [nvarchar](600) NULL,
	[After_Data] [nvarchar](600) NULL,
	[Create_Time] [datetime] NULL,
	[Created_By] [numeric](15, 0) NULL,
	[Created_By_Name] [nvarchar](100) NULL,
	[Last_UpDatetimed_Time] [datetime] NULL,
	[Last_UpDatetimed_By] [numeric](15, 0) NULL,
	[Last_UpDatetimed_By_Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_AS_Assets_Build_Change] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO