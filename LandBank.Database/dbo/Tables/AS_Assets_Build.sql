CREATE TABLE [dbo].[AS_Assets_Build](
	[ID] [int] NOT NULL,
	[Officer_Branch] [varchar](30) NOT NULL,
	[Asset_Number] [varchar](30) NOT NULL,
	[Asset_Category_Code] [varchar](30) NOT NULL,
	[Book_Type] [varchar](15) NOT NULL,
	[Deprn_Method_Code] [varchar](20) NOT NULL,
	[Life_Years] [numeric](20, 0) NOT NULL,
	[Life_Months] [numeric](20, 0) NOT NULL,
	[Location_Disp] [varchar](30) NOT NULL,
	[Parent_Asset_Number] [varchar](30) NOT NULL,
	[Old_Asset_Number] [varchar](30) NOT NULL,
	[Old_Asset_Category_Code] [nvarchar](30) NOT NULL,
	[Description] [nvarchar](600) NOT NULL,
	[Used_Type] [varchar](30) NOT NULL,
	[Used_Status] [varchar](30) NOT NULL,
	[Obtained_Method] [varchar](30) NOT NULL,
	[Original_Cost] [numeric](20, 2) NOT NULL,
	[Salvage_Value] [numeric](20, 2) NOT NULL,
	[Deprn_Amount] [numeric](20, 2) NOT NULL,
	[Reval_Adjustment_Amount] [numeric](20, 2) NOT NULL,
	[Business_Area_Size] [numeric](20, 2) NOT NULL,
	[NONBusiness_Area_Size] [numeric](20, 2) NOT NULL,
	[Current_Cost] [numeric](20, 2) NOT NULL,
	[Reval_Reserve] [numeric](20, 2) NOT NULL,
	[Business_Book_Amount] [numeric](20, 2) NOT NULL,
	[NONBusiness_Book_Amount] [numeric](20, 2) NOT NULL,
	[Deprn_Reserve] [numeric](20, 2) NOT NULL,
	[Business_Deprn_Reserve] [numeric](20, 2) NOT NULL,
	[NONBusiness_Deprn_Reserve] [numeric](20, 2) NOT NULL,
	[Date_Placed_In_Service] [datetime] NOT NULL,
	[Remark1] [nvarchar](600) NOT NULL,
	[Remark2] [nvarchar](600) NOT NULL,
	[Current_Units] [numeric](20, 0) NOT NULL,
	[Delete_Reason] [varchar](30) NOT NULL,
	[Urban_Renewal] [nvarchar](255) NOT NULL,
	[Authorization_Number] [nvarchar](600) NOT NULL,
	[Authorization_Name] [nvarchar](150) NOT NULL,
	[City_Code] [varchar](20) NOT NULL,
	[City_Name] [nvarchar](255) NOT NULL,
	[District_Code] [varchar](20) NOT NULL,
	[District_Name] [nvarchar](255) NOT NULL,
	[Section_Code] [varchar](20) NOT NULL,
	[Sectioni_Name] [nvarchar](255) NOT NULL,
	[Sub_Sectioni_Name] [nvarchar](225) NOT NULL,
	[Build_Number] [varchar](8) NOT NULL,
	[Build_Address] [nvarchar](1200) NOT NULL,
	[Building_Total_Floor] [numeric](20, 0) NOT NULL,
	[Building_STRU] [varchar](240) NOT NULL,
	[Authorized_Area] [numeric](20, 2) NOT NULL,
	[Build_Name] [varchar](30) NOT NULL,
	[Deprn_Type] [int] NOT NULL,
	[Not_Deprn_Flag] [bit] NOT NULL,
	[DEPRN_Counts] [int] NULL,
	[Create_Time] [datetime] NOT NULL,
	[Created_By] [varchar](15) NOT NULL,
	[Created_By_Name] [varchar](100) NOT NULL,
	[Last_Updated_Time] [datetime] NULL,
	[Last_Updated_By] [varchar](15) NOT NULL,
	[Last_UpDatetimed_By_Name] [varchar](100) NOT NULL,
 CONSTRAINT [PK_AS_Assets_Build] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Officer_Branch]  DEFAULT ('') FOR [Officer_Branch]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Asset_Number]  DEFAULT ('') FOR [Asset_Number]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Asset_Category_Code]  DEFAULT ('') FOR [Asset_Category_Code]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Book_Type]  DEFAULT ('') FOR [Book_Type]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Deprn_Method_Code]  DEFAULT ('') FOR [Deprn_Method_Code]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Location_Disp]  DEFAULT ('') FOR [Location_Disp]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Parent_Asset_Number]  DEFAULT ('') FOR [Parent_Asset_Number]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Old_Asset_Number]  DEFAULT ('') FOR [Old_Asset_Number]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Old_Asset_Category_code]  DEFAULT ('') FOR [Old_Asset_Category_Code]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Description]  DEFAULT ('') FOR [Description]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Used_Type]  DEFAULT ('') FOR [Used_Type]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Used_Status]  DEFAULT ('') FOR [Used_Status]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Obtained_Method]  DEFAULT ('') FOR [Obtained_Method]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Original_Cost]  DEFAULT ((0)) FOR [Original_Cost]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Salvage_Value]  DEFAULT ((0)) FOR [Salvage_Value]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Deprn_Amount]  DEFAULT ((0)) FOR [Deprn_Amount]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Reval_Adjustment_Amount]  DEFAULT ((0)) FOR [Reval_Adjustment_Amount]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Business_Area_Size]  DEFAULT ((0)) FOR [Business_Area_Size]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_NONBusiness_Area_Size]  DEFAULT ((0)) FOR [NONBusiness_Area_Size]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Current_Cost]  DEFAULT ((0)) FOR [Current_Cost]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Reval_Reserve]  DEFAULT ((0)) FOR [Reval_Reserve]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Business_Book_Amount]  DEFAULT ((0)) FOR [Business_Book_Amount]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_NONBusiness_Book_Amount]  DEFAULT ((0)) FOR [NONBusiness_Book_Amount]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Deprn_Reserve]  DEFAULT ((0)) FOR [Deprn_Reserve]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Business_Deprn_Reserve]  DEFAULT ((0)) FOR [Business_Deprn_Reserve]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_NONBusiness_Deprn_Reserve]  DEFAULT ((0)) FOR [NONBusiness_Deprn_Reserve]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Remark1]  DEFAULT ('') FOR [Remark1]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Remark2]  DEFAULT ('') FOR [Remark2]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Current_Units]  DEFAULT ((0)) FOR [Current_Units]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Delete_Reason]  DEFAULT ('') FOR [Delete_Reason]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Urban_Renewal]  DEFAULT ('') FOR [Urban_Renewal]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Authorization_Number]  DEFAULT ('') FOR [Authorization_Number]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Authorization_Name]  DEFAULT ('') FOR [Authorization_Name]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_City_Code]  DEFAULT ('') FOR [City_Code]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_City_Name]  DEFAULT ('') FOR [City_Name]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_District_Code]  DEFAULT ('') FOR [District_Code]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_District_Name]  DEFAULT ('') FOR [District_Name]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Section_Code]  DEFAULT ('') FOR [Section_Code]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Sectioni_Name]  DEFAULT ('') FOR [Sectioni_Name]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Sub_Sectioni_Name]  DEFAULT ('') FOR [Sub_Sectioni_Name]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Build_Number]  DEFAULT ('') FOR [Build_Number]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Build_Address]  DEFAULT ('') FOR [Build_Address]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Building_Total_Floor]  DEFAULT ((1)) FOR [Building_Total_Floor]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Building_STRU]  DEFAULT ('') FOR [Building_STRU]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF__AS_Assets__Autho__6F6A7CB2]  DEFAULT ((0)) FOR [Authorized_Area]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF__AS_Assets__Build__705EA0EB]  DEFAULT ('') FOR [Build_Name]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF__AS_Assets__Deprn__7152C524]  DEFAULT ((1)) FOR [Deprn_Type]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Not_Deprn_Flag]  DEFAULT ((0)) FOR [Not_Deprn_Flag]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_CreateTime]  DEFAULT (getdate()) FOR [Create_Time]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_CreatedBy]  DEFAULT ('') FOR [Created_By]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF__AS_Assets__Creat__742F31CF]  DEFAULT ('') FOR [Created_By_Name]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF_AS_Assets_Build_Last_Updated_By]  DEFAULT ('') FOR [Last_Updated_By]
GO

ALTER TABLE [dbo].[AS_Assets_Build] ADD  CONSTRAINT [DF__AS_Assets__Last___75235608]  DEFAULT ('') FOR [Last_UpDatetimed_By_Name]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'管轄單位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Officer_Branch'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'財產編號(資產編號)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Asset_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'帳本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Asset_Category_Code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資產分類' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Book_Type'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折舊方式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Deprn_Method_Code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'年限年數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Life_Years'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'年限月數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Life_Months'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'放置地點' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Location_Disp'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'母資產編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Parent_Asset_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原資產編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Old_Asset_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原資產分類' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Old_Asset_Category_Code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'摘要' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Description'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用別(業務別)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Used_Type'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用現況(使用情況)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Used_Status'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取得方式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Obtained_Method'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取得成本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Original_Cost'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'殘值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Salvage_Value'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'累計減損' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Deprn_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'未實現重估增值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Reval_Adjustment_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'營業用面積' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Business_Area_Size'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'非營業用面積' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'NONBusiness_Area_Size'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'帳面金額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Current_Cost'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'帳面金額-重估增值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Reval_Reserve'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'營業用帳面金額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Business_Book_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'非營業用帳面金額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'NONBusiness_Book_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'累計折舊' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Deprn_Reserve'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'營業用房屋累計折舊' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Business_Deprn_Reserve'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'非營業用房屋累計折舊' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'NONBusiness_Deprn_Reserve'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'啟用日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Date_Placed_In_Service'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註事項1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Remark1'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註事項2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Remark2'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'單位量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Current_Units'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'註銷原因' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Delete_Reason'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'都更案號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Urban_Renewal'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'權狀字號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Authorization_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'地政事務所' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Authorization_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-縣市代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'City_Code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-縣市名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'City_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-鄉鎮市區代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'District_Code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-鄉鎮市區名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'District_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-段代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Section_Code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-段名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Sectioni_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-小段名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Sub_Sectioni_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Build_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'房屋門牌' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Build_Address'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'本建物總層數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Building_Total_Floor'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'構造' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Building_STRU'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'權利面積' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Authorized_Area'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'大樓名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Build_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'折舊註記' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build', @level2type=N'COLUMN',@level2name=N'Deprn_Type'
GO