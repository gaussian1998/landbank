﻿CREATE TABLE [dbo].[AS_Prog_Template]
(
	[User_ID] NVARCHAR(10) NOT NULL , 
    [Flow_Code] INT NULL, 
    [Transaction_Status] VARCHAR NULL, 
    [Transaction_Datetime] DATETIME NULL, 
    [Is_Active] BIT NULL DEFAULT 0, 
    [Delete_Flag] BIT NULL DEFAULT 0, 
    [Create_Time] DATETIME NULL, 
    [Created_By] NUMERIC(15) NULL, 
    [Last_Updated_Time] DATETIME NULL, 
    [Last_Updated_By] NUMERIC(15) NULL, 
    [version] VARCHAR(2) NOT NULL, 
    CONSTRAINT [PK_AS_Prog_Template] PRIMARY KEY ([User_ID], [version])
)

GO

CREATE INDEX [IX_AS_Prog_Template_UserID] ON [dbo].[AS_Prog_Template] ([User_ID])
