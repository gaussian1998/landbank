﻿CREATE TABLE [dbo].[AS_GL_Trade_Account] (
    [ID]                  INT          IDENTITY (1, 1) NOT NULL,
    [Account_Key]         VARCHAR (10) DEFAULT ('') NOT NULL,
    [Master_No]           NUMERIC (6)  DEFAULT ((0)) NOT NULL,
    [Account]             VARCHAR (12) DEFAULT ('') NOT NULL,
    [Related_No]          NUMERIC (6)  DEFAULT ((0)) NOT NULL,
    [DB_CR]               CHAR (1)     DEFAULT ('') NOT NULL,
    [Asset_Category_code] VARCHAR (30) DEFAULT ('') NOT NULL,
    [Post_Sub_Account]    BIT          DEFAULT ((0)) NOT NULL,
    [Is_Sub_Account]      BIT          DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AS_GL_Trade_Account] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Trade_Account1]
    ON [dbo].[AS_GL_Trade_Account]([Master_No] ASC, [Related_No] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Trade_Account2]
    ON [dbo].[AS_GL_Trade_Account]([Account_Key] ASC, [Account] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Trade_Account3]
    ON [dbo].[AS_GL_Trade_Account]([Account] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Trade_Account4]
    ON [dbo].[AS_GL_Trade_Account]([Master_No] ASC, [Asset_Category_code] ASC);
