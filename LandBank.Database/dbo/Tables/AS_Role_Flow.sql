﻿CREATE TABLE [dbo].[AS_Role_Flow] (
    [ID]                 INT      IDENTITY (1, 1) NOT NULL,
    [RoleFlow_Header_ID] INT      NOT NULL,
    [User_ID_App]        INT      NULL,
    [User_ID]            INT      NOT NULL,
    [Create_Time]        DATETIME NULL,
    [Created_By]         INT      NULL,
    [Last_Updated_Time]  DATETIME NULL,
    [Last_Updated_By]    INT      NULL,
    CONSTRAINT [PK_AS_Role_Flow] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_AS_Role_Flow_AS_Users] FOREIGN KEY ([User_ID]) REFERENCES [dbo].[AS_Users] ([ID]),
    CONSTRAINT [FK_AS_Role_Flow_AS_RoleFlow_Header] FOREIGN KEY ([RoleFlow_Header_ID]) REFERENCES [dbo].[AS_RoleFlow_Header] ([ID])
);