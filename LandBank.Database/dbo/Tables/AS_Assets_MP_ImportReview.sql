﻿CREATE TABLE [dbo].[AS_Assets_MP_ImportReview] (
    [ImportReviewID]    VARCHAR (50) NOT NULL,
    [ImportID]          INT          CONSTRAINT [DF_AS_Assets_MP_ImportReview_ImportNum] DEFAULT ('') NOT NULL,
    [Create_Time]       DATETIME     CONSTRAINT [DF_AS_Assets_MP_ImportReview_Create_Time] DEFAULT (getdate()) NOT NULL,
    [Created_By]        VARCHAR (15) CONSTRAINT [DF_AS_Assets_MP_ImportReview_Created_By] DEFAULT ('') NOT NULL,
    [Last_Updated_Time] DATETIME     NULL,
    [Last_Updated_By]   VARCHAR (15) CONSTRAINT [DF_AS_Assets_MP_ImportReview_Last_Updated_By] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_AS_Assets_MP_ImportReview_1] PRIMARY KEY CLUSTERED ([ImportReviewID] ASC, [ImportID] ASC)
);


