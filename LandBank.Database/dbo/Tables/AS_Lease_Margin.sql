CREATE TABLE [dbo].[AS_Lease_Margin] (
    [ID]               INT           IDENTITY (1, 1) NOT NULL,
    [LEASE_HEADER_ID]  NUMERIC (18)  NULL,
    [LEASE_NUMBER]     NVARCHAR (20) NULL,
    [RECEIVED_DATE]    NVARCHAR (20) NULL,
    [RETURN_DATE]      NVARCHAR (20) NULL,
    [BANK]             NVARCHAR (20) NULL,
    [PRICE]            INT           NULL,
    [CURRENCY]         NVARCHAR (10) NULL,
    [CURRENCY_VALUE]   NVARCHAR (5)  DEFAULT ((1)) NULL,
    [CREATION_DATE]    DATE          NULL,
    [CREATED_BY]       NUMERIC (15)  NULL,
    [LAST_UPDATE_DATE] DATE          NULL,
    [LAST_UPDATED_BY]  NUMERIC (15)  NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

