﻿CREATE TABLE [dbo].[AS_TRX_Contents_Detail] (
    [ID]              INT           NOT NULL,
	[Contents_ID]           INT   NULL,
    [SEQ]           NUMERIC(2)   NULL,
	[Contents]           NVARCHAR(MAX)   NULL, 
    CONSTRAINT [PK_AS_TRX_Contents_Detail] PRIMARY KEY ([ID])
);



GO


CREATE INDEX [IX_AS_TRX_Contents_Detail_Contents_ID_SEQ] ON [dbo].[AS_TRX_Contents_Detail] ([Contents_ID],[SEQ])
