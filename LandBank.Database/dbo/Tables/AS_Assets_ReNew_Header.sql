CREATE TABLE [dbo].[AS_Assets_ReNew_Header](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TRX_Header_ID] [varchar](18) NOT NULL,
	[Transaction_Type] [varchar](4) NOT NULL,
	[Transaction_Status] [varchar](20) NOT NULL,
	[Book_Type] [varchar](20) NOT NULL,
	[Transaction_Datetime_Entered] [datetime] NOT NULL,
	[Accounting_Datetime] [datetime] NULL,
	[Office_Branch] [varchar](30) NOT NULL,
	[Description] [nvarchar](600) NOT NULL,
	[Create_Time] [datetime] NOT NULL,
	[Created_By] [varchar](15) NOT NULL,
	[Last_Updated_Time] [datetime] NULL,
	[Last_Updated_By] [varchar](15) NOT NULL,
 CONSTRAINT [PK_AS_Assets_ReNew_Header] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Header] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Header_Description]  DEFAULT ('') FOR [Description]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Header] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Header_Created_By]  DEFAULT ('') FOR [Created_By]
GO

ALTER TABLE [dbo].[AS_Assets_ReNew_Header] ADD  CONSTRAINT [DF_AS_Assets_ReNew_Header_Last_Updated_By]  DEFAULT ('') FOR [Last_Updated_By]
GO


