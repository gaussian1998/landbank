CREATE TABLE [dbo].[AS_Flow_FormInfo] (
    [ID]                INT           IDENTITY (1, 1) NOT NULL,
    [Doc_Type]          NVARCHAR (40) NULL,
    [Doc_No]            VARCHAR (4)   NOT NULL,
    [Version]           NUMERIC (2)   DEFAULT ((0)) NOT NULL,
    [Form_Name]         VARCHAR (100) NULL,
    [Start_Time]        DATE          NOT NULL,
    [End_Time]          DATE          NOT NULL,
    [Last_Updated_Time] DATETIME      NULL,
    [Last_Updated_By]   NUMERIC (15)  NULL,
    CONSTRAINT [PK_AS_Flow_FormInfo] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_FormInfo1]
    ON [dbo].[AS_Flow_FormInfo]([Doc_No] ASC, [Version] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_FormInfo2]
    ON [dbo].[AS_Flow_FormInfo]([Doc_No] ASC, [Start_Time] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_FormInfo3]
    ON [dbo].[AS_Flow_FormInfo]([Doc_No] ASC, [Last_Updated_Time] ASC);


