

CREATE TABLE [dbo].[AS_Assets_Land_Record](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TRX_Header_ID] [varchar](18) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_TRX_Heard_ID]  DEFAULT (''),
	[Asset_Number] [varchar](30) NOT NULL,
	[Current_units] [numeric](20, 0) NOT NULL,
	[Datetime_Placed_In_Service] [datetime] NOT NULL,
	[Old_Asset_Number] [varchar](30) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_Old_Asset_Number]  DEFAULT (''),
	[Asset_Category_code] [nvarchar](30) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_Asset_Category_code]  DEFAULT (''),
	[Location_Disp] [varchar](30) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_Location_Disp]  DEFAULT (''),
	[Description] [nvarchar](600) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_Description]  DEFAULT (''),
	[Urban_Renewal] [varchar](225) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_Urban_Renewal]  DEFAULT (''),
	[City_Code] [varchar](20) NOT NULL,
	[District_Code] [varchar](20) NOT NULL,
	[Section_Code] [varchar](20) NOT NULL,
	[Sub_Section_Name] [nvarchar](255) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_Sub_Section_Name]  DEFAULT (''),
	[Parent_Land_Number] [varchar](20) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_Parent_Land_Number]  DEFAULT ('母地號'),
	[Filial_Land_Number] [varchar](20) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_Filial_Land_Number]  DEFAULT ('子地號'),
	[Authorization_Number] [nvarchar](600) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_Authorization_Number]  DEFAULT (''),
	[Authorized_name] [nvarchar](150) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_Authorized_name]  DEFAULT (''),
	[Area_Size] [numeric](20, 0) NOT NULL,
	[Authorized_Scope_Molecule] [numeric](20, 0) NOT NULL,
	[Authorized_Scope_Denomminx] [numeric](20, 0) NOT NULL,
	[Authorized_Area] [numeric](20, 2) NOT NULL,
	[Used_Type] [varchar](30) NOT NULL,
	[Used_Status] [varchar](30) NOT NULL,
	[Obtained_Method] [varchar](30) NOT NULL,
	[Used_Partition] [varchar](600) NOT NULL,
	[Is_Marginal_Land] [char](1) NOT NULL,
	[Own_Area] [numeric](20, 2) NOT NULL,
	[NONOwn_Area] [numeric](20, 2) NOT NULL,
	[Original_Cost] [numeric](20, 2) NOT NULL,
	[Deprn_Amount] [numeric](20, 2) NOT NULL,
	[Current_Cost] [numeric](20, 2) NOT NULL,
	[Reval_Adjustment_Amount] [numeric](20, 2) NOT NULL,
	[Reval_Land_VAT] [numeric](20, 2) NOT NULL,
	[Reval_Reserve] [numeric](20, 2) NOT NULL,
	[Business_Area_Size] [numeric](20, 2) NOT NULL,
	[Business_Book_Amount] [numeric](20, 2) NOT NULL,
	[Business_Reval_Reserve] [numeric](20, 2) NOT NULL,
	[NONBusiness_Area_Size] [numeric](20, 2) NOT NULL,
	[NONBusiness_Book_Amount] [numeric](20, 2) NOT NULL,
	[NONBusiness_Reval_Reserve] [numeric](20, 2) NOT NULL,
	[Year_Number] [numeric](4, 0) NOT NULL,
	[Announce_Amount] [numeric](20, 2) NOT NULL,
	[Announce_Price] [numeric](20, 2) NOT NULL,
	[Tax_Type] [varchar](4) NOT NULL,
	[Reduction_reason] [varchar](4) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_Reduction_reason]  DEFAULT (''),
	[Transfer_Price] [numeric](20, 2) NOT NULL,
	[Reduction_Area] [numeric](20, 2) NOT NULL,
	[Declared_Price] [numeric](20, 2) NOT NULL,
	[Dutiable_Amount] [numeric](20, 2) NOT NULL,
	[Remark1] [nvarchar](600) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_Remark1]  DEFAULT (''),
	[Remark2] [nvarchar](600) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_Remark2]  DEFAULT (''),
	[Land_Item] [varchar](20) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_Land_Item]  DEFAULT (''),
	[CROP] [nvarchar](2) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_CROP]  DEFAULT (''),
	[Create_Time] [datetime] NOT NULL,
	[Created_By] [varchar](15) NOT NULL CONSTRAINT [DF_AS_Assets_Land_Record_Created_By]  DEFAULT (''),
	[Asset_Land_ID] [nvarchar](20) NULL,
	[Serial_Number] [nvarchar](20) NULL,
	[City_Name] [nvarchar](255) NULL,
	[District_Name] [nvarchar](255) NULL,
	[Section_Name] [nvarchar](255) NULL,
	[Created_By_Name] [nvarchar](100) NULL,
	[Last_UpDatetimed_Time] [datetime] NULL,
	[Last_UpDatetimed_By] [decimal](15, 0) NULL,
	[Last_UpDatetimed_By_Name] [nvarchar](100) NULL,
	[Expense_Account] [varchar](300) NULL,
 CONSTRAINT [PK_AS_Assets_Land_Record] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'單據編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'TRX_Header_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'財產編號(資產編號)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Asset_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'單位量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Current_units'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'啟用日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Datetime_Placed_In_Service'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原資產編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Old_Asset_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資產分類' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Asset_Category_code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'放置地點' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Location_Disp'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'摘要' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Description'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'都更案號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Urban_Renewal'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-縣市代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'City_Code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-鄉鎮市區代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'District_Code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-段代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Section_Code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標示-小段名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Sub_Section_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Parent_Land_Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Parent_Land_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filial_Land_Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Filial_Land_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'權狀字號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Authorization_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'地政事務所' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Authorized_name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'整筆面積' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Area_Size'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'權利範圍(分子)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Authorized_Scope_Molecule'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'權利範圍(分母)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Authorized_Scope_Denomminx'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'權利面積' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Authorized_Area'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用別(業務別)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Used_Type'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用現況' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Used_Status'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取得方式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Obtained_Method'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用分區' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Used_Partition'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'畸零地否' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Is_Marginal_Land'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自用面積' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Own_Area'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'非自用面積' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'NONOwn_Area'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取得成本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Original_Cost'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'累計減損' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Deprn_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'帳面金額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Current_Cost'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'未實現重估增值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Reval_Adjustment_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'估計應付土地增值稅' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Reval_Land_VAT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'帳面金額-重估增值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Reval_Reserve'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'營業用面積' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Business_Area_Size'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'營業用帳面金額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Business_Book_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'營業用帳面金額-重估增值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Business_Reval_Reserve'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'非營業用面積' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'NONBusiness_Area_Size'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'非營業用帳面金額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'NONBusiness_Book_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'非營業用帳面金額-重估增值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'NONBusiness_Reval_Reserve'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'年度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Year_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公告現值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Announce_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公告地價' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Announce_Price'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'稅種' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Tax_Type'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'減免原因' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Reduction_reason'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'移轉地價' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Transfer_Price'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'減稅面積' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Reduction_Area'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'申報地價' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Declared_Price'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'應繳稅額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Dutiable_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註事項1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Remark1'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註事項2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Remark2'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'地目' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Land_Item'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'等則' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'CROP'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Create_Time'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立人員 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Created_By'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立人員姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Created_By_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後異動時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Last_UpDatetimed_Time'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後異動人員 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Last_UpDatetimed_By'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後異動人員姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Last_UpDatetimed_By_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'會計科目' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Land_Record', @level2type=N'COLUMN',@level2name=N'Expense_Account'
GO


