﻿CREATE TABLE [dbo].[AS_TRX_Headers] (
    [ID]              INT     IDENTITY (1, 1)      NOT NULL, 
	[Form_Number]              varchar(17)     NOT NULL, 
	[Office_Branch]              varchar(3)      NOT NULL, 
	[Book_Type_Code]              nvarchar(15)      NOT NULL, 
	[Transaction_Type]              varchar(4)      NOT NULL, 
	[Transaction_Status]              varchar(20)      NOT NULL, 
	[Transaction_Datetime_Entered]              datetime      NOT NULL, 
	[Accounting_Datetime]              datetime      NULL, 
	[Description]              nvarchar(600)      NULL, 
	[Amotized_Adjustment_Flag]              VARCHAR      NOT NULL DEFAULT 'N', 
	[Create_Time]              datetime      NULL, 
	[Created_By]              numeric(15)      NULL, 
	[Created_By_Name]             nvarchar(100)     NULL, 
	[Last_UpDatetimed_Time]              datetime      NULL, 
	[Last_UpDatetimed_By]              numeric(15)      NULL, 
	[Last_UpDatetimed_By_Name]              nvarchar(100)      NULL, 
    CONSTRAINT [PK_AS_TRX_Headers] PRIMARY KEY ([ID])
);
