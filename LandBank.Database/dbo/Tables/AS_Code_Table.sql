CREATE TABLE [dbo].[AS_Code_Table] (
    [ID]                INT            IDENTITY (1, 1) NOT NULL,
    [Class]             VARCHAR (20)   NULL,
    [Class_Name]        NVARCHAR (120) NOT NULL,
    [Code_ID]           VARCHAR (20)   NOT NULL,
    [Text]              NVARCHAR (100) NULL,
    [Is_Active]         BIT            DEFAULT ((1)) NOT NULL,
    [Cancel_Code]       BIT            NOT NULL,
    [Parameter1]        VARCHAR (40)   NULL,
    [Parameter2]        VARCHAR (40)   NULL,
    [Remark]            NVARCHAR (500) NULL,
    [Value1]            NUMERIC (6)    NULL,
    [Value2]            NUMERIC (6, 2) NULL,
    [Value3]            NUMERIC (9, 4) NULL,
    [Last_Updated_Time] DATETIME       NULL,
    [Last_Updated_By]   INT            NULL,
    CONSTRAINT [PK_AS_CodeTable] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [UX_AS_CodeTable_Class_CodeID] UNIQUE NONCLUSTERED ([Class] ASC, [Code_ID] ASC)
);

