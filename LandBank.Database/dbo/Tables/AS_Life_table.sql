CREATE TABLE [dbo].[AS_Life_table] (
    [ID]                INT            IDENTITY (1, 1) NOT NULL,
    [Life_Code_No]      VARCHAR (11)   DEFAULT (' ') NOT NULL,
    [Life_Code_1]       CHAR (1)       DEFAULT (' ') NOT NULL,
    [Life_Code_2]       CHAR (2)       DEFAULT (' ') NOT NULL,
    [Life_Code_3]       CHAR (2)       DEFAULT (' ') NOT NULL,
    [Life_Code_4]       CHAR (2)       DEFAULT (' ') NOT NULL,
    [Life_Code_5]       CHAR (4)       DEFAULT (' ') NOT NULL,
    [Asset_Text]        NVARCHAR (100) DEFAULT (' ') NOT NULL,
    [Life_Years]        NUMERIC (3)    DEFAULT ((0)) NOT NULL,
    [Life_Months]       NUMERIC (6)    DEFAULT ((0)) NOT NULL,
    [Not_Deprn_Flag]    BIT            DEFAULT ((0)) NOT NULL,
    [Last_Updated_Time] DATETIME       NULL,
    [Last_Updated_By]   NUMERIC (15)   NULL,
    CONSTRAINT [PK_AS_Life_table] PRIMARY KEY CLUSTERED ([ID] ASC)
);
