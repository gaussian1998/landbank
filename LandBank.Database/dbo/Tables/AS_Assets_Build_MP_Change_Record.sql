USE [AS_LandBank]
GO

/****** Object:  Table [dbo].[AS_Assets_Build_MP_Change_Record]    Script Date: 2017/11/23 下午 04:59:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AS_Assets_Build_MP_Change_Record](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TRX_Header_ID] [int] NULL,
	[AS_Assets_Build_MP_Record_ID] [int] NULL,
	[UPD_File] [varchar](1) NULL,
	[Field_Name] [varchar](240) NULL,
	[Modify_Type] [varchar](1) NULL,
	[Before_Data] [nvarchar](600) NULL,
	[After_Data] [nvarchar](600) NULL,
	[Expense_Account] [varchar](300) NULL,
	[Create_Time] [datetime] NULL,
	[Created_By] [numeric](15, 0) NULL,
	[Created_By_Name] [nvarchar](100) NULL,
	[Last_UpDatetimed_Time] [datetime] NULL,
	[Last_UpDatetimed_By] [numeric](15, 0) NULL,
	[Last_UpDatetimed_By_Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_AS_Assets_Build_MP_Change_Record] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'單據編號ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Change_Record', @level2type=N'COLUMN',@level2name=N'TRX_Header_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'重大組成單據明細記錄檔ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Change_Record', @level2type=N'COLUMN',@level2name=N'AS_Assets_Build_MP_Record_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'異動檔案' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Change_Record', @level2type=N'COLUMN',@level2name=N'UPD_File'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'異動欄位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Change_Record', @level2type=N'COLUMN',@level2name=N'Field_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'異動狀態' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Change_Record', @level2type=N'COLUMN',@level2name=N'Modify_Type'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'異動前資料' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Change_Record', @level2type=N'COLUMN',@level2name=N'Before_Data'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'異動後資料' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Change_Record', @level2type=N'COLUMN',@level2name=N'After_Data'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'會計科目' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Change_Record', @level2type=N'COLUMN',@level2name=N'Expense_Account'
GO


