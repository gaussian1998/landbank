﻿CREATE TABLE [dbo].[AS_Checked_SubSystem]
(
	[ID] INT NOT NULL IDENTITY, 
    [ToDoList_ID] INT NOT NULL, 
    [Flow_Code] VARCHAR(21) NULL, 
    [Now_User_ID] INT NULL, 
    [Flow_status] VARCHAR NULL, 
    [Create_Time] DATETIME NOT NULL, 
    [Created_By] INT NOT NULL, 
    [Last_Updated_Time] DATETIME NOT NULL, 
    [Last_Updated_By] INT NOT NULL, 
    CONSTRAINT [PK_AS_Checked_SubSystem] PRIMARY KEY ([ID]), 
    CONSTRAINT [FK_AS_Checked_SubSystem_AS_ToDoList] FOREIGN KEY ([ToDoList_ID]) REFERENCES [AS_ToDoList]([ID])
)

GO

CREATE INDEX [IX_AS_Checked_SubSystem_ToDoList_ID] ON [dbo].[AS_Checked_SubSystem] ([ToDoList_ID])
