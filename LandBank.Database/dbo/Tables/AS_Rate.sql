﻿CREATE TABLE [dbo].[AS_Rate] (
    [ID]           INT              IDENTITY (1, 1) NOT NULL,
    [Branch_Code]  NUMERIC (3)      NULL,
    [Department]   NVARCHAR (20)     NULL,
    [Book_Type]    VARCHAR (20)     NULL,
    [Currency]     VARCHAR (4)      NULL,
    [Rate_ID]      VARCHAR (6)      NULL,
    [Rate]         NUMERIC (14, 10) NULL,
    [Input_Date]   DATE             NULL,
    [Basis]        NUMERIC (3)      DEFAULT ((0)) NOT NULL,
    [Days_Of_Year] NUMERIC (3)      DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AS_Rate] PRIMARY KEY CLUSTERED ([ID] ASC)
);


--GO
--CREATE UNIQUE NONCLUSTERED INDEX [UX_AS_Rate1]
--    ON [dbo].[AS_Rate]([Branch_Code] ASC, [Department] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Rate1]
    ON [dbo].[AS_Rate]([Currency] ASC);