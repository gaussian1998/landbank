CREATE TABLE [dbo].[AS_Assets_DocumentType] (
    [ID]                BIGINT        NOT NULL,
    [Name]              NVARCHAR (50) NULL,
    [Create_Time]       DATETIME      NULL,
    [Created_By]        INT           NULL,
    [Last_Updated_Time] DATETIME      NULL,
    [Last_Updated_By]   INT           NULL,
    CONSTRAINT [PK_AS_Assets_DocumentType] PRIMARY KEY CLUSTERED ([ID] ASC)
);

