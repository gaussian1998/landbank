﻿CREATE TABLE [dbo].[AS_GL_Trade_Define] (
    [ID]          INT           IDENTITY (1, 1) NOT NULL,
    [Book_Type]   VARCHAR (20)  NULL,
    [Trans_Type]  CHAR (1)      NULL,
    [Account_Key] VARCHAR (10)  NULL,
    [Master_No]   NUMERIC (6)   NULL,
    [Master_Text] NVARCHAR (60) NULL,
    [Trade_Type]  CHAR (3)      NULL,
    [Client_Type] NUMERIC (2)   NULL,
    [Soft_Lock]   BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AS_GL_Trade_Define] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Trade_Define1]
    ON [dbo].[AS_GL_Trade_Define]([Account_Key] ASC, [Master_No] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Trade_Define2]
    ON [dbo].[AS_GL_Trade_Define]([Trans_Type] ASC, [Account_Key] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Trade_Define3]
    ON [dbo].[AS_GL_Trade_Define]([Master_No] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_GL_Trade_Define4]
    ON [dbo].[AS_GL_Trade_Define]([Trade_Type] ASC);
