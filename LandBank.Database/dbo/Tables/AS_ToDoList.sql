﻿CREATE TABLE [dbo].[AS_ToDoList]
(
	[ID] INT NOT NULL IDENTITY, 
    [ToDoList_Code] VARCHAR(12) NOT NULL, 
    [ToDoList_Status] VARCHAR NOT NULL, 
    [User_ID] INT NOT NULL, 
    [ToDoList_Branch] VARCHAR(3) NULL, 
    [ToDoList_Department] VARCHAR(5) NULL, 
    [Subject] NVARCHAR(160) NOT NULL, 
    [Detail] NVARCHAR(3000) NULL, 
    [ToDoList_Startdate] DATE NOT NULL, 
    [ToDoList_Enddate] DATE NOT NULL, 
    [Create_Time] DATETIME NOT NULL, 
    [Created_By] INT NOT NULL, 
    [Last_Updated_Time] DATETIME NOT NULL, 
    [Last_Updated_By] INT NOT NULL, 
    [ToDoList_Type] VARCHAR NOT NULL, 
    [RemindDays] INT NOT NULL, 
    CONSTRAINT [PK_AS_ToDoList] PRIMARY KEY ([ID])
)

GO

CREATE INDEX [IX_AS_ToDoList_ToDoListID] ON [dbo].[AS_ToDoList] ([ToDoList_Code])

GO

CREATE INDEX [IX_AS_ToDoList_ToDoListID_ToDoList_Status_UserID] ON [dbo].[AS_ToDoList] ([ToDoList_Code], [ToDoList_Status], [User_ID])
