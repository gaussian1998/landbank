﻿CREATE TABLE [dbo].[AS_Users_FlowRole] (
    [ID]              INT           IDENTITY (1, 1) NOT NULL,
    [UserID]          INT           NOT NULL,
    [FlowRoleID]      INT           NOT NULL,
    [FlowRoleType]    NVARCHAR (20) NULL,
    [BranchCode]      NUMERIC (3)   NULL,
    [Department]      VARCHAR (20)  NULL,
    [IsSuperRole]     BIT           NULL,
    [LastUpdatedTime] DATETIME      NULL,
    [LastUpdatedBy]   NUMERIC (15)  NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_AS_Users_FlowRole_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[AS_Users] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_AS_Users_FlowRole_FlowRoleID] FOREIGN KEY ([FlowRoleID]) REFERENCES [dbo].[AS_Code_Table] ([ID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Users_FlowRole_BranchCode_UserID]
    ON [dbo].[AS_Users_FlowRole]([BranchCode] ASC, [UserID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Users_FlowRole_UserID_FlowRoleID]
    ON [dbo].[AS_Users_FlowRole]([UserID] ASC, [FlowRoleID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Users_FlowRole_BranchCode_Department_UserID]
    ON [dbo].[AS_Users_FlowRole]([BranchCode] ASC, [Department] ASC, [UserID] ASC);
