﻿CREATE TABLE [dbo].[AS_Log_Record] (
    [ID]              INT     IDENTITY(1,1)       NOT NULL,
	[UserID]              NUMERIC (15)       NOT NULL,
    [BranchCode]      NUMERIC (3)    NULL,
    [ModeID]      VARCHAR (50)   NULL,
    [IP]  VARCHAR (1000) NULL,
    [Description]        NVARCHAR (1000)            NULL,
	[OperationTime] DATETIME NULL,
	[Log_define_ID] VARCHAR (50) NULL,
	[ModifyDescription] NVARCHAR (1000) NULL, 
    CONSTRAINT [PK_AS_Log_Record] PRIMARY KEY ([ID])
);



GO

CREATE INDEX [IX_AS_Log_Record_UserID] ON [dbo].[AS_Log_Record] ([UserID])

GO

CREATE INDEX [IX_AS_Log_Record_OperationTime] ON [dbo].[AS_Log_Record] ([OperationTime])

GO

CREATE INDEX [IX_AS_Log_Record_BranchCode] ON [dbo].[AS_Log_Record] ([BranchCode])
