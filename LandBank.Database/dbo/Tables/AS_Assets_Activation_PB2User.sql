CREATE TABLE [dbo].[AS_Assets_Activation_PB2User] (
    [ID]            BIGINT         IDENTITY (1, 1) NOT NULL,
    [Branch_Code]   VARCHAR (50)   NULL,
    [business_Name] NVARCHAR (50)  NULL,
    [User_Code]     VARCHAR (50)   NULL,
    [UserName]      NVARCHAR (50)  NULL,
    [Remark]        NVARCHAR (200) NULL,
    CONSTRAINT [PK_AS_Assets_Activation_PB2User] PRIMARY KEY CLUSTERED ([ID] ASC)
);

