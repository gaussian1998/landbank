CREATE TABLE [dbo].[AS_Assets_Inspect_Pic] (
    [ID]       BIGINT         NOT NULL,
    [DetailID] BIGINT         NULL,
    [FileName] NVARCHAR (200) NULL,
    CONSTRAINT [PK_AS_Assets_Inspect_Pic] PRIMARY KEY CLUSTERED ([ID] ASC)
);

