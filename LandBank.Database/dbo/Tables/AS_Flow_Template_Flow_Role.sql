﻿CREATE TABLE [dbo].[AS_Flow_Template_Flow_Role] (
    [ID]               INT IDENTITY (1, 1) NOT NULL,
    [Flow_Template_ID] INT NOT NULL,
    [Flow_Role_ID]     INT NOT NULL,
    [Step_No]          INT NOT NULL,
    [End_Point]        BIT NULL,
    CONSTRAINT [PK_AS_Flow_Template_Flow_Role] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_AS_Flow_Template_Flow_Role_Template] FOREIGN KEY ([Flow_Template_ID]) REFERENCES [dbo].[AS_Flow_Template] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_AS_Flow_Template_Flow_Role_Role] FOREIGN KEY ([Flow_Role_ID]) REFERENCES [dbo].[AS_Code_Table] ([ID]) ON DELETE CASCADE
);





