﻿CREATE TABLE [dbo].[AS_Assets_Compare_Apply] (
    [ID]                BIGINT   NOT NULL,
    [AssetMaster_ID]    BIGINT   NULL,
    [Land_ID]           INT      NULL,
    [Create_Time]       DATETIME NULL,
    [Created_By]        INT      NULL,
    [Last_Updated_Time] DATETIME NULL,
    [Last_Updated_By]   INT      NULL,
    CONSTRAINT [PK_AS_Assets_Compare_Apply] PRIMARY KEY CLUSTERED ([ID] ASC)
);

