﻿CREATE TABLE [dbo].[AS_History_Parameter] (
    [ID]                    INT            IDENTITY (1, 1) NOT NULL,
    [Transaction_Type]      VARCHAR (10)   NOT NULL,
    [Source_Table]          VARCHAR (50)   NOT NULL,
    [Purpose_Table]         VARCHAR (50)   NULL,
    [Source_Table_Name]     NVARCHAR (100) NULL,
    [Formal_Area]           BIT            DEFAULT ((1)) NULL,
    [Formal_Area_Months]    INT            NULL,
    [History_Area]          BIT            DEFAULT ((1)) NULL,
    [History_Area _Months]  INT            NULL,
    [Description]           NVARCHAR (600) NULL,
    [Create_Time]           DATETIME       NULL,
    [Created_By]            INT            NULL,
    [Last_UpDatetimed_Time] DATETIME       NULL,
    [Last_UpDatetimed_By]   INT            NULL,
    CONSTRAINT [PK_AS_History_Parameter] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_AS_History_Parameter_AS_Users_Create] FOREIGN KEY ([Created_By]) REFERENCES [dbo].[AS_Users] ([ID]),
    CONSTRAINT [FK_AS_History_Parameter_AS_Users_Update] FOREIGN KEY ([Last_UpDatetimed_By]) REFERENCES [dbo].[AS_Users] ([ID])
);