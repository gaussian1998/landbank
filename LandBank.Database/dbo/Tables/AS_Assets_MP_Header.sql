﻿CREATE TABLE [dbo].[AS_Assets_MP_Header] (
    [ID]                INT            IDENTITY (1, 1) NOT NULL,
    [TRX_Header_ID]     VARCHAR (18)   NOT NULL,
    [Transaction_Type]  VARCHAR (4)    NOT NULL,
    [Source]            VARCHAR (1)    NOT NULL,
    [Book_Type]         VARCHAR (20)   CONSTRAINT [DF_AS_Assets_MP_Header_Book_Type] DEFAULT ('') NOT NULL,
    [OriOffice_Branch]  VARCHAR (30)   CONSTRAINT [DF_AS_Assets_MP_Header_OriOffice_Branch] DEFAULT ('') NOT NULL,
    [Office_Branch]     VARCHAR (30)   CONSTRAINT [DF_AS_Assets_MP_Header_Office_Branch] DEFAULT ('') NOT NULL,
    [DisposeReason]     VARCHAR (30)   CONSTRAINT [DF_AS_Assets_MP_Header_DisposeReason] DEFAULT ('') NOT NULL,
    [Remark]            NVARCHAR (600) CONSTRAINT [DF_AS_Assets_MP_Header_Remark] DEFAULT ('') NOT NULL,
    [Flow_Status]       VARCHAR (1)    CONSTRAINT [DF_AS_Assets_MP_Header_Flow_Status] DEFAULT ('') NOT NULL,
    [SummarySDate]      DATETIME       NULL,
    [SummaryEDate]      DATETIME       NULL,
    [Create_Time]       DATETIME       CONSTRAINT [DF_AS_Assets_MP_Header_Create_Time] DEFAULT (getdate()) NOT NULL,
    [Created_By]        VARCHAR (15)   NOT NULL,
    [Last_Updated_Time] DATETIME       NULL,
    [Last_Updated_By]   VARCHAR (15)   CONSTRAINT [DF_AS_Assets_MP_Header_Last_Updated_By] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_AS_Assets_MP_Header] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�ʲ������', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Header';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Header', @level2type = N'COLUMN', @level2name = N'ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'��ڽs��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Header', @level2type = N'COLUMN', @level2name = N'TRX_Header_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�������', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Header', @level2type = N'COLUMN', @level2name = N'Transaction_Type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�ӷ�����', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Header', @level2type = N'COLUMN', @level2name = N'Source';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�b��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Header', @level2type = N'COLUMN', @level2name = N'Book_Type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'���ҳ��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Header', @level2type = N'COLUMN', @level2name = N'Office_Branch';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�K�n', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Header', @level2type = N'COLUMN', @level2name = N'Remark';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ñ�֪��A', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Header', @level2type = N'COLUMN', @level2name = N'Flow_Status';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�إ߮ɶ�', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Header', @level2type = N'COLUMN', @level2name = N'Create_Time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�إߤH��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Header', @level2type = N'COLUMN', @level2name = N'Created_By';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�̫Ყ�ʮɶ�', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Header', @level2type = N'COLUMN', @level2name = N'Last_Updated_Time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'�̫Ყ�ʤH��', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Header', @level2type = N'COLUMN', @level2name = N'Last_Updated_By';

