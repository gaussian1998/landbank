CREATE TABLE [dbo].[AS_Assets_Build_Land](
	[ID] [int] NOT NULL,
	[Asset_Build_ID] [int] NOT NULL,
	[Asset_Land_ID] [int] NOT NULL,
	[Create_Time] [datetime] NOT NULL,
	[Created_By] [varchar](15) NOT NULL,
	[Last_Updated_Time] [datetime] NULL,
	[Last_Updated_By] [varchar](15) NOT NULL,
 CONSTRAINT [PK_AS_Assets_Build_Land_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AS_Assets_Build_Land] ADD  CONSTRAINT [DF_AS_Assets_Build_Land_Create_Time]  DEFAULT (getdate()) FOR [Create_Time]
GO

ALTER TABLE [dbo].[AS_Assets_Build_Land] ADD  CONSTRAINT [DF_AS_Assets_Build_Land_Created_By]  DEFAULT ('') FOR [Created_By]
GO

ALTER TABLE [dbo].[AS_Assets_Build_Land] ADD  CONSTRAINT [DF_AS_Assets_Build_Land_Last_Updated_By]  DEFAULT ('') FOR [Last_Updated_By]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�ЫΥD��ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_Land', @level2type=N'COLUMN',@level2name=N'Asset_Build_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�g�a�D��ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_Land', @level2type=N'COLUMN',@level2name=N'Asset_Land_ID'
GO


