﻿CREATE TABLE [dbo].[AS_TRX_Contents] (
    [ID]              INT           NOT NULL,
	[FormNo]           VARCHAR (17)   NOT NULL,
    [DocNo]           VARCHAR (6)   NOT NULL,
    [tableID]         INT NOT NULL,
    [FormVersion]        NUMERIC(2)  NULL,
    [FormName]         VARCHAR (100)   NULL,
    [Contents]          NVARCHAR (MAX)   NULL,
    [Subject_NUMBER]      VARCHAR (30)  NULL,
    [isMult]        BIT           NULL,
    [LastUpdatedTime] DATETIME      NULL,
    [LastUpdatedBy]   NUMERIC (15)  NULL, 
    CONSTRAINT [PK_AS_TRX_Contents] PRIMARY KEY ([ID])
);


GO

CREATE INDEX [IX_AS_TRX_Contents_DocNo] ON [dbo].[AS_TRX_Contents] ([DocNo])

GO

CREATE INDEX [IX_AS_TRX_Contents_FormNo_FormVersion] ON [dbo].[AS_TRX_Contents] ([FormNo],[FormVersion])
