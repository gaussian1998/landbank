﻿CREATE TABLE [dbo].[AS_Assets_Build_Category](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TRX_Header_ID] [int] NOT NULL,
	[Asset_Build_ID] [int] NOT NULL,
	[Serial_Number] [numeric](20, 0) NOT NULL,
	[Asset_Number] [nvarchar](30) NOT NULL,
	[Old_Asset_Category_Code] [varchar](30) NOT NULL,
	[NEW_Asset_Category_Code] [varchar](30) NULL,
	[Description] [nvarchar](600) NULL,
	[Expense_Account] [nvarchar](300) NULL,
	[Create_Time] [datetime] NULL,
	[Created_By] [numeric](15, 0) NULL,
	[Created_By_Name] [nvarchar](100) NULL,
	[Last_UpDatetimed_Time] [datetime] NULL,
	[Last_UpDatetimed_By] [numeric](15, 0) NULL,
	[Last_UpDatetimed_By_Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_AS_Assets_Build_ Category] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO