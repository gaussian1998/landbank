
CREATE TABLE [dbo].[AS_Assets_MP_Deprn] (
    [ID]                INT          IDENTITY (1, 1) NOT NULL,
    [TRX_Header_ID]     VARCHAR (18) CONSTRAINT [DF_AS_ASSETS_MP_Deprn_TRX_Header_ID] DEFAULT ('') NOT NULL,
    [Asset_Number]      VARCHAR (30) CONSTRAINT [DF_AS_ASSETS_MP_Deprn_Assets_ID] DEFAULT ('') NOT NULL,
    [Deprn_Date]        DATETIME     NULL,
    [Post_Date]         DATETIME     NULL,
    [Life_Month]        INT          NOT NULL,
    [Deprn_Source]      VARCHAR (1)  CONSTRAINT [DF_AS_ASSETS_MP_Deprn_Deprn_Source] DEFAULT ('') NOT NULL,
    [Deprn_Cost]        NUMERIC (18) CONSTRAINT [DF_AS_ASSETS_MP_Deprn_Deprn_Cost] DEFAULT ((0)) NOT NULL,
    [Adjusted_Cost]     NUMERIC (18) CONSTRAINT [DF_AS_ASSETS_MP_Deprn_Adjusted_Cost] DEFAULT ((0)) NOT NULL,
    [Create_Time]       DATETIME     CONSTRAINT [DF_AS_ASSETS_MP_Deprn_CreateTime] DEFAULT (getdate()) NOT NULL,
    [Created_By]        VARCHAR (15) CONSTRAINT [DF_AS_ASSETS_MP_Deprn_CreatedBy] DEFAULT ('') NOT NULL,
    [Last_Updated_Time] DATETIME     NULL,
    [Last_Updated_By]   VARCHAR (15) CONSTRAINT [DF_AS_ASSETS_MP_Deprn_UpdatedBy] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_AS_ASSETS_MP_Deprn] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'動產折舊明細檔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Deprn';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Deprn', @level2type = N'COLUMN', @level2name = N'ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'單據編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Deprn', @level2type = N'COLUMN', @level2name = N'TRX_Header_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Deprn', @level2type = N'COLUMN', @level2name = N'Asset_Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'執行折舊日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Deprn', @level2type = N'COLUMN', @level2name = N'Deprn_Date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'出帳日/過帳日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Deprn', @level2type = N'COLUMN', @level2name = N'Post_Date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'年限(以月分計算)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Deprn', @level2type = N'COLUMN', @level2name = N'Life_Month';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'來源類型', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Deprn', @level2type = N'COLUMN', @level2name = N'Deprn_Source';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'折舊金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Deprn', @level2type = N'COLUMN', @level2name = N'Deprn_Cost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'折舊後資產成本', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Deprn', @level2type = N'COLUMN', @level2name = N'Adjusted_Cost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Deprn', @level2type = N'COLUMN', @level2name = N'Create_Time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立人員', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Deprn', @level2type = N'COLUMN', @level2name = N'Created_By';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最後異動時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Deprn', @level2type = N'COLUMN', @level2name = N'Last_Updated_Time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最後異動人員', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AS_Assets_MP_Deprn', @level2type = N'COLUMN', @level2name = N'Last_Updated_By';

