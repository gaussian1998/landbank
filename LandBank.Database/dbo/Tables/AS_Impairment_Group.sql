﻿CREATE TABLE [dbo].[AS_Impairment_Group] (
    [ID]                  INT          IDENTITY (1, 1) NOT NULL,
    [Impairm_Group_No]    NUMERIC (6)  NULL,
    [Asset_Category_Code] VARCHAR (30) NULL,
    [Use_Type]            VARCHAR (2)  NULL,
    [Asset_Number]        VARCHAR (30) NULL,
    [Account]             VARCHAR (12) NULL,
    CONSTRAINT [PK_AS_Impairment_Group] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_AS_Impairment_Group1]
    ON [dbo].[AS_Impairment_Group]([Impairm_Group_No] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Impairment_Group2]
    ON [dbo].[AS_Impairment_Group]([Asset_Category_Code] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Impairment_Group3]
    ON [dbo].[AS_Impairment_Group]([Account] ASC);
