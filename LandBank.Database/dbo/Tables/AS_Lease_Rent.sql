CREATE TABLE [dbo].[AS_Lease_Rent] (
    [ID]               INT            IDENTITY (1, 1) NOT NULL,
    [LEASE_HEADER_ID]  NUMERIC (18)   NULL,
    [LEASE_NUMBER]     NVARCHAR (20)  NULL,
    [START_DATE]       DATE           NULL,
    [END_DATE]         DATE           NULL,
    [CALCULATE_METHOD] NVARCHAR (10)  NULL,
    [PAYMENT_PERIOD]   NVARCHAR (5)   NULL,
    [DUE_DAY]          INT            NULL,
    [LAST_PAYMENT]     NVARCHAR (50)  NULL,
    [NEXT_PAYMENT]     NVARCHAR (50)  NULL,
    [EFFECTIVE_DATE]   DATE           NULL,
    [PRICE]            INT            NULL,
    [CURRENCY]         NVARCHAR (10)  NULL,
    [CURRENCY_VALUE]   NVARCHAR (5)   DEFAULT ((1)) NULL,
    [DESCRIPTION]      NVARCHAR (250) NULL,
    [CREATION_DATE]    DATE           NULL,
    [CREATED_BY]       NUMERIC (15)   NULL,
    [LAST_UPDATE_DATE] DATE           NULL,
    [LAST_UPDATED_BY]  NUMERIC (15)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

