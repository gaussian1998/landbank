﻿CREATE TABLE [dbo].[AS_Flow_Serial] (
    [ID]       INT         IDENTITY (1, 1) NOT NULL,
    [Years]    NUMERIC (4) NULL,
    [DocNo]    VARCHAR (4) NULL,
    [TXDate]   DATE        NULL,
    [SerialNo] NUMERIC (6) NULL,
    CONSTRAINT [PK_AS_Flow_Serial] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Serial_DocNo_Txdate]
    ON [dbo].[AS_Flow_Serial]([DocNo] ASC, [TXDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Serial_Years]
    ON [dbo].[AS_Flow_Serial]([Years] ASC);