﻿CREATE TABLE [dbo].[AS_Mail_User]
(
	[ID] INT NOT NULL IDENTITY , 
    [ToDoList_ID] INT NOT NULL, 
    [ToDoList_Branch] VARCHAR(3) NULL, 
    [ToDoList_Department] VARCHAR(5) NULL, 
    [User_ID] INT NOT NULL,
    [Creat_Mail] BIT NOT NULL, 
    [Remind_Mail] BIT NOT NULL, 
    [Finished_Mail] BIT NOT NULL, 
    [Create_Time] DATETIME NOT NULL, 
    [Created_By] INT NOT NULL, 
    [Last_Updated_Time] DATETIME NOT NULL, 
    [Last_Updated_By] INT NOT NULL, 
    CONSTRAINT [PK_AS_Mail_User] PRIMARY KEY ([ID]), 
    CONSTRAINT [FK_AS_Mail_User_ToDoList] FOREIGN KEY ([ToDoList_ID]) REFERENCES [AS_ToDoList]([ID]) ON DELETE CASCADE
)
