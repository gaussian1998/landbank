﻿CREATE TABLE [dbo].[AS_Assets_Build_Years](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TRX_Header_ID] [int] NOT NULL,
	[Asset_Build_ID] [int] NOT NULL,
	[Serial_Number] [numeric](20, 0) NOT NULL,
	[Asset_Number] [nvarchar](30) NOT NULL,
	[Old_Life_Years] [numeric](20, 0) NOT NULL,
	[Old_Life_Months] [numeric](20, 0) NULL,
	[New_Life_Years] [numeric](20, 0) NULL,
	[New_Life_Months] [numeric](20, 0) NULL,
	[Description] [nvarchar](600) NULL,
	[Create_Time] [datetime] NULL,
	[Created_By] [numeric](15, 0) NULL,
	[Created_By_Name] [nvarchar](100) NULL,
	[Last_UpDatetimed_Time] [datetime] NULL,
	[Last_UpDatetimed_By] [numeric](15, 0) NULL,
	[Last_UpDatetimed_By_Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_AS_Assets_Build_Years] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO