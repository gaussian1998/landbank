﻿CREATE TABLE [dbo].[AS_Flow_Name] (
    [ID]                INT            IDENTITY (1, 1) NOT NULL,
    [Flow_Code]         VARCHAR (10)   NOT NULL,
    [Flow_Type]         NVARCHAR (40)  NOT NULL,
    [Flow_Name]         NVARCHAR (60)  NOT NULL,
    [Is_Active]         BIT            DEFAULT ((1)) NOT NULL,
    [Cancel_Code]       BIT            DEFAULT ((0)) NOT NULL,
    [Remark]            NVARCHAR (100) NULL,
    [Last_Updated_Time] DATETIME       NULL,
    [Last_Updated_By]   NUMERIC (15)   NULL,
    CONSTRAINT [PK_AS_Flow_Name] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [UX_AS_Flow_Name_FlowCode] UNIQUE NONCLUSTERED ([Flow_Code] ASC)
);