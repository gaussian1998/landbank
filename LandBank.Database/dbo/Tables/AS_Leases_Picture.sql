﻿CREATE TABLE [dbo].[AS_Leases_Picture] (
    [ID]                INT             NOT NULL,
    [LEASE_HEADER_ID]   NUMERIC (18)    NULL,
    [NAME]              VARCHAR (50)    NULL,
    [PATH]              VARCHAR (255)   NULL,
    [PICTURE_CONTENT]   VARBINARY (MAX) NULL,
    [DESCRIPTION]       VARCHAR (250)   NULL,
    [LAST_UPDATE_DATE]  DATE            NULL,
    [LAST_UPDATED_BY]   NUMERIC (15)    NULL,
    [CREATION_DATE]     DATE            NULL,
    [CREATED_BY]        NUMERIC (15)    NULL,
    [LAST_UPDATE_LOGIN] NUMERIC (15)    NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);
