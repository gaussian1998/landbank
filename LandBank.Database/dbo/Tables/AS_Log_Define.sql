﻿CREATE TABLE [dbo].[AS_Log_Define] (
    [ID]              INT     IDENTITY(1,1)       NOT NULL,
	[MainFunctionID]              INT       NOT NULL,
    [MainFunction]      NVARCHAR (1000)   NULL,
    [MainID]      VARCHAR (50)   NULL,
    [MainIDDescription]  NVARCHAR (1000) NULL,
    [ModeID]        VARCHAR (50)            NULL,
	[ActionPageName] VARCHAR (100) NULL,
	[ActionBtnName] VARCHAR (100) NULL,
	[Description] NVARCHAR (1000) NULL,
	[DescriptionRes] VARCHAR (50) NULL,
	[IsActive] BIT NULL,
	[LastUpdatedTime] DATETIME NULL,
	[LastUpdatedBy] NUMERIC(15) NULL, 
    CONSTRAINT [PK_AS_Log_Define] PRIMARY KEY ([ID])
);
