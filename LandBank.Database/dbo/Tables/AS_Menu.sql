﻿CREATE TABLE [dbo].[AS_Menu]
(
	[ID] INT NOT NULL  IDENTITY, 
    [Doc_Type] VARCHAR(10) NOT NULL, 
    [Is_Display] BIT NULL, 
    [Order] INT NULL, 
    [Branch_Type] VARCHAR(10) NOT NULL, 
    CONSTRAINT [AK_AS_Menu_DocType_BranchType] UNIQUE ([Doc_Type], [Branch_Type]), 
    CONSTRAINT [PK_AS_Menu] PRIMARY KEY ([ID])
)
