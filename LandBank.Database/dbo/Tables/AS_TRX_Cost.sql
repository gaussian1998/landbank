﻿CREATE TABLE [dbo].[AS_TRX_Cost] (
    [ID]              INT      IDENTITY (1, 1)     NOT NULL,
	[TRX_Header_ID] INT           NOT NULL,
	[Assets_Land_ID] INT           NOT NULL,
    CONSTRAINT [PK_AS_TRX_Cost] PRIMARY KEY ([ID]), 
    CONSTRAINT [FK_AS_TRX_Cost_TRX_Header_ID] FOREIGN KEY ([TRX_Header_ID]) REFERENCES [AS_TRX_Headers]([ID]), 
    CONSTRAINT [FK_AS_TRX_Cost_Assets_Land_ID] FOREIGN KEY ([Assets_Land_ID]) REFERENCES [AS_Assets_Land]([ID])
);
