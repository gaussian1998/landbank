CREATE TABLE [dbo].[AS_Assets_Land_Header] (
    [ID]                           INT            IDENTITY (1, 1) NOT NULL,
    [TRX_Header_ID]                VARCHAR (18)   NOT NULL,
    [Transaction_Type]             VARCHAR (4)    NOT NULL,
    [Transaction_Status]           VARCHAR (20)   NOT NULL,
    [Book_Type]                    VARCHAR (20)   NOT NULL,
    [Transaction_Datetime_Entered] DATETIME       NOT NULL,
    [Accounting_Datetime]          DATETIME       NULL,
    [Office_Branch]                VARCHAR (30)   NOT NULL,
    [Description]                  NVARCHAR (600) CONSTRAINT [DF_AS_Assets_Land_Header_Description] DEFAULT ('') NOT NULL,
    [Create_Time]                  DATETIME       NOT NULL,
    [Created_By]                   VARCHAR (15)   CONSTRAINT [DF_AS_Assets_Land_Header_Created_By] DEFAULT ('') NOT NULL,
    [Last_Updated_Time]            DATETIME       NULL,
    [Last_Updated_By]              VARCHAR (15)   CONSTRAINT [DF_AS_Assets_Land_Header_Last_Updated_By] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_AS_Assets_Land_Header] PRIMARY KEY CLUSTERED ([ID] ASC)
);


