﻿CREATE TABLE [dbo].[AS_Post_His]
(
	[ID] INT NOT NULL IDENTITY , 
    [Flow_Code] VARCHAR(30) NULL, 
    [Transaction_Status] INT NULL, 
    [Transaction_Datetime] DATETIME NULL,
	[Main_Type] VARCHAR(1) NULL, 
    [User_ID] INT NULL, 
    [Post_ID] VARCHAR(12) NULL, 
    [Post_Department] VARCHAR NULL, 
    [Subject] NVARCHAR(150) NULL, 
    [Detail] NVARCHAR(MAX) NULL, 
    [Post_Startdate] DATETIME NOT NULL, 
    [Post_Enddate] DATETIME NOT NULL, 
    [Is_Active] BIT NULL, 
    [Create_Time] DATETIME NULL, 
    [Created_By] INT NULL, 
    [Last_Updated_Time] DATETIME NULL, 
    [Last_Updated_By] INT NULL, 
    CONSTRAINT [PK_AS_Post_His] PRIMARY KEY ([ID]) 
)

GO

CREATE INDEX [IX_AS_Post_His_ID_FlowCode] ON [dbo].[AS_Post_His] ([ID], [Flow_Code])

GO

CREATE INDEX [IX_AS_Post_His_ID_FlowCode_PostID] ON [dbo].[AS_Post_His] ([ID], [Flow_Code], [Post_ID])
