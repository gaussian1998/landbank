﻿CREATE TABLE [dbo].[AS_Flow_Template] (
    [ID]                INT          IDENTITY (1, 1) NOT NULL,
    [Flow_Name_ID]      INT          NOT NULL,
    [Version]           NUMERIC (3)  NOT NULL,
    [Is_Email]          BIT          NULL,
    [Last_Updated_Time] DATETIME     NULL,
    [Last_Updated_By]   NUMERIC (15) NULL,
    CONSTRAINT [PK_AS_Flow_Template] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_AS_Flow_Template_Flow_Name_ID] FOREIGN KEY ([Flow_Name_ID]) REFERENCES [dbo].[AS_Flow_Name] ([ID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Flow_Template_FlowCode]
    ON [dbo].[AS_Flow_Template]([Flow_Name_ID] ASC);

