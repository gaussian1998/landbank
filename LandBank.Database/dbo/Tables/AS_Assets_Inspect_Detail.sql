CREATE TABLE [dbo].[AS_Assets_Inspect_Detail] (
    [ID]                BIGINT         NOT NULL,
    [AID]               BIGINT         NULL,
    [BID]               BIGINT         NULL,
    [Land_ID]           INT            NULL,
    [Detail_No]         NVARCHAR (9)   NULL,
    [BeginDate]         DATETIME       NULL,
    [EndDate]           DATETIME       NULL,
    [Type]              INT            NULL,
    [Inspect_User]      INT            NULL,
    [DESCRIPTION]       NVARCHAR (600) NULL,
    [InspectDate]       DATETIME       NULL,
    [InspectType]       INT            NULL,
    [InspectReason]     NVARCHAR (600) NULL,
    [InspectResult]     NVARCHAR (600) NULL,
    [Remark]            NVARCHAR (600) NULL,
    [Create_Time]       DATETIME       NULL,
    [Created_By]        INT            NULL,
    [Last_Updated_Time] DATETIME       NULL,
    [Last_Updated_By]   INT            NULL,
    CONSTRAINT [PK_AS_Assets_Inspect_Detail] PRIMARY KEY CLUSTERED ([ID] ASC)
);

