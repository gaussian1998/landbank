USE [AS_LandBank]
GO

/****** Object:  Table [dbo].[AS_Assets_Build_MP_Years]    Script Date: 2017/11/23 下午 05:00:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AS_Assets_Build_MP_Years](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TRX_Header_ID] [int] NOT NULL,
	[Asset_Build_MP_ID] [int] NOT NULL,
	[Serial_Number] [numeric](20, 0) NOT NULL,
	[Asset_Number] [nvarchar](30) NOT NULL,
	[Old_Life_Years] [numeric](20, 0) NOT NULL,
	[Old_Life_Months] [numeric](20, 0) NULL,
	[New_Life_Years] [numeric](20, 0) NULL,
	[New_Life_Months] [numeric](20, 0) NULL,
	[Description] [nvarchar](600) NULL,
	[Create_Time] [datetime] NULL,
	[Created_By] [numeric](15, 0) NULL,
	[Created_By_Name] [nvarchar](100) NULL,
	[Last_UpDatetimed_Time] [datetime] NULL,
	[Last_UpDatetimed_By] [numeric](15, 0) NULL,
	[Last_UpDatetimed_By_Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_AS_Assets_Build_MP_Years] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'單據表頭ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Years', @level2type=N'COLUMN',@level2name=N'TRX_Header_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'重大組成主檔ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Years', @level2type=N'COLUMN',@level2name=N'Asset_Build_MP_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'項次' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Years', @level2type=N'COLUMN',@level2name=N'Serial_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'財產編號(資產編號)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Years', @level2type=N'COLUMN',@level2name=N'Asset_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'異動前年限年數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Years', @level2type=N'COLUMN',@level2name=N'Old_Life_Years'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'異動前年限月數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Years', @level2type=N'COLUMN',@level2name=N'Old_Life_Months'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'異動後年限年數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Years', @level2type=N'COLUMN',@level2name=N'New_Life_Years'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'異動後年限月數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Years', @level2type=N'COLUMN',@level2name=N'New_Life_Months'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'摘要' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AS_Assets_Build_MP_Years', @level2type=N'COLUMN',@level2name=N'Description'
GO


