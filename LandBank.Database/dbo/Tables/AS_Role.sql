﻿CREATE TABLE [dbo].[AS_Role] (
    [ID]                INT            IDENTITY (1, 1) NOT NULL,
    [Role_Code]         VARCHAR (10)   NOT NULL,
    [Role_Name]         NVARCHAR (120) NOT NULL,
    [Text]              NVARCHAR (100) NULL,
    [Is_Active]         BIT            DEFAULT ((0)) NULL,
    [Cancel_Code]       BIT            DEFAULT ((0)) NULL,
    [Remark]            NVARCHAR (500) NULL,
    [Create_Time]       DATETIME       NULL,
    [Created_By]        NUMERIC (15)   NULL,
    [Last_Updated_Time] DATETIME       NULL,
    [Last_Updated_By]   NUMERIC (15)   NULL,
    [Doc_Type]          VARCHAR (10)   DEFAULT ('') NOT NULL,
    [Operation_Type] VARCHAR(10) NULL, 
    [Branch_Type] VARCHAR(10) NULL, 
    [ExpireDate] DATETIME NULL, 
    CONSTRAINT [PK_AS_Role] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [UK_AS_Role_Role_Id] UNIQUE NONCLUSTERED ([Role_Code] ASC)
);
