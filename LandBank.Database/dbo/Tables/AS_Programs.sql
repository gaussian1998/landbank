﻿CREATE TABLE [dbo].[AS_Programs] (
    [ID]              INT            IDENTITY (1, 1) NOT NULL,
    [Doc_Type]        VARCHAR (10)   DEFAULT ('') NOT NULL,
    [Programs_Code]   VARCHAR (120)  DEFAULT ('') NOT NULL,
    [Programs_Number] VARCHAR (20)   DEFAULT ('') NOT NULL,
    [Programs_Name]   NVARCHAR (120) DEFAULT ('') NOT NULL,
    [Brief_Name]      NVARCHAR (60)  DEFAULT ('') NOT NULL,
    [Text]            NVARCHAR (200) DEFAULT ('') NULL,
    [Is_Active]       BIT            DEFAULT ((1)) NULL,
    [Cancel_Code]     BIT            DEFAULT ((0)) NULL,
    [Remark]          NVARCHAR (500) DEFAULT ('') NULL,
    [Create_Time]     DATETIME       NULL,
    [Create_By]       NUMERIC (15)   NULL,
	[Last_Updated_Time] DATETIME NULL, 
    [Last_Updated_By] NUMERIC(15) NULL, 
    CONSTRAINT [PK_AS_Programs] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Programs1]
    ON [dbo].[AS_Programs]([Doc_Type] ASC, [Programs_Code] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AS_Programs2]
    ON [dbo].[AS_Programs]([Programs_Code] ASC);
