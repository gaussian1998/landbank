CREATE TRIGGER [dbo].[AS_Assets_Activation01]
   ON  [dbo].[AS_Assets_Activation] 
  For INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Declare @b int 
	Declare @id bigint 
	set @id = (select id from inserted)
	set @b = isnull((select convert(int,max(substring([Activation_No],12,6))) from AS_Assets_Activation where id != @id),0)  

SELECT *
FROM    inserted
UPDATE  AS_Assets_Activation
SET     [Activation_No] = AS_Assets_Activation.[Activation_No] + right('000000'+convert(varchar,(@b+1)),6)
FROM    inserted
WHERE   AS_Assets_Activation.id = inserted.id


END

	
GO

ALTER TABLE [dbo].[AS_Assets_Activation] ENABLE TRIGGER [AS_Assets_Activation01]
GO


