
CREATE TRIGGER [dbo].[AS_Assets_AssetMaster01]
   ON  [dbo].[AS_Assets_AssetMaster] 
  For INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Declare @b int 
	Declare @id bigint 
	set @id = (select id from inserted)
	set @b = isnull((select convert(int,max(substring(TRX_Header_ID,12,6))) from AS_Assets_AssetMaster where id != @id),0)  

SELECT *
FROM    inserted
UPDATE  AS_Assets_AssetMaster
SET     TRX_Header_ID = AS_Assets_AssetMaster.TRX_Header_ID + right('000000'+convert(varchar,(@b+1)),6)
FROM    inserted
WHERE   AS_Assets_AssetMaster.id = inserted.id


END

	
GO

ALTER TABLE [dbo].[AS_Assets_AssetMaster] ENABLE TRIGGER [AS_Assets_AssetMaster01]
GO


