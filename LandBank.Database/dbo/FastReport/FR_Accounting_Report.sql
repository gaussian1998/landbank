﻿CREATE TABLE [dbo].[FR_Accounting_Report]
(
	[FR_Branch]        VARCHAR (100)   NOT NULL,
    [FR_Account]           VARCHAR (100)     NOT NULL,
    [FR_Account_Name]      NVARCHAR (40)    NOT NULL,
	[FR_Debit_Amount]            VARCHAR (100)    NOT NULL,
	[FR_Credit_Amount]            VARCHAR (100)    NOT NULL,
	[FR_Trans_Seq]         VARCHAR (100)     NOT NULL,
	[FR_Asset_Number]      VARCHAR (100)     NOT NULL,
	[FR_Lease_Number]      VARCHAR (100)      NOT NULL,
	[FR_Batch_Seq]         VARCHAR (100)     NOT NULL,
	[FR_Post_Seq]          VARCHAR (100)                NOT NULL,
	[FR_Post_Date]         VARCHAR (100)             NOT NULL,
	[FR_Settle_Date]       VARCHAR (100)            NOT NULL,
	[FR_Payment_User]      NVARCHAR (100)    NOT NULL,
	[FR_Virtual_Account]   NVARCHAR (100)    NOT NULL
)
