﻿CREATE PROCEDURE [dbo].[AS_SP_gen_deprn_build] (
	@yyyymm varchar(6)
)
AS 
DECLARE @yyyymmdd DATE;												--折舊日
DECLARE @dd VARCHAR(2) = '21';								--折舊指定日
DECLARE @enable_dd VARCHAR(2);								--啟用指定日
DECLARE @depreciation_cnt INT;								--折舊期數
DECLARE @depreciation_cur INT;								--當前折舊期數
DECLARE @depreciation_end BIT = 0;						--是否折舊期數最後一期
/****** 讀取AS_Assets_Build的欄位 begin ******/
DECLARE @DEPRN_Counts INT;										--累計已折舊月數
DECLARE @Deprn_Reserve NUMERIC(20,2);					--累計折舊
DECLARE @Life_Years INT;											--耐用年限年數
DECLARE @LIFE_MONTHS INT;											--耐用年限月份數
DECLARE @Date_Placed_In_Service DATETIME;			--啟用日期
DECLARE @Create_Time DATETIME;								--建立時間
/****** 讀取AS_Assets_Build的欄位 end ******/
/****** 寫入AS_Deprn的欄位 begin ******/
DECLARE @Office_Branch VARCHAR(3);						--管轄單位
DECLARE @Trans_Type VARCHAR(1) = '3';					--交易種類;僅:1動產/3房屋/4租賃權益/ A土地改良物
DECLARE @Form_Number VARCHAR(17) = '';				--事務處理編號
DECLARE @Assets_Number VARCHAR(30);						--資產編號
DECLARE @Doc_No VARCHAR(4) = '';							--表單編號
DECLARE @Deprn_Months VARCHAR(6);							--折舊月份
DECLARE @Post_Date DATE = NULL;								--入帳日期
DECLARE @Period INT;													--折舊期數
DECLARE @Deprn_Source VARCHAR(30);						--來源類型
DECLARE @Net_Value NUMERIC(18,2);							--淨值
DECLARE @Original_Cost NUMERIC(18,2);					--原始取得成本
DECLARE	@Period_Deprn_Charges NUMERIC(18,2);	--本期折舊金額
DECLARE @Accum_Deprn NUMERIC(18,2);						--累積折舊成額
DECLARE @After_Deprn_Cost NUMERIC(18,2);			--折舊後成本
DECLARE @Addt_Deprn_Months VARCHAR(6) = '';		--增補提列折舊月份
DECLARE @Addt_Deprn_Amt NUMERIC(18,2) = 0;		--增補提列折舊金額
DECLARE @Addt_Deprn_Flag BIT = 0;							--增補提列折舊註記
DECLARE @Is_Post BIT = 0;											--已出帳
DECLARE @Batch_Seq VARCHAR(20) = '';          --批次編號
DECLARE @Post_Seq INT = '';										--出帳序號
DECLARE @Asset_Category VARCHAR(30);					--資產類別
DECLARE @Notes NVARCHAR(100) = '';						--備註
/****** 寫入AS_Deprn的欄位 end ******/

DECLARE Bd_Cursor CURSOR FOR									--依房屋主檔計算產生折舊明細檔(AS_Deprn)
	SELECT bd.Officer_Branch, bd.Asset_Number, ISNULL(bd.DEPRN_Counts, 0), CASE WHEN bd.Officer_Branch = '001' THEN 'H' ELSE 'B' END, ISNULL(bd.Current_Cost, 0), ISNULL(bd.Deprn_Reserve, 0), bd.Asset_Category_Code
	      ,ISNULL(bd.Life_Years, 0), ISNULL(bd.LIFE_MONTHS, 0), ISNULL(bd.Date_Placed_In_Service, 0), bd.Create_Time
		FROM AS_Assets_Build bd
	 WHERE ISNULL(bd.Not_Deprn_Flag, 0) = 0;
BEGIN
	--本月折舊明細資料(AS_Deprn)只保留未出帳。已出帳搬移到歷史記錄區(AS_Deprn_His)
	INSERT INTO AS_Deprn_His (
														Office_Branch, Trans_Type, Form_Number, Assets_Number, Doc_No, Deprn_Months, Deprn_Date, Post_Date, Period, Deprn_Source
													 ,Original_Cost, Net_Value, Period_Deprn_Charges, Accum_Deprn, After_Deprn_Cost, Addt_Deprn_Months, Addt_Deprn_Amt, Addt_Deprn_Flag
													 ,Is_Post, Batch_Seq, Post_Seq, Asset_Category, Notes, Deprn_Reserve, Salvage_Value, Remark, Deprn_Type
													 )
	SELECT Office_Branch, Trans_Type, Form_Number, Assets_Number, Doc_No, Deprn_Months, Deprn_Date, Post_Date, Period, Deprn_Source
	      ,Original_Cost, Net_Value, Period_Deprn_Charges, Accum_Deprn, After_Deprn_Cost, Addt_Deprn_Months, Addt_Deprn_Amt, Addt_Deprn_Flag
				,Is_Post, Batch_Seq, Post_Seq, Asset_Category, Notes, Deprn_Reserve, Salvage_Value, Remark, Deprn_Type
	  FROM AS_Deprn
	 WHERE Is_Post = 1
	   AND Trans_Type = @Trans_Type;

	DELETE 
		FROM AS_Deprn
	 WHERE Is_Post = 1
	   AND Trans_Type = @Trans_Type;

	--設定折舊日
	SET @yyyymmdd = CONVERT(DATETIME, @yyyymm + @dd);

	--重複產生，刪除舊資料
	--未出帳 且折舊月份 <= 系統月份
	DELETE 
		FROM AS_Deprn
	 WHERE Deprn_Months <= @yyyymm
	   AND Is_Post = 0
		 AND Trans_Type = @Trans_Type;

	OPEN Bd_Cursor;
		FETCH  NEXT FROM Bd_Cursor
		INTO @Office_Branch, @Assets_Number, @DEPRN_Counts, @Deprn_Source, @Original_Cost, @Deprn_Reserve, @Asset_Category
		    ,@Life_Years, @LIFE_MONTHS, @Date_Placed_In_Service, @Create_Time
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			--無須折舊處理
			IF (@Deprn_Reserve >= @Original_Cost)
			BEGIN
				PRINT '無須折舊:' + @Assets_Number;
				GOTO NEXT_FECTH;
			END
			--本期折舊金額
			SET @Period_Deprn_Charges = 0;
			SELECT @Period_Deprn_Charges = ISNULL(Period_Deprn_Charges, 0)
			  FROM AS_Deprn_Charges
			 WHERE Assets_Number = @Assets_Number
			   AND Trans_Type = @Trans_Type;
			--折舊金額異常處理
			IF (@Period_Deprn_Charges <= 0)
			BEGIN
				PRINT '折舊金額異常:' + @Assets_Number;
				GOTO NEXT_FECTH;
			END

			--判斷是否新增資料
			--新增;建立日為當月
			IF (@yyyymm = (CONVERT(VARCHAR(6), @Create_Time, 112)))
			BEGIN
				--啟用日有值才處理
				IF (@Date_Placed_In_Service <> 0)
				BEGIN 
					--取得啟用指定日
					SET @enable_dd = SUBSTRING((CONVERT(VARCHAR(8), @Date_Placed_In_Service, 112)), 7, 2);
					--取得折舊期數
					SET @depreciation_cnt = DATEDIFF(MONTH, @Date_Placed_In_Service, @yyyymmdd);
					--啟用指定日小於折舊指定日，則折舊期數加1
					IF (@enable_dd < @dd)
					BEGIN
						SET @depreciation_cnt = @depreciation_cnt + 1;
					END
					--判斷折舊期數是否含最後一期
					IF (@depreciation_cnt > (@Life_Years * 12 + @LIFE_MONTHS))
					BEGIN
						SET @depreciation_end = 1;
						SET @depreciation_cnt = @Life_Years * 12 + @LIFE_MONTHS;
					END
					ELSE
					BEGIN
					  SET @depreciation_end = 0;
					END
					--當前折舊期數初始化
					SET @depreciation_cur = 1;
					--有折舊期數之處理
					WHILE (@depreciation_cnt > 0)
					BEGIN
						--折舊期數最後一期處理
						IF ((@depreciation_cnt = 1) AND (@depreciation_end = 1))
						BEGIN
							SET @Net_Value = @Original_Cost - @Deprn_Reserve - (@Period_Deprn_Charges * (@depreciation_cur - 1));
							SET @Period_Deprn_Charges = @Net_Value;
							SET @Accum_Deprn = @Original_Cost;
							SET @After_Deprn_Cost = 0;
						END
						ELSE
						BEGIN
							SET @Net_Value = @Original_Cost - @Deprn_Reserve - (@Period_Deprn_Charges * (@depreciation_cur - 1));
							SET @Accum_Deprn = @Deprn_Reserve + (@Period_Deprn_Charges * @depreciation_cur);
							SET @After_Deprn_Cost = @Original_Cost - @Deprn_Reserve - (@Period_Deprn_Charges * @depreciation_cur);
						END
						--新增折舊資料
						INSERT INTO AS_Deprn(
																	Office_Branch,
																	Trans_Type,
																	Form_Number,
																	Assets_Number,
																	Doc_No,
																	Deprn_Months,
																	Deprn_Date,
																	Post_Date,
																	Period,
																	Deprn_Source,
																	Net_Value,
																	Original_Cost,
																	Period_Deprn_Charges,
																	Accum_Deprn,
																	After_Deprn_Cost,
																	Addt_Deprn_Months,
																	Addt_Deprn_Amt,
																	Addt_Deprn_Flag,
																	Is_Post,
																	Batch_Seq,
																	Post_Seq,
																	Asset_Category,
																	Notes
																)
						VALUES(
										@Office_Branch,
										@Trans_Type,
										@Form_Number,
										@Assets_Number,
										@Doc_No,
										CONVERT(VARCHAR(6), DATEADD(MONTH, 1 - @depreciation_cnt, @yyyymmdd), 112),
										GETDATE(),
										@Post_Date,
										@depreciation_cur,
										@Deprn_Source,
										@Net_Value,
										@Original_Cost,
										@Period_Deprn_Charges,
										@Accum_Deprn,
										@After_Deprn_Cost,
										@Addt_Deprn_Months,
										@Addt_Deprn_Amt,
										@Addt_Deprn_Flag,
										@Is_Post,
										@Batch_Seq,
										@Post_Seq,
										@Asset_Category,
										@Notes
									)
						--待折舊期數減1
						SET @depreciation_cnt = @depreciation_cnt - 1;
						--當前折舊期數加1
						SET @depreciation_cur = @depreciation_cur + 1;
					END
				END
			END
			--非新增
			ELSE
			BEGIN
				--最後一期檢核
				IF ((@Life_Years = 0) AND (@LIFE_MONTHS = 1))
				BEGIN
					SET @Period_Deprn_Charges = @Original_Cost - @Deprn_Reserve
				END
				--新增折舊資料
				INSERT INTO AS_Deprn(
															Office_Branch,
															Trans_Type,
															Form_Number,
															Assets_Number,
															Doc_No,
															Deprn_Months,
															Deprn_Date,
															Post_Date,
															Period,
															Deprn_Source,
															Net_Value,
															Original_Cost,
															Period_Deprn_Charges,
															Accum_Deprn,
															After_Deprn_Cost,
															Addt_Deprn_Months,
															Addt_Deprn_Amt,
															Addt_Deprn_Flag,
															Is_Post,
															Batch_Seq,
															Post_Seq,
															Asset_Category,
															Notes
														)
				VALUES(
								@Office_Branch,
								@Trans_Type,
								@Form_Number,
								@Assets_Number,
								@Doc_No,
								@yyyymm,
								GETDATE(),
								@Post_Date,
								@DEPRN_Counts + 1,
								@Deprn_Source,
								@Original_Cost - @Deprn_Reserve,
								@Original_Cost,
								@Period_Deprn_Charges,
								@Deprn_Reserve + @Period_Deprn_Charges,
								@Original_Cost - @Deprn_Reserve - @Period_Deprn_Charges,
								@Addt_Deprn_Months,
								@Addt_Deprn_Amt,
								@Addt_Deprn_Flag,
								@Is_Post,
								@Batch_Seq,
								@Post_Seq,
								@Asset_Category,
								@Notes
							)
			END
NEXT_FECTH:
			FETCH  NEXT FROM Bd_Cursor
			INTO @Office_Branch, @Assets_Number, @DEPRN_Counts, @Deprn_Source, @Original_Cost, @Deprn_Reserve, @Asset_Category
					,@Life_Years, @LIFE_MONTHS, @Date_Placed_In_Service, @Create_Time
		END
	CLOSE Bd_Cursor;
	DEALLOCATE Bd_Cursor;
END