﻿CREATE PROCEDURE [dbo].[AS_Fn_gen_DeprnAmt_mp] (
	@Trx_Code INT																--1-新增 2-修改年限 3-修改啟用日期
)
AS 
/****** 讀取AS_Assets_MP的欄位 begin ******/
DECLARE @Life_Years INT;											--耐用年限年數
DECLARE @LIFE_MONTHS INT;											--耐用年限月份數
/****** 讀取AS_Assets_MP的欄位 begin ******/
/****** 寫入AS_Deprn_Charges的欄位 begin ******/
DECLARE @Office_Branch VARCHAR(3);						--管轄單位
DECLARE @Trans_Type VARCHAR(1) = '1';					--交易種類;僅:1動產/3房屋/4租賃權益/ A土地改良物
DECLARE @Assets_Number VARCHAR(30);						--資產編號
DECLARE @Period INT;													--折舊期數
DECLARE @Original_Cost NUMERIC(18,2);					--原始取得成本
DECLARE @Deprn_Reserve NUMERIC(18,2);					--累計折舊金額
DECLARE	@Period_Deprn_Charges NUMERIC(18,2);	--本期折舊金額
DECLARE @Rest_Counts INT;											--剩餘期數
/****** 寫入AS_Deprn_Charges的欄位 end ******/

DECLARE Mp_Cursor CURSOR FOR									--依資產主檔計算產生每月折舊金額(AS_Deprn_Charges)
	SELECT mp.Office_Branch, mp.Asset_Number, ISNULL(mp.DEPRN_Counts, 0), ISNULL(mp.Assets_Fixed_Cost, 0), ISNULL(mp.Deprn_Reserve, 0)
	      ,ISNULL(mp.Life_Years, 0), ISNULL(mp.LIFE_MONTHS, 0)
		FROM AS_Assets_MP mp
	 WHERE ISNULL(mp.Not_Deprn_Flag, 0) = 0
	   AND ISNULL(mp.Assets_Fixed_Cost, 0) > ISNULL(mp.Deprn_Reserve, 0)
		 AND ((ISNULL(mp.Life_Years, 0) > 0) OR (ISNULL(mp.LIFE_MONTHS, 0) > 0));
BEGIN
	--若折舊金額檔已存在, DELETE 原紀錄
	DELETE
	  FROM AS_deprn_Charges
	 WHERE Assets_Number IN (SELECT Asset_Number
	                           FROM AS_Assets_MP)
		 AND Trans_Type = @Trans_Type;

	--1-新增  --暫時先讓1、2、3做一樣的處理
	IF (@Trx_Code IN (1, 2, 3))
	BEGIN
		OPEN Mp_Cursor;
			FETCH  NEXT FROM Mp_Cursor
			INTO @Office_Branch, @Assets_Number, @Period, @Original_Cost, @Deprn_Reserve
					,@Life_Years, @LIFE_MONTHS
			WHILE (@@FETCH_STATUS = 0)
			BEGIN
				--剩餘期數
				SET @Rest_Counts = @Life_Years * 12 + @LIFE_MONTHS;
				--本期折舊金額
				--最後一期
				IF (@Rest_Counts = 1)
				BEGIN
				  SET @Period_Deprn_Charges = @Original_Cost - @Deprn_Reserve;
				END
				ELSE
				BEGIN
					SET @Period_Deprn_Charges = ROUND(CONVERT(NUMERIC(18,2), (@Original_Cost - @Deprn_Reserve)) / @Rest_Counts, 0);
				END
				--新增每月應折舊金額檔
				INSERT INTO AS_Deprn_Charges(
																			Office_Branch,
																			Trans_Type,
																			Assets_Number,
																			Period,
																			Original_Cost,
																			Deprn_Reserve,
																			Net_Value,
																			Period_Deprn_Charges,
																			Rest_Counts,
																			Last_Updated_Time
				                            )
				VALUES(
				        @Office_Branch,
								@Trans_Type,
								@Assets_Number,
								@Period,
								@Original_Cost,
								@Deprn_Reserve,
								@Original_Cost - @Deprn_Reserve,
								@Period_Deprn_Charges,
								@Rest_Counts,
								GETDATE()
				      )

				FETCH  NEXT FROM Mp_Cursor
				INTO @Office_Branch, @Assets_Number, @Period, @Original_Cost, @Deprn_Reserve
						,@Life_Years, @LIFE_MONTHS
			END
		CLOSE Mp_Cursor;
	END
	--2-修改年限
	ELSE IF (@Trx_Code = 2) 
	BEGIN
		SELECT @Trx_Code;
	END
	--3-修改啟用日期
	ELSE IF (@Trx_Code = 3)
	BEGIN
		SELECT @Trx_Code;
	END

	DEALLOCATE Mp_Cursor;
END
