﻿CREATE PROCEDURE [dbo].[AS_SP_gen_months_deprn_mp] (
  @Branch_Code VARCHAR(3),
	@yyyymm varchar(6),
	@Last_Updated_By NUMERIC(15,0)
)
AS
DECLARE @dd VARCHAR(2) = '01';								                --月初指定日
DECLARE @boyyyymm DATE;                                       --yyyymm的第一天
DECLARE @eoyyyymm DATE;                                       --yyyymm的最後一天
DECLARE @Branch_Code_Def VARCHAR(3) = '001';                  --分行別總行
DECLARE @Book_Type_Def VARCHAR(12) = 'NTD_RE_IFRS';						--帳本類別預設值
DECLARE @Currency_Def VARCHAR(4) = 'NTD';                     --幣別預設值
DECLARE @Department_Def VARCHAR(3) = '08-';                   --部門別-總行
DECLARE @Department_Other VARCHAR(3) = '00-';                 --部門別-他行
DECLARE @Account_Multi VARCHAR(12) = '*';                     --科子細目多筆
DECLARE @Trans_Type_Def VARCHAR(1) = '1';					            --交易種類預設;1動產
DECLARE @Batch_Seq_Tmp VARCHAR(30);                           --序號暫存
DECLARE @SysDateTime DATETIME2;                               --系統時間
/****** 讀取AS_Deprn的欄位 begin ******/
DECLARE @Asset_Category VARCHAR(30);                          --資產類別
/****** 讀取AS_Deprn的欄位 end ******/
/****** 讀取AS_GL_Trade_Account的欄位 begin ******/
DECLARE @Related_No NUMERIC(6,0);                             --
/****** 讀取AS_GL_Trade_Account的欄位 end ******/
/****** 寫入AS_GL_Acc_Deprn的欄位 begin ******/
DECLARE @Book_Type VARCHAR(12);								                --帳本類別
DECLARE @Department VARCHAR(20);                              --部門
DECLARE @Branch_Ind CHAR(1);                                  --總/分行區分
DECLARE @Account VARCHAR(12);                                 --科子細目
DECLARE @Account_Name NVARCHAR(40);                           --科子細目名稱
DECLARE @Asset_Liablty CHAR(1);                               --資產負債別
DECLARE @Category CHAR(1) = 'B';                              --種類
DECLARE @Currency VARCHAR(4);                                 --幣別
DECLARE @GL_Account VARCHAR(20);                              --分行/科子細目/幣別
DECLARE @Master_No NUMERIC(6,0);                              --分錄編號
DECLARE @Open_Type VARCHAR(3) = 'ST';                         --分錄產生時點
DECLARE @Trans_Type VARCHAR(1) = @Trans_Type_Def;					    --交易種類;僅:1動產/3房屋/4租賃權益/ A土地改良物
DECLARE @Account_Key VARCHAR(10) = 'M820';                    --
DECLARE @Trans_Seq VARCHAR(30) = '';                          --交易序號
DECLARE @Trade_Type CHAR(3);                                  --交易區分
DECLARE @Post_Seq INT;                                        --出帳序號
DECLARE @DB_CR CHAR(1);                                       --借/貸
DECLARE @Real_Memo CHAR(1);                                   --實/虛分錄區分
DECLARE @Stat_Balance_Date DATE = NULL;                       --結算日
DECLARE @Status CHAR(1) = '0';                                --帳務狀態
DECLARE @Asset_Number VARCHAR(30);                            --資產編號
DECLARE @Lease_Number VARCHAR(9) = '';                        --租約編號
DECLARE @Virtual_Account VARCHAR(14) = '';                    --虛擬帳號
DECLARE @Payment_User NVARCHAR(60) = '';                      --繳款人姓名
DECLARE @Payment_User_ID VARCHAR(20) = '';                    --繳款人ID
DECLARE @Batch_Seq VARCHAR(20);                               --批次序號;系統產生, 試算批次編號yyyymmddhhmiss+微秒3位共17碼 (同一批次同一個batch_seq,共用)
DECLARE @Import_Number VARCHAR(20) = '';                      --匯入序號
DECLARE @Soft_Lock BIT = 0;                                   --Soft Lock Flag
DECLARE @Close_Date DATE = NULL;                              --關帳日
DECLARE @Stat_Frequency NUMERIC(6,0) = 0;                     --結算週期
DECLARE @Post_Date DATE = NULL;                               --過帳日
DECLARE @Value_Date DATE = NULL;                              --核帳確認日
DECLARE @Settle_Date DATE = NULL;                             --拋轉日
DECLARE @Amount NUMERIC(15,2);                                --金額
DECLARE @Notes NVARCHAR(100) = '';                            --帳務備註
DECLARE @Notes1 NVARCHAR(100) = '';                           --分錄備註
DECLARE @Is_Updated BIT = 0;                                  --更新註記
DECLARE @Updated_Notes NVARCHAR(100) = '';                    --更新備註
DECLARE @Deprn_Months VARCHAR(6);                             --折舊月份
/****** 寫入AS_GL_Acc_Deprn的欄位 end ******/
--依折舊明細檔(AS_Deprn)計算出本月會計科目明細_折舊科目(AS_GL_Acc_Deprn)
DECLARE Dn_Mp_Cursor CURSOR FOR							  
	SELECT Assets_Number, ISNULL(dn.Department, ''), SUBSTRING(ISNULL(dn.Deprn_Source, ''), 1, 1), dn.Asset_Category,
	       Period_Deprn_Charges, Deprn_Months
		FROM AS_Deprn dn
	 WHERE dn.Office_Branch = @Branch_Code
	   AND dn.Trans_Type = @Trans_Type
	   AND dn.Deprn_Months <= @yyyymm
		 AND Is_Post = 0;
BEGIN
  --計算yyyymm的第一天
	SET @boyyyymm = @yyyymm + @dd;
  --計算yyyymm的最後一天
	SET @eoyyyymm = DATEADD(DAY, 1, EOMONTH(@boyyyymm));

	--搬移前月折舊帳務資料
	INSERT INTO AS_GL_Acc_Deprn_His (
														Book_Type, Branch_Code, Department, Branch_Ind, Account, Account_Name, Asset_Liablty, 
														Category, Currency, GL_Account, Master_No, Open_Date, Open_Type, Trans_Type, Account_Key, 
														Trans_Seq, Trade_Type, Post_Seq, DB_CR, Real_Memo, Stat_Balance_Date, Status, Asset_Number, 
														Lease_Number, Virtual_Account, Payment_User, Payment_User_ID, Batch_Seq, Import_Number, 
														Soft_Lock, Close_Date, Stat_Frequency, Post_Date, Value_Date, Settle_Date, Amount, Notes, 
														Notes1, Last_Updated_Time, Last_Updated_By, Is_Updated, Updated_Notes, Deprn_Months
													 )
	SELECT Book_Type, Branch_Code, Department, Branch_Ind, Account, Account_Name, Asset_Liablty, 
				 Category, Currency, GL_Account, Master_No, Open_Date, Open_Type, Trans_Type, Account_Key, 
				 Trans_Seq, Trade_Type, Post_Seq, DB_CR, Real_Memo, Stat_Balance_Date, Status, Asset_Number, 
				 Lease_Number, Virtual_Account, Payment_User, Payment_User_ID, Batch_Seq, Import_Number, 
				 Soft_Lock, Close_Date, Stat_Frequency, Post_Date, Value_Date, Settle_Date, Amount, Notes, 
				 Notes1, Last_Updated_Time, Last_Updated_By, Is_Updated, Updated_Notes, Deprn_Months
	  FROM AS_GL_Acc_Deprn
	 WHERE Branch_Code = @Branch_Code
	   AND Trans_Type = @Trans_Type
		 AND Deprn_Months < @yyyymm;
		 
	DELETE 
		FROM AS_GL_Acc_Deprn
	 WHERE Branch_Code = @Branch_Code
	   AND Trans_Type = @Trans_Type
		 AND Deprn_Months < @yyyymm;

  --刪除本月試算未出帳資料
	DELETE 
		FROM AS_GL_Acc_Deprn
	 WHERE Branch_Code = @Branch_Code
	   AND Trans_Type = @Trans_Type
		 AND Open_Date >= @boyyyymm 
		 AND Open_Date < @eoyyyymm
		 AND Status = 0;

	--依折舊明細檔(AS_Deprn)計算出本月會計科目明細_折舊科目(AS_GL_Acc_Deprn)
  OPEN Dn_Mp_Cursor;
		FETCH  NEXT FROM Dn_Mp_Cursor
		INTO @Asset_Number, @Department, @Branch_Ind, @Asset_Category, 
		     @Amount, @Deprn_Months
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
		  --帳本類別、幣別
			SET @Book_Type = @Book_Type_Def;
			SET @Currency = @Currency_Def;
			SELECT @Book_Type = ISNULL(Book_Type, @Book_Type_Def),
							@Currency = ISNULL(Currency, @Currency_Def)
				FROM AS_Branch_Currency
			 WHERE Branch_Code = @Branch_Code;
			 
			--部門
			SET @Department = CASE WHEN @Branch_Code = @Branch_Code_Def
			                       THEN @Department_Def + @Department
			  										 ELSE @Department_Other + @Department
														 END;

			--分錄編號
			SET @Master_No = 0;
			SELECT @Master_No = ISNULL(Master_No, 0)
			  FROM AS_Deprn_Account
			 WHERE Asset_Category = SUBSTRING(@Asset_Category, 1, 8)
			   AND Trans_Type = @Trans_Type

			IF (@Master_No = 0)
			BEGIN
				PRINT '分錄編號異常:Assets_Number=' + @Asset_Number + ',Asset_Category_code=' + @Asset_Category;
				GOTO NEXT_SUB_FECTH;
			END

			--交易區分
			SELECT @Trade_Type = Trade_Type
			  FROM AS_GL_Trade_Define
			 WHERE Master_No = @Master_No;

			--科子細目相關處理
			DECLARE Ta_Cursor CURSOR FOR							  
				SELECT ISNULL(Account, @Account_Multi), ISNULL(Related_No, 0), DB_CR
					FROM AS_GL_Trade_Account ta
				 WHERE ta.Master_No = @Master_No;

			--出帳序號
			SET @Post_Seq = NEXT VALUE FOR Post_Seq;

			OPEN Ta_Cursor;
				FETCH  NEXT FROM Ta_Cursor
				INTO @Account, @Related_No, @DB_CR
				WHILE (@@FETCH_STATUS = 0)
				BEGIN
				  --科子細目
				  IF (@Account = @Account_Multi)
					BEGIN
					  SELECT @Account = ISNULL(Account, @Account_Multi)
						  FROM AS_GL_Trade_Acc_Sub
						 WHERE Master_No = @Master_No
						   AND Related_No = @Related_No
							 AND Asset_Category_code = SUBSTRING(@Asset_Category, 3, 6)
					END
					
					IF (@Account = @Account_Multi)
					BEGIN
						PRINT '科子細目異常:Assets_Number=' + @Asset_Number + ',Master_No=' + CONVERT(VARCHAR(6), @Master_No) + ',Asset_Category_code=' + @Asset_Category;
						GOTO NEXT_SUB_FECTH;
					END

					--科子細目名稱、資產負債別、實/虛分錄區分
					SELECT @Account_Name = Account_Name, 
					       @Asset_Liablty = Asset_Liablty,
								 @Real_Memo = Account_Type
					  FROM AS_GL_Account
					 WHERE Account = @Account

					--分行/科子細目/幣別
					--branch_Code(3)+department(2)前2碼+account(11)+currency(4) 最多20位
					SET @GL_Account = @Branch_Code + SUBSTRING(@Department, 1, 2) + SUBSTRING(@Account, 1, 11) + @Currency;

					--新增資料 AS_GL_Acc_Deprn會計科目明細檔_本月折舊檔
					INSERT INTO AS_GL_Acc_Deprn(
																			Book_Type,
																			Branch_Code,
																			Department,
																			Branch_Ind,
																			Account,
																			Account_Name,
																			Asset_Liablty,
																			Category,
																			Currency,
																			GL_Account,
																			Master_No,
																			Open_Date,
																			Open_Type,
																			Trans_Type,
																			Account_Key,
																			Trans_Seq,
																			Trade_Type,
																			Post_seq,
																			DB_CR,
																			Real_Memo,
																			Stat_Balance_Date,
																			Status,
																			Asset_Number,
																			Lease_Number,
																			Virtual_Account,
																			Payment_User,
																			Payment_User_ID,
																			Batch_Seq,
																			Import_Number,
																			Soft_Lock,
																			Close_Date,
																			Stat_Frequency,
																			Post_Date,
																			Value_Date,
																			Settle_Date,
																			Amount,
																			Notes,
																			Notes1,
																			Last_Updated_Time,
																			Last_Updated_By,
																			Is_Updated,
																			Updated_Notes,
																			Deprn_Months
					                           )
					VALUES(
					       @Book_Type,
								 @Branch_Code,
								 @Department,
								 @Branch_Ind,
								 @Account,
								 @Account_Name,
								 @Asset_Liablty,
								 @Category,
								 @Currency,
								 @GL_Account,
								 @Master_No,
								 @SysDateTime,
								 @Open_Type,
								 @Trans_Type,
								 @Account_Key,
								 @Trans_Seq,
								 @Trade_Type,
								 @Post_Seq,
								 @DB_CR,
								 @Real_Memo,
								 @Stat_Balance_Date,
								 @Status,
								 @Asset_Number,
								 @Lease_Number,
								 @Virtual_Account,
								 @Payment_User,
								 @Payment_User_ID,
								 @Batch_Seq,
								 @Import_Number,
								 @Soft_Lock,
								 @Close_Date,
								 @Stat_Frequency,
								 @Post_Date,
								 @Value_Date,
								 @Settle_Date,
								 @Amount,
								 @Notes,
								 @Notes1,
								 @SysDateTime,
								 @Last_Updated_By,
								 @Is_Updated,
								 @Updated_Notes,
								 @Deprn_Months
					      );

NEXT_SUB_FECTH:
					FETCH  NEXT FROM Ta_Cursor
					INTO @Account, @Related_No, @DB_CR
				END
			CLOSE Ta_Cursor;
			DEALLOCATE Ta_Cursor;

NEXT_FECTH:
			FETCH  NEXT FROM Dn_Mp_Cursor
			INTO @Asset_Number, @Department, @Branch_Ind, @Asset_Category, 
			     @Amount, @Deprn_Months
		END
	CLOSE Dn_Mp_Cursor;
  DEALLOCATE Dn_Mp_Cursor;
END