﻿CREATE PROCEDURE [dbo].[AS_SP_gen_deprn_build_trans]  (
	@yyyymm varchar(6)
)
AS 
BEGIN 
	DECLARE @yyyymmdd date;

	SET @yyyymmdd = CONVERT(datetime, @yyyymm + '21')

	INSERT INTO AS_Deprn(Office_Branch, Trans_Type, Form_Number, Assets_Number, Doc_No, Deprn_Months, Deprn_Date, Post_Date, Period, Deprn_Source, Original_Cost, Net_Value, Period_Deprn_Charges, Accum_Deprn, After_Deprn_Cost, Addt_Deprn_Months, Addt_Deprn_Amt, Addt_Deprn_Flag, Is_Post, Batch_Seq, Post_Seq, Asset_Category, Notes, Deprn_Reserve, Salvage_Value, Remark, Deprn_Type)
	SELECT bd.Officer_Branch, '3', '', '', '', @yyyymm, @yyyymmdd, null, ISNULL(bd.DEPRN_Counts, 0), CASE WHEN bd.Officer_Branch = '001' THEN 'H' ELSE 'B' END, ISNULL(bd.Current_Cost, 0), ISNULL(bd.Current_Cost, 0), 0, ISNULL(bd.Deprn_Reserve, 0), ISNULL(bd.Current_Cost, 0), 0, 0, 0, 1, '', 0, bd.Asset_Category_code, '', ISNULL(bd.Deprn_Reserve, 0), ISNULL(bd.SALVAGE_VALUE, 0), '', 0
	  FROM AS_Assets_Build bd
	 WHERE ((ISNULL(bd.Current_Cost, 0) - ISNULL(bd.Deprn_Reserve, 0) - ISNULL(bd.Deprn_Amount, 0)) > 0)
	   AND ((ISNULL(bd.Life_years, 0) * 12 + ISNULL(bd.Life_months, 0)) > 0)
END