﻿CREATE PROCEDURE [dbo].[AS_SP_gen_deprn_mp_trans] (
	@yyyymm varchar(6)
)
AS 
BEGIN 
	DECLARE @yyyymmdd date;

	SET @yyyymmdd = CONVERT(datetime, @yyyymm + '21')

	INSERT INTO AS_Deprn(Office_Branch, Trans_Type, Form_Number, Assets_Number, Doc_No, Deprn_Months, Deprn_Date, Post_Date, Period, Deprn_Source, Original_Cost, Net_Value, Period_Deprn_Charges, Accum_Deprn, After_Deprn_Cost, Addt_Deprn_Months, Addt_Deprn_Amt, Addt_Deprn_Flag, Is_Post, Batch_Seq, Post_Seq, Asset_Category, Notes, Deprn_Reserve, Salvage_Value, Remark, Deprn_Type)
	SELECT mp.Office_Branch, '1', '', '', '', @yyyymm, @yyyymmdd, null, ISNULL(mp.DEPRN_Counts, 0), mp.Source, ISNULL(mp.Assets_Fixed_Cost, 0), ISNULL(mp.Assets_Fixed_Cost, 0), 0, ISNULL(mp.Deprn_Reserve, 0), ISNULL(mp.Assets_Fixed_Cost, 0), 0, 0, 0, 1, '', 0, mp.Asset_Category_code, '', ISNULL(mp.Deprn_Reserve, 0), ISNULL(mp.Deprn_Reserve, 0), mp.Remark, 0
	  FROM AS_Assets_MP mp
	 WHERE ((ISNULL(mp.Assets_Fixed_Cost, 0) - ISNULL(mp.Deprn_Reserve, 0)) > 0)
	   AND ((ISNULL(mp.Life_years, 0) * 12 + ISNULL(mp.Life_months, 0)) > 0)
END