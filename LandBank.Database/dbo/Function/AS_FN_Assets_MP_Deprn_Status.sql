-- ===========�ʲ����ª��A=======================
Create FUNCTION [dbo].[AS_FN_Assets_MP_Deprn_Status]
(	
	@date datetime
)
RETURNS TABLE 
AS
RETURN 
(
	 select d.TRX_Header_ID,d.Asset_Number,d.Deprn_Date,d.Post_Date
	 ,d.Life_Month,d.Deprn_Source,d.Deprn_Cost,d.Adjusted_Cost 
	 from AS_Assets_MP_Deprn d
	 inner join
	 (
		 select Asset_Number,max(Deprn_Date) as Deprn_Date
		 from AS_Assets_MP_Deprn
		 where Deprn_Date<=@date
		 group by Asset_Number
	 ) v
	 on d.Asset_Number=v.Asset_Number and d.Deprn_Date=v.Deprn_Date
)


GO


