
CREATE VIEW [dbo].[AS_VW_Asset_Family]
-- view Update by veronika 2017.10.2  AS_VW_Asset_Family 母子資產關係檔
(Layer,Trans_Type,Family_Type,Parent_Code,Main_Kind,Category_Code,Asset_Number,description,Life_Years,Life_Months,Salvage_Value,Not_Deprn_Flag  )
AS
-- 房屋
(select 1,3,1,b.Parent_Asset_Number,substring(b.Asset_Category_Code,1,1),b.Asset_Category_Code,b.Asset_Number,b.description,b.Life_Years,b.Life_Months,b.Salvage_Value,
   (case b.Deprn_type when 0 then 1 when 1 then 0  end)
 from  AS_Assets_Build a with (nolock),AS_Assets_Build b with (nolock)
  where a.Parent_asset_number = b.Asset_Number )
union
 (select 2,3,1,a.Parent_Asset_Number,substring(a.Asset_Category_Code,1,1),a.Asset_Category_Code,a.Asset_Number,a.description,a.Life_Years,a.Life_Months,a.Salvage_Value,
    (case a.Deprn_type when 0 then 1 when 1 then 0  end)
 from  AS_Assets_Build a with (nolock),AS_Assets_Build b with (nolock)
 where a.Parent_asset_number = b.Asset_Number )
--動產
union
(select 1,1,1,b.Assets_Parent_Number,substring(b.Asset_Category_Code,1,1),b.Asset_Category_Code,b.Asset_Number,b.description,b.Life_Years,b.Life_Months,b.Salvage_Value,b.Not_Deprn_Flag
 from  AS_Assets_Mp a with (nolock),AS_Assets_MP b with (nolock)
 where a.Assets_Parent_Number = b.Asset_Number )
union
( select 2,1,1,a.Assets_Parent_Number,substring(a.Asset_Category_Code,1,1),a.Asset_Category_Code,a.Asset_Number,a.description,a.Life_Years,a.Life_Months,a.Salvage_Value,a.Not_Deprn_Flag
 from  AS_Assets_Mp a with (nolock),AS_Assets_MP b with (nolock)
 where a.Assets_Parent_Number = b.Asset_Number )
-- 重大組成
union
 (select 1,3,2,b.Parent_Asset_Number,substring(b.Asset_Category_Code,1,1),b.Asset_Category_Code,b.Asset_Number,b.description,b.Life_Years,b.Life_Months,b.Salvage_Value,
   (case b.Deprn_type when 0 then 1 when 1 then 0  end)
 from  AS_Assets_Build_Mp a with (nolock),AS_Assets_Build b with (nolock)
 where a.Parent_asset_number = b.Asset_Number
union
  select 2,3,2,a.Parent_Asset_Number,substring(a.Asset_Category_Code,1,1),a.Asset_Category_Code,a.Asset_Number,a.description,a.Life_Years,a.Life_Months,a.Salvage_Value,
    (case a.Deprn_type when 0 then 1 when 1 then 0  end)
 from  AS_Assets_Build_Mp a with (nolock),AS_Assets_Build b with (nolock)
 where a.Parent_asset_number = b.Asset_Number)
