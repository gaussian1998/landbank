
CREATE view [dbo].[AS_VW_Code_Table] as
select Class,Class_Name,Code_ID,[Text] from AS_Code_Table where Is_Active = 1 and Cancel_Code = 0 and Class not in ('MPForm')
union
select Class,Parameter1,Code_ID,[Text] from AS_Code_Table where Is_Active = 1 and Cancel_Code = 0 and Class in ('MPForm')
union
select 'Currency',N'幣別',Currency,Currency_Name from AS_Currency
union
select distinct 'City',N'縣市',City_Code,City_Name from AS_Land_Code
--鄉鎮市區
union
select distinct 'District',N'鄉鎮市區',District_Code,District_Name from AS_Land_Code
--段
union
select distinct 'Section',N'段',Section_Code,Sectioni_Name from AS_Land_Code
--小段
union
select distinct 'SubSection',N'小段',Sub_Sectioni_Name,Sub_Sectioni_Name from AS_Land_Code