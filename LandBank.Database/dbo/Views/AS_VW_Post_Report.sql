﻿CREATE VIEW [dbo].[AS_VW_Post_Report] AS
SELECT  dbo.AS_Post_His.Post_ID AS Code, dbo.AS_Post_His.Subject, dbo.AS_Post_His.Post_Startdate AS Start, 
                   dbo.AS_Post_His.Post_Enddate AS [End], dbo.AS_Users.User_Name, CATEGORY.Text AS Category, 
                   DEPARTMENT.Text AS Department
FROM      dbo.AS_Post_His INNER JOIN
                       (SELECT  Class, Code_ID, Text
                        FROM       dbo.AS_Code_Table
                        WHERE    (Class = 'P03')) AS DEPARTMENT ON dbo.AS_Post_His.Post_Department = DEPARTMENT.Code_ID INNER JOIN
                       (SELECT  Class, Code_ID, Text
                        FROM       dbo.AS_Code_Table AS AS_Code_Table_1
                        WHERE    (Class = 'P04')) AS CATEGORY ON dbo.AS_Post_His.ID = CATEGORY.Code_ID INNER JOIN
                   dbo.AS_Users ON dbo.AS_Post_His.User_ID = dbo.AS_Users.ID