﻿CREATE VIEW [dbo].[AS_VW_Department_Report]
	AS SELECT  dbo.AS_Users.User_Name, Department.Text, { fn CONCAT(dbo.AS_Users.User_Code, '@landbank.com.tw') 
                   } AS Email
FROM      dbo.AS_Users INNER JOIN
                       (SELECT  Class, Code_ID, Text
                        FROM       dbo.AS_Code_Table
                        WHERE    (Class = 'C02')) AS Department ON dbo.AS_Users.Department_Code = Department.Code_ID