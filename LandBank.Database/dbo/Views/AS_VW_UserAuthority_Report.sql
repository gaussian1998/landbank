﻿Create View AS_VW_UserAuthority_Report As
    SELECT  U.User_Name AS Name,
			RFH.Transaction_Status AS Status,
			R.Role_Name AS Role,
            (SELECT STUFF(          
                    (SELECT ',' + PG.Programs_Name
						FROM dbo.AS_Programs PG, dbo.AS_Programs_Roles PR
						WHERE PG.ID = PR.Program_ID
						AND R.ID = PR.Role_ID
						FOR XML PATH(''))          
                     ,1          
                     ,1
					 ,'')      
            ) AS Programs,
			RFH.Application_Reson AS Reason,
			RFH.Create_Time AS Time
    FROM dbo.AS_Users U, dbo.AS_RoleFlow_Header RFH, dbo.AS_Role R, dbo.AS_Role_Flow RF
	WHERE U.ID = RFH.Created_By
	AND RFH.ID = RF.RoleFlow_Header_ID