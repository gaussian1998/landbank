
/*?????J???v??*/
CREATE view [dbo].[AS_VW_MP_ImportHis] as
select ImportID,
       Substring(Asset_Number,1,1) as [col1],
       SUBSTRING(Asset_Number,2,6) as [col2],
	   SUBSTRING(Asset_Number,8,1) as [col3],
	   Assets_Category_ID,Assigned_ID,Assets_Name,
	   Assets_Fixed_Cost,Convert(int,(Assets_Fixed_Cost / Assets_Unit)) as [unitprice],Assets_Unit,'' as [unit],
	   Assigned_Branch,Model_Number,PO_Number,Description,
	   isnull(Convert(varchar(10),Post_Date,111),'') as [Post_Date],'' as [accessory equipment],'' as [standardname],Convert(varchar(10),Transaction_Date,111) as [Transaction_Date]
from AS_Assets_MP_ImportHIS