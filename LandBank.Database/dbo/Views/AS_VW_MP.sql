
/*?????T*/
CREATE view [dbo].[AS_VW_MP] as
select m.Asset_Number,m.Asset_Category_Code,m.Assets_Category_ID,m.Assets_Name,m.Model_Number,m.Transaction_Date
,dbo.AS_FN_ChildMP(m.Asset_Number) as Children --????]??
,m.Date_Placed_In_Service
,m.Life_Years,m.Assets_Unit,m.Assets_Fixed_Cost,m.PO_Number,m.Assigned_Branch,h.Remark,m.Location_Disp,m.Deprn_Reserve,m.Salvage_Value
,(m.Assets_Fixed_Cost - m.Deprn_Reserve - m.Salvage_Value) as [Net_Value]
,isnull(d.Life_Month,0) as [Life_Month],isnull(d.Deprn_Cost,0) as [Deprn_Cost],isnull(d.Deprn_Date,'') as [Deprn_Date]
from AS_Assets_MP_Header h
inner join AS_Assets_MP_Record r on h.TRX_Header_ID=r.TRX_Header_ID
--inner join AS_Assets_MP_HIS s on h.TRX_Header_ID=s.TRX_Header_ID
inner join AS_Assets_MP m on r.Asset_Number=m.Asset_Number
left join AS_FN_Assets_MP_Deprn_Status(getdate()) d on m.Asset_Number = d.Asset_Number