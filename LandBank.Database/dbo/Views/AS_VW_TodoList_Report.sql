﻿CREATE VIEW [dbo].[AS_VW_TodoList_Report]
	AS SELECT  dbo.AS_ToDoList.ToDoList_Code AS Code, dbo.AS_Users.User_Name, dbo.AS_ToDoList.ToDoList_Startdate AS Start, 
                   dbo.AS_ToDoList.ToDoList_Enddate AS [End], dbo.AS_ToDoList.Create_Time, STATUS.Text AS Status, 
                   dbo.AS_ToDoList.Subject
FROM      dbo.AS_ToDoList INNER JOIN
                   dbo.AS_Users ON dbo.AS_ToDoList.User_ID = dbo.AS_Users.ID INNER JOIN
                       (SELECT  Class, Code_ID, Text
                        FROM       dbo.AS_Code_Table
                        WHERE    (Class = 'S01')) AS STATUS ON dbo.AS_ToDoList.ToDoList_Status = STATUS.Code_ID