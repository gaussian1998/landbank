﻿CREATE VIEW dbo.Acc_EntriesByHeader
AS
SELECT          A.Post_Date, A.Master_No, A.Branch_Code, tbBranch.Text AS Branch_Name, A.Department, 
                            tbDepartment.Text AS Department_Name, A.Account, tbAccount.Account_Name, 
                            SUM((CASE A.DB_CR WHEN 'D' THEN A.Amount END)) AS DB, SUM((CASE A.DB_CR WHEN 'C' THEN A.Amount END)) 
                            AS CR, A.Currency, A.Notes, tbTradeDefine.Master_Text, A.Asset_Number, A.Lease_Number, A.Trans_Type, 
                            A.Batch_Seq, A.Import_Number, A.Open_Date, A.EntriesType, A.Book_Type, A.Trans_Seq, A.Trade_Type
FROM              (SELECT          ID, Book_Type, Branch_Code, Department, Branch_Ind, Account, Account_Name, Asset_Liablty, 
                                                        Category, Currency, GL_Account, Master_No, Open_Date, Open_Type, Trans_Type, Trade_Type, 
                                                        Account_Key, Trans_Seq, Post_Seq, DB_CR, Real_Memo, Stat_Balance_Date, Status, Asset_Number, 
                                                        Lease_Number, Virtual_Account, Payment_User, Payment_User_ID, Batch_Seq, Import_Number, 
                                                        Soft_Lock, Close_Date, Stat_Frequency, Post_Date, Amount, Value_Date, Settle_Date, Notes, Notes1, 
                                                        Last_Updated_Time, Last_Updated_By, Is_Updated, Updated_Notes, 'Deprn' AS EntriesType
                            FROM               dbo.AS_GL_Acc_Deprn
                            UNION ALL
                            SELECT          ID, Book_Type, Branch_Code, Department, Branch_Ind, Account, Account_Name, Asset_Liablty, 
                                                        Category, Currency, GL_Account, Master_No, Open_Date, Open_Type, Trans_Type, Trade_Type, 
                                                        Account_Key, Trans_Seq, Post_Seq, DB_CR, Real_Memo, Stat_Balance_Date, Status, Asset_Number, 
                                                        Lease_Number, Virtual_Account, Payment_User, Payment_User_ID, Batch_Seq, Import_Number, 
                                                        Soft_Lock, Close_Date, Stat_Frequency, Post_Date, Amount, Value_Date, Settle_Date, Notes, Notes1, 
                                                        Last_Updated_Time, Last_Updated_By, Is_Updated, Updated_Notes, 'Months' AS EntriesType
                            FROM              dbo.AS_GL_Acc_Months) AS A LEFT OUTER JOIN
                            dbo.AS_GL_Trade_Define AS tbTradeDefine ON A.Master_No = tbTradeDefine.Master_No LEFT OUTER JOIN
                            dbo.AS_Code_Table AS tbBranch ON A.Branch_Code = tbBranch.Code_ID AND 
                            tbBranch.Class = 'C01' LEFT OUTER JOIN
                            dbo.AS_Code_Table AS tbDepartment ON A.Department = tbDepartment.Code_ID AND 
                            tbDepartment.Class = 'C02' LEFT OUTER JOIN
                            dbo.AS_GL_Account AS tbAccount ON A.Account = tbAccount.Account
GROUP BY   A.Post_Date, A.Master_No, A.Branch_Code, tbBranch.Text, A.Department, tbDepartment.Text, A.Account, 
                            tbAccount.Account_Name, A.Currency, A.Notes, tbTradeDefine.Master_Text, A.Asset_Number, A.Lease_Number, 
                            A.Trans_Type, A.Batch_Seq, A.Import_Number, A.Open_Date, A.EntriesType, A.Book_Type, A.Trans_Seq, 
                            A.Trade_Type
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "A"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 254
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbTradeDefine"
            Begin Extent = 
               Top = 6
               Left = 292
               Bottom = 136
               Right = 473
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbBranch"
            Begin Extent = 
               Top = 6
               Left = 511
               Bottom = 136
               Right = 727
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbDepartment"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 254
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbAccount"
            Begin Extent = 
               Top = 138
               Left = 292
               Bottom = 268
               Right = 507
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 20
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
        ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'Acc_EntriesByHeader';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N' Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'Acc_EntriesByHeader';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'Acc_EntriesByHeader';

