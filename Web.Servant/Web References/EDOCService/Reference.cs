﻿//------------------------------------------------------------------------------
// <auto-generated>
//     這段程式碼是由工具產生的。
//     執行階段版本:4.0.30319.42000
//
//     對這個檔案所做的變更可能會造成錯誤的行為，而且如果重新產生程式碼，
//     變更將會遺失。
// </auto-generated>
//------------------------------------------------------------------------------

// 
// 原始程式碼已由 Microsoft.VSDesigner 自動產生，版本 4.0.30319.42000。
// 
#pragma warning disable 1591

namespace Web.Servant.Skeleton.EDOCService {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="EDOCServiceSoapBinding", Namespace="https://adm.landbankt.com.tw/EPaper/service/EDOCService")]
    public partial class EDOCService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback applyEDOCOperationCompleted;
        
        private System.Threading.SendOrPostCallback applyQueryOperationCompleted;
        
        private System.Threading.SendOrPostCallback applyQuery2OperationCompleted;
        
        private System.Threading.SendOrPostCallback exportEDOCDataOperationCompleted;
        
        private System.Threading.SendOrPostCallback getCityOperationCompleted;
        
        private System.Threading.SendOrPostCallback getSectionOperationCompleted;
        
        private System.Threading.SendOrPostCallback getVersionOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public EDOCService() {
            this.Url = global::Web.Servant.Skeleton.Properties.Settings.Default.Web_Servant_Skeleton_EDOCService_EDOCService;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event applyEDOCCompletedEventHandler applyEDOCCompleted;
        
        /// <remarks/>
        public event applyQueryCompletedEventHandler applyQueryCompleted;
        
        /// <remarks/>
        public event applyQuery2CompletedEventHandler applyQuery2Completed;
        
        /// <remarks/>
        public event exportEDOCDataCompletedEventHandler exportEDOCDataCompleted;
        
        /// <remarks/>
        public event getCityCompletedEventHandler getCityCompleted;
        
        /// <remarks/>
        public event getSectionCompletedEventHandler getSectionCompleted;
        
        /// <remarks/>
        public event getVersionCompletedEventHandler getVersionCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace="http://webservice.eDoc.secureinside.com", ResponseNamespace="https://adm.landbankt.com.tw/EPaper/service/EDOCService")]
        [return: System.Xml.Serialization.SoapElementAttribute("applyEDOCReturn")]
        public string applyEDOC(string xml) {
            object[] results = this.Invoke("applyEDOC", new object[] {
                        xml});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void applyEDOCAsync(string xml) {
            this.applyEDOCAsync(xml, null);
        }
        
        /// <remarks/>
        public void applyEDOCAsync(string xml, object userState) {
            if ((this.applyEDOCOperationCompleted == null)) {
                this.applyEDOCOperationCompleted = new System.Threading.SendOrPostCallback(this.OnapplyEDOCOperationCompleted);
            }
            this.InvokeAsync("applyEDOC", new object[] {
                        xml}, this.applyEDOCOperationCompleted, userState);
        }
        
        private void OnapplyEDOCOperationCompleted(object arg) {
            if ((this.applyEDOCCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.applyEDOCCompleted(this, new applyEDOCCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace="http://webservice.eDoc.secureinside.com", ResponseNamespace="https://adm.landbankt.com.tw/EPaper/service/EDOCService")]
        [return: System.Xml.Serialization.SoapElementAttribute("applyQueryReturn")]
        public string applyQuery(string bioid) {
            object[] results = this.Invoke("applyQuery", new object[] {
                        bioid});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void applyQueryAsync(string bioid) {
            this.applyQueryAsync(bioid, null);
        }
        
        /// <remarks/>
        public void applyQueryAsync(string bioid, object userState) {
            if ((this.applyQueryOperationCompleted == null)) {
                this.applyQueryOperationCompleted = new System.Threading.SendOrPostCallback(this.OnapplyQueryOperationCompleted);
            }
            this.InvokeAsync("applyQuery", new object[] {
                        bioid}, this.applyQueryOperationCompleted, userState);
        }
        
        private void OnapplyQueryOperationCompleted(object arg) {
            if ((this.applyQueryCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.applyQueryCompleted(this, new applyQueryCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace="http://webservice.eDoc.secureinside.com", ResponseNamespace="https://adm.landbankt.com.tw/EPaper/service/EDOCService")]
        [return: System.Xml.Serialization.SoapElementAttribute("applyQuery2Return")]
        public string applyQuery2(string BIOID, string OID, string CityID, string AreaID, string SectionID, string IR00, string IR49, string LIDN, string StartDate, string EndDate, string ApplyUser) {
            object[] results = this.Invoke("applyQuery2", new object[] {
                        BIOID,
                        OID,
                        CityID,
                        AreaID,
                        SectionID,
                        IR00,
                        IR49,
                        LIDN,
                        StartDate,
                        EndDate,
                        ApplyUser});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void applyQuery2Async(string BIOID, string OID, string CityID, string AreaID, string SectionID, string IR00, string IR49, string LIDN, string StartDate, string EndDate, string ApplyUser) {
            this.applyQuery2Async(BIOID, OID, CityID, AreaID, SectionID, IR00, IR49, LIDN, StartDate, EndDate, ApplyUser, null);
        }
        
        /// <remarks/>
        public void applyQuery2Async(string BIOID, string OID, string CityID, string AreaID, string SectionID, string IR00, string IR49, string LIDN, string StartDate, string EndDate, string ApplyUser, object userState) {
            if ((this.applyQuery2OperationCompleted == null)) {
                this.applyQuery2OperationCompleted = new System.Threading.SendOrPostCallback(this.OnapplyQuery2OperationCompleted);
            }
            this.InvokeAsync("applyQuery2", new object[] {
                        BIOID,
                        OID,
                        CityID,
                        AreaID,
                        SectionID,
                        IR00,
                        IR49,
                        LIDN,
                        StartDate,
                        EndDate,
                        ApplyUser}, this.applyQuery2OperationCompleted, userState);
        }
        
        private void OnapplyQuery2OperationCompleted(object arg) {
            if ((this.applyQuery2Completed != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.applyQuery2Completed(this, new applyQuery2CompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace="http://webservice.eDoc.secureinside.com", ResponseNamespace="https://adm.landbankt.com.tw/EPaper/service/EDOCService")]
        [return: System.Xml.Serialization.SoapElementAttribute("exportEDOCDataReturn")]
        public string exportEDOCData(string guid) {
            object[] results = this.Invoke("exportEDOCData", new object[] {
                        guid});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void exportEDOCDataAsync(string guid) {
            this.exportEDOCDataAsync(guid, null);
        }
        
        /// <remarks/>
        public void exportEDOCDataAsync(string guid, object userState) {
            if ((this.exportEDOCDataOperationCompleted == null)) {
                this.exportEDOCDataOperationCompleted = new System.Threading.SendOrPostCallback(this.OnexportEDOCDataOperationCompleted);
            }
            this.InvokeAsync("exportEDOCData", new object[] {
                        guid}, this.exportEDOCDataOperationCompleted, userState);
        }
        
        private void OnexportEDOCDataOperationCompleted(object arg) {
            if ((this.exportEDOCDataCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.exportEDOCDataCompleted(this, new exportEDOCDataCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace="http://webservice.eDoc.secureinside.com", ResponseNamespace="https://adm.landbankt.com.tw/EPaper/service/EDOCService")]
        [return: System.Xml.Serialization.SoapElementAttribute("getCityReturn")]
        public string getCity() {
            object[] results = this.Invoke("getCity", new object[0]);
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void getCityAsync() {
            this.getCityAsync(null);
        }
        
        /// <remarks/>
        public void getCityAsync(object userState) {
            if ((this.getCityOperationCompleted == null)) {
                this.getCityOperationCompleted = new System.Threading.SendOrPostCallback(this.OngetCityOperationCompleted);
            }
            this.InvokeAsync("getCity", new object[0], this.getCityOperationCompleted, userState);
        }
        
        private void OngetCityOperationCompleted(object arg) {
            if ((this.getCityCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.getCityCompleted(this, new getCityCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace="http://webservice.eDoc.secureinside.com", ResponseNamespace="https://adm.landbankt.com.tw/EPaper/service/EDOCService")]
        [return: System.Xml.Serialization.SoapElementAttribute("getSectionReturn")]
        public string getSection(string cityID) {
            object[] results = this.Invoke("getSection", new object[] {
                        cityID});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void getSectionAsync(string cityID) {
            this.getSectionAsync(cityID, null);
        }
        
        /// <remarks/>
        public void getSectionAsync(string cityID, object userState) {
            if ((this.getSectionOperationCompleted == null)) {
                this.getSectionOperationCompleted = new System.Threading.SendOrPostCallback(this.OngetSectionOperationCompleted);
            }
            this.InvokeAsync("getSection", new object[] {
                        cityID}, this.getSectionOperationCompleted, userState);
        }
        
        private void OngetSectionOperationCompleted(object arg) {
            if ((this.getSectionCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.getSectionCompleted(this, new getSectionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace="http://webservice.eDoc.secureinside.com", ResponseNamespace="https://adm.landbankt.com.tw/EPaper/service/EDOCService")]
        [return: System.Xml.Serialization.SoapElementAttribute("getVersionReturn")]
        public string getVersion() {
            object[] results = this.Invoke("getVersion", new object[0]);
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void getVersionAsync() {
            this.getVersionAsync(null);
        }
        
        /// <remarks/>
        public void getVersionAsync(object userState) {
            if ((this.getVersionOperationCompleted == null)) {
                this.getVersionOperationCompleted = new System.Threading.SendOrPostCallback(this.OngetVersionOperationCompleted);
            }
            this.InvokeAsync("getVersion", new object[0], this.getVersionOperationCompleted, userState);
        }
        
        private void OngetVersionOperationCompleted(object arg) {
            if ((this.getVersionCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.getVersionCompleted(this, new getVersionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void applyEDOCCompletedEventHandler(object sender, applyEDOCCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class applyEDOCCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal applyEDOCCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void applyQueryCompletedEventHandler(object sender, applyQueryCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class applyQueryCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal applyQueryCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void applyQuery2CompletedEventHandler(object sender, applyQuery2CompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class applyQuery2CompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal applyQuery2CompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void exportEDOCDataCompletedEventHandler(object sender, exportEDOCDataCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class exportEDOCDataCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal exportEDOCDataCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void getCityCompletedEventHandler(object sender, getCityCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class getCityCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal getCityCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void getSectionCompletedEventHandler(object sender, getSectionCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class getSectionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal getSectionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    public delegate void getVersionCompletedEventHandler(object sender, getVersionCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2046.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class getVersionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal getVersionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591