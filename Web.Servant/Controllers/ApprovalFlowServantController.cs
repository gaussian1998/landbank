﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.ApprovalFlow;
using Library.Servant.Servant.ApprovalFlow.Models;
using Library.Servant.Servant.Common.Models;


namespace Web.Servant.Skeleton.Controllers
{
    public class ApprovalFlowServantController : ServantController
    {
        [HttpPost]
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return Servant.Index(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Logs(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return Servant.Logs( model.Value ).Stringify();
            });
        }

        private LocalApprovalFlowServant Servant = new LocalApprovalFlowServant();
    }
}