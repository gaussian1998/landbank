﻿using Library.Servant.Servant.MP;
using Library.Servant.Servant.MP.MPModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Communicate;

namespace Web.Servant.Skeleton.Controllers
{
    public class MPServantController : ServantController
    {
        private IMPServant MPServant = new LocalMPServant();
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return MPServant.Index(model).Stringify();
            });
        }

        public ActionResult CreateTRXID(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return new StringResult() { StrResult = MPServant.CreateTRXID(model.Transaction_Type) }.Stringify();
            });
        }


        public ActionResult GetDetail(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return MPServant.GetDetail(model.TRXHeaderID).Stringify();
            });
        }

        public ActionResult HandleHeader(WebOptional optional)
        {
            return Validate<MPHeaderModel>(optional, model => {
                return MPServant.HandleHeader(model).Stringify();
            });
        }

        public ActionResult Create_Asset(WebOptional optional)
        {
            return Validate<MPModel>(optional, model => {
                return MPServant.Create_Asset(model).Stringify();
            });
        }

        public ActionResult GetAssetDetail(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return MPServant.GetAssetDetail(model.ID).Stringify();
            });
        }

        public ActionResult Update_Asset(WebOptional optional)
        {
            return Validate<MPModel>(optional, model => {
                return MPServant.Update_Asset(model).Stringify();
            });
        }

        public ActionResult GetDocID(WebOptional optional)
        {
            int DocID;
            return Validate<SearchModel>(optional, model => {
                return new StringResult() { IsSuccess = MPServant.GetDocID(model.DocCode, out DocID), StrResult = DocID.ToString() }.Stringify();
            });
        }

        public ActionResult UpdateFlowStatus(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return new StringResult() { IsSuccess = MPServant.UpdateFlowStatus(model.TRXHeaderID,model.FlowStatus)}.Stringify();
            });
        }

        public ActionResult CopyAsset(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new StringResult() { IsSuccess = MPServant.CopyAsset(model.CopyAssetID, model.CopyCount) }.Stringify();
            });
        }

        public ActionResult CreateAssetNumber(WebOptional optional)
        {
            return Validate<AssetModel>(optional, model => {
                return new StringResult() { StrResult = MPServant.CreateAssetNumber(model.type1, model.type2,model.type3) }.Stringify();
            });
        }

        public ActionResult DeleteAsset(WebOptional optional)
        {
            return Validate<AssetModel>(optional, model => {
                return new StringResult() { IsSuccess = MPServant.DeleteAsset(model.ID) }.Stringify();
            });
        }

        public ActionResult GetAssetMain_Kind(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new CodeItemModel() { Items = MPServant.GetAssetMain_Kind(model.AssetMainKindType) }.Stringify();
            });
        }

        public ActionResult GetAssetDetail_Kind(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new CodeItemModel() { Items = MPServant.GetAssetDetail_Kind(model.MainKind) }.Stringify();
            });
        }

        public ActionResult GetAssetUse_Types(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new CodeItemModel() { Items = MPServant.GetAssetUse_Types() }.Stringify();
            });

        }

        public ActionResult GetKeep_Position(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new CodeItemModel() { Items = MPServant.GetKeep_Position(model.KeepPosition) }.Stringify();
            });
        }

        public ActionResult GetUser(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new CodeItemModel() { Items = MPServant.GetUser(model.Branch,model.Dept) }.Stringify();
            });
        }

        public ActionResult GetAccountItem(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new StringResult() { StrResult = MPServant.GetAccountItem(model.AssetDetailKind) }.Stringify();
            });
        }

        public ActionResult GetCodeItem(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new CodeItemModel() { Items = MPServant.GetCodeItem(model.CodeClass) }.Stringify();
            });
        }

        public ActionResult SearchAssetsByCategoryCode(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new CodeItemModel() { Items = MPServant.GetCodeItem(model.CodeClass) }.Stringify();
            });
        }

        public ActionResult GetAssetDetailByAssetNumber(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return MPServant.GetAssetDetailByAssetNumber(model.Asset_Number).Stringify();
            });
        }

        public ActionResult SearchAssets(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return MPServant.SearchAssets(model).Stringify();
            });
        }

        public ActionResult CreateMP(WebOptional optional)
        {
            return Validate<MPModel>(optional, model => {
                return MPServant.CreateMP(model).Stringify();
            });
        }

        public ActionResult CreateImport_Num(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return new StringResult() { StrResult = MPServant.CreateImport_Num() }.Stringify();
            });
        }

        public ActionResult CreateToMP(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return MPServant.CreateToMP(model.TRXHeaderID).Stringify();
            });
        }

        public ActionResult CreateImport(WebOptional optional)
        {
            return Validate<MPImportPostModel>(optional, model => {
                return MPServant.CreateImport(model.import,model.AssetList).Stringify();
            });
        }

        public ActionResult GetHeader(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return MPServant.GetHeader(model.AssetNumber, model.Transaction_Type).Stringify();
            });
        }

        public ActionResult UpdateToMP(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return MPServant.UpdateToMP(model.TRXHeaderID).Stringify();
            });
        }

        public ActionResult ImportIndex(WebOptional optional)
        {
            return Validate<ImportSearchModel>(optional, model => {
                return MPServant.ImportIndex(model).Stringify();
            });
        }

        public ActionResult CreateImportReview(WebOptional optional)
        {
            return Validate<MPImportPostModel>(optional, model => {
                return MPServant.CreateImportReview(model.IRID,model.ImportList,model.Header).Stringify();
            });
        }

        public ActionResult GetImportData(WebOptional optional)
        {
            return Validate<MPImportPostModel>(optional, model => {
                return new MPImportPostModel() { ImportHis  = MPServant.GetImportData(model.ImportID) }.Stringify();
            });
        }

        public ActionResult GetImportReviewDetail(WebOptional optional)
        {
            return Validate<MPImportPostModel>(optional, model => {
                return MPServant.GetImportReviewDetail(model.IRID).Stringify();
            });
        }

        public ActionResult ImportReviewIndex(WebOptional optional)
        {
            return Validate<MPImportPostModel>(optional, model => {
                return MPServant.ImportReviewIndex().Stringify();
            });
        }

        public ActionResult GetImportAssets(WebOptional optional)
        {
            return Validate<MPImportPostModel>(optional, model => {
                return MPServant.GetImportAssets(model.ImportID).Stringify();
            });
        }

        public ActionResult GetImportAssetDetail(WebOptional optional)
        {
            return Validate<MPImportPostModel>(optional, model => {
                return MPServant.GetImportAssetDetail(model.ID).Stringify();
            });
        }

        public ActionResult DeprnRecordModel(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new MPModel() { DeprnRecord = MPServant.GetDeprnRecord(model.Asset_Number) }.Stringify();
            });
        }

    }
}