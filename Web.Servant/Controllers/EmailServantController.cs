﻿using Library.Servant.Communicate;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Email;
using Library.Servant.Servant.Email.Models;
using System.Web.Mvc;

namespace Web.Servant.Skeleton.Controllers
{
    public class EmailServantController : ServantController
    {
        [HttpPost]
        public ActionResult IsExist(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                return Servant.IsExist().Stringify();
            });
        }

        [HttpPost]
        public ActionResult Send(WebOptional optional)
        {
            return Validate<EmailModel>(optional, model => {

                return Servant.Send(model).Stringify();
            });
        }

        private static readonly LocalEmailServant Servant = new LocalEmailServant();
    }
}