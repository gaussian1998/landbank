﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Library.Entity.Edmx;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.CodeTableSetting;
using Library.Servant.Servant.CodeTableSetting.Models;

namespace Web.Servant.Skeleton.Controllers
{
    public class CodeTableServantController : ServantController
    {
        public ActionResult Detail(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {

                return Servant.Detail(model.Value).Stringify();
            });
        }

        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return Servant.Index(model).Stringify();
            });
        }

        public ActionResult SaveSetting(WebOptional optional)
        {
            return Validate<CodeTableInfo> (optional, model => {

                return Servant.SaveSetting(model).Stringify();
            });
        }

        public ActionResult SaveSettings(WebOptional optional)
        {
            return Validate<ListRemoteModel<CodeTableInfo>>(optional, model => {

                return Servant.SaveSettings(model).Stringify();
            });
        }

        private ICodeTableSetting Servant = new LocalCodeTableSetting();
    }
}
