﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Library.Entity.Edmx;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.PostsManagement;
using Library.Servant.Servant.PostsManagement.Models;

namespace Web.Servant.Skeleton.Controllers
{
    public class PostsManagementServantController : ServantController
    {
        public ActionResult Delete(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {

                return Servant.Delete(model.Value).Stringify();
            });
        }

        public ActionResult Detail(WebOptional optional)
        {
            return Validate<PostInfo>(optional, model => {

                return Servant.Detail(model).Stringify();
            });
        }

        public List<AS_VW_Post_Report> GetReportContent(ReportSearchModel rsm)
        {
            throw new NotImplementedException();
        }

        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return Servant.Index(model).Stringify();
            });
        }

        public ActionResult SaveForm(WebOptional optional)
        {
            return Validate<HeaderModel>(optional, model => {

                return Servant.SaveForm(model).Stringify();
            });
        }

        public ActionResult SavePost(WebOptional optional)
        {
            return Validate<PostInfo>(optional, model => {

                return Servant.SavePost(model).Stringify();
            });
        }

        public ActionResult Search(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {

                return Servant.Search(model.Value).Stringify();
            });
        }

        public ActionResult Submit(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {

                return Servant.Submit(model.Value).Stringify();
            });
        }

        private LocalPostsManagementServant Servant = new LocalPostsManagementServant();
    }
}
