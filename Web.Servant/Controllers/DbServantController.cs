﻿using Library.Servant.Communicate;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Db;
using System.Web.Mvc;

namespace Web.Servant.Skeleton.Controllers
{
    public class DbServantController :  ServantController
    {
        [HttpPost]
        public ActionResult IsExist(WebOptional optional)
        {
            return Validate<VoidModel>( optional, model => {

                return Servant.IsExist().Stringify();
            });
        }

        private static readonly LocalDbServant Servant = new LocalDbServant();
    }
}