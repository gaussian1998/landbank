﻿using Library.Servant.Servant.Land;
using Library.Servant.Servant.Land.LandModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Communicate;

namespace Web.Servant.Skeleton.Controllers
{
    public class LandServantController : ServantController
    {
        private ILandServant LandServant = new LocalLandServant();
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return LandServant.Index(model).Stringify();
            });
        }
        public ActionResult IndexQuery(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return LandServant.Index(model).Stringify();
            });
        }

        public ActionResult CreateTRXID(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return new StringResult() { StrResult = LandServant.CreateTRXID(model.Transaction_Type) }.Stringify();
            });
        }


        public ActionResult GetDetail(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return LandServant.GetDetail(model.TRXHeaderID).Stringify();
            });
        }
        public ActionResult GetMPDetail(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return LandServant.GetMPDetail(model.TRXHeaderID).Stringify();
            });
        }
        public ActionResult GetRetireDetail(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return LandServant.GetRetireDetail(model.TRXHeaderID).Stringify();
            });
        }

        public ActionResult HandleHeader(WebOptional optional)
        {
            return Validate<LandHeaderModel>(optional, model => {
                return LandServant.HandleHeader(model).Stringify();
            });
        }

        public ActionResult UpdateHeader(WebOptional optional)
        {
            return Validate<LandHeaderModel>(optional, model => {
                return LandServant.HandleHeader(model).Stringify();
            });
        }
        public ActionResult Create_Asset(WebOptional optional)
        {
            return Validate<LandModel>(optional, model => {
                return LandServant.Create_Asset(model).Stringify();
            });
        }
        public ActionResult CreateLandMP(WebOptional optional)
        {
            return Validate<LandMPModel>(optional, model => {
                return LandServant.CreateLandMP(model).Stringify();
            });
        }
        public ActionResult Create_MPAsset(WebOptional optional)
        {
            return Validate<LandMPModel>(optional, model => {
                return LandServant.Create_MPAsset(model).Stringify();
            });
        }
        public ActionResult Create_MPAssetChange(WebOptional optional)
        {
            return Validate<LandMPModel>(optional, model => {
                return LandServant.Create_MPAssetChange(model).Stringify();
            });
        }
        public ActionResult Create_MPAssetLefeChange(WebOptional optional)
        {
            return Validate<LandMPModel>(optional, model => {
                return LandServant.Create_MPAssetLefeChange(model).Stringify();
            });
        }
        public ActionResult Create_MPAssetRetire(WebOptional optional)
        {
            return Validate<LandMPModel>(optional, model => {
                return LandServant.Create_MPAssetRetire(model).Stringify();
            });
        }
        public ActionResult GetAssetLandDetail(WebOptional optional)
        {
            return Validate<AssetsDetailSearchModel>(optional, model => {
                return LandServant.GetAssetLandDetail(model.ID, model.TableName).Stringify();
            });
        }
        public ActionResult GetAssetLandMPDetail(WebOptional optional)
        {
            return Validate<AssetsDetailSearchModel>(optional, model => {
                return LandServant.GetAssetLandMPDetail(model.ID, model.TableName).Stringify();
            });
        }
        public ActionResult GetAssetLandRetireDetail(WebOptional optional)
        {
            return Validate<AssetsDetailSearchModel>(optional, model => {
                return LandServant.GetAssetLandRetireDetail(model.ID).Stringify();
            });
        }
        public ActionResult GetAssetLandMPRetireDetail(WebOptional optional)
        {
            return Validate<AssetsDetailSearchModel>(optional, model => {
                return LandServant.GetAssetLandMPRetireDetail(model.ID).Stringify();
            });
        }

        public ActionResult Update_Asset(WebOptional optional)
        {
            return Validate<LandModel>(optional, model => {
                return LandServant.Update_Asset(model).Stringify();
            });
        }
        public ActionResult Update_MPAsset(WebOptional optional)
        {
            return Validate<LandMPModel>(optional, model => {
                return LandServant.Update_MPAsset(model).Stringify();
            });
        }

        public ActionResult Update_RetireAsset(WebOptional optional)
        {
            return Validate<LandModel>(optional, model => {
                return LandServant.Update_RetireAsset(model).Stringify();
            });
        }

        public ActionResult Update_SplitAsset(WebOptional optional)
        {
            return Validate<LandModel>(optional, model => {
                return LandServant.Update_SplitAsset(model).Stringify();
            });
        }
        public ActionResult Update_MergeAsset(WebOptional optional)
        {
            return Validate<LandModel>(optional, model => {
                return LandServant.Update_MergeAsset(model).Stringify();
            });
        }
        public ActionResult Update_AssetChangClass(WebOptional optional)
        {
            return Validate<LandModel>(optional, model => {
                return LandServant.Update_AssetChangClass(model).Stringify();
            });
        }
        public ActionResult Update_AssetMPChangClass(WebOptional optional)
        {
            return Validate<LandMPModel>(optional, model => {
                return LandServant.Update_AssetMPChangClass(model).Stringify();
            });
        }
        public ActionResult Update_AssetMPLifeChang(WebOptional optional)
        {
            return Validate<LandMPModel>(optional, model => {
                return LandServant.Update_AssetMPLifeChang(model).Stringify();
            });
        }
        public ActionResult Update_MPAssetRetire(WebOptional optional)
        {
            return Validate<LandMPModel>(optional, model => {
                return LandServant.Update_MPAssetRetire(model).Stringify();
            });
        }
        public ActionResult GetDocID(WebOptional optional)
        {
            int DocID;
            return Validate<SearchModel>(optional, model => {
                return new StringResult() { IsSuccess = LandServant.GetDocID(model.DocCode, out DocID), StrResult = DocID.ToString() }.Stringify();
            });
        }

        public ActionResult UpdateFlowStatus(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return new StringResult() { IsSuccess = LandServant.UpdateFlowStatus(model.TRXHeaderID,model.FlowStatus)}.Stringify();
            });
        }

        public ActionResult CreateAssetNumber(WebOptional optional)
        {
            return Validate<AssetLandModel>(optional, model => {
                return new StringResult() { StrResult = LandServant.CreateAssetNumber(model.type1, model.type2) }.Stringify();
            });
        }
        public ActionResult CreateMPAssetNumber(WebOptional optional)
        {
            return Validate<AssetLandModel>(optional, model => {
                return new StringResult() { StrResult = LandServant.CreateMPAssetNumber(model.type1, model.type2) }.Stringify();
            });
        }

        public ActionResult Delete_Asset(WebOptional optional)
        {
            return Validate<LandModel>(optional, model => {
                
                return LandServant.Delete_Asset(model).Stringify();
            });
        }

        public ActionResult Delete_MPAsset(WebOptional optional)
        {
            return Validate<LandMPModel>(optional, model => {
                
                return LandServant.Delete_MPAsset(model).Stringify();
            });
        }
        public ActionResult Delete_AssetRetire(WebOptional optional)
        {
            return Validate<LandModel>(optional, model => {

                return LandServant.Delete_AssetRetire(model).Stringify();
            });
        }
        public ActionResult Delete_MPAssetRetire(WebOptional optional)
        {
            return Validate<LandMPModel>(optional, model => {

                return LandServant.Delete_MPAssetRetire(model).Stringify();
            });
        }
        public ActionResult SearchAssets(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return LandServant.SearchAssets(model).Stringify();
            });
        }
        public ActionResult SearchAssetsMP(WebOptional optional)
        {
            return Validate<AssetsMPSearchModel>(optional, model => {
                return LandServant.SearchAssetsMP(model).Stringify();
            });
        }

        public ActionResult SearchAssetsByCategoryCode(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                System.IO.File.AppendAllText("D:\\Temp\\Logs.txt", "model.CategoryCode:" + model.CategoryCode+System.Environment.NewLine);
                return new CodeItemModel() { Items = LandServant.SearchAssetsByCategoryCode(model.CategoryCode) }.Stringify();
            });
        }
        public ActionResult CreateLand(WebOptional optional)
        {
            return Validate<LandModel>(optional, model => {
                return LandServant.CreateLand(model).Stringify();
            });
        }
        
        public ActionResult CreateToLand(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return LandServant.CreateToLand(model.TRXHeaderID).Stringify();
            });
        }



        public ActionResult GetHeaderData(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return LandServant.GetHeaderData(model.TRXHeaderID).Stringify();
            });
        }
        public ActionResult GetHeader(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return LandServant.GetHeader(model.AssetNumber, model.Transaction_Type).Stringify();
            });
        }

        public ActionResult UpdateToLand(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return LandServant.UpdateToLand(model.TRXHeaderID).Stringify();
            });
        }

        public ActionResult GetDistrict(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new CodeItemModel() { Items = LandServant.GetDistrict(model.City_Code) }.Stringify();
            });
        }
        public ActionResult GetSection(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new CodeItemModel() { Items = LandServant.GetSection(model.City_Code, model.District_Code) }.Stringify();
            });
        }
        public ActionResult GetSubSection(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new CodeItemModel() { Items = LandServant.GetSubSection(model.City_Code, model.District_Code, model.Section_Code) }.Stringify();
            });
        }
        public ActionResult GetAssetMain_Kind(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new CodeItemModel() { Items = LandServant.GetAssetMain_Kind(model.AssetMainKindType) }.Stringify();
            });
        }

        public ActionResult GetAssetDetail_Kind(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new CodeItemModel() { Items = LandServant.GetAssetDetail_Kind(model.MainKind) }.Stringify();
            });
        }

        public ActionResult GetAssetUse_Types(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new CodeItemModel() { Items = LandServant.GetAssetUse_Types() }.Stringify();
            });

        }
        public ActionResult GetCodeItem(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return new CodeItemModel() { Items = LandServant.GetCodeItem(model.CodeClass) }.Stringify();
            });
        }
    }
}