﻿using Library.Servant.Servant.MU;
using Library.Servant.Servant.MU.MUModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Communicate;
using System.Xml;
using System.Xml.Linq;

namespace Web.Servant.Skeleton.Controllers
{
    public class MUServantController : ServantController
    {

        private IMUServant MUServant = new LocalMUServant();
        
        public ActionResult EDOCServiceTest(WebOptional optional)
        {
            EDOCService.EDOCService a = new EDOCService.EDOCService();
            return Validate<EJObject>(optional, model => {
                return a.applyEDOC("");
            });
           
           
        }
       
        
        public ActionResult ApplyQuery(WebOptional optional)
        {
            EDOCService.EDOCService a = new EDOCService.EDOCService();
            return Validate<EString>(optional, model => {
                EString res = new EString();
               
                var aa = XDocument.Parse(a.applyQuery(model.type));

                 XElement root = aa.Element("EDOCQuery").Element("RespMsg");
                if (root.Value.Trim() == "查詢成功")
                    res.type = "GUID:" + aa.Element("EDOCQuery").Element("Result").Element("EDOCData").Element("GUID").Value;
                else
                    res.type = root.Value;
                return res.Stringify();
            });
        }
        
        public ActionResult ApplyEDOC(WebOptional optional)
        {
            EDOCService.EDOCService a = new EDOCService.EDOCService();
            return Validate<EString>(optional, model => {
                EString res = new EString();
                
                var aa = XDocument.Parse(a.applyEDOC(model.type));
         
                var root = aa.Element("EDOCAPPLY").Element("RespMsg");
               
               
                res.type = root.Value;
                return res.Stringify();
            });
        }
        public ActionResult ExportEDOCData(WebOptional optional)
        {
            EDOCService.EDOCService a = new EDOCService.EDOCService();
            return Validate<EString>(optional, model => {
                EString res = new EString();

                
                    res.type = a.exportEDOCData(model.type);
               

                return res.Stringify();
            });
        }
        public ActionResult ImportLandData(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return MUServant.ImportLandData(model).Stringify();
            });
        }
        public ActionResult GetDetail2(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return MUServant.GetDetail2(model).Stringify();
            });
        }
        
        public ActionResult GetLand(WebOptional optional)
        {
            
            return Validate<EJObject>(optional, model => {
              
                return MUServant.GetLand(model).Stringify();
            });
        }
        
        public ActionResult GetLandF300(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return MUServant.GetLandF300(model).Stringify();
            });
        }
        public ActionResult GetApplyOrder(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return MUServant.GetApplyOrder(model).Stringify();
            });
        }
        public ActionResult GetMaxNo(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return MUServant.GetMaxNo(model).Stringify();
            });
        }
        public ActionResult GetDetail(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return MUServant.GetDetail(model).Stringify();
            });
        }
        public ActionResult Index(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return MUServant.Index(model).Stringify();
            });
        }
      
      
        public ActionResult GetTarget(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return MUServant.GetTarget(model).Stringify();
            });
        }
        public ActionResult DocumentType(WebOptional optional)
        {
            return Validate<EInt>(optional, model => {
                return MUServant.DocumentType(model).Stringify();
            });
        }
        public ActionResult DocumentTypeOne(WebOptional optional)
        {
            return Validate<EInt>(optional, model => {
                return MUServant.DocumentTypeOne(model).Stringify();
            });
        }
        public ActionResult GetDPM(WebOptional optional)
        {
            return Validate<EInt>(optional, model => {
                return MUServant.GetDPM(model).Stringify();
            });
        }
        public ActionResult GetOrder(WebOptional optional)
        {
            return Validate<EString>(optional, model => {
                return MUServant.GetOrder(model).Stringify();
            });
        }
        
        public ActionResult GetCode(WebOptional optional)
        {
            return Validate<EString>(optional, model => {
                return MUServant.GetCode(model).Stringify();
            });
        }
        public ActionResult UpdateOrder(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return MUServant.UpdateOrder(model).Stringify();
            });

        }
        public ActionResult UpdateF200Order(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return MUServant.UpdateF200Order(model).Stringify();
            });

        }
    }
}