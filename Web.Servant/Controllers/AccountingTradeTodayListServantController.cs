﻿using Library.Servant.Servant.AccountingTradeTodayList;
using System.Web.Mvc;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Communicate;

namespace Web.Servant.Skeleton.Controllers
{
    public class AccountingTradeTodayListServantController : ServantController
    {
        [HttpPost]
        public ActionResult Options(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                return Servant.Options().Stringify();
            });
        }

        [HttpPost]
        public ActionResult Index(WebOptional optional)
        {
            return Validate<BranchSearchModel>(optional, model => {

                return Servant.Index(model).Stringify();
            });
        }

        private LocalAccountingTradeTodayListServant Servant = new LocalAccountingTradeTodayListServant();
    }
}