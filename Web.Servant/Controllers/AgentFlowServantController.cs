﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.AgentFlow;
using Library.Servant.Servant.AgentFlow.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common;

namespace Web.Servant.Skeleton.Controllers
{
    public class AgentFlowServantController : ServantController
    {
        [HttpPost]
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return AgentFlowServant.Index(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult New(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                return AgentFlowServant.New().Stringify();
            });
        }
        [HttpPost]
        public ActionResult NewByOprationType(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {

                return AgentFlowServant.New(model.Value).Stringify();
            });
        }
        [HttpPost]
        public ActionResult Create(WebOptional optional)
        {
            return Validate<CreateModel>(optional, model => {

                return AgentFlowServant.Create(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Update(WebOptional optional)
        {
            return Validate<UpdateModel>(optional, model => {

                return AgentFlowServant.Update(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Delete(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return AgentFlowServant.Delete(model.Value).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Edit(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return AgentFlowServant.Edit(model.Value).Stringify();
            });
        }

        private LocalAgentFlowServant AgentFlowServant = new LocalAgentFlowServant();
    }
}