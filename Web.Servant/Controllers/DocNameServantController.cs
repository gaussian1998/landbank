﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.DocName;
using Library.Servant.Servant.DocName.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common;

namespace Web.Servant.Skeleton.Controllers
{
    public class DocNameServantController : ServantController
    {
        [HttpPost]
        public ActionResult New(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                return DocNameServant.New().Stringify();
            });
        }

        [HttpPost]
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return DocNameServant.Index(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Details(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return DocNameServant.Details(model.Value).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Create(WebOptional optional)
        {
            return Validate<CreateModel>(optional, model => {

                return DocNameServant.Create(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Update(WebOptional optional)
        {
            return Validate<UpdateModel>(optional, model => {

                return DocNameServant.Update(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Delete(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return DocNameServant.Delete(model.Value).Stringify();
            });
        }

        private IDocNameServant DocNameServant = new LocalDocNameServant();
    }
}