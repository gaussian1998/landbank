﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.ApprovalMember;
using Library.Servant.Servant.ApprovalMember.Models;
using Library.Servant.Servant.Common.Models;

namespace Web.Servant.Skeleton.Controllers
{
    public class ApprovalMemberServantController : ServantController
    {
        [HttpPost]
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return Servant.Index(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult IndexAll(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return Servant.IndexAll(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Edit(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return Servant.Edit( model.Value ).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Update(WebOptional optional)
        {
            return Validate<UpdateRoleModel>(optional, model => {

                return Servant.Update(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult BatchUpdate(WebOptional optional)
        {
            return Validate<BatchUpdateModel>(optional, model => {

                return Servant.BatchUpdate(model).Stringify();
            });
        }

        private LocalApprovalMemberServant Servant = new LocalApprovalMemberServant();
    }
}