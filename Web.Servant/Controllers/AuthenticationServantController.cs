﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.Authentication;
using Library.Servant.Servant.Authentication.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;

namespace Web.Servant.Skeleton.Controllers
{
    public class AuthenticationServantController : ServantController
    {
        [HttpPost]
        public ActionResult IsExist(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                return Servant.IsExist().Stringify();
            });
        }

        public ActionResult Login(WebOptional optional)
        {
            return Validate<LoginModel>( optional, model => {

                return Servant.Login(model.UserID, model.Password).Stringify();
            });
        }

        public ActionResult LoginRecord(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return Servant.LoginRecord( model.Value, Request.UserHostAddress ).Stringify();
            });
        }

        public ActionResult Logout(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return Servant.Logout( model.Value, Request.UserHostAddress).Stringify();
            });
        }

        private readonly LocalAuthenticationServant Servant = new LocalAuthenticationServant();
    }
}