﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Library.Entity.Edmx;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.UserConfig;
using Library.Servant.Servant.UserConfig.Models;

namespace Web.Servant.Skeleton.Controllers
{
    public class UserConfigServantController : ServantController
    {
        public ActionResult Index(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return Servant.Index(model.Value).Stringify();
            });
        }

        public ActionResult Update(WebOptional optional)
        {
            return Validate<UserInfoModel> (optional, model => {

                return Servant.Update(model).Stringify();
            });
        }

        public ActionResult GetUserName(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {

                return new StringModel { Value = Servant.GetUserName(model.Value) }.Stringify();
            });
        }

        private IUserConfigServant Servant = new LocalUserConfigServant();
    }
}
