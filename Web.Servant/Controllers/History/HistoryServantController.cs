﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Library.Entity.Edmx;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.HistoryData;
using Library.Servant.Servant.HistoryData.Models;

namespace Web.Servant.Skeleton.Controllers
{
    public class HistoryServantController : ServantController
    {
        public ActionResult GetHistory(WebOptional optional)
        {
            return Validate<HistoryCondition>(optional, model => {

                return Servant.GetHistory(model).Stringify();
            });
        }

        public ActionResult GetHistoryList(WebOptional optional)
        {
            return Validate<ListRemoteModel<VoidModel>>(optional, model => {

                return Servant.GetHistoryList().Stringify();
            });
        }

        public ActionResult GetSettings(WebOptional optional)
        {
            return Validate<SettingCondition> (optional, model => {

                return Servant.GetSettings(model).Stringify();
            });
        }

        public ActionResult SaveSettings(WebOptional optional)
        {
            return Validate<ListRemoteModel<SettingItemsInfo>>(optional, model => {

                return Servant.SaveSettings(model).Stringify();
            });
        }

        public ActionResult GetCsvContent(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {

                return Servant.GetCsvContent(model.Value).Stringify();
            });
        }

        private LocalHistoryData Servant = new LocalHistoryData();
    }
}
