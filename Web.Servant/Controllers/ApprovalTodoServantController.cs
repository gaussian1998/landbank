﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.ApprovalTodo;
using Library.Servant.Servant.ApprovalTodo.Models;
using Library.Servant.Servant.Common.Models;

namespace Web.Servant.Skeleton.Controllers
{
    public class ApprovalTodoServantController : ServantController
    {
        [HttpPost]
        public ActionResult IndexApproval(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return Servant.IndexApproval(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult IndexRevoke(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return Servant.IndexRevoke(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult IndexReassign(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return Servant.IndexReassign(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult IndexWithdraw(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return Servant.IndexWithdraw(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Edit(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return Servant.Edit(model.Value).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Form(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return Servant.Form(model.Value).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Withdraw(WebOptional optional)
        {
            return Validate<WithdrawModel>(optional, model => {

                return Servant.Withdraw(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Revoke(WebOptional optional)
        {
            return Validate<RevokeModel>(optional, model => {

                return Servant.Revoke(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Approval(WebOptional optional)
        {
            return Validate<ApprovalModel>(optional, model => {

                return Servant.Approval(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Reject(WebOptional optional)
        {
            return Validate<RejectModel>(optional, model => {

                return Servant.Reject(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult RejectTo(WebOptional optional)
        {
            return Validate<RejectToModel>(optional, model => {

                return Servant.RejectTo(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Reassign(WebOptional optional)
        {
            return Validate<ReassignModel>(optional, model => {

                return Servant.Reassign(model).Stringify();
            });
        }

        private readonly LocalApprovalTodoServant Servant = new LocalApprovalTodoServant();
    }
}