﻿using Library.Servant.Communicate;
using Library.Servant.Servant.Build;
using Library.Servant.Servant.Build.BuildModels;
using Library.Servant.Servant.MPBuild.BuildModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Servant.Skeleton.Controllers
{
    public class BuildServantController : ServantController
    {
        private IBuildServant BuildServant = new LocalBuildServant();

        public ActionResult HeaderAndAsset_Del(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return new StringResult() { IsSuccess = BuildServant.HeaderAndAsset_Del(model.Form_Number,model.Type) }.Stringify();
            });

            //return Validate<SearchModel>(optional, model => {
            //    return BuildServant.HeaderAndAsset_Del(model.Form_Number).Stringify();
            //});
        }
        public ActionResult HeaderAndMPAsset_Del(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return new StringResult() { IsSuccess = BuildServant.HeaderAndMPAsset_Del(model.Form_Number, model.Type) }.Stringify();
            });
        }
        public ActionResult Asset_Del(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return BuildServant.Asset_Del(model.id, model.Type).Stringify();
            });
        }
        public ActionResult MPAsset_Del(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return BuildServant.MPAsset_Del(model.id, model.Type).Stringify();
            });
        }
        public ActionResult CreateBuild(WebOptional optional)
        {
            return Validate<BuildModel>(optional, model => {
                return BuildServant.CreateBuild(model).Stringify();
            });
        }
        public ActionResult CreateBuildMP(WebOptional optional)
        {
            return Validate<BuildMPModel>(optional, model => {
                return BuildServant.CreateBuildMP(model).Stringify();
            });
        }
        public ActionResult CreateToBuild(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return BuildServant.CreateToBuild(model.TRXHeaderID).Stringify();
            });     
        }
        public ActionResult CreateToBuildMP(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return BuildServant.CreateToBuildMP(model.TRXHeaderID).Stringify();
            });
        }
        public ActionResult CreateBuildFormNumber(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return new StringResult() { StrResult = BuildServant.CreateBuildFormNumber(model.Type) }.Stringify();
            });
        }
        public ActionResult CreateTRXID(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return new StringResult() { StrResult = BuildServant.CreateTRXID(model.Type) }.Stringify();
            });
        }
        public ActionResult Create_Asset(WebOptional optional)
        {
            return Validate<BuildModel>(optional, model => {
                return BuildServant.Create_Asset(model).Stringify();
            });
        }
        public ActionResult Create_AssetMP(WebOptional optional)
        {
            return Validate<BuildMPModel>(optional, model => {
                return BuildServant.Create_AssetMP(model).Stringify();
            });
        }
        public ActionResult GetAssetDetail(WebOptional optional) //---
        {
            BuildTRXHeaderModel header;
            return Validate<SearchModel>(optional, model => {
                return new StringResult() { AssetModel = BuildServant.GetAssetDetail(model.id, out header), BuildTRXHeaderModel = header }.Stringify();
            });
        }
        public ActionResult GetAssetDetailByAssetNumber(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return BuildServant.GetAssetDetailByAssetNumber(model.AssetNumber).Stringify();
            });
        }
        public ActionResult GetAssetMPDetail(WebOptional optional) //---
        {
            BuildTRXHeaderModel header;
            return Validate<SearchModel>(optional, model => {
                return new StringResult() { AssetMPModel = BuildServant.GetAssetMPDetail(model.id, out header), BuildTRXHeaderModel = header }.Stringify();
            });
        }
        public ActionResult GetDetail(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return BuildServant.GetDetail(model.idStr).Stringify();
            });
        }
        public ActionResult GetDocID(WebOptional optional) //---
        {
            int DocID;
            return Validate<SearchModel>(optional, model => {
                return new StringResult() { IsSuccess = BuildServant.GetDocID(model.DocCode, out DocID), StrResult = DocID.ToString() }.Stringify();
            });
        }
        public ActionResult GetMPDetail(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return BuildServant.GetMPDetail(model.idStr).Stringify();
            });
        }
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return BuildServant.Index(model).Stringify();
            });
        }
        public ActionResult SearchAssets(WebOptional optional)
        {
            return Validate<AssetsSearchModel>(optional, model => {
                return BuildServant.SearchAssets(model).Stringify();
            });
        }
        public ActionResult SearchAssetsMP(WebOptional optional)
        {
            return Validate<AssetsMPSearchModel>(optional, model => {
                return BuildServant.SearchAssetsMP(model).Stringify();
            });
        }
        public ActionResult UpdateFlowStatus(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return new StringResult() { IsSuccess = BuildServant.UpdateFlowStatus(model.TRX_Header_ID,model.FlowStatus) }.Stringify();
            });
        }
        public ActionResult UpdateToBuild(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return BuildServant.UpdateToBuild(model.TRXHeaderID).Stringify();
            });
        }
        public ActionResult UpdateToBuildMP(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return BuildServant.UpdateToBuildMP(model.TRXHeaderID).Stringify();
            });
        }
        public ActionResult SearchUserDataByBranch(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return BuildServant.SearchUserDataByBranch(model.BranchCode).Stringify();
            });
        }
    }
}