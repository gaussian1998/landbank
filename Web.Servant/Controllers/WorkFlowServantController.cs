﻿using Library.Servant.Communicate;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.WorkFlow;
using Library.Servant.Servant.WorkFlow.Models;
using System.Web.Mvc;

namespace Web.Servant.Skeleton.Controllers
{
    public class WorkFlowServantController : ServantController
    {
        [HttpPost]
        public ActionResult New(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                return WorkFlowServant.New().Stringify();
            });
        }

        [HttpPost]
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return WorkFlowServant.Index(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Details(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return WorkFlowServant.Details(model.Value).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Create(WebOptional optional)
        {
            return Validate<CreateModel>(optional, model => {

                return WorkFlowServant.Create(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Update(WebOptional optional)
        {
            return Validate<UpdateModel>(optional, model => {

                return WorkFlowServant.Update(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Delete(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return WorkFlowServant.Delete(model.Value).Stringify();
            });
        }
        [HttpPost]
        public ActionResult CheckFlowCode(WebOptional optional)
        {
            return Validate<UpdateModel>(optional, model => {
                bool _pass = WorkFlowServant.CheckFlowCode(model);
                BoolResult _result = new BoolResult();
                _result.Value = _pass;
                return _result.Stringify();
            });
        }

        private LocalWorkFlowServant WorkFlowServant = new LocalWorkFlowServant();
    }
}