﻿using Library.Servant.Communicate;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Exceptions;
using System.Web.Mvc;

namespace Web.Servant.Skeleton.Controllers
{
    public class ExceptionServantController : ServantController
    {
        public ActionResult Index(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                return ExceptionServant.Exception().Stringify();
            });
        }

        public ActionResult NotSupported(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                return ExceptionServant.NotSupported().Stringify();
            });
        }

        public ActionResult InvalidOperation(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                return ExceptionServant.InvalidOperation().Stringify();
            });
        }

        public ActionResult NOT_NULL(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                return ExceptionServant.NOT_NULL().Stringify();
            });
        }

        public ActionResult UNIQUE(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                return ExceptionServant.UNIQUE().Stringify();
            });
        }

        private LocalExceptionServant ExceptionServant = new LocalExceptionServant();
    }
}