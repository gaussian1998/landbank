﻿using Library.Servant.Servant.Inspect;
using Library.Servant.Servant.Inspect.InspectModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Communicate;
using System.Xml;

namespace Web.Servant.Skeleton.Controllers
{
    public class InspectServantController : ServantController
    {

        private IInspectServant InspectServant = new LocalInspectServant();


        public ActionResult GetLand(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return InspectServant.GetLand(model).Stringify();
            });
        }
        public ActionResult GetReport(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return InspectServant.GetReport(model).Stringify();
            });
        }
        
        public ActionResult UpdateRemark(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return InspectServant.UpdateRemark(model).Stringify();
            });
        }
        
        public ActionResult GetUser(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return InspectServant.GetUser(model).Stringify();
            });
        }
        
        public ActionResult GetDocID(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return InspectServant.GetDocID(model).Stringify();
            });
        }
        
        public ActionResult OnI200Finish(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return InspectServant.OnI200Finish(model).Stringify();
            });
        }
        public ActionResult OnFinish(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return InspectServant.OnFinish(model).Stringify();
            });
        }
        public ActionResult GetPicList(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return InspectServant.GetPicList(model).Stringify();
            });
        }
        
        public ActionResult SavePic(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return InspectServant.SavePic(model).Stringify();
            });
        }
        
        public ActionResult UpdateDetail(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return InspectServant.UpdateDetail(model).Stringify();
            });
        }
        
        public ActionResult GetHisIns(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return InspectServant.GetHisIns(model).Stringify();
            });
        }
        public ActionResult GetHisInsA(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return InspectServant.GetHisInsA(model).Stringify();
            });
        }
        public ActionResult ContentDetail(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return InspectServant.ContentDetail(model).Stringify();
            });
        }
        public ActionResult ContentDetailA(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return InspectServant.ContentDetailA(model).Stringify();
            });
        }
        public ActionResult GetMaxNo(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return InspectServant.GetMaxNo(model).Stringify();
            });
        }
        public ActionResult GetDetail(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return InspectServant.GetDetail(model).Stringify();
            });
        }
        public ActionResult GetDetailA(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return InspectServant.GetDetailA(model).Stringify();
            });
        }
        public ActionResult Index(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return InspectServant.Index(model).Stringify();
            });
        }
        public ActionResult IndexA(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return InspectServant.IndexA(model).Stringify();
            });
        }

        public ActionResult GetTarget(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return InspectServant.GetTarget(model).Stringify();
            });
        }
        public ActionResult GetCategoryKind(WebOptional optional)
        {
            return Validate<EInt>(optional, model => {
                return InspectServant.GetCategoryKind(model).Stringify();
            });
        }
        public ActionResult GetCategoryType(WebOptional optional)
        {
            return Validate<EInt>(optional, model => {
                return InspectServant.GetCategoryType(model).Stringify();
            });
        }
        public ActionResult DocumentType(WebOptional optional)
        {
            return Validate<EInt>(optional, model => {
                return InspectServant.DocumentType(model).Stringify();
            });
        }
        public ActionResult DocumentTypeOne(WebOptional optional)
        {
            return Validate<EInt>(optional, model => {
                return InspectServant.DocumentTypeOne(model).Stringify();
            });
        }
        public ActionResult GetDPM(WebOptional optional)
        {
            return Validate<EInt>(optional, model => {
                return InspectServant.GetDPM(model).Stringify();
            });
        }
        public ActionResult GetOrder(WebOptional optional)
        {
            return Validate<EString>(optional, model => {
                return InspectServant.GetOrder(model).Stringify();
            });
        }

        public ActionResult GetCode(WebOptional optional)
        {
            return Validate<EString>(optional, model => {
                return InspectServant.GetCode(model).Stringify();
            });
        }
        public ActionResult UpdateOrder(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return InspectServant.UpdateOrder(model).Stringify();
            });

        }
        public ActionResult UpdateOrderA(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return InspectServant.UpdateOrderA(model).Stringify();
            });

        }
    }
}