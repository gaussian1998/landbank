﻿using Library.Entity.Extension;
using Library.Servant.Communicate;
using Library.Utility;
using Newtonsoft.Json;
using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Reflection.Emit;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Web.Servant.Skeleton.Controllers
{
    public class ServantController :Controller
    {
        protected ActionResult Validate<Model>(WebOptional optional, Func<Model, string> func)
            where Model : AbstractEncryptionDTO, new()
        {
            try
            {
                Optional<Model> model = optional.Get<Model>();
                if (model)
                    return Content(func(model.Value));
                else
                    return Content(WebOptional.Error(model.ErrorMessage, model.ErrorCode));
            }
            catch(DbUpdateException ex)
            {
                return Content( WebOptional.Error(ex.Parser()) );
            }
            catch(DbEntityValidationException ex)
            {
                return Content(WebOptional.Error(ex.Parser()));
            }
            catch (Exception ex)
            {
                return Content(WebOptional.Error(ex.Message));
            }
        }

        protected ActionResult Validate(Func<string> func)
        {
            try
            {
                return Content( func() );
            }
            catch (DbUpdateException ex)
            {
                return Content(WebOptional.Error(ex.Parser()));
            }
            catch (DbEntityValidationException ex)
            {
                return Content(WebOptional.Error(ex.Parser()));
            }
            catch (Exception ex)
            {
                return Content(WebOptional.Error(ex.Message));
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string _controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            string _actionName = filterContext.ActionDescriptor.ActionName;
            //string _id = filterContext.ActionParameters.First(x => x.Key == "id").Value.ToString();
            //if (_controllerName != "Account" && _actionName != "Login")
            //{
            //    //檢核session
            //    if (HttpContext.Session["_EmployeeId"] == null)
            //    {
            //        FormsAuthentication.SignOut();
            //        //檢核Request類型決定直接導回首頁或丟SessionExpiredException由 js handle error 導回首頁
            //        if (!Request.IsAjaxRequest())
            //        {
            //            filterContext.Result = new RedirectResult("~/Account/Login");
            //        }
            //        else
            //        {
            //            filterContext.Result = new RedirectResult("~/Account/Login");
            //            throw new SessionExpiredException("session error");
            //        }
            //    }
            //}
            //else
            //{
            //    ViewBag.IsInsert = true;
            //}
            base.OnActionExecuting(filterContext);

        }

        protected override void OnException(ExceptionContext filterContext)
        {

            //ExceptionHandlerEventArgs _Args = new ExceptionHandlerEventArgs
            //     (
            //     ExceptionLevel.Error,
            //     filterContext.Exception,
            //     HttpContext
            //     );

            //ExceptionHandler.Handle
            //    (
            //    filterContext.Controller,
            //    _Args
            //    );
            ////LogException
            //TxtFileExceptionHandler eh = new TxtFileExceptionHandler();

            //TxtFileExceptionHandlerInfoData _txtfile = new TxtFileExceptionHandlerInfoData();
            //_txtfile.FileName = "Error_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            //_txtfile.Path = AppDomain.CurrentDomain.BaseDirectory + "\\log";
            //_txtfile.ApplicationName = filterContext.Controller.GetType().ToString();
            //eh.Handle(filterContext.Controller.GetType(), filterContext.Exception, _txtfile);

            //HttpStatusCode _HttpStatusCode = HttpStatusCode.InternalServerError;
            //filterContext.HttpContext.Response.Clear();
            //filterContext.HttpContext.Response.StatusCode = (int)_HttpStatusCode;

            //if (filterContext.Exception is SessionExpiredException)
            //{
            //    filterContext.HttpContext.Response.StatusCode = 500;
            //    filterContext.Result = Content
            //            (
            //                JsonConvert.SerializeObject(new ExceptionMessage("401", "Session Expire.", "en", null)),
            //                 "application/json"
            //            );
            //}
            //else
            //{
            //    string JsonContent = "";

            //    //if (filterContext.Exception is BaseException)
            //    //{
            //    //    filterContext.HttpContext.Response.StatusCode = 444;
            //    //    JsonContent = JsonConvert.SerializeObject(((BaseException)filterContext.Exception).ExceptionMessage);
            //    //}
            //    //else
            //    //{
            //    //    if (filterContext.Exception is BusinessLogic.Model.WebException)
            //    //    {
            //    //        filterContext.HttpContext.Response.StatusCode = 444;
            //    //        JsonContent = JsonConvert.SerializeObject(((BusinessLogic.Model.WebException)filterContext.Exception).ExceptionMessage);
            //    //    }
            //    //    else
            //    //    {
            //    //        filterContext.HttpContext.Response.StatusCode = 500;
            //    //        JsonContent = JsonConvert.SerializeObject(new ExceptionMessage("500", "Internal ServerError.", "ch-tw", null));
            //    //    }
            //    //}

            //    filterContext.Result = Content
            //        (
            //           JsonContent,
            //             "application/json"
            //        );
            //}
            //filterContext.ExceptionHandled = true;
            //filterContext.HttpContext.ClearError();

            base.OnException(filterContext);
        }
    }
}