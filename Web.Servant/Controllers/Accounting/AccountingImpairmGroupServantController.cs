﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.AccountingImpairmGroup;
using Library.Servant.Servant.AccountingImpairmGroup.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace Web.Servant.Skeleton.Controllers
{
    public class AccountingImpairmGroupServantController : ServantController
    {
        public ActionResult BatchDelete(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return Servant.Index(model).Stringify();
            });
        }
        public ActionResult Create(WebOptional optional)
        {
            return Validate<DetailModel>(optional, model => {

                return Servant.Create(model).Stringify();
            });
        }
        public ActionResult Detail(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return Servant.Detail(model.Value).Stringify();
            });
        }
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return Servant.Index(model).Stringify();
            });
        }
        public ActionResult Update(WebOptional optional)
        {
            return Validate<DetailModel>(optional, model => {

                return Servant.Update(model).Stringify();
            });
        }
        private readonly LocalAccountingImpairmGroupServant Servant = new LocalAccountingImpairmGroupServant();
    }
}