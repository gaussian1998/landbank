﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.KeepPosition;
using Library.Servant.Servant.KeepPosition.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.IO;
using System.Text;

namespace Web.Servant.Skeleton.Controllers
{
    public class KeepPositionServantController : ServantController
    {
        public ActionResult BatchDelete(WebOptional optional)
        {
            return Validate<BatchDeleteModel>(optional, model => {
                return Servant.BatchDelete(model).Stringify();
            });
        }
        public ActionResult Create(WebOptional optional)
        {
            return Validate<DetailModel>(optional, model => {
                return Servant.Create(model).Stringify();
            });
        }
        public ActionResult Update(WebOptional optional)
        {
            return Validate<DetailModel>(optional, model => {
                return Servant.Update(model).Stringify();
            });
        }
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return Servant.Index(model).Stringify();
            });
        }
        public ActionResult Detail(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {
                return Servant.Detail(model.Value).Stringify();
            });
        }
        public ActionResult GetSubDept(WebOptional optional)
        {
            ListModel _listModel = new ListModel();
            _listModel.ListSelectedModel = new List<SelectedModel>();
            _listModel.ListSelectedModel = Servant.GetSubDept();
            return Validate<VoidModel>(optional, model => {
                return _listModel.Stringify();
            });
        }
        private readonly LocalKeepPositionServant Servant = new LocalKeepPositionServant();
    }
}