﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.AccountingAssetParentRelation;
using Library.Servant.Servant.AccountingAssetParentRelation.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.IO;
using System.Text;

namespace Web.Servant.Skeleton.Controllers
{
    public class AccountingAssetParentRelationServantController : ServantController
    {
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return Servant.Index(model).Stringify();
            });
        }
        private readonly LocalAccountingAssetParentRelationServant Servant = new LocalAccountingAssetParentRelationServant();
    }
}