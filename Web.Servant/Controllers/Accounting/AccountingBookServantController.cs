﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.AccountingBook;
using Library.Servant.Servant.AccountingBook.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace Web.Servant.Skeleton.Controllers
{
    public class AccountingBookServantController : ServantController
    {
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return Servant.Index(model).Stringify();
            });
        }
        public ActionResult Create(WebOptional optional)
        {
            return Validate<CreateModel>(optional, model => {

                return Servant.Create(model).Stringify();
            });
        }
        public ActionResult Detail(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return Servant.Detail(model.Value).Stringify();
            });
        }
        public ActionResult Update(WebOptional optional)
        {
            return Validate<UpdateModel>( optional, model => {

                return Servant.Update(model).Stringify();
            });
        }

        public ActionResult BatchUpdate(WebOptional optional)
        {
            return Validate<ListBatchUpdateModel>(optional, model => {

                return Servant.BatchUpdate(model.ListData).Stringify();
            });
        }

        private readonly LocalAccountingBookServant Servant = new LocalAccountingBookServant();
    }
}