﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.AccountingTradeAccount;
using Library.Servant.Servant.AccountingTradeAccount.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.IO;
using System.Text;

namespace Web.Servant.Skeleton.Controllers
{
    public class AccountingTradeAccountSubServantController : ServantController
    {
        public ActionResult BatchDelete(WebOptional optional)
        {
            return Validate<BatchDeleteModel>(optional, model => {
                return Servant.BatchDelete(model).Stringify();
            });
        }
        public ActionResult Create(WebOptional optional)
        {
            return Validate<SubDetailModel>(optional, model => {
                return Servant.Create(model).Stringify();
            });
        }
        public ActionResult Detail(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {
                return Servant.Detail(model.Value).Stringify();
            });
        }
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SubQueryModel>(optional, model => {
                return Servant.Index(model).Stringify();
            });
        }
        public ActionResult Update(WebOptional optional)
        {
            return Validate<SubDetailModel>(optional, model => {
                return Servant.Update(model).Stringify();
            });
        }
        private readonly LocalAccountingTradeAccountSubServant Servant = new LocalAccountingTradeAccountSubServant();
    }
}