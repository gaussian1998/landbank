﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.AssetLife;
using Library.Servant.Servant.AssetLife.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.IO;
using System.Text;

namespace Web.Servant.Skeleton.Controllers
{
    public class AssetLifeServantController : ServantController
    {
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return Servant.Index(model).Stringify();
            });
        }
        public ActionResult Create(WebOptional optional)
        {
            return Validate<DetailModel>(optional, model => {

                return Servant.Create(model).Stringify();
            });
        }
        public ActionResult Detail(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return Servant.Detail(model.Value).Stringify();
            });
        }
        public ActionResult Update(WebOptional optional)
        {
            return Validate<DetailModel>( optional, model => {

                return Servant.Update(model).Stringify();
            });
        }

        public ActionResult BatchUpdate(WebOptional optional)
        {
            return Validate<BatchUpdateModel>(optional, model => {
                return Servant.BatchUpdate(model).Stringify();
            });
        }
        public ActionResult ExportData(WebOptional optional)
        {
            ByteModel _model = new ByteModel();

            return Validate<SearchModel>(optional, model => {
                _model.Value = Servant.ExportData(model);
                return _model.Stringify();
            });
        }
        public ActionResult ImportData()
        {
            string _httpContents;
            HttpRequestBase _request = HttpContext.Request;
            using (Stream receiveStream = _request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    _httpContents = readStream.ReadToEnd();
                }
            }
            WebOptional optional = JsonConvert.DeserializeObject<WebOptional>(_httpContents);
            return Validate<ListDetailModel>(optional, model => {
                return Servant.ImportData(model.List,model.LastUpdateBy).Stringify();
            });
        }
        private readonly LocalAssetLifeServant Servant = new LocalAssetLifeServant();
    }
}