﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.AccountingSubject;
using Library.Servant.Servant.AccountingSubject.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.IO;
using System.Text;

namespace Web.Servant.Skeleton.Controllers
{
    public class AccountingSubjectServantController : ServantController
    {
        public ActionResult BatchDelete(WebOptional optional)
        {
            return Validate<BatchDeleteModel>(optional, model => {
                return Servant.BatchDelete(model).Stringify();
            });
        }
        public ActionResult Create(WebOptional optional)
        {
            return Validate<DetailModel>(optional, model => {
                return Servant.Create(model).Stringify();
            });
        }
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return Servant.Index(model).Stringify();
            });
        }
        public ActionResult Update(WebOptional optional)
        {
            return Validate<DetailModel>(optional, model => {
                return Servant.Update(model).Stringify();
            });
        }
        public ActionResult Detail(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {
                return Servant.Detail(model.Value).Stringify();
            });
        }
        public ActionResult GetAccountInfo(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {
                return Servant.GetAccountInfo(model.Value).Stringify();
            });
        }
        public ActionResult ExportData(WebOptional optional)
        {
            ByteModel _byteModel = new ByteModel();
            return Validate<SearchModel>(optional, model => {
                _byteModel.Value = Servant.ExportData(model);
                return _byteModel.Stringify();
            });
        }
        public ActionResult GetFlowFormId(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {
                IntModel _intModel = new IntModel();
                _intModel.Value = Servant.GetFlowFormId();
                return _intModel.Stringify();
            });
        }
        public ActionResult ImportData()
        {
            string _httpContents;
            HttpRequestBase _request = HttpContext.Request;
            using (Stream receiveStream = _request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    _httpContents = readStream.ReadToEnd();
                }
            }
            WebOptional optional = JsonConvert.DeserializeObject<WebOptional>(_httpContents);
            return Validate<ListDetailModel>(optional, model => {
                return Servant.ImportData(model.ListData).Stringify();
            });
        }
        public ActionResult GetFlowFormNo(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {
                StringModel _stringModel = new StringModel();
                _stringModel.Value = Servant.GetFlowFormNo();
                return _stringModel.Stringify();
            });
        }
        private readonly LocalAccountingSubjectServant Servant = new LocalAccountingSubjectServant();
    }
}