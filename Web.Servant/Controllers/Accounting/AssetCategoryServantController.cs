﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.AssetCategory;
using Library.Servant.Servant.AssetCategory.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;
using Library.Entity.Edmx;
using System.Linq;

namespace Web.Servant.Skeleton.Controllers
{
    public class AssetCategoryServantController : ServantController
    {
        public ActionResult GetMainKindByNo(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {
                return Servant.GetMainKindByNo(model.Value).Stringify();
            });
        }

        public ActionResult GetMainKinds(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {
                ListAssetCategoryMainKindsModel _mode = new ListAssetCategoryMainKindsModel();
                _mode.ListData = new List<AssetCategoryMainKindsModel>();
                _mode.ListData = Servant.GetMainKinds().ToList();
                return _mode.Stringify();
            });
        }
        public ActionResult GetUseTypes(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {
                ListAssetCategoryUseTypesModel _mode = new ListAssetCategoryUseTypesModel();
                _mode.ListData = new List<AssetCategoryUseTypesModel>();
                _mode.ListData = Servant.GetUseTypes().ToList();
                return _mode.Stringify();
            });
        }
        public ActionResult AddNewMainKind(WebOptional optional)
        {
            return Validate<AssetCategoryMainKindsModel>(optional, model => {
                Servant.UserId = (int)model.Last_Updated_By;
                Servant.AddNewMainKind(model);
                return (new VoidModel { }).Stringify();
            });
        }
        public ActionResult UpdateMainKind(WebOptional optional)
        {
            return Validate<AssetCategoryMainKindsModel>(optional, model => {
                Servant.UserId = (int)model.Last_Updated_By;
                Servant.UpdateMainKind(model);
                return (new VoidModel { }).Stringify();
            });
        }
        public ActionResult DeleteMainKind(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {
                //Servant.UserId = (int)model.OldData.Last_Updated_By;
                Servant.DeleteMainKind(model.Value);
                return (new VoidModel { }).Stringify();
            });
        }
        public ActionResult GetDetailKind(WebOptional optional)
        {
            return Validate<AssetCategoryDetailKindsModel>(optional, model => {
                AssetCategoryDetailKindsModel _mode = new AssetCategoryDetailKindsModel();
                //_mode = new AS_Asset_Category_Detail_Kinds();
                _mode =Servant.GetDetailKind(model.Main_Kind_ID,model.No);
                //for (int _iCount = 0; _iCount < _mode.ListData.Count; _iCount++)
                //{
                //    _mode.ListData[_iCount].AS_Asset_Category = null;
                //}
                return _mode.Stringify();
            });
        }
        public ActionResult GetDetailKindByID(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {
                AssetCategoryDetailKindsModel _mode = new AssetCategoryDetailKindsModel();
                //_mode.Data = new AS_Asset_Category_Detail_Kinds();
                _mode = Servant.GetDetailKind(model.Value);
                //for (int _iCount = 0; _iCount < _mode.ListData.Count; _iCount++)
                //{
                //    _mode.ListData[_iCount].AS_Asset_Category = null;
                //}
                return _mode.Stringify();
            });
        }
        public ActionResult AddNewDetailKind(WebOptional optional)
        {
            return Validate<AssetCategoryDetailKindsModel>(optional, model => {
                Servant.UserId = (int)model.Last_Updated_By;
                Servant.AddNewDetailKind(model);
                return (new VoidModel { }).Stringify();
            });
        }
        public ActionResult UpdateDetailKind(WebOptional optional)
        {
            return Validate<AssetCategoryDetailKindsModel>(optional, model => {
                Servant.UserId = (int)model.Last_Updated_By;
                Servant.UpdateDetailKind(model);
                return (new VoidModel { }).Stringify();
            });
        }
        public ActionResult DeleteDetailKind(WebOptional optional)
        {
            return Validate<AssetCategoryDetailKindsModel>(optional, model => {
                Servant.UserId = (int)model.Last_Updated_By;
                Servant.DeleteDetailKind(model.Main_Kind_ID,model.No);
                return (new VoidModel { }).Stringify();
            });
        }
        public ActionResult GetUseTypeByNo(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {
                AssetCategoryUseTypesModel _model = new AssetCategoryUseTypesModel();
                //_mode.Data = new AS_Asset_Category_Use_Types();
                _model= Servant.GetUseTypeByNo(model.Value);
               
                return _model.Stringify();
            });
        }
        public ActionResult AddNewUserType(WebOptional optional)
        {
            return Validate<AssetCategoryUseTypesModel>(optional, model => {
                Servant.UserId = (int)model.Last_Updated_By;
                Servant.AddNewUserType(model);
                return (new VoidModel { }).Stringify();
            });
        }
        public ActionResult UpdateUseType(WebOptional optional)
        {
            return Validate<AssetCategoryUseTypesModel>(optional, model => {
                Servant.UserId = (int)model.Last_Updated_By;
                Servant.UpdateUseType(model);
                return (new VoidModel { }).Stringify();
            });
        }
        public ActionResult DeleteUserType(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {
                Servant.UserId = 0;
                Servant.DeleteUserType(model.Value);
                return (new VoidModel { }).Stringify();
            });
        }
        public ActionResult GetCategory(WebOptional optional)
        {
            return Validate<AssetCategoryModel>(optional, model => {
                AssetCategoryModel _mode = new AssetCategoryModel();
                _mode = Servant.GetCategory(model.Main_Kind_ID, model.Detail_Kind_ID, model.Use_Type_ID);
                return _mode.Stringify();
            });
        }
        public ActionResult AddNewCategory(WebOptional optional)
        {
            return Validate<AssetCategoryModel>(optional, model => {
                Servant.UserId = (int)model.Last_Updated_By;
                Servant.AddNewCategory(model);
                return (new VoidModel { }).Stringify();
            });
        }
        public ActionResult DeleteCategory(WebOptional optional)
        {
            return Validate<AssetCategoryModel>(optional, model => {               
                Servant.DeleteCategory(model);
                return (new VoidModel { }).Stringify();
            });
        }
        public ActionResult GetListDetailKindByMainKindNo(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {
                ListAssetCategoryDetailKindsModel _mode = new ListAssetCategoryDetailKindsModel();
                _mode.ListData = new List<AssetCategoryDetailKindsModel>();
                _mode.ListData = Servant.GetListDetailKindByMainKindNo(model.Value).ToList();
                return _mode.Stringify();
            });
        }
        private readonly LocalAssetCategoryServant Servant = new LocalAssetCategoryServant();
    }
}