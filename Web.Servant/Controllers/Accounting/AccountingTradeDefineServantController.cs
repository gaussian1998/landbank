﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.AccountingTradeDefine;
using Library.Servant.Servant.AccountingTradeDefine.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.IO;
using System.Text;

namespace Web.Servant.Skeleton.Controllers
{
    public class AccountingTradeDefineServantController : ServantController
    {
        public ActionResult BatchDelete(WebOptional optional)
        {
            return Validate<BatchDeleteModel>(optional, model => {
                return Servant.BatchDelete(model).Stringify();
            });
        }
        public ActionResult Create(WebOptional optional)
        {
            return Validate<DetailModel>(optional, model => {
                return Servant.Create(model).Stringify();
            });
        }
        public ActionResult Detail(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {
                return Servant.Detail(model.Value).Stringify();
            });
        }
        public ActionResult DetailDecimal(WebOptional optional)
        {
            return Validate<DecimalModel>(optional, model => {
                return Servant.Detail(model.Value).Stringify();
            });
        }
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return Servant.Index(model).Stringify();
            });
        }
        public ActionResult QueryTradeDefine(WebOptional optional)
        {
            return Validate<QueryTradeDefineModel>(optional, model => {
                return Servant.QueryTradeDefine(model.TransType,model.TradeType).Stringify();
            });
        }
        public ActionResult Update(WebOptional optional)
        {
            return Validate<DetailModel>(optional, model => {
                return Servant.Update(model).Stringify();
            });
        }
        private readonly LocalAccountingTradeDefineServant Servant = new LocalAccountingTradeDefineServant();
    }
}