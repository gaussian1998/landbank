﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.ExchangeRate;
using Library.Servant.Servant.ExchangeRate.Model;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.IO;
using System.Text;

namespace Web.Servant.Skeleton.Controllers
{
    public class ExchangeRateServantController : ServantController
    {
        public ActionResult BatchDelete(WebOptional optional)
        {
            return Validate<BatchDeleteModel>(optional, model => {
                return Servant.BatchDelete(model).Stringify();
            });
        }
        public ActionResult Create(WebOptional optional)
        {
            return Validate<CreateModel>(optional, model => {
                return Servant.Create(model).Stringify();
            });
        }
        public ActionResult Detail(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {
                return Servant.Detail(model.Value).Stringify();
            });
        }
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return Servant.Index(model).Stringify();
            });
        }
        private readonly LocalExchangeRateServant Servant = new LocalExchangeRateServant();
    }
}