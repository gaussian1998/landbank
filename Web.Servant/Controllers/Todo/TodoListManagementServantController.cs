﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Library.Entity.Edmx;
using Library.Servant.Communicate;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.TodoListManagement;
using Library.Servant.Servant.TodoListManagement.Models;
using Library.Servant.Servants.AbstractFactory;

namespace Web.Servant.Skeleton.Controllers
{
    public class TodoListManagementServantController : ServantController
    {
        public ActionResult Delete(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {

                return Servant.Delete(model.Value).Stringify();
            });
        }

        public ActionResult Detail(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {

                return Servant.Detail(model.Value).Stringify();
            });
        }

        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return Servant.Index(model).Stringify();
            });
        }

        public ActionResult QueryIndex(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {

                return Servant.QueryIndex(model, model.UserId).Stringify();
            });
        }
        
        public ActionResult SaveTodoTask(WebOptional optional)
        {
            return Validate<TodoListInfo>(optional, model => {

                return Servant.SaveTodoTask(model).Stringify();
            });
        }

        public ActionResult GetTodoList(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return LocalTodoListManagement.GetTodoList(model.Value).Stringify();
            });
        }

        //public ActionResult GetReportContent(WebOptional optional)
        //{
        //    return Validate<ReportSearchModel>(optional, model => {

        //        return Servant.GetReportContent(model).Stringify();
        //    });
        //}

        private LocalTodoListManagement Servant = new LocalTodoListManagement();
    }
}
