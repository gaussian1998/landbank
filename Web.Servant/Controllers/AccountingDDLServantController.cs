﻿using Library.Servant.Communicate;
using Library.Servant.Servant.Accounting;
using Library.Servant.Servant.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Servant.Skeleton.Controllers
{
    public class AccountingDDLServantController : ServantController
    {
        [HttpPost]
        public ActionResult GetTradeTypes(WebOptional optional)
        {
            return Content("Pending");
        }

        [HttpPost]
        public ActionResult GetTransTypes(WebOptional optional)
        {
            return Content("Pending");
        }

        private static readonly IAccountingDDLServant _ddlServant = new LocalAccountingDDLServant();
    }
}