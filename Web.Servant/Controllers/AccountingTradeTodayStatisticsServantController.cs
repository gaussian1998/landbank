﻿using Library.Servant.Communicate;
using Library.Servant.Servant.AccountingTradeTodayStatistics;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System.Web.Mvc;

namespace Web.Servant.Skeleton.Controllers
{
    public class AccountingTradeTodayStatisticsServantController : ServantController
    {
        [HttpPost]
        public ActionResult Options(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                return Servant.Options().Stringify();
            });
        }

        [HttpPost]
        public ActionResult Index(WebOptional optional)
        {
            return Validate<BranchSearchModel>(optional, model => {

                return Servant.Index(model).Stringify();
            });
        }

        private LocalAccountingTradeTodayStatisticsServant Servant = new LocalAccountingTradeTodayStatisticsServant();
    }
}