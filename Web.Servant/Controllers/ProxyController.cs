﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Web.Servant.Skeleton.Controllers
{
    public class ProxyController : Controller
    {
        // GET: Proxy
        public ActionResult Index()
        {
            return Content( Request.ServerVariables["ALL_HTTP"] );
        }
    }
}