﻿using Library.Servant.Communicate;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.FileServant;
using Library.Servant.Servant.FileServant.Models;
using Library.Servant.Servant;
using System.Web;
using System.Web.Mvc;

namespace Web.Servant.Skeleton.Controllers
{
    public class FileServantController : ServantController
    {
        [HttpPost]
        public ActionResult Open(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {

                return new IntModel { Value = Servant.Open(model.Value) }.Stringify();
            });
        }

        [HttpPost]
        public ActionResult Create(WebOptional optional)
        {
            return Validate<CreateModel>(optional, model => {

                return new IntModel { Value = Servant.Create(model.FilePath,model.Owner) }.Stringify();
            });
        }

        [HttpPost]
        public ActionResult Write(HttpPostedFileBase file, FormCollection form)
        {
            return Validate( () => {

                int fd = int.Parse( file.FileName );
                byte[] data = file.InputStream.ReadToEnd();

                return new BoolResult { Value = Servant.Write( fd, data ) }.Stringify();
            });
        }

        [HttpPost]
        public ActionResult Read(WebOptional optional)
        {
            return Validate<IntModel>( optional, model => {

                return new ByteModel { Value = Servant.Read( model.Value ) }.Stringify();
            });
        }

        private LocalFileServant Servant = new LocalFileServant();
    }
}