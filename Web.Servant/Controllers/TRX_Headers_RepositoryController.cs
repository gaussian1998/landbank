﻿using Library.Servant.Communicate;
using Library.Servant.Repository.Models;
using Library.Servant.Repository.TRX_Headers;
using Library.Servant.Repository.TRX_Headers.Models;
using Library.Servant.Servant.Common;
using System.Web.Mvc;

namespace Web.Servant.Skeleton.Controllers
{
    public class TRX_Headers_RepositoryController : ServantController
    {
        [HttpPost]
        public ActionResult Index(WebOptional optional)
        {
            return Validate< VoidModel >(optional, model => {

                return Repository.Index().Stringify();
            });
        }


        private readonly Local_TRX_Headers_Repository Repository = new Local_TRX_Headers_Repository();
    }
}