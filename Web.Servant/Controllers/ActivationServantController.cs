﻿using Library.Servant.Servant.Activation;
using Library.Servant.Servant.Activation.ActivationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Communicate;

namespace Web.Servant.Skeleton.Controllers
{
    public class ActivationServantController : ServantController
    {

        private IActivationServant ActivationServant = new LocalActivationServant();
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return ActivationServant.Index(model).Stringify();
            });
        }
        public ActionResult EDOCServiceTest(WebOptional optional)
        {
            EDOCService.EDOCService a = new EDOCService.EDOCService();
            return Validate<EJObject>(optional, model => {
                return a.getCity();
            });
           
           
        }
        public ActionResult GetDocID(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return ActivationServant.GetDocID(model).Stringify();
            });
        }
        
        public ActionResult GetMaxNo(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return ActivationServant.GetMaxNo(model).Stringify();
            });
        }
        public ActionResult testIndex(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return ActivationServant.testIndex(model).Stringify();
            });
        }
        public ActionResult J200IndexList(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return ActivationServant.J200IndexList(model).Stringify();
            });
        }
        
        public ActionResult GetBuild(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return ActivationServant.GetBuild(model).Stringify();
            });
        }
        public ActionResult GetLand(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return ActivationServant.GetLand(model).Stringify();
            });
        }
        public ActionResult GetPB(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return ActivationServant.GetPB(model).Stringify();
            });
        }
        public ActionResult GetAPB(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return ActivationServant.GetAPB(model).Stringify();
            });
        }
        public ActionResult GetAB(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return ActivationServant.GetAB(model).Stringify();
            });
        }
        public ActionResult GetAL(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return ActivationServant.GetAL(model).Stringify();
            });
        }
        public ActionResult GetTarget(WebOptional optional)
        {
            return Validate<SearchModel>(optional, model => {
                return ActivationServant.GetTarget(model).Stringify();
            });
        }
        public ActionResult GetTargetE(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return ActivationServant.GetTargetE(model).Stringify();
            });
        }
        public ActionResult DocumentType(WebOptional optional)
        {
            return Validate<EInt>(optional, model => {
                return ActivationServant.DocumentType(model).Stringify();
            });
        }
        public ActionResult DocumentTypeOne(WebOptional optional)
        {
            return Validate<EInt>(optional, model => {
                return ActivationServant.DocumentTypeOne(model).Stringify();
            });
        }
        public ActionResult GetDPM(WebOptional optional)
        {
            return Validate<EInt>(optional, model => {
                return ActivationServant.GetDPM(model).Stringify();
            });
        }
        public ActionResult GetOrder(WebOptional optional)
        {
            return Validate<EString>(optional, model => {
                return ActivationServant.GetOrder(model).Stringify();
            });
        }
        
        public ActionResult GetCode(WebOptional optional)
        {
            return Validate<EString>(optional, model => {
                return ActivationServant.GetCode(model).Stringify();
            });
        }
        public ActionResult UpdateOrder(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return ActivationServant.UpdateOrder(model).Stringify();
            });

        }
    }
}