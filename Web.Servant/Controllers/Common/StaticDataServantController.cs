﻿using System.Web.Mvc;
using Library.Servant.Communicate;
//using Library.Servant.Servant.Authentication;
//using Library.Servant.Servant.Authentication.Models;
using Library.Servant.Servant.Common;
using Library.Servant.Servant.Common.Models;
using System.Collections.Generic;

namespace Web.Servant.Skeleton.Controllers
{
    public class StaticDataServantController : ServantController
    {
        public ActionResult getIDOptions(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {
                ListModel _list = new ListModel();
                _list.ListIDSelectedModel = new List<IDSelectedModel>();
                _list.ListIDSelectedModel = Servant.getIDOptions(model.Value);
                return _list.Stringify();
            });
        }

        public ActionResult getDocNames(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {
                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.getDocNames();
                return _list.Stringify();
            });
        }
        public ActionResult getDocNamesByDocType(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {
                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.getDocNames(model.Value);
                return _list.Stringify();
            });
        }
        public ActionResult GetStatusText(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {
              string _string=Servant.GetStatusText(model.Value);
                StringModel _stringModel = new StringModel { Value = _string };
                return _stringModel.Stringify();
            });
        }
        public ActionResult GetBranchs(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {
                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetBranchs();
                return _list.Stringify();
            });
        }
        public ActionResult GetDepts(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetDepts();
                return _list.Stringify();
            });
        }
        public ActionResult GetAccountingBooks(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetAccountingBooks();
                return _list.Stringify();
            });
        }
        public ActionResult GetCurrencys(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {
                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetCurrencys();
                return _list.Stringify();
            });
        }
        public ActionResult GetTransTypes(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {
                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetTransTypes();
                return _list.Stringify();
            });
        }
        public ActionResult GetTradeTypes(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {
                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetTradeTypes();
                return _list.Stringify();
            });
        }
        public ActionResult GetAssetMainKinds(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {
                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetAssetMainKinds();
                return _list.Stringify();
            });
        }
        public ActionResult GetAssetDetailKinds(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {
                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetAssetDetailKinds();
                return _list.Stringify();
            });
        }
        public ActionResult GetAssetUseTypes(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {
                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetAssetUseTypes();
                return _list.Stringify();
            });
        }
        public ActionResult GetAllUserInfo(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {
                ListModel _list = new ListModel();
                _list.ListUserInfoOptionModel = new List<UserInfoOptionModel>();
                _list.ListUserInfoOptionModel = Servant.GetAllUserInfo();
                return _list.Stringify();
            });
        }
        public ActionResult GetDocTypes(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {
                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetDocTypes();
                return _list.Stringify();
            });
        }
        public ActionResult GetUserTransactionTypes(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {
                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetUserTransactionTypes();
                return _list.Stringify();
            });
        }
        public ActionResult GetTradeTypesByTransType(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {

                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetTradeTypes(model.Value);
                return _list.Stringify();
            });
        }
        public ActionResult GetApproveRole(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {

                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetApproveRole(model.Value);
                return _list.Stringify();
            });
        }
        public ActionResult GetApproveRoleR04(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetApproveRoleR04();
                return _list.Stringify();
            });
        }
        public ActionResult GetApproveRoleR05(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetApproveRoleR05();
                return _list.Stringify();
            });
        }
        public ActionResult GetApproveRoleR06(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetApproveRoleR06();
                return _list.Stringify();
            });
        }
        public ActionResult GetApproveRoleR07(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetApproveRoleR07();
                return _list.Stringify();
            });
        }
        public ActionResult GetApproveRoleR08(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetApproveRoleR08();
                return _list.Stringify();
            });
        }
        public ActionResult GetFlowCode(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {

                ListModel _list = new ListModel();
                _list.ListSelectedModel = new List<SelectedModel>();
                _list.ListSelectedModel = Servant.GetFlowCode(model.Value);
                return _list.Stringify();
            });
        }
        private readonly LocalStaticDataServant Servant = new LocalStaticDataServant();
    }
}