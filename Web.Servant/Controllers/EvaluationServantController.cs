﻿using Library.Servant.Servant.Evaluation;
using Library.Servant.Servant.Evaluation.EvaluationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Servant.Communicate;
using System.Xml;

namespace Web.Servant.Skeleton.Controllers
{
    public class EvaluationServantController : ServantController
    {

        private IEvaluationServant EvaluationServant = new LocalEvaluationServant();


        public ActionResult GetLand(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return EvaluationServant.GetLand(model).Stringify();
            });
        }
        public ActionResult GetBuild(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return EvaluationServant.GetBuild(model).Stringify();
            });
        }
        
        public ActionResult GetReport(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return EvaluationServant.GetReport(model).Stringify();
            });
        }
        
        public ActionResult UpdateRemark(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return EvaluationServant.UpdateRemark(model).Stringify();
            });
        }
        
        public ActionResult GetUser(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return EvaluationServant.GetUser(model).Stringify();
            });
        }
        
        public ActionResult GetDocID(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return EvaluationServant.GetDocID(model).Stringify();
            });
        }
        
        public ActionResult OnI200Finish(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return EvaluationServant.OnI200Finish(model).Stringify();
            });
        }
        public ActionResult OnFinish(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return EvaluationServant.OnFinish(model).Stringify();
            });
        }
        public ActionResult GetPicList(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return EvaluationServant.GetPicList(model).Stringify();
            });
        }
        
        public ActionResult SavePic(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return EvaluationServant.SavePic(model).Stringify();
            });
        }
        
        public ActionResult UpdateDetail(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return EvaluationServant.UpdateDetail(model).Stringify();
            });
        }
        
        public ActionResult GetHisIns(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return EvaluationServant.GetHisIns(model).Stringify();
            });
        }
        public ActionResult GetHisInsA(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return EvaluationServant.GetHisInsA(model).Stringify();
            });
        }
        public ActionResult ContentDetail(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return EvaluationServant.ContentDetail(model).Stringify();
            });
        }
        public ActionResult ContentDetailA(WebOptional optional)
        {

            return Validate<EJObject>(optional, model => {

                return EvaluationServant.ContentDetailA(model).Stringify();
            });
        }
        public ActionResult GetMaxNo(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return EvaluationServant.GetMaxNo(model).Stringify();
            });
        }
        public ActionResult GetDetail(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return EvaluationServant.GetDetail(model).Stringify();
            });
        }
        public ActionResult GetDetailA(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return EvaluationServant.GetDetailA(model).Stringify();
            });
        }
        public ActionResult Index(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return EvaluationServant.Index(model).Stringify();
            });
        }
        public ActionResult IndexA(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return EvaluationServant.IndexA(model).Stringify();
            });
        }

        public ActionResult GetTarget(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return EvaluationServant.GetTarget(model).Stringify();
            });
        }
        public ActionResult GetCategoryKind(WebOptional optional)
        {
            return Validate<EInt>(optional, model => {
                return EvaluationServant.GetCategoryKind(model).Stringify();
            });
        }
        public ActionResult GetCategoryType(WebOptional optional)
        {
            return Validate<EInt>(optional, model => {
                return EvaluationServant.GetCategoryType(model).Stringify();
            });
        }
        public ActionResult DocumentType(WebOptional optional)
        {
            return Validate<EInt>(optional, model => {
                return EvaluationServant.DocumentType(model).Stringify();
            });
        }
        public ActionResult DocumentTypeOne(WebOptional optional)
        {
            return Validate<EInt>(optional, model => {
                return EvaluationServant.DocumentTypeOne(model).Stringify();
            });
        }
        public ActionResult GetDPM(WebOptional optional)
        {
            return Validate<EInt>(optional, model => {
                return EvaluationServant.GetDPM(model).Stringify();
            });
        }
        public ActionResult GetOrder(WebOptional optional)
        {
            return Validate<EString>(optional, model => {
                return EvaluationServant.GetOrder(model).Stringify();
            });
        }

        public ActionResult GetCode(WebOptional optional)
        {
            return Validate<EString>(optional, model => {
                return EvaluationServant.GetCode(model).Stringify();
            });
        }
        public ActionResult UpdateOrder(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return EvaluationServant.UpdateOrder(model).Stringify();
            });

        }
        public ActionResult UpdateOrderA(WebOptional optional)
        {
            return Validate<EJObject>(optional, model => {
                return EvaluationServant.UpdateOrderA(model).Stringify();
            });

        }
    }
}