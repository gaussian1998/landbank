﻿using System.Web.Mvc;
using Library.Servant.Communicate;
using Library.Servant.Servant.ApprovalRole;
using Library.Servant.Servant.ApprovalRole.Models;
using Library.Servant.Servant.Common.Models;
using Library.Servant.Servant.Common;

namespace Web.Servant.Skeleton.Controllers
{
    public class ApprovalRoleServantController : ServantController
    {
        [HttpPost]
        public ActionResult New(WebOptional optional)
        {
            return Validate<VoidModel>(optional, model => {

                return ApprovalRoleServant.New().Stringify();
            });
        }

        [HttpPost]
        public ActionResult Index(WebOptional optional)
        {
            return Validate<SearchModel>( optional, model => {

                return ApprovalRoleServant.Index(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Details(WebOptional optional)
        {
            return Validate<IntModel>(optional, model => {

                return ApprovalRoleServant.Details(model.Value).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Create(WebOptional optional)
        {
            return Validate<CreateModel>(optional, model => {

                return ApprovalRoleServant.Create(model).Stringify();
            });
        }

        [HttpPost]
        public ActionResult Update(WebOptional optional)
        {
            return Validate<UpdateModel>(optional, model => {

                return ApprovalRoleServant.Update(model).Stringify();
            });
        }
        [HttpPost]
        public ActionResult GetUserId(WebOptional optional)
        {
            return Validate<StringModel>(optional, model => {

                return ApprovalRoleServant.GetUserId(model.Value).Stringify();
            });
        }
        [HttpPost]
        public ActionResult CheckCodeId(WebOptional optional)
        {
            return Validate<UpdateModel>(optional, model => {
                bool _pass = ApprovalRoleServant.CheckCodeId(model);
                BoolResult _result = new BoolResult();
                _result.Value = _pass;
                return _result.Stringify();
            });
        }
        private LocalApprovalRoleServant ApprovalRoleServant = new LocalApprovalRoleServant();
    }
}